<?php
/*
*Develop A Player Home controller
*Develop By Rezaul Karim (karim@techpourri.com)
*/
App::uses('CommonController', 'Controller');
App::uses('CakeEmail','Network/Email');
class HomesController  extends CommonController {
	var $name = 'Homes';
	var $uses = array('PlayerDetail','Course','Club','PlayerPosition','User','CourseCoache','Banner','Product','Highlight','Newslatter');
	 public function beforeFilter() {
        $this->Auth->allow(array('index','trainer_name','newslatter'));
		$this->set('title_for_layout', 'Develop a Player');
	   parent::beforeFilter();
    }
	public function index()
	{
		$today_date=date('Y-m-d');
		$conditions_course= "Course.course_date >='".$today_date."' and Course.status='A' ";
		$course_details = $this->Course->find('all',array('conditions' =>$conditions_course,'order'=>array('Course.book_course desc'),'limit'=>4, 'offset'=>0));
		$this->set('course_details', $course_details);
		//pr($course_details);
		//'recursive'=>3,
		$conditions_p= array(array("User.type"=>"P","User.status"=>"A","PlayerDetail.propuler_player"=>"1"));
		$all_player_details= $this->PlayerDetail->find('all',array('conditions' =>$conditions_p,'fields'=>array('PlayerDetail.*','User.id', 'User.email_address','User.first_name','User.last_name','PrimaryPosition.id','PrimaryPosition.name','SecondaryPosition.id','SecondaryPosition.name'),'limit'=>50, 'offset'=>0));
		$this->set('all_player_details', $all_player_details);	
		//pr($all_player_details);
		
		//============================ Banner Image =======================================		
			$conditions_banner= "Banner.status = 'A'";
			$banner_details = $this->Banner->find('all',array('conditions' =>$conditions_banner));
			//pr($banner_details);
			$this->set('banner_details', $banner_details);		
		//========================== Home Page Coach List  ================================
		
			$this->User->unBindModel(array('hasOne' => array('Nationality','PlayerDetail','CoachSpecialityS')));	
			//$conditions_C= array(array("User.type"=>"C","User.status"=>"A"));
			$conditions_c= array(array("User.type"=>"C","User.status"=>"A","CoachDetail.top_coach_status"=>1));
			$all_coach_data = $this->User->find('all',array('conditions' =>$conditions_c,'fields'=>array('User.id','User.first_name','User.last_name','CoachDetail.profile_img','CoachSpecialityP.name'),'limit'=>8));
			//pr($all_coach_data);
			$this->set('all_coach_data', $all_coach_data);
		
		//========================== Home Page Product List ================================
		
			$this->Product->unBindModel(array('hasMany' => array('ProductSizeQty','AllProductImage')));	
			$all_product= $this->Product->find('all',array('conditions'=>array('Product.status'=>1),'fields'=>array('Product.id','Product.name','Product.description','Product.price','ProductImage.image'),'limit'=>10, 'offset'=>0));	
			$this->set('all_product', $all_product);
			//pr($all_product);
			$condition1='Highlight.status="A"';
			$highlight_video=$this->Highlight->find('all',array('conditions'=>$condition1,'limit'=>5,'order'=>'Highlight.created DESC'));	
			//pr($highlight_video);die;
			$this->set('highlight_video', $highlight_video);
			//$this->set(compact('highlight_video'));
	}
	
	/********************  For Home page Coach name through Request Action  *****************************/
	public function trainer_name()
	{
		//pr($this->params);
		$coach_id =  $this->params['named']['coach_id'];		
		$condition ="User.id='".$coach_id."' and User.type='C'";
		//$course_details = $this->Course->find('all',array('conditions' =>$conditions_course,'order'=>array('Course.book_course asc'),'limit'=>4, 'offset'=>0));
		$trainer_coachArray = $this->User->find('first',array('conditions'=>$condition,'recursive'=>-1));
		//pr($trainer_coachArray);
		if(!empty($trainer_coachArray))
		{
			$coach_name=$trainer_coachArray['User']['first_name']." ".$trainer_coachArray['User']['last_name'];
			//pr($trainer_coach_name);
		}
		else
		{
			$coach_name='';
		}		
		return $coach_name;
		exit();		
	}
	
	public function newslatter(){
		$this->layout = "ajax";
		$this->render(false);
		//print_r($this->request->data);die;
		if($this->request->data)
		{
			$condition ="Newslatter.email='".$this->request->data['email']."'";
			$news_count = $this->Newslatter->find('count',array('conditions'=>$condition));
			if($news_count == 0)
			{
				if($this->Newslatter->save($this->request->data))
				{					
                    /* $this->Email->to =  $this->request->data['email']; 
					$this->Email->subject = "Newsletter";
					$this->Email->from = Configure::read('admin_email');
					$this->Email->layout = '';
					$this->Email->template = 'newletter_subcription';
					$this->Email->sendAs = 'html';		
					//$this->set('all_coach_details', $all_coach_details);
				    $this->Email->send();*/					
					App::uses('CakeEmail', 'Network/Email');
					$this->Email = new CakeEmail('smtp');
					$this->Email->to(array($this->request->data['email']));
					$this->Email->subject('DevelopaPlaye:Newsletter');
					$this->Email->from(array(Configure::read('admin_email1')=>'Develop A Player'));				
					$this->Email->emailFormat('html');
                    //$this->Email->layout(array());					
					$this->Email->template('newletter_subcription',Null);
					$this->Email->send();			     
					$state =1;
				}
			}
            else {
				$state =0;
			}			
			echo json_encode(array('state'=>$state));
		}
	}
	
}
?>