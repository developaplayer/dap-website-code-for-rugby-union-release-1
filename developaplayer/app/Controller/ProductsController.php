<?php
App::uses('CommonController', 'Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class ProductsController  extends CommonController {
	var $name = 'Products';
	var $uses = array('Product','Admin.ProductCategory','ProductSizeQty');
	//var $components = array('Cookie');
	 public function beforeFilter() {
        $this->Auth->allow(array('product_details','product_list','product_search_filter','load_more_product_by_filter','ajx_check_stock'));
		$this->set('title_for_layout', 'Develop a Player');
		  parent::beforeFilter(); 
		    $this->Cookie->name = 'baker_id';
			$this->Cookie->time = 3600;  // or '1 hour'
			$this->Cookie->path = '/';
			//$this->Cookie->domain = 'example.com';
			$this->Cookie->secure = true;  // i.e. only sent if using secure HTTPS
			$this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
			$this->Cookie->httpOnly = true;
			//$this->Cookie->type('aes');
    }	
	public function index() {
		$this->layout = "ajax";
		$this->render(false);
		$this->redirect(array('action'=>'register'));
	}	
	public function product_details($productId)
	{
		$productid=base64_decode($productId);	
        $product_details= $this->Product->find('first',array('conditions'=>array('Product.id'=>$productid )));     	   
	    $this->set('product_details', $product_details);
      //============================================================//
       $product_sizes= $this->ProductSizeQty->find('all',array('conditions'=>array('ProductSizeQty.product_id'=>$productid )));	 
       $this->set('product_sizes', $product_sizes); 
      /*taking customer's Browsing History from details page*/	
		$this->loadModel('GuestSearch');
		$browser_history=array();
		$browser_history['GuestSearch']['session_id']=$this->Session->id();
		$browser_history['GuestSearch']['product_id']=$productid; 
		$browser_history['GuestSearch']['datetime']=date('Y-m-d H:i:s'); 
		 $product_id_exit_or_not=$this->isUniqueBrowse($productid);
		if( $product_id_exit_or_not==0)
		{
		    $this->GuestSearch->save($browser_history,true);
		}           
		$this->loadModel('GuestSearch');
		$this->Product->unBindModel(array('hasMany' => array('AllProductImage')));		
		$GuestDetails = $this->GuestSearch->browsing_products();
		//pr($GuestDetails);
		//exit;
		$this->set('GuestDetails', $GuestDetails);	
	}
	public function product_list()
		{
			$this->set('title_for_layout', 'Product List: All Product');
			$categories=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>0),'limit'=>3, 'offset'=>0,'order'=>'ProductCategory.id DESC'));			
			$this->set('categories',$categories);
		 //=============Product List========================//
		  $this->Product->unBindModel(array('hasMany' => array('ProductSizeQty')));		  		  
		  $all_product= $this->Product->find('all',array('conditions'=>array('Product.status'=>1),'limit'=>3, 'offset'=>0));	
                  $total_product= $this->Product->find('count',array('conditions'=>array('Product.status'=>1)));	
                  	  		 
		  $this->set('all_product', $all_product);	
                  $this->set('total_product', $total_product);	 
		//=============End==================================//						
		}


	public function product_search_filter()
	{  //Configure::write('debug', 2);
		$this->layout="ajax";
		//=============Product List========================//
		  $filterAttr=$this->request->data['filterAttr'];
		  $condition="";
		  if(isset($this->request->data['cat_id']))
		  {
			  $cat_id=$this->request->data['cat_id'];
			  if( $cat_id>0)
			  {
				  $condition='Product.cat_id='.$cat_id.' AND Product.status=1';		 
			  }
		  }else
          {
			  
			   $condition.='Product.status=1';	
		  }			  
        
		  
		  $product_price="";
		   if($filterAttr=='All')
		  {
			$product_price='';  
		  }
		  if($filterAttr=='heigh')
		  {
			$product_price='Product.price DESC ';  
		  }
		  if($filterAttr=='low')
		  {
			$product_price='Product.price ASC ';  
		  }
		   if($filterAttr=='new')
		  {
			$product_price='Product.date_modified DESC ';  
		  }
		 
		  $this->Product->unBindModel(array('hasMany' => array('ProductSizeQty')));
		  $all_product= $this->Product->find('all',array('conditions'=>$condition,'limit'=>5, 'offset'=>0,'order'=>$product_price));		  		 
		  $this->set('all_product', $all_product);
		//================================END=============================================//
	}
	  //==============================Load More Product==================================//
   public function load_more_product_by_filter()
	{
		$this->layout="ajax";
		$last_id=$this->request->data['last_id'];
        $limit=$this->request->data['limit'];
		//=============Product List========================//
		  $filterAttr=$this->request->data['filterAttr'];
		  $condition="";
		  if(isset($this->request->data['cat_id']))
		  {
			  $cat_id=$this->request->data['cat_id'];
			  if( $cat_id>0)
			  {
				  $condition.='Product.cat_id='.$cat_id.' AND Product.status=1';
			  }
		  }else
          {
			  
			  $condition.='Product.status=1'; 
			  
		  }

	  
		  $product_price="";
		  if($filterAttr=='All')
		  {
			$product_price='';  
		  }
		  if($filterAttr=='heigh')
		  {
			$product_price='Product.price DESC ';  
		  }
		  if($filterAttr=='low')
		  {
			$product_price='Product.price ASC ';  
		  }
		   if($filterAttr=='new')
		  {
			$product_price='Product.date_modified DESC ';  
		  }
		     
		//echo  $condition;
		//exit;
		 
		  $this->Product->unBindModel(array('hasMany' => array('ProductSizeQty')));
		  $all_product= $this->Product->find('all',array('conditions'=>$condition,'limit'=>$limit, 'offset'=>$last_id,'order'=>$product_price));	
		 // $all_product= $this->Product->find('all',array('conditions'=>array('Product.status'=>1),'limit'=>$limit, 'offset'=>$last_id,'order'=>$product_price));			  		 
		  $this->set('all_product', $all_product);
		//=============End==================================//
	}
	public function isUniqueBrowse($product_id){
		$this->loadModel('GuestSearch');
		$count=$this->GuestSearch->find("count",array("conditions"=>"product_id='".$product_id."' AND session_id='".session_id()."'"));
		if($count>0)
		return 1;
		else
		return 0;
	}
	function ajx_check_stock()
	{
		$productId=$this->request->data['productId'];
		$Product_size_code=$this->request->data['Product_size_code'];		
		$product_qty=$this->ProductSizeQty->find('first',array('conditions'=>array('ProductSizeQty.product_id'=>$productId,'ProductSizeQty.size_id'=>$Product_size_code)));		
		
	    $product_qty_count=($product_qty['ProductSizeQty']['qty']-$product_qty['ProductSizeQty']['sale_qty']);
		echo $product_qty_count;
		exit;
	}
}
  
