<?php
App::uses('CommonController', 'Controller');
class CmspagesController  extends CommonController {
	var $name = 'CmsPages';
	var $uses = array('Page','Contact','ContactDetail');
	public function beforeFilter() {
			$this->Auth->allow('about','faq','how_it_works','amigros_points','delivery_details','career','return_and_cancellation','terms_and_conditions','privacy_policy','social_media_disclaimer','contactus','karim_testmail','test_mail');
			$this->set('title_for_layout', 'Amigros | Cms');
				parent::beforeFilter();
		}
	function about() {
		$this->set('title_for_layout', 'About Develop A Player');
		$page_data=$this->Page->find('first', array('conditions' => array('Page.slug_url' =>'about','Page.status' =>1)));
	    $this->set(compact('page_data'));		//pr($page_data);
	}	
	function terms_and_conditions() {
		$this->set('title_for_layout', 'Terms & Conditions Develop A Player');
		$page_data=$this->Page->find('first', array('conditions' => array('Page.slug_url' =>'terms-conditions','Page.status' =>1)));
	    $this->set(compact('page_data'));		//pr($page_data);
	}		
	function contactus() {
			//Configure::write('debug', 2);
			$this->set('title_for_layout', 'Contact Us Develop A Player');
			if($this->request->data)
			{
				$contactus['Contact']['first_name']=$this->request->data['first_name'];
				$contactus['Contact']['last_name']=$this->request->data['last_name'];
				$contactus['Contact']['email']=$this->request->data['email'];
				$contactus['Contact']['phone']=$this->request->data['phone'];
				$contactus['Contact']['message']=$this->request->data['message'];
				$this->Contact->save($contactus);
				
				//$Email = new CakeEmail();
				/*App::uses('CakeEmail', 'Network/Email');
				$this->Email = new CakeEmail();
				//===================To User=========================//
				$this->Email->to =$contactus['Contact']['email'];
				$this->Email->subject = 'Contact Us';
				$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email1').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('contactus', $contactus);
				$this->Email->template = 'contactus';														
				$this->Email->send();				
				//=====================to Admin===========================//				
				//$this->Email->to ='Developaplayer' .'<'.Configure::read('admin_email1').'>';
				$this->Email->to ='test.player@developaplayer.com';
				$this->Email->subject = 'Contact Us';
				$this->Email->from = $contactus['Contact']['email'];
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';			
				$this->Email->template = 'contact_us_admin';																
				$this->Email->send();
				*/
				//================= Smtp Mail function ==================================
				App::uses('CakeEmail', 'Network/Email');
				$this->Email = new CakeEmail('smtp');
				//===================To User=========================//
				$this->Email->to(array($contactus['Contact']['email']));
				$this->Email->subject('Develop A Player Contact Us');
				//$this->Email->from(array('Develop A Player' .'<'.Configure::read('admin_email1').'>'));
				$this->Email->from(array(Configure::read('admin_email1')=>'Develop A Player'));
				//$this->Email->layout = '';
				//$this->Email->sendAs = 'html';
				$this->Email->emailFormat('html');
				//$this->set('contactus', $contactus);
				$this->Email->viewVars(array('contactus'=>$contactus));
				$this->Email->template('contactus');														
				$this->Email->send();				
				//=====================to Admin===========================//				
				//$this->Email->to ='Developaplayer' .'<'.Configure::read('admin_email1').'>';
				$this->Email->to(array(Configure::read('admin_email1')=>'Develop A Player'));
				//$this->Email->to(array('test.player@developaplayer.com'));
				$this->Email->subject('Develop A Player Contact Us');
				$this->Email->from(array($contactus['Contact']['email']));
				//$this->Email->layout = '';
				//$this->Email->sendAs = 'html';
				$this->Email->emailFormat('html');				
				$this->Email->template('contact_us_admin');													
				$this->Email->send();
				
				$this->Session->setFlash('Thank You For Contacting Us..');
				$this->redirect(array('controller' =>'cms_pages','action' => 'contactus'));
			}
		$ContactDetail=$this->ContactDetail->find('first');
		$this->set('ContactDetail',$ContactDetail);
	}
	
	function karim_testmail()
	{		
		Configure::write('debug', 2);
		App::uses('CakeEmail', 'Network/Email');
		$this->Email = new CakeEmail('default');
		//pr($Email);
		//exit();
		//$this->Email = new CakeEmail();
		$this->Email->from(array('karim@techpourri.com'));
		$this->Email->emailFormat('html');
		$this->Email->template("contact_us_admin");
		//$Email->helpers('Html');
		$this->Email->to(array('tuinvisie@yardesign.nl'));
		$this->Email->subject('Contact Us');
		$this->Email->send();
		exit();
	}
	function test_mail()
	{
		Configure::write('debug', 2);
		App::uses('CakeEmail', 'Network/Email');
		$Email = new CakeEmail('smtp');
		//pr($Email);
		//exit();
		//$this->Email = new CakeEmail();
		$Email = 'karim@techpourri.com';
		$Email->emailFormat='html';
		$Email->template="contact_us_admin";
		//$Email->helpers('Html');
		$Email->to='karim@techpourri.com';
		$Email->subject='Contact Us';
		$Email->send();
		exit();
		
	}
	
	
	
}