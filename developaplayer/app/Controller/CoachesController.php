<?php
App::uses('CommonController', 'Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class CoachesController  extends CommonController {
	var $name = 'Coaches';
	var $uses = array('Country','CoachSpeciality','User','CoachDetail','Certificate','CoachCertificate','Video','AgeGroup','AddCoachRating','LavelAssignment','CoachLevel');
	 public function beforeFilter() {
        $this->Auth->allow(array('register','complt_reg','login','coach_list','coach_profile','coach_list_ajax','forgot_password','create_new_password','auto_complate_name','coach_list_auto_complate','get_graph_value_by_age_group','product_details','email_exit_or_not'));
		$this->set('title_for_layout', 'Develop a Player');		
		  parent::beforeFilter(); 
		   AuthComponent::$sessionKey = 'Auth.Member';
			$this->Auth->authenticate = array(
				'Form' => array(
				'userModel'=> 'User',
				'fields' => array('username' => 'email_address','password' => 'password'), 
				'scope' => array('User.status' => 'A','User.type'=>'C')
				)
			);
		$this->Auth->logoutRedirect = array('controller' => 'coaches', 'action' => 'login');
		$this->Auth->loginRedirect = array('controller' => 'coaches','action'=>'coach_profile'); 
    }	
	public function index() {
		$this->layout = "ajax";
		$this->render(false);
		$this->redirect(array('action'=>'register'));
	}	
	public function register(){
		
		 if($this->Session->check('Auth.Member'))
		{			
			$this->redirect($this->Auth->redirectUrl());
        }	
		
		$country = $this->Country->find('list',array('fields'=>array('Country.id','Country.country_name')));
		$speciality = $this->CoachSpeciality->find('list',array('fields'=>array('CoachSpeciality.id','CoachSpeciality.name')));
		$certificate = $this->Certificate->find('list',array('fields'=>array('Certificate.id','Certificate.name')));
		$this->set('country',$country);
		$this->set('speciality',$speciality);
		$this->set('certificate',$certificate);
		if($this->request->is('post')){		
			$check_email = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['email_address'])));
			if(!empty($check_email)){				
			}else{
				$coach_details['User']['first_name'] = $this->request->data['first_name'];
				$coach_details['User']['last_name'] = $this->request->data['last_name'];
				$coach_details['User']['email_address'] = $this->request->data['email_address'];
				$coach_details['User']['password'] = $this->Auth->password($this->request->data['password']);
				$coach_details['User']['type'] = 'C';
				$coach_details['CoachDetail']['height'] = $this->request->data['height'];
				$coach_details['CoachDetail']['weight'] = $this->request->data['weight'];
				$coach_details['CoachDetail']['dob'] = $this->request->data['dob'];
				$coach_details['CoachDetail']['primary_speciality_id'] = $this->request->data['primary_speciality_id'];
				$coach_details['CoachDetail']['secondary_speciality_id'] = $this->request->data['secondary_speciality_id'];
				$coach_details['CoachDetail']['nationality'] = $this->request->data['nationality'];
				$coach_details['User']['status'] = 'I';
				if(isset($this->request->data['profile_img'])){
					$coach_details['CoachDetail']['profile_img'] = $this->request->data['profile_pic'];
				}
				if($this->User->save($coach_details)){
					$coach_details['CoachDetail']['user_id'] = $this->User->getLastInsertId();
					if($_FILES){
						$foo = new Upload($_FILES['profile_img']); 
						  $foo->allowed = array('image/jpg/jpeg');
							if ($foo->uploaded)
							{
								$foo->allowed = array('image/*');
								$foo->Process('uploads/profiles/coach/original/');
								$foo->image_resize          = true;				   
								if($foo->image_dst_x>$foo->image_dst_y)
								{   
									$foo->image_ratio_x        = true;
									$foo->image_y              = 357;   
								}else if($foo->image_dst_x<$foo->image_dst_y)
								{
								   $foo->image_ratio_y               = true;
								   $foo->image_x                     = 439;							   
								}else if($foo->image_dst_x==$foo->image_dst_y)
								{
								   $foo->image_ratio_y         = true;
								   $foo->image_x               = 357;
								}											   
								$foo->allowed = array('image/*');
								$foo->Process('uploads/profiles/coach/profile_pic/');
								$foo->image_resize          = true;
								$foo->image_y               = 160;
								$foo->image_x               = 130;
								$foo->allowed = array('image/*');
								$foo->Process('uploads/profiles/coach/thumb/');
								if ($foo->processed) {
									$coach_details['CoachDetail']['profile_img'] = $foo->file_dst_name;
								}
							}
					}
					$random_otp= $this->n_digit_random(4);
					$coach_details['CoachDetail']['otp'] =$random_otp;
					if($this->CoachDetail->save($coach_details))
					{
						/*$this->Email->to =$this->request->data['email_address']; 
						$this->Email->subject = "Otp confirmation alert for your valid Registration";
						$this->Email->from = Configure::read('admin_email');
						$this->Email->layout = '';
						$this->Email->template = 'confirm_otp';
						$this->Email->sendAs = 'html';
						$this->set('otp',$random_otp);
						$this->Email->send();*/
						
						//===================OTP=========================//
						
						App::uses('CakeEmail', 'Network/Email');
						$this->Email = new CakeEmail('smtp');
						
						$this->Email->to(array($this->request->data['email_address']));
						$this->Email->subject('Otp confirmation alert for your valid Registration');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');						
						$this->Email->viewVars(array('otp'=>$random_otp));
						$this->Email->template('confirm_otp');														
						$this->Email->send();
						
						
						$last_user_id =base64_encode($coach_details['CoachDetail']['user_id']);
						$this->set('message','<span class="alert alert-success">Account Successfully Created. Check Your Mail To Complete Registration <br/>.</span>');
						$this->redirect(array('action'=>'complt_reg','myArgument' => $last_user_id));
					}else{
						$this->set('message','<span class="text-danger">Unable To Creating Your Account.</span>');
					}
				}
			}
		}
	}
	public function complt_reg()
	{		
		 $user_id= base64_decode($this->request->params['named']['myArgument']);
		 $get_otp=$this->CoachDetail->find('first',array('conditions'=>array('CoachDetail.user_id'=>$user_id),'fields'=>'CoachDetail.otp'));
		 if($this->request->is('post'))
		 {
			$user_input_otp= $this->request->data['otp_name'];			
			if($get_otp['CoachDetail']['otp']==$user_input_otp)
			{
				$update_satus['User']['status']= 'A';
				$this->User->id=$user_id;
				$this->User->save($update_satus);
				$this->Session->setFlash(__('Registration is successful. Please login now'));				
				 return $this->redirect( array('action' => 'login'));				 
			}else{
			{
				 $this->Session->setFlash(__('invalid OTP please try again.'));
			}	
		 }
		 }
	}
	private function n_digit_random($digits)
	{
           return rand(pow(10, $digits - 1) - 1, pow(10, $digits) - 1);
    }
	//===================================Login======================================
	public function login()
	{		
	    if($this->Session->check('Auth.Member'))
		{			
			$this->redirect($this->Auth->redirectUrl());
        }
		 if($this->Session->check('Auth.Member') && $this->Auth->user('type')=='C' )
		{		        			
			 $this->redirect(array('controller'=>'coaches','action'=>'coach_profile')); 			
        }
		elseif($this->Session->check('Auth.Member') && $this->Auth->user('type')=='F' )
		{
			$this->redirect(array('controller'=>'families','action'=>'family_profile'));
		}elseif($this->Session->check('Auth.Member') && $this->Auth->user('type')=='P' )
		{			
			$this->redirect(array('controller'=>'players','action'=>'players_profile'));
		}
		if($this->request->is('post'))
		{
			if ($this->Auth->login())
			{
				//pr($this->Auth->Member());
				//exit();
				$this->Session->setFlash(__('Welcome you are login as a Coach'. $this->Auth->user('name'),'default',array('class'=>'alert alert-success alert-dismissable')));
			    return $this->redirect($this->Auth->redirectUrl());
            } else {				
				$this->Session->setFlash("Invalid username or password",'default',array('class'=>'alert alert-danger alert-dismissable'));
            }			
		}
		   $this->set('title_for_layout', ' Login');
	}	
	public function logout() {
		return $this->redirect($this->Auth->logout());
	}
	//==================================== Coach Profile ===================================================================
	public function coach_profile($id=null)
	{  
		  if($this->Auth->user('id')=="")
			{
			   $this->redirect(array('controller'=>'homes','action'=>'index/1'));	
			}	
		$id=base64_decode($id);
		$auth_user_id = $this->Auth->user('id');
		if($id=='')
		{
		  $user_id = $auth_user_id;
		}
		else
		{
			$user_id= $id;		
		}
		$this->set('user_id', $user_id); 	
        if(!$user_id)
		{
			return $this->redirect(array('action'=>'login'));			
		}
		//===================Coach Level Set=====================================//
		$all_lavel_by_coach_Tight_5=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.level_cat_id'=>1,'LavelAssignment.coach_id'=>$user_id)));
		$all_lavel_by_coach_Loose_Forwards=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.level_cat_id'=>2,'LavelAssignment.coach_id'=>$user_id)));
		$all_lavel_by_coach_Half_Backs=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.level_cat_id'=>3,'LavelAssignment.coach_id'=>$user_id)));
		$all_lavel_by_coach_Inside_Backs=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.level_cat_id'=>4,'LavelAssignment.coach_id'=>$user_id)));
		$all_lavel_by_coach_Outside_Backs=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.level_cat_id'=>5,'LavelAssignment.coach_id'=>$user_id)));
		$this->set('all_lavel_by_coach_Tight_5', $all_lavel_by_coach_Tight_5); 
		$this->set('all_lavel_by_coach_Loose_Forwards', $all_lavel_by_coach_Loose_Forwards); 
		$this->set('all_lavel_by_coach_Half_Backs', $all_lavel_by_coach_Half_Backs); 
		$this->set('all_lavel_by_coach_Inside_Backs', $all_lavel_by_coach_Inside_Backs); 
		$this->set('all_lavel_by_coach_Outside_Backs', $all_lavel_by_coach_Outside_Backs); 
		//pr($all_lavel_by_coach_Tight_5);
		//exit;
	//===============================END===================================================//	
        $AgeGroup_list=$this->AgeGroup->find('list');			
		$conditions = array(array("User.id"=>$user_id));		
		$all_coach_data= $this->User->find('first',array('conditions' =>$conditions));
        $this->set('all_coach_data', $all_coach_data); 
		$this->set('AgeGroup_list', $AgeGroup_list); 
        //===============certification==================================================
        $conditions_c= array(array("CoachCertificate.user_id"=>$user_id));	
		$all_coachcertificate_data= $this->CoachCertificate->find('all',array('conditions' =>$conditions_c));
		$this->set('all_coachcertificate_data', $all_coachcertificate_data);
        //==================Video=========================================================   
        $conditions_v= array(array("Video.user_id"=>$user_id));	
		$all_video_data= $this->Video->find('all',array('conditions' =>$conditions_v));
		$new_video_array_list=array();
		foreach((array) $all_video_data as  $all_video_datas)
		{			
			$new_video_array_list[$all_video_datas['Video']['video_position']]=$all_video_datas['Video'];			
		}
		$this->set('all_video_data', $new_video_array_list); 
		
        /*$most_view = $all_coach_data['CoachDetail']['most_view'];
		$visit_user_id = $all_coach_data['CoachDetail']['visit_user_id'];
		
		if($visit_user_id !=$this->Auth->user('id'))
		{		
		      $incriment_view= $most_view+1;
			 
              $this->CoachDetail->updateAll(array('CoachDetail.visit_user_id'=>$this->Auth->user('id')), array('CoachDetail.user_id'=>$id));			 
			  $this->CoachDetail->updateAll(array('CoachDetail.most_view'=>$incriment_view), array('CoachDetail.user_id'=>$id)); 
             

		}*/
		
		
	}
	//===========================  Add Youtube Video    ===============================================
	public function add_coach_video()
	{
		$video_url= $this->request->data['URL'];
		$user_id= $this->request->data['USERID'];
		$video_position= $this->request->data['position'];
		$id=$this->request->data['last_id'];
		// $video_url = $video_link;
		$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
		$youtube_data = json_decode(file_get_contents($url), true);		                                              
		$coach_details['Video']['user_id'] = $user_id;
		$coach_details['Video']['video_link'] = $video_url;
		$coach_details['Video']['video_iframe'] = $youtube_data['html'];
		$coach_details['Video']['video_title'] = $youtube_data['title'];
		$coach_details['Video']['video_image'] = $youtube_data['thumbnail_url'];
		$coach_details['Video']['video_position'] = $video_position;
		if( $id>0)
		{
			$last_id= $id;  
			$this->Video->id=$last_id;
			$this->Video->save($coach_details);
		}else{
			$this->Video->save($coach_details);
			$last_id=$this->Video->getLastInsertId();
		}
		$conditions_c= array(array("Video.id"=>$last_id));			   
		$all_video= $this->Video->find('first',array('conditions' =>$conditions_c));
		echo json_encode($all_video['Video']);
		exit;	   
	}
	public function coach_list()
	{
		//pr($this->request->data);
		//exit;
	   $sql="";	
      if(isset($this->request->data['select_value_speciality']))
		{
			  $select_value_speciality=$this->request->data['select_value_speciality'];			
			  $sql.="CoachDetail.primary_speciality_id='".$select_value_speciality."'";			
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
			  $all_coach_details= $this->User->find('all',array('conditions' =>$sql,'limit'=>25, 'offset'=>0));	  
		 }else if(isset($this->request->data['search_box']))
		    {
			  $search_text=$this->request->data['search_box'];
			  //$sql.="'".REPLACE(CONCAT('User.fname', 'User.lname',' User.fname'), ' ', '')."'" LIKE '%" . 'str_replace(' ', '', protect($search_text)) . "'"%'; 
			 $sql.="( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
             $sql.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) "; 			 
		     //$sql.=" OR User.last_names LIKE '". $search_text."%' )";
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
			  $all_coach_details= $this->User->find('all',array('conditions' =>$sql,'limit'=>25, 'offset'=>0));	  
		}else
		{
			$conditions_c= array(array("User.type"=>'C','User.status'=>'A'));	
			$all_coach_details= $this->User->find('all',array('conditions' =>$conditions_c,'limit'=>25, 'offset'=>0));
		}
		//pr( $all_coach_details);
		//exit;

                $coach_total = count($all_coach_details);
                $this->set('coach_total',$coach_total);

		$this->set('all_coach_details', $all_coach_details);				
		$all_coachspeciality= $this->CoachSpeciality->find('all');		
		$this->set('all_coachspeciality', $all_coachspeciality);

		if(isset($this->request->data['search_box']))
		{
		   $this->set('search_text',  $this->request->data['search_box']);
		}else
		{
			$this->set('search_text',"");
		}
	   if(isset($this->request->data['select_value_speciality']))
		{
		   $this->set('select_value_speciality',  $this->request->data['select_value_speciality']);
		}else
		{
			$this->set('select_value_speciality',"");
		}
	}
	public function coach_list_ajax()
	{
		$this->layout="ajax";
	        $sql="";
	        $last_id=$this->request->data['last_id'];
                $limit=$this->request->data['limit'];

		if(isset($this->request->data['position_id'])&& $this->request->data['position_id']!="")
		{
			  $position_id=$this->request->data['position_id'];			
			  $sql.="PlayerDetail.primary_position_id='".$position_id."'";			
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
			 // $all_coach_details= $this->User->find('all',array('conditions' =>$sql,'limit'=>$limit,'offset' =>$last_id));	
		}else if(isset($this->request->data['search_box'])&& $this->request->data['search_box']!="")
		{
			  $search_text=$this->request->data['search_box'];
			  //$sql.="'".REPLACE(CONCAT('User.fname', 'User.lname',' User.fname'), ' ', '')."'" LIKE '%" . 'str_replace(' ', '', protect($search_text)) . "'"%'; 
			 $sql.="( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
             $sql.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) "; 			 
		     //$sql.=" OR User.last_names LIKE '". $search_text."%' )";
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
			  $all_coach_details= $this->User->find('all',array('conditions' =>$sql,'limit'=>$limit,'offset' =>$last_id));	  
		}else
		{
			$conditions_c= array(array("User.type"=>'C','User.status'=>'A'));	
			$all_coach_details= $this->User->find('all',array('conditions' =>$conditions_c,'limit'=>$limit,'offset' =>$last_id));
                       
		}
			//pr($all_coach_details);
			$this->set('all_coach_details', $all_coach_details);				
			/*$all_coachspeciality= $this->CoachSpeciality->find('all');		
			$this->set('all_coachspeciality', $all_coachspeciality);
			*/
	}
	public function coach_list_auto_complate()
	{
		$this->layout="ajax";
	    $sql="";
		if(isset($this->request->data['search_box'])&& $this->request->data['search_box']!="")
		{
			 $search_text=$this->request->data['search_box'];
			  //$sql.="'".REPLACE(CONCAT('User.fname', 'User.lname',' User.fname'), ' ', '')."'" LIKE '%" . 'str_replace(' ', '', protect($search_text)) . "'"%'; 
			 $sql.="( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
             $sql.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			 $sql.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) "; 			 
		     //$sql.=" OR User.last_names LIKE '". $search_text."%' )";
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
			  $all_coach_details= $this->User->find('all',array('conditions' =>$sql));	  
		}
			$this->set('all_coach_details', $all_coach_details);				
			$all_coachspeciality= $this->CoachSpeciality->find('all');		
			$this->set('all_coachspeciality', $all_coachspeciality);
	}
public function auto_complate_name()
{
	$this->layout = "ajax";
    $search_text= $this->request->data['keyword'];
    $sql = "";    
	   if(!empty($this->request->data))
	   {
		      $sql.="( User.first_name LIKE '". $search_text."%'";    
		      $sql.=" OR User.last_name LIKE '". $search_text."%' )";
		      $sql.=" AND User.type ='C'";
			  $sql.=" AND User.status ='A'";
		  $all_search_data= $this->User->find('all',array('conditions'=>$sql,'recursive'=>-1));
		  $full_html="";
		  if(!empty( $all_search_data)) {
			$full_html.='<ul id="country-list">'; 
			   foreach($all_search_data as $all_search_datas) 
			   {
				   $name=$all_search_datas['User']['first_name'].' '.$all_search_datas['User']['last_name'];
$full_html.="<li onClick=\"selectCountry('".$name."')\"> ".$all_search_datas['User']['first_name'].' '.$all_search_datas['User']['last_name']."</li>";           
			  }
		  $full_html.='</ul>';
		 echo $full_html;
	 }	
	}	
	 exit;
  }	
  public function forgot_password()
  {	
 // Configure::write('debug', 2);
     if(!empty( $this->request->data))
	 {
		  $email= $this->request->data['User']['email_address'];
		  $conditions_c= array(array("User.type"=>'C',"User.email_address"=> $email));	
		  $all_coach_details= $this->User->find('first',array('conditions' =>$conditions_c,'recursive'=>-1));
		  if(count($all_coach_details)>0)
		  {
			        /*$this->Email->to =  $email; 
					$this->Email->subject = "Forgot Password";
					$this->Email->from = Configure::read('admin_email');
					$this->Email->layout = '';
					$this->Email->template = 'create_new_password';
					$this->Email->sendAs = 'html';		
					$this->set('all_coach_details', $all_coach_details);
				    $this->Email->send();*/
					
					    App::uses('CakeEmail', 'Network/Email');
						$this->Email = new CakeEmail('smtp');
						
						$this->Email->to(array($email));
						$this->Email->subject('Forgot Password');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');						
						$this->Email->viewVars(array('all_coach_details'=>$all_coach_details));
						$this->Email->template('create_new_password');														
						$this->Email->send();
					
					
			       // $this->Session->setFlash('Verification link sent to'.$email,array('class'=>'alert alert-success alert-dismissable'));
					 $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable">Verification link sent to '.$email.'</div>',array('class'=>'alert alert-success alert-dismissable')));
		  }else {
	       //$this->Session->setFlash('You are not registered with us. Please sign up to create an account.',array('class'=>'alert alert-danger alert-dismissable'));
            $this->Session->setFlash(__('<div class="alert alert-info alert-dismissable">You are not registered with us. Please sign up to create an account.</div>',array('class'=>'alert alert-success alert-dismissable')));			 	
	     } 
	 }
  }
  public function create_new_password($user_id=0)
  {
	 // Configure::write('debug', 2);
	  $id= base64_decode($user_id);
	  $this->set('param_id', $id); 
      if(!empty($this->request->data))
	  {
		  $password=$this->request->data['User']['password'];
		  $cpassword=$this->request->data['User']['cpassword'];
		  if($password==$cpassword)
		  {
			 $password=$this->Auth->password($this->request->data['User']['password']);
			 $this->request->data['User']['password']=$password;
			 $this->User->id= $this->request->data['User']['forget_user_id'];
			 $this->User->saveField('password', $password);
				 $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable">Your password updated successfully</div>','default',array('class'=>'alert alert-success alert-dismissable')));
				 $this->redirect(array('controller'=>'coaches','action'=>'login'));    			
		  }else
		  {		  
              $this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable">password and confirmPassword is not match please write correctly.</div>','default',array('class'=>'alert alert-success alert-dismissable')));
               $this->redirect(array('controller'=>'coaches','action'=>'create_new_password/'.base64_encode($this->request->data['User']['forget_user_id']))); 			  
	     }	  
	  }
  }
  public function edit_profile()
  {
	    $country = $this->Country->find('list',array('fields'=>array('Country.id','Country.country_name')));
		$speciality = $this->CoachSpeciality->find('list',array('fields'=>array('CoachSpeciality.id','CoachSpeciality.name')));
		$certificate = $this->Certificate->find('list',array('fields'=>array('Certificate.id','Certificate.name')));		
		$this->set('country',$country);
		$this->set('speciality',$speciality);
		$this->set('certificate',$certificate);
	 //===============================================================================//		
		   $id=$this->Auth->user('id');
		   $get_data= $this->User->find('first',array('conditions'=>array('User.id'=>$id)));
		   $this->set('get_edit_data',$get_data);
	  //===========================Coach Certificates=====================================//
		   $get_data_Certificates= $this->CoachCertificate->find('all',array('conditions'=>array('CoachCertificate.user_id'=>$id),'fields'=>'CoachCertificate.certificate_id'));
			//pr($get_data_Certificates);
			//exit;
			$new_arry_certi="";
			if(!empty($get_data_Certificates))
			{    $i=1;
				foreach($get_data_Certificates as $get_data_Certificates_data)
				{
					if(count($get_data_Certificates)==$i)
					{
						$comma="";
					}else
					{
						$comma=",";
					}
					  $new_arry_certi.=$get_data_Certificates_data['CoachCertificate']['certificate_id'].$comma;
					$i++;
				   }
				}else{			     
				 $this->set('new_arry_certi',"");				
			    }
               $this->set('new_arry_certi',$new_arry_certi);		  
		  // $this->set('get_data_Certificates',$get_data_Certificates);
		//====================when submit for coach profile=====================================//
		if($this->request->is('post'))
		{			//pr($this->request->data);die;
			    $check_email = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['email_address'])));			
				$coach_details['User']['first_name'] = $this->request->data['first_name'];
				$coach_details['User']['last_name'] = $this->request->data['last_name'];
				$coach_details['User']['email_address'] = $this->request->data['email_address'];				
				$coach_details['User']['type'] = 'C';
				$coach_details['CoachDetail']['height'] = $this->request->data['height'];
				$coach_details['CoachDetail']['weight'] = $this->request->data['weight'];
				$coach_details['CoachDetail']['dob'] = $this->request->data['dob'];
				$coach_details['CoachDetail']['primary_speciality_id'] = $this->request->data['primary_speciality_id'];
				$coach_details['CoachDetail']['secondary_speciality_id'] = $this->request->data['secondary_speciality_id'];
				$coach_details['CoachDetail']['nationality'] = $this->request->data['nationality'];
				$coach_details['CoachDetail']['note'] = $this->request->data['note'];
				$this->User->id=$this->Auth->user('id');
				if($this->User->save($coach_details)){
						if(isset($_FILES['profile_img']['name']) && $_FILES['profile_img']['name']!=""){
							$foo = new Upload($_FILES['profile_img']); 
							  $foo->allowed = array('image/jpg/jpeg');
								if ($foo->uploaded) {
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/coach/original/');
									   $foo->image_resize          = true;
								if($foo->image_dst_x>$foo->image_dst_y)
								   {   
									   $foo->image_x               = 357;
										$foo->image_ratio_y              = true;
								   }else if($foo->image_dst_x<$foo->image_dst_y)
								   {
									   $foo->image_y                = 439;
									   $foo->image_ratio_x          = true;
								   }else if($foo->image_dst_x==$foo->image_dst_y)
								   {
									   $foo->image_ratio_y         = true;
									   $foo->image_x               = 357;
								   }
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/coach/profile_pic/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 160;
									   $foo->image_x               = 130;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/coach/thumb/');
									  if ($foo->processed) {
											$coach_details['CoachDetail']['profile_img'] = $foo->file_dst_name;
									  }
								   }
							   }
				    $get_coach_id= $this->CoachDetail->find('first',array('conditions'=>array('CoachDetail.user_id'=>$this->Auth->user('id')),'fields'=>'CoachDetail.id'));				    
					$coach_id= $get_coach_id['CoachDetail']['id'];					
					$this->CoachDetail->id=$coach_id;					
					if($this->CoachDetail->save($coach_details)){
						$certificate_arr=$this->request->data['certificate_id'];
						if(!empty($certificate_arr))
						{
							$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$this->Auth->user('id')));
							foreach($certificate_arr as $certificate_arrs)
							{
								 $certificate_data['CoachCertificate']['user_id']=$this->Auth->user('id');
								 $certificate_data['CoachCertificate']['certificate_id']=$certificate_arrs;
								 $certificate_data['CoachCertificate']['modified']=date('Y-m-d H:i:s');
								 $this->CoachCertificate->save($certificate_data);
								 $this->CoachCertificate->create();
							}						
						}											
						$this->Session->setFlash(__(('<span class="alert alert-success ">Your Profile has been successfully updated.</span>')));
						$this->redirect(array('controller'=>'coaches','action'=>'coach_profile'));
					}else{
						$this->set('message','<span class="alert alert-danger">Unable To Creating Your Account.</span>');
					}
				}
		} 
  }
  public function get_graph_value_by_age_group()
	{
		$this->layout="";
		$age_group_id =$this->request->data['select_value'];
		$coach_id =$this->request->data['coach_id'];
		$all_rating_coach_attack= $this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.age_group_id'=>$age_group_id,'AddCoachRating.coach_id'=>$coach_id,'LevelCategory.id'=>1)));
        $all_rating_coach_deffence= $this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.age_group_id'=>$age_group_id,'AddCoachRating.coach_id'=>$coach_id,'LevelCategory.id'=>2)));
		$all_rating_coach_Condition= $this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.age_group_id'=>$age_group_id,'AddCoachRating.coach_id'=>$coach_id,'LevelCategory.id'=>4)));
        $all_rating_coach_mindset= $this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.age_group_id'=>$age_group_id,'AddCoachRating.coach_id'=>$coach_id,'LevelCategory.id'=>3)));		
		$this->set('all_rating_coach_attack',$all_rating_coach_attack);
		$this->set('all_rating_coach_deffence',$all_rating_coach_deffence);
		$this->set('all_rating_coach_Condition',$all_rating_coach_Condition);
		$this->set('all_rating_coach_mindset',$all_rating_coach_mindset);
	}
	public function product_details()
	{
	}		
	public function email_exit_or_not()
	{
		   $email= $this->request->data['email_address'];
		   $check_email = $this->User->find('count',array('conditions'=>array('User.email_address'=> $email)));
			if($check_email==0)
			{
				echo '1';
			}else
            {
				echo '0';
			}	
		exit;	
	}
	public function upload_video()
	{
          if(isset($_FILES['own_video']['name']) && $_FILES['own_video']['name']!="")
						{							
							$member_id_video=$this->request->data['member_id_video'];
							$coustom_video=$this->request->data['coustom_video'];														
							$foo = new Upload($_FILES['own_video']); 							
								if ($foo->uploaded) {									
								$foo->Process('uploads/video/coach/');																
							if ($foo->processed)
								{                              
								   $coach_details['Video']['user_id'] = $member_id_video;
								   $coach_details['Video']['video_link'] = $foo->file_dst_name;
								   $coach_details['Video']['video_iframe'] = $this->request->data['video_title'];
								   $coach_details['Video']['video_title'] = $this->request->data['video_title'];
								   $coach_details['Video']['video_image'] = $foo->file_dst_name;
								   $coach_details['Video']['video_position'] = $coustom_video;
								   $coach_details['Video']['video_type'] = 1;
								  $count_exit_video= $this->Video->find('first',array('conditions'=>array('Video.video_position'=>$coustom_video,'Video.user_id'=>$member_id_video)));
								   if(count($count_exit_video)>0)
								   {
										  $last_id=  $count_exit_video['Video']['id'];  
										  $this->Video->id=$last_id;
										  $this->Video->save($coach_details);
									}else
									{										
										 $this->Video->save($coach_details);
										 $last_id=$this->Video->getLastInsertId();
									}
									  }
								    }
							     }
				$this->redirect(array('controller'=>'coaches','action'=>'coach_profile'));	   
	}
}
