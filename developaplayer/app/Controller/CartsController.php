<?php
App::uses('CommonController', 'Controller');
class CartsController extends CommonController {
	var $name = "Carts";
	var $helpers = array('Html', 'Form', 'Paginator','Js');
	public $uses = array('User','Cart','Product','Attribute','Country','State','Order','OrderProduct','ProductSizeQty','Size','PlayerDetail');
	var $components = array('Session','Acl', 'Paginator','Email');
	
   function beforeFilter() 
   {
   		parent::beforeFilter();
		
   		
   }
	public function add_to_cart()
	{
		//pr($this->request->data);
		//exit;
		$this->autoRender = false;
		//CakeSession::write('cart',array());
		//exit;
        if ($this->request->is('post')) {
            $this->Cart->addProduct($this->request->data['product_id'],$this->request->data['product_size_code'],$this->request->data);
        }     
       $this->redirect(array('action'=>'view'));
	}
	public function view() {
		$carts = $this->Cart->readProduct();	
		$products = array();		
		$this->loadModel('Product');
		CakeSession::write('cart-view',array());
		//exit;
		$Subtotal=00;
		$total_size = 0;
		if( count($carts) > 0 ) {
			//$products = array( 0 => array('product_id'=>'','product_name'=>'','image'=>'','product_unit_price'=>'','total'=>'','qty'=>'', 'Size'=>'') );
			$products = array();
			$subtotal_arr= array();
			$i=0;
			$total = 0;
			//echo count($carts);
			//exit;
			foreach ( (array) $carts as $productIdKey => $productVal ) 
			{   if(isset($productVal['product_id']) && $productVal['product_id']!="")
				{
					$product_id = $productVal['product_id'];
					$products[$i]['product_id'] = $product_id;
					$productDetails = $this->Product->find('first', array('conditions' => array('Product.id'=>$product_id)));
					$total = $productDetails['Product']['price']*$productVal['product_qty'];
					$products[$i]['product_name'] = $productDetails['Product']['name'];				
					$products[$i]['image'] = $productDetails['ProductImage']['image'];
					$products[$i]['product_unit_price'] = $productDetails['Product']['price'];
					$products[$i]['product_stock'] = $productDetails['Product']['total_qty']-$productDetails['Product']['total_sale'];
					$products[$i]['total'] = $total;
					$products[$i]['size'] = $productVal['product_size'];				
					$products[$i]['qty'] = $productVal['product_qty'];
					$products[$i]['product_size_code'] = $productVal['product_size_code'];
					//$Subtotal=$total;
					$Subtotal = $Subtotal+$total;
					$total_size = $total_size+$productVal['product_qty'];
					$i++;
				}
			}
		}
            $subtotal_arr['subtotal']= $Subtotal;
			CakeSession::write('Subtotal',$subtotal_arr);	
			CakeSession::write('cart-view',$products);
			CakeSession::write('total_size',$total_size);
	//==============Sub Total Price============================	//
   $price_explode_sub_total = explode('.',$subtotal_arr['subtotal']);
	if(isset($price_explode_sub_total[0]))
	{
		$fistpartprice_sub_total= $price_explode_sub_total[0];	
	}else								
	{
		$fistpartprice_sub_total=00;	
	}							
	if(isset($price_explode_sub_total[1]))
	{
		$secpartprice_sub_total= $price_explode_sub_total[1];								
	}else								
	{
		$secpartprice_sub_total='00';										
	}
	    CakeSession::write('fprice',$fistpartprice_sub_total);
		CakeSession::write('sprice',$secpartprice_sub_total);
	//==========================================================//
		$this->set(compact('products'));
	}
	function ajx_check_stock() {
		$this->layout='ajax';
		$this->autoRender =false;
		$this->loadModel('ProductSizeQty');		
		$pId = $_POST['productId'];
		$Product_size_code = $_POST['Product_size_code']; 		
		$checkStock = $this->ProductSizeQty->find('first',array('conditions' => array("product_id"=>$pId,"size_id >="=>$Product_size_code),'recursive' => -1 ));
		$total_remaing_quantiy=$checkStock ['ProductSizeQty']['qty']-$checkStock ['ProductSizeQty']['sale_qty'];
		echo $total_remaing_quantiy;
	}
	function ajx_remove_item() 
	{
		$this->layout='ajax';
		$this->autoRender =false;
		$pId = $_POST['Product_size_code'];
		$re = 0;
		if ( $pId != '' ) {
			$carts = $this->Cart->readProduct();
			if( count($carts) > 0 ) {
				unset($carts[$pId]);	
				  CakeSession::write('cart',array());
				  CakeSession::write('cart',$carts);
				//$re = 1;
			}
		}
		$this->view();
		$this->render('view');
	}
	function ajx_edit_item()
	{
		$this->layout='ajax';	
		$pId = $_POST['Product_size_code'];
		$edit_qty = $_POST['edit_qty'];
		$re = 0;
		if ( $pId != '' )
			{ 
			$carts = $this->Cart->readProduct();
			if( count($carts) > 0 ) {
				$carts[$pId]['product_qty']= $edit_qty;
				if($edit_qty==0)
				{
					$carts[$pId]['product_qty']= 1;
				}
                 CakeSession::write('cart',array());				
				 CakeSession::write('cart',$carts);	
			}
		}
			 $this->view();
			 $this->render('view');
	}
	function check_out_step1() {
		if ($this->request->is('post')) {
			if( count($this->request->data['Cart']) > 0 ) {
				$data = $this->request->data['Cart'];
				$carts = $this->Cart->readProduct();
				$cartView = $this->Cart->readProductCheckoutSummery();
				if( count($carts) > 0 ) {
					foreach($data as $key=>$val) {
						$carts[$val['indx']]['qty'] = $val['qty']; 
						$cartView[$val['indx']]['qty'] = $val['qty']; 
						$cartView[$val['indx']]['total'] = $val['unit_price']*$val['qty'];
					}
					$carts = array_values($carts);
					$cartView = array_values($cartView);
					CakeSession::write('cart',$carts);
					CakeSession::write('cart-view',$cartView);
				}	
			}
		}
		$this->redirect(array('action'=>'check_out_user_auth'));
	}
	function check_out_user_auth() {
		$datas = array();
		if( $this->Session->read('member.id') == "" || $this->Session->read('member.id') == null ) {
			$page_name =  "User Authentication";
			$this->set('details', $datas);
		} else {
			$this->redirect(array('action'=>'check_out_step2'));
		}
	}
	function check_out_step2() {
		$datas = array();
		$products = $this->Cart->readProductCheckoutSummery();
		$page_name =  "Proced to CheckOut";
		$country = $this->Country->find('list');
		$this->set(compact('products','country'));
	}
	function ajx_change_state() {	
		$this->layout='ajax';	
		$this->autoRender =false;
		$bill_cid = $this->request->data['bill_cid'];
		$subcat = $this->State->find('all', array('conditions'=>array('country_id'=>$bill_cid,'status'=>'1'),'fields'=>array('id','name')));
		if( count($subcat) > 0 ) {
			foreach ($subcat as $key=>$val ){
				echo "<option value='".$val['State']['id']."' >".$val['State']['name']."</option>";
			}
		} else {
			echo "<option value='' >---empty---</option>";
		}
	}
	function check_out_process(){
		$this->autoRender =false;
		CakeSession::write('order-address',array());
		if( count($this->request->data['OrderAddress']) > 0 ) {
			$data = $this->request->data;
			//$orderAddress = array_values($data);
			CakeSession::write('order-address',$data);
			if( $this->request->data['OrderAddress']['payment'] == "paypal" ) {
			/*  ----------------- Payment with paypal ------------------------ */
				$getPaypalInfo = @$this->siteSettings->getSettingsByGroup('paypal_info');
				$paypalUserName = '';
				$paypalPwd = '';
				$paypalSignature = '';
				if( count($getPaypalInfo) > 0 ) {
					foreach($getPaypalInfo as $keyConfig=>$valConfig) {
						if( $valConfig['SiteSetting']['config_key'] == 'paypal_username' ) {
							$paypalUserName = $valConfig['SiteSetting']['config_value'];
						}
						if( $valConfig['SiteSetting']['config_key'] == 'paypal_password' ) {
							$paypalPwd = $valConfig['SiteSetting']['config_value'];
						}
						if( $valConfig['SiteSetting']['config_key'] == 'paypal_signature' ) {
							$paypalSignature = $valConfig['SiteSetting']['config_value'];
						}
					}
				}
				$this->redirect(array('controller' =>'Carts' , 'action' => 'paypalpayment_order/'));	
			} else {
				$this->cart_save('cod');
			}
		} else {
			CakeSession::write('order-address',array());
		}
	}
	/******************************Pay pal payment Start********************************************/
	function paypalpayment_order($learn=null,$price=null,$id=null)
	{	
		$this->layout = '';	
		$reference_id= $this->Session->read('SAVED_REF_ID');
		if(isset($id)) 
		$reference_id_encode = $id;
		else
		$reference_id_encode= base64_encode($this->Session->read('SAVED_REF_ID'));
		$products = $this->Cart->readProductCheckoutSummery();
		$this->set(compact('products'));
		$notifyURL='http://'.$_SERVER['HTTP_HOST'].$this->webroot.'Carts/paypal_response/'.$reference_id_encode;
		$this->set('notifyURL', $notifyURL);
		$returnpage ='http://'.$_SERVER['HTTP_HOST'].$this->webroot.'Carts/paypal_response/'.$reference_id_encode;
		$this->set('Returnpage', $returnpage);
		$cancelURL ='http://'.$_SERVER['HTTP_HOST'].$this->webroot.'Carts/cancel/';
		$this->set('cancelURL', $cancelURL);
		//$this->Session->delete('SAVED_REF_ID');
	}
	public function paypal_response($reference_id=null)
	{
		$ipn = true;
		if( $ipn ) { 
			$this->cart_save('paypal');
		} else {
			return $this->redirect(array("controller"=>"Carts", "action"=>"view"));
		}
	}
	public function thank_you() {
		//session_destroy('SAVED_REF_ID');
		//session_destroy($this->Session->read('Cart.session_id'));
		$this->Session->delete('SAVED_REF_ID');
		$this->Session->delete('Cart.session_id');
	}
	public function cancel()
	{
	}
	/******************************Pay pal payment End********************************************/
	function cart_save($paymentType){
		if( isset($paymentType) && $paymentType != '' )  {
			$products = $this->Cart->readProductCheckoutSummery();
			$orderAddress = $this->Cart->readProductCheckoutOrderAddress();
			pr($orderAddress);die();
			$paymentProcessType = "";
			$order_delivery_method = "cod";
			if($paymentType == "paypal") {
				$paymentProcessType = $paymentType;
				$order_delivery_method = "";
			}
			if( count($products) > 0 && ( $this->Session->read('member.id') != "") ) {	
				$this->loadModel('Order');
				$this->Order->create();
				$orderNo = $this->Order_number();
				$this->request->data['order_invoice_no'] = $orderNo;
				$this->request->data['order_transaction_id'] = $paymentProcessType;
				$this->request->data['order_transaction_type'] = $paymentType;
				$this->request->data['order_by_member_id'] = $this->Session->read('member.id');
				$this->request->data['order_amount'] = 0;
				$this->request->data['order_service_charge_amount'] = 0;
				$this->request->data['order_delivery_charge_amount'] = 0;
				$this->request->data['order_save_amount'] = 0;
				$this->request->data['order_delivery_method'] = $order_delivery_method;
				$this->request->data['order_status_id'] = 3;
				$this->request->data['order_date'] = date('Y-m-d h:i:s');
				$this->request->data['modified'] = date('Y-m-d h:i:s');
				if( $this->Order->save($this->request->data) ) {
					$order_id = $this->Order->getLastInsertId();
					foreach( $products as $cratKey=>$cartVal) {
						$this->loadModel('OrderProduct');
						$this->OrderProduct->create();
						$sizeOptStr = "";
						$i = 0;
						if( count($cartVal['SizeOption']) > 0 ) {
							foreach( $cartVal['SizeOption'] as $key=>$val) {
								if($i==0) {
									$sizeOptStr = $key.":".$val;
									$i++;
								} else {
									$sizeOptStr = $sizeOptStr.",".$key.":".$val;
								}
							}
						}
						$this->request->data['order_id'] = $order_id;
						$this->request->data['product_id'] = $cartVal['product_id'];
						$this->request->data['product_qty'] = $cartVal['qty'];
						$this->request->data['product_price'] = $cartVal['product_unit_price'];
						$this->request->data['order_total_price'] = $cartVal['total'];
						$this->request->data['size'] = $cartVal['Size'];
						$this->request->data['customize_details'] = $cartVal['customize_details'];
						$this->request->data['size_option'] = $sizeOptStr;
						$this->request->data['product_order_status'] = 3;
						$this->request->data['currency_id'] = $cartVal['currency_id'];
						$this->OrderProduct->save($this->request->data);
						unset($this->request->data['OrderProduct'] );
					}
					if( !isset($this->request->data['OrderAddress']) ) {
						$this->request->data = $orderAddress;
					}
					$this->request->data['OrderAddress']['order_id'] = $order_id;
					$this->loadModel('OrderAddress');
					$this->OrderAddress->create();
					$this->OrderAddress->save($this->request->data);
					/********************Mail*****************************/
					$adminEmail = @$this->siteSettings->getSiteSettings(0,'site_email');
					if( isset( $adminEmail['SiteSetting']['config_value'] ) && ( $adminEmail['SiteSetting']['config_value'] != '' ) ) {
						$adminEmail = $adminEmail['SiteSetting']['config_value'];
					} else {
						$adminEmail = 'info@febrique.com';
					}
					$url = Router::url('/', true);
					$data = array('products'=>$products,'OrderAddress'=>$orderAddress,'url'=>$url,'webroot'=>$this->webroot);
					/*$this->Email->to = $mail_id;
					$this->Email->subject = 'Order Details - Fabrique Group';
					$this->Email->from = 'Febrique Group' .'<'.$adminEmail.'>';
					$this->Email->layout = '';
					$this->Email->sendAs = 'html';
					$this->set('details', $data );
					$this->Email->template = 'order_template';
					$this->Email->send();*/
					
					
					
					/********************Mail*****************************/
					unset($this->request->data['OrderAddress'] );
					unset($this->request->data['Order'] );
					$this->Cart->destroyCart();
					return $this->redirect(array("controller"=>"Carts", "action"=>"thank_you"));
				}
			}else {
					return $this->redirect(array("controller"=>"Carts", "action"=>"view"));
			}
		}
	}
	public function test($hello)
	{
		$this->set('ut');
	}
	
	function Order_number() {
		$today = date("Ymdhis");
		$rand = strtoupper(substr(uniqid(sha1(time())),0,4));
		return $unique = $today . $rand;
	}
	
	//=========================Shipping Address==================================
     public function frist_step_add_to_cart()
	 { 
	 
	 }
	
	public function shipping_address()
	{ 
        $USERID=$this->Session->read('Auth.Member.id');
        $topupPoint=$this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$USERID),'fields'=>array('PlayerDetail.topup_point')));
        $this->set('topupPoint',$topupPoint);
		
		if($this->request->is('post'))
		{ 
	           $shipping_billing_address=$this->request->data;	        
			   $carts = $this->Cart->readProduct();
			   $carts= array_filter( $carts);
            		  
			//================billing Address==========================================================//
			     $shipping_billing_address_order['Order']['customer_id']= $this->Session->read('Auth.Member.id');
				 $shipping_billing_address_order['Order']['shipping_fullname']= $shipping_billing_address['ship_full_name'];
				 $shipping_billing_address_order['Order']['shipping_address']= $shipping_billing_address['ship_address'];
				 $shipping_billing_address_order['Order']['shipping_city']= $shipping_billing_address['ship_city'];
				 $shipping_billing_address_order['Order']['shipping_state']= $shipping_billing_address['ship_state'];
				 $shipping_billing_address_order['Order']['shipping_zip']= $shipping_billing_address['ship_zip'];
			 
				 $shipping_billing_address_order['Order']['payment_fullname']= $shipping_billing_address['bill_name'];
				 $shipping_billing_address_order['Order']['payment_address']= $shipping_billing_address['bill_address'];
				 $shipping_billing_address_order['Order']['payment_city']= $shipping_billing_address['bill_city'];
				 $shipping_billing_address_order['Order']['payment_state']= $shipping_billing_address['bill_state'];				 
				 $shipping_billing_address_order['Order']['payment_zip']= $shipping_billing_address['bill_zip'];
				  $shipping_billing_address_order['Order']['contact_no']= $shipping_billing_address['contact_no'];
				 
			 //===============================End=======================================================//
				  $total_price_arry=CakeSession::read('Subtotal');				
				  $subtotal=CakeSession::read('Subtotal');
				  $shipping_billing_address_order['Order']['total']=  $total_price_arry['subtotal']; 
				  $shipping_billing_address_order['Order']['cart_total']= $total_price_arry['subtotal'];
				  $shipping_billing_address_order['Order']['invoice_id']= "";
				  $shipping_billing_address_order['Order']['created']= date('Y-m-d');
                    
				
				  
                  $this->Order->save($shipping_billing_address_order);				  
				  $last_order_id=$this->Order->getLastInsertId();
			  
				 // $carts = $this->Cart->readProduct();
				  foreach ( $carts as $productIdKey => $productVal ) 
				  {   
					$product_id = $productVal['product_id'];				
					$products['OrderProduct']['order_id'] =  $last_order_id;	
					$products['OrderProduct']['product_id'] = $product_id;				
					$productDetails = $this->Product->find('first', array('conditions' => array('Product.id'=>$product_id)));				
					$products['OrderProduct']['price'] = $productDetails['Product']['price'];
					$products['OrderProduct']['size_id'] = $productVal['product_size_code'];				
					$products['OrderProduct']['quantity'] = $productVal['product_qty'];
					$products['OrderProduct']['created'] = date('Y-m-d');
					$this->OrderProduct->create();
					$this->OrderProduct->save($products);					
				 }
				 //==============================Payment Type=================================//
				   $payment_type=$this->request->data['payment_option'];
				  if($payment_type=='personal')
				  {
					  
					   return $this->redirect(array("controller"=>"Carts", "action"=>"personal_fund_success/". $last_order_id));	
				  }else
				  {
					   return $this->redirect(array("controller"=>"Carts", "action"=>"comm_web_payment_sec/". $last_order_id));	
					  
				  }

			     		 
		}		
		 
	}  
	
	
   //================================================================================
	/*public function review_order()
	{
		$carts = $this->Cart->readProduct();
		//CakeSession::write('cart',array());
		//exit;
		//pr($carts);
		//exit;
		$products = array();		
		$this->loadModel('Product');
		CakeSession::write('cart-view',array());
		//exit;
		$Subtotal=00;
		if( count($carts) > 0 ) {
			//$products = array( 0 => array('product_id'=>'','product_name'=>'','image'=>'','product_unit_price'=>'','total'=>'','qty'=>'', 'Size'=>'') );
			$products = array();
			$subtotal_arr= array();
			$i=0;
			$total = 0;
			$total_size = 0;
			foreach ( $carts as $productIdKey => $productVal ) 
			{   
				$product_id = $productVal['product_id'];
				$products[$i]['product_id'] = $product_id;
				$productDetails = $this->Product->find('first', array('conditions' => array('Product.id'=>$product_id)));
				$total = $productDetails['Product']['price']*$productVal['product_qty'];
				$products[$i]['product_name'] = $productDetails['Product']['name'];				
				$products[$i]['image'] = $productDetails['ProductImage']['image'];
				$products[$i]['product_unit_price'] = $productDetails['Product']['price'];
				$products[$i]['product_stock'] = $productDetails['Product']['total_qty']-$productDetails['Product']['total_sale'];
				$products[$i]['total'] = $total;
                $products[$i]['size'] = $productVal['product_size'];				
				$products[$i]['qty'] = $productVal['product_qty'];
				$products[$i]['product_size_code'] = $productVal['product_size_code'];
				//$Subtotal=$total;
			    $Subtotal = $Subtotal+$total;
				$total_size = $total_size+$productVal['product_qty'];
				$i++;
			}
		}
             $subtotal_arr['subtotal']= $Subtotal;
		CakeSession::write('Subtotal',$subtotal_arr);	
		CakeSession::write('cart-view',$products);
		CakeSession::write('total_size',$total_size);
	//==============Sub Total Price============================	//
   $price_explode_sub_total = explode('.',$subtotal_arr['subtotal']);
	if(isset($price_explode_sub_total[0]))
	{
		$fistpartprice_sub_total= $price_explode_sub_total[0];	
	}else								
	{
		$fistpartprice_sub_total=00;	
	}							
	if(isset($price_explode_sub_total[1]))
	{
		$secpartprice_sub_total= $price_explode_sub_total[1];								
	}else								
	{
		$secpartprice_sub_total='00';										
	}
	    CakeSession::write('fprice',$fistpartprice_sub_total);
		CakeSession::write('sprice',$secpartprice_sub_total);
	//==========================================================//
		$this->set(compact('products'));
	}*/
	public function pay_finish()
	{
	}

	public function comm_web_payment_sec($order_id)
	{       
	        $base_url= $this->webroot;
	        $total_price_arry=CakeSession::read('Subtotal');	             
			//$accessCode    =  "9A591939";//value from migs payment gateway
			//$merchantId    =  "TESTDEVPLACOM01";//value from migs payment gateway
			 $amount = $total_price_arry['subtotal'];
			
			//======================For Live Enviourment =======================//
		
			  $SECURE_SECRET = "0B4737508BA899863FBC279A51422CB2";
			  $unique_id = rand(999999,8988888888);//this is a sample random no
			  $order = $order_id;
		  
			$testarr = array(
			"vpc_Amount" => $amount*100,//Final price should be multifly by 100
			"vpc_Currency" => 'AUD',
			"vpc_AccessCode" => "A39B63F9",//Put your access code here
			"vpc_Command"=> "pay",
			"vpc_Locale"=> "en",
			"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
			"vpc_Merchant"=> "DEVPLACOM01",//Add your merchant number here
			"vpc_OrderInfo"=> $order_id,//this also better to be a unique number
			"vpc_ReturnURL"=> HTTP_SERVER_PATH."carts/success",
			"vpc_Version"=> "1"
			);
		
			$md5HashData = $SECURE_SECRET;	
			ksort ($testarr);		
			$appendAmp = 0;
	        $vpcURL = "https://migs.mastercard.com.au/vpcpay" . "?";
			foreach($testarr as $key => $value)
			{
               // create the md5 input and URL leaving out any fields that have no value
				if (strlen($value) > 0) {					
					// this ensures the first paramter of the URL is preceded by the '?' char
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);						
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);						
					}
					$md5HashData .= $value;
				}
			}
			if (strlen($SECURE_SECRET) > 0) 
			{
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
			}			
			header("Location: ".$vpcURL);			
			exit();
			
		//================================End============================================//
			
			
			/*$unique_id = rand(999999,8988888888);//this is a sample random no
			//$order = rand(10,100);
			$testarr = array(
			"vpc_Amount" => $amount*100,//Final price should be multifly by 100
			"vpc_Currency" => 'AUD',
			"vpc_AccessCode" => $accessCode,//Put your access code here
			"vpc_Command"=> "pay",
			"vpc_Locale"=> "en",
			"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
			"vpc_Merchant"=> $merchantId,//Add your merchant number here
			"vpc_OrderInfo"=>$order_id,//this also better to be a unique number
			"vpc_ReturnURL"=> HTTP_SERVER_PATH."carts/success",
			"vpc_Version"=> "1");
			ksort($testarr);
			$SECURE_SECRET =  "84712313F1FBE77FE54EF9101C779ABB";
 //value from migs payment gateway
			$securehash = $SECURE_SECRET;
			$url = "";
			foreach ($testarr as $key => $value)
			{
				$securehash .= $value;
				$url .= $key."=".urlencode($value)."&";
			}
			$securehash = md5($securehash);//Encoding
			$url .= "vpc_SecureHash=".$securehash;		
			header("location:https://migs.mastercard.com.au/vpcpay?$url");
		exit;*/
	}
	
	function success()
	{	
		App::uses('CakeEmail', 'Network/Email');
		$all_respone_by_comweb =$_GET;		
		//echo '<pre>';
		//print_r($all_respone_by_comweb);
		//exit;
		$all_oder_details=$this->Order->find('first',array('conditions'=>array('Order.id'=>$all_respone_by_comweb['vpc_OrderInfo']),'fields'=>'Order.customer_id'));
		$customer_id=$all_oder_details['Order']['customer_id'];
		//=========================UserDetails==================================================//
          $this->User->unBindModel(array('hasOne' => array('CoachDetail','PlayerDetail','Nationality','CoachSpecialityP','CoachSpecialityS')));	
		  $buyer_details=$this->User->find('first',array('conditions'=>array('User.id'=>$customer_id)));			
			//pr( $buyer_details);
			//exit;
		//===========================================================================//
		$BuyerEmail= $buyer_details['User']['email_address'];
		//echo $BuyerEmail;
		//exit;
		//==============================================================================//
		
		 $this->Order->bindModel(array(
			   'hasOne' => array(
								'OrderProduct'=>array(
									 'className'=>'OrderProduct',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'order_id',
									 'fields'=>''
								),
								 'Product'=>array(
									 'className'=>'Product',
									 'conditions'   => 'OrderProduct.product_id=Product.id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								  
								  'ProductImage'=>array(
									 'className'=>'ProductImage',
									 'conditions'   => 'ProductImage.product_id=OrderProduct.product_id And ProductImage.deflt_value=1',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								   'Size'=>array(
									 'className'=>'Size',
									 'conditions'   => 'Size.id=OrderProduct.size_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
		
		$all_oder_details=$this->Order->find('all',array('conditions'=>array('Order.id'=>$all_respone_by_comweb['vpc_OrderInfo'])));
		//pr($all_oder_details);
		//exit;		
        if($all_respone_by_comweb['vpc_Message']=='Approved')
		{
			$this->Order->id=$all_respone_by_comweb['vpc_OrderInfo']; //this is order_id
			$update_order['Order']['invoice_id']=$all_respone_by_comweb['vpc_MerchTxnRef'];
			$update_order['Order']['comweb_status']=$all_respone_by_comweb['vpc_Message'];
			$update_order['Order']['payment_status']=1;
			$update_order['Order']['paynent_type']='comweb';
			
			foreach($all_oder_details as $update_sale_products)
			{				
				$product_id=$update_sale_products['OrderProduct']['product_id'];
				$size_id=$update_sale_products['OrderProduct']['size_id'];				
				$buy_quantity=$update_sale_products['OrderProduct']['quantity'];				
				//============================find product size id=======================//
				$product_qty=$this->ProductSizeQty->find('first',array('conditions'=>array('ProductSizeQty.product_id'=>$product_id,'ProductSizeQty.size_id'=>$size_id)));
				//========================================================================//				
				$privious_quntity=$product_qty['ProductSizeQty']['sale_qty'];
				$total_sale_quantity=$privious_quntity+$buy_quantity;				
				//======================for upadte sale quinty========================//
				$this->ProductSizeQty->id=$product_qty['ProductSizeQty']['id'];							
				$size_qty_arr['ProductSizeQty']['sale_qty']= $total_sale_quantity;
				$this->ProductSizeQty->save($size_qty_arr);
				$this->ProductSizeQty->create();
				//=====================================================================//			
			}			
			if($this->Order->save($update_order))
			{
				/*$this->Email->to = $BuyerEmail;
				$this->Email->subject = 'Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has is successful - Developaplayer';
				$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('all_respone_by_comweb',$all_respone_by_comweb);
				$this->set('all_oder_details', $all_oder_details);
				$this->Email->template = 'order_details';
				$this->Email->send();

				$this->Email->to = 'info@developaplayer.com';
				$this->Email->subject = 'Received Order';
				$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('all_respone_by_comweb',$all_respone_by_comweb);
				$this->set('all_oder_details', $all_oder_details);
				$this->Email->template = 'order_detail_admin';															
				$this->set('txnId', $all_oder_details[0]['Order']['invoice_id']);
				$this->Email->send();*/

					
				App::uses('CakeEmail', 'Network/Email');
				$this->Email = new CakeEmail('smtp');
				
				//======================= Buyer Email =============================				
				$this->Email->to(array($BuyerEmail));
				$this->Email->subject('Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has is successful - Develop A Player');			
				$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));	
				$this->Email->emailFormat('html');				
				$this->Email->viewVars(array('all_oder_details'=>$all_oder_details,'all_respone_by_comweb'=>$all_respone_by_comweb));
				$this->Email->template('order_details');
				$this->Email->send();

				//======================= Admin Email =============================		
				$this->Email->to(array(Configure::read('admin_email')=>'Develop A Player'));	
				$this->Email->subject('Received Order');				
				$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));	
				$this->Email->emailFormat('html');	
				$this->Email->viewVars(array('all_oder_details'=>$all_oder_details,'all_respone_by_comweb'=>$all_respone_by_comweb,'txnId'=>$all_oder_details[0]['Order']['invoice_id']));
				$this->Email->template('order_detail_admin');				
				$this->Email->send();
				
				CakeSession::write('cart',array(''));
				CakeSession::write('Subtotal',0);	
				CakeSession::write('cart-view',array());
				CakeSession::write('total_size',0);
				CakeSession::write('fprice',00);
				CakeSession::write('sprice',00);
					
			}				
		}else
			{
				//echo $all_oder_details[0]['Order']['invoice_id'];
				//exit;
				//echo $all_respone_by_comweb['vpc_OrderInfo'];
				//exit;
				//echo '<pre>';
				  // print_r($all_respone_by_comweb);
				//exit;
			
				$this->Order->id=$all_respone_by_comweb['vpc_OrderInfo']; //this is order_id
				$update_order['Order']['invoice_id']=$all_respone_by_comweb['vpc_MerchTxnRef'];
				$update_order['Order']['comweb_status']=$all_respone_by_comweb['vpc_Message'];
				$update_order['Order']['payment_status']=1;
				$update_order['Order']['paynent_type']='comweb';
				$this->Order->save($update_order);
			
				/*$this->Email->to = $BuyerEmail;
				$this->Email->subject = 'Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has failed';
				$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('all_oder_details', $all_oder_details);
				$this->Email->template = 'payment_cancel';														
				$this->set('all_respone_by_comweb',$all_respone_by_comweb);	
				$this->set('txnId', $all_oder_details[0]['Order']['invoice_id']);					
				$this->Email->send();*/	   
			
				App::uses('CakeEmail', 'Network/Email');
				$this->Email = new CakeEmail('smtp');		
				$this->Email->to(array($BuyerEmail));
				$this->Email->subject('Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has failed');
				$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));					
				$this->Email->emailFormat('html');					
				$this->Email->template = 'payment_cancel';
				$this->Email->viewVars(array('all_oder_details'=>$all_oder_details,'txnId'=>$all_oder_details[0]['Order']['invoice_id'],'all_respone_by_comweb'=>$all_respone_by_comweb));								
				$this->Email->send();			
			}		
	}
	
	
	function order_summary($order_invoice_id=NULL)
	{	 if($this->Session->read('Auth.Member.id')=="")
			{
			   $this->redirect(array('controller'=>'homes','action'=>'index/1'));	
			}	
	    // $order_invoice_id=288377701;
        $user_id= $this->Session->read('Auth.Member.id');       	
		$this->Order->bindModel(
			array(
			'hasMany' => array(
					  'OrderProduct' => array(
						  'className' => 'OrderProduct',
						  'foreignKey' => 'order_id',
						 // 'order'   => 'id desc',
					  )
					  
					  ), 
                   
			  )
			);
		if($order_invoice_id)
		{
			 $all_purches_orders_details= $this->Order->find('all',array('conditions'=>array('Order.customer_id'=>$user_id,'Order.comweb_status'=>'Approved','Order.payment_status'=>1,'Order.invoice_id'=>$order_invoice_id)));	  
             $this->set('is_pageination',0);
		}else
		{
			
			  $this->Paginator->settings = array(
								'conditions' => array('Order.customer_id'=>$user_id,'Order.comweb_status'=>'Approved','Order.payment_status'=>1),								
								'order' =>'Order.id DESC',
								'limit' => 10
                              );
                    $all_purches_orders_details = $this->Paginator->paginate('Order');
					$this->set('is_pageination',1);
                    // $this->set(compact('data'));
		    
			   // $all_purches_orders_details= $this->Order->find('all',array('conditions'=>array('Order.customer_id'=>$user_id,'Order.comweb_status'=>'Approved','Order.payment_status'=>1),'order' => array('Order.id desc')));	  
	  
		}
		
     $i=0;
	  foreach( $all_purches_orders_details as  $all_purches)
	  {		  
		foreach($all_purches['OrderProduct'] as $key=>$OrderProduct)
		{
			$this->Product->unBindModel(array('hasMany' => array('AllProductImage')));
            $product_details =$this->Product->find('first',array('conditions'=>array('Product.id'=>$OrderProduct['product_id'])));
			
			$size_details =$this->Size->find('first',array('conditions'=>array('Size.id'=>$OrderProduct['size_id'])));
			$size_name=$size_details['Size']['name']; 
			$all_purches_orders_details[$i]['OrderProduct']['MYPRODUCT'][$key]=  $product_details['Product'];
			$all_purches_orders_details[$i]['OrderProduct']['MYPRODUCT'][$key]['ProductImage']=  $product_details['ProductImage'];
			$all_purches_orders_details[$i]['OrderProduct']['MYPRODUCT'][$key]['size']=  $size_name;
		}		  		  
		  $i++;
	  }	  
	  $this->set('all_purches_orders_details',$all_purches_orders_details);
	  

	 
	}
	
	function order_information($id=null){
		
		if($id == null){
			$this->redirect($this->referer);
		}
		$id = base64_decode($id);
		/*$this->Order->bindModel(array(
			   'belongsTo' => array(
								'User'=>array(
									 'className'=>'User',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'customer_id',
									 'fields'=>'User.mobile_number'
								)
		               )
				));*/
		$orderDetails = $this->Order->find('first',array('fields'=>array('Order.invoice_id','Order.total','Order.created','Order.payment_address','Order.payment_city','Order.payment_fullname','Order.payment_zip','Order.payment_state','Order.payment_state','Order.contact_no'),'conditions'=>array('Order.id'=>$id)));
		/*pr($orderDetails);
		die;*/
		$this->set('orderDetails',$orderDetails);
	}

  
function personal_fund_success($OrderId)
	{
		$unique_txn_id = rand(999999,8988888888);
		//$all_respone_by_comweb =$_GET;
	
		$all_oder_details=$this->Order->find('first',array('conditions'=>array('Order.id'=>$OrderId),'fields'=>array('Order.customer_id','Order.invoice_id','Order.total')));
		$customer_id=$all_oder_details['Order']['customer_id'];
		//=========================UserDetails==================================================//
          $this->User->unBindModel(array('hasOne' => array('CoachDetail','PlayerDetail','Nationality','CoachSpecialityP','CoachSpecialityS')));	
		  $buyer_details=$this->User->find('first',array('conditions'=>array('User.id'=>$customer_id)));
			
			//pr( $buyer_details);
			//exit;
		//===========================================================================//
		$BuyerEmail= $buyer_details['User']['email_address'];
		//echo $BuyerEmail;
		//exit;
		//==============================================================================//
		
			if($all_oder_details['Order']['invoice_id']=="")
			{
		       $update_order['Order']['invoice_id']=$unique_txn_id;
			  
				$this->Order->id=$OrderId;
				$update_order['Order']['comweb_status']='Approved';
				$update_order['Order']['paynent_type']='Personal';			
				$update_order['Order']['payment_status']=1;
			
			
			
			//===========================Update=============================//
			
			if($this->Order->save($update_order))
			{
					
		  //============All Order Details==========================//
             $this->Order->bindModel(array(
			   'hasOne' => array(
								'OrderProduct'=>array(
									 'className'=>'OrderProduct',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'order_id',
									 'fields'=>''
								),
								 'Product'=>array(
									 'className'=>'Product',
									 'conditions'   => 'OrderProduct.product_id=Product.id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								  
								  'ProductImage'=>array(
									 'className'=>'ProductImage',
									 'conditions'   => 'ProductImage.product_id=OrderProduct.product_id And ProductImage.deflt_value=1',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								   'Size'=>array(
									 'className'=>'Size',
									 'conditions'   => 'Size.id=OrderProduct.size_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
				 
			 $all_oder_details=$this->Order->find('all',array('conditions'=>array('Order.id'=>$OrderId)));			
			//======================================================//
		
			foreach($all_oder_details as $update_sale_products)
			{				
					 $product_id=$update_sale_products['OrderProduct']['product_id'];
					 $size_id=$update_sale_products['OrderProduct']['size_id'];				
					 $buy_quantity=$update_sale_products['OrderProduct']['quantity'];				
				//============================find product size id=======================//
				     $product_qty=$this->ProductSizeQty->find('first',array('conditions'=>array('ProductSizeQty.product_id'=>$product_id,'ProductSizeQty.size_id'=>$size_id)));
				//========================================================================//				
					$privious_quntity=$product_qty['ProductSizeQty']['sale_qty'];
					$total_sale_quantity=$privious_quntity+$buy_quantity;				
				//======================for upadte sale quinty========================//
					$this->ProductSizeQty->id=$product_qty['ProductSizeQty']['id'];							
					$size_qty_arr['ProductSizeQty']['sale_qty']= $total_sale_quantity;
					$this->ProductSizeQty->save($size_qty_arr);
					$this->ProductSizeQty->create();              
			}
			
			//=========================Update Personal Found=============================================//
			
					$topupPoint=$this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$customer_id),'fields'=>array('PlayerDetail.topup_point')));			 
					$total_remaing_topup_point= $topupPoint['PlayerDetail']['topup_point']-$all_oder_details[0]['Order']['total'];
					
         //====================================End====================================================================//
		 
					//$this->set('topupPoint',$topupPoint);				  
					$this->PlayerDetail->updateAll(array('PlayerDetail.topup_point'=>$total_remaing_topup_point),array('PlayerDetail.user_id'=> $customer_id));	
					
					/*$this->Email->to = $BuyerEmail;
					$this->Email->subject = 'Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has is successful - Developaplayer';
					$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
					$this->Email->layout = '';
					$this->Email->sendAs = 'html';
					$this->set('all_oder_details', $all_oder_details);
					$this->Email->template = 'order_details';													
					$this->set('txnId', $all_oder_details[0]['Order']['invoice_id']);
					$this->Email->send();
					//===================================Admin==================================//					
					$this->Email->to = 'info@developaplayer.com';
					$this->Email->subject = 'Recvied Order';
					$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
					$this->Email->layout = '';
					$this->Email->sendAs = 'html';
					$this->set('all_oder_details', $all_oder_details);
					$this->Email->template = 'order_detail_admin';
					$this->Email->send();										
					$this->set('txnId', $all_oder_details[0]['Order']['invoice_id']);*/

					App::uses('CakeEmail', 'Network/Email');
					$this->Email = new CakeEmail('smtp');					
					
					$this->Email->to(array($BuyerEmail));
					$this->Email->subject('Your transaction with DEVELOP A PLAYER PTY LTD on '.date('Y-m-d').' has is successful - Develop A Player');					
					$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));	
					$this->Email->emailFormat('html');					
					$this->Email->template('order_details');
					$this->Email->viewVars(array('txnId'=>$all_oder_details[0]['Order']['invoice_id'],'all_oder_details'=>$all_oder_details));					
					$this->Email->send();
					//===================================Admin==================================//					
					$this->Email->to(array(Configure::read('admin_email')=>'Develop A Player'));	
					$this->Email->subject('Recvied Order');					
					$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));	
					$this->Email->emailFormat('html');
					$this->Email->viewVars(array('txnId'=>$all_oder_details[0]['Order']['invoice_id'],'all_oder_details'=>$all_oder_details));					
					$this->Email->template('order_detail_admin');
					$this->Email->send();					
				}		
			
			}else

            {
				//============All Order Details==========================//
             $this->Order->bindModel(array(
			   'hasOne' => array(
								'OrderProduct'=>array(
									 'className'=>'OrderProduct',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'order_id',
									 'fields'=>''
								),
								 'Product'=>array(
									 'className'=>'Product',
									 'conditions'   => 'OrderProduct.product_id=Product.id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								  
								  'ProductImage'=>array(
									 'className'=>'ProductImage',
									 'conditions'   => 'ProductImage.product_id=OrderProduct.product_id And ProductImage.deflt_value=1',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								   'Size'=>array(
									 'className'=>'Size',
									 'conditions'   => 'Size.id=OrderProduct.size_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
				  
			        $all_oder_details=$this->Order->find('all',array('conditions'=>array('Order.id'=>$OrderId)));					
				    $this->set('all_oder_details', $all_oder_details);					
					$this->set('txnId', $all_oder_details[0]['Order']['invoice_id']);				
			}	
	
					 CakeSession::write('cart',array(''));
					 CakeSession::write('Subtotal',0);	
					 CakeSession::write('cart-view',array());
					 CakeSession::write('total_size',0);
					 CakeSession::write('fprice',00);
					 CakeSession::write('sprice',00);					
	}   
	
	
	 function test_email()
   { 
                     Configure::write('debug', 2);
                     $this->layout="ajax";
					 $all_oder_details=array();
					 
					 /*$Email->to = 'utpal@accenza.com'; 
					 $Email->subject = "Order Details";
					 $Email->from = 'finance@misschase.com';
					 $Email->layout = '';					
					 $Email->sendAs = 'html';		
				     $this->set('all_oder_details', $all_oder_details);	
					  $Email->template = 'default_new';
					 $Email->send();*/
                     
				    $data="";
					$this->Email->to = 'finance@misschase.com';
					$this->Email->cc = 'utpal@accenza.com';
					$this->Email->subject = 'Order Details - Developaplayer';
					$this->Email->from = 'Developaplayer' .'<'.Configure::read('admin_email').'>';
					$this->Email->layout = '';
					$this->Email->sendAs = 'html';
					$this->set('details', $data );
					$this->Email->template = 'default_new';
					$this->Email->send();
					
					echo 'successfully send';
						exit;
					
					
   }  
	
	
}
?>