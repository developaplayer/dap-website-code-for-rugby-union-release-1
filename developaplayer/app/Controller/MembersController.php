<?php
App::uses('CommonController', 'Controller');
class MembersController  extends CommonController {
	var $name = 'Homes';
	 public function beforeFilter() {
        $this->Auth->allow(array('index'));
		$this->set('title_for_layout', 'Develop a Player');
	   parent::beforeFilter();
       
    }
	public function index() {
		
	}
	
	public function login(){
	//$this->layout = 'admin_login';
		
        if($this->Session->check('Auth.User'))
		{			
         $this->redirect($this->Auth->redirectUrl());
        }
		if($this->request->is('post')){
			if ($this->Auth->login()) {
                $this->Session->setFlash(__('Welcome '. $this->Auth->user('name'),'default',array('class'=>'alert alert-success alert-dismissable')));
			   return $this->redirect($this->Auth->redirectUrl());

            } else {
				
				$this->Session->setFlash("Invalid username or password",'default',array('class'=>'alert alert-danger alert-dismissable'));
            }
			
		}
		   $this->set('title_for_layout', ' Login');
	}
	
	
	
}