<?php
App::uses('CommonController', 'Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class PlayersController  extends CommonController {
	var $name = 'Players';
	var $uses = array('PlayerDetail','AddPlayerRating','Country','Club','PlayerPosition','CoachSpeciality','User','CoachDetail','Certificate','CoachCertificate','Video','UserComment','PlayerRelationship','PlayerVideo','AchiveCertificate','AgeGroup','PlayerRecoardAchivement','PlayerTopup','CapabilityMatrixAge');
	
	public function beforeFilter() {
        $this->Auth->allow(array('index','register','login','player_profile','player_list','player_list_ajax','player_list_auto_complate','auto_complate_name','player_list_by_select','complt_reg','all_note_list','get_graph_value_by_age_group','get_graph_value_by_current','insert_player_achivement','forgot_password','create_new_password','success','add_topup'));
		$this->set('title_for_layout', 'Develop a Player');
		
		  parent::beforeFilter();
		    AuthComponent::$sessionKey = 'Auth.Member';
			$this->Auth->authenticate = array(
				'Form' => array(
				'userModel'=> 'User',
				'fields' => array('username' => 'email_address','password' => 'password'),               
				'scope' => array('User.status' => 'A','User.type'=>'P')
				)
			);
         
		$this->Auth->loginAction = array('controller' => 'players', 'action' => 'login');
		$this->Auth->logoutRedirect = array('controller' => 'players', 'action' => 'login');
		$this->Auth->loginRedirect = array('controller' => 'players','action'=>'player_profile');
    }	
	public function index() {
		$this->layout = "ajax";
		$this->render(false);
		$this->redirect(array('action'=>'register'));
	}
	
	public function login()
	{		
	   if($this->Session->check('Auth.Member'))
		{			
			$this->redirect($this->Auth->redirectUrl());
        }			
		if($this->request->is('post'))
		{
			 //pr($this->request->data);
			// exit;
           
			 if(isset($this->request->data['no_login']))
				{			
				   $no_login = $this->request->data['no_login'];
				   CakeSession::write('no_login', $no_login);
				   
				}
				
            if(!empty($this->request->data['User'])) 
            {				
			if($this->Auth->login()) 
			{	
		         $no_login = CakeSession::read('no_login');	
               
				$this->Session->setFlash(__('Welcome you are login as a Player'. $this->Auth->user('name'),'default',array('class'=>'alert alert-success alert-dismissable')));
				//echo  $no_login;
				//exit;
				if($no_login=="")
				{
					 return $this->redirect($this->Auth->redirectUrl());
				 
				}else
				{
					if($no_login==0)
					 {
						$this->redirect(array('controller' => 'carts', 'action' => 'view'));	
											 
					 }else{	
							 
						  return $this->redirect($this->Auth->redirectUrl());
					 }					
				}				
			    //return $this->redirect($previous_location);
				
            } else {                  
				$this->Session->setFlash("Invalid username or password",'default',array('class'=>'alert alert-danger alert-dismissable'));
			}
		}			
		}
		   $this->set('title_for_layout', ' Login');
	}
	//=============================== Player Registration  =======================================================	
	public function register(){
		//$country = $this->Country->find('list',array('fields'=>array('Country.id','Country.country_name')));
		$country=$this->Country->get_country();
		$club=$this->Club->get_club();
		$player_position=$this->PlayerPosition->get_player_position();
		$age_group=$this->AgeGroup->find('list');		
		$this->set('country',$country);
		$this->set('club',$club);
		$this->set('player_position',$player_position);
		$this->set('age_group',$age_group);
		
		if(!empty($this->data)){	
		//pr($this->data);
		//die;		
				//======================  User Table Information ==============================
				$player_user_details['User']['first_name'] = $this->data['User']['first_name'];
				$player_user_details['User']['last_name'] = $this->data['User']['last_name'];
				$player_user_details['User']['email_address'] = $this->data['User']['email_address'];
				$player_user_details['User']['password'] = $this->Auth->password($this->data['User']['password']);
				//$this->data['User']['password'] = $this->Auth->password($this->data['User']['password']);
				$player_user_details['User']['type'] = 'P';
				//===============================================================================
				$player_details['PlayerDetail']['height'] = $this->data['PlayerDetail']['height'];
				$player_details['PlayerDetail']['weight'] = $this->data['PlayerDetail']['weight'];
				$player_details['PlayerDetail']['dob'] = $this->data['PlayerDetail']['dob'];
				$player_details['PlayerDetail']['primary_position_id'] = $this->data['PlayerDetail']['primary_position_id'];
				$player_details['PlayerDetail']['secondary_position_id'] = $this->data['PlayerDetail']['secondary_position_id'];
				$player_details['PlayerDetail']['nationality'] = $this->data['PlayerDetail']['nationality'];				
				$player_details['PlayerDetail']['bench_press'] = $this->data['PlayerDetail']['bench_press'];
				$player_details['PlayerDetail']['dead_lift'] = $this->data['PlayerDetail']['dead_lift'];
				$player_details['PlayerDetail']['20m_sprint'] = $this->data['PlayerDetail']['20m_sprint'];
				$player_details['PlayerDetail']['60m_sprint'] = $this->data['PlayerDetail']['60m_sprint'];
				$player_details['PlayerDetail']['club_id'] = $this->data['PlayerDetail']['club_id'];
				$player_details['PlayerDetail']['age_group_id'] = $this->data['PlayerDetail']['age_group_id'];
				
				$player_details['User']['status'] = 'I';
				if(isset($this->params['form']['profile_img']['name'])){
					$player_details['PlayerDetail']['profile_img'] = $this->params['form']['profile_img']['name'];
				}
			
			$this->User->set($player_user_details);
			$this->PlayerDetail->set($player_details);
			//$this->PlayerDetail->set($this->data);
			if($this->User->validates($player_user_details)){
				//echo $this->PlayerDetail->validates($player_details);
				//exit();
				if($this->PlayerDetail->validates($player_details)){
					if($this->User->save($player_user_details)){
						
						$player_details['PlayerDetail']['user_id'] = $this->User->getLastInsertId();
						if($_FILES){
							$foo = new Upload($this->params['form']['profile_img']); 
							  $foo->allowed = array('image/jpg/jpeg');
								if ($foo->uploaded) {
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/original/');
									   $foo->image_resize          = true;
									   
								if($foo->image_dst_x>$foo->image_dst_y)
								   {   
										$foo->image_ratio_x        = true;
										$foo->image_y              = 357;
		
								   }else if($foo->image_dst_x<$foo->image_dst_y)
								   {
									   $foo->image_ratio_y               = true;
									   $foo->image_x                     = 439;
									   
								   }else if($foo->image_dst_x==$foo->image_dst_y)
								   {
									   $foo->image_ratio_y         = true;
									   $foo->image_x               = 357;
									   
								   }
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/profile_pic/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 160;
									   $foo->image_x               = 130;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/thumb/');
									  if ($foo->processed) {
											$player_details['PlayerDetail']['profile_img'] = $foo->file_dst_name;
									  }
								}
						}
						
						$random_otp= $this->n_digit_random(4);
						$player_details['PlayerDetail']['otp'] =$random_otp;
						//echo $player_user_details['User']['email_address'];
					    //exit;
						if($this->PlayerDetail->save($player_details))
						{
							
							
							/*$this->Email->to =$player_user_details['User']['email_address']; 
							$this->Email->subject = "Otp";
							$this->Email->from = Configure::read('admin_email');
							$this->Email->layout = '';
							$this->Email->template = 'confirm_otp';
							$this->Email->sendAs = 'html';		
							$this->set('otp',$random_otp);
							$this->Email->send();*/
							
						
							App::uses('CakeEmail', 'Network/Email');
							$this->Email = new CakeEmail('smtp');						
							$this->Email->to(array($player_user_details['User']['email_address']));
							$this->Email->subject('Otp confirmation alert for your valid Registration');							
							$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));							
							$this->Email->emailFormat('html');						
							$this->Email->viewVars(array('otp'=>$random_otp));
							$this->Email->template('confirm_otp');														
							$this->Email->send();
							
						
							
							$last_user_id =base64_encode($player_details['PlayerDetail']['user_id']);							
							$this->set('message','<span class="text-success">Account Successfully Created. Check Your Mail To Complete Registration.</span>');
							$this->redirect(array('action'=>'complt_reg','myArgument' => $last_user_id));
						}else{
							$this->set('message','<span class="text-danger">Unable To Creating Your Account.</span>');
						}
					}
				}
			}
			else
			{
				pr($this->User->invalidFields()); exit;
			}			
		}
	}
	
	public function complt_reg()
	{		
		 $user_id= base64_decode($this->request->params['named']['myArgument']);
		 $get_otp=$this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$user_id),'fields'=>'PlayerDetail.otp'));
		
		 if($this->request->is('post'))
		 {
			
			$user_input_otp= $this->request->data['otp_name'];			
			if($get_otp['PlayerDetail']['otp']==$user_input_otp)
			{
				$update_satus['User']['status']= 'A';
				$this->User->id=$user_id;
				$this->User->save($update_satus);

				$this->Session->setFlash(__('<strong style="color:green">Complete Your Registration process.now login your account.</strong>'));				
				 return $this->redirect( array('action' => 'login'));				 
			}else{
			{
				$this->Session->setFlash(__('invalid otp please try again.'));
				
			}	
			 
		 }
		
		 }
	}
	
	private function n_digit_random($digits)
	{
           return rand(pow(10, $digits - 1) - 1, pow(10, $digits) - 1);
    }
	
//=================================== Login ======================================
	public function logout() {
		CakeSession::write('no_login',1);
		return $this->redirect($this->Auth->logout());
	}
	//==================================== Player Profile ===================================================
	public function player_profile($id=null)
	{  
            if($this->Auth->user('id')=="")
			{
			   $this->redirect(array('controller'=>'homes','action'=>'index/1'));	
				
			}	
			
	   
	    $id=base64_decode($id);		
		if($id==null)
		{
		 $auth_user_id= $this->Auth->user('id');
		}
		else
	    {
		  $auth_user_id= $id;				
		}	
	   $this->User->unBindModel(array('hasOne' => array('Nationality')));	   
	   $this->PlayerDetail->bindModel(array(
			   'hasOne' => array(
								'Nationality'=>array(
									 'className'=>'Country',
									 'conditions'   => 'Nationality.id=PlayerDetail.nationality',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>'Nationality.country_name'
								),
								 'player_position_pimery'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'player_position_pimery.id=PlayerDetail.primary_position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
							     'player_position_secondery'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'player_position_secondery.id=PlayerDetail.secondary_position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
		
		// Player Achievements ==========================================================
		
		$PlayerRecoardAchivements = $this->PlayerRecoardAchivement->find('all',array('order'=>'PlayerRecoardAchivement.id desc','conditions'=>array('PlayerRecoardAchivement.player_id'=>$auth_user_id)));
		//pr($PlayerRecoardAchivements);
		//exit;
		$this->set('PlayerRecoardAchivements',$PlayerRecoardAchivements);
		//====================================================================
		
	     $all_player_user_data=$this->Auth->user();	
		 if($id>0)
		 {
	        $userDetails= $this->User->find('first',array('conditions'=>array('User.id'=> $auth_user_id)));
			$paramsexits_dtails=$userDetails['User'];
			$all_player_user_data=$paramsexits_dtails;
		 }

	   //==============unset data which model are not use this function=======================
	   //======================================================================================
	   
	   unset($all_player_user_data['CoachDetail']);
	   unset($all_player_user_data['CoachSpecialityP']);
	   unset($all_player_user_data['CoachSpecialityS']);
	   unset($all_player_user_data['Nationality']);
	   
	 //==============unset data====================================================================
	   $user_all_details= $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=> $auth_user_id)));	   
	   $this->set('player_details',$user_all_details);
	   $this->set('all_player_user_data', $all_player_user_data);
	   $this->set('auth_user_id', $auth_user_id);
	   $this->set('login_user_id',  $this->Auth->user('id'));
	   
	   //echo '<pre>';
	  //print_r($user_all_details);
	   //exit;

	   //========================================== Coach Details==============================================
	   
	    $this->UserComment->bindModel(array(
		'belongsTo' => array('CoachDetail'=>array(
									 'className'=>'CoachDetail',
									 'conditions'   => 'CoachDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    => false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ) 
								)
				               )   						
		               );
			   
       $user_comment_coach= $this->UserComment->find('all',array('conditions'=>array('UserComment.player_id'=>$auth_user_id,'User.type'=>'C')));
    
	 //===================================================== Player Details =================================//
	 
	 
	  $this->UserComment->bindModel(array(
		'belongsTo' => array('PlayerDetail'=>array(
									 'className'=>'PlayerDetail',
									 'conditions'   => 'PlayerDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ) 
								)
				               )   						
		               );
	
	  $user_comment_player= $this->UserComment->find('all',array('conditions'=>array('UserComment.player_id'=> $auth_user_id,'User.type'=>'P')));
	  
	  //===================================Family Details  =======================================//
	  
	    $this->UserComment->bindModel(array(
		'belongsTo' => array('FamilyDetail'=>array(
									 'className'=>'FamilyDetail',
									 'conditions'   => 'FamilyDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ) 
								),
				             )  
							);   								               	
	  $user_comment_family= $this->UserComment->find('all',array('conditions'=>array('UserComment.player_id'=> $auth_user_id,'User.type'=>'F')));    
	  $total_array= array_merge($user_comment_coach,  $user_comment_player, $user_comment_family);
	 
	  $all_result=array();	  
	  $i=0;
	  foreach($total_array as  $total_arrays)
	  {
			  if($total_arrays['User']['type']=='P')
			  {
				 $all_result[$i]['image']='uploads/profiles/player/profile_pic/'.$total_arrays['PlayerDetail']['profile_img'];
				 $all_result[$i]['UserComment']=$total_arrays['UserComment']['comment'];
				 $all_result[$i]['post_date']=$total_arrays['UserComment']['post_date'];
				 $all_result[$i]['name']=$total_arrays['User']['first_name'].' '.$total_arrays['User']['last_name'];
				
			}else if($total_arrays['User']['type']=='F')
			{
				
				//===================family Relation=======================//
				
		$this->PlayerRelationship->bindModel(array(
			   'hasOne' => array(
								'FamilyPlayerRelationship'=>array(
									 'className'=>'FamilyPlayerRelationship',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'relation_id',
									 'fields'=>''
								)
							)
						)	
		            );
		 
          $family_player_relation_array=$this->PlayerRelationship->find('first',array('conditions'=>array('FamilyPlayerRelationship.player_id'=>$id)));		 		

				
				//==========================================================//
				
				 $all_result[$i]['image']='uploads/profiles/family/profile_pic/'.$total_arrays['FamilyDetail']['profile_img'];
				 $all_result[$i]['UserComment']=$total_arrays['UserComment']['comment'];
				 $all_result[$i]['post_date']=$total_arrays['UserComment']['post_date'];
				   if($family_player_relation_array['PlayerRelationship']['relationship']=='Son')
				   {
					 $family_player_relation_array['PlayerRelationship']['relationship']='Father';  
					   
				   }
				      //$all_result[$i]['name']=$family_player_relation_array['PlayerRelationship']['relationship'];
					   $all_result[$i]['name']=$total_arrays['User']['first_name'].' '.$total_arrays['User']['last_name'];
				
			}else
			{
				  $all_result[$i]['image']='uploads/profiles/coach/profile_pic/'.$total_arrays['CoachDetail']['profile_img'];
				  $all_result[$i]['UserComment']=$total_arrays['UserComment']['comment'];
				  $all_result[$i]['post_date']=$total_arrays['UserComment']['post_date'];
				  $all_result[$i]['name']=$total_arrays['User']['first_name'].' '.$total_arrays['User']['last_name'].'(Coach)';
			}	 
	 
	   $i++;	
	}
	
	  //==================Video=================================================================

	  
        $conditions_v= array(array("PlayerVideo.user_id"=>$auth_user_id));	
		$all_video_data= $this->PlayerVideo->find('all',array('conditions' =>$conditions_v));
		$new_video_array_list=array();
		foreach((array) $all_video_data as  $all_video_datas)
		{			
			$new_video_array_list[$all_video_datas['PlayerVideo']['video_position']]=$all_video_datas['PlayerVideo'];			
		}			
		$this->set('all_video_data', $new_video_array_list);
		
		//====================================================================================//
		//===============Player Achievement=====================================================//
	    $this->AchiveCertificate->bindModel(array(
			   'belongsTo' => array(
								'Level'=>array(
									 'className'=>'Level',
									 'conditions'   => 'Level.id=AchiveCertificate.certificate_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'PlayerPosition.id=AchiveCertificate.position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );	 
	   $all_achive_certificate_attack= $this->AchiveCertificate->find('all',array('conditions'=>array('AchiveCertificate.player_id'=>$auth_user_id,'AchiveCertificate.category_id'=>1)));
	     $this->AchiveCertificate->bindModel(array(
			   'belongsTo' => array(
								'Level'=>array(
									 'className'=>'Level',
									 'conditions'   => 'Level.id=AchiveCertificate.certificate_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'PlayerPosition.id=AchiveCertificate.position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );	
       $all_achive_certificate_defence= $this->AchiveCertificate->find('all',array('conditions'=>array('AchiveCertificate.player_id'=>$auth_user_id,'AchiveCertificate.category_id'=>2)));
	     $this->AchiveCertificate->bindModel(array(
			   'belongsTo' => array(
								'Level'=>array(
									 'className'=>'Level',
									 'conditions'   => 'Level.id=AchiveCertificate.certificate_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'PlayerPosition.id=AchiveCertificate.position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );	
       $all_achive_certificate_mental= $this->AchiveCertificate->find('all',array('conditions'=>array('AchiveCertificate.player_id'=>$auth_user_id,'AchiveCertificate.category_id'=>3)));
	     $this->AchiveCertificate->bindModel(array(
			   'belongsTo' => array(
								'Level'=>array(
									 'className'=>'Level',
									 'conditions'   => 'Level.id=AchiveCertificate.certificate_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'PlayerPosition.id=AchiveCertificate.position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );	
	   $all_achive_certificate_phycalconditions= $this->AchiveCertificate->find('all',array('conditions'=>array('AchiveCertificate.player_id'=>$auth_user_id,'AchiveCertificate.category_id'=>4)));
	  
	
	   
	   $this->set('all_achive_certificate_attack', $all_achive_certificate_attack);
       $this->set('all_achive_certificate_defence', $all_achive_certificate_defence);
       $this->set('all_achive_certificate_mental', $all_achive_certificate_mental);
       $this->set('all_achive_certificate_phycalconditions', $all_achive_certificate_phycalconditions);	
	   
	   //============All Comment data=============================================
	        $this->set('all_comment_data', $all_result);	
	  //==================All Age Group and Position==============================
	  
		$all_player_positions=$this->PlayerPosition->find('list');
		$AgeGroup=$this->AgeGroup->find('list');
	    $this->set('all_player_positions', $all_player_positions);
	    $this->set('AgeGroup', $AgeGroup);
		
		//=======================================================================

}

	Public function player_list()
	{		
		$country=$this->Country->get_country();
		$club=$this->Club->get_club();
		$player_position=$this->PlayerPosition->get_player_position();
		//pr($player_position);			
		$this->set('country',$country);
		$this->set('club',$club);
		$this->set('player_position',$player_position);
		
		$conditions_p= array(array("User.type"=>"P","User.status"=>"A"));

		$all_player_details= $this->PlayerDetail->find('all',array('conditions' =>$conditions_p,'fields'=>array('PlayerDetail.*','User.id', 'User.email_address','User.first_name','User.last_name','PrimaryPosition.id','PrimaryPosition.name','SecondaryPosition.id','SecondaryPosition.name','Country.id','Country.country_name','Club.id','Club.name','Club.logo'),'limit'=>15, 'offset'=>0));


       		$all_player_total= $this->PlayerDetail->find('count',array('conditions' =>$conditions_p,'fields'=>array('PlayerDetail.*','User.id', 'User.email_address','User.first_name','User.last_name','PrimaryPosition.id','PrimaryPosition.name','SecondaryPosition.id','SecondaryPosition.name','Country.id','Country.country_name','Club.id','Club.name','Club.logo')));
		
                $this->set('all_player_total', $all_player_total);
                $this->set('all_player_details', $all_player_details);
	}
	
	public function player_list_ajax()
	{
		$this->layout="ajax";
	        $sql="";

		//Pr($this->data);
		//exit();

		$last_id=$this->data['last_id'];
        $limit=$this->data['limit'];
		
		//$conditions_p= array(array("User.type"=>"P","User.status"=>"A"));
		$conditions_p = "User.type = 'P' and User.status = 'A' ";
		$fields=array('PlayerDetail.*','User.id', 'User.email_address','User.first_name','User.last_name','PrimaryPosition.id','PrimaryPosition.name','SecondaryPosition.id','SecondaryPosition.name','Country.id','Country.country_name','Club.id','Club.name','Club.logo');
		
		//====================== All condition checking   ======================================
		
		if(isset($this->request->data['position'])&& $this->request->data['position']!="")
		{
			$position_id=$this->request->data['position'];			
			$conditions_p.=" AND PlayerDetail.primary_position_id='".$position_id."'";
		}
		if(isset($this->request->data['club_id'])&& $this->request->data['club_id']!="")
		{
			$club_id=$this->request->data['club_id'];			
			$conditions_p.=" AND PlayerDetail.club_id='".$club_id."'";
		}
		if(isset($this->request->data['nationality'])&& $this->request->data['nationality']!="")
		{
			$nationality=$this->request->data['nationality'];			
			$conditions_p.=" AND PlayerDetail.nationality='".$nationality."'";
		}
		if(isset($this->request->data['search_box'])&& $this->request->data['search_box']!="")
		{
			$search_text=$this->request->data['search_box'];			
			$conditions_p.=" AND( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			$conditions_p.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
			$conditions_p.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			$conditions_p.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) ";			
		}		
		$all_player_details= $this->PlayerDetail->find('all',array('conditions' =>$conditions_p,'fields'=>$fields,'limit'=>$limit, 'offset'=>$last_id));
		$this->set('all_player_details', $all_player_details);
	}

	/*************************** Player Name Search Auto Complete  *************************************/
	public function auto_complate_name()
	{
		$this->layout = "ajax";
		$search_text= $this->data['keyword'];
		$sql = "";    
		if(!empty($this->request->data))
		{
			$sql.="( User.first_name LIKE '". $search_text."%'";    
			$sql.=" OR User.last_name LIKE '". $search_text."%' )";
			$sql.=" AND User.type ='P'";	

			$all_search_data= $this->User->find('all',array('conditions'=>$sql,'recursive'=>-1));
			//pr($all_search_data);
			$full_html="";
			if(!empty( $all_search_data))
			{
				$full_html.='<ul id="player-list">'; 
				foreach($all_search_data as $all_search_datas) 
				{   $name=$all_search_datas['User']['first_name'].' '.$all_search_datas['User']['last_name'];
					$full_html.="<li onClick=\"selectPlayer('".$name."')\">".$all_search_datas['User']['first_name'].' '.$all_search_datas['User']['last_name']."</li>";
				}
				$full_html.='</ul>';
				echo $full_html;
			}
		}	
		exit;
	}	
  
	/************************ Player Search auto complete  *********************************************/
	
	public function player_list_auto_complate()
	{
		$this->layout="ajax";
	    $sql="";
		//pr($this->data);
		if(isset($this->data['search_box'])&& $this->data['search_box']!="")
		{
			$search_text=$this->request->data['search_box'];
			//$sql.="'".REPLACE(CONCAT('User.fname', 'User.lname',' User.fname'), ' ', '')."'" LIKE '%" . 'str_replace(' ', '', protect($search_text)) . "'"%'; 
			$sql.="( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			$sql.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
			$sql.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			$sql.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) "; 			 
			//$sql.=" OR User.last_names LIKE '". $search_text."%' )";
			$sql.=" AND User.type ='P'";
			$sql.=" AND User.status =1";
			
			//echo  $sql;
			$all_player_details= $this->PlayerDetail->find('all',array('conditions' =>$sql));
		}else
		{
			
			$all_player_details= $this->PlayerDetail->find('all',array('conditions'=>array('User.type'=>'P','User.status'=>1),'order'=>array('PlayerDetail.id ASC')));
			
		}	
		//pr($all_player_details);		
		$this->set('all_player_details', $all_player_details);      		
	}
	
	//============================= Player List drop down search ===============================
	function player_list_by_select()
	{
		//pr($this->request->data);		
		$this->layout='ajax';		
		//$sql="User.type ='P' ";
		$sql = "User.type = 'P' and User.status = 'A' ";
		$last_id=$this->request->data['last_id'];
		$limit=$this->request->data['limit'];
		$fields=array('PlayerDetail.*','User.id', 'User.email_address','User.first_name','User.last_name','PrimaryPosition.id','PrimaryPosition.name','SecondaryPosition.id','SecondaryPosition.name','Country.id','Country.country_name','Club.id','Club.name','Club.logo');		
		if(isset($this->request->data['position'])&& $this->request->data['position']!="")
		{
			$position_id=$this->request->data['position'];			
			$sql.=" AND PlayerDetail.primary_position_id='".$position_id."'";
		}
		if(isset($this->request->data['club_id'])&& $this->request->data['club_id']!="")
		{
			$club_id=$this->request->data['club_id'];			
			$sql.=" AND PlayerDetail.club_id='".$club_id."'";
		}
		if(isset($this->request->data['nationality'])&& $this->request->data['nationality']!="")
		{
			$nationality=$this->request->data['nationality'];			
			$sql.=" AND PlayerDetail.nationality='".$nationality."'";
		}
		if(isset($this->request->data['search_box'])&& $this->request->data['search_box']!="")
		{
			$search_text=$this->request->data['search_box'];			
			$sql.=" AND( CONCAT(User.first_name, '', User.last_name) LIKE '%" . $search_text . "%'" ;
			$sql.="OR CONCAT(User.last_name, '', User.first_name) LIKE '%" . $search_text . "%'";
			$sql.= "OR CONCAT(User.first_name, ' ', User.last_name) LIKE '%" . $search_text . "%'" ;
			$sql.="OR CONCAT(User.last_name, ' ', User.first_name) LIKE '%" . $search_text . "%' ) ";			
		}
		//echo $sql;
		$all_player_details= $this->PlayerDetail->find('all',array('conditions' =>$sql,'fields'=>$fields,'limit'=>$limit, 'offset'=>$last_id));
		//pr($all_player_details);
		$this->set('all_player_details', $all_player_details);
	}

	//==========================================================================================
		
	
	public function add_player_video()
	{
	   $video_url= $this->request->data['URL'];
	   $user_id= $this->request->data['USERID'];
	   $video_position= $this->request->data['position'];
	   $id=$this->request->data['last_id'];
	   $url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
	   $youtube_data = json_decode(file_get_contents($url), true);		                                              
	   $player_details['PlayerVideo']['user_id'] = $user_id;
	   $player_details['PlayerVideo']['video_link'] = $video_url;
	   $player_details['PlayerVideo']['video_iframe'] = $youtube_data['html'];
	   $player_details['PlayerVideo']['video_title'] = $youtube_data['title'];
	   $player_details['PlayerVideo']['video_image'] = $youtube_data['thumbnail_url'];
	   $player_details['PlayerVideo']['video_position'] = $video_position;	
	   $player_details['PlayerVideo']['video_type'] = 0;
	   $count_exit_video= $this->PlayerVideo->find('first',array('conditions'=>array('PlayerVideo.video_position'=>$video_position,'PlayerVideo.user_id'=>$user_id)));					  
	   if(count($count_exit_video)>0)	
		{
		   $last_id=  $count_exit_video['PlayerVideo']['id'];
		   $this->PlayerVideo->id=$last_id;
		   $this->PlayerVideo->save($player_details);
	    }
	   else
		{
			$this->PlayerVideo->save($player_details);
			$last_id=$this->PlayerVideo->getLastInsertId();	
		}
	   $conditions_c= array(array("PlayerVideo.id"=>$last_id));	
	   $all_video= $this->PlayerVideo->find('first',array('conditions' =>$conditions_c));
	   echo json_encode($all_video['PlayerVideo']);
	   exit;		   
	}
	
	
  public function forgot_password()
  {	
     if(!empty( $this->request->data))
	 {
		  $email= $this->request->data['User']['email_address'];
		  $conditions_c= array(array("User.type"=>'P',"User.email_address"=> $email));	
		  $all_coach_details= $this->User->find('first',array('conditions' =>$conditions_c,'recursive'=>-1));

		  if(count($all_coach_details)>0)
		  {
			  
					   /* $this->Email->to =  $email; 
						$this->Email->subject = "Forgot Password";
						$this->Email->from = Configure::read('admin_email');
						$this->Email->layout = '';
						$this->Email->template = 'create_new_password_player';
						$this->Email->sendAs = 'html';		
						$this->set('all_coach_details', $all_coach_details);
						$this->Email->send();*/
					
					    App::uses('CakeEmail', 'Network/Email');
						$this->Email = new CakeEmail('smtp');						
						$this->Email->to(array($email));
						$this->Email->subject('Forgot Password');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');						
						$this->Email->viewVars(array('all_coach_details'=>$all_coach_details));
						$this->Email->template('create_new_password_player');
						$this->Email->send();
					
			       // $this->Session->setFlash('Verification link sent to'.$email,array('class'=>'alert alert-success alert-dismissable'));
					 $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable">Verification link sent to '.$email.'</div>',array('class'=>'alert alert-success alert-dismissable')));
			 
					  
		  }else {
	       //$this->Session->setFlash('You are not registered with us. Please sign up to create an account.',array('class'=>'alert alert-danger alert-dismissable')); 
           $this->Session->setFlash(__('<div class="alert alert-info alert-dismissable">You are not registered with us. Please sign up to create an account.</div>',array('class'=>'alert alert-success alert-dismissable')));
			 			
	     } 
		
	 }

  }
  
  public function create_new_password($user_id=0)
  {
	 // Configure::write('debug', 2);
	  $id= base64_decode($user_id);
	  $this->set('param_id', $id); 
	 
      if(!empty($this->request->data))
	  {
		  $password=$this->request->data['User']['password'];
		  $cpassword=$this->request->data['User']['cpassword'];
		  
		  if($password==$cpassword)
		  {
			 $password=$this->Auth->password($this->request->data['User']['password']);
			 $this->request->data['User']['password']=$password;
			 
			
			 $this->User->id= $this->request->data['User']['forget_user_id'];
			 $this->User->saveField('password', $password);
			 
				
				 $this->Session->setFlash(__('<div class="alert alert-success alert-dismissable">Your password updated successfully</div>','default',array('class'=>'alert alert-success alert-dismissable')));
				 $this->redirect(array('controller'=>'players','action'=>'login'));    			
				 
			
			  
		  }else
		  {		  
		    
              $this->Session->setFlash(__('<div class="alert alert-danger alert-dismissable">password and confirmPassword is not match please write correctly.</div>','default',array('class'=>'alert alert-success alert-dismissable')));
               $this->redirect(array('controller'=>'coaches','action'=>'create_new_password/'.base64_encode($this->request->data['User']['forget_user_id']))); 			  
	     }	  
	  
	  }
  }
  
  public function edit_profile()
  {
	    $country = $this->Country->find('list',array('fields'=>array('Country.id','Country.country_name')));
		$club=$this->Club->get_club();
		$player_position=$this->PlayerPosition->get_player_position();
		$this->set('country',$country);
		$this->set('club',$club);
		$this->set('player_position',$player_position);
		$age_group=$this->AgeGroup->find('list');
		$this->set('age_group',$age_group);
		
	 //===============================================================================//		
		   $id=$this->Auth->user('id');
		 
		   $get_data= $this->PlayerDetail->find('first',array('conditions'=>array('User.id'=>$id)));
		  // configure::write('debug', 2);
		  // pr($get_data);
		  // exit;
		   $this->set('get_edit_data',$get_data);
		   
	  //===========================Coach Certificates=====================================//
		   
		   $get_data_Certificates= $this->CoachCertificate->find('all',array('conditions'=>array('CoachCertificate.user_id'=>$id),'fields'=>'CoachCertificate.certificate_id'));
		   
		//====================when submit for player profile=====================================//
		
		if($this->request->is('post'))
		{		
			    $check_email = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['email_address'])));			
				$player_details['User']['first_name'] = $this->request->data['first_name'];
				$player_details['User']['last_name'] = $this->request->data['last_name'];
				$player_details['User']['email_address'] = $this->request->data['email_address'];				
				$player_details['User']['type'] = 'P';
				
				$player_details['PlayerDetail']['height'] = $this->request->data['height'];
				$player_details['PlayerDetail']['weight'] = $this->request->data['weight'];
				$player_details['PlayerDetail']['dob'] = $this->request->data['dob'];
				$player_details['PlayerDetail']['bench_press'] = $this->request->data['bench_press'];
				$player_details['PlayerDetail']['20m_sprint'] = $this->request->data['20m_sprint'];
				$player_details['PlayerDetail']['60m_sprint'] = $this->request->data['60m_sprint'];
				$player_details['PlayerDetail']['dead_lift'] = $this->request->data['dead_lift'];
				$player_details['PlayerDetail']['primary_position_id'] = $this->request->data['primary_position_id'];
				$player_details['PlayerDetail']['secondary_position_id'] = $this->request->data['secondary_position_id'];
				$player_details['PlayerDetail']['nationality'] = $this->request->data['nationality'];
				$player_details['PlayerDetail']['club_id'] = $this->data['club_id'];
				$player_details['PlayerDetail']['age_group_id'] = $this->request->data['age_group_id'];
				
				
				$this->User->id=$this->Auth->user('id');
				
				
				
				
				if($this->User->save($player_details)){
		
				    $get_player_id= $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$this->Auth->user('id'))));
					
						if(isset($_FILES['profile_img']['name']) && $_FILES['profile_img']['name']!="")
						{
							
							$foo = new Upload($_FILES['profile_img']); 
							  $foo->allowed = array('image/jpg/jpeg');
								if ($foo->uploaded) {
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/original/');
									   $foo->image_resize          = true;
								if($foo->image_dst_x>$foo->image_dst_y)
								   {   
									   $foo->image_x               = 357;
										$foo->image_ratio_y              = true;
		
								   }else if($foo->image_dst_x<$foo->image_dst_y)
								   {
									   $foo->image_y                = 439;
									   $foo->image_ratio_x          = true;
									   
								   }else if($foo->image_dst_x==$foo->image_dst_y)
								   {
									   $foo->image_ratio_y         = true;
									   $foo->image_x               = 357;
									   
								   }
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/profile_pic/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 160;
									   $foo->image_x               = 130;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/thumb/');
									  if ($foo->processed) {
											$player_details['PlayerDetail']['profile_img'] = $foo->file_dst_name;
												@unlink('uploads/profiles/player/thumb/'.$get_player_id['PlayerDetail']['profile_img']);
												@unlink('uploads/profiles/player/original/'.$get_player_id['PlayerDetail']['profile_img']);
												@unlink('uploads/profiles/player/profile_pic/'.$get_player_id['PlayerDetail']['profile_img']);
				           
									  }
								   }
							   }
				        	
					    
					$player_id= $get_player_id['PlayerDetail']['id'];
					$this->PlayerDetail->id=$player_id;	
					if($this->PlayerDetail->save($player_details)){
						$this->Session->setFlash(__(('<span class="alert alert-success ">Your Profile has been successfully updated.</span>')));
						$this->redirect(array('controller'=>'players','action'=>'player_profile'));
						
					}else{
						
						$this->set('message','<span class="alert alert-danger">Unable To Creating Your Account.</span>');
					}
				}
			
		} 	  
  }
  
 public function insert_comment()
  {
	$this->layout = "ajax";
	//$this->render(false);		
	$palyer_comment=array();
	$palyer_comment['UserComment']['player_id']= $this->request->data['player_id'];
	$palyer_comment['UserComment']['user_id']= $this->Auth->user('id');
	$palyer_comment['UserComment']['comment']= $this->request->data['commentText'];
	$palyer_comment['UserComment']['post_date']= date('Y-m-d H:i:s');	
	$this->UserComment->save($palyer_comment);
    $last_user_id=$this->UserComment->getLastInsertId();
	
	if($this->Auth->user('type')=='P')
	{   
	   $this->UserComment->bindModel(array(
			   'hasOne' => array(
								 'playerDetail'=>array(
									 'className'=>'playerDetail',
									 'conditions'   => 'playerDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
		
		
	}else if($this->Auth->user('type')=='F')
	{
	   $this->UserComment->bindModel(array(
		'hasOne' => array(
								 'FamilyDetail'=>array(
									 'className'=>'FamilyDetail',
									 'conditions'   => 'FamilyDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								  'PlayerRelationship'=>array(
									 'className'=>'PlayerRelationship',
									 'conditions'   => array('UserComment.player_id'=>$this->request->data['player_id']),
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'id',
									 'fields'=>''
								)
				               )   
							)
		               );						
	}else{
		
		 $this->UserComment->bindModel(array(
		'hasOne' => array(
								 'CoachDetail'=>array(
									 'className'=>'CoachDetail',
									 'conditions'   => 'CoachDetail.user_id=UserComment.user_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               )   
							)
		               );
				}
	
	$user_comment= $this->UserComment->find('first',array('conditions'=>array('UserComment.id'=> $last_user_id)));
    $all_result=array();
	
if($this->Auth->user('type')=='F')
{
 $this->PlayerRelationship->bindModel(array(
			   'hasOne' => array(
								'FamilyPlayerRelationship'=>array(
									 'className'=>'FamilyPlayerRelationship',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'relation_id',
									 'fields'=>''
								)
							)
						)	
		            );
		 
    $family_player_relation=$this->PlayerRelationship->find('first',array('conditions'=>array('FamilyPlayerRelationship.player_id'=>$this->request->data['player_id'])));		 		
}		
	
	
    if($this->Auth->user('type')=='P')
	{
		 $all_result['image']='uploads/profiles/player/profile_pic/'.$user_comment['playerDetail']['profile_img'];
		 $all_result['UserComment']=$user_comment['UserComment']['comment'];
		 $all_result['post_date']=$user_comment['UserComment']['post_date'];
		 $all_result['name']=$user_comment['User']['first_name'].' '.$user_comment['User']['last_name'];
		
	}else if($this->Auth->user('type')=='F')
	{
		
		          if($family_player_relation['PlayerRelationship']['relationship']=='Son')
				   {
					 $family_player_relation['PlayerRelationship']['relationship']='Father';  
					   
				   }
		 $all_result['image']='uploads/profiles/family/profile_pic/'.$user_comment['FamilyDetail']['profile_img'];
		 $all_result['UserComment']=$user_comment['UserComment']['comment'];
		 $all_result['post_date']=$user_comment['UserComment']['post_date'];
		// $all_result['name']=$family_player_relation['PlayerRelationship']['relationship'];
		$all_result['name']=$user_comment['User']['first_name'].' '.$user_comment['User']['last_name'];
		
		
	}else
	{
		  $all_result['image']='uploads/profiles/coach/profile_pic/'.$user_comment['CoachDetail']['profile_img'];
		  $all_result['UserComment']=$user_comment['UserComment']['comment'];
		  $all_result['post_date']=$user_comment['UserComment']['post_date'];
		  $all_result['name']=$user_comment['User']['first_name'].' '.$user_comment['User']['last_name'].'(Coach)';
	}  
	$this->set('commentDetails', $all_result);
	
  }

   public function get_graph_value_by_age_group()
	{
		$this->layout="";				
		
		$age_group_id =$this->request->data['Group_id'];
		$position_id=$this->request->data['position_id'];
		$id =$this->request->data['player_id'];
		
		$all_rating_player_attack =array();
		$all_rating_player_deffence=array();
		$all_rating_player_Condition=array();
		$all_rating_player_mindset=array();
	
	     $data_exits_or_nor=$this->AddPlayerRating->find('count',array('conditions'=>array('AddPlayerRating.player_id'=>$id)));
		
		 if( $data_exits_or_nor>0)
		 {
			 $rating_count_by_group_position=$this->AddPlayerRating->find('count',array('conditions'=>array('AddPlayerRating.age_group_id'=>$age_group_id,'AddPlayerRating.position_id'=>$position_id,'AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>1)));
			
			if( $rating_count_by_group_position>0)
			{	
			 
				$all_rating_player_attack= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.age_group_id'=>$age_group_id,'AddPlayerRating.position_id'=>$position_id,'AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>1)));
				$all_rating_player_deffence= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.age_group_id'=>$age_group_id,'AddPlayerRating.position_id'=>$position_id,'AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>2)));
				$all_rating_player_Condition= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.age_group_id'=>$age_group_id,'AddPlayerRating.position_id'=>$position_id,'AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>4)));
				$all_rating_player_mindset= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.age_group_id'=>$age_group_id,'AddPlayerRating.position_id'=>$position_id,'AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>3)));
				
			}else
			{
				
				 $all_rating_player_attack1= $this->CapabilityMatrixAge->find('first',array('conditions'=>array('CapabilityMatrixAge.age_group_id'=>$age_group_id,'CapabilityMatrixAge.player_position_id'=>$position_id)));	
			 if( !empty($all_rating_player_attack1))
			 {
				 $all_rating_player_attack['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
				 $all_rating_player_deffence['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
				 $all_rating_player_Condition['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
				 $all_rating_player_mindset['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
				 
				 $all_rating_player_attack['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
				 $all_rating_player_deffence['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
				 $all_rating_player_Condition['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
				 $all_rating_player_mindset['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
				 
					$all_rating_player_attack['AddPlayerRating']['position_id']=$position_id;
					$all_rating_player_attack['AddPlayerRating']['age_group_id']=$age_group_id;
			 }
				
				
			}
			
		 }else
		 {	
             $all_rating_player_attack1= $this->CapabilityMatrixAge->find('first',array('conditions'=>array('CapabilityMatrixAge.age_group_id'=>$age_group_id,'CapabilityMatrixAge.player_position_id'=>$position_id)));	
			 if( !empty($all_rating_player_attack1))
			 {
			 $all_rating_player_attack['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_deffence['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_Condition['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_mindset['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 
			 $all_rating_player_attack['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_deffence['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_Condition['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_mindset['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 
			    $all_rating_player_attack['AddPlayerRating']['position_id']=$position_id;
			    $all_rating_player_attack['AddPlayerRating']['age_group_id']=$age_group_id;
			 }
		 }	 
        
		 
		$this->set('all_rating_coach_attack',$all_rating_player_attack);
		$this->set('all_rating_coach_deffence',$all_rating_player_deffence);
		$this->set('all_rating_coach_Condition',$all_rating_player_Condition);
		$this->set('all_rating_coach_mindset',$all_rating_player_mindset);
				
	}
	
	 public function get_graph_value_by_current()
	{
		$this->layout="";						
		
		$id =$this->request->data['player_id'];		
		$position_and_group=$this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$id)));		
		$data_exits_or_nor=$this->AddPlayerRating->find('count',array('conditions'=>array('AddPlayerRating.player_id'=>$id)));
	
	
	     if( $data_exits_or_nor>0)
		 {
	
			$all_rating_player_attack= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>1),'order'=>'AddPlayerRating.id DESC'));		
			$all_rating_player_deffence= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>2),'order'=>'AddPlayerRating.id DESC'));
			$all_rating_player_Condition= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>4),'order'=>'AddPlayerRating.id DESC'));
			$all_rating_player_mindset= $this->AddPlayerRating->find('first',array('conditions'=>array('AddPlayerRating.player_id'=>$id,'LevelCategory.id'=>3),'order'=>'AddPlayerRating.id DESC'));		
		 }else
		 {
			 
			 $all_rating_player_attack1= $this->CapabilityMatrixAge->find('first',array('conditions'=>array('CapabilityMatrixAge.age_group_id'=>$position_and_group['PlayerDetail']['age_group_id'],'CapabilityMatrixAge.player_position_id'=>$position_and_group['PlayerDetail']['primary_position_id'])));
			 
			 //pr( $all_rating_player_attack1);
			// exit;
			 $all_rating_player_attack['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_deffence['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_Condition['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 $all_rating_player_mindset['AddPlayerRating']['lower_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['lower_limit'];
			 
			 $all_rating_player_attack['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_deffence['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_Condition['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
			 $all_rating_player_mindset['AddPlayerRating']['upper_limit'] =$all_rating_player_attack1['CapabilityMatrixAge']['upper_limit'];
		
			 $all_rating_player_attack['AddPlayerRating']['position_id']=$position_and_group['PlayerDetail']['primary_position_id'];
			 $all_rating_player_attack['AddPlayerRating']['age_group_id']=$position_and_group['PlayerDetail']['age_group_id'];
		 } 
		 
		  //echo '<pre>';
		 //print_r($all_rating_coach_attack);
		// exit;
			$this->set('all_rating_coach_attack',$all_rating_player_attack);
			$this->set('all_rating_coach_deffence',$all_rating_player_deffence);
			$this->set('all_rating_coach_Condition',$all_rating_player_Condition);
			$this->set('all_rating_coach_mindset',$all_rating_player_mindset);
		
		//=====================================================================//
		
		
		$all_player_positions=$this->PlayerPosition->find('list');
		$AgeGroup=$this->AgeGroup->find('list');
	    $this->set('all_player_positions', $all_player_positions);
	    $this->set('AgeGroup', $AgeGroup);
				
	}
	
	public function insert_player_achivement()
	{		
		$this->layout = "ajax";		
		$palyer_achive=array();
		$palyer_achive['PlayerRecoardAchivement']['player_id']= $this->Auth->user('id');
		$palyer_achive['PlayerRecoardAchivement']['achivement_title']= $this->request->data['acivement_title'];
		$palyer_achive['PlayerRecoardAchivement']['achivement_desc']= $this->request->data['acivement_title_desc'];
		$palyer_achive['PlayerRecoardAchivement']['achivement_date']= $this->request->data['datetimepicker1'];
		//pr($this->request->data);
		$this->PlayerRecoardAchivement->save($palyer_achive);
		echo $this->PlayerRecoardAchivement->getLastInsertID();
		exit;
  
	}
	
	public function delete_player_achivement()
	{		
		$this->layout = "ajax";
		$this->render(false);
		if($this->request->data){
			$this->PlayerRecoardAchivement->delete($this->request->data['id']);
		}
  
	}
	
	public function upload_video()
	{
          if(isset($_FILES['own_video']['name']) && $_FILES['own_video']['name']!="")
						{
							
							$member_id_video=$this->request->data['member_id_video'];
							$coustom_video=$this->request->data['coustom_video'];
							
							
							$foo = new Upload($_FILES['own_video']); 
							  //$foo->allowed = array('image/jpg/jpeg');
								if ($foo->uploaded) {
									  // $foo->allowed = array('image/*');
									   $foo->Process('uploads/video/player/');
									
							
							if ($foo->processed)
								{                              
								   $player_details['PlayerVideo']['user_id'] = $member_id_video;
								   $player_details['PlayerVideo']['video_link'] = $foo->file_dst_name;
								   $player_details['PlayerVideo']['video_iframe'] = $this->request->data['video_title'];
								   $player_details['PlayerVideo']['video_title'] = $this->request->data['video_title'];
								   $player_details['PlayerVideo']['video_image'] = $foo->file_dst_name;
								   $player_details['PlayerVideo']['video_position'] = $coustom_video;
								   $player_details['PlayerVideo']['video_type'] = 1;
								   								
								  $count_exit_video= $this->PlayerVideo->find('first',array('conditions'=>array('PlayerVideo.video_position'=>$coustom_video,'PlayerVideo.user_id'=>$member_id_video)));                              // echo '<pre>';
								//print_r($count_exit_video);								//exit;
								   if(count($count_exit_video)>0)
								   {
											$last_id=  $count_exit_video['PlayerVideo']['id'];  
											$this->PlayerVideo->id=$last_id;
											$this->PlayerVideo->save($player_details);
									}else
									{
											$this->PlayerVideo->save($player_details);
											$last_id=$this->PlayerVideo->getLastInsertId();
									}					
									  }
								    }
							     }
				$this->redirect(array('controller'=>'players','action'=>'player_profile'));	   
	}	
	//===============================================================================//
	/************************  Add player top up **************************************/
	function add_topup()
	{
		//pr($this->data);
		$player_id=$this->data['player_id_hidd'];
		$top_up_amount=$this->data['top_up_amount'];
		$description_topup=$this->data['description_topup'];
		
		$family_member_id = $this->Session->read('Auth.Member.id');		
		$top_up['PlayerTopup']['family_member_id']= $family_member_id;
		$top_up['PlayerTopup']['player_id']= $player_id;
		$top_up['PlayerTopup']['amount']= $top_up_amount;
		$top_up['PlayerTopup']['description_topup']= $description_topup;
		$top_up['PlayerTopup']['payment_date']= date('Y-m-d H:i:s');
		$top_up['PlayerTopup']['payment_status']= 1;
		$this->PlayerTopup->save($top_up);
		$order_id=$this->PlayerTopup->id;
		//==============================  Demo ====================
			             
		/*$accessCode    =  "9A591939";//value from migs payment gateway
		$merchantId    =  "TESTDEVPLACOM01";//value from migs payment gateway
		$amount =$top_up_amount;
		$unique_id = rand(999999,8988888888);//this is a sample random no	
		$testarr = array(
		"vpc_Amount" => $amount*100,//Final price should be multifly by 100
		"vpc_Currency" => 'AUD',
		"vpc_AccessCode" => $accessCode,//Put your access code here
		"vpc_Command"=> "pay",
		"vpc_Locale"=> "en",
		"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
		"vpc_Merchant"=> $merchantId,//Add your merchant number here
		"vpc_OrderInfo"=>$order_id,//this also better to be a unique number
		"vpc_ReturnURL"=> HTTP_SERVER_PATH."players/success",
		"vpc_Version"=> "1");
		ksort($testarr);
		$SECURE_SECRET =  "84712313F1FBE77FE54EF9101C779ABB";
		//value from migs payment gateway
		$securehash = $SECURE_SECRET;
		$url = "";
		foreach ($testarr as $key => $value)
		{
			$securehash .= $value;
			$url .= $key."=".urlencode($value)."&";
		}
		$securehash = md5($securehash);//Encoding
		$url .= "vpc_SecureHash=".$securehash;		
		header("location:https://migs.mastercard.com.au/vpcpay?$url");				
		exit();
		*/
		//======================For Live Enviourment =======================//
		
		  $SECURE_SECRET = "0B4737508BA899863FBC279A51422CB2";
		  $unique_id = rand(999999,8988888888);//this is a sample random no
		  $order = $order_id;
	
			$testarr = array(
			"vpc_Amount" => $top_up_amount*100,//Final price should be multifly by 100
			"vpc_Currency" => 'AUD',
			"vpc_AccessCode" => "A39B63F9",//Put your access code here
			"vpc_Command"=> "pay",
			"vpc_Locale"=> "en",
			"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
			"vpc_Merchant"=> "DEVPLACOM01",//Add your merchant number here
			"vpc_OrderInfo"=>$order_id,//this also better to be a unique number
			"vpc_ReturnURL"=> HTTP_SERVER_PATH."players/success",
			"vpc_Version"=> "1"
			);
		
			$md5HashData = $SECURE_SECRET;	
			ksort ($testarr);		
			$appendAmp = 0;
	        $vpcURL = "https://migs.mastercard.com.au/vpcpay" . "?";
			foreach($testarr as $key => $value)
			{
               // create the md5 input and URL leaving out any fields that have no value
				if (strlen($value) > 0) {					
					// this ensures the first paramter of the URL is preceded by the '?' char
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);						
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);						
					}
					$md5HashData .= $value;
				}
			}
			if (strlen($SECURE_SECRET) > 0) 
			{
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
			}			
			header("Location: ".$vpcURL);			
			exit();
			
		//================================End============================================//	

	}	
	//====================================================================================
	function success()
	{		
		App::uses('CakeEmail', 'Network/Email');
		$all_respone_by_comweb =$_GET;
		//pr($all_respone_by_comweb);
		//exit();
		//pr($all_respone_by_comweb);
		if($all_respone_by_comweb['vpc_Message']=='Approved')
		{
			$order_id = $all_respone_by_comweb['vpc_OrderInfo'];			
			$top_up['PlayerTopup']['id']= $order_id;
			$top_up['PlayerTopup']['payment_status']= 2;
			$top_up['PlayerTopup']['vpc_MerchTxnRef']= $all_respone_by_comweb['vpc_MerchTxnRef'];
			$top_up['PlayerTopup']['vpc_Merchant']= $all_respone_by_comweb['vpc_Merchant'];
			$top_up['PlayerTopup']['vpc_Message']= $all_respone_by_comweb['vpc_Message'];			
			$top_up['PlayerTopup']['vpc_BatchNo']= $all_respone_by_comweb['vpc_BatchNo'];
			$top_up['PlayerTopup']['vpc_TransactionNo']= $all_respone_by_comweb['vpc_TransactionNo'];
			$top_up['PlayerTopup']['vpc_ReceiptNo']= $all_respone_by_comweb['vpc_ReceiptNo'];
			$this->PlayerTopup->save($top_up);
		
			$this->PlayerTopup->bindModel(array(
			   'belongsTo' => array(
								'Family'=>array(
									 'className'	=>'User',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'	=>'family_member_id',
									 'fields'		=>array('Family.first_name','Family.last_name','Family.type','Family.email_address')
								),
								 'Player'=>array(
									 'className'	=>'User',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'	=>'player_id',
									 'fields'       =>array('Player.first_name','Player.last_name','Player.type','Player.email_address')
		                          )							     
				               )   
							)
		               );
					   
			
			$all_topup=$this->PlayerTopup->find('first',array('conditions'=>array('PlayerTopup.id'=>$all_respone_by_comweb['vpc_OrderInfo'])));	
			//pr($all_topup);	
			$this->set('all_topup',$all_topup);
			$player_id = $all_topup['PlayerTopup']['player_id'];
			$amount=$all_topup['PlayerTopup']['amount'];
			$condition="PlayerDetail.user_id='".$player_id."'";
			$player_topup=$this->PlayerDetail->find('first',array('conditions'=>$condition,'fields'=>array('PlayerDetail.id','PlayerDetail.user_id','PlayerDetail.topup_point')));	
			$topup_point=$player_topup['PlayerDetail']['topup_point'];
			
			$total_points=$topup_point + $amount;
			$con="PlayerTopup.id='".$all_respone_by_comweb['vpc_OrderInfo']."' and PlayerTopup.add_player_amount ='0'";
			$update_check=$this->PlayerTopup->find('first',array('conditions'=>$con));
			if(!empty($update_check))
			{
				$player_Array['PlayerDetail']['id']=$player_topup['PlayerDetail']['id'];
				$player_Array['PlayerDetail']['topup_point']=$total_points;
				$this->PlayerDetail->save($player_Array);
				
				$topuparray['PlayerTopup']['id']=$all_respone_by_comweb['vpc_OrderInfo'];
				$topuparray['PlayerTopup']['add_player_amount']='1';
				
				       App::uses('CakeEmail', 'Network/Email');
						$this->Email = new CakeEmail('smtp');
				if($this->PlayerTopup->save($topuparray))
				{
					   
					//==================== Player Email ====================
					if(!empty($all_topup['Player']['email_address']))
					{
						/*$this->Email->to = $all_topup['Player']['email_address'];
						$this->Email->subject = 'Added AUD '.$amount.' successfully';
						$this->Email->from = 'Develop A Player' .'<'.Configure::read('admin_email').'>';
						$this->Email->layout = '';
						$this->Email->sendAs = 'html';					
						$this->Email->template = 'walletpayment_to_user';
						$this->set('amount',$amount);
						$this->set('total_points',$total_points);	
						$this->set('txnId', $all_respone_by_comweb['vpc_MerchTxnRef']);					
						$this->Email->send(); */
						
						
						//================= Smtp Mail function ==================================
						
						//===================Player Email=========================//
						
						$this->Email->to(array($all_topup['Player']['email_address']));
						$this->Email->subject('Added AUD '.$amount.' successfully');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');						
						$this->Email->viewVars(array('amount'=>$amount,'total_points'=>$total_points,'txnId'=>$all_respone_by_comweb['vpc_MerchTxnRef']));
						$this->Email->template('walletpayment_to_user');				
					    $this->Email->send();		
						
					}
					
					//====================== Family Member Email ===================
					
					if(!empty($all_topup['Family']['email_address']))
					{
						/*$this->Email->to = $all_topup['Family']['email_address'];
						$this->Email->subject = 'Wallet payment successfully done';
						$this->Email->from = 'Develop A Player' .'<'.Configure::read('admin_email').'>';
						$this->Email->layout = '';
						$this->Email->sendAs = 'html';					
						$this->Email->template = 'walletpayment_to_family';
						$this->set('amount',$amount);
						$this->set('family_member',$all_topup['Family']['first_name']);
						$this->set('player_name',$all_topup['Player']['first_name']);							
						$this->set('txnId', $all_respone_by_comweb['vpc_MerchTxnRef']);					
						$this->Email->send();*/
						
						$this->Email->to(array($all_topup['Family']['email_address']));
						$this->Email->subject('Wallet payment successfully done');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');
						$this->Email->template('walletpayment_to_family');	
						$this->Email->viewVars(array('amount'=>$amount,'family_member'=>$all_topup['Family']['first_name'],'player_name'=>$all_topup['Player']['first_name'],'txnId'=>$all_respone_by_comweb['vpc_MerchTxnRef']));									
					    $this->Email->send();	
					}
					//====================  Admin Email =============================
						/*$this->Email->to = Configure::read('admin_email');
						$this->Email->subject = 'Wallet payment successfully done';
						$this->Email->from = 'Develop A Player' .'<'.Configure::read('admin_email').'>';
						$this->Email->layout = '';
						$this->Email->sendAs = 'html';					
						$this->Email->template = 'walletpayment_to_admin';
						$this->set('amount',$amount);
						$this->set('family_member',$all_topup['Family']['first_name']);
						$this->set('player_name',$all_topup['Player']['first_name']);							
						$this->set('txnId', $all_respone_by_comweb['vpc_MerchTxnRef']);					
						$this->Email->send(); */
						
						$this->Email->to(array(Configure::read('admin_email')));
						$this->Email->subject('Wallet payment successfully done');						
						$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));						
						$this->Email->emailFormat('html');
						$this->Email->template('walletpayment_to_admin');	
						$this->Email->viewVars(array('amount'=>$amount,'family_member'=>$all_topup['Family']['first_name'],'player_name'=>$all_topup['Player']['first_name'],'txnId'=>$all_respone_by_comweb['vpc_MerchTxnRef']));									
					    $this->Email->send();
						
				}				
			}
		}
		//exit;
	}	
	//==============================================================================================
	
	function test_email()
	{ 
		  $SECURE_SECRET = "0B4737508BA899863FBC279A51422CB2";
		  $unique_id = rand(999999,8988888888);//this is a sample random no
		  $order = rand(10,100);
	
			$testarr = array(
			"vpc_Amount" => 1000,//Final price should be multifly by 100
			"vpc_Currency" => 'AUD',
			"vpc_AccessCode" => "A39B63F9",//Put your access code here
			"vpc_Command"=> "pay",
			"vpc_Locale"=> "en",
			"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
			"vpc_Merchant"=> "DEVPLACOM01",//Add your merchant number here
			"vpc_OrderInfo"=>$order,//this also better to be a unique number
			"vpc_ReturnURL"=> HTTP_SERVER_PATH."players/success",
			"vpc_Version"=> "1"
			);
		
			$md5HashData = $SECURE_SECRET;	
			ksort ($testarr);
			// set a parameter to show the first pair in the URL
			$appendAmp = 0;
	        $vpcURL = "https://migs.mastercard.com.au/vpcpay" . "?";
			foreach($testarr as $key => $value)
			{
               // create the md5 input and URL leaving out any fields that have no value
				if (strlen($value) > 0) {					
					// this ensures the first paramter of the URL is preceded by the '?' char
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);						
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);						
					}
					$md5HashData .= $value;
				}
			}
			if (strlen($SECURE_SECRET) > 0) 
			{
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
			}
			
			header("Location: ".$vpcURL);

		 exit();
					 
	} 


}
?>
