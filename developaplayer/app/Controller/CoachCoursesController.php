<?php
App::uses('CommonController', 'Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class CoachCoursesController  extends CommonController {
	var $name = 'CoachCourses';
	var $uses = array('Course','CourseDate','CourseAgeGroup','CourseCoach','CourseToPosition','PlayerPosition','BookCourseByPlayer','PlayerDetail');
	 public function beforeFilter() {
        $this->Auth->allow(array('index','even_json_file','event_details','even_json_details','book_course','success','success_point','booking_summary','booking_details','all_booked_coach'));
		$this->set('title_for_layout', 'Develop a Player');
		
		  parent::beforeFilter(); 
		         
    }	
	public function index()
	{
		//$this->layout = "ajax";
		//$this->render(false);
		//$this->redirect(array('action'=>'register'));
	}
	
	public function event_details()
	{
		//pr($this->Auth->user('Member'));
		//pr($this->Session->read('Auth.Member'));
		$player_id = $this->Session->read('Auth.Member.id');		
		$course_id = $this->request->params['pass'][0];
		$course_date_id = $this->request->params['pass'][1];
		$cat_id = $this->request->params['pass'][2];		
		//$player_tot_points = $this->Session->read('Auth.Member.PlayerDetail.topup_point');
		$player_detail_id = $this->Session->read('Auth.Member.PlayerDetail.id');
		
		$PlayerArray= $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.id'=>$player_detail_id),'fields'=>array('PlayerDetail.id','PlayerDetail.topup_point')));
		$player_tot_points =$PlayerArray['PlayerDetail']['topup_point'];
		
		$courseArray= $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id,'Course.status'=>'A'),'fields'=>array('Course.id','Course.course_title','Course.cost')));
		$this->set('player_id',$player_id);
		$this->set('course_id',$course_id);
		$this->set('course_date_id',$course_date_id);
		$this->set('cat_id',$cat_id);
		$this->set('player_tot_points',$player_tot_points);
		$this->set('player_detail_id',$player_detail_id);
		$this->set('courseArray',$courseArray);
		//pr($courseArray);
		
	}
	
	public function even_json_file()
	{
		
		$this->layout = "ajax";	
		$category_id=$this->request->params['pass'][0];
		$all_course= $this->Course->find('all',array('conditions'=>array('CourseDate.course_date >='=>date("Y-m-d"),'Course.category_id'=>$category_id,'Course.status'=>'A'),'group'=>'CourseDate.course_id','group'=>'CourseDate.course_id','order' => 'CourseDate.course_date asc',));		  
		$this->set('all_course',$all_course);

	}
	
	public function even_json_details()
	{		
		$course_id=$this->request->params['pass'][0];
		$course_date_id=$this->request->params['pass'][1];		
		$all_course_detail= $this->Course->find('all',array('conditions'=>array('Course.id'=>$course_id)));
		//pr($all_course_detail);
		//exit;
		//==================Age Group=========================================================================//
		$AgeGroups=$this->CourseAgeGroup->find('all',array('conditions'=>array('CourseAgeGroup.course_id'=>$course_id),'fields'=>'AgeGroup.title'));		
		//====================== Coach ======================================================================//      
	    $coaches=$this->CourseCoach->find('all',array('conditions'=>array('CourseCoach.course_id'=>$course_id),'fields'=>array('User.id','User.first_name','User.last_name')));
	      //==================================position ====================================//
		  
		  //$get_edit_position= $this->CourseToPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$id)));
		  
		   $this->PlayerPosition->bindModel(array(
			   'hasOne' => array(
								'CourseToPosition'=>array(
									 'className'=>'CourseToPosition',
									// 'conditions'   => 'Nationality.id=PlayerDetail.nationality',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'position_id',
									 'fields'=>'PlayerPosition.name,PlayerPosition.id'
								),
		                    )
						)
					);

                 $course_position= $this->PlayerPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$course_id)));  
                
				
					
            //=================================================================================================//		 
	     
	    $i=0;
		$age_group_data ="";
		if(!empty($AgeGroups))
		{
			foreach($AgeGroups as $AgeGroups_name)
			{
				if($i==0)
				{
					$sparator='';
					
				}else
				{
					$sparator=';';					
				}			
				$age_group_data .=$sparator.''.$AgeGroups_name['AgeGroup']['title'];
			$i++;	
			}
		}
		
		$K=0;
		$coaches_list_text ="";
		if(!empty($coaches))
		{
			foreach($coaches as $coaches_list)
			{
				if($K==0)
				{
					$sparator='';
					
				}else
				{
					$sparator=' , ';			
				}
			    $base_url =$this->webroot;
			    $coach_profile_link= $base_url.'coaches/coach_profile/'.base64_encode($coaches_list['User']['id']);
				$coaches_list_text .=$sparator.'<a href=\"'. $coach_profile_link.'\" target=\'_blank\'>'.$coaches_list['User']['first_name'].' '.$coaches_list['User']['last_name'].'</a>';	
			   $K++;	
			}
		}	
	//=================== All position ==========================//	
	$p=0;
		$position_data ="";
		if(!empty($course_position))
		{
			foreach($course_position as $course_positions)
			{
				if($p==0)
				{
					$sparator='';
					
				}else
				{
					$sparator=' | ';
					
				}			
				$position_data .=$sparator.''.$course_positions['PlayerPosition']['name'];
			$p++;	
			}
		}
		
		$this->set('all_course_detail',$all_course_detail);
		$this->set('age_group_data',$age_group_data);
		$this->set('coaches_list_text',$coaches_list_text);
		$this->set('position_data',$position_data);
		//$this->set('position_data',$position_data);		
	}

	public function book_course()
	{
		
		//pr($this->data);
		//exit();
		/*$BookCourseByPlayer['BookCourseByPlayer']['course_id']=$this->request->pass[0];
		$BookCourseByPlayer['BookCourseByPlayer']['course_date_id']=$this->request->pass[1];
		$BookCourseByPlayer['BookCourseByPlayer']['player_id']=$this->Session->read('Auth.Member.id');
		$BookCourseByPlayer['BookCourseByPlayer']['book_status']=0;
		$BookCourseByPlayer['BookCourseByPlayer']['date']=date('Y-m-d h:i:s');
		$BookCourseByPlayer['BookCourseByPlayer']['cat_id']=$this->request->pass[2];*/
		
		
		$BookCourseByPlayer['BookCourseByPlayer']['course_id'] = $this->data['course_id'];
		$BookCourseByPlayer['BookCourseByPlayer']['course_date_id'] = $this->data['course_date_id'];
		$BookCourseByPlayer['BookCourseByPlayer']['player_id'] = $this->data['player_id'];
		$BookCourseByPlayer['BookCourseByPlayer']['book_status'] = 0;
		$BookCourseByPlayer['BookCourseByPlayer']['date'] = date('Y-m-d h:i:s');
		$BookCourseByPlayer['BookCourseByPlayer']['cat_id'] = $this->data['cat_id'];
		$amount=$this->data['course_cost'];
		$BookCourseByPlayer['BookCourseByPlayer']['amount']=$amount;
		
		//$avilivility= $this->Course->find('first',array('conditions'=>array('Course.id'=>$this->request->pass[0])));
		//pr($avilivility);		
		//exit();		
		$booking_player= $this->BookCourseByPlayer->find('count',array('conditions'=>array('BookCourseByPlayer.course_id'=>$this->data['course_id'],'BookCourseByPlayer.player_id'=>$this->data['player_id'])));		
		if($booking_player==0)
		{		
		   $payment_option = $this->data['payment_option'];
		   if($payment_option=='comweb')
			{
				
				$this->BookCourseByPlayer->save($BookCourseByPlayer);
				$order_id=$this->BookCourseByPlayer->id;		   
				//=========================   Comweb Payment Testing ===========================================
				/*$accessCode    =  "9A591939";//value from migs payment gateway
				$merchantId    =  "TESTDEVPLACOM01";//value from migs payment gateway
				$amount =$amount;
				$unique_id = rand(999999,8988888888);//this is a sample random no
				//$order = rand(10,100);
				$testarr = array(
				"vpc_Amount" => $amount*100,//Final price should be multifly by 100
				"vpc_Currency" => 'AUD',
				"vpc_AccessCode" => $accessCode,//Put your access code here
				"vpc_Command"=> "pay",
				"vpc_Locale"=> "en",
				"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
				"vpc_Merchant"=> $merchantId,//Add your merchant number here
				"vpc_OrderInfo"=>$order_id,//this also better to be a unique number
				"vpc_ReturnURL"=> HTTP_SERVER_PATH."coach_courses/success",
				"vpc_Version"=> "1");
				ksort($testarr);
				$SECURE_SECRET =  "84712313F1FBE77FE54EF9101C779ABB";
				//value from migs payment gateway
				$securehash = $SECURE_SECRET;
				$url = "";
				foreach ($testarr as $key => $value)
				{
					$securehash .= $value;
					$url .= $key."=".urlencode($value)."&";
				}
				$securehash = md5($securehash);//Encoding
				$url .= "vpc_SecureHash=".$securehash;		
				header("location:https://migs.mastercard.com.au/vpcpay?$url");	
				exit();
				*/
				
			//======================For Live Enviourment =======================//
		
			$SECURE_SECRET = "0B4737508BA899863FBC279A51422CB2";
			$unique_id = rand(999999,8988888888);//this is a sample random no
			$order = $order_id;
		  
			$testarr = array(
			"vpc_Amount" => $amount*100,//Final price should be multifly by 100
			"vpc_Currency" => 'AUD',
			"vpc_AccessCode" => "A39B63F9",//Put your access code here
			"vpc_Command"=> "pay",
			"vpc_Locale"=> "en",
			"vpc_MerchTxnRef"=> $unique_id , //This should be something unique number, i have used the session id for this
			"vpc_Merchant"=> "DEVPLACOM01",//Add your merchant number here
			"vpc_OrderInfo"=>$order_id,//this also better to be a unique number
			"vpc_ReturnURL"=> HTTP_SERVER_PATH."coach_courses/success",
			"vpc_Version"=> "1"
			);
		
			$md5HashData = $SECURE_SECRET;	
			ksort ($testarr);		
			$appendAmp = 0;
	        $vpcURL = "https://migs.mastercard.com.au/vpcpay" . "?";
			foreach($testarr as $key => $value)
			{
               // create the md5 input and URL leaving out any fields that have no value
				if (strlen($value) > 0) {					
					// this ensures the first paramter of the URL is preceded by the '?' char
					if ($appendAmp == 0) {
						$vpcURL .= urlencode($key) . '=' . urlencode($value);						
						$appendAmp = 1;
					} else {
						$vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);						
					}
					$md5HashData .= $value;
				}
			}
			if (strlen($SECURE_SECRET) > 0) 
			{
				$vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
			}			
			header("Location: ".$vpcURL);			
			exit();
			
			//================================End============================================//	
		
			}
			
			if($payment_option=='personal')
			{
				$BookCourseByPlayer['BookCourseByPlayer']['payment_type']=0;
				$BookCourseByPlayer['BookCourseByPlayer']['payment_status']= 2;
				$BookCourseByPlayer['BookCourseByPlayer']['book_status']=1;
				$num_str =sprintf("%06d", mt_rand(0, 999999));
				$BookCourseByPlayer['BookCourseByPlayer']['vpc_TransactionNo']=$num_str;
				
				$player_detail_id = $this->data['player_detail_id'];
				$this->BookCourseByPlayer->save($BookCourseByPlayer);
				$order_id=$this->BookCourseByPlayer->id;
				$player_tot_points = $this->data['player_tot_points'];
				$course_cost = $this->data['course_cost'];
				$remaing_cost= ($player_tot_points - $course_cost);
				
				$player_Array['PlayerDetail']['id']=$player_detail_id;
				$player_Array['PlayerDetail']['topup_point']=$remaing_cost;
				$this->PlayerDetail->save($player_Array);
 
               //=================Update availability======================================//	
			   
				$avilivility= $this->Course->find('first',array('conditions'=>array('Course.id'=>$this->data['course_id'])));
				$total_avibility=$avilivility['Course']['total_course_available'];
				$total_book=$avilivility['Course']['book_course'];
				//$now_avilibility=$total_avibility;
				$total_book_course=$total_book+1;
			    $this->Course->updateAll(array('Course.book_course'=>$total_book_course), array('Course.id'=>$course_id));
				$all_details= $this->Session->read('Auth.Member');
				//==========================================================================//
			    $all_respone_by_comweb['vpc_MerchTxnRef']=$num_str;
				//==================== Who Course Book ===================================//
				
				
				/*$this->Email->to =$all_details['email_address'];
				$this->Email->subject = 'Course Booked successfully Done - Developaplayer';
				$this->Email->from = 'Develop A Player' .'<'.Configure::read('admin_email').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('BookCourse', $avilivility);
				$this->set('all_details', $all_details);
				$this->set('all_respone_by_comweb', $all_respone_by_comweb);			
				$this->Email->template = 'course_booked';
				$this->Email->send();				
				//===================================Admin==================================//	   
				$this->Email->to = 'info@developaplayer.com';
				$this->Email->subject = 'Booked Course';
				$this->Email->from = 'Develop A Player'.'<'.Configure::read('admin_email').'>';
				$this->Email->layout = '';
				$this->Email->sendAs = 'html';
				$this->set('BookCourse', $avilivility);
				$this->set('all_respone_by_comweb', $all_respone_by_comweb);
				$this->set('all_details', $all_details);
				$this->Email->template = 'course_booked_admin';
				$this->Email->send();*/

				App::uses('CakeEmail', 'Network/Email');
				$this->Email = new CakeEmail('smtp');
				
				$this->Email->to(array($all_details['email_address']));
				$this->Email->subject('Course Booked successfully Done - Develop A Player');
				$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));				
				$this->Email->emailFormat('html');
				$this->Email->viewVars(array('BookCourse'=>$avilivility,'all_details'=>$all_details,'all_respone_by_comweb'=>$all_respone_by_comweb));			
				$this->Email->template('course_booked');
				$this->Email->send();				
				//===================================Admin==================================//				
				$this->Email->to(array(Configure::read('admin_email')=>'Develop A Player'));	
				$this->Email->subject('Booked Course');				
				$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));				
				$this->Email->emailFormat('html');
				$this->Email->viewVars(array('BookCourse'=>$avilivility,'all_details'=>$all_details,'all_respone_by_comweb'=>$all_respone_by_comweb));
				$this->Email->template('course_booked_admin');
				$this->Email->send();
				//===========================================================//
				$this->redirect(array('controller'=>'coach_courses','action'=>'success_point/'.$order_id));   
			}
			
		}
		else{
			$this->set('booking_player_count',$booking_player);		
		}
		
		//$this->set('booking_player_count',$booking_player);				
		//=================Update availability==================================//		
		/*$avilivility= $this->Course->find('first',array('conditions'=>array('Course.id'=>$this->request->pass[0])));
		$total_avibility=$avilivility['Course']['total_course_available'];
		$total_book=$avilivility['Course']['book_course'];
		//$now_avilibility=$total_avibility;
		$total_book_course=$total_book+1;		
		$this->Course->updateAll(array('Course.book_course'=>$total_book_course), array('Course.id'=>$this->request->pass[0]));*/
   	
	}
	//==============================================================================================
	function success()
	{		
		App::uses('CakeEmail', 'Network/Email');
		$all_respone_by_comweb =$_GET;	
		if($all_respone_by_comweb['vpc_Message']=='Approved')
		{
			$order_id = $all_respone_by_comweb['vpc_OrderInfo'];
			$avilivility= $this->BookCourseByPlayer->find('first',array('conditions'=>array('BookCourseByPlayer.id'=>$order_id)));
			$course_id=$avilivility['BookCourseByPlayer']['course_id'];			
			
			//=================Update availability==================================//	
			
			$avilivility= $this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id)));
			$total_avibility=$avilivility['Course']['total_course_available'];
			$total_book=$avilivility['Course']['book_course'];		
			$total_book_course=$total_book+1;		
			$this->Course->updateAll(array('Course.book_course'=>$total_book_course), array('Course.id'=>$course_id));			
			
			$BookCourse['BookCourseByPlayer']['id']= $order_id;
			$BookCourse['BookCourseByPlayer']['payment_status']= 2;
			$BookCourse['BookCourseByPlayer']['book_status']=1;
			$BookCourse['BookCourseByPlayer']['payment_type']=1;

			//============================ ========================
			$BookCourse['BookCourseByPlayer']['vpc_MerchTxnRef']= $all_respone_by_comweb['vpc_MerchTxnRef'];
			$BookCourse['BookCourseByPlayer']['vpc_Merchant']= $all_respone_by_comweb['vpc_Merchant'];
			$BookCourse['BookCourseByPlayer']['vpc_Message']= $all_respone_by_comweb['vpc_Message'];			
			$BookCourse['BookCourseByPlayer']['vpc_BatchNo']= $all_respone_by_comweb['vpc_BatchNo'];
			$BookCourse['BookCourseByPlayer']['vpc_TransactionNo']= $all_respone_by_comweb['vpc_TransactionNo'];
			$BookCourse['BookCourseByPlayer']['vpc_ReceiptNo']= $all_respone_by_comweb['vpc_ReceiptNo'];
			$this->BookCourseByPlayer->save($BookCourse);
			$all_details= $this->Session->read('Auth.Member');
			 
			 //$this->
			
			
			//echo '<pre>';
			//print_r($avilivility);
			//exit;
		//==================== Who Course Book ===================================//
	 
			/*$this->Email->to =$all_details['email_address'];
			$this->Email->subject = 'Course Booked successfully Done - Developaplayer';
			$this->Email->from = 'Develop A Player' .'<'.Configure::read('admin_email').'>';
			$this->Email->layout = '';
			$this->Email->sendAs = 'html';
			$this->set('BookCourse', $avilivility);
			$this->set('all_details', $all_details);
			$this->set('all_respone_by_comweb', $all_respone_by_comweb);			
			$this->Email->template = 'course_booked';
			$this->Email->send();*/
			//=======================  SMTP Email ============================
			$this->Email = new CakeEmail('smtp');
			
			$this->Email->to(array($all_details['email_address']));
			$this->Email->subject('Course Booked successfully Done - Develop A Player');			
			$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));		
			//$this->Email->layout = '';
			$this->Email->emailFormat('html');
			$this->Email->viewVars(array('BookCourse'=>$avilivility,'all_details'=>$all_details,'all_respone_by_comweb'=>$all_respone_by_comweb));						
			$this->Email->template('course_booked');
			$this->Email->send();
			
			
			//===================================Admin==================================//			
			$this->Email->to(array(Configure::read('admin_email')=>'Develop A Player'));	
			$this->Email->subject('Booked Course');		
			$this->Email->from(array(Configure::read('admin_email')=>'Develop A Player'));			
			$this->Email->emailFormat('html');
			$this->Email->viewVars(array('BookCourse'=>$avilivility,'all_details'=>$all_details,'all_respone_by_comweb'=>$all_respone_by_comweb));	
			$this->Email->template('course_booked_admin');
			$this->Email->send();	
			
		//===========================================================//
		
			$this->BookCourseByPlayer->bindModel(array(
			   'belongsTo' => array(								
								 'Player'=>array(
									 'className'	=>'User',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'	=>'player_id',
									 'fields'       =>array('Player.first_name','Player.last_name','Player.type')
		                          )							     
				               )   
							)
		               );					   
			$current_booking=$this->BookCourseByPlayer->find('first',array('conditions'=>array('BookCourseByPlayer.id'=>$all_respone_by_comweb['vpc_OrderInfo'])));			
			$this->set('current_booking',$current_booking);
		}		   
		//exit;
	}	
	//==============================================================================================
	function success_point($order_id=null)
	{
		$this->BookCourseByPlayer->bindModel(array(
		   'belongsTo' => array(								
							 'Player'=>array(
								 'className'	=>'User',
								 'conditions'   => '',
								 'order'        => '',
								 'dependent'    =>  false,
								 'foreignKey'	=>'player_id',
								 'fields'       =>array('Player.first_name','Player.last_name','Player.type')
							  )							     
						   )   
						)
				   );
				   
		$current_booking=$this->BookCourseByPlayer->find('first',array('conditions'=>array('BookCourseByPlayer.id'=>$order_id)));	
		//pr($current_booking);	
		$this->set('current_booking',$current_booking);					   
		//exit;
	}	
	
	/********************************** Booking History   ************************************************/
	function booking_summary()
	{
		$player_id = $this->Session->read('Auth.Member.id');
		if(!empty($player_id))
		{
			$this->BookCourseByPlayer->bindModel(array(
			   'belongsTo' => array(								
								 'Course'=>array(
									 'className'	=>'Course',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'	=>'course_id',
									 'fields'       =>array('Course.id','Course.course_title','Course.location','Course.cost','Course.course_date')
		                          )							     
				               )   
							)
		               );
			
			$booking_details = $this->BookCourseByPlayer->find('all',array('conditions'=>array('BookCourseByPlayer.player_id '=>$player_id)));
			//pr($booking_details);
			$this->set('booking_details',$booking_details);
		}
		else
		{
			$this->redirect(array('controller'=>'homes','action'=>'index'));   
		}		
	}
	
	function booking_details($booking_id=null)
	{
		$booking_id = base64_decode($booking_id);
		$player_id = $this->Session->read('Auth.Member.id');
		if(!empty($player_id))
		{
			$this->BookCourseByPlayer->bindModel(array(
			   'belongsTo' => array(								
								 'Course'=>array(
									 'className'	=>'Course',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'	=>'course_id',
									 //'fields'       =>array('Course.course_title','Course.location','Course.cost','Course.course_date')
		                          )							     
				               )   
							)
		               );
			
			$booking_details = $this->BookCourseByPlayer->find('first',array('conditions'=>array('BookCourseByPlayer.id '=>$booking_id)));
			//pr($booking_details);
			$this->set('booking_details',$booking_details);
		}
		else
		{
			$this->redirect(array('controller'=>'homes','action'=>'index'));   
		}		
	}

	/***************************** All Booking Coach **************************************/
	
	function all_booked_coach()
	{		
		$course_id =  $this->params['named']['course_id'];
		$booked_coach = $this->CourseCoach->find('all',array('conditions'=>array('CourseCoach.course_id '=>$course_id)));
		//pr($booked_coach);
		
		$Coach_name = "";
		foreach($booked_coach as $book_key=>$book_Val)
		{			
			$Coach_name=$book_Val['User']['first_name']." ".$book_Val['User']['last_name'].", ".$Coach_name;	
		}	
		
		return $Coach_name;
		
		
	}
	
}