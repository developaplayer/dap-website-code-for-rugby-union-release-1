<?php
App::uses('AdminAppController', 'Admin.Controller');
class AgeGroupsController extends AdminAppController {
	var $uses=array('Admin.AgeGroup');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Age_Group','view');
	}
	
	public function add_age_group()
	{
		$this->check_Permission('Age_Group','add');
		if($this->request->is('post')){
			//$this->AgeGroup->find('all',array('conditions'=>array('AgeGroup.title')));
			$data['AgeGroup']['title']=$this->request->data['title'];
			$data['AgeGroup']['initial_age']=$this->request->data['initial_age'];
			$data['AgeGroup']['final_age']=$this->request->data['final_age'];
			$data['AgeGroup']['status']=$this->request->data['status'];
			if($data['AgeGroup']['initial_age']>$data['AgeGroup']['final_age']){
				$this->Session->setFlash('Initial Age Must Be Greater Than Final Age..','default',array('class'=>'alert alert-danger alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'add_age_group'
					));
			}
			if($this->AgeGroup->save($data)){
				$this->Session->setFlash('Age Group Succesfully Added..','default',array('class'=>'alert alert-success alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
					));
			}else{
				$this->Session->setFlash('Error in Creating Age Group!!','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
		}
	}
	
	public function age_group_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'AgeGroup.id',
			1 =>'AgeGroup.title',
			2 =>'AgeGroup.initial_age',
			2 =>'AgeGroup.final_age',
			3 =>'AgeGroup.status',
			4 =>'AgeGroup.created',
		);
		$totalData = $this->AgeGroup->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( AgeGroup.title LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR AgeGroup.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR AgeGroup.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$age_groups = $this->AgeGroup->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($age_groups);
		}
		$data = array();
		foreach( $age_groups as $row ) {  // preparing an array
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y' && $this->Session->read('permissions.Age_Group.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['AgeGroup']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="club_title_'.$row['AgeGroup']["id"].'">'.$row['AgeGroup']["title"].'</div>';
			
			$nestedData[] = '<div id="club_initial_age_'.$row['AgeGroup']["id"].'">'.$row['AgeGroup']["initial_age"].'</div>';
			
			$nestedData[] = '<div id="club_final_age_'.$row['AgeGroup']["id"].'">'.$row['AgeGroup']["final_age"].'</div>';
			
			if($row['AgeGroup']["status"]=='Y'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'N';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'Y';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['AgeGroup']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['AgeGroup']["created"]));
			
			//================Player Age Group Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y' && $this->Session->read('permissions.Age_Group.status')=='N' && $this->Session->read('permissions.Age_Group.edit')=='N'  && $this->Session->read('permissions.Age_Group.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Age Group Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y' && $this->Session->read('permissions.Age_Group.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['AgeGroup']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['AgeGroup']['id'].'"><img id="status_img_'.$row['AgeGroup']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Age Group Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y' && $this->Session->read('permissions.Age_Group.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/age_groups/edit/'.$row['AgeGroup']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Age Group Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y' && $this->Session->read('permissions.Age_Group.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['AgeGroup']['title'].'?\')" href="'.$this->webroot.'admin/age_groups/delete_age_group/'.$row['AgeGroup']['id'].'" title="Delete" id="delete_'.$row['AgeGroup']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['AgeGroup']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['AgeGroup']['id'].'"><img id="status_img_'.$row['AgeGroup']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/age_groups/edit/'.$row['AgeGroup']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['AgeGroup']['title'].'?\')" href="'.$this->webroot.'admin/age_groups/delete_age_group/'.$row['AgeGroup']['id'].'" title="Delete" id="delete_'.$row['AgeGroup']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function status(){
		$this->check_Permission('Age_Group','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['AgeGroup']['status'] = $this->request->data['status'];
			$this->AgeGroup->id = $this->request->data['id'];
			if($this->AgeGroup->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_age_group($id=""){
		$this->check_Permission('Age_Group','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->AgeGroup->delete($id)){
				$this->Session->setFlash('AgeGroup Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable'));
			}else{
				$this->Session->setFlash('Error in Deleting AgeGroup...','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=""){
		$this->check_Permission('Age_Group','edit');
		if($id==""){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
						));
		}
		if($this->request->is('post')){
			$data['AgeGroup']['title']=$this->request->data['title'];
			$data['AgeGroup']['initial_age']=$this->request->data['initial_age'];
			$data['AgeGroup']['final_age']=$this->request->data['final_age'];
			$data['AgeGroup']['status']=$this->request->data['status'];
			if($data['AgeGroup']['initial_age']>$data['AgeGroup']['final_age']){
				$this->Session->setFlash('Initial Age Must Be Greater Than Final Age..','default',array('class'=>'alert alert-danger alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'edit',
						$id
					));
			}
			$this->AgeGroup->id = $id;
			if($this->AgeGroup->save($data)){
				$this->Session->setFlash('Age Group Succesfully Updated..','default',array('class'=>'alert alert-success alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
					));
			}else{
				$this->Session->setFlash('Error in Updating Age Group!!','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
		}
		$age_group_data = $this->AgeGroup->find('first',array('conditions'=>array('AgeGroup.id'=>$id)));
		if(empty($age_group_data)){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
						));
		}
		$this->set('age_group_data',$age_group_data);
	}
	
	function delete_group_multiple(){
		$this->check_Permission('Age_Group','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->AgeGroup->create();
				$this->AgeGroup->delete($id);
				$this->Session->setFlash('Age Group Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable'));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'age_groups',
						'action' => 'index'
						));
	}
}