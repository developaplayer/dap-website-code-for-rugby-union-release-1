<?php
App::uses('AdminAppController', 'Admin.Controller');
class CoachingsController extends AdminAppController { 
	var $uses=array('Admin.CoachSpeciality');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Coaching_Speciality','view');
	}
	
	public function primary_data()
	{
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'CoachSpeciality.id',
			1 =>'CoachSpeciality.name',
			2 =>'CoachSpeciality.status',
			2 =>'CoachSpeciality.created',
		);
		$totalData = $this->CoachSpeciality->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( CoachSpeciality.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR CoachSpeciality.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR CoachSpeciality.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$coachings = $this->CoachSpeciality->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->CoachSpeciality->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($coachings);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($coachings);
		}
		$data = array();
		foreach( $coachings as $row ) {  // preparing an array
			$nestedData=array();
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y' && $this->Session->read('permissions.Coaching_Speciality.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['CoachSpeciality']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="name_'.$row['CoachSpeciality']["id"].'">'.$row['CoachSpeciality']["name"].'</div>';
			
			if($row['CoachSpeciality']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['CoachSpeciality']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['CoachSpeciality']["created"]));
			
			//================Player Coaching Speciality Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y' && $this->Session->read('permissions.Coaching_Speciality.status')=='N' && $this->Session->read('permissions.Coaching_Speciality.edit')=='N'  && $this->Session->read('permissions.Coaching_Speciality.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Coaching Speciality Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y' && $this->Session->read('permissions.Coaching_Speciality.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['CoachSpeciality']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['CoachSpeciality']['id'].'"><img id="status_img_'.$row['CoachSpeciality']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Coaching Speciality Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y' && $this->Session->read('permissions.Coaching_Speciality.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="javascript:edit_data('.$row['CoachSpeciality']['id'].',\''.$row['CoachSpeciality']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Coaching Speciality Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y' && $this->Session->read('permissions.Coaching_Speciality.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['CoachSpeciality']['name'].'?\')" href="'.$this->webroot.'admin/coachings/delete_coaching/'.$row['CoachSpeciality']['id'].'" title="Delete" id="delete_'.$row['CoachSpeciality']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['CoachSpeciality']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['CoachSpeciality']['id'].'"><img id="status_img_'.$row['CoachSpeciality']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['CoachSpeciality']['id'].',\''.$row['CoachSpeciality']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['CoachSpeciality']['name'].'?\')" href="'.$this->webroot.'admin/coachings/delete_coaching/'.$row['CoachSpeciality']['id'].'" title="Delete" id="delete_'.$row['CoachSpeciality']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_coaching(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['CoachSpeciality']['name']);
			if(!empty($this->request->data['name'])){
				$check_coaching = $this->CoachSpeciality->find('first',array('conditions'=>array('CoachSpeciality.name'=>$this->request->data['name'])));
				//pr($check_coaching);
				if(empty($check_coaching)){
					$data['CoachSpeciality']['name'] = $this->request->data['name'];
					$data['CoachSpeciality']['type'] = 'P';
					if($this->CoachSpeciality->save($data)){
						$inserted_id = $this->CoachSpeciality->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="name_'.$inserted_id.'">'.$data['CoachSpeciality']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['CoachSpeciality']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['CoachSpeciality']['name'].'?\')" href="'.$this->webroot.'admin/coachings/delete_coaching/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coachings',
						'action' => 'index'
			));
		}
	}
	
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['CoachSpeciality']['status'] = $this->request->data['status'];
			$this->CoachSpeciality->id = $this->request->data['id'];
			if($this->CoachSpeciality->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_coaching($id=""){
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->CoachSpeciality->delete($id)){
				$this->Session->setFlash(__('Coaching Speciality Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Coaching Speciality...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coachings',
						'action' => 'index'
						));
		}
	}
	
	function update_coaching_speciality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_coaching = $this->CoachSpeciality->find('first',array('conditions'=>array('CoachSpeciality.name'=>$this->request->data['name'])));
			if(!empty($check_coaching)){
				echo "exist";
			}else{
				$data['CoachSpeciality']['name'] = $this->request->data['name'];
				$this->CoachSpeciality->id = $this->request->data['id'];
				if($this->CoachSpeciality->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_coaching_multiple(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->CoachSpeciality->create();
				$this->CoachSpeciality->delete($id);
				$this->Session->setFlash(__('Coaching Specialities Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coachings',
						'action' => 'index'
						));
	}
}