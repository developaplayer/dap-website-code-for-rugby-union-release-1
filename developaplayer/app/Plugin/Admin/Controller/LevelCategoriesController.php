<?php
App::uses('AdminAppController', 'Admin.Controller');
class LevelCategoriesController extends AdminAppController {
	var $uses=array('Admin.LevelCategory');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Level_Category','view');
	}
	
	public function category_data()
	{
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'LevelCategory.id',
			1 =>'LevelCategory.name',
			2 =>'LevelCategory.status',
			2 =>'LevelCategory.created',
		);
		$totalData = $this->LevelCategory->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( LevelCategory.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR LevelCategory.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR LevelCategory.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$categories = $this->LevelCategory->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->LevelCategory->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($categories);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($categories);
		}
		$data = array();
		foreach( $categories as $row ) {  // preparing an array
			$nestedData=array();
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['LevelCategory']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="category_name_'.$row['LevelCategory']["id"].'">'.$row['LevelCategory']["name"].'</div>';
			
			if($row['LevelCategory']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['LevelCategory']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['LevelCategory']["created"]));
			
			//================Player Level Category Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.status')=='N' && $this->Session->read('permissions.Level_Category.edit')=='N'  && $this->Session->read('permissions.Level_Category.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Level Category Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['LevelCategory']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['LevelCategory']['id'].'"><img id="status_img_'.$row['LevelCategory']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Level Category Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="javascript:edit_data('.$row['LevelCategory']['id'].',\''.$row['LevelCategory']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Level Category Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['LevelCategory']['name'].'?\')" href="'.$this->webroot.'admin/categories/delete_category/'.$row['LevelCategory']['id'].'" title="Delete" id="delete_'.$row['LevelCategory']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['LevelCategory']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['LevelCategory']['id'].'"><img id="status_img_'.$row['LevelCategory']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['LevelCategory']['id'].',\''.$row['LevelCategory']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['LevelCategory']['name'].'?\')" href="'.$this->webroot.'admin/categories/delete_category/'.$row['LevelCategory']['id'].'" title="Delete" id="delete_'.$row['LevelCategory']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_category()
	{
		$this->check_Permission('Level_Category','add');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['LevelCategory']['name']);
			if(!empty($this->request->data['category_name'])){
				$check_category = $this->LevelCategory->find('first',array('conditions'=>array('LevelCategory.name'=>$this->request->data['category_name'])));
				if(empty($check_category)){
					$data['LevelCategory']['name'] = $this->request->data['category_name'];
					$data['LevelCategory']['type'] = 'S';
					if($this->LevelCategory->save($data)){
						$inserted_id = $this->LevelCategory->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="category_name_'.$inserted_id.'">'.$data['LevelCategory']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['LevelCategory']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['LevelCategory']['name'].'?\')" href="'.$this->webroot.'admin/categories/delete_category/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Categories',
						'action' => 'index'
			));
		}
	}
	
	function status(){
		$this->check_Permission('Level_Category','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['LevelCategory']['status'] = $this->request->data['status'];
			$this->LevelCategory->id = $this->request->data['id'];
			if($this->LevelCategory->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_category($id="")
	{
		$this->check_Permission('Level_Category','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->LevelCategory->delete($id)){
				$this->Session->setFlash(__('Category Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Category...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Categories',
						'action' => 'index'
						));
		}
	}
	
	function update_category()
	{
		$this->check_Permission('Level_Category','edit');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_category = $this->LevelCategory->find('first',array('conditions'=>array('LevelCategory.name'=>$this->request->data['category'])));
			if(!empty($check_category)){
				echo "exist";
			}else{
				$data['LevelCategory']['name'] = $this->request->data['category'];
				$this->LevelCategory->id = $this->request->data['id'];
				if($this->LevelCategory->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_category_multiple()
	{
		$this->check_Permission('Level_Category','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->LevelCategory->create();
				$this->LevelCategory->delete($id);
				$this->Session->setFlash(__('Categories Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Categories',
						'action' => 'index'
						));
	}
}