<?php
App::uses('AdminAppController', 'Admin.Controller');
class UsersController extends AdminAppController {
  
  var $uses=array('Admin.Customer','Admin.AddressToCustomer','Admin.Order','Admin.User');
	 public function beforeFilter() {
        $this->Auth->allow(array('login','check_login','forget_password','reset_pass'));
	   parent::beforeFilter();       
    }	
	
	public function index(){
		$this->redirect(
				array("action" => "view_all"));
	}
	
	public function login()
	{
	    $this->layout = 'admin_login';
		 //if already logged-in, redirect
        if($this->Session->check('Auth.User'))
		{			
         $this->redirect($this->Auth->redirectUrl());
        }
		if($this->request->is('post')){
			if ($this->Auth->login()) {
                $this->Session->setFlash(__('Welcome '. $this->Auth->user('name'),'default',array('class'=>'alert alert-success alert-dismissable')));
			   return $this->redirect($this->Auth->redirectUrl());

            } else {
				
				$this->Session->setFlash("Invalid username or password",'default',array('class'=>'alert alert-danger alert-dismissable'));
            }
			
		}
		$this->set('title_for_layout', 'Admin Login');
	}
	
	function logout(){
		$this->Session->delete('Auth.User');
		$this->Session->setFlash("Logged out successfully",'default',array('class'=>'alert alert-success'));
		$this->Session->destroy('permissions');
	    return $this->redirect($this->Auth->logout());
	
	}
	
	public function dashboard(){
		/*$this->loadModel('Admin.Product');
		$this->loadModel('Admin.Order');
		$product=$this->Product->find('all');
		$customer=$this->Customer->find('all');
		$order_new=$this->Order->find('all',array('field'=>'Order.id','conditions'=>array('Order.order_status_id'=>'1')));
		$order_delivered=$this->Order->find('all',array('field'=>'Order.id','conditions'=>array('Order.order_status_id'=>'2')));
		$order=$this->Order->find('all',array('field'=>'Order.id'));
		$total_product=count($product);
		$total_delivered=count($order_delivered);
		$total_customer=count($customer);
		$total_orders=count($order);
		$total_new_order=count($order_new);
		$this->set('total_product',$total_product);
		$this->set('total_customer',$total_customer);
		$this->set('total_orders',$total_orders);
		$this->set('total_delivered',$total_delivered);
		$this->set('total_new_order',$total_new_order);*/
		$this->set('title_for_layout', 'Welcome Develop A Player Admin Panel');	
	}
	
	public function user_delete($id){
		$this->loadModel('Admin.OrderProduct');
		$this->loadModel('Admin.Waiting');
		$this->loadModel('Admin.Wishlist');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);
		$this->Customer->delete($id);
		$order_id=$this->Order->find('all',array('fields'=>'Order.id',array('conditions'=>array('Order.customer_id'=>$id))));
		$this->Order->deleteAll(array('Order.customer_id'=>$id));
		$this->OrderProduct->deleteAll(array('OrderProduct.order_id'=>$order_id[0]['Order']['id']));
		$this->Waiting->deleteAll(array('Waiting.user_id'=>$id));
		$this->Wishlist->deleteAll(array('Wishlist.customer_id'=>$id));
		$this->Session->setFlash(_('User Succesfully Deleted'));
		$this->redirect(
		array("action" => "view_all"));
	}
	
	public function send_mail($email_to='')
	{
		//showing all units
		$this->layout="ajax";
		$this->set('email_to',$email_to);
		if($this->request->data){
			$Email = new CakeEmail();
			if(isset($this->request->data['from']) && isset($this->request->data['to']) && isset($this->request->data['subject']) && isset($this->request->data['message'])){
				$Email->from(array($this->request->data['from']=>'Amigros'))
					->to($this->request->data['to'])
					->subject($this->request->data['subject'])
					->send($this->request->data['message']);
					$this->Session->setFlash(__('Email Sent..'));
			}else{
				$this->Session->setFlash(__('Email Sending Error..'));
			}
		}
	}
	
	public function user_multiple_delete()
	{
		//deleting selected multiple customer
		$this->autoRender= false;
		$this->layout="ajax";
		$this->loadModel('Admin.OrderProduct');
		$this->loadModel('Admin.Waiting');
		$this->loadModel('Admin.Wishlist');
		foreach($this->request->data['id'] as $id){
			$this->Customer->delete($id);
			$order_id=$this->Order->find('all',array('fields'=>'Order.id',array('conditions'=>array('Order.customer_id'=>$id))));
			$this->Order->deleteAll(array('Order.customer_id'=>$id));
			$this->OrderProduct->deleteAll(array('OrderProduct.order_id'=>$order_id[0]['Order']['id']));
			$this->Waiting->deleteAll(array('Waiting.user_id'=>$id));
			$this->Wishlist->deleteAll(array('Wishlist.customer_id'=>$id));
		}
		$this->Session->setFlash("Customer Successfully Deleted..");
		$this->redirect(
			array("action" => "view_all"));
	}
	
	public function view_all()
	{
		$data=$this->Customer->find('all');
		$this->set('title_for_layout', 'View All Users');
		$this->set('user',$data);
	}
	
	public function user_details($id){
		$id=base64_decode($id);
		if($this->request->is('post'))
		{
			$customer_details['id']=$id;
			$customer_details['first_name']=$this->request->data['first_name'];
			$customer_details['last_name']=$this->request->data['last_name'];
			$customer_details['mobile_number']=$this->request->data['mobile_number'];
			$customer_details['status']=$this->request->data['status'];
			if($this->Customer->save($customer_details)){
				$customer=$this->Customer->find('all',array('fields'=>'id','conditions'=>array('Customer.id'=>$id)));
				foreach($customer[0]['AddressToCustomer'] as $k=>$addr){
					$this->AddressToCustomer->delete($addr['id']);
				}
				foreach($this->request->data['address'] as $a=>$data){
					$this->AddressToCustomer->create();
					if(!empty($data)){
						$address['customer_id']=$id;
						$address['address']=$data;
						$address['country']="IND";
						$address['city']="0";
						$address['state']="0";
						$address['pincode']=$this->request->data['pincode'][$a];
						$this->AddressToCustomer->save($address);
					}
				}
				$this->Session->setFlash(_('Update Success..'));
				$this->redirect(
						array("action" => "view_all"));
			}else{
				$this->Session->setFlash(_('Update Error..'));
				$this->redirect(
						array("action" => "view_all"));
			}
		}
		$data="";
		$data=$this->Customer->find('all',array('conditions'=>array('Customer.id'=>$id)));
		$this->set('title_for_layout', 'Admin Panel: User Details('.$data[0]['Customer']['first_name'].')');	
		$this->set('user',$data);
	}
	
	public function add_user(){
		if($this->request->is('post'))
		{
			if(isset($this->request->data)){
				$customer_details['password']=$this->Auth->password($this->request->data['password']);
				$customer_details['email']=$this->request->data['email'];
				$customer_details['first_name']=$this->request->data['first_name'];
				$customer_details['last_name']=$this->request->data['last_name'];
				$customer_details['mobile_number']=$this->request->data['mobile_number'];
				$customer_details['status']=$this->request->data['status'];
				if($this->Customer->save($customer_details)){
					foreach($this->request->data['address'] as $a=>$data){
						$this->AddressToCustomer->create();
						if(!empty($data)){
							$address['customer_id']=$this->Customer->getLastInsertId();
							$address['address']=$data;
							$address['country']="IND";
							$address['city']="0";
							$address['state']="0";
							$address['pincode']=$this->request->data['pincode'][$a];
							if($this->AddressToCustomer->save($address)){
								$this->Session->setFlash(_('User Succesfully Added'));
							}else{
								$this->Customer->delete($address['customer_id']);
								$this->Session->setFlash(_('Error...'));
								$this->redirect(
								array("action" => "add_user"));
							}
						}
					}
				}else{
					$this->Session->setFlash(_('Error.. Add Again with different email id..'));
					$this->redirect(
					array("action" => "add_user"));
				}
				$this->redirect(
					array("action" => "view_all"));
			}
		}
	$this->set('title_for_layout', 'Admin Panel: Add User');	
	}
	
	
	
	public function get_city($data)
	{
		$this->layout="ajax";
		$city=$this->City->find('all',array('conditions'=>array('City.cityname'=>$data)));
		$this->set('city',$city);
	}
	
	
	public function change_password(){
		if($this->request->is('post')){
			$pass = $this->Auth->password($this->request->data['old_password']);
			$admin_pass = $this->User->find('first',array('fields' => array('User.password'),'conditions' => array('User.id' => $this->Session->read('Auth.User.id'),'password'=>$pass)));
			if(!empty($admin_pass)){
					$data = array(
								'User'=>array(
									'id'=>$this->Session->read('Auth.User.id'),
									'password'=>$this->Auth->password($this->request->data['password']),
								)
							);
					$this->User->save($data);
					$this->Session->setFlash("Password has been updated",'default',array('class'=>'alert alert-success'));
					$this->redirect(array("controller" => "users", 
									  "action" => "change_password",
									  "plugin" => "admin"
								));
			}else{
				$this->Session->setFlash("Please provide correct current password",'default',array('class'=>'alert alert-danger'));

				$this->redirect(array("controller" => "users", 
									  "action" => "change_password",
									  "plugin" => "admin"
								));


			}
			
		}
		
		$this->set('title_for_layout', 'Change Password');

	}
	
	public function forget_password(){
		$this->layout = 'admin_login';
		if($this->request->is('post')){
			$date = date('m.d.i.s');
			$user_data = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['User']['email_address'],'User.type'=>'A')));
			if(!empty($user_data)){
				$url = Router::fullbaseUrl();
				$link['User']['reset_pass_link'] = $this->Auth->password($this->request->data['User']['email_address']).$date;
				$this->User->id = $user_data['User']['id'];
				$this->User->save($link);
				if(mail($this->request->data['User']['email_address'],"developaplayer@no-reply.com","Forget Password",'You have requested for forget password link. Click <a href="'.$url.$this->webroot.'admin/users/reset_pass/'.$link['User']['reset_pass_link'].'">here</a> to change your password.',"Password reset link has been sent to your email.")){
					$this->Session->setFlash("Reset Password link has been sent to your email.",'default',array('class'=>'alert alert-success'));
				}
			}else{
				$this->Session->setFlash("Email Id Does Not Exist!!",'default',array('class'=>'alert alert-danger'));
			}
			
			/*$this->redirect(array("controller" => "users", 
								  "action" => "change_password",
								  "plugin" => "admin"
							));*/
		}
		$this->set('title_for_layout', 'Change Password');
	}
	
	function mail($to,$from,$subject,$message,$success_msg){
		$Email = new CakeEmail();
		if(isset($from) && isset($to) && isset($subject) && isset($message)){
				$Email->from(array($from=>'DevelopAPlayer'));
				$Email->to($to);
				$Email->subject($subject);
				$Email->emailFormat('html');
				$Email->send($message);
				$this->Session->setFlash(__($success_msg));
		}else{
			$this->Session->setFlash(__("There was an error while sending reset password link. Try Again after a while."));
		}
	}
	
	function check_login(){
		$this->layout="ajax";
		$this->render(false);
		if(!$this->Session->check('Auth.User')){
			echo 1;
		}else{
			echo 0;
		}
	}
	
	function reset_pass($reset_link=""){
		$this->layout = 'admin_login';
		if(!empty($reset_link)){
			$user_data = $this->User->find('first',array('conditions'=>array('User.reset_pass_link'=>$reset_link)));
			if(!empty($user_data)){
				if($this->request->is('post')){
				$this->User->id = $user_data['User']['id'];
				$forget_pass_data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
				$forget_pass_data['User']['reset_pass_link'] = "";
				$this->User->save($forget_pass_data);
				$this->Session->setFlash("Password has been succesfully reset.",'default',array('class'=>'alert alert-success'));
				$this->redirect(array("controller" => "users", 
								  "action" => "index",
								  "plugin" => "admin"
							));
			}
				$this->set('user_data',$user_data);
			}else{
				$this->redirect(array("controller" => "users", 
								  "action" => "index",
								  "plugin" => "admin"
							));
			}
		}
	}
	
}