<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class SubscribersController extends AdminAppController {	
	var $name = 'Subscribers';
	var $uses = array('Newslatter');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Subscriber','view');
		$view_content = $this->Newslatter->find('all');	
		$this->set(compact('view_content'));
       
	}	
	
	public function delete_page($id){
		$this->check_Permission('Subscriber','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);	
		if($this->Newslatter->delete($id)){			
			$this->Session->setFlash(__('Subscriber Email Succesfully Deleted..'));
			$this->redirect(array("action" => "index"));
		}
	}
	
	public function subscribers_multiple_delete()
	{		
		$this->check_Permission('Subscriber','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		
		foreach($this->request->data['id'] as $id){
			$this->Newslatter->delete($id);
		}
		$this->Session->setFlash("All Subscribers Email Successfully Deleted..");
		$this->redirect(array("action" => "index"));
	}
	
    
	public function download_subscribe_xls()
	{
		$flag = false;
		$data=array();

		$view_content = $this->Newslatter->find('all');	
		foreach ($view_content as $key=>$value){		
			$data[$key]['Subscribe Email']=$view_content[$key]['Newslatter']['email'];		
		}

				

		// file name for download
		$filename = "subscriber" . date('Ymd') . ".xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel");
		
	
		foreach($data as $row) 
		{
			if(!$flag) {
				echo implode("\t", array_keys($row)) . "\n";
				$flag = true;
			}
		
		 array_walk($row,function(&$row)
	     {
		$row = preg_replace("/\t/", "\\t", $row);
		$row= preg_replace("/\r?\n/", "\\n", $row);
		if(strstr($row, '"')) $row = '"' . str_replace('"', '""', $row) . '"';
		
	     });
			
			echo implode("\t", array_values($row)) . "\n";
		
		}
		exit;  
	}
	

}
