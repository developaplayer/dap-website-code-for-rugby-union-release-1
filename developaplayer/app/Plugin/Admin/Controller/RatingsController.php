<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class RatingsController extends AdminAppController {
	var $uses=array('Admin.Country','Admin.User','Admin.AddPlayerRating','Admin.CoachDetail','Admin.CoachSpeciality','Admin.Certificate','Admin.CoachDetail','Admin.CoachCertificate','Admin.Video','Admin.CoachCategory','Admin.CoachLevel','Admin.LavelAssignment','Admin.AddCoachRating','Admin.AddCoachRating','Admin.UpperLowerLimit','Admin.AgeGroup','Admin.LevelCategory','Admin.PlayerPosition');
	 public function beforeFilter() 
	 {
	   parent::beforeFilter();      
     }
	 
	//========================== Add Edit Coach Rating   ====================================================
	public function  add_rating($id="")
	{
		$id=$this->request->params['pass'][0];		 
		$all_rating_list=$this->AddPlayerRating->find('all',array('conditions'=>array('AddPlayerRating.player_id'=>$id))); 

		if(count($all_rating_list)>0)
		{			 
			//$all_rating_list=$this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.coach_id'=>$id))); 
			$this->set('all_rating_list', $all_rating_list);
			
		}else
		{     $all_rating_list=array();
		  $this->set('all_rating_list', $all_rating_list);
		}
		
	//======================================================================
		
		$PlayerPosition=$this->PlayerPosition->find('list');	
		$all_coach_category_list=$this->LevelCategory->find('list');		
		$AgeGroup_list=$this->AgeGroup->find('list');		
		$UpperLowerLimit_list=$this->UpperLowerLimit->find('all');	
		
		//==================================================================
		
		foreach($UpperLowerLimit_list as $UpperLowerLimit_lists)
		{
			$UpperLowerLimit_list_val[$UpperLowerLimit_lists['UpperLowerLimit']['id']]=$UpperLowerLimit_lists['UpperLowerLimit']['lower_limit'].'-'.$UpperLowerLimit_lists['UpperLowerLimit']['upper_limit'];			 
		}
		
		$this->set('all_coach_category_list', $all_coach_category_list);
		$this->set('AgeGroup_list',$AgeGroup_list);
		$this->set('UpperLowerLimit_list',$UpperLowerLimit_list_val);
        $this->set('PlayerPosition',$PlayerPosition);
		
		//=============check id exits rating id===============================================		
	
		$add_rating_arr=array();
		if($this->request->data)
		{
			$k=1;					
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{	$this->AddPlayerRating->create();
					$add_rating_arr['AddPlayerRating']['coach_id']=$id;				
					$add_rating_arr['AddPlayerRating']['position_id']=$this->request->data['position_1'];
					$add_rating_arr['AddPlayerRating']['coach_cat_id']=$this->request->data['coach_category_hidden_'.$i];
					$add_rating_arr['AddPlayerRating']['rating_limit_id']=0;
					$add_rating_arr['AddPlayerRating']['rating']=$this->request->data['rating_val_'.$i];
					$add_rating_arr['AddPlayerRating']['age_group_id']=$this->request->data['age_group_1'];
					$add_rating_arr['AddPlayerRating']['lower_limit']=$this->request->data['lower_limit_1'];
					$add_rating_arr['AddPlayerRating']['upper_limit']=$this->request->data['upper_limit_1'];
					$add_rating_arr['AddPlayerRating']['player_id']=$id;
					if($this->request->data['rating_val_'.$i]!="")
					{
						$conditions = array(
								'AddPlayerRating.position_id' => $add_rating_arr['AddPlayerRating']['position_id'],
								'AddPlayerRating.coach_cat_id' => $add_rating_arr['AddPlayerRating']['coach_cat_id'],
								'AddPlayerRating.age_group_id' => $add_rating_arr['AddPlayerRating']['age_group_id'],
								//'AddPlayerRating.rating_limit_id' => $add_rating_arr['AddPlayerRating']['rating_limit_id'],
								'AddPlayerRating.player_id' => $add_rating_arr['AddPlayerRating']['player_id']
							);
						$chekIfExist = $this->AddPlayerRating->find('first',array('fields'=>'AddPlayerRating.id','conditions'=>$conditions));
						if(!empty($chekIfExist)){
							$this->AddPlayerRating->id = $chekIfExist['AddPlayerRating']['id'];
						}
					   $this->AddPlayerRating->save($add_rating_arr);
					}					
					$k++;
				}
			
			if(count($all_rating_list)>0)
			{
					$this->Session->setFlash(__('Rating has been Successfully Updated ...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'ratings',
								'action' => 'view_rating',
								$id
								));
			}else{
			$this->Session->setFlash(__('Rating has been Successfully Added ...','default',array('class'=>'alert alert-success alert-dismissable')));
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'ratings',
						'action' => 'view_rating',
						$id
						));
			}						
		}
	}
	
	function view_rating($id='')
	{
		
		 $this->AddPlayerRating->bindModel(array(
			   'hasOne' => array('PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
                                     'conditions'=>'AddPlayerRating.position_id=PlayerPosition.id', 									 
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>'PlayerPosition.name,PlayerPosition.id'
								),
								'AgeGroup'=>array(
									 'className'=>'AgeGroup',
                                     'conditions'=>'AddPlayerRating.age_group_id=AgeGroup.id', 									 
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'CoachCategory'=>array(
									 'className'=>'CoachCategory',
                                     'conditions'=>'AddPlayerRating.coach_cat_id=CoachCategory.id', 									 
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
		                    )
						)
					);
		
		//array('group' => 'AddPlayerRating.coach_cat_id')
		$all_rating_list=$this->AddPlayerRating->find('all',array('conditions'=>array('AddPlayerRating.player_id'=>$id)));
		$this->set('all_rating_list',$all_rating_list);
		
		//pr($all_rating_list);
		//exit;
		
		/*if(empty($id)){
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'player_profile',
							'action' => 'index'
							));
		}else{
			$UserData = $this->User->find('first',array('fields'=>'User.id','conditions'=>array('User.id'=>$id)));
			if(empty($UserData)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'player_profile',
							'action' => 'index'
							));
			}
		}*/
	}
	
	
	
}