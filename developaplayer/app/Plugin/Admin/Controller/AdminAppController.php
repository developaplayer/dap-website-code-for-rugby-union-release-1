<?php
//App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
class AdminAppController extends AppController {
	var $pageLimit=30;
	var $projecturl="http://192.168.1.23/developaplayer/";
	public $helpers = array('Html', 'Form','Cache');
	public $uses = array('Admin.User');
	public $components = array(
		 'Email',
		 'Session',
		 'Paginator',
	 'Auth' => array(
		 'loginAction' => array('plugin' => 'admin','controller' => 'users','action' => 'login'),
		 'loginRedirect' => array('plugin'=>'admin','controller' => 'users', 'action' => 'dashboard'),
		 'logoutRedirect' => array('plugin'=>'admin','controller' => 'users', 'action' => 'login'),
		 'authError' => 'You must be logged in to view this page.',
		 'loginError' => 'Invalid Username or Password entered, please try again.',
		 'authenticate' => array(
			'Form' => array(
                'userModel'=> 'User',			
				'fields' => array('username' => 'email_address','password' => 'password'),				
				'scope' => array('OR' => array(array('User.status' => 'A','User.type' => 'M'), array('User.status' => 'A','User.type' => 'A')))			
			 )
		   )
		 )
	);
 
		// only allow the login controllers only
	public function beforeFilter() {				
		if($this->Session->check('Auth.User')){	
			$controller_name=preg_replace("/s\b/", "",$this->params['controller']);
			$controller_name=ucwords($controller_name);
			
			if($this->Auth->user('type')=='M')
			{
				if(!$this->Session->check('permissions'))
				{					
					$this->loadModel('Admin.AdminMenuPermission');
					$this->loadModel('Admin.AdminMenu');
							
					$this->AdminMenuPermission->bindModel(
								array('belongsTo'=>array(
											'AdminMenu'=>array(
											'className'=>'AdminMenu',
											'foreignKey'=>'admin_menu_id'
												))
											));
						$menu_permission=$this->AdminMenuPermission->find('all',array('conditions'=>array(
						'AdminMenuPermission.moderator_id'=>$this->Auth->user('id'))));
						
						
						
						$permissions = array();
						$permissions_new = array();
						foreach($menu_permission as $permission){
							
							$name=str_replace(' ','_',$permission['AdminMenu']['menu_name']);
							$permissions[$name]['view']=$permission['AdminMenuPermission']['view_permission'];
							$permissions[$name]['add']=$permission['AdminMenuPermission']['add_permission'];
							$permissions[$name]['edit']=$permission['AdminMenuPermission']['edit_permission'];
							$permissions[$name]['delete']=$permission['AdminMenuPermission']['delete_permission'];
							$permissions[$name]['status']=$permission['AdminMenuPermission']['status_permission'];	
                           //====================================================================================//							   
							$controller_name=$permission['AdminMenu']['controller'];
							$permissions_new[$controller_name]['view']=$permission['AdminMenuPermission']['view_permission'];
							$permissions_new[$controller_name]['add']=$permission['AdminMenuPermission']['add_permission'];
							$permissions_new[$controller_name]['edit']=$permission['AdminMenuPermission']['edit_permission'];
							$permissions_new[$controller_name]['delete']=$permission['AdminMenuPermission']['delete_permission'];
						}
						  $this->Session->write('permissions',$permissions);
						  $this->Session->write('permissions_new',$permissions_new);
						// $permissionsk=$this->Session->read('permissions.Player_Profile.view');
						
					}
				
				//echo $controller_name;
				//exit;
				/*if($this->Session->check('permissions.'.$controller_name.'.view')==false || $this->Session->read('permissions.'.$controller_name.'.view')=='N')
				{
						if($this->params['action']!='dashboard'){
							if($this->params['action']!='login'){
								if($this->params['action']!='logout'){
									$this->redirect(array(
												'plugin'=>'admin',
												'controller'=>'users',
												'action'=>'dashboard'
												));
								}
							}
						}
					} */
				
			}else{
				if($this->Session->check('permissions')){				
				  $this->Session->destroy('permissions');
				   $this->Session->destroy('permissions_new',$permissions_new);
				}
			} 
			
			$name = $this->request->params['controller'];
			$action =$this->request->params['action'];
			if($action='index')
			{
				$permission='view';
			}else if($action='edit')
			{
				$permission='edit';
			}else 
			{
				$permission='add';
			}	
			
			$this->check_Permission_gobal($name=NULL,$permission=NULL);
			
		} 
		
		/*$this->loadModel('Product');
		$this->loadModel('ProductToWarehouse');
			$this->Product->unbindModel(array(
							'hasMany'=>array('ProductToAttribute','ProductToCategory','ProductDiscount','ProductToWarehouse'),
							'belongsTo'=>array('UnitType')
								));
			$this->ProductToWarehouse->bindModel(array(
								'belongsTo'=>array(
									'Warehouse'=>array(
												'className'=>'Warehouse',
												'foreignKey'=>'warehouse_id'
											))
										)); */
			/*$this->Product->bindModel(array(
								'hasMany'=>array(
									'ProductToWarehouse'=>array(
												'className'=>'ProductToWarehouse',
												'foreignKey'=>'product_id',
												'conditions'=>array('ProductToWarehouse.qty'=>0)
											))
										));*/
			//$this->Product->recursive=2;
			//$this->ProductToWarehouse->unbindModel(array('belongsTo'=>array('Product')));
			//$notifications=$this->Product->find('all',array('fields'=>array('Product.id','Product.name','Product.slug_url'),'conditions'=>array('Product.status'=>1)));
			
			//$this->set('notifications',$notifications);
			//$this->loadModel('Setting');
			//$settings=$this->Setting->find('all');
			//$setting="";
			//foreach($settings as $data){
			//$setting[$data['Setting']['name']]=$data['Setting']['status'];
			//}
			//$data='';
			//$this->set('setting',$setting);
			//$this->set('permissions',$permissions);
			//$this->permission();
		}
		
		public function generate_slug_url($str){
		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_| -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_| -]+/", '-', $clean);
		$product_slug_check=$this->Product->find('first',array('conditions'=>array('Product.slug_url'=>$clean)));
		if(empty($product_slug_check)){
			return $clean;
		}else{
			if($this->params['action']=="add_product"){
				$num=count($product_slug_check)+1;
				return $clean."_".$num;
			}else{
				return $clean;
			}
		}
	}
	
	public function beforeRender(){
		$this->loadModel('Contact');
		$contacts_header=$this->Contact->find('all',array('conditions'=>array('Contact.status'=>'U')));
			/*echo "<pre>";
			print_r($contacts_header);
			die;*/
			$this->set('contacts_header',$contacts_header);
	}
	
	/*public function permission(){
		$action_name=$this->params['action'];
		if(isset($this->params['pass'][0])){
			$pass_name=$this->params['pass'][0];
			echo $pass_name;
			die;
		}else{
			$pass_name="";
		}
		if($this->Auth->user('type')=='M'){
			$admin_menu_permissions=$this->AdminMenuPermission->find('all',array('conditions'=>array('AdminMenuPermission.moderator_id'=>$this->Auth->user('id'))));
			$admin_menus=$this->AdminMenu->find('all');
			foreach($admin_menu_permissions as $admin_menu_permission){
				if($this->params['controller']==$admin_menu_permission['AdminMenu']['controller']){
					if($admin_menu_permission['AdminMenuPermission']['add_permission']<>'Y'){
						if($action_name=='add_user' || $action_name=='add_attribute' || $action_name=='add_delivery_boy' || $action_name=='add_banner' || $action_name=='add_banners' || $action_name=='add_product' || $action_name=='add_category' || $action_name=='add_manufacturer' || $action_name=='add_unit_type' || $action_name=='add_warehouse' || $action_name=='add_pincode'){
							$this->redirect(array("controller"=>"users","action" => "dashboard"));
						}
					}else if($admin_menu_permission['AdminMenuPermission']['edit_permission']<>'Y'){
								if($admin_menu_permission['AdminMenuPermission']['edit_permission']<>'Y'){
									if($action_name=='user_details' || $action_name=='user_details' || $action_name=='banner_detail'){
										$this->redirect(array("controller"=>"users","action" => "dashboard"));
									}
						}
					}else
						if($admin_menu_permission['AdminMenuPermission']['delete_permission']<>'Y'){
							if($action_name=='user_delete' || $action_name='user_multiple_delete' || $action_name=='banner_delete'){
								$this->redirect(array("controller"=>"users","action" => "dashboard"));
							}
						}
					else
						if($admin_menu_permission['AdminMenuPermission']['view_permission']<>'Y'){
							if($action_name=='view_all' || $action_name=='all_newsletters' || $action_name=='all_suggestion' || $action_name=='all_waitings' || $action_name=='all_wishlist' || $action_name=='all_pages' || $action_name=='all_shipping_details' || $action_name=='all_orders' || $action_name=='all_pincode' || $action_name=='all_warehouse' || $action_name=='all_unit_type' || $action_name=='all_attribute' || $action_name=='all_manufacturer' || $action_name=='all_category' || $action_name=='all_products' || $action_name=='all_banners' || $action_name=='all_banner' || $action_name=='all_delivery_boy' || $action_name=='all_admin_moderators'  || $action_name=='banner_detail' || $action_name=='all_contacts'){
								$this->redirect(array("controller"=>"users","action" => "dashboard"));
							}
						}
					}
				}
			}
		}*/
	
	public function check_Permission($name,$permission)
	  {
		  
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.'.$name.'.'.$permission)=='N')
        {     
                $this->Session->setFlash(__('<strong>Super Admin Not given permission for this panel.If any problem please contact Admin : contact@developaplayer.com</strong>'));				
			   //echo '<script>'.'window.location.href = "http://www.google.com" ' . '</script>';
			   //return true;
               $this->redirect(array('controller'=>'users','action'=>'dashboard'));              
        }   
		
	}
	
	public function check_Permission_gobal($name,$permission)
	  {
		 
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions_new.'.$name.'.'.$permission)=='N')
        {  
                $this->Session->setFlash(__('<strong>Super Admin Not given permission for this panel.If any problem please contact Admin : contact@developaplayer.com</strong>'));					   
                $this->redirect(array('controller'=>'users','action'=>'dashboard'));
                
        }   
		
	}
 
}
