<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class GenericPagesController extends AdminAppController {
	var $uses=array('Admin.Page','Admin.Banner');
	 public function beforeFilter() {
	   parent::beforeFilter();
    }
	public function index(){
		$this->redirect(array('action'=>'about_us'));
		$this->check_Permission('Cms_Page','view');
	}
	public function about_us(){
		if($this->request->data){
			$page_update['Page'] = $this->request->data;
			$this->Page->id = $this->request->data['id'];
			if($this->Page->save($page_update)){
				$this->Session->setFlash(__('About Us Page is Updated..','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('An Unexpected Error Occured While Updating About Us Page Details...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
		}
		$page = $this->Page->find('first',array('conditions'=>array('Page.slug_url'=>'about')));
		if(empty($page)){
			$page_detail['Page']['title'] = 'About Us';
			$page_detail['Page']['slug_url'] = 'about';
			$this->Page->save($page_detail);
			$page = $this->Page->find('first',array('conditions'=>array('Page.slug_url'=>'about')));
		}
		$this->set('page',$page);
	}
	public function terms_conditions(){
		if($this->request->data){
			$page_update['Page'] = $this->request->data;
			$this->Page->id = $this->request->data['id'];
			if($this->Page->save($page_update)){
				$this->Session->setFlash(__('Terms &#38; Conditions Page is Updated..','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('An Unexpected Error Occured While Updating Terms &#38; Conditions Page Details...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
		}
		$page = $this->Page->find('first',array('conditions'=>array('Page.slug_url'=>'terms-conditions')));
		if(empty($page)){
			$page_detail['Page']['title'] = 'Terms &#38; Conditions';
			$page_detail['Page']['slug_url'] = 'terms-conditions';
			$this->Page->save($page_detail);
			$page = $this->Page->find('first',array('conditions'=>array('Page.slug_url'=>'terms-conditions')));
		}
		$this->set('page',$page);
	}
	public function banner(){
	}
	public function add_banner(){
		if($this->request->is('post')){
			$data_image = $_FILES['image'];
				  $foo = new Upload($data_image); 
				  $foo->allowed = array('image/jpg/jpeg');
					if ($foo->uploaded) {
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/original/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 765;
						   $foo->image_x               = 1920;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/modified_banner/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 60;
						   $foo->image_x               = 140;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/thumb/');
						  if ($foo->processed) {
								$image_name= $foo->file_dst_name;
								$data['image']=$image_name;
								$data['name']=$this->request->data['name'];
								$data['status']=$this->request->data['status'];
								if($this->Banner->save($data))
									{
										$this->Session->setFlash(_('Banner Succesfully Added..'));
										$this->redirect(array(
												'action' => 'banner'
											));
									}
							  }else{
								  $this->Session->setFlash(_('Invalid Image File!!'));
								  $this->redirect(
										array("action" => "add_banner"));
							  }
					}
		}
	}
	public function edit_banner($id=""){
		if($id==""){
			$this->redirect(array(
						'action' => 'banner'
						));
		}
		$banner_data = $this->Banner->find('first',array('conditions'=>array('Banner.id'=>$id)));
		if(empty($banner_data)){
			$this->redirect(array('action' => 'banner'));												
		}
		if($this->request->is('post')){
			$check_banner = $this->Banner->find('first',array('conditions'=>array('Banner.name'=>$this->request->data['name'])));
			if(!empty($check_banner) && $banner_data['Banner']['name']!=$check_banner['Banner']['name']){
				$this->Session->setFlash(__('Error in Updating Banner, Name Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				if($_FILES['image']['name']!="" && $_FILES['image']['size']>0)
				{
				  $data_image = $_FILES['image'];
				  $foo = new Upload($data_image); 
				  $foo->allowed = array('image/jpg/jpeg');
					if ($foo->uploaded) {
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/original/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 765;
						   $foo->image_x               = 1920;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/modified_banner/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 60;
						   $foo->image_x               = 140;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/banners/thumb/');
						  if($foo->processed)
							  {
								$image_name= $foo->file_dst_name;
								$data['image']=$image_name;
								$data['name']=$this->request->data['name'];
								$data['status']=$this->request->data['status'];
								$this->Banner->id = $banner_data['Banner']['id'];
							  }
					     } 
					}else
					{
					            //$image_name= $foo->file_dst_name;
								//$data['image']=$image_name;
								$data['name']=$this->request->data['name'];
								$data['status']=$this->request->data['status'];
								$this->Banner->id = $banner_data['Banner']['id'];	
						
					}
					
				if($this->Banner->save($data))
					{
						  if(!empty($_FILES['image']['name'])){
							@unlink('uploads/banners/original/'.$banner_data['Banner']['image']);
							@unlink('uploads/banners/thumb/'.$banner_data['Banner']['image']);
							@unlink('uploads/banners/modified_banner/'.$banner_data['Banner']['image']);
					}
						$this->Session->setFlash(__('Banner Succesfully Updated...','default',array('class'=>'alert alert-success alert-dismissable')));
						$this->redirect(array(
							'action' => 'banner'
							));
					}
				  }
				}
		  $this->set('banner',$banner_data);
	 }
	 
	public function banner_data(){
		$this->layout="ajax";
		$this->render(false);
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Banner.id',
			1 =>'Banner.image',
			2 =>'Banner.name',
			3 =>'Banner.status',
			4 =>'Banner.created',
		);
		$totalData = $this->Banner->find('count'); 
		$totalFiltered = $totalData; 
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Banner.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR Banner.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Banner.created LIKE '".$requestData['search']['value']."%' )";
		}
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$clubs = $this->Banner->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($clubs);
		}
		$data = array();
		foreach( $clubs as $row ) {  // preparing an array
			$nestedData=array();
			$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Banner']["id"].'" type="checkbox" />';
			$nestedData[] = '<div id="banner_logo_'.$row['Banner']["id"].'"><img src="'.$this->webroot.'uploads/banners/thumb/'.$row['Banner']["image"].'" /></div>';
			$nestedData[] = '<div id="club_name_'.$row['Banner']["id"].'">'.$row['Banner']["name"].'</div>';
			if($row['Banner']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			$nestedData[] = '<div id="status_of_'.$row['Banner']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['Banner']["created"]));
			$nestedData[] = '<a href="javascript:change_status('.$row['Banner']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Banner']['id'].'"><img id="status_img_'.$row['Banner']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/generic_pages/edit_banner/'.$row['Banner']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Banner']['name'].'?\')" href="'.$this->webroot.'admin/generic_pages/delete_banner/'.$row['Banner']['id'].'" title="Delete" id="delete_'.$row['Banner']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			$data[] = $nestedData;
		}
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
		echo json_encode($json_data);  // send data as json format
	}
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Banner']['status'] = $this->request->data['status'];
			$this->Banner->id = $this->request->data['id'];
			if($this->Banner->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	function delete_banner($id=""){
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			$image = $this->Banner->find('first',array('fields'=>'Banner.image','conditions'=>array('Banner.id'=>$id)));
			@unlink('uploads/banners/original/'.$image['Banner']['image']);
			@unlink('uploads/banners/thumb/'.$image['Banner']['image']);
			@unlink('uploads/banners/modified_banner/'.$image['Banner']['image']);
			if($this->Banner->delete($id)){
				$this->Session->setFlash(__('Banner Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Banner...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'action' => 'banner'
						));
		}
	}
	function delete_banner_multiple(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Banner->create();
				$image = $this->Banner->find('first',array('fields'=>'Banner.image','conditions'=>array('Banner.id'=>$id)));
				@unlink('uploads/banners/original/'.$image['Banner']['image']);
				@unlink('uploads/banners/thumb/'.$image['Banner']['image']);
				@unlink('uploads/banners/modified_banner/'.$image['Banner']['image']);
				$this->Banner->delete($id);
				$this->Session->setFlash(__('Banners Successfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'action' => 'banner'
						));
	}
}