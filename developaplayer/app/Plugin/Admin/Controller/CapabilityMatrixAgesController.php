<?php
App::uses('AdminAppController', 'Admin.Controller');
class CapabilityMatrixAgesController extends AdminAppController {
	var $uses=array('Admin.CapabilityMatrixAge','Admin.LevelCategory','Admin.AgeGroup');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index(){           $this->check_Permission('Capability_Matrix_Position','view');
		
	}
	//======================= List View Data  =======================================
	public function get_all_data()	{
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'CapabilityMatrixAge.id',
			1 =>'AgeGroup.title',
			2 =>'LevelCategory.name',
			3 =>'CapabilityMatrixAge.lower_limit',
			4 =>'CapabilityMatrixAge.upper_limit',		
		);
		$totalData = $this->CapabilityMatrixAge->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( AgeGroup.title LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR LevelCategory.name LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR CapabilityMatrixAge.lower_limit = '".$requestData['search']['value']."' ";
			$sql.=" OR CapabilityMatrixAge.upper_limit = '".$requestData['search']['value']."' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$Matrixage = $this->CapabilityMatrixAge->find('all',array('conditions'=>$sql,'order'=>$order_by,'limit'=>$requestData['length'],'offset' => $requestData['start']));
		//pr($Matrixage);
		//exit();
		
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($Matrixage);
		}
		$data = array();
		foreach( $Matrixage as $row ) {  // preparing an array
			$nestedData=array();

			$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['CapabilityMatrixAge']["id"].'" type="checkbox" />';
			$nestedData[] = '<div id="club_title_'.$row['LevelCategory']["id"].'">'.$row['LevelCategory']["name"].'</div>';			
			$nestedData[] = '<div id="club_title_'.$row['AgeGroup']["id"].'">'.$row['AgeGroup']["title"].'</div>';			
			$nestedData[] = '<div id="club_initial_age_'.$row['CapabilityMatrixAge']["id"].'">'.$row['CapabilityMatrixAge']["lower_limit"].'</div>';			
			$nestedData[] = '<div id="club_final_age_'.$row['CapabilityMatrixAge']["id"].'">'.$row['CapabilityMatrixAge']["upper_limit"].'</div>';			
			/*if($row['AgeGroup']["status"]=='Y'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'N';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'Y';
			}*/
			
			/*$nestedData[] = '<div id="status_of_'.$row['AgeGroup']["id"].'">'.$status.'</div>';
			
			$nestedData[] = date('d-M-Y',strtotime($row['AgeGroup']["created"]));*/
			
			$nestedData[] = '<a href="'.$this->webroot.'admin/capability_matrix_ages/edit/'.$row['CapabilityMatrixAge']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete upper limit '.$row['CapabilityMatrixAge']['upper_limit'].'?\')" href="'.$this->webroot.'admin/capability_matrix_ages/delete_age_capability/'.$row['CapabilityMatrixAge']['id'].'" title="Delete" id="delete_'.$row['CapabilityMatrixAge']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';		
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	
	public function add_capabilitymatrix_age()	{
		 $this->check_Permission('Capability_Matrix_Position','add');
		if($this->request->is('post')){
			//pr($this->request->data);
			//exit();
			//$this->AgeGroup->find('all',array('conditions'=>array('AgeGroup.title')));
			$data['CapabilityMatrixAge']['level_category_id']=$this->request->data['level_category_id'];
			$data['CapabilityMatrixAge']['age_group_id']=$this->request->data['age_group_id'];
			$data['CapabilityMatrixAge']['lower_limit']=$this->request->data['lower_limit'];
			$data['CapabilityMatrixAge']['upper_limit']=$this->request->data['upper_limit'];
			if($data['CapabilityMatrixAge']['lower_limit']>$data['CapabilityMatrixAge']['upper_limit']){
				$this->Session->setFlash('Upper limit Must Be Greater Than lower limit..','default',array('class'=>'alert alert-danger alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'add_capabilitymatrix_age'
					));
			}
			//pr($data);
			//exit();
			if($this->CapabilityMatrixAge->save($data)){
				$this->Session->setFlash('Capability matrix by ages Successfully Added..','default',array('class'=>'alert alert-success alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
					));
			}else{
				$this->Session->setFlash('Error in Creating Age Group!!','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
		}
	}
	
	
	function status(){	  $this->check_Permission('Capability_Matrix_Position','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['AgeGroup']['status'] = $this->request->data['status'];
			$this->AgeGroup->id = $this->request->data['id'];
			if($this->AgeGroup->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_age_capability($id="")	{		$this->check_Permission('Capability_Matrix_Position','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->CapabilityMatrixAge->delete($id)){
				$this->Session->setFlash('Capability Matrix By Age Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable'));
			}else{
				$this->Session->setFlash('Error in Deleting AgeGroup...','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=""){
		$this->check_Permission('Capability_Matrix_Position','edit');
		if($id==""){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
						));
		}
		if($this->request->is('post')){
			$data['CapabilityMatrixAge']['level_category_id']=$this->request->data['level_category_id'];
			$data['CapabilityMatrixAge']['age_group_id']=$this->request->data['age_group_id'];
			$data['CapabilityMatrixAge']['lower_limit']=$this->request->data['lower_limit'];
			$data['CapabilityMatrixAge']['upper_limit']=$this->request->data['upper_limit'];
			if($data['CapabilityMatrixAge']['lower_limit']>$data['CapabilityMatrixAge']['upper_limit']){
				$this->Session->setFlash('Upper limit Must Be Greater Than Lower Limit..','default',array('class'=>'alert alert-danger alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'edit',
						$id
					));
			}
			$this->CapabilityMatrixAge->id = $id;
			if($this->CapabilityMatrixAge->save($data)){
				$this->Session->setFlash('Capability matrix by ages Successfully Updated..','default',array('class'=>'alert alert-success alert-dismissable'));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
					));
			}else{
				$this->Session->setFlash('Error in Updating Capability matrix by ages!!','default',array('class'=>'alert alert-danger alert-dismissable'));
			}
		}
		$CapabilityArray = $this->CapabilityMatrixAge->find('first',array('conditions'=>array('CapabilityMatrixAge.id'=>$id)));
		//pr($CapabilityArray);
		//exit();
		if(empty($CapabilityArray)){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
						));
		}
		$this->set('CapabilityArray',$CapabilityArray);
	}
	
	function delete_group_multiple()	{        $this->check_Permission('Capability_Matrix_Position','delete'); 
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->CapabilityMatrixAge->create();
				$this->CapabilityMatrixAge->delete($id);
				$this->Session->setFlash('CapabilityMatrixAge Successfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable'));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'capability_matrix_ages',
						'action' => 'index'
						));
	}
	//======================== Get All Category For Ajax  ===============================================
	function get_category(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$categories=$this->LevelCategory->find('all');
			$data="";
			foreach($categories as $category){
				if($id!="no"){
					if($category['LevelCategory']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$category['LevelCategory']['id'].'" id="'.$category['LevelCategory']['id'].'">'.$category['LevelCategory']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'capability_matrix_ages',
							'action' => 'index'
							));
		}
	}
	//======================== Get All AgeGroup For Ajax  ===============================================
	function get_agegroup(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$AgeGroup=$this->AgeGroup->find('all');
			$data="";
			foreach($AgeGroup as $AgeGroupval){
				if($id!="no"){
					if($AgeGroupval['AgeGroup']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$AgeGroupval['AgeGroup']['id'].'" id="'.$AgeGroupval['AgeGroup']['id'].'">'.$AgeGroupval['AgeGroup']['title'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'capability_matrix_ages',
							'action' => 'index'
							));
		}
	}
	
}