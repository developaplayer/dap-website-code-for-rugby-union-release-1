<?php
App::uses('AdminAppController', 'Admin.Controller');
class CourseController extends AdminAppController {
	var $uses=array('Admin.Level','Admin.LevelCategory','Admin.Course','Admin.AgeGroup','Admin.CourseCoach','Admin.CourseAgeGroup','Admin.CourseDate','Admin.PlayerPosition','Admin.CourseToPosition');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Course','view');
	}
	//==================== Display course data in Listing page =============================
	public function course_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Course.id',
			1 =>'Course.course_title',
			//2 =>'AgeGroup.title',
			2 =>'LevelCategory.name',
			3 =>'Course.location',
			4 =>'Course.duration',
			5 =>'Course.cost',
			6 =>'Course.status'
		);
		$totalData = $this->Course->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		$category_sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Course.course_title LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Course.location LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Course.cost LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Course.duration LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Course.status LIKE '".substr($requestData['search']['value'],0,1)."%' ";
			$sql.=" OR Course.created LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR LevelCategory.name LIKE '".$requestData['search']['value']."%' ";
			$sql.= ")";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		/*$this->Course->bindModel(
				array(
						'belongsTo'=>array(
							'LevelCategory'	=>	array(
									'className'	=>	'LevelCategory',
									'foreignKey'	=>	'category_id',
									'dependent'	=>	true
									)
							
							'AgeGroup'	=>	array(
									'className'	=>	'AgeGroup',
									'foreignKey'	=>	'age_group_id',
									'dependent'	=>	true
									)
								)
				));*/
				
		/*if( !empty($requestData['search']['value']) ) {   		
			$levels = $this->Course->find('all',array(
												'fields'=>array('DISTINCT Course.id','Course.course_title','Course.duration','Course.location','Course.cost','Course.status','Course.created','LevelCategory.name'),
												'conditions'=>$sql,
												 'joins'=>array(
															array(
																'type'=>'left',
																'table'=>'courses',
																//'alias'=>'LevelCategory',
																'conditions'=>array(
																	'LevelCategory.id = Course.category_id'
																),
																'group' => 'LevelCategory.id'	
															)
														),
															
												'order'=>$order_by,
												'offset' => $requestData['start'],
												'limit'=>$requestData['length'],
												'recursive'=>3
												));
		}else{*/
			$levels = $this->Course->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
													
			//pr($levels);
			//exit();
		//}
												
		/*$log = $this->Level->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($levels);
		die;*/
		/*pr($levels);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($levels);
		}
		$data = array();
		foreach( $levels as $row ) {  // preparing an array
			$nestedData=array();
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y' && $this->Session->read('permissions.Course.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Course']["id"].'" type="checkbox" />';
			}
			$nestedData[] = stripslashes($row['Course']["course_title"]);
			//$nestedData[] = $row['AgeGroup']["title"];
			$nestedData[] = $row['LevelCategory']["name"];
			$nestedData[] = substr($row['Course']["location"],0,30)."...";
			$nestedData[] = $row['Course']["duration"]." mins";
			$nestedData[] = $row['Course']["cost"]." AUD";
			if($row['Course']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Course']["id"].'">'.$status.'</div>';
			
			//$nestedData[] = date('d-M-Y',strtotime($row['Level']["created"]));
			
			//================Course Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y' && $this->Session->read('permissions.Course.status')=='N' && $this->Session->read('permissions.Course.edit')=='N'  && $this->Session->read('permissions.Course.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Course Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y' && $this->Session->read('permissions.Course.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Course']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Course']['id'].'"><img id="status_img_'.$row['Course']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Course Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y' && $this->Session->read('permissions.Course.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/course/edit/'.$row['Course']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Course Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y' && $this->Session->read('permissions.Course.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Course']['course_title'].'?\')" href="'.$this->webroot.'admin/course/delete_course/'.$row['Course']['id'].'" title="Delete" id="delete_'.$row['Course']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Course']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Course']['id'].'"><img id="status_img_'.$row['Course']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/course/edit/'.$row['Course']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Course']['course_title'].'?\')" href="'.$this->webroot.'admin/course/delete_course/'.$row['Course']['id'].'" title="Delete" id="delete_'.$row['Course']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	//================== add course information ======================================
	function add_course(){
		$this->check_Permission('Course','add');
		//pr($this->request->data);
		//exit;
		if($this->request->data){
		 
			$course['Course']=$this->request->data;
			$course_edited['Course']['course_title']= addslashes($this->request->data['course_title']);
			$course_edited['Course']['description']= addslashes($this->request->data['description']);
		
			$check_level = $this->Course->find('first',array('conditions'=>array('Course.course_title'=>$course['Course']['course_title'])));
			if(!empty($check_level)){
				$this->Session->setFlash(__('Course Title Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				
				//==================Course Add======================================================//
				
				$course['Course']['position_id']=0;// for temporary  forcefully//
				$course['Course']['player_certification_level_id']=0;// for temporary  forcefully//
				
				
				if($this->Course->save($course))
				{					
					if(!empty($course['Course']['coach_id']))
					{
						foreach($course['Course']['coach_id'] as $key=>$valCoachId)
						{
							$coachArray['CourseCoach']['course_id'] = $this->Course->id;
							$coachArray['CourseCoach']['coach_id'] = $valCoachId;
							$this->CourseCoach->create();
							$this->CourseCoach->save($coachArray);							
						}
					}	

					if(!empty($course['Course']['age_group_id']))
					{
						foreach($course['Course']['age_group_id'] as $key=>$valAgeId)
						{
							$coachAgeArray['CourseAgeGroup']['course_id'] = $this->Course->id;
							$coachAgeArray['CourseAgeGroup']['age_group_id'] = $valAgeId;
							$this->CourseAgeGroup->create();
							$this->CourseAgeGroup->save($coachAgeArray);						
						}
					}

					//==========Multiple Position Add=====================================//
									
						if(!empty($course['Course']['position_id_multiple']))
						{
							  foreach($course['Course']['position_id_multiple'] as $key=>$position_id)
							   {
								$CourseToPosition['CourseToPosition']['course_id'] = $this->Course->id;
								$CourseToPosition['CourseToPosition']['position_id'] = $position_id;
								$this->CourseToPosition->create();
								$this->CourseToPosition->save($CourseToPosition);						
							   }						
						   }						   						   						 
					//=====================================================================//
					
					if(!empty($course['Course']['course_date']))
					{
						$coachdateArray['CourseDate']['course_id'] = $this->Course->id;
						$coachdateArray['CourseDate']['course_date'] = $course['Course']['course_date'];
						$coachdateArray['CourseDate']['course_start_time'] = $course['Course']['course_start_time'];						
						$this->CourseDate->save($coachdateArray);
					}					
					
					$this->Session->setFlash(__('Course Successfully Added...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'course',
								'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Adding Course...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function status(){
		$this->check_Permission('Course','status');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Course']['status'] = $this->request->data['status'];
			$this->Course->id = $this->request->data['id'];
			if($this->Course->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_course($id=""){
		$this->check_Permission('Course','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->Course->delete($id)){
				$this->Session->setFlash(__('Course Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Course...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'course',
						'action' => 'index'
						));
		}
	}
	
	//======================== Edit Course Details   ============================================
	function edit($id=null){
		$this->check_Permission('Course','edit');
		if($id!=null)
		{
			$course=$this->Course->find('first',array('conditions'=>array('Course.id'=>$id)));
			$this->set('course',$course);
		    $get_position_edit= $this->get_position_edit();
			$get_edit_position= $this->CourseToPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$id)));
			
			$selectd_position=array();
			if(!empty($get_edit_position))
			{
				foreach($get_edit_position as $key=>$value)
				{
					$selectd_position[]=$value['CourseToPosition']['position_id'];
					
					
				}
			}
			
		
			$this->set('all_position_data',$get_position_edit);
			$this->set('selectd_position',$selectd_position);
			$this->set('course',$course);
			if(empty($course)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'course',
						'action' => 'index'
						));
		}
		
		if($this->request->data){
			$course_edited['Course']=$this->request->data;		
		    $course_edited['Course']['course_title']= addslashes($this->request->data['course_title']);
			$course_edited['Course']['description']= addslashes($this->request->data['description']);
			//description
			
			// echo '<pre>';
            // print_r( $course_edited);
//exit;			 
			//$course_edited['Course']=$this->request->data['Course']['course_title'];
			$check_course = $this->Course->find('first',array('conditions'=>array('Course.course_title'=>$course_edited['Course']['course_title'])));
			if($course_edited['Course']['course_title']==$course['Course']['course_title']){
				$check_course="";
			}
			if(!empty($check_course)){
				$this->Session->setFlash(__('Course Title Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				$this->Course->id = $id;
				//pr($course_edited);
				//exit();
				$description=$course_edited['Course']['description'];
				$description  = str_replace("\n"," ",$description);
				$course_edited['Course']['description'] = $description;
				
				if($this->Course->save($course_edited)){
					
					$this->CourseAgeGroup->deleteAll(array('CourseAgeGroup.course_id'=>$id));
					$this->CourseCoach->deleteAll(array('CourseCoach.course_id'=>$id));
					
					
					if(!empty($course_edited['Course']['coach_id']))
					{
						foreach($course_edited['Course']['coach_id'] as $key=>$valCoachId)
						{
							$coachArray['CourseCoach']['course_id'] = $id;
							$coachArray['CourseCoach']['coach_id'] = $valCoachId;
							$this->CourseCoach->create();
							$this->CourseCoach->save($coachArray);						
						}
					}	

                     // pr($course_edited['Course']['position_id_multiple']);
                     // exit;					  
					//==========Multiple Position edit=====================================//										
						if(!empty($course_edited['Course']['position_id_multiple']))
						{
							$this->CourseToPosition->deleteAll(array('CourseToPosition.course_id'=>$id));// delete all existing course data.
							  foreach($course_edited['Course']['position_id_multiple'] as $key=>$position_id)
							   {
								$CourseToPosition['CourseToPosition']['course_id'] = $id;
								$CourseToPosition['CourseToPosition']['position_id'] = $position_id;
								$this->CourseToPosition->create();
								$this->CourseToPosition->save($CourseToPosition);						
							   }						
						   }
					//=========================================================================	   

					if(!empty($course_edited['Course']['age_group_id']))
					{
						foreach($course_edited['Course']['age_group_id'] as $key=>$valAgeId)
						{
							$coachArray['CourseAgeGroup']['course_id'] = $id;
							$coachArray['CourseAgeGroup']['age_group_id'] = $valAgeId;
							$this->CourseAgeGroup->create();
							$this->CourseAgeGroup->save($coachArray);						
						}
					}
					
					if(!empty($course_edited['Course']['course_date']))
					{
						$this->CourseDate->deleteAll(array('CourseDate.course_id'=>$id));// delete all existing course data.
						
						$coachdateArray['CourseDate']['course_id'] = $id;
						$coachdateArray['CourseDate']['course_date'] = $course_edited['Course']['course_date'];
						$coachdateArray['CourseDate']['course_start_time'] = $course_edited['Course']['course_start_time'];
						$this->CourseDate->save($coachdateArray);	
					}
				
					$this->Session->setFlash(__('Course Succesfully Updated...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Updating Course...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function delete_course_multiple()
	{
		$this->check_Permission('Course','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Course->create();
				$this->Course->delete($id);
				$this->Session->setFlash(__('Courses Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'course',
						'action' => 'index'
						));
	}
	
	function get_category(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$categories=$this->LevelCategory->find('all');
			$data="";
			foreach($categories as $category){
				if($id!="no"){
					if($category['LevelCategory']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$category['LevelCategory']['id'].'" id="'.$category['LevelCategory']['id'].'">'.$category['LevelCategory']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}
	}
	
	function get_age_group(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$age_groups=$this->AgeGroup->find('all');
			$data="";
			foreach($age_groups as $age_group){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id){
						if($age_group['AgeGroup']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$age_group['AgeGroup']['id'].'" id="'.$age_group['AgeGroup']['id'].'">'.$age_group['AgeGroup']['title'].'</option>';
			}			
			//echo $data;
			
			echo '<label for="age_group">Select Age Group</label><select multiple id="age_group" style="width:100%" data-placeholder="Select Age Group" class="required form-control valid select2" name="age_group_id[]">'.$data.'</select>';
			
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}
	}
	
	function get_position(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$PlayerPosition=$this->PlayerPosition->find('all');
			$data="";
			foreach($PlayerPosition as $PlayerPositions){
				if($id!="no"){
					if($PlayerPositions['PlayerPosition']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$PlayerPositions['PlayerPosition']['id'].'" id="'.$PlayerPositions['PlayerPosition']['id'].'">'.$PlayerPositions['PlayerPosition']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}
	}
	
	
	function get_player_level()
	{		
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$position_id = $this->request->data['id'];
			//array('conditions'=>array('Level.position_id'=>$position_id);
			$PlayerLevel=$this->Level->find('all');
			if(!empty($PlayerLevel))
			{
				
				$data='<option value="" id="">--Select Your certificate--</option>';
				foreach($PlayerLevel as $PlayerLevels){
					
					$data.='<option  value="'.$PlayerLevels['Level']['id'].'" id="'.$PlayerLevels['Level']['id'].'">Level'.$PlayerLevels['Level']['level_number'].'</option>';
				}
			}else
			{
				$data='<option  value="" >--No Certificate--</option>';
				
				
			}
			
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}
	
		
		
	}
	
	function get_position_edit(){
		
			$PlayerPosition=$this->PlayerPosition->find('list');
			return $PlayerPosition;
		
	
}

}