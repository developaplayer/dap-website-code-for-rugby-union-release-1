<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class ContactsController extends AdminAppController {	
	var $name = 'Contacts';
	var $uses = array('Contact');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Contacts','view');
		$view_content = $this->Contact->find('all');	
		$this->set(compact('view_content'));
	}	
	
	public function delete_page($id){
		$this->check_Permission('Contacts','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);	
		if($this->Contact->delete($id)){			
			$this->Session->setFlash(__('Contact has been Succesfully Deleted..'));
			$this->redirect(array("action" => "index"));
		}
	}
	
	public function contact_multiple_delete()
	{		
		$this->check_Permission('Contacts','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		
		foreach($this->request->data['id'] as $id){
			$this->Contact->delete($id);
		}
		$this->Session->setFlash("Contact has been Successfully Deleted..");
		$this->redirect(array("action" => "index"));
	}
}