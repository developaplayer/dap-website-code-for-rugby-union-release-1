<?php
App::uses('AdminAppController', 'Admin.Controller');
class NationalitiesController extends AdminAppController {
	var $uses=array('Admin.Country');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		 $this->check_Permission('Nationality','view'); 
	}
	
	public function nationality_data()
	{
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Country.id',
			1 =>'Country.country_name',
			2 =>'Country.status',
		);
		$totalData = $this->Country->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Country.country_name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR Country.status LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$levels = $this->Country->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->Country->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($levels);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($levels);
		}
		$data = array();
		foreach( $levels as $row ) {  // preparing an array
			$nestedData=array();
			$nestedData[] = '<div id="nationality_name_'.$row['Country']["id"].'">'.$row['Country']["country_name"].'</div>';
			
			if($row['Country']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Country']["id"].'">'.$status.'</div>';
			
			//$nestedData[] = '<a href="javascript:change_status('.$row['Country']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Country']['id'].'"><img id="status_img_'.$row['Country']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			
			//================Player Nationality Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.status')=='N' && $this->Session->read('permissions.Nationality.edit')=='N'  && $this->Session->read('permissions.Nationality.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Nationality Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Country']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Country']['id'].'"><img id="status_img_'.$row['Country']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Nationality Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="javascript:edit_data('.$row['Country']['id'].',\''.$row['Country']["country_name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Nationality Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Country']['country_name'].'?\')" href="'.$this->webroot.'admin/nationalities/delete_nationality/'.$row['Country']['id'].'" title="Delete" id="delete_'.$row['Country']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Country']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Country']['id'].'"><img id="status_img_'.$row['Country']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['Country']['id'].',\''.$row['Country']["country_name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Country']['country_name'].'?\')" href="'.$this->webroot.'admin/nationalities/delete_nationality/'.$row['Country']['id'].'" title="Delete" id="delete_'.$row['Country']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function status()
	{
		 $this->check_Permission('Nationality','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Country']['status'] = $this->request->data['status'];
			$this->Country->id = $this->request->data['id'];
			if($this->Country->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function add_nationality()
	{
		$this->check_Permission('Nationality','add');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['Country']['country_name']);
			if(!empty($this->request->data['nationality_name'])){
				$check_level = $this->Country->find('first',array('conditions'=>array('Country.country_name'=>$this->request->data['nationality_name'])));
				if(empty($check_level)){
					$data['Country']['country_name'] = $this->request->data['nationality_name'];
					if($this->Country->save($data)){
						$inserted_id = $this->Country->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="nationality_name_'.$inserted_id.'">'.$data['Country']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['Country']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['Country']['name'].'?\')" href="'.$this->webroot.'admin/nationalities/delete_nationality/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'nationalities',
						'action' => 'index'
			));
		}
	}
	
	function delete_nationality($id="")
	{
		$this->check_Permission('Nationality','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->Country->delete($id)){
				$this->Session->setFlash(__('Nation Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Nation...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'nationalities',
						'action' => 'index'
						));
		}
	}
	
	function update_nationality()
	{
		$this->check_Permission('Nationality','edit');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_nationality = $this->Country->find('first',array('conditions'=>array('Country.country_name'=>$this->request->data['nationality_name'])));
			if(!empty($check_nationality)){
				echo "exist";
			}else{
				$data['Country']['country_name'] = $this->request->data['nationality_name'];
				$this->Country->id = $this->request->data['id'];
				if($this->Country->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_nationality_multiple()
	{
		$this->check_Permission('Nationality','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Country->create();
				$this->Country->delete($id);
				$this->Session->setFlash(__('Nationalities Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'nationalities',
						'action' => 'index'
						));
	}
}