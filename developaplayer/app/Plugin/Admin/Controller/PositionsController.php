<?php
App::uses('AdminAppController', 'Admin.Controller');
class PositionsController extends AdminAppController {
	var $uses=array('Admin.PlayerPosition');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		//pr($this->Session->read('permissions.Palyer_positions.status'));die;
		$this->check_Permission('Palyer_positions','view');
		
	}
	
	public function primary_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'PlayerPosition.id',
			1 =>'PlayerPosition.name',
			2 =>'PlayerPosition.status',
			2 =>'PlayerPosition.created',
		);
		$totalData = $this->PlayerPosition->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( PlayerPosition.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR PlayerPosition.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR PlayerPosition.created LIKE '".$requestData['search']['value']."%' )";
		}
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$positions = $this->PlayerPosition->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->PlayerPosition->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($positions);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($positions);
		}
		$data = array();
		foreach( $positions as $row ) {  // preparing an array
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M'&& $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['PlayerPosition']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="position_name_'.$row['PlayerPosition']["id"].'">'.$row['PlayerPosition']["name"].'</div>';
			
			if($row['PlayerPosition']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['PlayerPosition']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['PlayerPosition']["created"]));
			
			//================Player Position Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y'  && $this->Session->read('permissions.Palyer_positions.delete')=='N' && $this->Session->read('permissions.Palyer_positions.edit')=='N' && $this->Session->read('permissions.Palyer_positions.status')=='N')
			{  
				$nestedData[] ="";
			}
			//================Player Positon Add==============================//
			$nestedData_add="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.add')=='Y')
			{  
				//$nestedData[] ="";
			}
			//================Player Positon Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['PlayerPosition']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['PlayerPosition']['id'].'"><img id="status_img_'.$row['PlayerPosition']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Positon Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="javascript:edit_data('.$row['PlayerPosition']['id'].',\''.$row['PlayerPosition']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Positon Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['PlayerPosition']['name'].'?\')" href="'.$this->webroot.'admin/positions/delete_position/'.$row['PlayerPosition']['id'].'" title="Delete" id="delete_'.$row['PlayerPosition']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//============================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['PlayerPosition']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['PlayerPosition']['id'].'"><img id="status_img_'.$row['PlayerPosition']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['PlayerPosition']['id'].',\''.$row['PlayerPosition']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['PlayerPosition']['name'].'?\')" href="'.$this->webroot.'admin/positions/delete_position/'.$row['PlayerPosition']['id'].'" title="Delete" id="delete_'.$row['PlayerPosition']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_position(){
		$this->check_Permission('Palyer_positions','add');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['PlayerPosition']['name']);
			if(!empty($this->request->data['position_name'])){
				$check_position = $this->PlayerPosition->find('first',array('conditions'=>array('PlayerPosition.name'=>$this->request->data['position_name'])));
				if(empty($check_position)){
					$data['PlayerPosition']['name'] = $this->request->data['position_name'];
					$data['PlayerPosition']['type'] = 'P';
					if($this->PlayerPosition->save($data)){
						$inserted_id = $this->PlayerPosition->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="position_name_'.$inserted_id.'">'.$data['PlayerPosition']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['PlayerPosition']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['PlayerPosition']['name'].'?\')" href="'.$this->webroot.'admin/positions/delete_position/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Positions',
						'action' => 'index'
			));
		}
	}
	
	function status(){
		$this->check_Permission('Palyer_positions','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['PlayerPosition']['status'] = $this->request->data['status'];
			$this->PlayerPosition->id = $this->request->data['id'];
			if($this->PlayerPosition->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_position($id=""){
		$this->check_Permission('Palyer_positions','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->PlayerPosition->delete($id)){
				$this->Session->setFlash(__('Primary Position Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Primary Position...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Positions',
						'action' => 'index'
						));
		}
	}
	
	function update_position(){
		
		$this->check_Permission('Palyer_positions','edit');		
		// $this->redirect(array('controller'=>'users','action'=>'dashboard'));		
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_position = $this->PlayerPosition->find('first',array('conditions'=>array('PlayerPosition.name'=>$this->request->data['position'])));
			if(!empty($check_position)){
				echo "exist";
			}else{
				$data['PlayerPosition']['name'] = $this->request->data['position'];
				$this->PlayerPosition->id = $this->request->data['id'];
				if($this->PlayerPosition->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_position_multiple(){
		$this->check_Permission('Palyer_positions','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->PlayerPosition->create();
				$this->PlayerPosition->delete($id);
				$this->Session->setFlash(__('Primary Positions Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'Positions',
						'action' => 'index'
						));
	}
}