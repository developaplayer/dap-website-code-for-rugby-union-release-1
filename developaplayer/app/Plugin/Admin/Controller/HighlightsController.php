<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class HighlightsController extends AdminAppController {	
	var $name = 'Highlights';
	var $uses = array('Highlight');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$view_content = $this->Highlight->find('all');	
		//print_r($view_content);
		//die;
		$this->set(compact('view_content'));
		$this->check_Permission('Highlight_Video','view');
	}
	
	public function add_highlight() {
		$this->check_Permission('Highlight_Video','add');
	    $this->layout="default";

		if($this->request->data) {
			//pr($this->request->data);
			//die;
		if(isset($this->request->data['video_link']) && $this->request->data['video_link']!="")
		{
		     $video_url= $this->request->data['video_link'];
		
		if($video_url)
		{
			$this->request->data['Highlight']['type']="Youtube";
		}				
			$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
			$youtube_data = json_decode(file_get_contents($url), true);							
			$this->request->data['Highlight']['video_link'] = $video_url;
			$this->request->data['Highlight']['video_iframe'] = $youtube_data['html'];				
			$this->request->data['Highlight']['video_title'] = $youtube_data['title'];		
			$this->request->data['Highlight']['video_image'] = $youtube_data['thumbnail_url'];
	
		}
		
	
		if(isset($_FILES['own_video']['name']) && $_FILES['own_video']['name']!="")
		{
			$foo = new Upload($_FILES['own_video']); 
						if ($foo->uploaded) {
							   $foo->Process('uploads/video/custom_video/');
							
					
					if ($foo->processed)
						{  
					
						   $player_details['Highlight']['video_link'] = $foo->file_dst_name;
						   $player_details['Highlight']['video_iframe'] = '<video width="127" height="112"><source src="'.$this->webroot.'uploads/video/custom_video/'.$foo->file_dst_name.'"></source></video>';
							   $player_details['Highlight']['video_title'] = $this->request->data['video_title'];
							   $player_details['Highlight']['video_image'] = $foo->file_dst_name;
							   $player_details['Highlight']['status'] = "I";
							   $player_details['Highlight']['type'] = "Custom";
							   $this->Highlight->save($player_details);
							   if(isset($this->request->data['video_link']) && $this->request->data['video_link']!=""){
							   $this->Highlight->create();
							   }
						}
						
					 }
				   }
		
					$this->Highlight->save($this->request->data);
					$this->redirect(array("action" => "index"));
			}				   
	}
	
	public function edit($id)
	{
		$this->check_Permission('Highlight_Video','edit');
		$id=base64_decode($id);
		if($this->request->is('post'))
		{	
			//print_r($this->data);
			//print_r($_FILES);
			//die;
			$edit=$this->Highlight->find('first',array('conditions'=>array('Highlight.id'=>$id)));
			if($edit['Highlight']['type']=="Youtube"){
			
	                $this->request->data['Highlight']['id']=$id;  
			$video_url= $this->request->data['video_link'];  
			$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
			$youtube_data = json_decode(file_get_contents($url), true);					
			$this->request->data['Highlight']['video_link'] = $video_url;
			$this->request->data['Highlight']['video_iframe'] = $youtube_data['html'];
			$this->request->data['Highlight']['video_title'] = $youtube_data['title'];		
			$this->request->data['Highlight']['video_image'] = $youtube_data['thumbnail_url'];		
			$this->Highlight->save($this->request->data);
			$this->Session->setFlash(__('Highlight Details Succesfully Updated'));
			$this->redirect(array("action" => "index"));
			
			}
			else{
				$edit=$this->Highlight->find('first',array('conditions'=>array('Highlight.id'=>$id)));
				 $this->request->data['Highlight']['id']=$id;
				$this->request->data['Highlight']['video_title'] = $this->request->data['video_title'];  
				$this->Highlight->save($this->request->data);				
				if(isset($_FILES['own_video']['name']) && $_FILES['own_video']['name']!=""){
					$foo = new Upload($_FILES['own_video']); 
								if ($foo->uploaded) {
									   $foo->Process('uploads/video/custom_video/');
									
							
							if ($foo->processed)
								{  
							
								   $player_details['Highlight']['video_link'] = $foo->file_dst_name;
								   $player_details['Highlight']['video_iframe'] = '<video width="127" height="112" controls=""><source src="'.$this->webroot.'uploads/video/custom_video/'.$foo->file_dst_name.'"></source></video>';
								   $player_details['Highlight']['video_title'] = $this->request->data['video_title'];
								   $player_details['Highlight']['video_image'] = $foo->file_dst_name;
								   $this->request->data['Highlight']['id']=$id;
								   //$old_img = $this->webroot.'uploads/video/custom_video/'.$foo->file_dst_name;
								   unlink(WWW_ROOT.'uploads/video/custom_video/'.$edit['Highlight']['video_image']);
									$this->Highlight->save($player_details);
									$this->Session->setFlash(__('Highlight Details Succesfully Updated'));
									$this->redirect(array("action" => "index"));
								}
								
						}
				}	
					
			}
			
			
		}
		$edit=$this->Highlight->find('first',array('conditions'=>array('Highlight.id'=>$id)));			
		$this->set(compact('edit'));
	}
		
	public function delete_page($id){
		$this->check_Permission('Highlight_Video','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);	
                $fetch_data=$this->Highlight->find('first',array('conditions'=>array('Highlight.id'=>$id)));	             

		if($this->Highlight->delete($id)){
                      	unlink(WWW_ROOT.'uploads/video/custom_video/'.$fetch_data['Highlight']['video_image']);
			$this->Session->setFlash(__('Video has been Succesfully Deleted..'));
			$this->redirect(array("action" => "index"));
		}
	}
	
	public function status($id,$status){
		$this->check_Permission('Highlight_Video','status');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);	
		
		
		if($status=="I"){
			$this->Highlight->id  = $id;
			$this->Highlight->saveField('status',"A");
		}else{
			$this->Highlight->id  = $id;
			$this->Highlight->saveField('status',"I");
		}
	$this->redirect(array("action" => "index"));
	}
	
	public function highlight_multiple_delete()
	{		
		$this->check_Permission('Highlight_Video','delete');
		$this->autoRender= false;
		$this->layout="ajax";		

		foreach($this->request->data['id'] as $id){

	        $fetch_data=$this->Highlight->find('first',array('conditions'=>array('Highlight.id'=>$id)));	             	
         	$this->Highlight->delete($id);
                unlink(WWW_ROOT.'uploads/video/custom_video/'.$fetch_data['Highlight']['video_image']);
		}
		$this->Session->setFlash("Video is Successfully Deleted..");
		$this->redirect(array("action" => "index"));
	}
}
