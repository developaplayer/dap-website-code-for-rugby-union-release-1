<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class CoachProfileController extends AdminAppController {
	var $uses=array('Admin.Country','Admin.User','Admin.CoachDetail','Admin.CoachSpeciality','Admin.Certificate','Admin.CoachDetail','Admin.CoachCertificate','Admin.Video','Admin.CoachCategory','Admin.OrderProduct','Admin.CoachLevel','Admin.LavelAssignment','Admin.AddCoachRating','Admin.AddCoachRating','Admin.UpperLowerLimit','Admin.AgeGroup','Admin.LevelCategory','Admin.Product','Admin.Order');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{		
		$this->check_Permission('Coach_Profile','view');
	}
	
	public function coach_data(){	
			
		$this->layout="ajax";
		$this->render(false);		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'User.id',
			1 =>'User.profile_img',
			3 =>'User.first_name',
			4 =>'CoachDetail.dob',
			5 =>'CoachDetail.primary_speciality_id',
			6 =>'User.status',
			7 =>'User.created'
		);
		
		$sql = "User.type='C'";
		if( !empty($requestData['search']['value']) ) {
			$sql.="AND ( User.first_name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR User.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR User.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$totalData = $this->User->find('count',array('conditions'=>array('User.type'=>'C'))); 
		$totalFiltered = $totalData;
		/*$this->CoachDetail->bindModel(
				array(
					'belongsTo'=>array(
									'CoachSpeciality'	=>	array(
										'className'	=>	'CoachSpeciality',
										'foreignKey'	=>	'primary_speciality_id',
										'dependent'	=>	true
										),
									'second_CoachSpeciality'	=>	array(
										'className'	=>	'CoachSpeciality',
										'foreignKey'	=>	'secondary_speciality_id',
										'dependent'	=>	true
										)
								)
				));*/
				
				
				
				
		/*$this->User->bindModel(
				array(
					'hasOne'=>array(
						'CoachDetail'	=>	array(
								'className'	=>	'CoachDetail',
								'foreignKey'	=>	'user_id',
								'dependent'	=>	true
								)
						)
				));*/
		//$this->CoachDetail->recursive = 3;
		$users = $this->User->find('all',array('conditions'=>$sql,'recursive'=>2,'limit'=>$requestData['length'],'offset' => $requestData['start']));			
		if( !empty($requestData['search']['value']) )
		{  
		  $totalFiltered = count($users);
		}
		
		$data = array();
		foreach( $users as $row ) 
		{ 
			$cheked="";		
			if($row['CoachDetail']['top_coach_status']==1)
			{
				$cheked='checked';
			}else
			{
				$cheked="";				
			}
			
			
			if(isset($row['AddCoachRating']['id']))
			{
				$limit_id='/'.$row['AddCoachRating']['id'];
				
			}else
			{
				$limit_id='/'.'0';
				
			}
		
			$nestedData=array();
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y' && $this->Session->read('permissions.Coach_Profile.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['User']["id"].'" type="checkbox" />';
			}
			
			$nestedData[] = '<div id="coach_logo_'.$row['User']['id'].'"><img height="120px" width="100%" src="'.$this->webroot.'uploads/profiles/coach/thumb/'.$row['CoachDetail']['profile_img'].'" /></div>';
			
			$nestedData[] = '<div id="coach_name_'.$row['User']['id'].'">'.$row['User']['first_name']." ".$row['User']['last_name'].'</div>';
			
			$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">'.$row['CoachDetail']['dob'].'</div>';
			if(!empty($row['CoachDetail']['CoachSpeciality']['name']) && !empty($row['CoachDetail']['second_CoachSpeciality']['name']))
			{
				$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">Primary: '.$row['CoachDetail']['CoachSpeciality']['name'].'<br />Secondary: '.$row['CoachDetail']['second_CoachSpeciality']['name'].'</div>';	
			}
			else
			{
				$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">Primary: No record found<br />Secondary: No record found</div>';
			}
			
			if($row['User']['status']=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}			
			$nestedData[] = '<div id="status_of_'.$row['User']["id"].'">'.$status.'</div>';			
			$nestedData[] = date('d-M-Y',strtotime($row['User']["created"]));
			$nestedData[] ='<input class="checkbox_del" id="top_coach_id_'.$row['User']['id'].'" name="top_coach_id" value="'.$row['User']["id"].'" type="checkbox" onclick="top_coaches('.$row['User']["id"].')"  '.$cheked.' />';
			
			//<a href="'.$this->webroot.'admin/coach_profile/add_rating/'.$row['User']['id'].$limit_id.'"><strong>Add/Edit Rating</strong></a>
			
			
			//================Coach Profile Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y' && $this->Session->read('permissions.Coach_Profile.status')=='N' && $this->Session->read('permissions.Coach_Profile.edit')=='N'  && $this->Session->read('permissions.Coach_Profile.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Coach Profile Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y' && $this->Session->read('permissions.Coach_Profile.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Coach Profile Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y' && $this->Session->read('permissions.Coach_Profile.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/coach_profile/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Coach Profile Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y' && $this->Session->read('permissions.Coach_Profile.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/coach_profile/delete_coach/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/coach_profile/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/coach_profile/delete_coach/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_coach(){
									
		//============================================List of coach categories======================			
			$coachcategories= $this->CoachCategory->find('all');
			$CoachLevel= $this->CoachLevel->find('list');		
           
		    $this->set('coachcategories',$coachcategories);
		    $this->set('CoachLevel',$CoachLevel);
		//===========================================================================================				
		if($this->request->data){							
			//pr($this->request->data);
			$check_email = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['email_address'])));
			if(!empty($check_email)){
				$this->Session->setFlash(__('This Email Already exists. Try Again..','default',array('class'=>'alert alert-danger alert-dismissable')));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_profile',
						'action' => 'add_coach'
						));
			}
			

			$coach_details['User']['first_name'] = $this->request->data['first_name'];
			$coach_details['User']['last_name'] = $this->request->data['last_name'];
			$coach_details['User']['email_address'] = $this->request->data['email_address'];
			$coach_details['User']['password'] = $this->Auth->password($this->request->data['password']);
			$coach_details['User']['type'] = 'C';
			$coach_details['User']['status'] = $this->request->data['status'];
			$coach_details['CoachDetail']['dob'] = $this->request->data['dob'];
			$coach_details['CoachDetail']['height'] = $this->request->data['height'];
			$coach_details['CoachDetail']['weight'] = $this->request->data['weight'];
			$coach_details['CoachDetail']['primary_speciality_id'] = $this->request->data['coach_speciality_primary'];
			$coach_details['CoachDetail']['secondary_speciality_id'] = $this->request->data['coach_speciality_secondary'];
			$coach_details['CoachDetail']['nationality'] = $this->request->data['nationality'];
			if(isset($this->request->data['profile_img'])){
				$coach_details['CoachDetail']['profile_img'] = $this->request->data['profile_pic'];
			}
			if($this->User->save($coach_details)){
				$coach_details['CoachDetail']['user_id'] = $this->User->getLastInsertId();
				if($_FILES){
					$foo = new Upload($_FILES['profile_img']); 
					
				
					  $foo->allowed = array('image/jpg/jpeg');
						if ($foo->uploaded) {
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/original/');
							   $foo->image_resize          = true;
							   $foo->image_ratio           = true;							  
							   $foo->image_x               = 357;
							   $foo->image_y               = 439;

							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/profile_pic/');
							   $foo->image_resize          = true;
							   //$foo->image_ratio           = true;
							  if($foo->image_dst_x>$foo->image_dst_y)
							   {   
						           $foo->image_x               = 357;
								    $foo->image_ratio_y              = true;
    
							   }else if($foo->image_dst_x<$foo->image_dst_y)
							   {
								   $foo->image_y                = 439;
                                   $foo->image_ratio_x          = true;
								   
							   }else if($foo->image_dst_x==$foo->image_dst_y)
							   {
								   $foo->image_ratio_y         = true;
                                   $foo->image_x               = 357;
								   
							   }
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/thumb/');
							  if ($foo->processed) {
									$coach_details['CoachDetail']['profile_img'] = $foo->file_dst_name;
							  }
						}
				}
				if($this->CoachDetail->save($coach_details)){
					foreach($this->request->data['certificate_id'] as $certificate_id){
						$coach_details['CoachCertificate']['user_id'] = $coach_details['CoachDetail']['user_id'];
						$coach_details['CoachCertificate']['certificate_id'] = $certificate_id;
						$this->CoachCertificate->create();
						$this->CoachCertificate->save($coach_details);
					}
					if(!empty($this->request->data['video_link']))
					{						
						$i=1;
						foreach($this->request->data['video_link'] as $video_link)
						{						
							//========= Get all youtube video information ========================
							if(!empty($video_link))
							{
								$video_url = $video_link;
								$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
								$youtube_data = json_decode(file_get_contents($url), true);
								//pr($youtube_data);						
								$coach_details['Video']['user_id'] = $coach_details['CoachDetail']['user_id'];
								$coach_details['Video']['video_link'] = $video_link;
								$coach_details['Video']['video_iframe'] = $youtube_data['html'];
								$coach_details['Video']['video_title'] = $youtube_data['title'];
								$coach_details['Video']['video_image'] = $youtube_data['thumbnail_url'];
								$coach_details['Video']['video_position'] = $i;
								$coach_details['Video']['video_type'] = 0;
								$this->Video->create();
								$this->Video->save($coach_details);
								$i++;
							}
						}
					}
					
					//pr($this->request->data['Coach_Level']);
					//exit;
					if(!empty($this->request->data['Coach_Level']))
					{		 						
						foreach($this->request->data['Coach_Level'] as $key=>$laves)
						{	
							if(!empty($laves))
							{								
								foreach($laves as $insert_lavel)
								{									
									if(!empty($insert_lavel))
									{
										$this->LavelAssignment->create();
										$LavelAssignment['LavelAssignment']['lavel_id'] = $insert_lavel;
										$LavelAssignment['LavelAssignment']['coach_id'] = $coach_details['CoachDetail']['user_id'];
										$LavelAssignment['LavelAssignment']['level_cat_id'] = $key;								
										$this->LavelAssignment->save($LavelAssignment); 
									}									
								} 
							}							
						}
					}	
					
					$this->Session->setFlash(__('Profile Succesfully Added..','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
						'controller' => 'coach_profile',
						'action' => 'index'
						));
				}
			}
		}
	}
	
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['User']['status'] = $this->request->data['status'];
			$this->User->id = $this->request->data['id'];
			if($this->User->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_coach($id=""){
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->User->delete($id)){
				$details = $this->CoachDetail->find('first',array('conditions'=>array('CoachDetail.user_id'=>$id)));
				unlink(WWW_ROOT.'uploads/profiles/coach/thumb/'.$details['CoachDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/coach/original/'.$details['CoachDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/coach/profile_pic/'.$details['CoachDetail']['profile_img']);
				$this->CoachDetail->deleteAll(array('CoachDetail.user_id'=>$id));
				$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$id));
				$this->Video->deleteAll(array('Video.user_id'=>$id));
				$this->Session->setFlash(__('Coach Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Coach...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_profile',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=""){	      
		if($id!=""){
            $this->CoachCategory->recursive = 2;
	        $coachcategories= $this->CoachCategory->find('all');			
			$CoachLevel= $this->CoachLevel->find('list');
			$lavel_1=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$id,'LavelAssignment.level_cat_id'=>1)));		
			$lave_1_array=array();
			if(!empty($lavel_1))	
			{	
				foreach($lavel_1 as $lavel_coach_certificate)
				{			
					$lave_1_array[]=$lavel_coach_certificate['LavelAssignment']['lavel_id'];				
				}			
			}		
			$lavel_2=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$id,'LavelAssignment.level_cat_id'=>2)));$lave_2_array=array();
			if(!empty($lavel_2))		
			{		
				foreach($lavel_2 as $lavel_coach_certificates)	
				{			
					$lave_2_array[]=$lavel_coach_certificates['LavelAssignment']['lavel_id'];								
				}		
			}
			$lavel_3=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$id,'LavelAssignment.level_cat_id'=>3)));	$lave_3_array=array();	
			if(!empty($lavel_3))		
			{			
				foreach($lavel_3 as $lavel_coach_certificate)	
				{			
					$lave_3_array[]=$lavel_coach_certificate['LavelAssignment']['lavel_id'];					
				}
			}		          
			$lavel_4=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$id,'LavelAssignment.level_cat_id'=>4)));
			$lave_4_array=array();		
			if(!empty($lavel_4))	
			{		
				foreach($lavel_4 as $lavel_coach_certificate)		
				{				
					$lave_4_array[]=$lavel_coach_certificate['LavelAssignment']['lavel_id'];					
				}			
			}			
			//echo' <pre>';		
			//print_r($lave_1_array);
			//exit;				 			 
		    $this->set('coachcategories',$coachcategories);
		    $this->set('CoachLevel',$CoachLevel);
			//$this->set('all_edit_lavel',$all_edit_lavel);				
			$this->User->recursive = 2;
			$user=$this->User->find('first',array('conditions'=>array('User.id'=>$id)));
			
		
			if(empty($user)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_profile',
							'action' => 'index'
							));
			}
			$this->set('user',$user);
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'course',
						'action' => 'index'
						));
		}
		    $this->set('lave_array_select',$lave_1_array);	
			$this->set('lave_array_select2',$lave_2_array);		
			$this->set('lave_array_select3',$lave_3_array);	
			$this->set('lave_array_select4',$lave_4_array);
			
		if($this->request->data){
			$this->CoachDetail->deleteAll(array('CoachDetail.user_id'=>$id));
			$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$id));
			
			$coach_details['User']['first_name'] = $this->request->data['first_name'];
			$coach_details['User']['last_name'] = $this->request->data['last_name'];
			$coach_details['User']['email_address'] = $this->request->data['email_address'];
			$coach_details['User']['type'] = 'C';
			$coach_details['User']['status'] = $this->request->data['status'];
			$coach_details['CoachDetail']['dob'] = $this->request->data['dob'];
			$coach_details['CoachDetail']['height'] = $this->request->data['height'];
			$coach_details['CoachDetail']['weight'] = $this->request->data['weight'];
			$coach_details['CoachDetail']['primary_speciality_id'] = $this->request->data['coach_speciality_primary'];
			$coach_details['CoachDetail']['secondary_speciality_id'] = $this->request->data['coach_speciality_secondary'];
			$coach_details['CoachDetail']['nationality'] = $this->request->data['nationality'];
			$coach_details['CoachDetail']['note'] = $this->request->data['note'];
			if(isset($this->request->data['profile_img'])){
				$coach_details['CoachDetail']['profile_img'] = $this->request->data['profile_pic'];
			}
			$this->User->id = $id;
			if($this->User->save($coach_details)){
				$coach_details['CoachDetail']['user_id'] = $id;
				if(!empty($_FILES['profile_img']['name'])){
					$foo = new Upload($_FILES['profile_img']);

	                // echo $foo->image_dst_x;
					 // echo $foo->image_dst_y;
					//exit;					
					  $foo->allowed = array('image/jpg/jpeg');
						if ($foo->uploaded) {
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/original/');
							   $foo->image_resize          = true;
							   //$foo->image_ratio           = true;
							   
							   if($foo->image_dst_x>$foo->image_dst_y)
							   {   
						           $foo->image_x               = 357;
								    $foo->image_ratio_y              = true;
    
							   }else if($foo->image_dst_x<$foo->image_dst_y)
							   {
								   $foo->image_y                = 439;
                                   $foo->image_ratio_x          = true;
								   
							   }else if($foo->image_dst_x==$foo->image_dst_y)
							   {
								   $foo->image_ratio_y         = true;
                                   $foo->image_x               = 357;
								   
							   }
							   
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/profile_pic/');
							   $foo->image_resize          = true;
					
							   $foo->image_y         = 160;
							   $foo->image_x         = 130;
								
							   
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/coach/thumb/');
							  if ($foo->processed) {
								  unlink(WWW_ROOT.'uploads/profiles/coach/thumb/'.$user['CoachDetail']['profile_img']);
								  unlink(WWW_ROOT.'uploads/profiles/coach/original/'.$user['CoachDetail']['profile_img']);
								  unlink(WWW_ROOT.'uploads/profiles/coach/profile_pic/'.$user['CoachDetail']['profile_img']);
								  $coach_details['CoachDetail']['profile_img'] = $foo->file_dst_name;
							  }
						}
				}else{
					$coach_details['CoachDetail']['profile_img'] = $user['CoachDetail']['profile_img'];
				}
				if($this->CoachDetail->save($coach_details)){
					
					if(!empty($this->request->data['Coach_Level']))
							{
                               $this->LavelAssignment->deleteAll(array('LavelAssignment.coach_id'=>$id));
						
								foreach($this->request->data['Coach_Level'] as $key=>$laves)
					        	{	
									if(!empty($laves))
									{								
									   foreach($laves as $insert_lavel)
									   {																		 
											if(!empty($insert_lavel))
											{
												$this->LavelAssignment->create();
												$LavelAssignment['LavelAssignment']['lavel_id'] = $insert_lavel;
												$LavelAssignment['LavelAssignment']['coach_id'] = $coach_details['CoachDetail']['user_id'];
												$LavelAssignment['LavelAssignment']['level_cat_id'] = $key;					
												$this->LavelAssignment->save($LavelAssignment);
											}											
										} 
									}
									
								}
							}	
					
					foreach($this->request->data['certificate_id'] as $certificate_id){
						$coach_details['CoachCertificate']['user_id'] = $coach_details['CoachDetail']['user_id'];
						$coach_details['CoachCertificate']['certificate_id'] = $certificate_id;
						$this->CoachCertificate->create();
						$this->CoachCertificate->save($coach_details);
					}
					if(!empty($this->request->data['video_link']))
					{
						$this->Video->deleteAll(array('Video.user_id'=>$id,'Video.video_type'=>0));
						$i=1;
						foreach($this->request->data['video_link'] as $video_link)
						{						
							//========= Get all youtube video information ========================
							if(!empty($video_link))
							{
								$video_url = $video_link;
								$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
								$youtube_data = json_decode(file_get_contents($url), true);
								//pr($youtube_data);						
								$coach_details['Video']['user_id'] = $coach_details['CoachDetail']['user_id'];
								$coach_details['Video']['video_link'] = $video_link;
								$coach_details['Video']['video_iframe'] = $youtube_data['html'];
								$coach_details['Video']['video_title'] = $youtube_data['title'];
								$coach_details['Video']['video_image'] = $youtube_data['thumbnail_url'];
								$coach_details['Video']['video_position'] = $i;
								$coach_details['Video']['video_type'] = 0;
								$this->Video->create();
								$this->Video->save($coach_details);
								$i++;
							}
						}
					}
					
					$this->Session->setFlash(__('Profile Succesfully Updated..','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
						'controller' => 'coach_profile',
						'action' => 'index'
						));
				}
			}
		}
	}
	
	function delete_coach_multiple(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$details = $this->CoachDetail->find('first',array('conditions'=>array('CoachDetail.user_id'=>$id)));
				unlink(WWW_ROOT.'uploads/profiles/coach/thumb/'.$details['CoachDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/coach/original/'.$details['CoachDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/coach/profile_pic/'.$details['CoachDetail']['profile_img']);
				$this->CoachDetail->deleteAll(array('CoachDetail.user_id'=>$id));
				$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$id));
				$this->Video->deleteAll(array('Video.user_id'=>$id));
				$this->User->delete($id);
				$this->Session->setFlash(__('Coaches Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_profile',
						'action' => 'index'
						));
	}
	
	function get_nationality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$countries=$this->Country->find('all');
			$data.='<option  value="">'.'-Select a Nationality-'.'</option>';
			foreach($countries as $country){
				if($id!="no"){
					if($country['Country']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				
				$data.='<option '.$status.' value="'.$country['Country']['id'].'" id="'.$country['Country']['id'].'">'.$country['Country']['country_name'].'</option>';
			}
			echo $data;
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
			}*/
	}
	
	function get_speciality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$coach_specialities=$this->CoachSpeciality->find('all');
			$data="";
			foreach($coach_specialities as $coach_speciality){
				if($id!="no"){
					if($coach_speciality['CoachSpeciality']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$coach_speciality['CoachSpeciality']['id'].'" id="'.$coach_speciality['CoachSpeciality']['id'].'">'.$coach_speciality['CoachSpeciality']['name'].'</option>';
			}
			echo $data;
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}*/
	}
	
	function get_certificate(){
		$this->layout="ajax";
		$this->render(false);		
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$certificates=$this->Certificate->find('all',array('conditions'=>array('Certificate.status'=>'A')));
			//pr($certificates);
			$data="";
			foreach($certificates as $certificate){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id){
						if($certificate['Certificate']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$certificate['Certificate']['id'].'" id="'.$certificate['Certificate']['id'].'">'.$certificate['Certificate']['name'].'</option>';
			}
			
			echo '<label for="coach_certificate">Certificate</label><select multiple id="coach_certificate" style="width:100%" data-placeholder="Select Certificate" class="required form-control valid select2" name="certificate_id[]">'.$data.'</select>';
		}
		else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_profile',
							'action' => 'index'
							));
		}
	}
	
	function load_video(){
		$this->layout="ajax";
		if(!empty($this->request->data['link']))
		{
			if($this->request->data)
			{
				//============= Youtube Information ================
				$video_url = $this->request->data['link'];
				$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
				
				if($url===false) 
				{					
					$video_iframe = 'no iframe';
					$this->set('video',$video_iframe);
				}else{					
					$content_value=file_get_contents($url);
					$youtube_data = json_decode($content_value, true);
					$video_iframe = $youtube_data['html'];
					$this->set('video',$video_iframe);
				}
			}
		}
	}
	
	//======================= Get All Caoch  ==================================================
	function get_allcoach(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$allcoachArray=$this->User->find('all',array('conditions'=>array('User.type'=>'C','User.status'=>'A',)));
			$data="";
			foreach($allcoachArray as $allcoach){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id){
						if($allcoach['User']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$allcoach['User']['id'].'" id="'.$allcoach['User']['id'].'">'.$allcoach['User']['first_name'].' '.$allcoach['User']['last_name'].'</option>';
			}
			
			echo '<label for="coach_course">Coach</label><select multiple id="coach_course" style="width:100%" data-placeholder="Select Coach" class="required form-control valid select2" name="coach_id[]">'.$data.'</select>';
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}*/
	}
	//========================== Add Edit Coach Rating   ====================================================
	public function  add_rating($id="")
	{
		
		$rating_id=$this->request->params['pass'][1];		 
		$all_rating_list=$this->AddCoachRating->find('all',array('conditions'=>array('AddCoachRating.coach_id'=>$id))); 

		if(count($all_rating_list)>0)
		{			 
			//$all_rating_list=$this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.coach_id'=>$id))); 
			$this->set('all_rating_list', $all_rating_list);			
		}else
		{     $all_rating_list=array();
		  $this->set('all_rating_list', $all_rating_list);
		}
		$all_coach_category_list=$this->LevelCategory->find('list');		
		$AgeGroup_list=$this->AgeGroup->find('list');		
		$UpperLowerLimit_list=$this->UpperLowerLimit->find('all');	
		foreach($UpperLowerLimit_list as $UpperLowerLimit_lists)
		{
			$UpperLowerLimit_list_val[$UpperLowerLimit_lists['UpperLowerLimit']['id']]=$UpperLowerLimit_lists['UpperLowerLimit']['lower_limit'].'-'.$UpperLowerLimit_lists['UpperLowerLimit']['upper_limit'];			 
		}
		
		$this->set('all_coach_category_list', $all_coach_category_list);
		$this->set('AgeGroup_list',$AgeGroup_list);
		$this->set('UpperLowerLimit_list',$UpperLowerLimit_list_val);		
		//=============check id exits rating id===============================================		
		
		
		$add_rating_arr=array();
		if($this->request->data)
		{
			if(count($all_rating_list)>0)
			{
				$this->AddCoachRating->deleteAll(array('AddCoachRating.coach_id'=>$id));
			}				
			$k=1;
			for($z=1;$z<=count($AgeGroup_list);$z++)
			{
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{	$this->AddCoachRating->create();
					$add_rating_arr['AddCoachRating']['coach_id']=$id;				
					//$add_rating_arr['AddCoachRating']['coach_cat_id']=$this->request->data['coach_category_'.$k];
					$add_rating_arr['AddCoachRating']['coach_cat_id']=$this->request->data['coach_category_hidden_'.$k];
					$add_rating_arr['AddCoachRating']['rating_limit_id']=$this->request->data['upper_lower_limit_'.$z];
					$add_rating_arr['AddCoachRating']['rating']=$this->request->data['rating_val_'.$k];
					$add_rating_arr['AddCoachRating']['age_group_id']=$this->request->data['age_group_'.$z];
					$this->AddCoachRating->save($add_rating_arr);					
					$k++;
				}
			}

			if(count($all_rating_list)>0)
			{
					$this->Session->setFlash(__('Successfully Update Rating...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'coach_profile',
								'action' => '/index'
								));
			}else{								
			$this->Session->setFlash(__('Coaches Successfully Add Rating...','default',array('class'=>'alert alert-success alert-dismissable')));
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_profile',
						'action' => '/index'
						));
			}						
		}
	}
	
	public function select_top_coach()
	{
		$this->layout="ajax";
		$this->render(false);
		$coach_id= $this->request->data['id'];
		$status= $this->request->data['status'];
		$this->CoachDetail->updateAll(array('CoachDetail.top_coach_status'=>$status),array('CoachDetail.user_id'=>$coach_id));
	}
	
	
	//=============================All Coach Xls file download========================================//
	public function download_coach_xls()
	{
		$flag = false;
		$data=array();
		$this->loadModel('Order');
		$this->loadModel('OrderProduct');
	    $this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video','PlayerVideo')));
	    $this->User->unBindModel(array('hasOne' => array('PlayerDetail','FamilyDetail')));
	    // $this->User->CoachDetail(array('belongsTo' => array('PlayerDetail','FamilyDetail')));
	    
        $this->CoachDetail->bindModel(array(
			   'belongsTo' => array(
								'User'=>array(
									 'className'=>'User',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=> 'user_id',
									 'fields'=>''
								)
				               )
							)
		               );
	
		
	    //===========================Get Total Coach=============================//
		$total_coach = $this->User->find('all',array('conditions'=>array('User.type'=>'C','status'=>'A')));
	  
		foreach ($total_coach as $key=>$value)
		{
				$this->Order->bindModel(array(
				'hasMany' => array(
						'OrderProduct'=>array(
						'className' =>'OrderProduct',
						'order'          => '',
						'foreignKey '=> 'order_id',
						'dependent' =>  false,
						'fields'=>''
						)
					)   
				)
			);	
			
			$user_id = $value['User']['id']; //get user id            
			$total_order = $this->Order->find('all',array('conditions'=>array('Order.customer_id'=>$user_id)));
			//$total_order = $this->Order->find('all',array('conditions'=>''));
			
			//exit();
			
            $total_order_count=count($total_order);
            $pro_array="";
            $p=1;
			if(!empty($total_order))
			{
				foreach($total_order as $purchase)
				{
					
					

					if(!empty($purchase['OrderProduct'] ))
					{						
						
						foreach($purchase['OrderProduct'] as $p)
						{
							//pr($p);
							//exit();
							$purchase_order=$this->Product->find('first',array('conditions'=>array('Product.id'=>$p['product_id'])));
							if($p<$total_order_count)
							{
								$pro_array .= $purchase_order['Product']['name'].",";	
							}
							else
							{
								$pro_array .=$purchase_order['Product']['name'];
							}
							$p++;
						}
					}

				}
			}
			
            
			$id = $value['CoachDetail']['nationality']; //get nationality id
		    $pri_id = $value['CoachDetail']['primary_speciality_id']; //get primary_speciality_id
			$second_id = $value['CoachDetail']['secondary_speciality_id']; //get primary_speciality_id
			
			$country = $this->Country->find('first',array('conditions'=>array('Country.id'=>$id))); // fetch nationality 
			$pri_data = $this->CoachSpeciality->find('first',array('conditions'=>array('CoachSpeciality.id'=>$pri_id))); // fetch primary speciality 
			$second_data = $this->CoachSpeciality->find('first',array('conditions'=>array('CoachSpeciality.id'=>$second_id))); // fetch secondary speciality
			
		    $lavel_1=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$user_id,'LavelAssignment.level_cat_id'=>1)));	
		    $lave_1_array="";
		    
		  
			if(!empty($lavel_1))	
			{
				  $total1 = count($lavel_1);	
				  $i=1;
				//================Fetch total Category of level 1=================//
				
				foreach($lavel_1 as $lavel_coach_certificate)
				{		
					
					$coach_level=$this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.id'=>$lavel_coach_certificate['LavelAssignment']['lavel_id'])));	
				    
				    if($i<$total1){
						 $lave_1_array .=$coach_level['CoachLevel']['name'].",";		
						}
					else{
						$lave_1_array .=$coach_level['CoachLevel']['name'];
						}
					 
				    $i++;		
				}	
						
			}		
			
			$lavel_2=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$user_id,'LavelAssignment.level_cat_id'=>2)));	
			$lave_2_array="";
			
			if(!empty($lavel_2))		
			{	
				$total2 = count($lavel_2);	
				$i2=1;
				//=================Fetch total Category of level 2==================//	
				
				foreach($lavel_2 as $lavel_coach_certificates)	
				{			
					$coach_level=$this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.id'=>$lavel_coach_certificates['LavelAssignment']['lavel_id'])));	
					
					if($i2<$total2){
						 $lave_2_array .=$coach_level['CoachLevel']['name'].",";		
						}
					else{
						$lave_2_array .=$coach_level['CoachLevel']['name'];
						}
					 
				    $i2++;	
									
				}	
				
			}
			
			$lavel_3=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$user_id,'LavelAssignment.level_cat_id'=>3)));
			$lave_3_array="";
		
			
			if(!empty($lavel_3))		
			{	
				$total3 = count($lavel_3);	
				$i3=1;	
				//===================Fetch total Category of level 3=================//	
				
				foreach($lavel_3 as $lavel_coach_certificate)	
				{	
							
					$coach_level=$this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.id'=>$lavel_coach_certificate['LavelAssignment']['lavel_id'])));	
					
					if($i3<$total3){
						 $lave_3_array .=$coach_level['CoachLevel']['name'].",";		
						}
					else{
						$lave_3_array .=$coach_level['CoachLevel']['name'];
						}
					 
				    $i3++;	
					
							
				}
				
			}		
			          
			$lavel_4=$this->LavelAssignment->find('all',array('conditions'=>array('LavelAssignment.coach_id'=>$user_id,'LavelAssignment.level_cat_id'=>4)));
			$lave_4_array="";
			
	
			if(!empty($lavel_4))	
			{		
				$total4 = count($lavel_4);	
				$i4=1;
				//===================Fetch total Category of level 4=================//	
				
				foreach($lavel_4 as $lavel_coach_certificate)		
				{				
					$coach_level=$this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.id'=>$lavel_coach_certificate['LavelAssignment']['lavel_id'])));	
					
						if($i4<$total4){
						 $lave_4_array .=$coach_level['CoachLevel']['name'].",";				
						}
					else{
						$lave_4_array .=$coach_level['CoachLevel']['name'];
						}
					 
				    $i4++;
							
				}
								
			}
			
			$data[$key]['Coach First Name']=$value['User']['first_name'];		
			$data[$key]['Coach Last Name']=$value['User']['last_name'];	
			$data[$key]['Date of Birth']=$value['CoachDetail']['dob'];	
		    $data[$key]['E-mail']=$value['User']['email_address'];
		    $data[$key]['Nationality']=$country['Country']['country_name'];
		    $data[$key]['Primary Speciality']=$pri_data['CoachSpeciality']['name'];
		    $data[$key]['Secondary Speciality']=$second_data['CoachSpeciality']['name'];
		    $data[$key]['Attack Certificate']=$lave_1_array;
		    $data[$key]['Defence Certificate']=$lave_2_array;
		    $data[$key]['Mental Conditioning Certificate']=$lave_3_array;	
		    $data[$key]['Physical Conditioning Certificate']=$lave_4_array;			
		    $data[$key]['Total Purchase Made']=$total_order_count;
		    $data[$key]['Purchase Made']=$pro_array;
		    
		}		

		// file name for download
		$filename = "coach" . date('Ymd') . ".xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel");
		
	
		foreach($data as $row) 
		{
			if(!$flag) {
				echo implode("\t", array_keys($row)) . "\n";
				$flag = true;
			}
		
		 array_walk($row,function(&$row)
	     {
		$row = preg_replace("/\t/", "\\t", $row);
		$row= preg_replace("/\r?\n/", "\\n", $row);
		if(strstr($row, '"')) $row = '"' . str_replace('"', '""', $row) . '"';
		
	     });
			
			echo implode("\t", array_values($row)) . "\n";
		
		}
		exit;  
	}
}
