<?php
App::uses('AdminAppController', 'Admin.Controller');
class LevelsController extends AdminAppController {
	var $uses=array('Admin.Level','Admin.LevelCategory');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('level','view');
		
	}
	
	public function level_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Level.id',
			1 =>'Level.level_number',
			2 =>'Level.name',			
			3 =>'Level.status',
			4 =>'Level.created',
		);
		$totalData = $this->Level->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		$category_sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Level.name LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Level.level_number LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Level.status LIKE '".substr($requestData['search']['value'],0,1)."%' ";
			$sql.=" OR Level.created LIKE '".$requestData['search']['value']."%' ";
			//$sql.=" OR LevelCategory.name LIKE '".$requestData['search']['value']."%' ";
			$sql.= ")";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$this->Level->bindModel(
				array(
						'belongsTo'=>array(
							'LevelCategory'	=>	array(
									'className'	=>	'LevelCategory',
									'foreignKey'	=>	'category_id',
									'dependent'	=>	true
									)
							)
				));
				
		if( !empty($requestData['search']['value']) ) {   		
			$levels = $this->Level->find('all',array(
												'fields'=>array('DISTINCT Level.id','Level.name','Level.level_number','Level.status','Level.created','LevelCategory.name'),
												'conditions'=>$sql,
												 'joins'=>array(
															array(
																'type'=>'left',
																'table'=>'level_categories',
																//'alias'=>'LevelCategory',
																'conditions'=>array(
																	'LevelCategory.id = Level.category_id'
																),
																'group' => 'LevelCategory.id'	
															)
														),
															
												'order'=>$order_by,
												'offset' => $requestData['start'],
												'limit'=>$requestData['length'],
												'recursive'=>3
												));
		}else{
			$levels = $this->Level->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		}
												
		/*$log = $this->Level->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($levels);
		die;*/
		//print_r($levels);
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($levels);
		}
		$data = array();
		foreach( $levels as $row ) {  // preparing an array
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' && $this->Session->read('permissions.level.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Level']["id"].'" type="checkbox" />';
			}
			$nestedData[] = $row['Level']["level_number"];
			$nestedData[] = substr($row['Level']["name"], 0, 20)."...";
			//$nestedData[] = $row['LevelCategory']["name"];
			
			if($row['Level']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Level']["id"].'">'.$status.'</div>';
			
			$nestedData[] = date('d-M-Y',strtotime($row['Level']["created"]));
			
			//================Player Level Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' && $this->Session->read('permissions.level.status')=='N' && $this->Session->read('permissions.level.edit')=='N'  && $this->Session->read('permissions.level.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Level Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' && $this->Session->read('permissions.level.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Level']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Level']['id'].'"><img id="status_img_'.$row['Level']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Level Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' && $this->Session->read('permissions.level.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/levels/edit/'.$row['Level']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Level Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' && $this->Session->read('permissions.level.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Level']['name'].'?\')" href="'.$this->webroot.'admin/levels/delete_level/'.$row['Level']['id'].'" title="Delete" id="delete_'.$row['Level']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Level']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Level']['id'].'"><img id="status_img_'.$row['Level']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/levels/edit/'.$row['Level']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Level']['name'].'?\')" href="'.$this->webroot.'admin/levels/delete_level/'.$row['Level']['id'].'" title="Delete" id="delete_'.$row['Level']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_level(){
		$this->check_Permission('level','add');
		if($this->request->data){
			$level['Level']=$this->request->data;
			$level['Level']['level_number'] = number_format($level['Level']['level_number'],2);
			$check_level = $this->Level->find('first',array('conditions'=>array('Level.name'=>$level['Level']['name'])));
			if(!empty($check_level)){
				$this->Session->setFlash(__('Level Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				if($this->Level->save($level)){
					$this->Session->setFlash(__('Level Succesfully Added...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'levels',
							'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Adding Level...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function status(){
		$this->check_Permission('level','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Level']['status'] = $this->request->data['status'];
			$this->Level->id = $this->request->data['id'];
			if($this->Level->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_level($id=""){
		$this->check_Permission('level','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->Level->delete($id)){
				$this->Session->setFlash(__('Level Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Level...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'levels',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=null){
		$this->check_Permission('level','edit');
		if($id!=null){
			$level=$this->Level->find('first',array('conditions'=>array('Level.id'=>$id)));
			if(empty($level)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'levels',
							'action' => 'index'
							));
			}
			$this->set('level',$level);
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'levels',
						'action' => 'index'
						));
		}
		
		if($this->request->data){
			$level_edited['Level']=$this->request->data;
			$level_edited['Level']['level_number'] = number_format($level_edited['Level']['level_number'],2);
			$check_level = $this->Level->find('first',array('conditions'=>array('Level.category_id'=>$level['Level']['category_id'],'Level.name'=>$level['Level']['name'])));
			if($level_edited['Level']['level_number']==$level['Level']['level_number']){
				$check_level="";
			}
			if(!empty($check_level)){
				$this->Session->setFlash(__('Level Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				$this->Level->id = $id;
				if($this->Level->save($level_edited)){
					$this->Session->setFlash(__('Level Succesfully Updated...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'levels',
							'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Updating Level...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function delete_level_multiple(){
		$this->check_Permission('level','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Level->create();
				$this->Level->delete($id);
				$this->Session->setFlash(__('Levels Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'levels',
						'action' => 'index'
						));
	}
	
	function get_category(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$categories=$this->LevelCategory->find('all');
			$data="";
			foreach($categories as $category){
				if($id!="no"){
					if($category['LevelCategory']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$category['LevelCategory']['id'].'" id="'.$category['LevelCategory']['id'].'">'.$category['LevelCategory']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'levels',
							'action' => 'index'
							));
		}
	}
}