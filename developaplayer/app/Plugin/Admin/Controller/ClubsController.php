<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class ClubsController extends AdminAppController {
	var $uses=array('Admin.Club');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Club','view');
	}
	
	public function add_club(){
		$this->check_Permission('Club','add');
		if($this->request->is('post')){
			$data_image = $_FILES['logo'];
				  $foo = new Upload($data_image); 
				  $foo->allowed = array('image/*');
					if ($foo->uploaded) {
						   $foo->image_resize          = true;
						   $foo->image_y               = 510;
						   $foo->image_x               = 1181;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/club/logo/original/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 60;
						   $foo->image_x               = 140;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/club/logo/thumbnail/');
						  if ($foo->processed) {
								$image_name= $foo->file_dst_name;
								$this->Club->create();
								$data['logo']=$image_name;
								$data['name']=$this->request->data['name'];
								$data['status']=$this->request->data['status'];
								if($this->Club->save($data))
									{
										$this->Session->setFlash(_('Club Successfully Added..'));
										$this->redirect(array(
												'plugin' => 'admin',
												'controller' => 'clubs',
												'action' => 'index'
											));
									}
							  }else{
								  $this->Session->setFlash(_('Invalid Image File!!'));
								  $this->redirect(
										array("action" => "add_club"));
							  }
					}
		}
	}
	
	public function club_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Club.id',
			1 =>'Club.logo',
			2 =>'Club.name',
			3 =>'Club.status',
			4 =>'Club.created',
		);
		$totalData = $this->Club->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Club.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR Club.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Club.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$clubs = $this->Club->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($clubs);
		}
		$data = array();
		foreach( $clubs as $row ) {  // preparing an array
			$nestedData=array();
			if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Club']["id"].'" type="checkbox" />';	
			}
			$nestedData[] = '<div id="club_logo_'.$row['Club']["id"].'"><img src="'.$this->webroot.'uploads/club/logo/thumbnail/'.$row['Club']["logo"].'" /></div>';
			
			$nestedData[] = '<div id="club_name_'.$row['Club']["id"].'">'.$row['Club']["name"].'</div>';
			
			if($row['Club']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Club']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['Club']["created"]));
			
			//================Player Club Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.status')=='N' && $this->Session->read('permissions.Club.edit')=='N'  && $this->Session->read('permissions.Club.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Player Club Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Club']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Club']['id'].'"><img id="status_img_'.$row['Club']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Player Club Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/clubs/edit/'.$row['Club']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Player Club Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Club']['name'].'?\')" href="'.$this->webroot.'admin/clubs/delete_club/'.$row['Club']['id'].'" title="Delete" id="delete_'.$row['Club']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Club']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Club']['id'].'"><img id="status_img_'.$row['Club']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/clubs/edit/'.$row['Club']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Club']['name'].'?\')" href="'.$this->webroot.'admin/clubs/delete_club/'.$row['Club']['id'].'" title="Delete" id="delete_'.$row['Club']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
			
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Club']['status'] = $this->request->data['status'];
			$this->Club->id = $this->request->data['id'];
			if($this->Club->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_club($id=""){
		$this->check_Permission('Club','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			$club_data = $this->Club->find('first',array('fields'=>'Club.logo','conditions'=>array('Club.id'=>$id)));
			@unlink('uploads/club/logo/original/'.$club_data['Club']['logo']);
			@unlink('uploads/club/logo/thumbnail/'.$club_data['Club']['logo']);
			if($this->Club->delete($id)){
				$this->Session->setFlash(__('Club Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Club...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'clubs',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=""){
		$this->check_Permission('Club','edit');
		if($id==""){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'clubs',
						'action' => 'index'
						));
		}
		$club_data = $this->Club->find('first',array('conditions'=>array('Club.id'=>$id)));
		if(empty($club_data)){
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'clubs',
						'action' => 'index'
						));
		}
		if($this->request->is('post')){
			$check_club = $this->Club->find('first',array('conditions'=>array('Club.name'=>$this->request->data['name'])));
			if(!empty($check_club) && $club_data['Club']['name']!=$check_club['Club']['name']){
				$this->Session->setFlash(__('Error in Updating Club, Name Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				$data_image = $_FILES['logo'];
				  $foo = new Upload($data_image); 
				  $foo->allowed = array('image/jpg/png/gif/jpeg');
					if ($foo->uploaded) {
						   $foo->image_resize          = true;
						   $foo->image_y               = 510;
						   $foo->image_x               = 1181;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/club/logo/original/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 60;
						   $foo->image_x               = 140;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/club/logo/thumbnail/');
						  if ($foo->processed) {
								$image_name= $foo->file_dst_name;
								$this->Club->create();
								$data['logo']=$image_name;
								$data['name']=$this->request->data['name'];
								$data['status']=$this->request->data['status'];
								$this->Club->id = $club_data['Club']['id'];
								if($this->Club->save($data))
									{
										 if(!empty($_FILES['logo']['name'])){
											@unlink('uploads/club/logo/original/'.$club_data['Club']['logo']);
											@unlink('uploads/club/logo/thumbnail/'.$club_data['Club']['logo']);
										}
										$this->Session->setFlash(__('Club Succesfully Updated...','default',array('class'=>'alert alert-success alert-dismissable')));
										$this->redirect(array(
											'plugin' => 'admin',
											'controller' => 'clubs',
											'action' => 'index'
											));
									}
							  }else{
								 $this->Session->setFlash(__('Error in Updating Club...','default',array('class'=>'alert alert-danger alert-dismissable')));
							  }
					}
			}
		}
		$this->set('club_data',$club_data);
	}
	
	function delete_club_multiple(){
		$this->check_Permission('Club','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Club->create();
				$club_data = $this->Club->find('first',array('fields'=>'Club.logo','conditions'=>array('Club.id'=>$id)));
				@unlink('uploads/club/logo/original/'.$club_data['Club']['logo']);
				@unlink('uploads/club/logo/thumbnail/'.$club_data['Club']['logo']);
				$this->Club->delete($id);
				$this->Session->setFlash(__('Banners Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'clubs',
						'action' => 'index'
						));
	}
}