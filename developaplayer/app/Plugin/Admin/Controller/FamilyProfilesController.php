<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class FamilyProfilesController extends AdminAppController {
	var $uses=array('Admin.Country','Admin.User','Admin.FamilyDetail','Admin.OrderProduct','Admin.Order','Admin.CoachSpeciality','Admin.Certificate','Admin.FamilyDetail','Admin.CoachCertificate','Admin.Video','Admin.PlayerRelationship','Admin.FamilyPlayerRelationship','Admin.Product');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{		
		$this->check_Permission('Family_Profile','view');
	}
	
	public function family_data(){	
			
		$this->layout="ajax";
		$this->render(false);		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'User.id',
			1 =>'User.profile_img',
			2 =>'User.first_name',
			3 =>'FamilyDetail.dob',		
			4 =>'User.status',
			5 =>'User.created'
		);
		
		$sql = "User.type='F'";
		if( !empty($requestData['search']['value']) ) {
			$sql.="AND ( User.first_name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR User.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR User.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$totalData = $this->User->find('count',array('conditions'=>array('User.type'=>'F'))); 
		$totalFiltered = $totalData;
		
		$users = $this->User->find('all',array('conditions'=>$sql,'recursive'=>1,'limit'=>$requestData['length'],'offset' => $requestData['start']));
		
	
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($users);
		}
		//pr($users);
		//exit;
		$data = array();
		foreach( $users as $row ) {  // preparing an array
		
			//pr($row);
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y' && $this->Session->read('permissions.Family_Profile.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['User']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="coach_logo_'.$row['User']['id'].'"><img height="120px" width="100%" src="'.$this->webroot.'uploads/profiles/family/thumb/'.$row['FamilyDetail']['profile_img'].'" /></div>';
			
			$nestedData[] = '<div id="coach_name_'.$row['User']['id'].'">'.$row['User']['first_name'].$row['User']['last_name'].'</div>';
			
			$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">'.$row['FamilyDetail']['dob'].'</div>';
			
			
			
			if($row['User']['status']=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['User']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['User']["created"]));
			
			//================Family Profile Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y' && $this->Session->read('permissions.Family_Profile.status')=='N' && $this->Session->read('permissions.Family_Profile.edit')=='N'  && $this->Session->read('permissions.Family_Profile.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Family Profile Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y' && $this->Session->read('permissions.Family_Profile.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Family Profile Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y' && $this->Session->read('permissions.Family_Profile.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/family_profiles/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Family Profile Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y' && $this->Session->read('permissions.Family_Profile.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/family_profiles/delete_family/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/family_profiles/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/family_profiles/delete_family/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_family(){	
		$this->check_Permission('Family_Profile','add');
		$this->User->virtualFields = array('full_name' => "CONCAT(User.first_name, ' ', User.last_name)");
		$player_list = $this->User->find('list',array('conditions'=>array('User.type'=>'P'),'fields'=>array('User.id','User.full_name')));	
		$relation_list = $this->PlayerRelationship->find('list',array('fields'=>array('PlayerRelationship.id','PlayerRelationship.relationship')));
	    $this->set('player_list',$player_list);
		$this->set('relation_list',$relation_list);
		if($this->request->data){
			//pr($this->request->data);
			$check_email = $this->User->find('first',array('conditions'=>array('User.email_address'=>$this->request->data['email_address'])));
			if(!empty($check_email)){
				$this->Session->setFlash(__('This Email Already exists. Try Again..','default',array('class'=>'alert alert-danger alert-dismissable')));
				$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'family_profiles',
						'action' => 'add_family'
						));
					exit;
			}
			$FamilyDetail['User']['first_name'] = $this->request->data['first_name'];
			$FamilyDetail['User']['last_name'] = $this->request->data['last_name'];
			$FamilyDetail['User']['email_address'] = $this->request->data['email_address'];
			$FamilyDetail['User']['password'] = $this->Auth->password($this->request->data['password']);
			$FamilyDetail['User']['type'] = 'F';
			$FamilyDetail['User']['status'] = $this->request->data['status'];
			$FamilyDetail['FamilyDetail']['dob'] = $this->request->data['dob'];			
			$FamilyDetail['FamilyDetail']['player_id'] = 0; //$this->request->data['player_id'];
			$FamilyDetail['FamilyDetail']['relation_id'] = 0;//$this->request->data['relation_id'];
			$FamilyDetail['FamilyDetail']['nationality'] = $this->request->data['nationality'];
			if(isset($this->request->data['profile_img'])){
				$coach_details['FamilyDetail']['profile_img'] = $this->request->data['profile_pic'];
			}
			if($this->User->save($FamilyDetail)){
				$FamilyDetail['FamilyDetail']['user_id'] = $this->User->getLastInsertId();
				if($_FILES){
					$foo = new Upload($_FILES['profile_img']);				
					  $foo->allowed = array('image/jpg/jpeg');
						if ($foo->uploaded) {
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/original/');
							   $foo->image_resize          = true;
							   $foo->image_ratio           = true;							  
							   $foo->image_x               = 357;
							   $foo->image_y               = 439;

							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/profile_pic/');
							   $foo->image_resize          = true;
							   //$foo->image_ratio           = true;
							  if($foo->image_dst_x>$foo->image_dst_y)
							   {   
						           $foo->image_x               = 357;
								    $foo->image_ratio_y              = true;
    
							   }else if($foo->image_dst_x<$foo->image_dst_y)
							   {
								   $foo->image_y                = 439;
                                   $foo->image_ratio_x          = true;
								   
							   }else if($foo->image_dst_x==$foo->image_dst_y)
							   {
								   $foo->image_ratio_y         = true;
                                   $foo->image_x               = 357;
								   
							   }
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/thumb/');
							  if ($foo->processed) {
									$FamilyDetail['FamilyDetail']['profile_img'] = $foo->file_dst_name;
							  }
						}
				}
				if($this->FamilyDetail->save($FamilyDetail))
				{
					$last_id= $this->FamilyDetail->id;
					$payer_array=$this->request->data['player_id'];
					//echo $FamilyDetail['FamilyDetail']['user_id'];
					//pr($payer_array);
					//exit;
						
						$family_relation_data=array();
						if(!empty($payer_array))
						{   $i=0;
							foreach($payer_array as $payer_arrays)
							{
								$family_relation_data['FamilyPlayerRelationship']['member_id']=$FamilyDetail['FamilyDetail']['user_id'];
								$family_relation_data['FamilyPlayerRelationship']['player_id']=$payer_arrays;
								$family_relation_data['FamilyPlayerRelationship']['relation_id']=$this->request->data['relation_id'][$i];
								$this->FamilyPlayerRelationship->create();
								$this->FamilyPlayerRelationship->save($family_relation_data);								
								$i++;
							}
						}
					
					$this->Session->setFlash(__('Profile Succesfully Added..','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
						'controller' => 'family_profiles',
						'action' => 'index'
						));
				}
			}
		}
	}
	
	function status(){
		$this->check_Permission('Family_Profile','status');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['User']['status'] = $this->request->data['status'];
			$this->User->id = $this->request->data['id'];
			if($this->User->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_family($id=""){
		$this->check_Permission('Family_Profile','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->User->delete($id)){
				$details = $this->FamilyDetail->find('first',array('conditions'=>array('FamilyDetail.user_id'=>$id)));
				@unlink('uploads/profiles/family/thumb/'.$details['FamilyDetail']['profile_img']);
				@unlink('uploads/profiles/family/original/'.$details['FamilyDetail']['profile_img']);
				@unlink('uploads/profiles/family/profile_pic/'.$details['FamilyDetail']['profile_img']);
				$this->FamilyDetail->deleteAll(array('FamilyDetail.user_id'=>$id));
				
				$this->Session->setFlash(__('Family Member Successfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Family Member...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'family_profiles ',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=""){	
		$this->check_Permission('Family_Profile','edit');
		if($id!=""){							
			$this->User->recursive = 2;
			$user=$this->User->find('first',array('conditions'=>array('User.id'=>$id)));		
			if(empty($user)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'family_profile',
							'action' => 'index'
							));
			}
			$this->set('user',$user);
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'course',
						'action' => 'index'
						));
		}
		
		//===========Relationship=====================================================//
		$family_player_list = $this->FamilyPlayerRelationship->find('all',array('fields'=>array('FamilyPlayerRelationship.player_id')));		
		$player_id_list= array();		
		if(!empty($family_player_list))
		{
			foreach($family_player_list as $family_player_lists)
			{
				$player_id_list[]=$family_player_lists['FamilyPlayerRelationship']['player_id'];
			}
		}
		//"NOT" => array( "User.id" =>$player_id_list)
		$this->User->virtualFields = array('full_name' => "CONCAT(User.first_name, ' ', User.last_name)");
		$player_list = $this->User->find('list',array('conditions'=>array('User.type'=>'P'),'fields'=>array('User.id','User.full_name')));	
		$relation_list = $this->PlayerRelationship->find('list',array('fields'=>array('PlayerRelationship.id','PlayerRelationship.relationship')));
	    $FamilyDetail_data = $this->FamilyDetail->find('first',array('conditions'=>array('FamilyDetail.user_id'=>$id),'recursive'=>-1));
	   $relation_player_id= $this->FamilyPlayerRelationship->find('all',array('conditions'=>array('FamilyPlayerRelationship.member_id'=>$id))); 
	    $this->set('FamilyDetail_data',$FamilyDetail_data);		
		$this->set('player_list',$player_list);
		$this->set('relation_list',$relation_list);
		$this->set('all_relation_player_id',$relation_player_id);	
		//============================================================================//
		
		/*$this->User->virtualFields = array('full_name' => "CONCAT(User.first_name, ' ', User.last_name)");
		$player_list = $this->User->find('list',array('conditions'=>array('User.type'=>'P'),'fields'=>array('User.id','User.full_name')));	
		$relation_list = $this->PlayerRelationship->find('list',array('fields'=>array('PlayerRelationship.id','PlayerRelationship.relationship')));
	
		
		$this->set('player_list',$player_list);
		$this->set('relation_list',$relation_list);*/
		
		if($this->request->data){
			$this->FamilyDetail->deleteAll(array('FamilyDetail.user_id'=>$id));
			//$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$id));
			//$this->Video->deleteAll(array('Video.user_id'=>$id));
			$FamilyDetail['User']['first_name'] = $this->request->data['first_name'];
			$FamilyDetail['User']['last_name'] = $this->request->data['last_name'];
			$FamilyDetail['User']['email_address'] = $this->request->data['email_address'];
			$FamilyDetail['User']['type'] = 'F';
			$FamilyDetail['User']['status'] = $this->request->data['status'];
			$FamilyDetail['FamilyDetail']['dob'] = $this->request->data['dob'];
			
			$FamilyDetail['FamilyDetail']['player_id'] = 0;//$this->request->data['player_id'];
			$FamilyDetail['FamilyDetail']['relation_id'] =0; //$this->request->data['relation_id'];
			$FamilyDetail['FamilyDetail']['nationality'] = $this->request->data['nationality'];
			
			if(isset($this->request->data['profile_img'])){
				$FamilyDetail['FamilyDetail']['profile_img'] = $this->request->data['profile_pic'];
			}
			$this->User->id = $id;
			
			//pr($_FILES) ; exit;
			if($this->User->save($FamilyDetail)){
				$FamilyDetail['FamilyDetail']['user_id'] = $id;
				if(isset($_FILES['profile_img']['name']) && $_FILES['profile_img']['name']!="" ){
					$foo = new Upload($_FILES['profile_img']);
	
					  $foo->allowed = array('image/jpg/jpeg');
						if ($foo->uploaded) {
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/original/');
							   $foo->image_resize          = true;
							   //$foo->image_ratio           = true;
							   
							   if($foo->image_dst_x>$foo->image_dst_y)
							   {   
						           $foo->image_x               = 357;
								    $foo->image_ratio_y              = true;
    
							   }else if($foo->image_dst_x<$foo->image_dst_y)
							   {
								   $foo->image_y                = 439;
                                   $foo->image_ratio_x          = true;
								   
							   }else if($foo->image_dst_x==$foo->image_dst_y)
							   {
								   $foo->image_ratio_y         = true;
                                   $foo->image_x               = 357;
								   
							   }
							   
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/profile_pic/');
							   $foo->image_resize          = true;
					
							   $foo->image_y         = 160;
							   $foo->image_x         = 130;
								
							   
							   $foo->allowed = array('image/*');
							   $foo->Process('uploads/profiles/family/thumb/');
							  if ($foo->processed) {
								  @unlink('uploads/profiles/family/thumb/'.$user['FamilyDetail']['profile_img']);
								  @unlink('uploads/profiles/family/original/'.$user['FamilyDetail']['profile_img']);
								  @unlink('uploads/profiles/family/profile_pic/'.$user['FamilyDetail']['profile_img']);
								  $FamilyDetail['FamilyDetail']['profile_img'] = $foo->file_dst_name;
							  }
						}
						
				}else{
					
					$FamilyDetail['FamilyDetail']['profile_img'] = $user['FamilyDetail']['profile_img'];
				}				
				
				if($this->FamilyDetail->save($FamilyDetail))
				{
                    $payer_array=$this->request->data['player_id'];	
//pr( $payer_array);
//exit;	

//pr($this->request->data['relation_id']);
//exit;				
						$family_relation_data=array();
						if(!empty($payer_array))
						{   $i=0;					
					        $this->FamilyPlayerRelationship->deleteAll(array('FamilyPlayerRelationship.member_id'=>$id));
							foreach($payer_array as $payer_arrays)
							{
								if($payer_arrays>0 && $this->request->data['relation_id'][$i]>0)
								{
									$family_relation_data['FamilyPlayerRelationship']['member_id']=$id;
									$family_relation_data['FamilyPlayerRelationship']['player_id']=$payer_arrays;
									$family_relation_data['FamilyPlayerRelationship']['relation_id']=$this->request->data['relation_id'][$i];
									$this->FamilyPlayerRelationship->create();
									$this->FamilyPlayerRelationship->save($family_relation_data);
								}
								$i++;
							}
						}
			        
					$this->Session->setFlash(__('Profile Successfully Updated..','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
						'controller' => 'family_profiles',
						'action' => 'index'
						));
				}
			}
		}
	}
	
	function delete_family_multiple(){
		$this->check_Permission('Family_Profile','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$details = $this->FamilyDetail->find('first',array('conditions'=>array('FamilyDetail.user_id'=>$id)));
				@unlink('uploads/profiles/coach/thumb/'.$details['FamilyDetail']['profile_img']);
				@unlink('uploads/profiles/coach/original/'.$details['FamilyDetail']['profile_img']);
				@unlink('uploads/profiles/coach/profile_pic/'.$details['FamilyDetail']['profile_img']);
				$this->FamilyDetail->deleteAll(array('FamilyDetail.user_id'=>$id));
				$this->CoachCertificate->deleteAll(array('CoachCertificate.user_id'=>$id));
				$this->Video->deleteAll(array('Video.user_id'=>$id));
				$this->User->delete($id);
				$this->Session->setFlash(__('Coaches Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'family_profile',
						'action' => 'index'
						));
	}
	
	function get_nationality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$countries=$this->Country->find('all');
			$data="";
			foreach($countries as $country){
				if($id!="no"){
					if($country['Country']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$country['Country']['id'].'" id="'.$country['Country']['id'].'">'.$country['Country']['country_name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}
	}
	
	
	public function download_family_xls()
	{
		$flag = false;
		$data=array();

        $this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video','PlayerVideo')));
        $this->User->unBindModel(array('hasOne'=>array('PlayerDetail','CoachDetail')));

	    $this->FamilyDetail->bindModel(array(
	    'belongsTo' => array(
						'Country'=>array(
							 'className'=>'Country',
							 'order'        => '',
							 'dependent'    =>  false,
							 'foreignKey'=> 'nationality',
							 'fields'=>''
						)
					   )
					)
			   );     

				
		$this->Order->bindModel(array(
		'hasMany' => array(
					'OrderProduct'=>array(
					'className' =>'OrderProduct',
					'order'          => '',
					'foreignKey '=> 'order_id',
					'dependent' =>  false,
					'fields'=>''
					)
				)   
			)
		);	 		   

		$this->User->recursive=2;
		$view_content = $this->User->find('all',array('conditions'=>array('User.type'=>'F','User.status'=>'A')));
        $name='';
        $relation_arr=array();
      
   
		foreach ($view_content as $key=>$value)
		{
			
			 	
		 	$total_order = $this->Order->find('all',array('conditions'=>array('Order.customer_id'=>$view_content[$key]['User']['id'])));
            $total_order_count=count($total_order);
            $pro_array="";
            $p=1;
             
          
            if(!empty($total_order)){
				
				//pr($total_order);
				foreach($total_order as $purchase)
				{
				    $order_pro= $this->OrderProduct->find('all',array('conditions'=>array('OrderProduct.order_id'=>$view_content[$key]['User']['id'])));
				
					if(!empty($order_pro))
					{						
						
						foreach($order_pro as $p)
						{
							
							$purchase_order=$this->Product->find('first',array('conditions'=>array('Product.id'=>$p['OrderProduct']['product_id'])));
							
							if($p<$total_order_count)
							{
								$pro_array .= $purchase_order['Product']['name'].",";	
							}
							else
							{
								$pro_array .=$purchase_order['Product']['name'].",";
							}
							$p++;
						}
					}

				}
			}
            
            
			$data[$key]['First Name']=$view_content[$key]['User']['first_name'];		
			$data[$key]['Last Name']=$view_content[$key]['User']['last_name'];		
			$data[$key]['Date of Birth']=$view_content[$key]['FamilyDetail']['dob'];	
			$data[$key]['E-mail']=$view_content[$key]['User']['email_address'];		
			$data[$key]['Nationality']=$view_content[$key]['FamilyDetail']['Country']['country_name'];	
		    $data[$key]['Total Purchase Made']=$total_order_count;
		    $data[$key]['Purchase Made']=$pro_array;		 
		   //pr($view_content);		  		    
		    $total_relation=$this->FamilyPlayerRelationship->find('all',array('conditions'=>array('FamilyPlayerRelationship.member_id'=>$view_content[$key]['User']['id'])));		
		     $r=0;
		    
		   // pr($total_relation);
			///exit;
			
			  if(!empty($total_relation) && count($total_relation)>0)
			  {
		   
				  foreach($total_relation as $key1=>$val){
				  
					  $relatn_id=$val['FamilyPlayerRelationship']['relation_id'];	
					  
					  $player_id=$val['FamilyPlayerRelationship']['player_id'];
					  
					//=======================get relation user name============================//
					
					 $this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video','PlayerVideo')));
					 $this->User->unBindModel(array('hasOne'=>array('FamilyDetail','CoachDetail')));
						 
					  $player_detail = $this->User->find('first',array('conditions'=>array('User.id'=>$player_id,'User.type'=>'P','User.status'=>'A')));
					 
			  
					  $player_relatnshp = $this->PlayerRelationship->find('first',array('conditions'=>array('PlayerRelationship.id'=>$relatn_id)));
					  
					 
					
						   if(isset($player_detail['User']['first_name']))
						   {					   
							$name= $player_detail['User']['first_name'].' '.$player_detail['User']['last_name'];
					$relation_arr[$key]['player_associaton'.$r] = $name.'-'.$player_relatnshp['PlayerRelationship']['relationship'];		
						   }else{
							$name="";	
							$relation_arr[$key]['player_associaton'.$r] = $name.''.$player_relatnshp['PlayerRelationship']['relationship'];
						   }
					       
			          $r++;
					} 
					
					//pr($relation_arr);
					//exit;
				
			} 
			 //echo $relation_arr[$key]['player_associaton0'];
			// exit;
			
			 if(isset($relation_arr[$key]['player_associaton0']))
				{
					$data[$key]['Player Associaton 1']=$relation_arr[$key]['player_associaton0'];
				 }else
				 {				
					$data[$key]['Player Associaton 1']='';
				 }
			 
			 if(isset($relation_arr[$key]['player_associaton1']))
			{
			    $data[$key]['Player Associaton 2']=$relation_arr[$key]['player_associaton1'];
			 }else
			 {				
				$data[$key]['Player Associaton 2']='';
			 }
			 
			  if(isset($relation_arr[$key]['player_associaton2']))
			  {
			    $data[$key]['Player Associaton 3']=$relation_arr[$key]['player_associaton2'];
			 }else
			 {				
				$data[$key]['Player Associaton 3']='';
			 } 
			
		}
       
	  //  pr($data);
		//exit;
	   
		// file name for download
		$filename = "family" . date('Ymd') . ".xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Content-Type: application/vnd.ms-excel");
		
	
		foreach($data as $row) 
		{
			if(!$flag) {
				echo implode("\t", array_keys($row)) . "\n";
				$flag = true;
			}
		
		 array_walk($row,function(&$row)
	     {
		$row = preg_replace("/\t/", "\\t", $row);
		$row= preg_replace("/\r?\n/", "\\n", $row);
		if(strstr($row, '"')) $row = '"' . str_replace('"', '""', $row) . '"';
		
	     });
			
			echo implode("\t", array_values($row)) . "\n";
		
		}
		exit;  
	}
	
	
}
