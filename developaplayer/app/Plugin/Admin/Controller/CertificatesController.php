<?php
App::uses('AdminAppController', 'Admin.Controller');
class CertificatesController extends AdminAppController {
	var $uses=array('Admin.Certificate');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('Certificate','view');
	}
	
	public function certificate_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Certificate.id',
			1 =>'Certificate.name',
			2 =>'Certificate.status',
			2 =>'Certificate.created',
		);
		$totalData = $this->Certificate->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "Certificate.type = 'C'";
		if( !empty($requestData['search']['value']) ) {
			$sql.="AND ( Certificate.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR Certificate.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Certificate.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$positions = $this->Certificate->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->Certificate->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($positions);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($positions);
		}
		$data = array();
		foreach( $positions as $row ) {  // preparing an array
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Certificate']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="name_'.$row['Certificate']["id"].'">'.$row['Certificate']["name"].'</div>';
			
			if($row['Certificate']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Certificate']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['Certificate']["created"]));
			
			//================Certificate Permission Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.status')=='N' && $this->Session->read('permissions.Certificate.edit')=='N'  && $this->Session->read('permissions.Certificate.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//==============Certificate Permission Status========================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Certificate']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Certificate']['id'].'"><img id="status_img_'.$row['Certificate']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Certificate Permission Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="javascript:edit_data('.$row['Certificate']['id'].',\''.$row['Certificate']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Certificate Permission Delete======================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Certificate']['name'].'?\')" href="'.$this->webroot.'admin/certificates/delete_certificate/'.$row['Certificate']['id'].'" title="Delete" id="delete_'.$row['Certificate']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Certificate']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Certificate']['id'].'"><img id="status_img_'.$row['Certificate']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['Certificate']['id'].',\''.$row['Certificate']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Certificate']['name'].'?\')" href="'.$this->webroot.'admin/certificates/delete_certificate/'.$row['Certificate']['id'].'" title="Delete" id="delete_'.$row['Certificate']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_certificate(){
		$this->check_Permission('Certificate','add');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['Certificate']['name']);
			if(!empty($this->request->data['name'])){
				$check_position = $this->Certificate->find('first',array('conditions'=>array('Certificate.name'=>$this->request->data['name'])));
				if(empty($check_position)){
					$data['Certificate']['name'] = $this->request->data['name'];
					$data['Certificate']['type'] = 'C';
					if($this->Certificate->save($data)){
						$inserted_id = $this->Certificate->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="name_'.$inserted_id.'">'.$data['Certificate']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['Certificate']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['Certificate']['name'].'?\')" href="'.$this->webroot.'admin/certificates/delete_certificate/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'certificates',
						'action' => 'index'
			));
		}
	}
	
	function status(){
		$this->check_Permission('Certificate','view');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Certificate']['status'] = $this->request->data['status'];
			$this->Certificate->id = $this->request->data['id'];
			if($this->Certificate->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_certificate($id=""){
		$this->check_Permission('Certificate','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->Certificate->delete($id)){
				$this->Session->setFlash(__('Certificate Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Certificate...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'certificates',
						'action' => 'index'
						));
		}
	}
	
	function update_certificate(){
		$this->check_Permission('Certificate','edit');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_position = $this->Certificate->find('first',array('conditions'=>array('Certificate.name'=>$this->request->data['position'])));
			if(!empty($check_position)){
				echo "exist";
			}else{
				$data['Certificate']['name'] = $this->request->data['position'];
				$this->Certificate->id = $this->request->data['id'];
				if($this->Certificate->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_certificate_multiple(){
		$this->check_Permission('Certificate','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->Certificate->create();
				$this->Certificate->delete($id);
				$this->Session->setFlash(__('Certificates Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'certificates',
						'action' => 'index'
						));
	}
}