<?php
App::uses('AdminAppController', 'Admin.Controller');
class AdminModeratorsController extends AdminAppController {
 var $uses=array('Admin.AdminMenu','Admin.AdminMenuPermission','Admin.AdminModerator','Admin.User');
	 public function beforeFilter() {
        $this->Auth->allow(array('all_admin_moderators','add_admin_moderator','admin_moderator_detail','admin_moderator_delete','admin_moderator_multiple_delete','check_exist_email'));
	   parent::beforeFilter();
	   
    }
	public function index(){
		$this->redirect(
				array("action" => "all_admin_moderators"));
	}
	
	public function all_admin_moderators()
	{
		$admin_moderators=$this->User->find('all',array('conditions'=>array('User.type'=>'M')));
		$this->set('title_for_layout', 'View All Moderators');
		$this->set('admin_moderators',$admin_moderators);
	}
	
	public function send_mail($email_to='')
	{
		//showing all units
		$this->layout="ajax";
		if(isset($email_to)){
			$this->set('email_to',$email_to);
			if($this->request->data){
				$Email = new CakeEmail();
				if(isset($this->request->data['from']) && isset($this->request->data['to']) && isset($this->request->data['subject']) && isset($this->request->data['message'])){
					$Email->from(array($this->request->data['from']=>'Amigros'))
						->to($this->request->data['to'])
						->subject($this->request->data['subject'])
						->send($this->request->data['message']);
						$this->Session->setFlash(__('Email Sent..'));
				}else{
					$this->Session->setFlash(__('Email Sending Error..'));
				}
			}
		}else{
			$this->Session->setFlash(__('No email address was entered..'));
			$this->redirect(
				array("action" => "send_mail","/"=>$email_to));
		}
	}
	
	public function admin_moderator_delete($id='')
	{
		$this->autoRender= false;
		$this->layout="ajax";
		if($id<>''){
			$id=base64_decode($id);
			if($this->User->delete($id)){
				$this->AdminMenuPermission->deleteAll(array('AdminMenuPermission.moderator_id'=>$id));
				$this->Session->setFlash(_('Moderator Succesfully Deleted'));
			}else{
				$this->Session->setFlash(_('Moderator Was Not Deleted'));
			}
		}else{
			$this->Session->setFlash("No Moderator was Deleted..");
		}
		$this->redirect(
		array("action" => "all_admin_moderators"));
	}
	
	public function admin_moderator_multiple_delete()
	{
		//deleting selected multiple AdminModerator
		$this->autoRender= false;
		$this->layout="ajax";
		if(isset($this->request->data['id'])){
			foreach($this->request->data['id'] as $id){
				$this->User->delete($id);
				$this->AdminMenuPermission->deleteAll(array('AdminMenuPermission.moderator_id'=>$id));
			}
		}else{
			$this->Session->setFlash("No Moderators were Deleted..");
		}
		$this->Session->setFlash("Moderator Successfully Deleted..");
		$this->redirect(
			array("action" => "all_admin_moderators"));
	}
	
	
	public function admin_moderator_detail($id)
	{
		$id=base64_decode($id);
		if($this->request->is('post'))
		{
			$del_permissions=$this->AdminMenuPermission->find('all',array('conditions'=>array('AdminMenuPermission.moderator_id'=>$id)));
			foreach($del_permissions as $del_permission){
				$this->AdminMenuPermission->delete($del_permission['AdminMenuPermission']['id']);
			}
			foreach($this->request->data['permission'] as $k=>$data){
						$permission = array();
						$permission['moderator_id']=$id;
						$permission['admin_menu_id']=$k;
						if(isset($data['add_permission'])){
							$permission['add_permission']=$data['add_permission'];
						}
						if(isset($data['view_permission'])){
							$permission['view_permission']=$data['view_permission'];
						}
						if(isset($data['edit_permission'])){
							$permission['edit_permission']=$data['edit_permission'];
						}
						if(isset($data['delete_permission'])){
							$permission['delete_permission']=$data['delete_permission'];
						}
						
						if(isset($data['status_permission'])){
							$permission['status_permission']=$data['status_permission'];
						}
						
						$this->AdminMenuPermission->create();
						if($this->AdminMenuPermission->save($permission)){
							$this->Session->setFlash(_('Moderator Succesfully Added'));
						}else{
							$this->Session->setFlash(_('Error!! Try Again..'));
							}
			}
			if($this->User->save($this->request->data)){
				$this->Session->setFlash(_('Moderator Succesfully Updated'));
			}else{
				$this->Session->setFlash(_('Error!!'));
			}
			$this->redirect(
				array("action" => "all_admin_moderators"));
		}
		$this->AdminMenu->bindModel(array(
					'hasOne'=>array(
						'AdminMenuPermission'=>array(
							'className'=>'AdminMenuPermission',
							 'conditions'   => array('AdminMenuPermission.moderator_id'=>$id),
							 'order'        => '',
							 'dependent'    =>  true,
							'foreignKey'=>'admin_menu_id',
							'fields'       =>''
							)
					)
			));
		$menus=$this->AdminMenu->find('all');
		$this->set('menus',$menus);
		$moderator=$this->User->find('first',array('conditions'=>array('User.id'=>$id)));
		$this->set('title_for_layout', 'Admin Panel: Moderator Details('.$moderator['User']['first_name'].')');	
		$this->set('moderator',$moderator);
	}
	
	public function add_admin_moderator()
	{
		if($this->request->is('post'))
		{
			$flag=0;
			
			if(isset($this->request->data)){
				
				$this->request->data['type']='M';
				
				//pr($this->request->data);
				//exit;
				
					$this->request->data['password']=$this->Auth->password($this->request->data['password']);
					$this->User->save($this->request->data);
				
					$inserted_id = $this->User->getLastInsertId();
					if(isset($this->request->data['permission'])){
					foreach($this->request->data['permission'] as $k=>$data){
						$permission = array();
						$permission['moderator_id']=$inserted_id;
						$permission['admin_menu_id']=$k;
						if(isset($data['add_permission'])){
							$permission['add_permission']=$data['add_permission'];
						}
						if(isset($data['view_permission'])){
							$permission['view_permission']=$data['view_permission'];
						}
						if(isset($data['edit_permission'])){
							$permission['edit_permission']=$data['edit_permission'];
						}
						if(isset($data['delete_permission'])){
							$permission['delete_permission']=$data['delete_permission'];
						}
						
						if(isset($data['status_permission'])){
							$permission['status_permission']=$data['status_permission'];
						}
						$this->AdminMenuPermission->create();
						if($this->AdminMenuPermission->save($permission)){
							$this->Session->setFlash(_('Moderator Succesfully Added'));
						}else{
							$flag=1;
							break;
							}
					}
					
				}else{
					$this->Session->setFlash(_('This email id is already registered!'));
				}
				}else{
				$this->Session->setFlash(_('Form Was Blank!!'));
				}
				if($flag==1){
					$this->User->delete($inserted_id);
					$this->AdminMenuPermission->query('Delete * from admin_menu_permission where moderator_id='.$inserted_id);
						$this->Session->setFlash(_('Error!! Try Again..'));
				}
				$this->Session->setFlash(_('Moderator Succesfully Added'));
				$this->redirect(				
					array("action" => "all_admin_moderators"));
			       }			
		         $menus=$this->AdminMenu->find('all');
				
		$this->set('menus',$menus);
	$this->set('title_for_layout', 'Admin Panel: Add Moderator');	
	}
	
	public function check_exist_email()
	{
		$this->layout = 'ajax'; 
		$this->autoRender = false;
		$moderator_check=$this->User->find('count', array('conditions' => array('User.email_address' =>$this->request->data['email'])));
		if($moderator_check == 1) 
		{
			return 1;
		}
	}
}