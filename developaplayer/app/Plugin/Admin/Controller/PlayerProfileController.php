<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class PlayerProfileController extends AdminAppController {
	var $uses=array('Admin.Country','Admin.User','Admin.PlayerDetail','Admin.Video','Admin.CoachCategory','Admin.CoachLevel','Admin.LavelAssignment','Admin.AddCoachRating','Admin.AddCoachRating','Admin.UpperLowerLimit','Admin.AgeGroup','Admin.LevelCategory','Admin.BookCourseByPlayer','Admin.CourseToPosition','Admin.AchiveCertificate','Admin.AchiveCertificate','Admin.PlayerVideo','Admin.BookCourseByPlayer','Admin.PlayerPosition','Admin.Level','Admin.FamilyPlayerRelationship','Admin.PlayerRelationship','Admin.Club');
	 public function beforeFilter() {
	   parent::beforeFilter();
    }
     
	 public function index()
	 {
          
		  $this->check_Permission('Player_Profile','view');
          	   
	 }	
	
	function certificate_status(){		
	$certificated_id= $this->request->params['pass'][0];   
	$status= $this->request->params['pass'][1]; if($status==0) 
	{	$status_last=1;  } 
	else {	$status_last=0;   }	
	$this->AchiveCertificate->id=$certificated_id;	
	$upadate_array['AchiveCertificate']['status']= $status_last;  
	$this->AchiveCertificate->save($upadate_array);	
	$player_id=$this->AchiveCertificate->find('first',array('conditions'=>array('AchiveCertificate.id'=>$certificated_id)));
	$this->redirect(array('plugin' => 'admin','controller'=>'player_profile','action'=>'view_certificate/'.$player_id['AchiveCertificate']['player_id']));
	}
	
	function delete_certificate()
	{	 
	$certificated_id= $this->request->params['pass'][0];
	$player_id=$this->AchiveCertificate->find('first',array('conditions'=>array('AchiveCertificate.id'=>$certificated_id)));
	$palyer_cer_id=  $player_id['AchiveCertificate']['player_id'];
	$this->AchiveCertificate->delete($certificated_id);	
	$player_id=$this->AchiveCertificate->find('first',array('conditions'=>array('AchiveCertificate.id'=>$certificated_id)));
	$this->redirect(array('plugin' => 'admin','controller'=>'player_profile','action'=>'view_certificate/'. $palyer_cer_id));
	} 	
	
	
	public function player_data(){
		$this->layout="ajax";
		$this->render(false);
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'User.id',
			1 =>'User.profile_img',
			3 =>'User.first_name',
			4 =>'PlayerDetail.dob',
			5 =>'PlayerDetail.primary_speciality_id',
			6 =>'User.status',
			7 =>'User.created'
		);
		$sql = "User.type='P'";
		if( !empty($requestData['search']['value']) ) {
			$sql.="AND ( User.first_name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR User.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR User.created LIKE '".$requestData['search']['value']."%' )";
		}
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$totalData = $this->User->find('count',array('conditions'=>array('User.type'=>'P'))); 
		$totalFiltered = $totalData;
		$this->User->unBindModel(array('hasOne' => array('User','CoachDetail','FamilyDetail')));
		$this->User->unBindModel(array('hasMany' => array('User','CoachCertificate','Video')));
		$users = $this->User->find('all',array('conditions'=>$sql,'recursive'=>2,'limit'=>$requestData['length'],'offset' => $requestData['start']));
		//pr($users);die;
		if( !empty($requestData['search']['value']) )
		{            
		  $totalFiltered = count($users);
		}						
		$data = array();
		foreach( $users as $row ) { 
		// preparing an array
		$cheked="";		
		if($row['PlayerDetail']['propuler_player']==1)
		{
			$cheked='checked';
		}else
		{
		$cheked="";				
		}
			if(isset($row['AddCoachRating']['id']))
			{
				$limit_id='/'.$row['AddCoachRating']['id'];				
			}else
			{ 
				$limit_id='/'.'0';    
			}
			$nestedData=array();

			if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['User']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="coach_logo_'.$row['User']['id'].'"><img height="120px" width="100%" src="'.$this->webroot.'uploads/profiles/player/thumb/'.$row['PlayerDetail']['profile_img'].'" /></div>';
			$nestedData[] = '<div id="coach_name_'.$row['User']['id'].'">'.$row['User']['first_name'].' '.$row['User']['last_name'].'</div>';
			$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">'.$row['PlayerDetail']['dob'].'</div>';
			if(!empty($row['PlayerDetail']['PrimaryPosition']['name']) && !empty($row['PlayerDetail']['SecondaryPosition']['name']))
			{
				$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">Primary: '.$row['PlayerDetail']['PrimaryPosition']['name'].'<br />Secondary: '.$row['PlayerDetail']['SecondaryPosition']['name'].'</div>';	
			}
			else
			{
				$nestedData[] = '<div id="coach_dob_'.$row['User']['id'].'">Primary: No record found<br />Secondary: No record found</div>';
			}
			if($row['User']['status']=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			
		    $nestedData[] = '<div id="status_of_'.$row['User']["id"].'">'.$status.'</div>';		
			$nestedData[] = date('d-M-Y',strtotime($row['User']["created"]));
			$nestedData[] ='<input class="checkbox_del" id="highlight_id_'.$row['User']['id'].'" name="highlight_id" value="'.$row['User']["id"].'" type="checkbox" onclick="propuler_player('.$row['User']["id"].')"  '.$cheked.' />';
			$certificate1 = '';
			if($this->BookCourseByPlayer->hasAny(array('BookCourseByPlayer.player_id'=>$row['User']['id']))){
				$certificate1 = '&nbsp;<a href="'.$this->webroot.'admin/player_profile/assign_certificate/'.$row['User']['id'].$limit_id.'"><img alt="Assign certificate" title="Assign certificate without course" width="16px"  src="'.$this->webroot.'admin/img/icons/add_certificate.png" /></a>';
			}else
			{
			  $certificate1 = '&nbsp;<a href="'.$this->webroot.'admin/player_profile/assign_certificate_without_course/'.$row['User']['id'].$limit_id.'"><img alt="Assign certificate" title="Assign certificate without course" width="16px"  src="'.$this->webroot.'admin/img/icons/add_certificate.png" /></a>'; 				
			}
			
		//================Player Permission Check==============================//
		
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.delete')=='N' && $this->Session->read('permissions.Player_Profile.edit')=='N' && $this->Session->read('permissions.Player_Profile.stuatus')=='N')
		{  
			$nestedData[]="";
			
		}						
		//================Player Permission Delete==============================//
         $nestedData_delete="";   
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.delete')=='Y')
		{  
			$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].' '.$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/player_profile/delete_coach/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="24px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
		}
          //================Player Permission edit==============================//
		$nestedData_edit="";
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.edit')=='Y')
		{  
			$nestedData_edit ='&nbsp<a href="'.$this->webroot.'admin/player_profile/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="20px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
		}
		//================Player Permission status==============================//
		$nestedData_status="";
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.status')=='Y')
		{  
			$nestedData_status ='<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="20px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
		}
		
		//================Player Permission certificate==============================//
		$nestedData_certificate="";
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y' && $this->Session->read('permissions.Player_Profile.add')=='Y' && $this->Session->read('permissions.Player_Profile.edit')=='Y')
		{  
			$nestedData_certificate = $certificate1.'&nbsp<a href="'.$this->webroot.'admin/player_profile/view_certificate/'.$row['User']['id'].'"><img alt="View certificate" title="View certificate" width="16px" src="'.$this->webroot.'admin/img/icons/view_certificate.png" /></a>&nbsp<a href="'.$this->webroot.'admin/ratings/add_rating/'.$row['User']['id'].'"><img alt="Add Rating" title="Add Rating" width="24px" src="'.$this->webroot.'admin/img/icons/add_rating.png" /></a> &nbsp<a href="'.$this->webroot.'admin/ratings/view_rating/'.$row['User']['id'].'"><img alt="View Rating" title="View Rating" width="24px" src="'.$this->webroot.'admin/img/icons/view_rating.png" /></a>';
		}
			 
		//==============================================================================//
		    if($this->Session->read('Auth.User.type')=='M')
			{	
		       $nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete.''.$nestedData_certificate;
			   
			}else{
				
				$nestedData[] = '<a href="javascript:change_status('.$row['User']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['User']['id'].'"><img id="status_img_'.$row['User']['id'].'" alt="Inactive" width="20px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/player_profile/edit/'.$row['User']['id'].'"><img alt="Edit" title="Edit" width="20px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['User']['first_name'].' '.$row['User']['last_name'].'?\')" href="'.$this->webroot.'admin/player_profile/delete_coach/'.$row['User']['id'].'" title="Delete" id="delete_'.$row['User']['id'].'"><img alt="Delete" width="24px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>'.$certificate1.'&nbsp<a href="'.$this->webroot.'admin/player_profile/view_certificate/'.$row['User']['id'].'"><img alt="View certificate" title="View certificate" width="16px" src="'.$this->webroot.'admin/img/icons/view_certificate.png" /></a>&nbsp<a href="'.$this->webroot.'admin/ratings/add_rating/'.$row['User']['id'].'"><img alt="Add Rating" title="Add Rating" width="24px" src="'.$this->webroot.'admin/img/icons/add_rating.png" /></a> &nbsp<a href="'.$this->webroot.'admin/ratings/view_rating/'.$row['User']['id'].'"><img alt="View Rating" title="View Rating" width="24px" src="'.$this->webroot.'admin/img/icons/view_rating.png" /></a>';
			
			}
			
			$data[] = $nestedData;
		}
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
		echo json_encode($json_data);  // send data as json format
	}
	function add_player(){
		
		$this->check_Permission('Player_Profile','add');
		
		$playerPosition = $this->PlayerPosition->find('list',array('fields'=>array('PlayerPosition.id','PlayerPosition.name')));
		$this->set('playerPosition',$playerPosition);
		//pr($playerPosition);
		//exit;
		if(!empty($this->data)){
				//======================  User Table Information ==============================
				$player_user_details['User']['first_name'] = $this->data['first_name'];
				$player_user_details['User']['last_name'] = $this->data['last_name'];
				$player_user_details['User']['email_address'] = $this->data['email_address'];
				$player_user_details['User']['password'] = $this->Auth->password($this->data['password']);
				$player_user_details['User']['type'] = 'P';
				//===============================================================================
				$player_details['PlayerDetail']['height'] = $this->data['height'];
				$player_details['PlayerDetail']['weight'] = $this->data['weight'];
				$player_details['PlayerDetail']['dob'] = $this->data['dob'];
				$player_details['PlayerDetail']['primary_position_id'] = $this->data['primary_position_id'];
				$player_details['PlayerDetail']['secondary_position_id'] = $this->data['secondary_position_id'];
				$player_details['PlayerDetail']['nationality'] = $this->data['nationality'];
				$player_details['PlayerDetail']['bench_press'] = $this->data['bench_press'];
				$player_details['PlayerDetail']['dead_lift'] = $this->data['dead_lift'];
				$player_details['PlayerDetail']['20m_sprint'] = $this->data['20m_sprint'];
				$player_details['PlayerDetail']['60m_sprint'] = $this->data['60m_sprint'];
				$player_details['PlayerDetail']['club_id'] = $this->data['club_id'];
				$player_details['User']['status'] = $this->data['status'];
				$player_details['PlayerDetail']['manage_by'] = $this->data['manage_by'];
				$player_details['PlayerDetail']['sponsor_by'] = $this->data['sponsor_by'];
				if(isset($this->params['form']['profile_img']['name'])){
					$player_details['PlayerDetail']['profile_img'] = $this->params['form']['profile_img']['name'];
				}
			$this->User->set($this->data);
			if($this->User->validates($this->data)){
				if($this->PlayerDetail->validates($this->data)){
					if($this->User->save($player_user_details,false)){						$player_details['PlayerDetail']['user_id'] = $this->User->getLastInsertId();
						if($_FILES){
							$foo = new Upload($this->params['form']['profile_img']); 
							  $foo->allowed = array('image/jpg/jpeg');
								if ($foo->uploaded) {
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/original/');
									   $foo->image_resize          = true;									   
								if($foo->image_dst_x>$foo->image_dst_y)
								   {
										$foo->image_ratio_x        = true;
										$foo->image_y              = 357;
								   }else if($foo->image_dst_x<$foo->image_dst_y)
								   {
									   $foo->image_ratio_y               = true;
									   $foo->image_x                     = 439;
								   }else if($foo->image_dst_x==$foo->image_dst_y)
								   {
									   $foo->image_ratio_y         = true;
									   $foo->image_x               = 357;
								   }
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/profile_pic/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 160;
									   $foo->image_x               = 130;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/profiles/player/thumb/');
									  if ($foo->processed) {
										$player_details['PlayerDetail']['profile_img'] = $foo->file_dst_name;
									  }
								}
						}
						//$random_otp = '0123';
						//$player_details['PlayerDetail']['otp'] =$random_otp;
						if($this->PlayerDetail->save($player_details)){
							$last_user_id =base64_encode($player_details['PlayerDetail']['user_id']);
							if(!empty($this->request->data['video_link']))
							{
								//pr($this->request->data['video_link']);
								$i=1;
								foreach($this->request->data['video_link'] as $video_link)
								{
									//========= Get all youtube video information ========================
									if(!empty($video_link))	
									{									
										$video_url = $video_link;
										$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
										$youtube_data = json_decode(file_get_contents($url), true);
										//pr($youtube_data);
										$player_v_details['PlayerVideo']['user_id'] = $player_details['PlayerDetail']['user_id'];
										$player_v_details['PlayerVideo']['video_link'] = $video_link;
										$player_v_details['PlayerVideo']['video_iframe'] = $youtube_data['html'];
										$player_v_details['PlayerVideo']['video_title'] = $youtube_data['title'];
										$player_v_details['PlayerVideo']['video_image'] = $youtube_data['thumbnail_url'];
										$player_v_details['PlayerVideo']['video_position'] = $i;
										$player_v_details['PlayerVideo']['video_type'] = 0;
										$this->PlayerVideo->create();
										$this->PlayerVideo->save($player_v_details);
										$i++;					
									}
								}
							}
							$this->Session->setFlash(__('<strong style="color:green">Profile Created.</strong>'));
							$this->redirect(array('action'=>'index'));
							//$this->redirect(array('action'=>'complt_reg','myArgument' => $last_user_id));
						}else{
							$this->Session->setFlash(__('<strong style="color:red">Unable To Update Profile.</strong>'));
						}
					}
				}
			}else
			{
				//pr($this->User->invalidFields());
			}		}
	}
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['User']['status'] = $this->request->data['status'];
			$this->User->id = $this->request->data['id'];
			if($this->User->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	function delete_coach($id=""){
		//Configure::write('debug', 2);
		$this->check_Permission('Player_Profile','delete');
		
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->User->delete($id)){
				$details = $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$id)));
				unlink(WWW_ROOT.'uploads/profiles/player/thumb/'.$details['PlayerDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/player/original/'.$details['PlayerDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/player/profile_pic/'.$details['PlayerDetail']['profile_img']);
				$this->PlayerDetail->deleteAll(array('PlayerDetail.user_id'=>$id));				
				$this->PlayerVideo->deleteAll(array('PlayerVideo.user_id'=>$id));
				$this->Session->setFlash(__('Player Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Player...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'player_profile',
						'action' => 'index'
						));
		}
	}
	function edit($id=""){
		
		$this->check_Permission('Player_Profile','edit');	
		  
		$playerPosition = $this->PlayerPosition->find('list',array('fields'=>array('PlayerPosition.id','PlayerPosition.name')));
		$this->set('playerPosition',$playerPosition);
		if($id!=""){
			$user=$this->User->find('first',array('conditions'=>array('User.id'=>$id)));
			if(empty($user)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'player_profile',
							'action' => 'index'
							));
			}
			$this->set('user',$user);
		}
		if(!empty($this->data)){
				//======================  User Table Information ==============================
				$player_user_details['User']['first_name'] = $this->data['first_name'];
				$player_user_details['User']['last_name'] = $this->data['last_name'];
				//$player_user_details['User']['email_address'] = $this->data['email_address'];
				//$player_user_details['User']['password'] = $this->Auth->password($this->data['password']);
				$player_user_details['User']['type'] = 'P';
				//===============================================================================
				$player_details['PlayerDetail']['height'] = $this->data['height'];
				$player_details['PlayerDetail']['weight'] = $this->data['weight'];
				$player_details['PlayerDetail']['dob'] = $this->data['dob'];
				$player_details['PlayerDetail']['primary_position_id'] = $this->data['primary_position_id'];
				$player_details['PlayerDetail']['secondary_position_id'] = $this->data['secondary_position_id'];
				$player_details['PlayerDetail']['nationality'] = $this->data['nationality'];
				$player_details['PlayerDetail']['bench_press'] = $this->data['bench_press'];
				$player_details['PlayerDetail']['dead_lift'] = $this->data['dead_lift'];
				$player_details['PlayerDetail']['20m_sprint'] = $this->data['20m_sprint'];
				$player_details['PlayerDetail']['60m_sprint'] = $this->data['60m_sprint'];
				$player_details['PlayerDetail']['club_id'] = $this->data['club_id'];
				$player_details['User']['status'] = $this->data['status'];
				$player_details['PlayerDetail']['manage_by'] = $this->data['manage_by'];
				$player_details['PlayerDetail']['sponsor_by'] = $this->data['sponsor_by'];
				if(isset($this->params['form']['profile_img']['name'])){
					$player_details['PlayerDetail']['profile_img'] = $this->params['form']['profile_img']['name'];
				}
			$this->User->set($this->data);
			if($this->User->validates($this->data)){
				if($this->PlayerDetail->validates($this->data)){
					$this->User->id = $id;
					if($this->User->save($player_user_details,false))					{						$playerId = $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$id)));						//$player_details['PlayerDetail']['user_id'] = $this->User->getLastInsertId();
						if($_FILES){
							if($_FILES['profile_img']['error'] == 0){
								//pr($_FILES['profile_img']['error']);die;
								$foo = new Upload($this->params['form']['profile_img']); 
								  $foo->allowed = array('image/jpg/jpeg');
									if ($foo->uploaded) {
										   $foo->allowed = array('image/*');
										   $foo->Process('uploads/profiles/player/original/');
										   $foo->image_resize          = true;
									if($foo->image_dst_x>$foo->image_dst_y)
									   {  
											$foo->image_ratio_x        = true;
											$foo->image_y              = 357;
									   }else if($foo->image_dst_x<$foo->image_dst_y)
									   {										   $foo->image_ratio_y               = true;
										   $foo->image_x                     = 439;
									   }else if($foo->image_dst_x==$foo->image_dst_y)
									   {
										   $foo->image_ratio_y         = true;
										   $foo->image_x               = 357;
									   }
										   $foo->allowed = array('image/*');
										   $foo->Process('uploads/profiles/player/profile_pic/');
										   $foo->image_resize          = true;
										   $foo->image_y               = 160;
										   $foo->image_x               = 130;
										   $foo->allowed = array('image/*');
										   $foo->Process('uploads/profiles/player/thumb/');
										  if ($foo->processed) {
												$player_details['PlayerDetail']['profile_img'] = $foo->file_dst_name;
												unlink(WWW_ROOT.'uploads/profiles/player/thumb/'.$playerId['PlayerDetail']['profile_img']);
												unlink(WWW_ROOT.'uploads/profiles/player/original/'.$playerId['PlayerDetail']['profile_img']);
												unlink(WWW_ROOT.'uploads/profiles/player/profile_pic/'.$playerId['PlayerDetail']['profile_img']);
										  }										}
							}else{
								$player_details['PlayerDetail']['profile_img'] = $playerId['PlayerDetail']['profile_img'];
							}
						}
						$random_otp = '0123';
						//$random_otp= $this->n_digit_random(4);
						$player_details['PlayerDetail']['otp'] =$random_otp;
						//echo $player_user_details['User']['email_address'];
					    //exit;
						$player_details['PlayerDetail']['user_id'] = $id;
						$this->PlayerDetail->id = $playerId['PlayerDetail']['id'];
						if($this->PlayerDetail->save($player_details))						{
							if(!empty($this->request->data['video_link']))
							{								$this->PlayerVideo->deleteAll(array('PlayerVideo.user_id'=>$id,'PlayerVideo.video_type'=>0));
								$i=1;
								foreach($this->request->data['video_link'] as $video_link)
								{
									//========= Get all youtube video information ========================
									if(!empty($video_link))									{										$video_url = $video_link;
										$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
										$youtube_data = json_decode(file_get_contents($url), true);
										//pr($youtube_data);
										$player_v_details['PlayerVideo']['user_id'] = $player_details['PlayerDetail']['user_id'];
										$player_v_details['PlayerVideo']['video_link'] = $video_link;
										$player_v_details['PlayerVideo']['video_iframe'] = $youtube_data['html'];
										$player_v_details['PlayerVideo']['video_title'] = $youtube_data['title'];
										$player_v_details['PlayerVideo']['video_image'] = $youtube_data['thumbnail_url'];
										$player_v_details['PlayerVideo']['video_position'] = $i;
										$player_v_details['PlayerVideo']['video_type'] = 0;
										$this->PlayerVideo->create();
										$this->PlayerVideo->save($player_v_details);
										$i++;									}
								}
							}
							$last_user_id =base64_encode($player_details['PlayerDetail']['user_id']);
							$this->Session->setFlash(__('Player profile has been updated successfully.'));
							$this->redirect(array('action'=>'index'));
						}else{
							$this->Session->setFlash(__('<strong style="color:red">Unable to Update Profile.</strong>'));
						}
					}
				}
			}else
			{
				//pr($this->User->invalidFields());
			}
		}
	}	
	public function delete_course_multiple()
	{		
		$this->autoRender= false;
		$this->layout="ajax";
		foreach($this->request->data['id'] as $id){
			$this->BookCourseByPlayer->delete($id);
		}
		$this->Session->setFlash("Courses has been Successfully Deleted..");
		$this->redirect(array("action" => "bookedCourse"));
	}
	
	
	function delete_player_multiple(){
		$this->check_Permission('Player_Profile','delete');	
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$details = $this->PlayerDetail->find('first',array('conditions'=>array('PlayerDetail.user_id'=>$id)));
				unlink(WWW_ROOT.'uploads/profiles/player/thumb/'.$details['PlayerDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/player/original/'.$details['PlayerDetail']['profile_img']);
				unlink(WWW_ROOT.'uploads/profiles/player/profile_pic/'.$details['PlayerDetail']['profile_img']);
				$this->PlayerDetail->deleteAll(array('PlayerDetail.user_id'=>$id));				
				$this->PlayerVideo->deleteAll(array('PlayerVideo.user_id'=>$id));
				$this->User->delete($id);
				$this->Session->setFlash(__('Players Successfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'player_profile',
						'action' => 'index'
						));
	}
	function get_nationality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$countries=$this->Country->find('all');
			$data="";
			foreach($countries as $country){
				if($id!="no"){
					if($country['Country']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$country['Country']['id'].'" id="'.$country['Country']['id'].'">'.$country['Country']['country_name'].'</option>';
			}
			echo $data;
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}*/
	}
	function get_speciality(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$coach_specialities=$this->CoachSpeciality->find('all');
			$data="";
			foreach($coach_specialities as $coach_speciality){
				if($id!="no"){
					if($coach_speciality['CoachSpeciality']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$coach_speciality['CoachSpeciality']['id'].'" id="'.$coach_speciality['CoachSpeciality']['id'].'">'.$coach_speciality['CoachSpeciality']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}
	}
	function get_certificate(){
		$this->layout="ajax";
		$this->render(false);		
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$certificates=$this->Certificate->find('all',array('conditions'=>array('Certificate.status'=>'A')));
			//pr($certificates);
			$data="";
			foreach($certificates as $certificate){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id){
						if($certificate['Certificate']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$certificate['Certificate']['id'].'" id="'.$certificate['Certificate']['id'].'">'.$certificate['Certificate']['name'].'</option>';
			}
			echo '<label for="coach_certificate">Certificate</label><select multiple id="coach_certificate" style="width:100%" data-placeholder="Select Certificate" class="required form-control valid select2" name="certificate_id[]">'.$data.'</select>';
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_profile',
							'action' => 'index'
							));
		}
	}
	/************************** Load player video   **************************************/
	function load_video(){
		$this->layout="ajax";
		if(!empty($this->request->data['link']))
		{
			if($this->request->data)
			{
				//============= Youtube Information ================
				$video_url = $this->request->data['link'];
				$url = 'http://www.youtube.com/oembed?format=json&url='.$video_url;
				if($url===false) 
				{					
					$video_iframe = 'no iframe';
					$this->set('video',$video_iframe);
				}else{					
					$content_value=file_get_contents($url);
					$youtube_data = json_decode($content_value, true);
					$video_iframe = $youtube_data['html'];
					$this->set('video',$video_iframe);
				}
			}
		}
	}
	//======================= Get All Caoch  ==================================================
	function get_allcoach(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$allcoachArray=$this->User->find('all',array('conditions'=>array('User.type'=>'C','User.status'=>'A',)));
			$data="";
			foreach($allcoachArray as $allcoach){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id){
						if($allcoach['User']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$allcoach['User']['id'].'" id="'.$allcoach['User']['id'].'">'.$allcoach['User']['first_name'].' '.$allcoach['User']['last_name'].'</option>';
			}
			echo '<label for="coach_course">Coach</label><select multiple id="coach_course" style="width:100%" data-placeholder="Select Coach" class="required form-control valid select2" name="coach_id[]">'.$data.'</select>';
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach',
							'action' => 'index'
							));
		}
	}
	//========================== Add Edit Coach Rating   ====================================================
	public function  add_rating123($id="")
	{
		$rating_id=$this->request->params['pass'][1];		 
		$all_rating_list=$this->AddCoachRating->find('all',array('conditions'=>array('AddCoachRating.coach_id'=>$id))); 
		if(count($all_rating_list)>0)
		{			 
			//$all_rating_list=$this->AddCoachRating->find('first',array('conditions'=>array('AddCoachRating.coach_id'=>$id))); 
			$this->set('all_rating_list', $all_rating_list);			
		}else
		{     $all_rating_list=array();
		  $this->set('all_rating_list', $all_rating_list);
		}
		$all_coach_category_list=$this->LevelCategory->find('list');		
		$AgeGroup_list=$this->AgeGroup->find('list');		
		$UpperLowerLimit_list=$this->UpperLowerLimit->find('all');	
		foreach($UpperLowerLimit_list as $UpperLowerLimit_lists)
		{
			$UpperLowerLimit_list_val[$UpperLowerLimit_lists['UpperLowerLimit']['id']]=$UpperLowerLimit_lists['UpperLowerLimit']['lower_limit'].'-'.$UpperLowerLimit_lists['UpperLowerLimit']['upper_limit'];			 
		}
		$this->set('all_coach_category_list', $all_coach_category_list);
		$this->set('AgeGroup_list',$AgeGroup_list);
		$this->set('UpperLowerLimit_list',$UpperLowerLimit_list_val);		
		//=============check id exits rating id===============================================		
		$add_rating_arr=array();
		if($this->request->data)
		{
			if(count($all_rating_list)>0)
			{
				$this->AddCoachRating->deleteAll(array('AddCoachRating.coach_id'=>$id));
			}				
			$k=1;
			for($z=1;$z<=count($AgeGroup_list);$z++)
			{
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{	$this->AddCoachRating->create();
					$add_rating_arr['AddCoachRating']['coach_id']=$id;				
					//$add_rating_arr['AddCoachRating']['coach_cat_id']=$this->request->data['coach_category_'.$k];
					$add_rating_arr['AddCoachRating']['coach_cat_id']=$this->request->data['coach_category_hidden_'.$k];
					$add_rating_arr['AddCoachRating']['rating_limit_id']=$this->request->data['upper_lower_limit_'.$z];
					$add_rating_arr['AddCoachRating']['rating']=$this->request->data['rating_val_'.$k];
					$add_rating_arr['AddCoachRating']['age_group_id']=$this->request->data['age_group_'.$z];
					$this->AddCoachRating->save($add_rating_arr);					
					$k++;
				}
			}
			if(count($all_rating_list)>0)
			{
					$this->Session->setFlash(__('Successfully Update Rating...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'coach_profile',
								'action' => '/index'
								));
			}else{								
			$this->Session->setFlash(__('Coaches Successfully Add Rating...','default',array('class'=>'alert alert-success alert-dismissable')));
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_profile',
						'action' => '/index'
						));
			}						
		}
	}
	public function bookedCourse()
	{
		$this->check_Permission('Booked_Course','view');
	}
	public function player_order_course(){
		$this->layout="ajax";
		$this->render(false);
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'BookCourseByPlayer.id',
			1 =>'User.first_name',
			2 =>'Course.course_title',
			3 =>'CourseDate.course_date',
			4 =>'BookCourseByPlayer.status',
			5 =>'BookCourseByPlayer.vpc_TransactionNo',
			6 =>'BookCourseByPlayer.payment',
		);
		$totalData = $this->BookCourseByPlayer->find('count'); 
		$totalFiltered = $totalData; 
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( User.first_name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR BookCourseByPlayer.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR CourseDate.created LIKE '".$requestData['search']['value']."%' )";
		}
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$BookCourseByPlayer = $this->BookCourseByPlayer->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($BookCourseByPlayer);
		}
		$data = array();
		foreach($BookCourseByPlayer as $row ) {  // preparing an array
			$nestedData=array();
			if($row['BookCourseByPlayer']["book_status"]==1)
			{
				$status='Confirmed';
			}else
			{
				$status='Pending';
			}
			
			if($row['BookCourseByPlayer']["payment_type"]==1)
			{
				$payment='Commonwealth';
			}else
			{
				$payment='Personal';
			}
			if($this->Session->read('Auth.User.type')=='A')
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['BookCourseByPlayer']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$row['User']["first_name"].' '.$row['User']["last_name"].'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$row['Course']["course_title"].'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$row['CourseDate']["course_date"].'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$row['BookCourseByPlayer']["date"].'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$status.'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$row['BookCourseByPlayer']["vpc_TransactionNo"].'</div>';
			$nestedData[] = '<div id="name_'.$row['BookCourseByPlayer']["id"].'">'.$payment.'</div>';
			$data[] = $nestedData;
		}
		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
		echo json_encode($json_data);  // send data as json format
	}
	//================certificate Add======================================//
	 function  assign_certificate($id="")
	 {
//pr($this->request->data);die;
		$player_id=$this->request->params['pass'][0];		 		
		//=========================select booked course By player id===========================		
		 $all_course=$this->BookCourseByPlayer->find('all',array('conditions'=>array('BookCourseByPlayer.player_id'=>$player_id)));		
	      $all_course_list=array();
		 if(!empty($all_course))
		 {
			 foreach($all_course as $key=>$all_courses)
			 {
				$all_course_list[$all_courses['Course']['id']]=$all_courses['Course']['course_title']; 				 				 
			 }
		 }
		 $this->set('all_course_list', $all_course_list);
		//=====================================================================================
		$all_coach_category_list=$this->LevelCategory->find('list');		
		$AgeGroup_list=$this->AgeGroup->find('list');		
		$this->set('all_coach_category_list', $all_coach_category_list);
		$this->set('AgeGroup_list',$AgeGroup_list);
		//=============check id exits rating id===============================================		
		$add_rating_arr=array();
		if($this->request->data)
		{
			$achive_certificates['AchiveCertificate']['course_id']=$this->request->data['course'];
			$achive_certificates['AchiveCertificate']['position_id']=$this->request->data['position'];
			$achive_certificates['AchiveCertificate']['category_id']=$this->request->data['coach_category'];
			$achive_certificates['AchiveCertificate']['group_id']=$this->request->data['age_group_id'];
			$achive_certificates['AchiveCertificate']['certificate_id']=$this->request->data['certificate_id'];
			$achive_certificates['AchiveCertificate']['achivement_date']=$this->request->data['achivement_date'];
			$achive_certificates['AchiveCertificate']['status']=$this->request->data['status'];
			$achive_certificates['AchiveCertificate']['player_id']=$player_id;			
		    $this->AchiveCertificate->save($achive_certificates);
					$this->Session->setFlash(__('Successfully assign certificate to Player ...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'player_profile',
								'action' => '/index'
								));
		}
	}
  public function get_age_group()
  {
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			  $this->AgeGroup->bindModel(array(
			   'belongsTo' => array(
								'CourseAgeGroup'=>array(
									 'className'=>'CourseAgeGroup',
									 'conditions'   => 'AgeGroup.id=CourseAgeGroup.age_group_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );
			$age_groups=$this->AgeGroup->find('all',array('conditions'=>array('CourseAgeGroup.course_id'=>$this->request->data['id'])));
			$data='<option value="" id="">--Select Your Age Group--</option>';
			foreach($age_groups as $age_group){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id)
					{
						if($age_group['AgeGroup']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$age_group['AgeGroup']['id'].'" id="'.$age_group['AgeGroup']['id'].'">'.$age_group['AgeGroup']['title'].'</option>';
			}			
			//echo $data;
			echo '<label for="age_group">Select Age Group</label><select id="age_group" style="width:100%" data-placeholder="Select Age Group" class="required form-control valid select2" name="age_group_id">'.$data.'</select>';
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}*/
	}
	//======================Coach List==============================================//
	public function get_coach_by_course()
     {
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			  $id = $this->request->data['id'];	
              $this->User->unBindModel(array('hasOne' => array('Nationality','PlayerDetail','FamilyDetail','CoachDetail')));
			  $this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video')));
			  //$this->User->unBindModel(array('belongsTo' => array('CourseCoache')));
			  $this->User->bindModel(array(
			   'belongsTo' => array(
								'CourseCoache'=>array(
									 'className'=>'CourseCoache',
									 'conditions'   => 'User.id=CourseCoache.coach_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );
			$all_coach=$this->User->find('all',array('conditions'=>array('CourseCoache.course_id'=>$this->request->data['id'])));
			$data='<option value="" id="">--Select Your Coach--</option>';
			foreach($all_coach as $all_coachs){
				if($id!="no"){
					$ids = explode(',',$id);
					foreach($ids as $selected_id)
					{
						if($all_coachs['User']['id']==$selected_id){
							$status = "selected='selected'";
							break;
						}else{
							$status = "";
						}
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$all_coachs['User']['id'].'" id="'.$all_coachs['User']['id'].'">'.$all_coachs['User']['first_name'].' '.$all_coachs['User']['last_name'].'</option>';
			}			
			//echo $data;
			echo '<label for="age_group">Select Coach</label><select  id="age_group" style="width:100%" data-placeholder="Select Age Group" class="required form-control valid select2" name="age_group_id[]">'.$data.'</select>';
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}*/
	}
	//=================get position============================================//
	function get_club(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			//=============================================================
				$this->loadModel('Club');
				$club=$this->Club->find('all');
			//$PlayerPosition=$this->CourseToPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$this->request->data['id'])));	
			$data='<option value="" id="">--Select Your Club--</option>';	
			foreach($club as $clubs){
				if($id!="no"){
					if($clubs['Club']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}  
				$data.='<option '.$status.' value="'.$clubs['Club']['id'].'" name="certificate_id" id="'.$clubs['Club']['id'].'">'.$clubs['Club']['name'].'</option>';
			}
			echo $data;
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}*/
	}
	//=================get position============================================//
	function get_position(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
				$this->loadModel('PlayerPosition');
			//=======================================================
			 $this->CourseToPosition->bindModel(array(
			   'belongsTo' => array(
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'CourseToPosition.position_id=PlayerPosition.id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								)
				               )   
							)
		               );
				//$PlayerPosition=$this->CourseToPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$this->request->data['id']))); */
			//=============================================================
			$PlayerPosition=$this->CourseToPosition->find('all',array('conditions'=>array('CourseToPosition.course_id'=>$this->request->data['id'])));        
			$data='<option value="" id="">--Select Your Position--</option>';	
			foreach($PlayerPosition as $PlayerPositions){
				if($id!="no"){
					if($PlayerPositions['PlayerPosition']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}			  
				$data.='<option '.$status.' value="'.$PlayerPositions['PlayerPosition']['id'].'" name="certificate_id" id="'.$PlayerPositions['PlayerPosition']['id'].'">'.$PlayerPositions['PlayerPosition']['name'].'</option>';
			}
			echo $data;
		}
		/*else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'course',
							'action' => 'index'
							));
		}*/
	}
  //===================View Player certification=================================================//
	function view_certificate()
	{
	  $player_id=$this->request->params['pass'][0];	 
	  $this->AchiveCertificate->bindModel(array(
			   'belongsTo' => array(
								'Level'=>array(
									 'className'=>'Level',
									 'conditions'   => 'Level.id=AchiveCertificate.certificate_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'AgeGroup'=>array(
									 'className'=>'AgeGroup',
									 'conditions'   => 'AgeGroup.id=AchiveCertificate.group_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'PlayerPosition'=>array(
									 'className'=>'PlayerPosition',
									 'conditions'   => 'PlayerPosition.id=AchiveCertificate.position_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'CoachCategory'=>array(
									 'className'=>'CoachCategory',
									 'conditions'   => 'CoachCategory.id=AchiveCertificate.category_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'=>''
								),
								'Course'=>array(
								'className'=>'Course',
									 'conditions'   => '',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'course_id',
									 'fields'=>'Course.course_title'
								)
				            )   
							)
		               );	 
	   $all_achive_certificate= $this->AchiveCertificate->find('all',array('conditions'=>array('AchiveCertificate.player_id'=>$player_id)));
	   $this->set('all_achive_certificate', $all_achive_certificate);
	}
	function get_upper_lower_limit()
	{
		$this->layout="ajax";
		$this->render(false);
		$this->loadModel('CapabilityMatrixAge');
        $position_id= $this->request->data['position_id']; 
        $group_id= $this->request->data['group_id'];
		$all_upper_limit_lower_limit=$this->CapabilityMatrixAge->find('first',array('conditions'=>array('CapabilityMatrixAge.age_group_id'=>$group_id,'CapabilityMatrixAge.player_position_id'=>$position_id)));
		if(!empty($all_upper_limit_lower_limit))
		{
			$lower_limit=$all_upper_limit_lower_limit['CapabilityMatrixAge']['lower_limit'];
			$upper_limit=$all_upper_limit_lower_limit['CapabilityMatrixAge']['upper_limit'];
		}else
		{
			$lower_limit="";
			$upper_limit="";
		}
		echo $lower_limit.'|'.$upper_limit;
	}
	public function make_propuler_player()
	{
		$this->layout="ajax";
		$this->render(false);
		$palyer_id= $this->request->data['id'];
		$status= $this->request->data['status'];
		$this->PlayerDetail->updateAll(array('PlayerDetail.propuler_player'=>$status),array('PlayerDetail.user_id'=>$palyer_id));
	}
	public function all_course_book()
	{	
	    $player_id=  $this->request->data['payler_id'];
	    $cat_id=  $this->request->data['id'];
	      $all_course_list=array();
	     $all_course=$this->BookCourseByPlayer->find('all',array('conditions'=>array('BookCourseByPlayer.player_id'=>$player_id,'BookCourseByPlayer.cat_id'=>$cat_id)));		
		   $data='<option value="" id="">--Select Your Course--</option>';
		 if(!empty($all_course))
		 {
			 foreach($all_course as $key=>$all_courses)
			 {
				$all_course_list[$all_courses['Course']['id']]=$all_courses['Course']['course_title']; 				 				 
			 }
			 foreach($all_course_list as $key=>$all_course_lists)
			 {
				 $data.='<option  value="'.$key.'" id="'.$key.'">'.$all_course_lists.'</option>';
			 }
        }
	echo $data;
	exit;
	}

//========================for chanchal==========================================//

 function  assign_certificate_without_course()
	 {
//pr($this->request->data);die;
		$player_id=$this->request->params['pass'][0];		 		
		//=========================select booked course By player id===========================		
		 $all_course=$this->BookCourseByPlayer->find('all',array('conditions'=>array('BookCourseByPlayer.player_id'=>$player_id)));		
	      $all_course_list=array();
		 if(!empty($all_course))
		 {
			 foreach($all_course as $key=>$all_courses)
			 {
				$all_course_list[$all_courses['Course']['id']]=$all_courses['Course']['course_title']; 				 				 
			 }
		 }
		 $this->set('all_course_list', $all_course_list);
		//=====================================================================================
		$all_coach_category_list=$this->LevelCategory->find('list');			
		$AgeGroup_list=$this->AgeGroup->find('list');			
		$this->set('all_coach_category_list', $all_coach_category_list);
		$this->set('AgeGroup_list',$AgeGroup_list);
		$playerPosition=$this->PlayerPosition->find('list',array('fields'=>array('PlayerPosition.id','PlayerPosition.name')));
		$this->set('playerPosition',$playerPosition);
		$all_achive_certificate= $this->Level->find('list',array('fields'=>array('Level.level_number')));
	   $this->set('all_achive_certificate', $all_achive_certificate);
		//=============check id exits rating id===============================================		
		$add_rating_arr=array();
		if($this->request->data)
		{
			//$achive_certificates['AchiveCertificate']['course_id']=$this->request->data['course'];
			$achive_certificates['AchiveCertificate']['position_id']=$this->request->data['position'];
			$achive_certificates['AchiveCertificate']['category_id']=$this->request->data['coach_category'];
			$achive_certificates['AchiveCertificate']['group_id']=$this->request->data['age_group_id'];
			$achive_certificates['AchiveCertificate']['certificate_id']=$this->request->data['certificate_id'];
			$achive_certificates['AchiveCertificate']['achivement_date']=$this->request->data['achivement_date'];
			$achive_certificates['AchiveCertificate']['status']=$this->request->data['status'];
			$achive_certificates['AchiveCertificate']['player_id']=$player_id;			
		    $this->AchiveCertificate->save($achive_certificates);
					$this->Session->setFlash(__('Successfully assign certificate to Player ...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
								'plugin' => 'admin',
								'controller' => 'player_profile',
								'action' => '/index'
								));
		}
	}
	
	public function download_players_xls1()
	{
		
		$flag = false;
		$data=array();
		$this->loadModel('Course');
		
		/*$this->PlayerDetail->bindModel(array(
			   'belongsTo' => array(
								'Country'=>array(
									 'className'=>'Country',
									 'conditions'   => 'Country.id=PlayerDetail.nationality',
									 'order'        => '',
									 'foreignKey'=>'',
									 'fields'=>''
								    ), 
								 'BookCourseByPlayer'=>array(
								 'className'=>'BookCourseByPlayer',
								 'conditions'  => 'BookCourseByPlayer.player_id=PlayerDetail.user_id',
								 'order'       => '',
								 'foreignKey'=>'',
								 'fields'=>''
								),
								'FamilyPlayerRelationship'=>array(
								 'className'=>'FamilyPlayerRelationship',
								 'conditions'   => 'FamilyPlayerRelationship.player_id=PlayerDetail.user_id',
								 'order'        => '',
								 'foreignKey'=>'',
								 'fields'=>''
								)	    
			  )
		    ));*/
			
			$this->User->bindModel(array(
			   'hasMany' => array(
								'BookCourseByPlayer'=>array(
									 'className'=>'BookCourseByPlayer',								
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    ),
									'FamilyPlayerRelationship'=>array(
									 'className'=>'FamilyPlayerRelationship',									
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    )
								)
							)
						);
						
			
			
			/*$this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video','PlayerVideo')));
			$this->User->unBindModel(array('hasOne' => array('CoachDetail','PlayerDetail','FamilyDetail',)));
			
			
			$this->User->bindModel(array(
			   'hasMany' => array(
								'BookCourseByPlayer'=>array(
									 'className'=>'BookCourseByPlayer',								
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    ),
									'FamilyPlayerRelationship'=>array(
									 'className'=>'FamilyPlayerRelationship',									
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    )
								)
							)
						);
			
			$this->PlayerDetail->bindModel(array(
			   'belongsTo' => array(
								'Country'=>array(
									 'className'=>'Country',
									// 'conditions'   => 'Country.id=PlayerDetail.nationality',
									 'order'        => '',
									 'foreignKey'=>'nationality',
									 'fields'=>''
								    )
								)
							)
						);*/
			
			
		//view_content = $this->PlayerDetail->find('all',array('conditions'=>array('PlayerDetail.user_id'=>98)));
		//$view_content = $this->PlayerDetail->find('all');
		
		    $this->User->unBindModel(array('belongsTo'=>array('CoachCertificate','Video','PlayerVideo')));
			$this->User->unBindModel(array('hasOne' => array('CoachDetail','FamilyDetail',)));
			
		    $view_content = $this->User('all',array('conditions'=>array('User.type'=>'P','User.status'=>'A')));
		
		pr($view_content);
		exit;
		   //echo 'sadsa';
			//exit;
			//,'recursive'=>2
        $all_course_title= "";		
		foreach ((array) $view_content as $key=>$value)
		{
            //$nationality_id=$value['PlayerDetail']['nationality'];	country_name
		    //$course_title_arr=$this->Country->find('all',array('conditions'=>array('Country.id'=>$nationality_id)));
			
			
			$cr_id=$value['BookCourseByPlayer']['course_id'];
			$relatn_id=$value['FamilyPlayerRelationship']['relation_id'];
			$i=0;
             
			$course_title_arr=$this->Course->find('all',array('conditions'=>array('Course.id'=>$cr_id)));			
			$family_assoc_arr=$this->PlayerRelationship->find('all',array('conditions'=>array('PlayerRelationship.id'=>$relatn_id)));
			
			$all_course_title="";
			if(!empty($course_title_arr)){				
				foreach((array) $course_title_arr as $val){					
					if(count($course_title_arr)>1){				     
				     	$all_course_title .=$val['Course']['course_title'].',';
					}
					else{					
						$all_course_title = $val['Course']['course_title'];
					}
			    }
				
			}
			
			
			$data[$key]['Player First Name']=$view_content[$key]['User']['first_name'];	
			$data[$key]['Player Last Name']=$view_content[$key]['User']['last_name'];	
			$data[$key]['Date of birth']=$view_content[$key]['PlayerDetail']['dob'];
			$data[$key]['E-mail']=$view_content[$key]['User']['email_address'];
			$data[$key]['Primary Position']=$view_content[$key]['PrimaryPosition']['name'];
			$data[$key]['Playing Club']=$view_content[$key]['Club']['name'];
			$data[$key]['Nationality']=$view_content[$key]['Country']['country_name'];
			$data[$key]['Courses Booked']= $all_course_title;
		    
			if(!empty($family_assoc_arr))
			{
				
				foreach((array)$family_assoc_arr as $key=>$val){
					
					$i++;
					$data[$key]['Family Associaton'.' '.$i]= $val['PlayerRelationship']['relationship'];
			    }
				
			}
            
                
	        } 
		

     /* function cleanData(&$str)
	  {
		$str = preg_replace("/\t/", "\\t", $str);
		$str = preg_replace("/\r?\n/", "\\n", $str);
		if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
	  }*/
	  
	    // file name for download
	  $filename = "player" . date('Ymd') . ".xls";
	 
	  header("Content-Disposition: attachment; filename=\"$filename\"");
	  header("Content-Type: application/vnd.ms-excel");

	  foreach((array) $data as $row) {
		
		if(!$flag) {
		  echo implode("\t", array_keys($row)) . "\n";
		  $flag = true;
		}
		
		 array_walk($row,function(&$row)
	     {
		$row = preg_replace("/\t/", "\\t", $row);
		$row= preg_replace("/\r?\n/", "\\n", $row);
		if(strstr($row, '"')) $row = '"' . str_replace('"', '""', $row) . '"';
		
	     });
	     
		//array_walk($row, 'cleanData');
		echo implode("\t", array_values($row)) . "\n";
	  }
	  	exit;  
	}
	
	//==============All Palyer Xls file download========================================//
	public function download_players_xls()
	{
		
		$flag = false;
		$data=array();
		$this->loadModel('Course');
		
		
			$this->User->bindModel(array(
			   'hasMany' => array(
								'BookCourseByPlayer'=>array(
									 'className'=>'BookCourseByPlayer',								
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    ),
									'FamilyPlayerRelationship'=>array(
									 'className'=>'FamilyPlayerRelationship',									
									 'order'        => '',
									 'foreignKey'=>'player_id',
									 'fields'=>''
								    )
								)
							)
						);
			
		//==============All Payer Data=================================================//	
		$this->User->unBindModel(array('hasOne' => array('CoachDetail','FamilyDetail')));
		$this->User->unBindModel(array('hasMany' => array('CoachCertificate','Video','PlayerVideo')));		
		$view_content = $this->User->find('all',array('conditions'=>array('User.type'=>'P','User.status'=>'A')));
		//=====================End ====================================================//		  
     // pr($view_content);
	  // exit;
		$relation_arr=array();
		foreach ((array) $view_content as $key=>$value)
		{	
		
		//=============Nationality=================================================================//
		    $nationality_id=$value['PlayerDetail']['nationality'];
		    $nationality_arr=$this->Country->find('first',array('conditions'=>array('Country.id'=>$nationality_id),'fields'=>array('Country.country_name')));
			
		//==============================pimary Player Position======================================================//

            $primary_position_id=$value['PlayerDetail']['primary_position_id'];			
		    $primary_position_arr=$this->PlayerPosition->find('first',array('conditions'=>array('PlayerPosition.id'=>$primary_position_id),'fields'=>array('PlayerPosition.name')));
		
		//==============================Club Name======================================================//
            $club_id=$value['PlayerDetail']['club_id'];			
		    $club_arr=$this->Club->find('first',array('conditions'=>array('Club.id'=>$club_id),'fields'=>array('Club.name')));	

		 //=================================================================================//		
		  $course_book_arr_by_player=  $value['BookCourseByPlayer'];
		//====================get course title ============================================//
	        $c=0;
			$all_course_title="";			
			if(!empty($course_book_arr_by_player))
			{				
				foreach((array) $course_book_arr_by_player as $course_book_arr_by_players)
				{
				  //==================get course id=================//	
				  $course_id=$course_book_arr_by_players['course_id'];
				  //================================================//
				  $this->Course->unBindModel(array('hasMany' => array('CourseCoache')));
				  $course_title_arr=$this->Course->find('first',array('conditions'=>array('Course.id'=>$course_id),'fields'=>array('Course.id','Course.id','Course.course_title')));
				
				  if($c>0){	
				  
				     	$all_course_title .=','. $course_title_arr['Course']['course_title'];
					}
					else{					
						$all_course_title =   $course_title_arr['Course']['course_title'];
					}
					
                  $c++;
			    }				
			}
			
			
			if(isset($primary_position_arr['PlayerPosition']['name']))
			{
				$primary_position_arr_name=$primary_position_arr['PlayerPosition']['name'];				
			}else
			{
				$primary_position_arr_name="";
			}	
			
			if(isset($club_arr['Club']['name']))
			{
				$club_name=$club_arr['Club']['name'];
				
			}else
			{
				$club_name="";
			}	
			
			
			if(isset($nationality_arr['Country']['country_name']))
			{
				$nationality_name=$nationality_arr['Country']['country_name'];
				
			}else
			{
				$nationality_name="";
			}	
			
			
			$data[$key]['Player First Name']=$view_content[$key]['User']['first_name'];	
			$data[$key]['Player Last Name']=$view_content[$key]['User']['last_name'];	
			$data[$key]['Date of birth']=$view_content[$key]['PlayerDetail']['dob'];
			$data[$key]['E-mail']=$view_content[$key]['User']['email_address'];
			$data[$key]['Primary Position']=$primary_position_arr_name;
			$data[$key]['Playing Club']= $club_name;
			$data[$key]['Nationality']= $nationality_name;
			$data[$key]['Courses Booked']= $all_course_title;
		    
			//=================All relation player========================================//			
			$FamilyPlayerRelationship_arr=  $value['FamilyPlayerRelationship'];			
			$r=0;
			$family_assoc_arr="";			
			if(!empty($FamilyPlayerRelationship_arr))
			{				
				foreach((array) $FamilyPlayerRelationship_arr as $key1=>$FamilyPlayerRelationship_arrs)
				{
					//=====================get relation==============================================//
					
					$relatn_id=$FamilyPlayerRelationship_arrs['relation_id'];					
					$relation_user_id=$FamilyPlayerRelationship_arrs['member_id'];					
					$family_assoc_arr=$this->PlayerRelationship->find('first',array('conditions'=>array('PlayerRelationship.id'=>$relatn_id),'fields'=>array('PlayerRelationship.id','PlayerRelationship.relationship')));					
					//=================get relation user name========================================//					
					$relation_user_detals = $this->User->find('first',array('conditions'=>array('User.id'=>$relation_user_id,'User.type'=>'F','User.status'=>'A'),'fields'=>array('User.id','User.first_name','User.last_name'),'recursive'=>-1));
					
					if(isset($relation_user_detals['User']['first_name']))
					$name= $relation_user_detals['User']['first_name'].' '.$relation_user_detals['User']['last_name'];
					else
					$name="";	
					$relation_arr[$key]['family_associaton'.$r] = $name.'-'.$family_assoc_arr['PlayerRelationship']['relationship'];
			 $r++;
			 
			    }				
			  }
			  
			 

            if(isset($relation_arr[$key]['family_associaton0']))
			{
			    $data[$key]['Family Associaton1']=$relation_arr[$key]['family_associaton0'];
			 }else
			 {				
				$data[$key]['Family Associaton1']='';
			 }
			 
			 if(isset($relation_arr[$key]['family_associaton1']))
			{
			    $data[$key]['Family Associaton2']=$relation_arr[$key]['family_associaton1'];
			 }else
			 {				
				$data[$key]['Family Associaton2']='';
			 }
			 
			  if(isset($relation_arr[$key]['family_associaton2']))
			  {
			    $data[$key]['Family Associaton3']=$relation_arr[$key]['family_associaton2'];
			 }else
			 {				
				$data[$key]['Family Associaton3']='';
			 }
		
            
			 
	        } 
			
		
	  
	    // file name for download
	  $filename = "player" . date('Ymd') . ".xls";
	 
	  header("Content-Disposition: attachment; filename=\"$filename\"");
	  header("Content-Type: application/vnd.ms-excel");

	  foreach((array) $data as $row) {
		
		if(!$flag) {
		  echo implode("\t", array_keys($row)) . "\n";
		  $flag = true;
		}
		
		 array_walk($row,function(&$row)
	     {
		$row = preg_replace("/\t/", "\\t", $row);
		$row= preg_replace("/\r?\n/", "\\n", $row);
		if(strstr($row, '"')) $row = '"' . str_replace('"', '""', $row) . '"';
		
	     });
	     
		//array_walk($row, 'cleanData');
		echo implode("\t", array_values($row)) . "\n";
	  }
	  	exit;  
	}
		
}
