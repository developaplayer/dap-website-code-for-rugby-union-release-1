<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class OrdersController extends AdminAppController {	
	var $name = 'Orders';
	var $uses = array('Order','Admin.OrderProduct','Admin.Product');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$view_content = $this->Order->find('all',array('conditions'=>array('Order.payment_status'=>1,'Order.comweb_status'=>'Approved'),'order' => array('Order.id desc')));			
		$this->set(compact('view_content'));
		$this->check_Permission('Order_Management','view');	
	}	
	
	public function delete_page($id){
		$this->check_Permission('Order_Management','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);	
		if($this->Order->delete($id)){			
			$this->Session->setFlash(__('Order Details Succesfully Deleted..'));
			$this->redirect(array("action" => "index"));
		}
	}
	
	public function orders_multiple_delete()
	{		
		$this->check_Permission('Order_Management','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		
		foreach($this->request->data['id'] as $id){
			$this->Order->delete($id);
		}
		$this->Session->setFlash("All Order Details Successfully Deleted..");
		$this->redirect(array("action" => "index"));
	}
	
	public function order_details($order_id)
	{	
		$this->Order->bindModel(array(
				   'belongsTo' => array(
									'User'=>array(
										 'className'=>'User',
										 'conditions'   => '',
										 'order'        => '',
										 'dependent'    =>  false,
										 'foreignKey'=>'customer_id',
										 'fields'=>array('User.mobile_number','User.first_name','User.last_name','User.email_address')
									)
						   )
					));
        $all_orders_detail=$this->Order->find('first',array('conditions'=>array('Order.id'=>$order_id)));
		//pr($all_orders_detail);
		//exit;
		    	
		$this->OrderProduct->bindModel(array(			   
								'hasOne' => array( 'Product'=>array(
									 'className'=>'Product',
									 'conditions'   => 'OrderProduct.product_id=Product.id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								  
								  'ProductImage'=>array(
									 'className'=>'ProductImage',
									 'conditions'   => 'ProductImage.product_id=OrderProduct.product_id And ProductImage.deflt_value=1',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          ),
								   'Size'=>array(
									 'className'=>'Size',
									 'conditions'   => 'Size.id=OrderProduct.size_id',
									 'order'        => '',
									 'dependent'    =>  false,
									 'foreignKey'=>'',
									 'fields'       =>''
		                          )
				               ) 
                           )							   
		               );
		
		$all_oder_detail_product=$this->OrderProduct->find('all',array('conditions'=>array('OrderProduct.order_id'=>$order_id)));
		//pr($all_oder_detail_product);
        //exit;
		
		$this->set('all_orders_detail',$all_orders_detail);
		$this->set('all_oder_detail_product',$all_oder_detail_product);
		
	} 	


	public function status_update(){
		$this->check_Permission('Order_Management','status');
		$this->layout='ajax';	
		$this->autoRender =false;
		$status_id = $this->request->data['status_id'];
		$order_id = $this->request->data['order_id'];
		$this->Order->id=$order_id;
		if($this->Order->saveField("order_status",$status_id)){
			echo 1;
		}else{
			echo 2;
		}
	}
	public function delete_order($id){
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);
		if($this->Order->delete($id)){	
				$result_logs = $this->OrderProduct->find('count', array('conditions'=>array('OrderProduct.order_id'=>$id)));
				if($result_logs > 0){
					$this->OrderProduct->deleteAll(array('OrderProduct.order_id' => $id));
				} 
			$this->Session->setFlash(__('Order has been Succesfully Deleted..'));
			$this->redirect(array("action" => "index"));
		}
		
	}
		
}