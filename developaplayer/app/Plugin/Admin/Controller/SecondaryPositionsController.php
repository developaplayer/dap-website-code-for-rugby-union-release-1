<?php
App::uses('AdminAppController', 'Admin.Controller');
class SecondaryPositionsController extends AdminAppController {
	var $uses=array('Admin.PlayerPosition');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index(){
	}
	
	public function secondary_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'PlayerPosition.id',
			1 =>'PlayerPosition.name',
			2 =>'PlayerPosition.status',
			2 =>'PlayerPosition.created',
		);
		$totalData = $this->PlayerPosition->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "PlayerPosition.type = 'S'";
		if( !empty($requestData['search']['value']) ) {
			$sql.="AND ( PlayerPosition.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR PlayerPosition.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR PlayerPosition.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$positions = $this->PlayerPosition->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
												
		/*$log = $this->PlayerPosition->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($positions);
		die;*/
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($positions);
		}
		$data = array();
		foreach( $positions as $row ) {  // preparing an array
			$nestedData=array();

			$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['PlayerPosition']["id"].'" type="checkbox" />';
			$nestedData[] = '<div id="position_name_'.$row['PlayerPosition']["id"].'">'.$row['PlayerPosition']["name"].'</div>';
			
			if($row['PlayerPosition']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['PlayerPosition']["id"].'">'.$status.'</div>';
			
			$nestedData[] = date('d-M-Y',strtotime($row['PlayerPosition']["created"]));
			
			$nestedData[] = '<a href="javascript:change_status('.$row['PlayerPosition']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['PlayerPosition']['id'].'"><img id="status_img_'.$row['PlayerPosition']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="javascript:edit_data('.$row['PlayerPosition']['id'].',\''.$row['PlayerPosition']["name"].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['PlayerPosition']['name'].'?\')" href="'.$this->webroot.'admin/secondary_positions/delete_position/'.$row['PlayerPosition']['id'].'" title="Delete" id="delete_'.$row['PlayerPosition']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_position(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			//print_r($this->request->data['PlayerPosition']['name']);
			if(!empty($this->request->data['position_name'])){
				$check_position = $this->PlayerPosition->find('first',array('conditions'=>array('PlayerPosition.name'=>$this->request->data['position_name'])));
				if(empty($check_position)){
					$data['PlayerPosition']['name'] = $this->request->data['position_name'];
					$data['PlayerPosition']['type'] = 'S';
					if($this->PlayerPosition->save($data)){
						$inserted_id = $this->PlayerPosition->getLastInsertId();
						
						echo '<td><input class="checkbox_del" name="id[]" value="'.$inserted_id.'" type="checkbox" /></td><td><div id="position_name_'.$inserted_id.'">'.$data['PlayerPosition']['name'].'</div></td><td><div id="status_of_'.$inserted_id.'">Active</div></td><td>'.date('d-M-Y').'</td>';
						
						echo '<td><a href="javascript:change_status('.$inserted_id.',\'I\')" title="Inactive" id="status_'.$inserted_id.'"><img id="status_img_'.$inserted_id.'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/unlock.png" /></a>&nbsp<a href="javascript:edit_data('.$inserted_id.',\''.$data['PlayerPosition']['name'].'\')"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$data['PlayerPosition']['name'].'?\')" href="'.$this->webroot.'admin/secondary_positions/delete_position/'.$inserted_id.'" title="Delete" id="delete_'.$inserted_id.'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a></td>';
						
					}else{
						echo "save_error";
					}
				}else{
					echo "exist";
				}
			}else{
				echo "empty_error";
			}
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'SecondaryPositions',
						'action' => 'index'
			));
		}
	}
	
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['PlayerPosition']['status'] = $this->request->data['status'];
			$this->PlayerPosition->id = $this->request->data['id'];
			if($this->PlayerPosition->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_position($id=""){
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->PlayerPosition->delete($id)){
				$this->Session->setFlash(__('Secondary Position Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Secondary Position...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'SecondaryPositions',
						'action' => 'index'
						));
		}
	}
	
	function update_position(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$check_position = $this->PlayerPosition->find('first',array('conditions'=>array('PlayerPosition.name'=>$this->request->data['position'])));
			if(!empty($check_position)){
				echo "exist";
			}else{
				$data['PlayerPosition']['name'] = $this->request->data['position'];
				$this->PlayerPosition->id = $this->request->data['id'];
				if($this->PlayerPosition->save($data)){
					echo "success";
				}else{
					echo "update_error";
				}
			}
		}
	}
	
	function delete_position_multiple(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			foreach($this->request->data['id'] as $id){
				$this->PlayerPosition->create();
				$this->PlayerPosition->delete($id);
				$this->Session->setFlash(__('Secondary Positions Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'SecondaryPositions',
						'action' => 'index'
						));
	}
}