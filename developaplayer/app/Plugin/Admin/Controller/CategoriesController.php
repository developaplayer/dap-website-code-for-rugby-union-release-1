<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class CategoriesController extends AdminAppController {
	var $uses=array('Admin.ProductCategory','Admin.ProductToCategory');
	public $helpers = array('Html', 'Form');
 
	 public function beforeFilter() {
        $this->Auth->allow(array('login'));
	   parent::beforeFilter();
       
    }
	private function reArrayFiles(&$file_post) {

			$file_ary = array();
			$file_count = count($file_post['name']);
			$file_keys = array_keys($file_post);

			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				}
			}

			return $file_ary;
		}
	
	public function index(){
		$this->check_Permission('Product_Category','view');
				
	}
	
	public function all_category()
	{
		$this->check_Permission('Product_Category','view');
		$categories=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>0),'order'=>'ProductCategory.id DESC'));	
		$this->set('categories',$categories);
		$this->set('title_for_layout', 'Admin Panel: All Product Category');
	}
	
	public function category_details($id)
	{
		$this->check_Permission('Product_Category','edit');
		$id=base64_decode($id);
		if($this->request->is('post'))
		{			
			$this->Session->setFlash(_('ProductCategory Succesfully Updated'));
			/*print"<pre>";
			print_r($this->request->data);*/
			$ProductCategory_data=$this->request->data;
			$ProductCategory_data['name']=$this->request->data['first_name'];
			$ProductCategory_data['first_name']='';
			$this->ProductCategory->id=$id;
			if($this->ProductCategory->save($ProductCategory_data)){
				$this->Session->setFlash(_('ProductCategory Succesfully Updated'));
			}else{
				$this->Session->setFlash(_('ProductCategory Update Error'));
			}
			$this->redirect(array("action" => "all_category"));
		}
		
		$categories=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.id'=>$id)));
		$allcategories=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>'0')));
		if($categories[0]['ProductCategory']['parent_id']!=0){
			$parent_name=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.id'=>$categories[0]['ProductCategory']['parent_id'])));
			$this->set('parent_name',$parent_name);
		}
		$this->set('allcategories',$allcategories);
		
		$this->set('ProductCategory',$categories);
		
	}
	
	public function add_category()
	{
		$this->check_Permission('Product_Category','add');
		if($this->request->is('post'))
		{
			//pr($this->request->data);
		//exit;
		
			if(isset($this->request->data)){
				$this->request->data['slug_url']=$this->request->data['name'];
			
			
				//$data_image = $this->reArrayFiles($_FILES['image']);
				
				/*foreach($data_image as $kk=>$image){
				  $foo = new Upload($image); 
				  $foo->allowed = array('image/jpg/png/gif/jpeg');
					if ($foo->uploaded) {
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/ProductCategory/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 391;
						   $foo->image_x               = 451;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/ProductCategory/thumb/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 645;
						   $foo->image_x               = 1366;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/ProductCategory/app/large/');
						   $foo->image_resize          = true;
						   $foo->image_y               = 645;
						   $foo->image_x               = 1366;
						   $foo->allowed = array('image/*');
						   $foo->Process('uploads/ProductCategory/app/thumb/');
						   
						  if ($foo->processed){
								$image_name= $foo->file_dst_name;
								$this->request->data['image']=$image_name;
								$this->Session->setFlash(_('ProductCategory Succesfully Added'));
						  }else{
							  $this->Session->setFlash(_('Invaled Image File!!'));
							  $this->redirect(
								array("action" => "add_banner"));
						  }
					}
				}*/
				
				
				$this->ProductCategory->save($this->request->data);
				$this->redirect(
		              array("action" => "all_category")
					);
			}
		}
		
		 $ProductCategory=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>'0')));
		$this->set('ProductCategory',$ProductCategory);
		$this->set('title_for_layout', 'Admin Panel: Add ProductCategory');
	}
	
	
	public function category_delete($id)
	{
		$this->check_Permission('Product_Category','delete');
		$this->autoRender= false;
		$this->layout="ajax";
		$id=base64_decode($id);
		$ProductCategory=$this->ProductCategory->findById($id);
		if($this->ProductCategory->delete($id)){
			@unlink('uploads/ProductCategory/'.$ProductCategory['ProductCategory']['image']);
			@unlink('uploads/ProductCategory/thumb/'.$ProductCategory['ProductCategory']['image']);
			@unlink('uploads/ProductCategory/app/thumb/'.$ProductCategory['ProductCategory']['image']);
			@unlink('uploads/ProductCategory/app/large/'.$ProductCategory['ProductCategory']['image']);
			$this->ProductCategory->deleteAll(array('ProductCategory.id'=>$id));
			$this->Session->setFlash(_('ProductCategory Succesfully Deleted'));
		}else{
			$this->Session->setFlash(_('Error in Deleting'));
		}
		$this->redirect(
		array("action" => "all_category"));
	}
	
	public function category_multiple_delete()
	{
		$this->check_Permission('Product_Category','delete');
		//deleting selected multiple unit types
		$this->autoRender= false;
		$this->layout="ajax";
		if(isset($this->request->data)){
		foreach($this->request->data['id'] as $id){
			//$id=$this->request->data['id'];
			$ProductCategory=$this->ProductCategory->findById($id);
			foreach($ProductCategory as $id){
				if($this->ProductCategory->delete($id['id'])){
					@unlink('uploads/ProductCategory/'.$id['id']);
					@unlink('uploads/ProductCategory/thumb/'.$id['id']);
					@unlink('uploads/ProductCategory/app/thumb/'.$id['id']);
					@unlink('uploads/ProductCategory/app/large/'.$id['id']);
					$this->ProductCategory->deleteAll(array('ProductCategory.id'=>$id));
					$this->Session->setFlash("Categories Successfully Deleted..");
				}else{
				$this->Session->setFlash("Error in Deleting..");
			}
			}
		}
		}else{
			$this->Session->setFlash("No Newsletters were Deleted..");
		}
		$this->redirect(
		array("action" => "all_category"));
	}
	
}