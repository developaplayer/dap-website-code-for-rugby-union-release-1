<?php
App::uses('AdminAppController', 'Admin.Controller');
App::import('Vendor', 'Upload', array('file' => 'classupload/src/class.upload.php'));
class ProductsController extends AdminAppController {
	var $uses=array('Admin.Product','Admin.ProductCategory','Admin.ProductImage','Admin.Size','Admin.ProductSizeQty');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
			$this->check_Permission('Product_Management','view');
	}
	
	//=========================Product List===========================================
	
	 public function product_data()
   {
		$this->layout="ajax";
		$this->render(false);		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'Product.id',
			1 =>'Product.description',
			2 =>'ProductImage.image',
			3 =>'Product.price',
			4=>'Product.total_qty',
			5 =>'Product.status',
			6 =>'Product.date_added',
		);
		
		
		$totalData = $this->Product->find('count'); 

		$totalFiltered = $totalData; 
		
		$sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( Product.name LIKE '".$requestData['search']['value']."%' ";    
			$sql.=" OR Product.status LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR Product.created LIKE '".$requestData['search']['value']."%' )";
		}
		
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
				
		$products = $this->Product->find('all',array(
					'limit'=>$requestData['length'],
					'offset' => $requestData['start'],
					'conditions'=>$sql,
					'order'=>$order_by
					));	

			
		if(!empty($requestData['search']['value']) ) 
		{  
		   $totalFiltered = count($products);
		}
		
		$data = array();
		foreach( $products as $key=>$row ) { 
		
			$nestedData=array();
			if(isset($row['ProductImage']['image']))
			{
				$product_image=$row['ProductImage']['image'];
			}
			else
			{
				$product_image="";
			}
			
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y' && $this->Session->read('permissions.Product_Management.delete')=='Y'))
			{
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['Product']["id"].'" type="checkbox" />';
			}
			$nestedData[] = '<div id="product_name_'.$row['Product']["id"].'">'.$row['Product']["name"].'</div>';
			$nestedData[] = '<div id="product_name_'.$row['Product']["id"].'">'.$row['Product']["description"].'</div>';
			$nestedData[] = '<div  id="product_image_'.$row['Product']["id"].'"><img style="width:60px; height:60px" src="'.$this->webroot.'uploads/product/original/'.$product_image.'" /></div>';
			$nestedData[] = '<div id="product_price_'.$row['Product']["id"].'">'.$row['Product']["price"].'</div>';
			$nestedData[] = '<div id="product_price_'.$row['Product']["id"].'">'.$row['Product']["total_qty"].'</div>';
			if($row['Product']["status"]=='1'){
				$status = "Active";
				$img = "unlock.png";
				$stat = '0';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = '1';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['Product']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['Product']["date_modified"]));
			
			//================Product Management Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y' && $this->Session->read('permissions.Product_Management.status')=='N' && $this->Session->read('permissions.Product_Management.edit')=='N'  && $this->Session->read('permissions.Product_Management.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Product Management Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y' && $this->Session->read('permissions.Product_Management.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['Product']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Product']['id'].'"><img id="status_img_'.$row['Product']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Product Management Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y' && $this->Session->read('permissions.Product_Management.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/products/edit/'.$row['Product']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Product Management Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y' && $this->Session->read('permissions.Product_Management.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Product']['name'].'?\')" href="'.$this->webroot.'admin/Products/delete_product/'.$row['Product']['id'].'" title="Delete" id="delete_'.$row['Product']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['Product']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['Product']['id'].'"><img id="status_img_'.$row['Product']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/products/edit/'.$row['Product']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['Product']['name'].'?\')" href="'.$this->webroot.'admin/Products/delete_product/'.$row['Product']['id'].'" title="Delete" id="delete_'.$row['Product']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data" => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	//=======================Add Product======================================================
	
	public function add_product()
	{	
		$all_category=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>0),'order'=>'ProductCategory.id DESC'));
		$this->set('all_category',$all_category);		
		$all_Size=$this->Size->find('list');
		$this->set('all_Size',$all_Size);
		if($this->request->is('post'))
		{			
				if($this->Product->save($this->request->data))
				{					
				    $product_id = $this->Product->getLastInsertId();	
                    $all_product_size_qty=$this->request->data['product_size'];
					$size_qty_arr=array();
					$i=0;
					foreach($all_product_size_qty as $all_product_size_qtys)
					{
						
						$this->ProductSizeQty->create();
						$size_qty_arr['ProductSizeQty']['size_id']= $all_product_size_qtys;
						$size_qty_arr['ProductSizeQty']['qty']= $this->request->data['qty'][$i];
						$size_qty_arr['ProductSizeQty']['product_id']= $product_id;
						$this->ProductSizeQty->save($size_qty_arr);
						
						$i++;											
					}    
		
			    $data_image = $this->reArrayFiles($_FILES['ProductImage']); 
		        foreach($data_image as $kk=>$image)
						{	
			
						  $foo = new Upload($image); 
						  $foo->allowed = array('image/jpg/png/gif/jpeg');
								if ($foo->uploaded) {
									   $foo->image_resize          = true;
									   $foo->image_y               = 1000;
									   $foo->image_x               = 1000;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/product/original/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 60;
									   $foo->image_x               = 140;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/product/thumbnail/');
									  if ($foo->processed)
										  {
											$image_name= $foo->file_dst_name;																		
											$image_data['ProductImage']['image']=$image_name;							
											$image_data['ProductImage']['product_id']=  $product_id;			
											if(isset($this->request->data['default_image'])&& $this->request->data['default_image']==$image['name'])
												{
													$image_data['ProductImage']['deflt_value']= 1;
												}
												else
												{
													$image_data['ProductImage']['deflt_value']= 0;
												}
												
												$this->ProductImage->create();	
												$this->ProductImage->save($image_data);	
										     } 
									     
								         }else
										 {
											 $this->Session->setFlash(__('Product Image Error...','default',array('class'=>'alert alert-danger alert-dismissable'))); 
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'add_product'));
											 
										 }	 										 	                                								  
						             }
									 
											 $this->Session->setFlash(__('Product Successfully Added...','default',array('class'=>'alert alert-success alert-dismissable')));
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));									
									 
				                   }else{
									 
											 $this->Session->setFlash(__('Product Add Error...','default',array('class'=>'alert alert-danger alert-dismissable')));
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));									 
									   
								   }
		                      }   
	}	   
								   

	
	function status(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['Product']['status'] = $this->request->data['status'];
			$this->Product->id = $this->request->data['id'];
			if($this->Product->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	
	//=======================================Edit Product===============================================================
	
	function edit($id="")
	{	
           // pr($this->request->data);
			//exit;

	
		if($id=="")
		{
		 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));
		}

        //=====================Product Category=====================================
		$all_category=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.parent_id'=>0),'order'=>'ProductCategory.id DESC'));
		$this->set('all_category',$all_category);
		//==========================================================================
		
	//=====================Product Category========================================
		$all_Size=$this->Size->find('list');
		$this->set('all_Size',$all_Size);		
	
	//=============================================================================
		
		$product_size_qties = $this->ProductSizeQty->find('all',array('conditions'=>array('ProductSizeQty.product_id'=>$id)));
		
		//===========total sele quitity===================//
		$sale_sum=0;
		foreach($product_size_qties as $product_size_qties_by_sale)
		{
			$sale_sum=$sale_sum+$product_size_qties_by_sale['ProductSizeQty']['sale_qty'];
	
		}
		
		$this->set('sale_sum',$sale_sum);
		
		$this->set('product_size_qties',$product_size_qties);
		
	//================================Product data================================================
	$product_data = $this->Product->find('first',array('conditions'=>array('Product.id'=>$id)));
	//==========================================================================================
		
		if(empty($product_data))
		{			
	       $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));
		}
		
		if($this->request->is('post'))
		{  
			
			$check_club = $this->Product->find('first',array('conditions'=>array('Product.name'=>$this->request->data['name'])));
			
			if(!empty($check_club) && $product_data['Product']['name']!=$check_club['Product']['name'])
			{
				$this->Session->setFlash(__('Error in Updating Product, Name Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
				
			}else{
			
				$this->Product->id=$id;
				if($this->Product->save($this->request->data))
				{	                     			
				    //$this->ProductSizeQty->deleteAll(array('ProductSizeQty.product_id' => $id), false);				  	
                    $all_product_size_qty=$this->request->data['product_size'];
					$size_qty_arr=array();
					$i=0;
					
					foreach($all_product_size_qty as $all_product_size_qtys)
					{						
						$uniq_product_size_qty_data=$this->ProductSizeQty->find('first',array('conditions'=>array('ProductSizeQty.product_id'=>$id,'ProductSizeQty.size_id'=>$all_product_size_qtys)));
						
						if( count($uniq_product_size_qty_data)>0)
						{
							
							$this->ProductSizeQty->id=$uniq_product_size_qty_data['ProductSizeQty']['id'];
							
							$total_qty=$uniq_product_size_qty_data['ProductSizeQty']['qty']+$this->request->data['qty'][$i];
							
						    $size_qty_arr['ProductSizeQty']['qty']=$this->request->data['qty'][$i];
							
						}else
						{	
                           
					
							$size_qty_arr['ProductSizeQty']['product_id']= $id;
						    $size_qty_arr['ProductSizeQty']['size_id']= $all_product_size_qtys;
						    $size_qty_arr['ProductSizeQty']['qty']=$this->request->data['qty'][$i];
						}
							
						$this->ProductSizeQty->save($size_qty_arr);
						$this->ProductSizeQty->create();						
						$i++;
                           //pr($size_qty_arr);
						  // exit;					
												
					}
                     					
		        $this->ProductImage->updateAll(array('ProductImage.deflt_value' =>0),array('ProductImage.product_id' =>$id));
			    $data_image = $this->reArrayFiles($_FILES['ProductImage']); 
				
				
				
				if($data_image[0]['name']!="")
				{
		        foreach((array) $data_image as $kk=>$image)
						{	
			
						  $foo = new Upload($image); 
						  $foo->allowed = array('image/jpg/png/gif/jpeg');
								if ($foo->uploaded) {
									   $foo->image_resize          = true;
									   $foo->image_y               = 1000;
									   $foo->image_x               = 1000;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/product/original/');
									   $foo->image_resize          = true;
									   $foo->image_y               = 60;
									   $foo->image_x               = 140;
									   $foo->allowed = array('image/*');
									   $foo->Process('uploads/product/thumbnail/');
									  if ($foo->processed)
										  {
											$image_name= $foo->file_dst_name;																		
											$image_data['ProductImage']['image']=$image_name;							
											$image_data['ProductImage']['product_id']=  $id;
											
										if(isset($this->request->data['default_image'])&& $this->request->data['default_image']==$image['name'])
											{
												$image_data['ProductImage']['deflt_value']= 1;
											}
											else
											{
												$image_data['ProductImage']['deflt_value']= 0;
											}
											
											$this->ProductImage->create();	
											$this->ProductImage->save($image_data);	
										 } 
									     
								         }else
										 {
											 $this->Session->setFlash(__('Product Image Error...','default',array('class'=>'alert alert-danger alert-dismissable'))); 
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'edit/'.$id));											 
										 }	 										 	                                								  
						             }
									 
				}
									 //================================================================================
									 
									       if(is_numeric($this->request->data['default_image']))
										   {
					                          
							  
											  $this->ProductImage->updateAll(array('ProductImage.deflt_value' =>1),array('ProductImage.id' =>$this->request->data['default_image']));											 
										   }
									//==================================================================================
									
											 $this->Session->setFlash(__('Product Successfully Update...','default',array('class'=>'alert alert-success alert-dismissable')));
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));									
									 
				                   }else{
									 
											 $this->Session->setFlash(__('Product Update Error...','default',array('class'=>'alert alert-danger alert-dismissable')));
											 $this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));									 
									   
								   }
		                      }			
			               }
				
		                           $this->set('product_data',$product_data);
	          }
			  
	//=====================Delete Product=============================================		  
	function delete_product($id="")
	{
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id))
		{
			$product_image_data = $this->ProductImage->find('all',array('fields'=>'ProductImage.image','conditions'=>array('ProductImage.product_id'=>$id)));						
			foreach((array) $product_image_data as $product_image_datas)
			{
				@unlink('uploads/Product/logo/original/'.$product_image_datas['ProductImage']['image']);
				@unlink('uploads/Product/logo/thumbnail/'.$product_image_datas['ProductImage']['image']);
			}
			
			if($this->Product->delete($id))
			{
				 //==================image/size quitity delete=========================================//				 
				 $this->ProductImage->deleteAll(array('ProductImage.product_id' => $id), false);
				 $this->ProductSizeQty->deleteAll(array('ProductSizeQty.product_id' => $id), false);				 
				 //=======================end===========================================================//
				 
				$this->Session->setFlash(__('Product Successfully  Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Product...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'	));
						
						
						
					
		}
	}		  
			  
	//====================Delete Multiple Products ===============================================================// 		  
	
	function delete_product_multiple(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id'])
		{
			foreach($this->request->data['id'] as $id)
			{				
				$product_image_data = $this->ProductImage->find('all',array('fields'=>'ProductImage.image','conditions'=>array('ProductImage.product_id'=>$id)));						
				 foreach((array) $product_image_data as $product_image_datas)
				{
					@unlink('uploads/Product/logo/original/'.$product_image_datas['ProductImage']['image']);
					@unlink('uploads/Product/logo/thumbnail/'.$product_image_datas['ProductImage']['image']);
				}
			
				if($this->Product->delete($id))
			    {
				 //==================image/size quitity delete=========================================//				 
				 $this->ProductImage->deleteAll(array('ProductImage.product_id' => $id), false);
				 $this->ProductSizeQty->deleteAll(array('ProductSizeQty.product_id' => $id), false);				 
				 //=======================end===========================================================//
				 
				$this->Session->setFlash(__('Product Successfully  Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting Product...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			
			}
			
	         	$this->redirect(array('plugin' => 'admin','controller' => 'products','action' => 'index'));	
		}
		
			
	}
	
	//================================Multiple Image============================================================
	
		private function reArrayFiles(&$file_post) {

			$file_ary = array();
			$file_count = count($file_post['name']);
			$file_keys = array_keys($file_post);

			for ($i=0; $i<$file_count; $i++) {
				foreach ($file_keys as $key) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				}
			}

			return $file_ary;
		}
		
		
  
	
	public function remove_image()
	{
		$this->layout="";
		$Image_id=$this->request->data['id'];		
		$product_data = $this->ProductImage->find('first',array('fields'=>'ProductImage.image','conditions'=>array('ProductImage.id'=>$Image_id)));
		@unlink('uploads/Product/original/'.$product_data['ProductImage']['image']);
		@unlink('uploads/Product/thumbnail/'.$product_data['ProductImage']['image']);
		$this->ProductImage->delete($Image_id);		
		exit();		
	}
	
	
	
}