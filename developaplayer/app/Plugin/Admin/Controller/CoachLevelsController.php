<?php
App::uses('AdminAppController', 'Admin.Controller');
class CoachLevelsController extends AdminAppController {
	var $uses=array('Admin.CoachLevel','Admin.LevelCategory');
	 public function beforeFilter() {
	   parent::beforeFilter();
       
    }
	
	public function index()
	{
		$this->check_Permission('coach_level','view');
	}
	
	public function level_data(){
		$this->layout="ajax";
		$this->render(false);
		
		
		$requestData= $_REQUEST;
		$columns = array(
			0 =>'CoachLevel.id',
			1 =>'CoachLevel.level_number',
			2 =>'CoachLevel.name',			
			3 =>'CoachLevel.status',
			4 =>'CoachLevel.created',
		);
		$totalData = $this->CoachLevel->find('count'); 
		$totalFiltered = $totalData; 
		
		$sql = "";
		$category_sql = "";
		if( !empty($requestData['search']['value']) ) {
			$sql.="( CoachLevel.name LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR CoachLevel.level_number LIKE '".$requestData['search']['value']."%' ";
			$sql.=" OR CoachLevel.status LIKE '".substr($requestData['search']['value'],0,1)."%' ";
			$sql.=" OR CoachLevel.created LIKE '".$requestData['search']['value']."%' ";
			//$sql.=" OR LevelCategory.name LIKE '".$requestData['search']['value']."%' ";
			$sql.= ")";
		}
		
		$order_by=$columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir'];
		$limit = $requestData['start'].','.$requestData['length'];
		$this->CoachLevel->bindModel(
				array(
						'belongsTo'=>array(
							'LevelCategory'	=>	array(
									'className'	=>	'LevelCategory',
									'foreignKey'	=>	'category_id',
									'dependent'	=>	true
									)
							)
				));
				
		if( !empty($requestData['search']['value']) ) {   		
			$levels = $this->CoachLevel->find('all',array(
												'fields'=>array('DISTINCT CoachLevel.id','CoachLevel.name','CoachLevel.level_number','CoachLevel.status','CoachLevel.created','LevelCategory.name'),
												'conditions'=>$sql,
												 'joins'=>array(
															array(
																'type'=>'left',
																'table'=>'level_categories',
																//'alias'=>'LevelCategory',
																'conditions'=>array(
																	'LevelCategory.id = CoachLevel.category_id'
																),
																'group' => 'LevelCategory.id'	
															)
														),
															
												'order'=>$order_by,
												'offset' => $requestData['start'],
												'limit'=>$requestData['length'],
												'recursive'=>3
												));
		}else{
			$levels = $this->CoachLevel->find('all',array(
													'limit'=>$requestData['length'],
													'offset' => $requestData['start'],
													'conditions'=>$sql,
													'order'=>$order_by
													));
		}
												
		/*$log = $this->CoachLevel->getDataSource()->getLog(false, false);
		debug($log);
		print"<pre>";
		print_r($levels);
		die;*/
		//print_r($levels);
		if( !empty($requestData['search']['value']) ) {  
		 $totalFiltered = count($levels);
		}
		$data = array();
		foreach( $levels as $row ) {  // preparing an array
			$nestedData=array();
			
			if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y' && $this->Session->read('permissions.coach_level.delete')=='Y'))
			{ 
				$nestedData[] = '<input class="checkbox_del" name="id[]" value="'.$row['CoachLevel']["id"].'" type="checkbox" />';
			}
			$nestedData[] = $row['CoachLevel']["level_number"];
			$nestedData[] = substr($row['CoachLevel']["name"], 0, 20)."...";
			//$nestedData[] = $row['LevelCategory']["name"];
			
			if($row['CoachLevel']["status"]=='A'){
				$status = "Active";
				$img = "unlock.png";
				$stat = 'I';
			}else{
				$status = "InActive";
				$img = "lock.png";
				$stat = 'A';
			}
			
			$nestedData[] = '<div id="status_of_'.$row['CoachLevel']["id"].'">'.$status.'</div>';
			$nestedData[] = date('d-M-Y',strtotime($row['CoachLevel']["created"]));
			
			//================Coach Level Check==============================//
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y' && $this->Session->read('permissions.coach_level.status')=='N' && $this->Session->read('permissions.coach_level.edit')=='N'  && $this->Session->read('permissions.coach_level.delete')=='N')
			{  
				$nestedData[]="";
				
			}
			//================Coach Level Status==============================//
			$nestedData_status="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y' && $this->Session->read('permissions.coach_level.status')=='Y')
			{  
				$nestedData_status = '<a href="javascript:change_status('.$row['CoachLevel']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['CoachLevel']['id'].'"><img id="status_img_'.$row['CoachLevel']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>';
			}
			//================Coach Level Edit==============================//
			$nestedData_edit="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y' && $this->Session->read('permissions.coach_level.edit')=='Y')
			{  
				$nestedData_edit = '&nbsp<a href="'.$this->webroot.'admin/coach_levels/edit/'.$row['CoachLevel']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>';
			}
			//================Coach Level Delete==============================//
			$nestedData_delete="";   
			if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y' && $this->Session->read('permissions.coach_level.delete')=='Y')
			{  
				$nestedData_delete = '&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['CoachLevel']['name'].'?\')" href="'.$this->webroot.'admin/coach_levels/delete_level/'.$row['CoachLevel']['id'].'" title="Delete" id="delete_'.$row['CoachLevel']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			//===========================================================================//
			if($this->Session->read('Auth.User.type')=='M')
			{			       
				$nestedData[] = $nestedData_status.''.$nestedData_edit.''.$nestedData_delete;
			}
			else
			{
			$nestedData[] = '<a href="javascript:change_status('.$row['CoachLevel']['id'].',\''.$stat.'\')" title="Inactive" id="status_'.$row['CoachLevel']['id'].'"><img id="status_img_'.$row['CoachLevel']['id'].'" alt="Inactive" width="30px" height="23px" src="'.$this->webroot.'admin/img/icons/'.$img.'" /></a>&nbsp<a href="'.$this->webroot.'admin/coach_levels/edit/'.$row['CoachLevel']['id'].'"><img alt="Edit" title="Edit" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/edit.png" /></a>&nbsp<a onclick="return confirm(\'Are You Sure You Want To Delete '.$row['CoachLevel']['name'].'?\')" href="'.$this->webroot.'admin/coach_levels/delete_level/'.$row['CoachLevel']['id'].'" title="Delete" id="delete_'.$row['CoachLevel']['id'].'"><img alt="Delete" width="30px" height="23px" src="'.$this->webroot.'admin/admin_css_js/dist/img/delete.png" /></a>';
			}
			$data[] = $nestedData;
		}

		$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

		echo json_encode($json_data);  // send data as json format
	}
	
	function add_level(){
		$this->check_Permission('coach_level','add');
		if($this->request->data){
			$CoachLevel['CoachLevel']=$this->request->data;
			$CoachLevel['CoachLevel']['level_number'] = number_format($CoachLevel['CoachLevel']['level_number'],2);
			$check_level = $this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.name'=>$CoachLevel['CoachLevel']['name'])));
			if(!empty($check_level)){
				$this->Session->setFlash(__('CoachLevel Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				if($this->CoachLevel->save($CoachLevel)){
					$this->Session->setFlash(__('CoachLevel Succesfully Added...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_levels',
							'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Adding CoachLevel...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function status(){
		$this->check_Permission('coach_level','status');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->is('post')){
			$data['CoachLevel']['status'] = $this->request->data['status'];
			$this->CoachLevel->id = $this->request->data['id'];
			if($this->CoachLevel->save($data)){
				echo "success";
			}else{
				echo "update_error";
			}
		}
	}
	
	function delete_level($id=""){
		$this->check_Permission('coach_level','delete');
		$this->layout="ajax";
		$this->render(false);
		if(!empty($id)){
			if($this->CoachLevel->delete($id)){
				$this->Session->setFlash(__('CoachLevel Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}else{
				$this->Session->setFlash(__('Error in Deleting CoachLevel...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_levels',
						'action' => 'index'
						));
		}
	}
	
	function edit($id=null){
		$this->check_Permission('coach_level','edit');
		if($id!=null){
			$CoachLevel=$this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.id'=>$id)));
			if(empty($CoachLevel)){
				$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_levels',
							'action' => 'index'
							));
			}
			$this->set('CoachLevel',$CoachLevel);
		}else{
			$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_levels',
						'action' => 'index'
						));
		}
		
		if($this->request->data){
			$level_edited['CoachLevel']=$this->request->data;
			$level_edited['CoachLevel']['level_number'] = number_format($level_edited['CoachLevel']['level_number'],2);
			$check_level = $this->CoachLevel->find('first',array('conditions'=>array('CoachLevel.category_id'=>$CoachLevel['CoachLevel']['category_id'],'CoachLevel.name'=>$CoachLevel['CoachLevel']['name'])));
			if($level_edited['CoachLevel']['level_number']==$CoachLevel['CoachLevel']['level_number']){
				$check_level="";
			}
			if(!empty($check_level)){
				$this->Session->setFlash(__('CoachLevel Already Exist...','default',array('class'=>'alert alert-danger alert-dismissable')));
			}else{
				$this->CoachLevel->id = $id;
				if($this->CoachLevel->save($level_edited)){
					$this->Session->setFlash(__('CoachLevel Succesfully Updated...','default',array('class'=>'alert alert-success alert-dismissable')));
					$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_levels',
							'action' => 'index'
							));
				}else{
					$this->Session->setFlash(__('An Unexpected Error Occured While Updating CoachLevel...','default',array('class'=>'alert alert-danger alert-dismissable')));
				}
			}
		}
	}
	
	function delete_level_multiple(){
		$this->check_Permission('coach_level','delete');
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			
			foreach($this->request->data['id'] as $id){
				$this->CoachLevel->create();
				$this->CoachLevel->delete($id);
				$this->Session->setFlash(__('Levels Succesfully Deleted...','default',array('class'=>'alert alert-success alert-dismissable')));
			}
		}
		$this->redirect(array(
						'plugin' => 'admin',
						'controller' => 'coach_levels',
						'action' => 'index'
						));
	}
	
	function get_category(){
		$this->layout="ajax";
		$this->render(false);
		if($this->request->data['id']){
			$id = $this->request->data['id'];
			$categories=$this->LevelCategory->find('all');
			$data="";
			foreach($categories as $category){
				if($id!="no"){
					if($category['LevelCategory']['id']==$id){
						$status = "selected";
					}else{
						$status = "";
					}
				}else{
					$status = "";
				}
				$data.='<option '.$status.' value="'.$category['LevelCategory']['id'].'" id="'.$category['LevelCategory']['id'].'">'.$category['LevelCategory']['name'].'</option>';
			}
			echo $data;
		}else{
			$this->redirect(array(
							'plugin' => 'admin',
							'controller' => 'coach_levels',
							'action' => 'index'
							));
		}
	}
}