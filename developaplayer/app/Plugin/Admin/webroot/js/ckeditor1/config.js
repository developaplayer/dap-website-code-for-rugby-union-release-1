﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
	config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,forms,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,language,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,undo,wsc,lineutils,widget,placeholder,qrc,quicktable,autolink,autoembed,autogrow,autocorrect,backgrounds,basewidget,base64image,glyphicons,bootstrapVisibility,widgetbootstrap,cavacnote,chart,footnotes,ckwebspeech,pbckcode,codesnippet,xml,ajax,codesnippetgeshi,codeTag,codemirror,widgetcommon,ccmsconfighelper,confighelper,custimage,devtools,divarea,docprops,dropdownmenumanager,image2,eqneditor,extraformattributes,fastimage,filetools,fixed,floating-tools,fontawesome,googledocs,ckeditor-gwf-plugin,xmas,html5validation,htmlbuttons,Audio,iframedialog,imgbrowse,imagebrowser,imagepaste,imageresize,imagerotate,imgupload,imageresponsive,inlinecancel,closebtn,insertpre,symbol,inserthtml4x,textselection,layoutmanager,leaflet,letterspacing,lightbox,lineheight,locationmap,imgur,lite,widgettemplatemenu,textsignature,texttransform,token,toolbarswitch,uicolor,notification,notificationaggregator,uploadwidget,uploadimage,uploadcare,videosnapshot,youtube';
	config.skin = 'moono-dark';
	// %REMOVE_END%

	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
