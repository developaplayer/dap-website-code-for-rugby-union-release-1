<?php
class Course extends AdminAppModel
{
	public $name = 'Course';	
	
	public $belongsTo =array(
			'LevelCategory'=>	array(
				'className'	=>'Admin.LevelCategory',
				'foreignKey'=>'category_id',
				'dependent'	=>true,
				'fields'       =>''
			)
		);	
	public $hasMany = array(
				'CourseAgeGroup'=> array(
				'className' =>'Admin.CourseAgeGroup',
				'foreignKey'=>	'course_id',
				'dependent'	=>	true,
				'fields'	=>	''
			),
			'CourseCoach'=> array(
				'className' =>'Admin.CourseCoach',
				'foreignKey'=>	'course_id',
				'dependent'	=>	true,
				'fields'	=>	''
			)
			
	);
	public $hasOne = array(
				 'CourseDate'=>array(
				 'className'=>'CourseDate',
				 //'conditions'   => '',
				 'order'        => '',
				 'dependent'    =>  true,
				 'foreignKey'=>'course_id',			
				 )
			);
		
}