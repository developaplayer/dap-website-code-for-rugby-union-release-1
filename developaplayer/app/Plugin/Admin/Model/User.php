<?php
class User extends AdminAppModel
{
	//public $name = 'User';
	//public $useTable = 'dp_users';
	//public $useTable = 'users';
 	public $name = 'User';
	public $hasOne = array(
			'CoachDetail'=> array(
			'className' =>'Admin.CoachDetail',
			'foreignKey'=>'user_id',
			'fields' =>''
			),
			'PlayerDetail'=> array(
			'className' =>'Admin.PlayerDetail',
			'foreignKey'=>'user_id',
			'fields' =>''
			),
			'FamilyDetail'=> array(
			'className' =>'Admin.FamilyDetail',
			'foreignKey'=>'user_id',
			'fields' =>''
			)
		);
		
	/*'AddCoachRating'=> array(
			'className' =>'Admin.AddCoachRating',
			'foreignKey'=>'coach_id ',
			'fields' =>''
			)*/
		
	public $hasMany = array(
		'CoachCertificate'=> array(
		'className' =>'Admin.CoachCertificate',
		'foreignKey'=>	'user_id',
		'dependent'	=>	true,		'fields'	=>	array('CoachCertificate.certificate_id')
		),
		'Video'	=>	array(
		'className'	=>	'Admin.Video',
		'foreignKey'	=>	'user_id',
		'dependent'	=>	true,
		'fields'	=>	array('Video.id','Video.video_link','Video.video_iframe','Video.video_type')
		),
		'PlayerVideo'	=>	array(
		'className'	=>	'Admin.PlayerVideo',
		'foreignKey'	=>	'user_id',
		'dependent'	=>	true,
		'fields'	=>	array('PlayerVideo.id','PlayerVideo.video_link','PlayerVideo.video_iframe','PlayerVideo.video_type')
		)
	);
}
?>