<?php
class BookCourseByPlayer extends AdminAppModel
{
	public $name = 'BookCourseByPlayer';	
	
	public $belongsTo= array(
				'User'=> array(
				'className' =>'Admin.User',
				'conditions'=>'',
				'foreignKey'=>	'player_id',
				'dependent'	=>	true,
				'fields'	=>	''
			),
			'Course'=> array(
				'className' =>'Admin.Course',
				'conditions'=>'',
				'foreignKey'=>	'course_id',
				'dependent'	=>	true,
				'fields'	=>	''
			),
			'CourseDate'=> array(
				'className' =>'Admin.CourseDate',
				'conditions'=>'',
				'foreignKey'=>	'course_date_id',
				'dependent'	=>	true,
				'fields'	=>	''
			)
			
	);
	
}