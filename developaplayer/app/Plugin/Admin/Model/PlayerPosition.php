<?php
/*
*PlayerPosition Model 
* Rezaul Karim - Accenza Pvt Ltd 
* 27.2.2016 
*/
class PlayerPosition extends AppModel
{
	public $name = 'PlayerPosition';
	
	function get_player_position()
	{
		$playerPosition = $this->find('list',array('fields'=>array('PlayerPosition.id','PlayerPosition.name')));
		return $playerPosition;
	}
}
?>