<?php
class Product extends AdminAppModel
{
	public $name = 'Product';	
	
	public $hasOne= array(
				'ProductImage'=> array(
				'className' =>'Admin.ProductImage',
				'conditions'=>'ProductImage.deflt_value=1',
				'foreignKey'=>	'product_id',
				'dependent'	=>	true,
				'fields'	=>	''
			)
			
	);
	
		public $hasMany= array(
				'allProductImage'=> array(
				'className' =>'Admin.ProductImage',
				//'conditions'=>'ProductImage.deflt_value=1',
				'foreignKey'=>	'product_id',
				'dependent'	=>	true,
				'fields'	=>	''
			)
			
	);
		
}