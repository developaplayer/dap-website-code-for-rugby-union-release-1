<?php
class ProductCategory extends AdminAppModel
{
	public $name = 'ProductCategory';	
	
	public $hasMany = array(
				'SubCategory'=> array(
				'className' =>'Admin.ProductCategory',
				//'conditions'=>'ProductCategory.id=SubCategory.parent_id',
				'foreignKey'=>	'parent_id',
				'dependent'	=>	true,
				'fields'	=>	''
			)
			
	);
		
}