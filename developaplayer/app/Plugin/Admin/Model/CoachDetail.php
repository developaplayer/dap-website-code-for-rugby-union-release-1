<?php
class CoachDetail extends AdminAppModel
{
	public $name = 'CoachDetail';	
	
	public $belongsTo =array(
			'CoachSpeciality'=>	array(
				'className'	=>'Admin.CoachSpeciality',
				'foreignKey'=>'primary_speciality_id',
				'dependent'	=>false,
				'fields'       =>''
			),
			'second_CoachSpeciality'=>array(
				'className'	=>	'Admin.CoachSpeciality',
				'foreignKey'	=>	'secondary_speciality_id',
				'dependent'	=>	false,
				'fields'       =>''
			)
		);	
}