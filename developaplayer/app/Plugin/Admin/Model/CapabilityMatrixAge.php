<?php 
class CapabilityMatrixAge extends AdminAppModel {
/**
 * Behaviors
 *
 * @var array
 */
 	public $useTable = 'capability_matrix_ages';
 	public $name = 'CapabilityMatrixAge';
	public $belongsTo =array(
			'AgeGroup'=>	array(
				'className'	=>'Admin.AgeGroup',
				'foreignKey'=>'age_group_id',
				'dependent'	=>false,
				'fields'       =>''
			),
			'PlayerPosition'=>array(
				'className'	=>	'Admin.PlayerPosition',
				'foreignKey'	=>	'player_position_id',
				'dependent'	=>	false,
				'fields'       =>''
			)
		);
}
?>