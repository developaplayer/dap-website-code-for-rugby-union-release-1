<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Change Password</h1>
					 <?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Change Password</strong>
                        </div>

                        <div class="panel-body">
                            <?php 
						echo $this->Form->create("FormResetPassword", array('url'=>array('controller' =>'users', 'action' => 'change_password','plugin'=>'admin'),'id'=>'FormResetPassword','enctype'=>'multipart/form-data'));			
				
							?>            
										<div class="form-group">
                                            <label>Current Password </label>
                                            <?php  
											   echo $this->Form->input('current_password',array(
																	'class'=>'form-control',
																	'placeholder'=>'Enter Current Password',
																	'type'=>'password',
																	'autofocus'=>true,
																	'label' => false,
																	'autocomplete'=>'off',
																	'value'=>'',
																	'required'=>true
																	  )
																  ); 
											   ?>
                                        </div>
                                      
                                       <div class="form-group">
                                            <label>New Password </label>
                                            <?php  
											   echo $this->Form->input('new_password',array(
																	'class'=>'form-control',
																	'placeholder'=>'Enter New Password',
																	'type'=>'password',
																	'autocomplete'=>'off',
																	'value'=>'',
																	'label' => false,
																	'required'=>true
																	  )
																  ); 
											   ?>
                                        </div>
										
										 <div class="form-group">
                                            <label>Confirm New Password </label>
                                            <?php  
											   echo $this->Form->input('confirm_new_password',array(
																	'class'=>'form-control',
																	'placeholder'=>'Confirm New Password',
																	'type'=>'password',
																	'autocomplete'=>'off',
																	'value'=>'',
																	'label' => false,
																	'required'=>true
																	  )
																  ); 
											   ?>
                                        </div>
                                        <input class="btn btn-success" type="submit" name="send_mass_message" value="Save">
                                        <input class="btn btn-primary" type="reset" name="cacel_mass_message" value="Cancel">
                                   <?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
       