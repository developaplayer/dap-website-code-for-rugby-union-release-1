﻿    <div class="login-box">
				<?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
      <div class="login-logo">
	  
        <a href="javascript:void(0);"><b>Develop A Player</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Forget Password</p>
        <?php 
				echo $this->Form->create("User", array('url'=>array('plugin'=>'admin','controller' =>'users', 'action' => 'forget_password'),'id'=>'ForgetPassword'));
		?>                         
						 <fieldset>
                                <div class="form-group">
								<div class="input email">
								<label for="UserEmailAddress">Email Address</label>
                               <?php   
								echo $this->Form->input("email_address",array("label" => false, "div"=>false, "class" => false, "placeholder" => "E-mail", "class" => "form-control",'type'=>'email','autofocus'=>true,'required'=>true)); 
							   ?>
							   </div>
								</div>
                            <?php  
							   echo $this->Form->input('Reset Password',array(
													'class'=>'btn btn-lg btn-success btn-block',
													'type'=>'submit',
													'label' => false
													  )
												  ); 
							   ?>
							</fieldset>
                       <?php echo $this->Form->end(); ?>

       
        Dont want to reset password? Then <a href="<?php echo $this->webroot; ?>admin/users/login">Login here</a><br>
      

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
<script>
/*
checkLogin();
function checkLogin(){
	setTimeout(function(){
		$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/users/check_login',
					data:{
						'id':'1'
					},
					success:function(data){
						if(data==1){
							checkLogin();
						}else{
							window.location.href="<?php echo $this->webroot; ?>admin/users/dashboard";
						}
					}
				});
	}, 1500);
}*/
</script>