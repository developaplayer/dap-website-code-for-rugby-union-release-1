<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<?php

echo $this->Html->script('/admin/admin_css_js/plugins/jQuery/jQuery-2.1.4.min');
echo $this->Html->css('/admin/admin_css_js/dist/css/AdminLTE.min');
echo $this->Html->script('/admin/admin_css_js/plugins/jQuery/jquery.validate');
echo $this->Html->css('/admin/admin_css_js/bootstrap/css/bootstrap.min');

?>
<script>
//validate function
		$(document).ready(function() 
		{ 
			$('#email_form').validate();
		});
</script>
<html>
<body>
<div>
			<?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
						<?php echo $this->Session->flash(); ?>
                  </div>
			<?php }?>
	<h3 style="border-bottom:2px solid; color:#2e6da4;">Send Email</h3>
	<form method="post" type="multipart/form-data" action="" id="email_form">
		<div class="form-group">
			From:<input class="required form-control" type="text" name="from" value="<?php echo $this->Session->read('Auth.User.email_address') ?>" />
		</div>
		<div class="form-group">
			To:<input class="required form-control" type="text" name="to" value="<?php echo $email_to ?>" />
		</div>
		<div class="form-group">
			Subject:<input class="required form-control" type="text" name="subject" />
		</div>
		<div class="form-group">
			Message:<input class="required form-control" type="text" name="message" />
		</div>
		<input class="form-control btn btn-primary" type="submit" value="Send" />
	</form>
</div>
</body>
</html>