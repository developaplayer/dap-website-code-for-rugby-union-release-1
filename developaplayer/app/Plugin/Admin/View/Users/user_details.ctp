 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $user[0]['Customer']['first_name']."&nbsp;".$user[0]['Customer']['last_name'] ?>
            <small>User Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">User Data</a></li>
            <li class="active"><?php echo $user[0]['Customer']['first_name']."&nbsp;".$user[0]['Customer']['last_name'] ?></li>
          </ol>
        </section>
		
		  <!-- Main content -->
        <section class="content">
		
		<div class="row">
            <div class="col-xs-12">

              <div class="box">
			   <div class="box-header">
                  <h3 class="box-title">User Data</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
				<!---<form method="post" type="multipart" action="<?php echo $this->webroot; ?>admin/users/add_user">--->
                <div class="box-body">
				  
				 <form method="post" action="" enctype="multipart" id="form">
                  <div class="box-body">
				<?php
				  
				  echo $this->Form->input('id',array(
													'div'=>'form-group',
													'label' =>false,
													'type'=>'hidden',
													'name'=>'id',
													'value'=>$user[0]['Customer']['id']
													)
												);
												
				  echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'First Name',
													'type'=>'text',
													'name'=>'first_name',
													'placeholder'=>'Enter First Name',
													'value'=>$user[0]['Customer']['first_name']
													)
												);
												
				  echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Last Name',
													'type'=>'text',
													'name'=>'last_name',
													'placeholder'=>'Enter Last Name',
													'value'=>$user[0]['Customer']['last_name']
													)
												);
												
				  
				  echo $this->Form->input('mobile_number',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Mobile',
													'type'=>'text',
													'name'=>'mobile_number',
													'placeholder'=>'Enter Mobile',
													'value'=>$user[0]['Customer']['mobile_number']
													)
												);
												
				$num=0;
				$counted=count($user[0]['AddressToCustomer']);
				foreach($user[0]['AddressToCustomer'] as $k=>$addr){
					$num++;
				  echo $this->Form->input('address['.$num.']',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Address('.$num.')',
													'placeholder'=>'Enter Address',
													'style'=>'width: 1047px; height: 78px;',
													'name'=>'address['.$num.']',
													'type' =>'textarea',
													'value'=>$addr['address']
													)
												);
															
				echo $this->Form->input('pincode',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'PinCode',
													'type'=>'text',
													'name'=>'pincode['.$num.']',
													'placeholder'=>'Enter PinCode',
													'value'=>$addr['pincode']
													)
												);
				} ?>
				<div id="add_button" class="pull-left btn btn-primary"><a href="javascript:void(0)" onclick="add_addr('<?php echo $num ?>')"><img src="<?php echo $this->webroot; ?>images/add.png" width="20px" />Add More Address</a></div>
				<br /><br />
				<?php
												
				  echo $this->Form->input('email',array(
													'div'=>'form-group',
													'label' =>false,
													'type'=>'hidden',
													'name'=>'email',
													'value'=>$user[0]['Customer']['email']
													)
												);
									
				  echo $this->Form->input('password',array(
													'div'=>'form-group',
													'label' =>false,
													'type'=>'hidden',
													'name'=>'password',
													'value'=>$user[0]['Customer']['password']
													)
												);
										?>
				  
				 <div class="form-group has-feedback">
                    <label>Status</label>
                    <select name="status" class="required form-control" data-placeholder="Select a Status">
						<option <?php if($user[0]['Customer']['status']=='A'){ ?> selected="selected" <?php }?> value="A">Published</option>
					<option <?php if($user[0]['Customer']['status']=='I'){ ?> selected="selected" <?php }?> value="I">Unpublished</option>
					
					</select>
                    </div>
				  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
					 <b><a href="<?php echo $this->webroot; ?>admin/users/view_all" class="btn-primary btn">Cancel</a></b>
                  </div>
                </form>
			   </div>
			  </div>
			  </div>
			  </div>
		
		
		
		</section>
		
		 <script type="javascript/text">
	 
	 $(function () {
    //Initialize Select2 Elements
        $(".select2").select2();
	 });
	 </script>