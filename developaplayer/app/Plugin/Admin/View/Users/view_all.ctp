<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Users</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/users/user_multiple_delete" enctype="multipart" id="delete_user">
				<?php if($this->Session->read('Auth.User.type')=='A'){	?>
						  <input class="selectall" type="checkbox" />
						  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px" />
			<?php }else{ ?>
				<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
									<input class="selectall" type="checkbox" />
									<input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px" />
						<?php }
						} 
					}
				} ?>

						  <br />
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A'){	?>
								<th>Select</th>
						<?php }else{ ?>
						<?php if(!empty($permissions)){ ?>
							<?php foreach($permissions as $k=>$permission) {?>
								<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
											<th>Select</th>
								<?php }
								} 
							}
						} ?>
						
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
						<th>Status</th>
						<th>Options</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($user as $k=>$data){ ?>
                   <tr>
					 
					 
			<?php if($this->Session->read('Auth.User.type')=='A'){	?>
			<td>
					  <input class="checkbox_del" name="id[]" value="<?php echo $data['Customer']['id'] ?>" type="checkbox" /></td>
			<?php }else{ ?>
				<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
						<td>
						<input class="checkbox_del" name="id[]" value="<?php echo $data['Customer']['id'] ?>" type="checkbox" /></td>
						<?php }
						} 
					}
				} ?>
					  
                      <td><?php echo $data['Customer']['first_name']."&nbsp".$data['Customer']['last_name'] ?></td>
                        
						<td><a onclick="fancy_box('<?php echo $data['Customer']['id'] ?>','<?php echo $data['Customer']['email'] ?>')" class="<?php echo $data['Customer']['id'] ?>" href="javascript:void(0)"><?php echo $data['Customer']['email'] ?></a></td>
                        <td><?php echo $data['Customer']['mobile_number'] ?></td>
                        <td><?php echo $data['Customer']['signup_date'] ?></td>
						<td>	<?php if($data['Customer']['status']=='A'){?>
									Published
									<?php } else {
										if($data['Customer']['status']=='I'){ ?>
										Unpublished
									<?php } }?>
						</td>
						<td style="width:118px !important"><b>
						
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['edit_permission']=='Y'){ ?>
					<a href="<?php echo $this->webroot; ?>admin/users/user_details/<?php echo base64_encode($data['Customer']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>&nbsp;
					<?php }
				}
			} ?>
						
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
						<a onclick="confirm_delete('<?php echo base64_encode($data['Customer']['id']) ?>','<?php echo $data['Customer']['first_name'].' '.$data['Customer']['last_name'] ?>','user','<?php echo $this->webroot.'admin/users/user_delete/'.base64_encode($data['Customer']['id']) ?>')" href="javascript:void(0)" title="Delete" id="<?php echo base64_encode($data['Customer']['id']) ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
					<?php }
				}
			}
			
			if($this->Session->read('Auth.User.type')=='A'){	?>
				<a href="<?php echo $this->webroot; ?>admin/users/user_details/<?php echo base64_encode($data['Customer']['id']); ?>">
				<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>&nbsp;
				<a onclick="confirm_delete('<?php echo base64_encode($data['Customer']['id']) ?>','<?php echo $data['Customer']['first_name'].' '.$data['Customer']['last_name'] ?>','user','<?php echo $this->webroot.'admin/users/user_delete/'.base64_encode($data['Customer']['id']) ?>')" href="javascript:void(0)" title="Delete" id="<?php echo base64_encode($data['Customer']['id']) ?>">
				<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
			<?php } ?>
						
						
						</td>
                      </tr>

					<?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
						 <?php if($this->Session->read('Auth.User.type')=='A'){	?>
								<th>Select</th>
						<?php }else{ ?>
						<?php if(!empty($permissions)){ ?>
							<?php foreach($permissions as $k=>$permission) {?>
								<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
											<th>Select</th>
								<?php }
								} 
							}
						} ?>
                        <th>User Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
						<th>Status</th>
						<th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  
	   <script>
	  function unit_type_confirm_delete($redirect_link)
	  {
		  if(confirm('Are You Sure You Want To Delete')==true)
		  {
			  window.location=($redirect_link);
		  }
	  }
	  </script>
