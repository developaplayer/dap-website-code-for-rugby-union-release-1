<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Send Mass Email</h1>
					 <?php echo $this->Session->flash(); ?>
                </div> 
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                           <strong>Send Email</strong> 
					   </div>

                        <div class="panel-body">
                            <?php 
						echo $this->Form->create("FormSendMassmail", array('url'=>array('controller' =>'users', 'action' => 'sendmassmail','plugin'=>'admin'),'id'=>'FormSendMassmail','enctype'=>'multipart/form-data'));			
				
							?>            
										<div class="form-group">
                                            <label>Mail Subject </label>
                                            <?php  
											   echo $this->Form->input('mass_mail_subject',array(
																	'class'=>'form-control',
																	'type'=>'text',
																	'autofocus'=>true,
																	'required'=>true,
																	'label' => false
																	  )
																  ); 
											   ?>
                                        </div>
                                      
                                       
                                        <div class="form-group">
                                            <label>Mail Content</label>
                                           
											 <?php  
											   echo $this->Form->textarea('mass_mail_content', array(
																'class' => 'ckeditor',
																'required'=>true
													));
											   ?>
                                        </div>
                                        
                                        <input class="btn btn-success" type="submit" name="send_mass_message" value="Send"> 
                                   <?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
       