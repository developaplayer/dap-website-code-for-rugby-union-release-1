<script type="text/javascript">
//window.onload=load_country();
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input User Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
				  <form method="post" action="" enctype="multipart" id="form">
					
					<?php
					
						echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'First Name',
													'type'=>'text',
													'name'=>'first_name',
													'placeholder'=>'Your First Name'
													)
												);
					
						echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Last Name',
													'type'=>'text',
													'name'=>'last_name',
													'placeholder'=>'Your Last Name'
													)
												);
					
						

						echo $this->Form->input('mobile_number',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Mobile',
													'type'=>'text',
													'name'=>'mobile_number',
													'placeholder'=>'Enter Mobile'
													)
												);
						$num=1;
						echo $this->Form->input('address['.$num.']',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Address('.$num.')',
													'placeholder'=>'Enter Address',
													'style'=>'width: 1047px; height: 78px;',
													'name'=>'address['.$num.']',
													'type' =>'textarea',
													'value'=>''
													)
												);
															
						echo $this->Form->input('pincode',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'PinCode',
													'type'=>'text',
													'name'=>'pincode['.$num.']',
													'placeholder'=>'Enter PinCode',
													'value'=>''
													)
												); ?>
					<div id="add_button" class="pull-left btn btn-primary"><a href="javascript:void(0)" onclick="add_addr('<?php echo $num ?>')"><img src="<?php echo $this->webroot; ?>images/add.png" width="20px" />Add More Address</a></div>
				<br /><br />
					<h3>Login Details</h3>
                    
				  <?php
				  
					echo $this->Form->input('email',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Email address',
													'type'=>'email',
													'name'=>'email',
													'placeholder'=>'Enter Email'
													)
												);
				  
				  	echo $this->Form->input('password',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Password',
													'type'=>'password',
													'name'=>'password',
													'placeholder'=>'Password'
													)
												);
				  
				   echo $this->Form->input('conf_password',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Type Password Again',
													'type'=>'password',
													'name'=>'conf_password',
													'placeholder'=>'Confirm Password'
													)
												);
				   echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control select2',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'published',
																	'I'=>'unpublished'
																	)
																)
												);
				  ?>
				   <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
                  </div><!-- /.box-body -->

                 
				                    <div class="box-footer">
                   
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
            
	  
     <script type="javascript/text">
	 
	 $(function () {
    //Initialize Select2 Elements
        $(".select2").select2();
      });
    </script>
     