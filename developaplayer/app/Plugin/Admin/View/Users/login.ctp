﻿<div class="login-box">
			<?php if($this->Session->check('Message.flash')){?>
				<div class="alert alert-danger alert-dismissable">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<h4><i class="icon fa fa-ban"></i> Alert!</h4>
				<?php echo $this->Session->flash(); ?>   
			  </div>
			<?php }?>
  <div class="login-logo">
	<a href="javascript:void(0);"><b>Develop A Player</b></a>
  </div><!-- /.login-logo -->
  <div class="login-box-body">
	<p class="login-box-msg">Admin login</p>
	<?php 
		echo $this->Form->create("User", array('url'=>array('controller' =>'users', 'action' => 'login','plugin'=>'Admin'),'id'=>'Login'));  
	?>                         
					 <fieldset>
							<div class="form-group">
							<div class="input email">
							<label for="UserEmailAddress">Email Address</label>
						   <?php   
							echo $this->Form->input("email_address",array("label" => false, "div"=>false, "class" => false, "placeholder" => "E-mail", "class" => "form-control",'type'=>'email','autofocus'=>true)); 
						   ?>
						   </div>
							</div>
							<div class="form-group">
							<?php  
							echo $this->Form->input('password',array(
												'placeholder'=>'Password',
												'class'=>'form-control',
												'type'=>'password'
												  )
											  ); 
							?>
							
							</div>
						<?php  
						   echo $this->Form->input('login',array(
												'class'=>'btn btn-lg btn-success btn-block',
												'type'=>'submit',
												'label' => false
												
												  )
											  ); 
						   ?>
						</fieldset>
				   <?php echo $this->Form->end(); ?>

   
	<a href="<?php echo $this->webroot; ?>admin/users/forget_password">I forgot my password</a><br>
  

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->