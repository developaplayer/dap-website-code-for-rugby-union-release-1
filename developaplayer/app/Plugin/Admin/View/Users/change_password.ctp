<div class="content-wrapper">
<!-- Content Header (Page header) -->
	<?php echo $this->element('breadcrumb'); ?>

<!-- Main content -->
	<section class="content">
	<div class="row">
		<div class="col-xs-12">
		<div class="box-header with-border">
                  <h3 class="box-title">Input Admin Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
		<div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-inbox"></i>Change Password</li>
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <?php if($this->Session->check('Auth.User.type')=='A'){
								$type="Super Admin";
						  }else{
								$type="Moderator";
						  } 
					  $auth_data=$this->Session->read('Auth.User');
					  ?>
				  
                  <div class="chart tab-pane active" id="edit-profile" style="position: relative; height: 300px;">
					<!---------------edit Password xxxxx---------------- -->
				<form action="<?php echo $this->webroot; ?>admin/users/change_password" method="post" id="form">
						  <div class="form-group has-feedback col-lg-6">
							<input type="password" id="old_password" name="old_password" class=" required form-control " placeholder="Your Password"/>
						  </div>
						   <div class="clearfix"></div>
						  <div class="form-group has-feedback col-lg-6">
							<input type="password" id="password" name="password" class=" required form-control " placeholder="New Password"/>
						  </div>
						   <div class="clearfix"></div>
						  <div class="form-group has-feedback col-lg-6">
							<input type="password" id="conf_password" name="conf_password" class="form-control  required" placeholder="Confirm Password"/>
						  </div>
						  <div class="clearfix"></div>
						  <div class="col-xs-4">
							  <input type="submit" value="Update Password" class="btn btn-primary btn-block btn-flat" />
							</div>
						 </form>
				<!---------------end edit Password xxxx --------------------->
				  </div>
				  
				  
                </div>
              </div>
			  
			  </div>
			  </div>
			  </div>
			  