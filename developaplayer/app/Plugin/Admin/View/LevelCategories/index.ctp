<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Categories</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/level_categories/delete_category_multiple" enctype="multipart" id="delete_category">
				<?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.delete')=='Y'))
				{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
						  <br />
                  <table id="category_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.delete')=='Y'))
						{ ?>
								<th>Select</th>
						<?php } ?>
						
                        <th>Category</th>
						<th>Status</th>
                        <th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.edit')=='N' && $this->Session->read('permissions.Level_Category.status')=='N' && $this->Session->read('permissions.Level_Category.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					</tbody>
                    <tfoot>
                      <tr>
						 <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.delete')=='Y'))
						{ ?>
								<th>Select</th>
						<?php } ?>
                        <th>Category</th>
						<th>Status</th>
						<th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && $this->Session->read('permissions.Level_Category.edit')=='N' && $this->Session->read('permissions.Level_Category.status')=='N' && $this->Session->read('permissions.Level_Category.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	function table(){
			var dataTable = $('#category_table').DataTable({
				"dom": '<"row"<"col-md-5 pull-left add_input"><"pull-left error_message hide"><"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/level_categories/category_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".category_table-error").html("");
						$("#category_table").append('<tbody class="category_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#category_table_processing").css("display","none");
					}
				}
			});
			<?php 
				if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y' && ($this->Session->read('permissions.Level_Category.add')=='Y' || $this->Session->read('permissions.Level_Category.edit')=='Y')))
			{ ?>
			$(".add_input").html('<input style="width:63%" class="form-control" id="category_input" placeholder="Add a Category.." /><div onclick="add_categories()"  id="save" class="btn btn-default">Save</div>');
			<?php } ?>
	}
	
	function add_categories(){
		//alert();
		if($('#category_id').length > 0){
			var id = $('#category_id').val();
			update_categories(id);
		}else{
			$('.error_message').removeClass('hide');
			$('.error_message').html('<span style="color:green">Loading..</span>');
			var category_name = $('#category_input').val();
			$('#category_table_processing').show();
			//alert(category_name);
			$('#category_input').val('');
			$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/level_categories/add_category',
				data:{
					'category_name':category_name
				},
				success:function(response){
					if(response=="save_error"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:red">There was an unexpected error</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
					}else
						if(response=="empty_error"){
							$('.error_message').removeClass('hide');
							$('.error_message').html('<span style="color:red">Category name must not be empty</span>');
							setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
						}else
							if(response=="exist"){
								$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Category Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
							}else{
									$('.dataTables_empty').remove();
									$('#category_table').append('<tr>'+response+'</tr>');
									$('.error_message').removeClass('hide');
									$('.error_message').html('<span style="color:green">Category Added</span>');
									setTimeout(function(){
											$('.error_message').addClass('hide');
										},3000);
								}
					$('#category_table_processing').hide();
				}
			});
		}
	}
	
	function change_status(id,status){
		$('#category_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/level_categories/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#category_table_processing').hide();
					}
				/*setTimeout(function(){
					$('#message').hide();
				},2000);*/
			}
		});
	}
	
	function edit_data(id,category){
		//alert(category);
		//$('.category_name_'+id).val();
		if($('#cancel').length == 0){
			$('#category_input').val($('#category_name_'+id).text());
			$('#save').after('<div class="btn btn-default" onclick="cancel()" id="cancel">Cancel</div>');
			$('#save').after('<input type="hidden" id="category_id" value="'+id+'" />');
		}
	}
	
	function cancel(){
		$('#category_input').val('');
		$('#cancel').remove();
		$('#category_id').remove();
		//$('#save').prop('onclick','add_categories()');
	}
	
	function update_categories(id){
		//alert(id);
		$('.error_message').removeClass('hide');
		$('.error_message').html('<span style="color:green">Loading..</span>');
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/level_categories/update_category',
			data:{
				'id':id, 'category':$('#category_input').val()
			},
			success:function(response){
				if(response=="update_error"){
					$('.error_message').removeClass('hide');
					$('.error_message').html('<span style="color:red">Unexpected Error Occurred..</span>');
					setTimeout(function(){
							$('.error_message').addClass('hide');
						},3000);
				}else
					if(response=="success"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:green">Category Updated..</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},3000);
						$('#category_name_'+id).html($('#category_input').val());
						cancel();
					}
					else
						if(response=="exist"){
							$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Category Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
						}
			}
		});
	}
</script>
