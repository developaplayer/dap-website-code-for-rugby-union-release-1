<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
			   <form method="post" action="" enctype="multipart/form-data" id="form">
              <div class="box box-primary">
			  <!-- form start -->
               
				
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                 
				  <p style="padding-top:5px;"></p>
				  <!--<div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>-->
				  <p style="padding-top:5px;"></p>			
                </div>
				
		
	
	
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div style="background-color:#ccc; padding:5px;margin-bottom :5px;font-size: 150%"> Assign Certificate </div>	

<?php echo $this->Form->input('category',array('div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Category',
													'type'=>'select',
													'empty' => '--Select Category--',
													'name'=>'coach_category',
													'onchange'=>'get_course_data(this)',
													'data-placeholder'=>'Select Category',
													'style'=>'width:100%',
													'options'=>$all_coach_category_list
													)
												);
						 ?>
        <?php	echo $this->Form->input('course',array(
										'div'=>'form-group has-feedback',
										'class'=>'required form-control',
										'label' => 'Course',
										'type'=>'select',
										'empty' => '--Select Course--',
										'onchange'=>'create_all_data()',							
										'name'=>'course',
										'id'=>'booked_course_id',
										'data-placeholder'=>'Select Position',
										'style'=>'width:100%',
										//'options'=>$all_course_list
									)
								);	


							?>
		                <?php						
						echo $this->Form->input('age_group',array(
											'div'=>'form-group has-feedback age_group',
											'class'=>'required form-control',
											'label' => 'Age Group',
											'type'=>'select',
											'onchange'=>'create_all_data()',
											//'selected'=>$val,									
											'name'=>'age_group',
											'id'=>'age_group',
											'data-placeholder'=>'Select Age Group',
											'style'=>'width:100%',
											'options'=>''
										)
									);	
							 ?>		
	                        <?php			
								echo $this->Form->input('position',array(
										'div'=>'form-group has-feedback',
										'class'=>'required form-control',
										'label' => 'Position',
										'type'=>'select',
										'onchange'=>'payer_level()',									
										'name'=>'position',
										'id'=>'add_position_group',
										'data-placeholder'=>'Select Position',
										'style'=>'width:100%',
										'options'=>''
									)
								);	
							

echo $this->Form->input('certificate_date',array('div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'data-date-format'=>'YYYY-MM-DD',
													'label' => 'Achievement Date',
													'type'=>'text',
													'id'=>'achivement_date',
													'name'=>'achivement_date',
													'value'=>"",
													'placeholder'=>'YYYY-MM-DD',
													'style'=>'width:100%'
													)
												);
										
								echo $this->Form->input('certificate',array(
										'div'=>'form-group has-feedback',
										'class'=>'required form-control',
										'label' => 'certificate',
										'type'=>'select',
										//'selected'=>$val,									
										'name'=>'certificate_id',
										'id'=>'player_certification_level_id',
										'data-placeholder'=>'Select Position',
										'style'=>'width:100%',
										'options'=>''
									)
								);	
								
								


							?>
		
		<?php
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{
		?>
		
				<?php
				
				}
				
				?>
				
				<?php			
				$options=array("0"=>"Deactive","1"=>"Active");
				echo $this->Form->input('status',array(
					'div'=>'form-group has-feedback',
					'class'=>'required form-control',
					'label' => 'Status',
					'type'=>'select',															
					'name'=>'status',
					'value'=>0,
					'id'=>'add_position_group',
					'data-placeholder'=>'Select Status',
					'style'=>'width:100%',
					'options'=>$options
					)
				);								
				?>
				
			</div>
			
		
	          </div>		
					<br/>
				
					
					</div>				  
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>				  
				  <div class="box-footer">
                  </div>				  
                </form>
              </div>
		

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>

<script>
function create_all_data()
{
	var course_id = $('#booked_course_id').val();
	
	//==============Add Age Group==================================//
	if(course_id!="")
	{
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/player_profile/get_age_group',
				data:{
					'id':course_id
				},
				success:function(response){
					$('.age_group').html(response);
					
				},
				error:function(response){
					$('.age_group_div').html('');
					$('.age_group_div').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
				}
		});
		
		//==============get Course==================================//
		
		$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/player_profile/get_position',
		data:{
			'id':course_id
		},
		success:function(response){
			$('#add_position_group').html(response);
			
		},
		error:function(response){
			$('.add_position_group').html('');
			$('.add_position_group').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
		}
	});
	
   }
   
} 
	//========================get position=========================================//
	function payer_level(id)
      {	
	           var position_id =$('#add_position_group').val();
                $.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_player_level',
				data:{
					'id':position_id
				},
				success:function(response){
					$('#player_certification_level_id').html(response);
				}
		});	
	
}

function get_course_data(id)
      {	
	            var cat_id = id.value;
	            var payler_id= '<?php echo $this->request->params['pass'][0] ?>';
                $.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/player_profile/all_course_book',
				data:{
					'id':cat_id,'payler_id':payler_id
				},
				success:function(response){
					$('#booked_course_id').html(response);
				}
		});	
	
}


	$('#achivement_date').datetimepicker({
		//format: 'DD/MM/YYYY' // Disable Time Picker
		format: 'YYYY-MM-DD' // Disable Time Picker
	});

</script>
