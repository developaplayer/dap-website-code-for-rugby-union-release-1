<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>

<!--<script>
var url = 'https://www.youtube.com/watch?v=FhnMNwiGg5M';
var isyouTubeUrl = /((http|https):\/\/)?(www\.)?(youtube\.com)(\/)?([a-zA-Z0-9\-\.]+)\/?/.test(url);
alert(isyouTubeUrl);
</script>-->

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
			  <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                  <h3 class="box-title">Input Player Details</h3>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>
				  <p style="padding-top:5px;"></p>
				    
				  <?php 						
						echo $this->Form->input('email_address',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'readonly'=>'readonly',
													'label' => 'Username (email)',
													'type'=>'email',
													'name'=>'email_address',
													'value'=>$user['User']['email_address'],
													'placeholder'=>'Email'
													)
												);

						if($user['User']['status']=="A"){
							$options = array(array('name' => 'Active','value' => 'A','selected' => TRUE),'I'=>'InActive');
						}else{
							$options = array(array('name' => 'InActive','value' => 'I','selected' => TRUE),'A'=>'Active');
						}
						echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>$options
																)
															);
				  
				  ?>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Personal Information </div>
				  
				 
                </div><!-- /.box-header -->
              
				<!--- body --->
                  <div class="box-body">
					<?php
					
					echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Player First Name',
													'type'=>'text',
													'id'=>'first_name',
													'name'=>'first_name',
													'value'=>$user['User']['first_name'],
													'placeholder'=>'Player First Name'
													)
												);
												
					echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Player Last Name',
													'type'=>'text',
													'id'=>'last_name',
													'name'=>'last_name',
													'value'=>$user['User']['last_name'],
													'placeholder'=>'Player Last Name'
													)
												); ?>
					<div class="form-group has-feedback">
					<label for="dob">Date Of Birth</label>
						<div class='input-group date' id='datetimepicker1'>
							<input type="text" name="dob" class="form-control pickDate" value="<?php echo $user['PlayerDetail']['dob']; ?>" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<?php
					
					echo $this->Form->input('height',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Height (cm)',
													'type'=>'number',
													'id'=>'height',
													'name'=>'height',
													'value'=>$user['PlayerDetail']['height'],
													'placeholder'=>'Height'
													)
												);
												
					echo $this->Form->input('weight',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Weight (kg)',
													'type'=>'number',
													'id'=>'weight',
													'name'=>'weight',
													'value'=>$user['PlayerDetail']['weight'],
													'placeholder'=>'Weight'
													)
												);
												
					echo $this->Form->input('bench_press',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Bench Press (kg)',
													'type'=>'number',
													'id'=>'bench_press',
													'name'=>'bench_press',
													'value'=>$user['PlayerDetail']['bench_press'],
													'placeholder'=>'Bench Press'
													)
												);
												
					echo $this->Form->input('dead_lift',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Dead Lift (kg)',
													'type'=>'number',
													'id'=>'dead_lift',
													'name'=>'dead_lift',
													'value'=>$user['PlayerDetail']['dead_lift'],
													'placeholder'=>'Dead Lift'
													)
												);
												
					echo $this->Form->input('20m_sprint',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => '20m Sprint (set)',
													'type'=>'number',
													'id'=>'20m_sprint',
													'name'=>'20m_sprint',
													'value'=>$user['PlayerDetail']['20m_sprint'],
													'placeholder'=>'20m Sprint'
													)
												);
												
					echo $this->Form->input('60m_sprint',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => '60m Sprint (set)',
													'type'=>'number',
													'id'=>'60m_sprint',
													'name'=>'60m_sprint',
													'value'=>$user['PlayerDetail']['60m_sprint'],
													'placeholder'=>'60m Sprint'
													)
												);
															
					echo $this->Form->input('playing_club',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Playing Club',
													'type'=>'select',
													'name'=>'club_id',
													'data-placeholder'=>'Select Playing Club',
													'style'=>'width:100%',
													'options'=>array(
																	''=>'Loading Playing Club',
																	)
																)
															);
															
					echo $this->Form->input('primary_position_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Primary Position',
													'type'=>'select',
													'name'=>'primary_position_id',
													'data-placeholder'=>'Select Position',
													'style'=>'width:100%',
													'selected'=>$user['PlayerDetail']['primary_position_id'],
													
													'options'=>$playerPosition
																)
															);
															
					echo $this->Form->input('secondary_position_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Secondary Position',
													'type'=>'select',
													'name'=>'secondary_position_id',
													'data-placeholder'=>'Select Specialities',
													'empty'=>'Select Position',
													'selected'=> $user['PlayerDetail']['secondary_position_id'],
													'style'=>'width:100%',
													'options'=>$playerPosition
																)
															);
					
					echo $this->Form->input('nationality',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Nationality',
													'type'=>'select',
													'name'=>'nationality',
													'empty'=>'Select a Nationality',
													'options'=>array(
																	''=>'Loading Nationalities',
																	)
																)
															);
					echo '<img src="'.$this->webroot.'uploads/profiles/player/thumb/'.$user['PlayerDetail']['profile_img'].'" />';
					echo $this->Form->input('profile_img',array(
													'div'=>'form-group has-feedback',
													'class'=>'btn btn-primary',
													'label' => 'Profile Picture',
													'type'=>'file',
													'id'=>'profile_img',
													'style'=>array('width'=>'300px'),
													'name'=>'profile_img'
													)
												);
												
					echo "<output id='list'></output>";
					
					echo $this->Form->input('manage_by',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Manage By',
													'type'=>'text',
													'id'=>'manage_by',
													'name'=>'manage_by',
													'value'=>$user['PlayerDetail']['manage_by'],
													'placeholder'=>'Manage By'
													)
												);
					
					echo $this->Form->input('sponsor_by',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Sponsor By',
													'type'=>'text',
													'id'=>'sponsor_by',
													'name'=>'sponsor_by',
													'value'=>$user['PlayerDetail']['sponsor_by'],
													'placeholder'=>'Sponsor By'
													)
												);	
					
					$z = 1;
					$class = '';
					$div_id = 'video_default_div';
					//pr($user);
					
					foreach($user['PlayerVideo'] as $video_link){
						if($z!=1){
							$class = 'video_link_'.$z.$video_link['id'];
							$div_id = 'video_'.$z.$video_link['id'];
						} 
						if($video_link['video_type']==0)
						{
						?>
						
						
						<div id="<?php echo $div_id; ?>" class="form-group has-feedback <?php echo $class; ?>">
						<?php if($z==1){ ?>
							<label for="video_link_<?php echo $z.$video_link['id']; ?>">Video Link ( Youtube Link. Example: https://www.youtube.com/watch?v=1KqWm87BkAM)</label>
						<?php } ?>
							<input onblur='show_video("<?php echo $z.$video_link['id']; ?>")' type='text' id='video_link_<?php echo $z.$video_link['id']; ?>' placeholder='Video Link' value='<?php echo $video_link['video_link']; ?>' class='required form-control' name='video_link[]'>
							<?php if($z!=1){ ?>
									<a class="pull-right text-danger" onclick="delete_video('<?php echo $z.$video_link['id']; ?>')" href="javascript:void(0)">- Remove</a>
							<?php } ?>
						</div>
						<div id="youtube_video_<?php echo $z.$video_link['id']; ?>"><?php echo $video_link['video_iframe']; ?></div>
					<?php	$z++; } }
					
					echo '<span onclick="add_video(2)" id="add_videos" class="btn btn-primary">+ Add More Videos</span>';
					
					?>					
					
					</div>				  
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<script type="text/javascript">
function show_video(id){
	var link = $('#video_link_'+id).val();
	$('#youtube_video_'+id).html("Loading Video..");
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/load_video',
		data:{
			'link':$('#video_link_'+id).val()
		},
		success:function(response){
			$('#youtube_video_'+id).html(response);
		},
		error:function(response){
			$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
}

function add_video(num){
	var next_num = num+1;
	var total = $('div[class*="video_link_"]').length;
	var html_data = '<div class="form-group has-feedback video_link_'+num+'" id="video_'+num+'"><input onblur="show_video('+num+')" type="text" placeholder="Video Link" id="video_link_'+num+'" class="required form-control" name="video_link[]"><a href="javascript:void(0)" onclick="delete_video('+num+')" class="pull-right text-danger">- Remove</a></div"><div id="youtube_video_'+num+'"></div>';
	$('#add_videos').before(html_data);
	if((++total)==5){
		$('#add_videos').hide();
	}else{
		$('#add_videos').attr('onclick','add_video('+next_num+')');
	}
}

function delete_video(num){
	$('#add_videos').show();
	$('#video_'+num).remove();
	$('#youtube_video_'+num).remove();
}
function handleFileSelect(evt) {
	var files = evt.target.files;
	$('.hide_shown_image').hide();

	// Loop through the FileList and render image files as thumbnails.
	for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}
		
		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
		return function(e) {
			// Render thumbnail.
			var span = document.createElement('span');
			span.innerHTML = 
			[
			'<img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
			e.target.result,
			'" title="', escape(theFile.name), 
			'"/>'
			].join('');

			document.getElementById('list').insertBefore(span, null);
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
	}
}

document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);

$(function(){
	$('#password').after('<a class="text-danger password_toogle pull-right" onclick="show_password()" href="javascript:void(0)">show password</a>');
	$('label[for="dob"]').after('<br />');
	$(".select2").select2();
	//---------------  for year --------------------//
	var year = "";
	var today = new Date();
	var dateyear = today.getFullYear();
	for(var i=dateyear-16; i>dateyear-57; i--){
		year+="<option value="+i+">"+i+"</option>";
	}
	$('#dobYear').html(year);
	//---------------------------------------------//
	$('.ajax_error').remove();
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/player_profile/get_nationality',
		data:{
			'id':"<?php if(!empty($user['PlayerDetail']['nationality'])) { echo $user['PlayerDetail']['nationality'];} else { echo 'no'; } ?>"
		},
		success:function(response){
			//alert(response);
			$('#nationality').html(response);
		},
		error:function(response){
			$('#nationality').html('');
			$('#nationality').after('<span class="error ajax_error">There was an error while loading Nationality. Refresh the page.</span>');
		}
	});
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/player_profile/get_position',
		data:{
			'id':"<?php if(!empty($user['PlayerDetail']['primary_position_id'])){echo $user['PlayerDetail']['primary_position_id'];} else { echo 'no';} ?>"
		},
		success:function(response){
			//$('#primary_position_id').html(response);
		},
		error:function(response){
			$('#primary_position_id').html('');
			$('#primary_position_id').after('<span class="error ajax_error">There was an error while loading Primary Speciality. Refresh the page.</span>');
		}
	});
	
	
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/player_profile/get_position',
		data:{
			'id':"<?php if(!empty($user['PlayerDetail']['secondary_position_id'])){echo $user['PlayerDetail']['secondary_position_id'];} else { echo 'no';} ?>"
		},
		success:function(response){
			//$('#secondary_position_id').html(response);
		},
		error:function(response){
			$('#secondary_position_id').html('');
			$('#secondary_position_id').after('<span class="error ajax_error">There was an error while loading Secondary Speciality. Refresh the page.</span>');
		}
	});
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/player_profile/get_club',
		data:{
			'id':"<?php if(!empty($user['PlayerDetail']['club_id'])){echo $user['PlayerDetail']['club_id'];} else { echo 'no';} ?>"
		},
		success:function(response){
			$('#playing_club').html(response);
		},
		error:function(response){
			$('#playing_club').html('');
			$('#playing_club').after('<span class="error ajax_error">There was an error while loading Club. Refresh the page.</span>');
		}
	});

$('#datetimepicker1').datetimepicker({
		
		format: 'YYYY-MM-DD' // Disable Time Picker
	});
});
</script>