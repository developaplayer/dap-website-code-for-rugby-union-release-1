<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">View Certificate</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/coach_profile/delete_coach_multiple" enctype="multipart" id="delete_coach">						 				  <br />
                  <table id="coach_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					    <th>Category Name</th>
						 <th>Course Name</th>
						<th>AgeGroup</th>	
						<th>Position </th>	
						<th>Level Name </th>
                        <th></th>
                        <th></th> 						
											
                      </tr>
                    </thead>
                    <tbody>
					
					
					<?php 
					//echo '<pre>';
					//print_r($all_achive_certificate);
					//die;
				if(!empty($all_achive_certificate))
				{
					foreach($all_achive_certificate as $all_achive_certificates)
					{
					?>
				<tr>
					<td><?php echo $all_achive_certificates['CoachCategory']['category_name']; ?></td>
					<td><?php if(!empty($all_achive_certificates['Course']['course_title'])){ echo $all_achive_certificates['Course']['course_title']; } else echo "<b>Without Course</b>"; ?></td>
					<td><?php echo $all_achive_certificates['AgeGroup']['title']; ?></td>	
					<td><?php echo $all_achive_certificates['PlayerPosition']['name']; ?></td>	
					<td><?php echo 'Level'.$all_achive_certificates['Level']['level_number']; ?></td>
					<?php if($all_achive_certificates['AchiveCertificate']['status']==0)
					{
					  $active='Deactive';	
					}else
					{
						$active='Active';
					}
					?>
                    <td><a href="<?php echo $this->webroot ?>admin/player_profile/certificate_status/<?php echo $all_achive_certificates['AchiveCertificate']['id'] ?>/<?php echo $all_achive_certificates['AchiveCertificate']['status'];?>"><?php echo $active ?></a></td>
                    <td><a  href="<?php echo $this->webroot ?>admin/player_profile/delete_certificate/<?php echo $all_achive_certificates['AchiveCertificate']['id'] ?>"><strong>Delete</strong></a></td>					   
					<?php
					}
					
					}else{
					?>
					
                <td> No Certificate Found!</td>	
			</tr>	
				
					<?php } ?>	
                  				
					</tbody>
                    <tfoot>
                      <tr>
						 <?php if($this->Session->read('Auth.User.type')=='A'){	?>
								<th>Select</th>
						<?php }else{ ?>
						<?php if(!empty($permissions)){ ?>
							<?php foreach($permissions as $k=>$permission) {?>
								<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
											<th>Select</th>
								<?php }
								} 
							}
						} ?>
                       
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	
</script>
