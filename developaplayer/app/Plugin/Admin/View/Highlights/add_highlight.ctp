<style>
  .error_to(border:1px solid #ff0000 !important;)
</style>
<script>
function value_validate()
{
	var Youtube = document.getElementById("name").value;
	var video_title = document.getElementById("video_title").value;
	var own_video = document.getElementById("own_video").value;
	
	if(Youtube==""&&own_video==""){	
		$('#name').css( "border", "1px solid #F70414" );
		$('#own_video').css( "border", "1px solid #F70414" );
		return false;
	}
	else if(own_video!=""){
		if(video_title==""){
			$('#video_title').css( "border", "1px solid #F70414" );
			return false;
		}
	}
	
}
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Highlight Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form" onsubmit="javascript: return value_validate()">
				<?php //pr($all_category); exit; ?>
				<!--- body --->
				<div class="box-body">			 
					<label for="name">Upload Video</label>
						<?php											
						echo $this->Form->input('video_link',array(
														'div'=>'form-group has-feedback',
														'class'=>'form-control',
														'label' => false,
														'type'=>'text',
														'id'=>'name',										'placeholder'=>'Video URL'
														)
													);
													?>
							<label for="video_link_195">Video Link ( Youtube Link. Example: https://www.youtube.com/watch?v=1KqWm87BkAM)</label>							
				</div>	
						
						<div class="row vspace10">
                            	<div class="col-lg-12"><input type="text" name="video_title" id="video_title" class="form-control" placeholder="Video Title" ></div>				 
						</div>
						
						 <div class="row vspace10">
                            	<div class="col-lg-12"><input type="file" name="own_video" accept="mp4,WebM,Ogg,ogv" id="own_video"  >
								<label for="video_link_195">Video Upload(Only Mp4 Can be Uploaded.)</label>
								</div>
									
                          </div>
						
						
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
    <script type="text/javascript">
	
	var html="";
	var num = 1;
$(document).ready(function(){
	
	$("#add_size" ).click(function() {	
	var next_num = num++;
	//alert(next_num);
    var html=' <div id="size_id'+next_num+'"><div style="float:left; margin:0 5px 5px 0"><select onchange=select_size_value('+next_num+') name="product_size[]" id="select_size'+next_num+'" class="form-control"><option value="">---Select Product Size---</option> <?php foreach($all_Size as $key=>$all_Sizes){?><option value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option><?php } ?> </select></div><div style="float:left; margin:0 5px 5px 0">Quantity: <input type="number" onblur=add_qty('+next_num+')  name="qty[]" id="qty'+next_num+'" placeholder="Quantity" class="required"/></div><div style="float:left; margin:0 5px 5px 0;cursor:pointer" onclick=delete_size_qnty('+next_num+')><a href="#">X</a></div><div style="clear:both"></div></div>'; 									 	
	$('#size_quitity_div').append(html);							
								 
	 // $("#select_size'"+next_num+"' option[value='2']").attr('disabled','disabled');
	});
});

	function select_size_value(number)
	{
			//alert(number);
			
			var get_val=$('#select_size'+number).val();
			
			
        if( $('#size_quitity_div').find('select option[value='+get_val+']:selected').length>1)
		{
            alert('option is already selected');
			
            $('#select_size'+number).val('');   
        }
 
	}
		
	function add_qty(number)
	{		var sum = 0;		
		  $("#size_quitity_div input[type=number]").each(function()
		  {
              //var input = $(this).val(); 
               sum += Number($(this).val());   			  
          });
		   $('#total_qty').val(sum);
		//alert(sum);
	}
	
		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/><p><input style="margin-left:40px" type="radio" name="default_image"  id="default_image" value="'+escape(theFile.name)+'"/></p></div>'].join('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);

$('document').ready(function(){
	CKEDITOR.replace('description');
	$('#description').before('<label for="description">Content</label>');
	$('#meta_description').before('<label for="meta_description">Meta Description</label>');
});

function delete_size_qnty(delete_size_qnty)
{
	$('#size_id'+delete_size_qnty).remove();
	
}
  </script>
  