<?php //pr($product_data); exit;?>
 
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Highlight Videos</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
				<div class="box-body">
					<label for="name">Upload Video</label>
						<?php if($edit['Highlight']['type']=="Youtube"){?>
						<input type="text" name="video_link" value="<?php echo $edit['Highlight']['video_link'] ?>" class="required form-control" id="title" placeholder="Video Link">    
						<?php } else {?>
						<input type="text" name="video_title" value="<?php echo $edit['Highlight']['video_title'] ?>" class="required form-control" id="video_title" placeholder="Video Title"> 
						<input type="file" name="own_video" accept="mp4,WebM,Ogg,ogv" id="own_video" placeholder="Video Link">
						<?php }?>               					
				</div>
				
				<div class="box-body">
					<?php echo $edit['Highlight']['video_iframe'] ?>                                     				
				</div>
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
    <script type="text/javascript">
		
		$(document).ready(function(){
		
		var sum = 0;		
		  $("#size_quitity_div input[type=number]").each(function()
		  {			  
              //var input = $(this).val(); 
               sum += Number($(this).val());  
			 // alert(sum); 
          });
		   $('#current_qty').val(sum);
		
		});
		function handleFileSelect(evt) {
			var files = evt.target.files;
			//$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			var g=0;
			for (var i = 0, f; f = files[i]; i++) {
				
                     //alert(i);
				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					count =g++;
					//alert(count);
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div id="new_product_image'+count+'" style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/><p><input style="margin-left:40px" type="radio" name="default_image"  id="default_image" value="'+escape(theFile.name)+'"/><span style="padding-left:4px"><a onclick="new_remove_image('+count+')"; style="cursor:pointer;" >X</a></span></p></div>'].join('');
					//document.getElementById('list').insertBefore(span, null);
					$('#list').append(span);	
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
				g++;
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);

		//================================Edit Size Qty====================================
		
		//===================edit qty size================================	
var html="";
var num = '<?php echo count($product_size_qties); ?>';
$(document).ready(function(){
	
	$("#add_size" ).click(function() {	
	var next_num = num++;
	//alert(next_num);
    var html=' <div id="size_id'+next_num+'"><div style="float:left; margin:0 5px 5px 0"><select onchange=select_size_value('+next_num+') name="product_size[]" id="select_size'+next_num+'" class="form-control"><option value="">---Select Product Size---</option> <?php foreach($all_Size as $key=>$all_Sizes){?><option value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option><?php } ?> </select></div><div style="float:left; margin:0 5px 5px 27px">New Quantity: <input type="number" onblur=add_qty('+next_num+')  name="qty[]" id="qty'+next_num+'" placeholder="Quantity" class="required"/></div><div style="float:left; margin:0 5px 5px 0" onclick=delete_size_qnty('+next_num+')><a href="#">X</a></div><div style="clear:both"></div></div>'; 									 	
	$('#size_quitity_div').append(html);							
								 
	 // $("#select_size'"+next_num+"' option[value='2']").attr('disabled','disabled');
	});
});

	function select_size_value(number)
	{
	   var get_val=$('#select_size'+number).val();	
        if( $('#size_quitity_div').find('select option[value='+get_val+']:selected').length>1)
		{
            alert('option is already selected');			
            $('#select_size'+number).val('');   
        }
 
	}
		
	function add_qty(number)
	{		var sum = 0;		
		  $("#size_quitity_div input[type=number]").each(function()
		  {			  
              //var input = $(this).val(); 
               sum += Number($(this).val());  
			 // alert(sum); 
          });
		   $('#current_qty').val(sum);
		   
		      var total_stock='<?php echo $product_data['Product']['total_qty']; ?>';
			  if(total_stock>sum)
			  {
				 var increasestock =(parseInt(total_stock)-parseInt(sum)); 
			  }else
			  {
                var increasestock =(parseInt(sum)-parseInt(total_stock));				  
			  }
		     
			 var total_sale_qty_count= '<?php echo $total_sale_qty_count; ?>';
			 var totalincreasestock =(parseInt(sum)+parseInt(total_sale_qty_count));
			  $('#total_qty').val(totalincreasestock);
		      //alert(totalincreasestock);
		
	}
	
	function delete_size_qnty(delete_size_qnty)
	{
		$('#size_id'+delete_size_qnty).remove();
		
	}
	
	
	//============Previous Image=======================//
	function remove_image(imageId)
	{
		$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/products/remove_image',
		data:{
			'id': imageId
		},
		success:function(response){
			
			 $('#image_span_id'+imageId).animate({
			opacity: 0.25,
			//left: "+=50",
			height: "toggle"
		  }, 1000, function() {
			// Animation complete.
			$('#image_span_id'+imageId).remove();
		});
				
			
			
		//	$('.update_certificate_field').html(response);
			//$(".select2").select2();
		},
		error:function(response){
			//$('#coach_certificate').html('');
			//$('.update_certificate_field').after('<span class="error ajax_error">There was an error while loading Ceritificates. Refresh the page.</span>');
		}
	});
		
		
		
		
	}
	
	//================new add image==================================//
	
	function new_remove_image(divId)
	{
		
		 $('#new_product_image'+divId).animate({
			opacity: 0.25,
			//left: "+=50",
			//height: "toggle"
		  }, 5000, function() {
			// Animation complete.
			$('#new_product_image'+divId).remove();
		});
		//alert(divId);
		
		
	}

	  </script>