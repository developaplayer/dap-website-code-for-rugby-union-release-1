      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Contact header) -->
        <section class="content-header">
          <h1>
            Highlight Video Details
            <small>advanced tables</small>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Videos</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				
				<form method="post" action="<?php echo $this->webroot; ?>admin/highlights/highlight_multiple_delete" enctype="multipart" id="delete_UnitType">
                  
				  <?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.delete')=='Y')))
				{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
					<table id="example1" class="table table-bordered table-striped">
				 
                    <thead>
						<?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
						<th>Video Title</th>						
						<th>Video</th>                                               
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.edit')=='N' && $this->Session->read('permissions.Highlight_Video.status')=='N' && $this->Session->read('permissions.Highlight_Video.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>                     
                    </thead>
                    <tbody>
					
					<?php
					 foreach($view_content as $k=>$highlight){ ?>
					<tr>
					 <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.delete')=='Y'))
						{ ?>
                      <td>
					  <input class="checkbox_del" name="id[]" value="<?php echo $highlight['Highlight']['id'] ?>" type="checkbox" />
					  </td>					  					  
						<?php } ?>
						<td><?php echo $highlight['Highlight']['video_title'] ?></td>
						
						<td><?php if($highlight['Highlight']['type']=="Youtube"){?>
						<img src="<?php echo $highlight['Highlight']['video_image'] ?>" style="height:100px;width:100px" ><?php } else { ?> <video width="100" height="100">
						<img src="<?php echo $highlight['Highlight']['video_iframe'] ?>" alt="" />
						</video><?php } ?></td>      
						<td style="width:118px !important">
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.status')=='Y' && $this->Session->read('permissions.Highlight_Video.edit')=='Y'  && $this->Session->read('permissions.Highlight_Video.delete')=='Y'))
						{ ?>
						<a href="<?php echo $this->webroot; ?>admin/highlights/edit/<?php echo base64_encode($highlight['Highlight']['id']); ?>">						
						<img alt="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>
						admin/admin_css_js/dist/img/edit.png" /></a>
						&nbsp;												
						<a href="<?php echo $this->webroot; ?>admin/highlights/delete_page/<?php echo base64_encode($highlight['Highlight']['id']); ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						
						<a href= "<?php echo $this->webroot; ?>admin/highlights/status/<?php echo base64_encode($highlight['Highlight']['id']); ?>/<?php echo $highlight['Highlight']['status']; ?>" title="<?php if($highlight['Highlight']['status'] == 'I'){ ?>Inactive<?php }else{ ?>Active<?php } ?>" id=""><img = alt="Inactive" width="20px" src="<?php echo $this->webroot; ?> admin/img/icons/<?php if($highlight['Highlight']['status'] == 'I'){ ?>lock<?php }else{ ?>unlock<?php } ?>.png"></a>
						<?php } ?>
						<?php  
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.status')=='Y')
						{ ?>
						<a href= "<?php echo $this->webroot; ?>admin/highlights/status/<?php echo base64_encode($highlight['Highlight']['id']); ?>/<?php echo $highlight['Highlight']['status']; ?>" title="<?php if($highlight['Highlight']['status'] == 'I'){ ?>Inactive<?php }else{ ?>Active<?php } ?>" id=""><img = alt="Inactive" width="20px" src="<?php echo $this->webroot; ?> admin/img/icons/<?php if($highlight['Highlight']['status'] == 'I'){ ?>lock<?php }else{ ?>unlock<?php } ?>.png"></a>
						<?php } ?>
						<?php if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.edit')=='Y')
						{?>
						<a href="<?php echo $this->webroot; ?>admin/highlights/edit/<?php echo base64_encode($highlight['Highlight']['id']); ?>">						
						<img alt="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>
						admin/admin_css_js/dist/img/edit.png" /></a>
						<?php } ?>
						<?php if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y' && $this->Session->read('permissions.Highlight_Video.delete')=='Y')
						{ ?>
						<a href="<?php echo $this->webroot; ?>admin/highlights/delete_page/<?php echo base64_encode($highlight['Highlight']['id']); ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						<?php } ?>
						</td>
						
						
					</tr>

					<?php } ?>
                    </tbody>
                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->      