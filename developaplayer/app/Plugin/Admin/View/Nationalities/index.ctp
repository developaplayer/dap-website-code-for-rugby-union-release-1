<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Nationality</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
                  <table id="nationality_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
						
                        <th>Nationalities</th>
						<th>Status</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.edit')=='N' && $this->Session->read('permissions.Nationality.status')=='N' && $this->Session->read('permissions.Nationality.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					</tbody>
                    <tfoot>
                      <tr>
                        <th>Nationalities</th>
						<th>Status</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.view')=='Y' && $this->Session->read('permissions.Nationality.edit')=='N' && $this->Session->read('permissions.Nationality.status')=='N' && $this->Session->read('permissions.Nationality.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	function table(){
			var dataTable = $('#nationality_table').DataTable({
				"dom": '<"row"<"col-md-5 pull-left add_input"><"pull-left error_message hide"><"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/nationalities/nationality_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".nationality_table-error").html("");
						$("#nationality_table").append('<tbody class="nationality_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#nationality_table_processing").css("display","none");
					}
				}
			});
			<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Nationality.add')=='Y')) { ?>
			$(".add_input").html('<input style="width:63%" class="form-control" id="nationality_input" placeholder="Add a Nation.." /><div onclick="add_nationality()"  id="save" class="btn btn-default">Save</div>');
			<?php } ?>
	}
	
	function add_nationality(){
		//alert();
		if($('#nationality_id').length > 0){
			var id = $('#nationality_id').val();
			update_nationality(id);
		}else{
			$('.error_message').removeClass('hide');
			$('.error_message').html('<span style="color:green">Loading..</span>');
			var nationality_name = $('#nationality_input').val();
			$('#nationality_table_processing').show();
			//alert(nationality_name);
			$('#nationality_input').val('');
			$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/nationalities/add_nationality',
				data:{
					'nationality_name':nationality_name
				},
				success:function(response){
					if(response=="save_error"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:red">There was an unexpected error</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
					}else
						if(response=="empty_error"){
							$('.error_message').removeClass('hide');
							$('.error_message').html('<span style="color:red">Nationality name must not be empty</span>');
							setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
						}else
							if(response=="exist"){
								$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Nationality Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
							}else{
									$('.dataTables_empty').remove();
									$('#primary_table').append('<tr>'+response+'</tr>');
									$('.error_message').removeClass('hide');
									$('.error_message').html('<span style="color:green">Nation Added</span>');
									setTimeout(function(){
											$('.error_message').addClass('hide');
										},3000);
								}
					$('#nationality_table_processing').hide();
				}
			});
		}
	}	
	
	function change_status(id,status){
		$('#nationality_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/nationalities/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#nationality_table_processing').hide();
					}
			}
		});
	}
	
	function edit_data(id,nationality){
		//alert(level);
		//$('.level_name_'+id).val();
		if($('#cancel').length == 0){
			$('#nationality_input').val($('#nationality_name_'+id).text());
			$('#save').after('<div class="btn btn-default" onclick="cancel()" id="cancel">Cancel</div>');
			$('#save').after('<input type="hidden" id="nationality_id" value="'+id+'" />');
		}
	}
	
	function cancel(){
		$('#nationality_input').val('');
		$('#cancel').remove();
		$('#nationality_id').remove();
		//$('#save').prop('onclick','add_levels()');
	}
	
	function update_nationality(id){
		//alert(id);
		$('.error_message').removeClass('hide');
		$('.error_message').html('<span style="color:green">Loading..</span>');
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/nationalities/update_nationality',
			data:{
				'id':id, 'nationality_name':$('#nationality_input').val()
			},
			success:function(response){
				if(response=="update_error"){
					$('.error_message').removeClass('hide');
					$('.error_message').html('<span style="color:red">Unexpected Error Occurred..</span>');
					setTimeout(function(){
							$('.error_message').addClass('hide');
						},3000);
				}else
					if(response=="success"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:green">Nation Updated..</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},3000);
						$('#nationality_name_'+id).html($('#nationality_input').val());
						cancel();
					}
					else
						if(response=="exist"){
							$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Nation Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
						}
			}
		});
	}
</script>
