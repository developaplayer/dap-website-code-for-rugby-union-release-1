<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php //echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
			   <form method="post" action="" enctype="multipart/form-data" id="form">
              <div class="box box-primary">
			  <!-- form start -->
               
				
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                  <h3 class="box-title">Input Player Rating</h3>
				  <p style="padding-top:5px;"></p>
				  <!--<div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>-->
				  <p style="padding-top:5px;"></p>			
                </div>
		
	<?php
	
$k=1;
for($z=1;$z<=count($AgeGroup_list);$z++)
{
	if($k<2)
    {
	if(!empty($all_rating_list[$k-1]['AddCoachRating']['age_group_id']))
	{
		$val=$all_rating_list[$k-1]['AddCoachRating']['age_group_id'];
	}
	else
	{
		$val=0;
	}
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div style="background-color:#ccc; padding:5px;margin-bottom :5px;font-size: 150%">Rating Information </div>
<?php			
			echo $this->Form->input('Position',array(
					'div'=>'form-group has-feedback',
					'class'=>'required form-control',
					'label' => 'Position',
					'type'=>'select',
					'selected'=>$val,
                    'onchange'=>'upper_lower_limit()',						
					'name'=>'position_'.$z,
					'empty'=>'--Select Position---',
					'default'=>'Select Position',
					'style'=>'width:100%',
					'options'=>$PlayerPosition
				)
			);	
			
			echo $this->Form->input('age_group',array(
					'div'=>'form-group has-feedback',
					'class'=>'required form-control',
					'label' => 'Age Group',
					'type'=>'select',
					'selected'=>$val,
                    'onchange'=>'upper_lower_limit()',					
					'name'=>'age_group_'.$z,
					'empty'=>'--Select Age Group--',
					'style'=>'width:100%',
					'options'=>$AgeGroup_list
				)
			);
             
			 	echo $this->Form->input('lower_limit',array(
					'div'=>'form-group has-feedback update_certificate_field',
					'class'=>'required form-control',
					'label' => 'lower Limit',
					'type'=>'text',
					'id'=>'lower_limit',
					'readonly'=>'readonly',
					'name'=>'lower_limit_'.$z,
					'data-placeholder'=>'Lower Limit',
					'style'=>'width:100%',					
				)
			);
			
			echo $this->Form->input('upper_limit',array(
					'div'=>'form-group has-feedback update_certificate_field',
					'class'=>'required form-control',
					'label' => 'Upper Limit',
					'type'=>'text',
					'id'=>'upper_limit',
					'readonly'=>'readonly',
					'name'=>'upper_limit_'.$z,
					'data-placeholder'=>'Upper Limit',
					'style'=>'width:100%',					
				)
			);
                			
			/*echo $this->Form->input('upper_lower_limit',array(
					'div'=>'form-group has-feedback update_certificate_field',
					'class'=>'required form-control',
					'label' => 'lower Upper Limit',
					'type'=>'select',
					'name'=>'upper_lower_limit_'.$z,
					'selected'=>isset($all_rating_list[$k-1]['AddCoachRating']['rating_limit_id'])  ? $all_rating_list[$k-1]['AddCoachRating']['rating_limit_id'] : "",
					'data-placeholder'=>'Select Upper lower Limit',
					'style'=>'width:100%',
					'options'=>$UpperLowerLimit_list,
				)
			);*/		
		?>					
		<?php
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{
		?>
		<div class=" col-lg-2 col-md-3 col-sm-3 col-xs-12">
		<?php					
					echo $this->Form->input('coach_category',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Label Category',
													'type'=>'select',
													'selected'=>$i,
													'disabled'=>'disabled',
													'name'=>'coach_category_'.$k,
													'data-placeholder'=>'Select Category',
													'style'=>'width:100%',
													'options'=>$all_coach_category_list
													)
												);
					echo $this->Form->input('coach_category_hidden',array('label' => false,'hidden'=>'hidden','value'=>$i,'name'=>'coach_category_hidden_'.$k,));				
					
					echo $this->Form->input('rating_val_'.$i,array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Rating',
													'type'=>'text',
													'onblur'=>'checklimit(\'rating_val_'.$i.'\')',
													'value'=>isset($all_rating_list[$k-1]['AddCoachRating']['rating'])  ? $all_rating_list[$k-1]['AddCoachRating']['rating'] : "",
													'name'=>'rating_val_'.$k
																)
														);
														?>
					</div>
				<?php
				$k++;
				}
               }
				?>
				
			</div>
			
			<?php	
		}
		?>
	</div>		
					<br/>
				
					
					</div>				  
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>				  
				  <div class="box-footer">
                  </div>				  
                </form>
              </div>
		

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<script>
function upper_lower_limit()
{
	
	
	var position_id=$('#Position').val();
	var group_id=$('#age_group').val();
	
		if(position_id!="" && group_id!="")
		{
			$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/player_profile/get_upper_lower_limit',
					data:{
						'position_id':position_id,'group_id':group_id
					  },
					success:function(response){
						//alert(response);
						var valData= response;
                        var valNew=valData.split('|');
						$('#lower_limit').val(valNew[0]);
						$('#upper_limit').val(valNew[1]);
						
					},
					error:function(response){
						//$('.update_coach_field').html('');
						//$('.update_coach_field').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
					}
				});
	 }
}

function checklimit(id){
	$('#limitError').remove();
	if(/\S/.test($('#'+id).val())){
		if(/\S/.test($('#lower_limit').val())){
			var lower_limit = $('#lower_limit').val();
			var upper_limit = $('#upper_limit').val();
			if(parseFloat($('#'+id).val()) < lower_limit || parseFloat($('#'+id).val()) > upper_limit){
				$('#'+id).val('');
					$('#'+id).after('<span id="limitError" class="text-danger">Rating Must Be Greater Than '+lower_limit+' Or Less Then '+upper_limit+'</span>');
			}
		}else{
			$('#'+id).val('');
		}
	}
}
</script>
