<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Rating List</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/coach_profile/delete_coach_multiple" enctype="multipart" id="delete_coach">
						  <input class="selectall" type="checkbox" />
						  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
						  <br />
                  <table id="coach_table" class="table table-bordered table-striped">
                    <thead>
                    <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A'){	?>
								
						<?php }else{ ?>
						<?php if(!empty($permissions)){ ?>
							<?php foreach($permissions as $k=>$permission) {?>
								<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
										
								<?php }
								} 
							}
						} ?>						
                        <th>Position</th>
                        <th>Group</th>
						<th>Category</th>
                        <th>Rating Value</th>												
                      </tr>
                    </thead>
                    <tbody>
					<tr>
					<?php 
				if(!empty($all_rating_list))
				{
					foreach($all_rating_list as $all_rating_lists)
					{
					?>
			   <tr>
				    <td><?php echo $all_rating_lists['PlayerPosition']['name']; ?></td>						
					<td><?php echo $all_rating_lists['AgeGroup']['title']; ?></td>	
					<td><?php echo $all_rating_lists['CoachCategory']['category_name']; ?></td>
					<td><?php echo $all_rating_lists['AddPlayerRating']['rating']; ?></td>												
					<?php
				     	}					
					}else{
					?>					
                   <td> No Ratings Found!</td>	
			  </tr>				
					<?php } ?>				
			</tr>
					</tbody>
                    <tfoot>
                      <tr>
						 <?php if($this->Session->read('Auth.User.type')=='A'){	?>
							
						<?php }else{ ?>
						<?php if(!empty($permissions)){ ?>
							<?php foreach($permissions as $k=>$permission) { ?>
								<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['delete_permission']=='Y'){ ?>
										
								<?php }
								}
							}
						} ?>
                       <th>Position</th>
                        <th>Group</th>
						<th>Category</th>
                        <th>Rating Value</th>	
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	/*function table(){
			var dataTable = $('#coach_table').DataTable({
				"dom": '<"row"<"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/player_profile/player_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".coach_table-error").html("");
						$("#coach_table").append('<tbody class="coach_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#coach_table_processing").css("display","none");
					}
				}
			});
	}*/
	
	function change_status(id,status){
		$('#coach_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/player_profile/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#coach_table_processing').hide();
					}
			}
		});
	}
</script>
