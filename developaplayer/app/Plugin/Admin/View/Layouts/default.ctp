<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html> 
<head>
	<?php echo $this->Html->charset('utf-8'); ?>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>	DevelopAPlayer :<?php echo $title_for_layout; ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>images/FAVICON.png"/>
	<?php
		echo $this->Html->css('/admin/admin_css_js/bootstrap/css/bootstrap.min');
		echo $this->Html->css('/admin/admin_css_js/dist/css/AdminLTE.min');
		echo $this->Html->css('/admin/admin_css_js/dist/css/skins/_all-skins.min');
		echo $this->Html->css('/admin/admin_css_js/plugins/datatables/dataTables.bootstrap');
		echo $this->Html->css('/admin/fancybox_2/source/jquery.fancybox.css?v=2.1.5');
		echo $this->Html->css('/admin/css/ionicons.min.css');
		echo $this->Html->css('/admin/css/font-awesome.min.css');
		//script
		echo $this->Html->script('/admin/js/jquery-1.11.1.min.js');
		echo $this->Html->script('/admin/js/jquery-ui.min.js');
		echo $this->Html->script('/admin/admin_css_js/bootstrap/js/bootstrap.min');
		echo $this->Html->script('/admin/js/script.js');
		//echo $this->Html->script('/admin/js/ckeditor/ckeditor.js');
		echo $this->Html->script('/admin/admin_css_js/dist/js/app.min');
		echo $this->Html->script('/admin/js/jquery.validate.min.js');
		echo $this->Html->script('/admin/admin_css_js/plugins/chartjs/Chart.min');
		echo $this->Html->script('/admin/admin_css_js/plugins/jQuery/jquery.validate');
		echo $this->Html->script('/admin/admin_css_js/plugins/datatables/jquery.dataTables.min'); 
		echo $this->Html->script('/admin/admin_css_js/plugins/datatables/dataTables.bootstrap.min');
		echo $this->Html->script('/admin/fancybox_2/source/jquery.fancybox.pack.js?v=2.1.5');
	?>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!-- [endif] -->
	
	<script type="text/javascript">
      $.widget.bridge('uibutton', $.ui.button);
		base_url='<?php echo $this->webroot; ?>';
			
			$(document).ready(function() {
		$(".fancybox").fancybox();
	});
	
	 function add_addr(id){
		 //alert(id);
		 id++;
			 $('#add_button').before('<div class="form-group has-feedback"><label>Address('+id+')</label><input style="width: 1047px; height: 78px;" name="address['+id+']" type="text" id="'+id+'" class="form-control" placeholder="Enter Address" /></div>');
			 $('#add_button').before('<div class="form-group has-feedback"><label>PinCode</label><input type="text" name="pincode['+id+']" placeholder="Enter Pincode" class="form-control" /></div>');
			 $('#add_button').html('<a href="javascript:void(0)" onclick="add_addr('+id+')"><img src="<?php echo $this->webroot; ?>images/add.png" width="20px" />Add More Address</a>');
		 
		 if(id>2){
			$('#add_button').hide();
		 }
		 //alert(id);
	 }
			
		$(".various").fancybox({
		maxWidth	: 400,
		maxHeight	: 700,
		fitToView	: false,
		width		: '70%',
		height		: '65%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
	function fancy_box(cls,email){
		$("."+cls).fancybox({
			maxWidth	: 400,
			maxHeight	: 750,
			fitToView	: false,
			width		: '70%',
			height		: '75%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
			type: 'iframe',
			href: 'send_mail/'+email
		});
	}
	
	function fancy_order_box(id){
		//alert(id);
		$("#"+id).fancybox({
			maxWidth	: 400,
			fitToView	: false,
			width		: '70%',
			height		: '80%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
			type: 'iframe',
			href: 'order_details/'+id
		});
	}
	
	function mass_mail(){
		$(".mass_mail").fancybox({
			maxWidth	: 400,
			maxHeight	: 750,
			fitToView	: false,
			width		: '70%',
			height		: '75%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
			type: 'iframe',
			href: 'send_mass_mail'
		});
	}
			
			
		function delete_image(id,code,url_link)
		{
			if(confirm('Are You Sure You Want To Delete?')==true)
			{
				$.ajax({
					type:"post",
					url:url_link,
					data:{
						'id':id,'code':code
					},
					success:function(data){
						$('.'+code).hide(); //hides image
						$('#message').show(); //shows a success message
						setTimeout(function(){
							$('#message').hide();
						},2000); //after the given second the success message hides
					}
				});
			}
		}
		
		function delete_all_image(id,code,url_link)
		{
			if(confirm('Are You Sure You Want To Delete?')==true)
			{
				for(var i=0;i<id;i++){
					alert(id[i]);
				}
				$.ajax({
					type:"post",
					url:url_link,
					data:{
						'id':id,'code':code
					},
					success:function(data){
						$('#'+code).hide(); //hides image
						$('#message').show(); //shows a success message
						setTimeout(function(){
							$('#message').hide();
						},2000); //after the given second the success message hides
					}
				});
			}
		}
		
		$(document).ready(function() {
			$('.selectall').click(function(event) {  //on click
				if(this.checked) { // check select status
					$('.checkbox_del').each(function() { //loop through each checkbox
						this.checked = true;  //select all checkboxes with class "checkbox1"              
					});
				}else{
					$('.checkbox_del').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                      
					});        
				}
			});
		});
			
			
		$(document).ready(function() {
			$('.selectallhistory').click(function(event) {  //on click
				if(this.checked) { // check select status
					$('.historycheckbox_del').each(function() { //loop through each checkbox
						this.checked = true;  //select all checkboxes with class "checkbox1"              
					});
				}else{
					$('.historycheckbox_del').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                      
					});        
				}
			});
			
			
		   
		});
			
	</script>
	
	<style>
	#popup-content { display:none; text-align:center}
		.shadow_left{
			-webkit-box-shadow: -6px -1px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: -6px -1px 5px 0px rgba(0,0,0,0.75);
			box-shadow: -6px -1px 5px 0px rgba(0,0,0,0.75);
		}
		
		.shadow_right{
			-webkit-box-shadow: 8px 2px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: 8px 2px 5px 0px rgba(0,0,0,0.75);
			box-shadow: 8px 2px 5px 0px rgba(0,0,0,0.75);
		}
		
		#img_table a {
			float: left;
			margin-right: 10px;
			position: relative;
			vertical-align: top;
		}
		.error{
			color:red;
		}
	</style>
	<!--/cache-->
</head>
<body class="skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">
        <!-- header -->
			  <?php echo $this->element('header'); ?>
		<!-- left menu -->
		 <?php echo $this->element('left-panel'); ?>
		<!-- Main Content -->
		<?php echo $this->fetch('content'); ?>
			   <!-- footer -->
		<?php echo $this->element('footer'); ?>
    </div>
	 <div class="control-sidebar-bg"></div>
    </div>

	 <!-- page script -->
    <script type="text/javascript">
      $(function () {
		  //data table functionj
			$("#example1").DataTable({
				 "sDom": '<"H"flr>t<"F"ip>',
				"pageLength": 100,
				 "info": true,
				"lengthChange": true,
				 "stateSave": true
			});
		
		$(document).on( 'init.dt', function ( e, settings ) {
			var api = new $.fn.dataTable.Api( settings );
			var state = api.state.loaded();
		});
	
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
	});
	  
	  //validate function
		$(document).ready(function() 
		{
			$('#form').validate();
		});
		
		//validate function
		$("#form").validate({
			rules: {                   
				password :"required",
			conf_password:{
				equalTo: "#password"
				},
			mobile:{
				number: true
				},
			number:{
				number: true
				},
			pincode:{
				number: true
				},
			unit:{
				number: true
				},
			price:{
				number: true
				},
			qty:{
				number: true
				}
			},                             
			messages: {
				password :" Enter Password",
				conf_password :" Enter Confirm Password Same as Password",
				mobile :" Enter A Vaild Number",
				number :" Enter A Vaild Number",
				unit :" Enter A Vaild Unit",
				price :" Enter A Vaild Price",
				qty :" Enter A Vaild Quantity",
				pincode :" Enter A Vaild Pincode"
			}
		});
    </script>

</body>

</html>
