﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Develop A Player | Admin Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>images/FAVICON.png"/>
   
   	<?php echo $this->Html->css('/admin/admin_css_js/plugins/jvectormap/jquery-jvectormap-1.2.2'); ?>
	<?php echo $this->Html->css('/admin/admin_css_js/plugins/datatables/dataTables.bootstrap'); ?>
    <!-- Theme style -->
	<?php echo $this->Html->css('/admin/admin_css_js/dist/css/AdminLTE.min'); ?>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
	<?php echo $this->Html->css('/admin/admin_css_js/dist/css/skins/_all-skins.min'); ?>
   
   
	<?php echo $this->Html->css('/admin/admin_css_js/bootstrap/css/bootstrap.min'); ?>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    <!-- jvectormap -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	
	<?php //echo $this->Html->script('/admin/assets/plugins/jquery-1.10.2');  ?>
	<?php echo $this->Html->script('/admin/admin_css_js/plugins/jQuery/jQuery-2.1.4.min');  ?>
	<!-- jQuery jquery.validate -->
	<?php echo $this->Html->script('/admin/admin_css_js/plugins/jQuery/jquery.validate');  ?>
	<!-- Bootstrap 3.3.2 JS -->
	<?php echo $this->Html->script('/admin/admin_css_js/bootstrap/js/bootstrap.min');  ?>

	<!-- iCheck -->
	<?php echo $this->Html->script('/admin/admin_css_js/plugins/iCheck/icheck.min');  ?>

	<script type="text/javascript" language="javascript">
	$(document).ready(function() 
	{ 
		$('#qure_admin_login').validate();
	});
	</script>
  </head>
<!--<body class="body-Login-back">
</body>-->
	<body class="login-page">
	<?php echo $this->fetch('content'); ?>	
	<!--   footer start  --->
	<!-- jQuery 2.1.4 -->

	</body>
</html>
