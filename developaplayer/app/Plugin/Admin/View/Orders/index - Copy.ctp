      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Contact header) -->
        <section class="content-header">
          <h1>
            Order Details
            <small>advanced tables</small>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Order</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				
				<form method="post" action="<?php echo $this->webroot; ?>admin/orders/orders_multiple_delete" enctype="multipart" id="delete_UnitType">
                  
				  <input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" />
					<table id="example1" class="table table-bordered table-striped">
				 
                    <thead>
                      <tr>
					  <th>
                          
					 </th>
                        <th>Name</th>												
                        <th>order id</th>						
                        <th>Phone No</th>
						<th>Payment Status</th>	
						<th>Purchased On</th>	
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($view_content as $k=>$order){ ?>
                   <tr>
					 
                      <td>
					  <!--<input class="checkbox_del" name="id[]" value="<?php echo $order['Order']['id'] ?>" type="checkbox" />-->
					  </td>					  					  
						
						<td><?php echo $order['Order']['shipping_fullname'] ?></td>
                        <td><a href="<?php echo $this->webroot ?>admin/orders/order_details/<?php echo $order['Order']['id'] ?>"><?php echo $order['Order']['invoice_id']; ?></a></td>                        
                        <td><?php echo $order['Order']['contact_no']; ?></td>                        
                        <td><?php echo $order['Order']['comweb_status']; ?></td>
                         <td><?php echo date('j F Y',strtotime($order['Order']['created'])); ?></td> 
						<!--<td style="width:118px !important">																
						<a href="<?php echo $this->webroot; ?>admin/orders/delete_page/<?php echo base64_encode($order['Order']['id']); ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						</td>-->					
                    </tr>

					<?php } ?>
                    </tbody>
                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
