      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Contact header) -->
        <section class="content-header">
          <h1>
            Order Details
            <small>advanced tables</small>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
               <!--<div class="box-header">
                  <h3 class="box-title">All Order</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div>--><!-- /.box-header -->
                <div class="box-body">
				
				<form method="post" action="<?php echo $this->webroot; ?>admin/orders/orders_multiple_delete" enctype="multipart" id="delete_UnitType">
                  
				  <!--<input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" />-->
					<table id="example1" class="table table-bordered table-striped">
				 
                    <thead>
                      <tr>
                        <th>Name</th>												
                        <th>order id</th>						
                        <!--<th>Phone No</th>-->
						<th>Payment Status</th>	
						<th>Purchased On</th>	
						<th>Paynent Type</th>	
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Order_Management.view')=='Y' && $this->Session->read('permissions.Order_Management.edit')=='N' && $this->Session->read('permissions.Order_Management.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
						$i=0;
					 foreach($view_content as $k=>$order){ ?>
                   <tr>			  					  
						<td><?php echo $order['Order']['shipping_fullname'] ?></td>
                        <td><?php echo $order['Order']['invoice_id']; ?></td>                        
                       <!-- <td><?php echo $order['Order']['contact_no']; ?></td> -->                       
                        <td><?php echo $order['Order']['comweb_status']; ?></td>
                         <td><?php echo date('j F Y',strtotime($order['Order']['created'])); ?></td> 
						 
						 
                         <td><?php  if($order['Order']['paynent_type']){
							 echo "Personal Development Fund";
						 } 
						 else{
							 echo "Commonwealth Payment Getway";
						 }?></td> 
						 
						 
						 
						 
						<td >
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Order_Management.view')=='Y' && $this->Session->read('permissions.Order_Management.edit')=='Y' && $this->Session->read('permissions.Order_Management.delete')=='Y'))
						{ ?>
						<a href="<?php echo $this->webroot ?>admin/orders/order_details/<?php echo $order['Order']['id'] ?>">
						<img alt="Delete" height="23px" src="<?php echo $this->webroot; ?>admin/img/icons/view_certificate.png" /></a>
						<?php $order_value=$order['Order']['id'] ;?>
						<select style="margin:0 2px 0 5px" name="order_status2" id="order_status2<?php echo $i;?>" onchange="return order_status(<?php echo $i;?>,<?php echo $order_value;?>)">
								<option value="1" <?php if($order['Order']['order_status']==1){ echo "selected=selected";} ?>>Pending</option>
								<option value="2" <?php if($order['Order']['order_status']==2){ echo "selected=selected";} ?>>Processing</option>
								<option value="3" <?php if($order['Order']['order_status']==3){ echo "selected=selected";} ?>>Delivered</option>
						</select>
						<a href="<?php echo $this->webroot ?>admin/orders/delete_order/<?php echo base64_encode($order['Order']['id']); ?>">
						<img alt="Delete" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						<?php } ?>
						<?php $nestedData_delete="";   
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Order_Management.view')=='Y' && $this->Session->read('permissions.Order_Management.delete')=='Y')
						{?>
						<a href="<?php echo $this->webroot ?>admin/orders/delete_order/<?php echo base64_encode($order['Order']['id']); ?>">
						<img alt="Delete" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						<?php } ?>
						<?php $nestedData_edit="";   
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Order_Management.view')=='Y' && $this->Session->read('permissions.Order_Management.edit')=='Y')
						{ ?>
						<a href="<?php echo $this->webroot ?>admin/orders/order_details/<?php echo $order['Order']['id'] ?>">
						<img alt="Delete" height="23px" src="<?php echo $this->webroot; ?>admin/img/icons/view_certificate.png" /></a>
						<?php $order_value=$order['Order']['id'] ;?>
						<select style="margin:0 2px 0 5px" name="order_status2" id="order_status2<?php echo $i;?>" onchange="return order_status(<?php echo $i;?>,<?php echo $order_value;?>)">
								<option value="1" <?php if($order['Order']['order_status']==1){ echo "selected=selected";} ?>>Pending</option>
								<option value="2" <?php if($order['Order']['order_status']==2){ echo "selected=selected";} ?>>Processing</option>
								<option value="3" <?php if($order['Order']['order_status']==3){ echo "selected=selected";} ?>>Delivered</option>
						</select>
						<?php } ?>
						</td>					
                    </tr>

					<?php $i++;} ?>
                    </tbody>
                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script>
function order_status(inrID,order_id) {
var status_id = $('#order_status2'+inrID).val();
var order_id = order_id;
$.ajax({
		url:"<?php echo Router::url(array('controller'=>'Orders','action'=>'status_update'));?>",
		type: 'POST',
		data: {status_id : status_id,order_id : order_id},
		success: function(crg) {
			//alert(crg);
			if(crg==1){
				
			}
			
		}
	});
}
	</script>