<div class="content-wrapper">			
	<!-- Main -->
	<div id="content">
		<div class="cl">&nbsp;</div>								
		<!-- Sidebar -->
		<?php //echo $this->element("master/side_right"); ?> 
		<!-- End Sidebar -->		
		<!-- Content -->		
		<div id="content">				
		<!-- Box -->
		<div class="box orderDetails">
			<!-- Box Head -->
			<div class="box-head">
				<h2 class="viewOrder">View Order</h2>
			</div>
			<!-- End Box Head -->			
				<!-- Form -->
				<div class="form">
                	<div class="row orderDetailsFormRow">
                    <div class="col-lg-12">
                    	<label><b>Order Reference No:</b> <?php echo $all_orders_detail['Order']['invoice_id']?></label><br/>
                        <label><b>Purchase On:</b> <?php echo date('j F,Y',strtotime($all_orders_detail['Order']['created']));?></label><br/>
                        <label><b>Payment Method:</b> 
                            ComWeb
                        </label>
                    </div>
                    <div class="col-lg-6">
                        
                        
                        <div class="customerInfoHolder">
                            <div class="buttons" style="font-size:16px;" ><strong>Customer Information</strong></div>
                            <p>
                                <label><b>Name: </b>
                                <?php echo $all_orders_detail['User']['first_name'].' '.$all_orders_detail['User']['last_name'];?></label><br/>		
                                <label><b>Email: </b><?php echo $all_orders_detail['User']['email_address']?></label><br/>
                            </p>
						</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="shippingInfoHolder">
                            <div class="buttons" style="font-size:16px;" ><strong>Shipping Information</strong></div>
                            <p>
                            <label><b>Name: </b><?php echo $all_orders_detail['Order']['shipping_fullname']?></label><br/>
                            <label><b>Street Address: </b><?php echo $all_orders_detail['Order']['shipping_address']?></label><br/>				
                            <label><b>City: </b><?php echo $all_orders_detail['Order']['shipping_city']?></label><br/>
                            <label><b>Pin code: </b><?php echo $all_orders_detail['Order']['shipping_zip']?></label><br/>
                            <label><b>State: </b><?php echo $all_orders_detail['Order']['shipping_state']?></label><br/>
                            <label><b>Country: </b>Australia</label><br/>
                            <label><b>Phone No: </b><?php echo $all_orders_detail['Order']['contact_no']?></label><br/>
                            </p>
                        </div>
                	</div>
                </div>
                <div class="row">
                <div class="col-lg-12 purchasedProducts">
				<div class="buttons"><strong>Purchased Products </strong></div>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					<th width="7%" height="29" align="left"><label><b>Product</b></label></th>
					<th width="20%" align="left" ><label><b>Name</b></label></th>																				
					<th width="10%" align="left" ><label><b>Size</b></label></th>
					<th width="9%" align="left" ><label><b>Quantity</b></label></th>
					<th width="10%" align="left" ><label><b>Price (AUD)</b></label></th>
					<th width="15%" align="left" ><b>Subtotal (AUD)</b></th>					
					</tr>
					<?php 			
					////App::import('Model','ProductOptionAttribute');
					//$ProductOptionAttribute = new ProductOptionAttribute;
					//App::import('Model','ProductImage');
					//$ProductImage = new ProductImage;
					
					foreach($all_oder_detail_product as $key=>$value){
					
					?>
					<tr>
						<td>
						<img width="100px" height="80px" src="<?php echo $this->webroot ?>uploads/product/original/<?php echo $value['ProductImage']['image']; ?>"/>
						</td>
						<td>
						 <label><?php echo $value['Product']['name'];?></label></td>
						
						<td>
						 <label><?php echo $value['Size']['name'];?></label></td>
					
						</td>
						<td><label><?php echo $value['OrderProduct']['quantity'];?></label></td>
						<td><label>AUD <?php echo $value['OrderProduct']['price'];?></label></td>
						<td>
						<label>AUD <?php echo $value['OrderProduct']['price']*$value['OrderProduct']['quantity'];?>
						</label>
						</td>
				
					</tr>
					<?php
					}
					?>				
				  </table>
				
                <label style="float:right;"><b>Payable Amount: </b>AUD <?php echo $all_orders_detail['Order']['total'];?></label>					
	<!-- End Box -->
  	</div>
    </div></div>	
		<div class="cl">&nbsp;</div>			
	</div>
	</div>		
		<div class="cl">&nbsp;</div>			
	</div>
	
	<!-- Main -->
</div>
<script>
function showPD(){
jQuery('#OrderProcessedData').toggle('slow', function() {
    // Animation complete.
  });
 }
</script>