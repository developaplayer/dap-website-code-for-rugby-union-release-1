      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Contact header) -->
        <section class="content-header">
          <h1>
            Front Pages Details
            <small>advanced tables</small>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Pages</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				
				<form method="post" action="<?php echo $this->webroot; ?>admin/subscribers/subscribers_multiple_delete" enctype="multipart" id="delete_UnitType">
                  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y' && $this->Session->read('permissions.Subscriber.delete')=='Y'))
					{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" class="btn btn-primary" onclick="return confirm('Sure You Want To Delete?');" value="Delete" />
					<?php } ?>
					
					<a href="<?php echo $this->webroot;?>admin/subscribers/download_subscribe_xls">
					<input type="button" value="Downloadxls" name="action">
					</a>
					<table id="example1" class="table table-bordered table-striped">
				 
                    <thead>
                      <tr>
						<?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y' && $this->Session->read('permissions.Subscriber.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
                        <th>Subscribe Email</th>
						<th>Created</th>						
                        <?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y' && $this->Session->read('permissions.Subscriber.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>						
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($view_content as $k=>$subscriber){ ?>
                   <tr>
					 
                      
					  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y' && $this->Session->read('permissions.Subscriber.delete')=='Y'))
					   { ?>
						<td>
					  <input class="checkbox_del" name="id[]" value="<?php echo $subscriber['Newslatter']['id'] ?>" type="checkbox" /></td>
					   <?php } ?>
					  					  					  
						
						<td><?php echo $subscriber['Newslatter']['email'] ?></a></td>
                        <td><?php echo $subscriber['Newslatter']['created']; ?></td>                        
						<td style="width:118px !important">
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y' && $this->Session->read('permissions.Subscriber.delete')=='Y'))
						{ ?>															
						<a href="<?php echo $this->webroot; ?>admin/subscribers/delete_page/<?php echo base64_encode($subscriber['Newslatter']['id']); ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						</td>
						<?php } ?>
						
                      </tr>

					<?php } ?>
                    </tbody>
                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
