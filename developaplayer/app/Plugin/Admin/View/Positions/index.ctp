<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Positions</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/positions/delete_position_multiple" enctype="multipart" id="delete_position">
				<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M'&& $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.delete')=='Y'))
				{ ?>
						  <input class="selectall" type="checkbox" />
						  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
						  <br />
                  <table id="primary_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M'&& $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
						<th>Playing Position</th>
						<th>Status</th>
                        <th>Created</th>
						<?php
							if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y'  && $this->Session->read('permissions.Palyer_positions.delete')=='N' && $this->Session->read('permissions.Palyer_positions.edit')=='N' && $this->Session->read('permissions.Palyer_positions.status')=='N') { 
						?>
						<?php } else { ?>
						<th>options</th>	
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					</tbody>
                    <tfoot>
                      <tr>
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M'&& $this->Session->read('permissions.Palyer_positions.view')=='Y' && $this->Session->read('permissions.Palyer_positions.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
                        <th>Playing Position</th>
						<th>Status</th>
						<th>Created</th>
						<?php
							if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y'  && $this->Session->read('permissions.Palyer_positions.delete')=='N' && $this->Session->read('permissions.Palyer_positions.edit')=='N' && $this->Session->read('permissions.Palyer_positions.status')=='N') { 
						?>
						<?php } else { ?>
						<th>options</th>	
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	function table(){
			var dataTable = $('#primary_table').DataTable({
				"dom": '<"row"<"col-md-5 pull-left add_input"><"pull-left error_message hide"><"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/positions/primary_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".primary_table-error").html("");
						$("#primary_table").append('<tbody class="primary_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#primary_table_processing").css("display","none");
					}
				}
			});
			
		<?php
		if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y' && ($this->Session->read('permissions.Palyer_positions.add')=='Y' || $this->Session->read('permissions.Palyer_positions.edit')=='Y')){ ?>
			
				$(".add_input").html('<input style="width:63%" class="form-control" id="primary_position_input" placeholder="Add a Position.." /><div onclick="add_positions()"  id="save" class="btn btn-default">Save</div>');
			<?php } ?>
	}
	
	function add_positions(){
		//alert();
		if($('#position_id').length > 0){
			var id = $('#position_id').val();
			update_positions(id);
		}else{
			$('.error_message').removeClass('hide');
			$('.error_message').html('<span style="color:green">Loading..</span>');
			var position_name = $('#primary_position_input').val();
			$('#primary_table_processing').show();
			//alert(position_name);
			$('#primary_position_input').val('');
			$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/positions/add_position',
				data:{
					'position_name':position_name
				},
				success:function(response){
					if(response=="save_error"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:red">There was an unexpected error</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
					}else
						if(response=="empty_error"){
							$('.error_message').removeClass('hide');
							$('.error_message').html('<span style="color:red">Player Position name must not be empty</span>');
							setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
						}else
							if(response=="exist"){
								$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Player Position Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
							}else{
									$('.dataTables_empty').remove();
									$('#primary_table').append('<tr>'+response+'</tr>');
									$('.error_message').removeClass('hide');
									$('.error_message').html('<span style="color:green">Player Position Added</span>');
									setTimeout(function(){
											$('.error_message').addClass('hide');
										},3000);
								}
					$('#primary_table_processing').hide();
				}
			});
		}
	}
	
	function change_status(id,status){
		$('#primary_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/positions/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#primary_table_processing').hide();
					}
				/*setTimeout(function(){
					$('#message').hide();
				},2000);*/
			}
		});
	}
	
	function edit_data(id,position){
		//alert(position);
		//$('.position_name_'+id).val();
		if($('#cancel').length == 0){
			$('#primary_position_input').val($('#position_name_'+id).text());
			$('#save').after('<div class="btn btn-default" onclick="cancel()" id="cancel">Cancel</div>');
			$('#save').after('<input type="hidden" id="position_id" value="'+id+'" />');
		}
	}
	
	function cancel(){
		$('#primary_position_input').val('');
		$('#cancel').remove();
		$('#position_id').remove();
		//$('#save').prop('onclick','add_positions()');
	}
	
	function update_positions(id){
		//alert(id);
		$('.error_message').removeClass('hide');
		$('.error_message').html('<span style="color:green">Loading..</span>');
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/positions/update_position',
			data:{
				'id':id, 'position':$('#primary_position_input').val()
			},
			success:function(response){
				if(response=="update_error"){
					$('.error_message').removeClass('hide');
					$('.error_message').html('<span style="color:red">Unexpected Error Occurred..</span>');
					setTimeout(function(){
							$('.error_message').addClass('hide');
						},3000);
				}else
					if(response=="success"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:green">Position Updated..</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},3000);
						$('#position_name_'+id).html($('#primary_position_input').val());
						cancel();
					}
					else
						if(response=="exist"){
							$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Player Position Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
						}
			}
		});
	}
</script>
