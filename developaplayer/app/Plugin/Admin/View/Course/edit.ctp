<?php //pr($course); exit;?>

<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>
<!-- Content Wrapper. Contains page content -->
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Course Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					
					echo $this->Form->input('category',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Category',
													'type'=>'select',
													'name'=>'category_id',
													'required'=>true,
													'empty' => 'Select Category',
													'data-placeholder'=>'Select a Category',
													'options'=>array(
																	''=>'Loading Category..'
																	)
																)
															);
					
                   	echo $this->Form->input('position_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Position',
													'type'=>'select',													
													'name'=>'position_id_multiple',
													'multiple'=>'multiple',
                                                    'onchange'=>'payer_level(this)',
													'selected' =>$selectd_position,
													'required'=>true,													
													'data-placeholder'=>'Select a position',
													'options'=>$all_position_data
																)
															);										
								/*echo $this->Form->input('player_certification_level_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select player Certificate For Position',
													'type'=>'select',													
													'name'=>'player_certification_level_id',
                                                    //'onchange'=>'payer_level(this.value)',													
													'required'=>true,
													'data-placeholder'=>'Select player Certificate For Position',
													'options'=>array(
																	''=>'Loading Select player Certificate For Position..'
																	)
																)
															);	*/	   					
															
															
					echo $this->Form->input('age_group',array(
													'div'=>'form-group has-feedback age_group_div',
													'class'=>'required form-control',
													'label' => 'Select Age Group',
													'type'=>'select',
													'name'=>'age_group_id',
													'required'=>true,
													'data-placeholder'=>'Select a Age Group',
													
													'options'=>array(
																	''=>'Loading Age Group..'
																	)
																)
															);
					
					echo $this->Form->input('course_title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Course Title',
													'type'=>'text',
													'id'=>'course_title',
													'name'=>'course_title',
													'value'=>stripslashes($course['Course']['course_title']),
													'placeholder'=>'Course Title'
													)
												);
					echo '<label  for="description">Description</label>';
					echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'type'=>'text',
													'id'=>'description',
													'name'=>'description',
													'value'=>stripslashes($course['Course']['description']),
													'placeholder'=>'Desciption',
													'style'=>'height: 150px'
													)
												);
					echo $this->Form->input('location',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Location',
													'type'=>'text',
													'id'=>'location',
													'name'=>'location',
													'value'=>$course['Course']['location'],
													'placeholder'=>'Location'
													)
												);
												
					echo $this->Form->input('google_location',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Google Map Location Link',
													'type'=>'text',
													'id'=>'google_location',
													'name'=>'google_location',
													'value'=>$course['Course']['google_location'],
													'placeholder'=>'Google Map Location Link'
													)
												);

					echo $this->Form->input('Course Date',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'data-date-format'=>'YYYY-MM-DD',
													'label' => 'Course Date',
													'type'=>'text',
													'id'=>'course_date',
													'name'=>'course_date',
													'value'=>$course['CourseDate']['course_date'],
													'placeholder'=>'YYYY-MM-DD',
													'style'=>'width: 22%'
													)
												);
					echo $this->Form->input('Course start time',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',													
													'label' => 'course start time',
													'type'=>'text',
													'id'=>'course_start_time',
													'name'=>'course_start_time',
													'value'=>$course['CourseDate']['course_start_time'],	
													'placeholder'=>'4:30PM',													
													)
												);	
												
					echo $this->Form->input('duration',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Duration (minutes)',
													'type'=>'number',
													'id'=>'duration',
													'name'=>'duration',
													'value'=>$course['Course']['duration'],
													'placeholder'=>'Duration',
													'style'=>'width: 22%'
													)
												);
					echo $this->Form->input('cost',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Cost (AUD)',
													'type'=>'number',
													'id'=>'cost',
													'name'=>'cost',
													'value'=>$course['Course']['cost'],
													'placeholder'=>'Cost',
													'style'=>'width: 22%'
													)
												);
					echo $this->Form->input('total_course_available',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Total Available Course',
													'type'=>'number',
													'id'=>'total_course_available',
													'name'=>'total_course_available',
													'value'=>$course['Course']['total_course_available'],
													'placeholder'=>'Total Available Course',
													'style'=>'width: 22%'
													)
												);

                   echo $this->Form->input('book_course',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Number of Course Book',
													'type'=>'text',
													'id'=>'book_course',
													//'readonly'=>'readonly',
													'name'=>'book_course',
													'value'=>$course['Course']['book_course'],
													'placeholder'=>'Total Book',
													'style'=>'width: 22%'
													)
												);     
												
												
				if($course['Course']['status']=="A"){
					$options = array(array('name' => 'Active','value' => 'A','selected' => TRUE),'I'=>'InActive');
				}else{
					$options = array(array('name' => 'InActive','value' => 'I','selected' => TRUE),'A'=>'Active');
				}		
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>$options
																)
															);
					
					?>
					<!-- /.box-header -->
						<!--<div class="box-body pad">
							<textarea id="editor1" name="description" placeholder="Description" rows="10" cols="80">
							</textarea>
						</div>-->
					</div>
					<!-- /.box -->
					<p style="padding-top:5px;"></p>
					<div style="background-color:#ccc; padding:5px;font-size: 150%">Course Assign To Coach  </div>
					<p style="padding-top:5px;"></p>
					<?php 
					echo $this->Form->input('coach_course',array(
													'div'=>'form-group has-feedback update_coach_field',
													'class'=>'required form-control',
													'label' => 'Course Assign To Coach',
													'type'=>'select',
													'name'=>'coach_id[]',
													'data-placeholder'=>'Select Coach',
													'style'=>'width:100%',
													'options'=>array(
													''=>'Loading Coach',
													)
												)
											);

					
					?>
					<?php 
					//==========================  Get selected age group id   ===========================================
						$selectedAgeId = "";
						$i = 1;
						foreach($course['CourseAgeGroup'] as $CourseAgeGroup){
							if($i!=1){
								$selectedAgeId = $selectedAgeId.','.$CourseAgeGroup['age_group_id'];
							}else{
								$selectedAgeId = $CourseAgeGroup['age_group_id'];
							}
							$i++;
						}
						
						//==========================  Get selected Coach id   ===========================================
						$selectedCoachId = "";
						$i = 1;
						foreach($course['CourseCoach'] as $CourseCoach){
							if($i!=1){
								$selectedCoachId = $selectedCoachId.','.$CourseCoach['coach_id'];
							}else{
								$selectedCoachId = $CourseCoach['coach_id'];
							}
							$i++;
						}
						
					?>
				
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
              
<script type="text/javascript">

 
 
 function payer_level_first_time(position_id)
{
	
$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_player_level',
				data:{
					'id':position_id
				},
				success:function(response){
					$('#player_certification_level_id').html(response);
				}
		});	
	
}
  
function payer_level(id)
{
	
	
	
$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_player_level',
				data:{
					'id':id.value
				},
				success:function(response){
					$('#player_certification_level_id').html(response);
				}
		});	
	
}

$(function(){
	//CKEDITOR.replace('description');
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/levels/get_category',
				data:{
					'id':'<?php echo $course['Course']['category_id'] ?>'
				},
				success:function(response){
					
					$('#category').html(response);
				}
		});
		
		
		/*$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_position',
				data:{
					'id':'no'
				},
				success:function(response){
					$('#position_id').html(response);
					var position_id = $('#position_id').val();
                    payer_level_first_time(position_id);
				}
		});*/
		
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_age_group',
				data:{
					'id':'<?php if(!empty($selectedAgeId)){echo $selectedAgeId;} else { echo "no";}?>'
				},
				success:function(response){
					$('.age_group_div').html(response);
					$(".select2").select2();
				},
				error:function(response){
					$('.age_group_div').html('');
					$('.age_group_div').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
				}
		});
		//====================== Get all Coach ==============
		$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/get_allcoach',
		data:{
			'id':'<?php if(!empty($selectedCoachId)){echo $selectedCoachId;} else { echo "no";}?>'
		},
		success:function(response){
			$('.update_coach_field').html(response);
			$(".select2").select2();
		},
		error:function(response){
			$('.update_coach_field').html('');
			$('.update_coach_field').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
		}
	});
				
		//=========== disable enter in text area ==============
		$('#description').keypress(function(e){
		if (e.keyCode == 13) return false
		})
		
		
		
});

$('#course_date').datetimepicker({
		//format: 'DD/MM/YYYY' // Disable Time Picker
		format: 'YYYY-MM-DD' // Disable Time Picker
	});
	
</script>