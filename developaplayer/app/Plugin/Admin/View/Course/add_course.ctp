<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>
 
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Course Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					if(!isset($this->request->data['course_title'])){
						$course['course_title']="";
					}else{
						$course['course_title']=$this->request->data['course_title'];
					}
					if(!isset($this->request->data['description'])){
						$course['description']="";
					}else{
						$course['description']=$this->request->data['description'];
					}
					if(!isset($this->request->data['course_date'])){
						$course['course_date']="";
					}else{
						$course['course_date']=$this->request->data['course_date'];
					}
					if(!isset($this->request->data['duration'])){
						$course['duration']="";
					}else{
						$course['duration']=$this->request->data['duration'];
					}
					if(!isset($this->request->data['cost'])){
						$course['cost']="";
					}else{
						$course['cost']=$this->request->data['cost'];
					}
					if(!isset($this->request->data['location'])){
						$course['location']="";
					}else{
						$course['location']=$this->request->data['location'];
					}
					
					if(!isset($this->request->data['google_location'])){
						$course['google_location']="";
					}else{
						$course['google_location']=$this->request->data['google_location'];
					}
					
					if(!isset($this->request->data['total_course_available'])){
						$course['total_course_available']="";
					}else{
						$course['total_course_available']=$this->request->data['total_course_available'];
					}
					
					
					
					echo $this->Form->input('category',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Category',
													'type'=>'select',													
													'name'=>'category_id',
													'required'=>true,
													'data-placeholder'=>'Select a Category',
													'default'=>'--Select a Category--',
													'options'=>array(
																	''=>'Loading Category..'
																	)
																)
															);
															
					echo $this->Form->input('position_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Position',
													'type'=>'select',													
													'name'=>'position_id_multiple',
                                                    'onchange'=>'payer_level(this)',													
													'required'=>true,
													'multiple'=>'multiple',
													'data-placeholder'=>'Select a position',
													'options'=>array(
																	''=>'Loading position..'
																	)
																)
															);										
								/*echo $this->Form->input('player_certification_level_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'label' => 'Select player Certificate For Position',
													'type'=>'select',													
													'name'=>'player_certification_level_id',
                                                    //'onchange'=>'payer_level(this)',													
													//'required'=>true,
													'data-placeholder'=>'Select player Certificate For Position',
													'options'=>array(
																	''=>'Loading Select player Certificate For Position..'
																	)
																)
															);*/							
															
					echo $this->Form->input('age_group',array(
													'div'=>'form-group has-feedback age_group_div',
													'class'=>'required form-control',
													'label' => 'Select Age Group',
													'type'=>'select',																		
													'name'=>'age_group_id[]',
													'required'=>true,
													'data-placeholder'=>'Select a Age Group',
													'options'=>array(
																	''=>'Loading Age Group..'
																	)
																)
															);															
															
					
					echo $this->Form->input('course_title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Course Title',
													'type'=>'text',
													'id'=>'course_title',
													'name'=>'course_title',
													'value'=>$course['course_title'],
													'placeholder'=>'Course Title'
													)
												);
					echo '<label  for="description">Description</label>';
					echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'type'=>'text',
													'id'=>'description',
													'name'=>'description',
													'value'=>$course['description'],
													'placeholder'=>'Desciption',
													'style'=>'height: 150px'
													)
												);
					echo $this->Form->input('location',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Location',
													'type'=>'text',
													'id'=>'location',
													'name'=>'location',
													'value'=>$course['location'],
													'placeholder'=>'Location'
													)
												);
												
					echo $this->Form->input('google_location',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Google Map Location Link',
													'type'=>'text',
													'id'=>'google_location',
													'name'=>'google_location',
													'value'=>$course['google_location'],
													'placeholder'=>'Google Map Location Link'
													)
												);
												
												
												
					echo $this->Form->input('Course Date',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'data-date-format'=>'YYYY-MM-DD',
													'label' => 'Course Date',
													'type'=>'text',
													'id'=>'course_date',
													'name'=>'course_date',
													'value'=>$course['course_date'],
													'placeholder'=>'YYYY-MM-DD',
													'style'=>'width: 22%'
													)
												);
					echo $this->Form->input('Course start time',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',													
													'label' => 'course start time',
													'type'=>'text',
													'id'=>'course_start_time',
													'name'=>'course_start_time',													
													'placeholder'=>'4:30PM',													
													)
												);													
												
					echo $this->Form->input('duration',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Duration (minutes)',
													'type'=>'number',
													'id'=>'duration',
													'name'=>'duration',
													'value'=>$course['duration'],
													'placeholder'=>'Duration',
													'style'=>'width: 22%'
													)
												);
					echo $this->Form->input('cost',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Cost (AUD)',
													'type'=>'number',
													'id'=>'cost',
													'name'=>'cost',
													'value'=>$course['cost'],
													'placeholder'=>'Cost',
													'style'=>'width: 22%'
													)
												);
												
				echo $this->Form->input('total_course_available',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Total Available Course',
													'type'=>'number',
													'id'=>'total_course_available',
													'name'=>'total_course_available',
													'value'=>$course['total_course_available'],
													'placeholder'=>'Total Available Course',
													'style'=>'width: 22%'
													)
												);								
												
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'Active',
																	'I'=>'InActive'
																	)
																)
															);
					
					?>
					<p style="padding-top:5px;"></p>
					<div style="background-color:#ccc; padding:5px;font-size: 150%">Course Assign To Coach  </div>
					<p style="padding-top:5px;"></p>
					<?php 
					echo $this->Form->input('coach_course',array(
													'div'=>'form-group has-feedback update_coach_field',
													'class'=>'required form-control',
													'label' => 'Course Assign To Coach',
													'type'=>'select',
													'name'=>'coach_id[]',
													'data-placeholder'=>'Select Coach',
													'style'=>'width:100%',
													'options'=>array(
													''=>'Loading Coach',
													)
												)
											);

					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
<script type="text/javascript">

function payer_level(id)
{
	
$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_player_level',
				data:{
					'id':id.value
				},
				success:function(response){
					$('#player_certification_level_id').html(response);
				}
		});	
	
}

$(function(){
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_category',
				data:{
					'id':'no'
				},
				success:function(response){
					$('#category').html(response);
				}
		});
		
			$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_position',
				data:{
					'id':'no'
				},
				success:function(response){
					$('#position_id').html(response);
				}
		});
		
		
		
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/course/get_age_group',
				data:{
					'id':'no'
				},
				success:function(response){
					$('.age_group_div').html(response);
					$(".select2").select2();
				},
				error:function(response){
					$('.age_group_div').html('');
					$('.age_group_div').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
				}
		});
		
		$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/get_allcoach',
		data:{
			'id':'no'
		},
		success:function(response){
			$('.update_coach_field').html(response);
			$(".select2").select2();
		},
		error:function(response){
			$('.update_coach_field').html('');
			$('.update_coach_field').after('<span class="error ajax_error">There was an error while loading Coach. Refresh the page.</span>');
		}
	});
		//=========== disable enter in text area ==============
		$('#description').keypress(function(e){
		if (e.keyCode == 13) return false
		})
		
		       
		
});

//disabling past date from datepicker
//var nowDate = new Date();
//var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

$(document).ready(function(){
	
 var date = new Date();
date.setDate(date.getDate());

//var date1 = new Date();


//alert(date);

 $('#course_date123').datepicker({
                    format: "dd/mm/yyyy",
					 startDate: '-0d',
                });  
				
$('#course_date1').datetimepicker({
	         
		//format: 'DD/MM/YYYY' // Disable Time Picker
		//format: 'YYYY-MM-DD',// Disable Time Picker
		//startDate: date 
	});
	
	
	 $(function () {
      // $('#course_date').data("DateTimePicker").minDate(date);
        $('#course_date').datetimepicker({
			format: 'YYYY-MM-DD',
            useCurrent: false ,//Important! See issue #1075
			// startDate : new Date('2016-04-17'),
			
                    disabledDates: [
                        moment(date),
                     // new Date(2016, 11 - 1, 21),
                       // "11/22/2016 00:53"
                    ],
					
					//defaultDate: date.setDate(date.getDate()+2)
			
        });
      // $("#course_date").on("dp.change", function (e) {
           //$('#course_date').data("DateTimePicker").minDate(date);
//});
        //$("#course_date").on("dp.change", function (e) {
            //$('#course_date').data("DateTimePicker").maxDate(e.date);
       // });
    });
	
	});
</script>