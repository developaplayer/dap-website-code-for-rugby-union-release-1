<section class="content-header">
          <h1>
            <?php echo ucwords(str_replace("_"," ",$this->params['controller'])); ?></a>
            <small>Preview</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo $this->webroot; ?>admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo $this->webroot; ?>admin/<?php echo $this->params['controller'].'/'.$this->params['action']; ?>"><?php echo ucwords(str_replace("_"," ",$this->params['controller'])); ?></a></li>
            <li class="active"><?php echo ucwords(str_replace("_"," ",$this->params['action'])); ?></li>
          </ol>
</section>