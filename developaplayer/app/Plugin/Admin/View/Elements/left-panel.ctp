
<?php //echo '<pre>'; print_r($this->Session->read('permissions')) ; exit;
//echo $this->Session->read('permissions.player_profile.view'); exit;
      //echo $this->Session->read('permissions.positions.view');
	  //exit;
?>
<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar ">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel ">
            <div class="pull-left image">
              <img src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Welcome <?php if($this->Session->read('Auth.User.type')=='M'){ ?>Moderator<?php }else{ ?>Admin<?php } ?></p>
            </div>
          </div>
         
          <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
		  <!-- User Management -->
		  <?php if($this->Session->read('Auth.User.type')=='A'  ){ ?>  	
		  <li class="shadow_right treeview <?php if($this->params['controller']=='admin_moderators'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Admin Moderator</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu ">
                <li class="shadow_right <?php if($this->params['action']=='all_admin_moderators'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/admin_moderators/all_admin_moderators"><i class="fa fa-circle-o"></i> All Moderators</a></li>
                <li class="shadow_right <?php if($this->params['action']=='add_admin_moderator'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/admin_moderators/add_admin_moderator"><i class="fa fa-circle-o"></i> Add Moderator</a></li>
              </ul>
            </li>
			
		  <?php } ?> 
		  
	<!------------------------For Palyer Profile-------------------------------->

<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.view')=='Y') ){ ?>  
		
	<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='player_profile' || $this->params['controller']=='ratings'){?> active <?php }?>">
	<a href="#"><i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Player Profile</span> <i class="fa fa-angle-left pull-right"></i>  </a>
	  
	<ul class="treeview-menu ">
		<li class="shadow_right treeview <?php if(($this->params['controller']=='player_profile' && $this->params['action']=='index') || ($this->params['controller']=='ratings' && ($this->params['action']=='view_rating' || $this->params['action']=='add_rating'))){?> active <?php }?>" >
				<a href="<?php echo $this->webroot; ?>admin/player_profile/">
				<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>View Players</span>
				</a>
			</li>
			
			<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Player_Profile.add')=='Y') ){ ?>
 			
				<li class="shadow_right treeview <?php if($this->params['controller']=='player_profile' && $this->params['controller']=='add_player'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/player_profile/add_player">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Player</span>
				  </a>
				</li>	
		     <?php } ?>
	</ul>
  </li>
    
	 <?php } ?>	  


<!-------End---------------------------------->	   


	<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='secondary_positions' || $this->params['controller']=='nationalities' || $this->params['controller']=='positions' || $this->params['controller']=='levels' || $this->params['controller']=='clubs' || $this->params['controller']=='age_groups'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Player Master Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">	
		
		<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Palyer_positions.view')=='Y') ){ ?>
 		
		<li class="shadow_right treeview <?php if($this->params['controller']=='positions'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Position</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/positions"><i class="fa fa-circle-o"></i> All Position</a></li>
				  </ul>
            </li>
			
	<?php } ?>

	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y' || $this->Session->read('permissions.level.add')=='Y') ){ ?>  
	
			<li class="shadow_right treeview <?php if($this->params['controller']=='levels'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Levels</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
			  <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.view')=='Y') ){ ?>  
	
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/levels"><i class="fa fa-circle-o"></i> All Levels</a></li>
			  <?php } ?>
			  
			  <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.level.add')=='Y') ){ ?>  
	
				<li class="shadow_right <?php if($this->params['action']=='add_level'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/levels/add_level"><i class="fa fa-circle-o"></i> Add Levels</a></li>
			  <?php } ?>
			  </ul>
            </li>
	
	<?php } ?>
	
	<!-- Club --->
 <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && ($this->Session->read('permissions.Club.view')=='Y' || $this->Session->read('permissions.Club.add')=='Y')) ){ ?>  
		 
			<li class="shadow_right treeview <?php if($this->params['controller']=='clubs'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Club</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
			  
<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y') ){ ?>  
	
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/clubs/"><i class="fa fa-circle-o"></i> All Clubs</a></li>
<?php } ?>
<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.add')=='Y') ){ ?>  
			
				<li class="shadow_right <?php if($this->params['action']=='add_club'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/clubs/add_club"><i class="fa fa-circle-o"></i> Add Club</a></li>
			  </ul>
            </li>
	<?php } ?>
	
 <?php } ?>
 
 <!-- Age Group============!-->
 <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && ($this->Session->read('permissions.Age_Group.view')=='Y' || $this->Session->read('permissions.Age_Group.add')=='Y')) ){ ?>  
	
	
			<li class="shadow_right treeview <?php if($this->params['controller']=='age_groups'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Age Groups</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
			  <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.view')=='Y') ){ ?>  	
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/age_groups/"><i class="fa fa-circle-o"></i> All Age Groups</a></li>
			  <?php } ?>
			   <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Age_Group.add')=='Y') ){ ?>  	
				
				<li class="shadow_right <?php if($this->params['action']=='add_age_group'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/age_groups/add_age_group"><i class="fa fa-circle-o"></i> Add Age Groups</a></li>
			   <?php } ?>
			  </ul>
            </li>
 <?php } ?>

 <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && ($this->Session->read('permissions.Capability_Matrix_Position.view')=='Y' || $this->Session->read('permissions.Capability_Matrix_Position.add')=='Y')) ){ ?>  
	<li class="shadow_right treeview <?php if($this->params['controller']=='capability_matrix_positions'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Capability Matrix by Position</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
				<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Capability_Matrix_Position.view')=='Y') ){ ?>
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/capability_matrix_positions/"><i class="fa fa-circle-o"></i> Capability Matrix By Position List</a></li>
				<?php } ?>
				 <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Capability_Matrix_Position.add')=='Y') ){ ?>  		
				<li class="shadow_right <?php if($this->params['action']=='capability_matrix_positions'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/capability_matrix_positions/add_capabilitymatrix_position"><i class="fa fa-circle-o"></i> Add Capability Matrix bY Position</a></li>
				 <?php } ?>
			  </ul>
            </li>
 <?php } ?>
 
   <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && ($this->Session->read('permissions.Nationality.view')=='Y' || $this->Session->read('permissions.Nationality.add')=='Y'))) { ?>  
						
			<li class="shadow_right treeview <?php if($this->params['controller']=='nationalities'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/nationalities/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Nationality</span>
              </a>
            </li>
    <?php } ?>
	
		</ul>
		</li>
	

<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Booked_Course.view')=='Y') ){ ?>  	
	<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='player_profile'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Booked Course</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
				<li class="shadow_right <?php if($this->params['action']=='bookedCourse'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/player_profile/bookedCourse"><i class="fa fa-circle-o"></i>View Booked Course</a></li>
				
			  </ul>
            </li>

	 <?php } ?>		
	 
<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Level_Category.view')=='Y') ){ ?>  	
	
<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='level_categories'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Level Category Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
	
			<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/level_categories/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Level Categories</span>
              </a>
            </li>
	
		</ul>
		</li>
		
<?php } ?>


<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='coachings' || $this->params['controller']=='coach_profile' || $this->params['controller']=='certificates' || $this->params['controller']=='secondary_coachings'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Coach Profile Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
	
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coaching_Speciality.view')=='Y') ){ ?>  	
	
			<li class="shadow_right treeview <?php if($this->params['controller']=='coachings'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/coachings/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Coach Specialities</span>
              </a>
            </li>
			
	<?php } ?>
	
	
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.view')=='Y') ){ ?>  		
			<li class="shadow_right treeview <?php if($this->params['controller']=='coach_levels'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Coach level</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu ">
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/coach_levels"><i class="fa fa-circle-o"></i> All Coach Levels</a></li>
				<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.coach_level.add')=='Y') ){ ?>  		
				<li class="shadow_right <?php if($this->params['action']=='add_level'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/coach_levels/add_level"><i class="fa fa-circle-o"></i> Add Coach Levels</a></li>
				<?php } 
				?>
			  </ul>
            </li>		
	<?php } ?>
	
	
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y') ){ ?>  		
		
			<li class="shadow_right treeview <?php if($this->params['controller']=='certificates'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/certificates/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Certificates</span>
              </a>
            </li>
			
	<?php } ?>
	
	
	
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.view')=='Y') ){ ?>  				
			<li class="shadow_right treeview <?php if($this->params['controller']=='coach_profile'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/coach_profile/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Coach Profile</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			   <ul class="treeview-menu ">
				<li class="shadow_right <?php if($this->params['action']=='index'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/coach_profile/"><i class="fa fa-circle-o"></i> All Coaches</a></li>
				<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Coach_Profile.add')=='Y') ){ ?>  					
				<li class="shadow_right <?php if($this->params['action']=='add_coach'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/coach_profile/add_coach"><i class="fa fa-circle-o"></i> Add Coach</a></li>
				<?php } ?>
			  </ul>
            </li>
	
		</ul>
		</li>
	<?php } ?>
	
<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y') ){ ?>  				
			
<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='course'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Course Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.view')=='Y') ){ ?>  				
	
			<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/course/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Courses</span>
              </a>
            </li>
			
	<?php } ?>
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Course.add')=='Y') ){ ?>  				
	
			<li class="shadow_right treeview <?php if($this->params['action']=='add_course'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/course/add_course">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Courses</span>
              </a>
            </li>
	<?php } ?>
	
		</ul>
		</li>
		
<?php } ?>

<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Cms_Page.view')=='Y') ){ ?>  				
	
<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='generic_pages'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Generic Pages</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">	
			<li class="shadow_right treeview <?php if($this->params['action']=='about_us'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/generic_pages/about_us">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>About Us</span>
              </a>
			<li class="shadow_right treeview <?php if($this->params['action']=='terms_conditions'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/generic_pages/terms_conditions">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Terms &#38; Conditions</span>
              </a>
            </li>
            
			<li class="shadow_right treeview <?php if($this->params['controller']=='generic_pages' && ($this->params['action']=='banner' || $this->params['action']=='add_banner')){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Banners</span><i class="fa fa-angle-left pull-right"></i>
              </a>
			  <ul class="treeview-menu">
				<li class="shadow_right <?php if($this->params['action']=='banner'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/generic_pages/banner"><i class="fa fa-circle-o"></i> All Banners</a></li>
				<li class="shadow_right <?php if($this->params['action']=='add_banner'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/generic_pages/add_banner"><i class="fa fa-circle-o"></i> Add Banner</a></li>
			  </ul>
            </li>     
		   </ul>
		</li>
		
		</li>
		
<?php } ?>


<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' )){ ?>  				
	
		<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='family_profiles'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Family Profiles Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.view')=='Y') ){ ?>
			<li class="shadow_right treeview <?php if($this->params['action']=='family_profiles'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/family_profiles/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Family Member</span>
              </a>
            </li>
	<?php } ?>
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Family_Profile.add')=='Y') ){ ?>  				
	
	
			<li class="shadow_right treeview <?php if($this->params['action']=='add_category'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/family_profiles/add_family/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Family Member</span>
              </a>
            </li>
	<?php } ?>
    
		</ul>
		</li>
		
    <?php } ?> 
	
	
     <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M') ){ ?>  				
	   
	<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='categories'){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Product Category Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
	<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y') ){ ?> 
			<li class="shadow_right treeview <?php if($this->params['action']=='all_category'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/categories/all_category/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Categories</span>
              </a>
            </li>
	<?php } ?>
   <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.add')=='Y') ){ ?>  				
	  
			<li class="shadow_right treeview <?php if($this->params['action']=='add_category'){?> active <?php }?>" >
              <a href="<?php echo $this->webroot; ?>admin/categories/add_category/">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Categories</span>
              </a>
            </li>
			
   <?php } ?>
    
		</ul>
		</li>
		
	 <?php } ?>
	 
	 
	  <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M') ){ ?>  				
	  
       <li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='products'){?> active <?php }?>">
			<a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Product Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
			<ul class="treeview-menu ">
		<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.view')=='Y') ){ ?>
				<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/products/index/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Products</span>
				  </a>
				</li>
		<?php } ?>
		<?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Management.add')=='Y') ){ ?>  				
	  
				<li class="shadow_right treeview <?php if($this->params['action']=='add_product'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/products/add_product/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Product</span>
				  </a>
				</li>
				
		<?php } ?>
		
			</ul>
		</li>
		
	  <?php } ?>
	  
	   <?php if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Order_Management.view')=='Y') ){ ?>  				
	  
		  <li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='orders'){?> active <?php }?>">
			<a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Order Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
			<ul class="treeview-menu ">
		
				<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/orders/index/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Orders</span>
				  </a>
			</li>
		
		
			</ul>
		</li>
		 
       <?php } ?>
	   
	    <?php
		  if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M') )
		  { 
         ?>  					  
	   <li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='highlights'){?> active <?php }?>">
			<a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Highlight Video</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
			<ul class="treeview-menu ">
		<?php
		  if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.view')=='Y') )
		  { 
         ?> 
				<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/highlights/index/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Video</span>
				  </a>
				</li>
		  <?php } ?>
		 <?php
		  if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Highlight_Video.add')=='Y') )
		  { 
         ?>  
				<li class="shadow_right treeview <?php if($this->params['action']=='add_product'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/highlights/add_highlight/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Add Video</span>
				  </a>
				</li>	
		  <?php
		  } 
		  ?>
				
			</ul>
		</li> 
		
	<?php 
	} 
	?>
	
	 <?php
		  if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y') )
		  { 
         ?> 
		
		<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='contacts'){?> active <?php }?>">
			<a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Contact</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
			<ul class="treeview-menu ">
		
				<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/contacts/index/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Contact</span>
				  </a>
				</li>
		
		
			</ul>
		</li>
		
		  <?php } ?>

		  
		  <?php
		  if(($this->Session->read('Auth.User.type')=='A') || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Subscriber.view')=='Y') )
		  { 
         ?> 
		
		<li class="main_parent_class shadow_right treeview <?php if($this->params['controller']=='subscribers'){?> active <?php }?>">
			<a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Subscriber</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
			<ul class="treeview-menu ">
		
				<li class="shadow_right treeview <?php if($this->params['action']=='index'){?> active <?php }?>" >
				  <a href="<?php echo $this->webroot; ?>admin/subscribers/index/">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>All Subscriber</span>
				  </a>
				</li>
				
		
			</ul>
		</li>
		
		  <?php } ?>


	</ul>
	</section>
</aside>

<script>
$(document).ready(function(){
$('.main_parent_class').each(function(){	
	var remove_element=  $(this).find('.treeview-menu').text();
	if($.trim(remove_element)=="")
	{
		 $(this).html('');
		
	}

});

});
</script>