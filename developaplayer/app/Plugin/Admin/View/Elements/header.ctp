<header class="main-header">
        <!-- Logo -->
        <a href="<?php echo $this->webroot; ?>admin/users/dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">DP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Develop A Player</b> Admin Panel</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
			  
	<?php if($this->Session->read('permissions.Contact_Management.view')=='Y'){ ?>
			   <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success"><?php echo count($contacts_header); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">You have <?php echo count($contacts_header); ?> messages</li>
                  <li>
                    <!-- inner menu: contains the messages -->
                    <ul class="menu">
					<?php foreach($contacts_header as $contact){ ?>
                      <li><!-- start message -->
                        <a href="<?php echo $this->webroot; ?>admin/contacts/all_contacts">
                          <div class="pull-left">
                            <!--User Image -->
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                          </div>
                          <!-- Message title and timestamp -->
                          <h4>
                            <?php echo $contact['Contact']['first_name'].' '.$contact['Contact']['last_name']; ?>
                            <!--<small><i class="fa fa-clock-o"></i> 5 mins</small>-->
                          </h4>
                          <!-- The message -->
                          <p><?php echo $contact['Contact']['message']; ?></p>
                        </a>
                      </li><!-- end message -->
					<?php } ?>
                    </ul><!-- /.menu -->
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li><!-- /.messages-menu -->
	<?php } ?>
			  
             <!-- Notifications Menu -->
	<?php if($this->Session->read('permissions.Product_Management.view')=='Y'){ ?>
              <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning notification_count_only"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header notification_count"></li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
					<?php
					$total_notification=0;
					foreach($notifications as $notification){
					if(!empty($notification['ProductToWarehouse'])){ ?>
					  <?php foreach($notification['ProductToWarehouse'] as $stock_in_warehouse){
							$total_notification++; ?>
                      <li><!-- start notification -->
			<?php if($this->Session->read('permissions.Product_Management.edit')=='Y'){ ?>
                        <a style="color:red" target="_blank" href="<?php echo $this->webroot; ?>admin/products/product_detail/<?php echo base64_encode($notification['Product']['id']); ?>">
			<?php } ?>
						
                          <i class="text-aqua"><img width="15px" height="15px" alt="image" src="<?php echo $this->webroot; ?>uploads/products/thumb/<?php if(isset($notification['ProductImage'][0]['image'])){ echo $notification['ProductImage'][0]['image']; } ?>" /></i>
						  
						  <?php echo $notification['Product']['name']; ?> is out of stock in <?php echo $stock_in_warehouse['Warehouse']['name']; ?>
			<?php if($this->Session->read('permissions.Product_Management.view')=='Y'){ ?>
                        </a>
			<?php } ?>
                      </li><!-- end notification -->
					  <?php } ?>
					<?php }
						} ?>
                    </ul>
                  </li>
                  <!--<li class="footer"><a href="#">View all</a></li>-->
                </ul>
              </li>
			  <input type="hidden" id="total_notification_count" value="<?php echo $total_notification; ?>" />
	<?php } ?>
	
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
                  <span class="hidden-xs">Welcome <?php echo $this->Session->read('Auth.User.first_name')." ".$this->Session->read('Auth.User.last_name');
				  if($this->Session->read('Auth.User.type')=='M'){ ?> (Moderator) <?php } ?></span>
                </a>
                <ul class="dropdown-menu">
				
                 <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
                    <p>
                      Super Admin
					  <?php /*if($this->Session->check('Auth.User.type')<>"A"){ ?>
							<small>Member since Nov. 2015</small>
					  <?php }else{ ?>
								<small>Admin since Nov. 2015</small>
					  <?php }*/ ?>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--<li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Moderators</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="<?php echo $this->webroot; ?>admin/profile/">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a target="_blank" href="<?php echo Router::url('/', true); ?>">Store</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--<div class="pull-left">
                      <a class="btn btn-default btn-flat" href="<?php echo $this->webroot; ?>admin/profile/">Profile</a>
                    </div>-->
					
                    <div class="pull-right">
                      <a href="<?php echo $this->webroot; ?>admin/users/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
             <li>
                <a href="javascript:void(0)" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>
	  <script>
	  $(function(){
		  var value=$('#total_notification_count').val();
		  $('.notification_count_only').html(value);
		  $('.notification_count').html('You have '+value+' notifications');
	  });
	  </script>