 <footer class="main-footer">
       
      </footer>
<style>
.hidden{
	display:none;
}
</style>
      <!-- Control Sidebar -->
       <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          
          
          <!-- Settings tab content -->
          <div class="tab-pane active" id="control-sidebar-settings-tab">
           <form method="post" class="hidden">
              <h3 class="control-sidebar-subheading">Store Settings</h3>
			  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 70%"></div>
                  </div>
				  
			  <div class="form-group">
                <label class="control-sidebar-subheading">
                  Disable Store
                  <input <?php if($setting['store']=='I'){?> checked <?php } ?> onchange="update_settings('store','I')" name="store" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked store will be unavailable
                </p>
              </div><!-- /.form-group -->
			  
			  <div class="form-group">
                <label class="control-sidebar-subheading">
                  <a style="color:yellow" href="<?php echo $this->webroot; ?>admin/store/logo">Change Logo</a>
                </label>
              </div>
			  
			   <div class="form-group">
                <label class="control-sidebar-subheading">
                  Disable Sign Up Verification
                  <input <?php if($setting['sign_up']=='I'){?> checked <?php } ?> onchange="update_settings('sign_up')" name="sign_up" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked store will be unavailable
                </p>
              </div>
			  
			  <div class="form-group">
                <label class="control-sidebar-subheading">
                  Disable Link Verification For Sign Up
                  <input <?php if($setting['link_verification']=='I'){?> checked <?php } ?> onchange="update_settings('link_verification')" name="link_verification" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked Activation Link will be enabled
                </p>
              </div>
			  
			  <div class="form-group">
                <label class="control-sidebar-subheading">
                  Disable OTP Verification For Sign Up
                  <input <?php if($setting['otp_verification']=='I'){?> checked <?php } ?> onchange="update_settings('otp_verification')" name="otp_verification" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked One Time Password(OTP) will be enabled
                </p>
              </div>
			  
              <div class="form-group">
			  <label class="control-sidebar-subheading">
                  Disable
                </label>
				<div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                <label class="control-sidebar-subheading">
                  Search Option
                  <input <?php if($setting['search']=='I'){?> checked <?php } ?> onchange="update_settings('search')" name="search" type="checkbox" class="pull-right" />
                </label>
				<p>
                  If checked it will disable search option in store
                </p>
				<label class="control-sidebar-subheading">
                  Search By Product Option
                  <input <?php if($setting['searchbyname']=='I'){?> checked <?php } ?> onchange="update_settings('searchbyname')" name="searchbyname" type="checkbox" class="pull-right" />
                </label>
				<p>
                  If checked it will disable search by product name option in store
                </p>
				<label class="control-sidebar-subheading">
                  Search By Brand Option
                  <input <?php if($setting['searchbybrand']=='I'){?> checked <?php } ?> onchange="update_settings('searchbycat')" name="searchbycat" type="checkbox" class="pull-right" />
                </label>
				<p>
                  If checked it will disable search by brand name option in store
                </p>
				<label class="control-sidebar-subheading">
                  Search By Category Option
                  <input <?php if($setting['searchbycat']=='I'){?> checked <?php } ?> onchange="update_settings('searchbybrand')" name="searchbybrand" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked it will disable search by category name option in store
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Main Banner
                  <input <?php if($setting['main_banner']=='I'){?> checked <?php } ?> onchange="update_settings('main_banner')" name="main_banner" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked Main Banner will disable
                </p>
              </div><!-- /.form-group -->

			  <div class="form-group">
                <label class="control-sidebar-subheading">
                  Left Banner
                  <input <?php if($setting['banner']=='I'){?> checked <?php } ?> onchange="update_settings('banner')" name="banner" type="checkbox" class="pull-right" />
                </label>
                <p>
                  If checked Left Banner will disable
                </p>
              </div><!-- /.form-group -->
            </form>
			
			<h3 class="control-sidebar-subheading hidden">Store Settings</h3>
			  <div class="progress progress-xxs hidden">
                    <div class="progress-bar progress-bar-warn" style="width: 70%"></div>
                  </div>

              <div class="form-group hidden">
                <label class="control-sidebar-subheading">
                  <a style="" href="<?php echo $this->webroot; ?>admin/settings/change_color">Change Store Background Color</a>
                </label>
              </div><!-- /.form-group -->
			
              <h3 class="control-sidebar-subheading">Profile Settings</h3>
			  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 70%"></div>
                  </div>

              <div class="form-group hide">
                <label class="control-sidebar-subheading">
                  <a style="color:yellow" href="<?php echo $this->webroot; ?>admin/profile">Edit Profile</a>
                </label>
              </div><!-- /.form-group -->

              <div class="form-group hide">
                <label class="control-sidebar-subheading">
                 <a style="color:yellow" href="<?php echo $this->webroot; ?>admin/profiles/email">Change Email</a>
                </label>
              </div><!-- /.form-group -->

			  
              <div class="form-group">
                <label class="control-sidebar-subheading">
                 <a style="color:yellow" href="<?php echo $this->webroot; ?>admin/users/change_password">Change Password</a>
                </label>
              </div><!-- /.form-group -->
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
	<div class="dialog"></div>
  </body>
</html>
<script>
function update_settings(name,status){
	if($('input[name="'+name+'"]:checked').length>0)
	{
		if(confirm('Are You Sure You Want To disable '+name+'?')==true)
			{
				$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/settings/',
					data:{
						'name':name,'status[Setting][status]':'I'
					},
					success:function(data){
							if (data==404) {
								window.location = "<?php echo $this->Html->url(array('plugin'=>'admin','controller' => 'users', 'action' => 'login')); ?>";
								return false;
							}
						if(data==1){
							$('input[name="'+name+'"]').prop('checked','checked');
						}else
						if(data==0){
							$('input[name="'+name+'"]').prop('checked',false);
						}else
						if(data=="otp_verification"){
							$('input[name="'+name+'"]').prop('checked','checked');
							$('input[name="otp_verification"]').prop('checked',false);
						}else
						if(data=="link_verification"){
							$('input[name="'+name+'"]').prop('checked','checked');
							$('input[name="link_verification"]').prop('checked',false);
						}else
						if(data=="sign_up"){
							$('input[name="'+name+'"]').prop('checked','checked');
							$('input[name="link_verification"]').prop('checked','checked');
							$('input[name="otp_verification"]').prop('checked','checked');
						}
					}
				});
			}else{
				$('input[name="'+name+'"]').prop('checked',false);
			}
	}else{
		if(confirm('Are You Sure You Want To enable '+name+'?')==true)
			{
				$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/settings/',
					data:{
						'name':name,'status[Setting][status]':'A'
					},
					success:function(data){
						if(data==1){
							$('input[name="'+name+'"]').prop('checked',false);
						}else
							if(data==0){
							$('input[name="'+name+'"]').prop('checked','checked');
						}else
						if(data=='otp_verification'){
							$('input[name="'+name+'"]').prop('checked',false);
							$('input[name="'+data+'"]').prop('checked','checked');
						}else
						if(data=='link_verification'){
							$('input[name="'+name+'"]').prop('checked',false);
							$('input[name="'+data+'"]').prop('checked','checked');
						}else
						if(data=="sign_up"){
							$('input[name="'+name+'"]').prop('checked',false);
							$('input[name="otp_verification"]').prop('checked','checked');
							$('input[name="link_verification"]').prop('checked',false);
						}
					}
				});
			}else{
				$('input[name="'+name+'"]').prop('checked','checked');
			}
	}
}
</script>