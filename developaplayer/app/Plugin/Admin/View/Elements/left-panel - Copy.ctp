<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar ">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel ">
            <div class="pull-left image">
              <img src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Welcome <?php if($this->Session->read('Auth.User.type')=='M'){ ?>Moderator<?php }else{ ?>Admin<?php } ?></p>
            </div>
          </div>
         
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
		  
		  <!-- User Management -->
	<li class="shadow_right treeview <?php if(($this->params['controller']=='deliveries' && $this->params['action']<>'dashboard') || ($this->params['controller']=='users' && $this->params['action']<>'dashboard') || ($this->params['controller']=='admin_moderators')){?> active <?php }?>">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>User Type Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
		
		<!-- User -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class="shadow_right treeview <?php if($this->params['controller']=='users' && $this->params['action']<>'dashboard'){?> active <?php }?>" >
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Customer Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class="treeview-menu ">
								<li class="shadow_right  <?php if($this->params['action']=='view_all'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/users/view_all"><i class="fa fa-circle-o"></i>  All Customers</a></li>
									<?php if(!empty($permissions)){ ?>
											<?php foreach($permissions as $k=>$permission){?>
												<?php if($permission['AdminMenu']['controller']=='users' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
													<li class="shadow_right  <?php if($this->params['action']=='add_user'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/users/add_user"><i class="fa fa-circle-o"></i> Add Customers</a></li>
												<?php }
											}
										} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='users' && $this->params['action']<>'dashboard'){?> active <?php }?>" >
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/customer management.png" />&nbsp;</i> <span>Customer Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class="treeview-menu ">
							<li class="shadow_right <?php if($this->params['action']=='view_all'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/users/view_all"><i class="fa fa-circle-o"></i> All Customers</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_user'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/users/add_user"><i class="fa fa-circle-o"></i> Add Customers</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
		
	<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
			<li class="shadow_right treeview <?php if($this->params['controller']=='admin_moderators'){?> active <?php }?>" >
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Admin Moderator</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu ">
                <li class="shadow_right <?php if($this->params['action']=='all_admin_moderators'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/admin_moderators/all_admin_moderators"><i class="fa fa-circle-o"></i> All Moderators</a></li>
                <li class="shadow_right <?php if($this->params['action']=='add_admin_moderator'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/admin_moderators/add_admin_moderator"><i class="fa fa-circle-o"></i> Add Moderator</a></li>
              </ul>
            </li>
	<?php } ?>
	
	<!-- User -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='deliveries' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='deliveries' && $this->params['action']<>'dashboard'){?> active <?php }?>" >
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Delivery Boy Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class="treeview-menu ">
								<li class="shadow_right <?php if($this->params['action']=='all_delivery_boy'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/deliveries/all_delivery_boy"><i class="fa fa-circle-o"></i> All Delivery Boys</a></li>
									<?php if(!empty($permissions)){ ?>
											<?php foreach($permissions as $k=>$permission){?>
												<?php if($permission['AdminMenu']['controller']=='deliveries' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
													<li class="shadow_right <?php if($this->params['action']=='add_delivery_boy'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/deliveries/add_delivery_boy"><i class="fa fa-circle-o"></i> Add Delivery Boys</a></li>
												<?php }
											}
										} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='deliveries' && $this->params['action']<>'dashboard'){?> active <?php }?>" >
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/delivery boy management.png" />&nbsp;</i> <span>Delivery Boy Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class="treeview-menu ">
							<li class="shadow_right <?php if($this->params['action']=='all_delivery_boy'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/deliveries/all_delivery_boy"><i class="fa fa-circle-o"></i> All Delivery Boys</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_delivery_boy'){?> active <?php }?>"> <a href="<?php echo $this->webroot; ?>admin/deliveries/add_delivery_boy"><i class="fa fa-circle-o"></i> Add Delivery Boys</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
		</ul>
		</li>

			
			
<!-- Site Management -->
	<li class="shadow_right treeview">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Site Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu <?php if(($this->params['controller']=='banners') || ($this->params['controller']=='leftpanelbanners') || ($this->params['controller']=='pages')){?> active <?php }?>">
			<!-- Banner -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='banners' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='banners'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Banner Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_banner'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/banners/all_banner"><i class="fa fa-circle-o"></i> All Banner</a></li>
									<?php if(!empty($permissions)){ ?>
										<?php foreach($permissions as $k=>$permission) {?>
											<?php if($permission['AdminMenu']['controller']=='banners' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
												<li class="shadow_right <?php if($this->params['action']=='add_banner'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/banners/add_banner"><i class="fa fa-circle-o"></i> Add Banner</a></li>
											<?php }
												}
									} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='banners'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/banner management.png" />&nbsp;</i> <span>Banner Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_banner'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/banners/all_banner"><i class="fa fa-circle-o"></i> All Banner</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_banner'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/banners/add_banner"><i class="fa fa-circle-o"></i> Add Banner</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
			
			<!-- left menu Banner -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='leftpanelbanners' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='leftpanelbanners'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Left Menu Banner</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_banners'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/leftpanelbanners/all_banners"><i class="fa fa-circle-o"></i> All Banner</a></li>
									<?php if(!empty($permissions)){ ?>
										<?php foreach($permissions as $k=>$permission) {?>
											<?php if($permission['AdminMenu']['controller']=='leftpanelbanners' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
												<li class="shadow_right <?php if($this->params['action']=='add_banner'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/leftpanelbanners/add_banners"><i class="fa fa-circle-o"></i> Add Banner</a></li>
											<?php }
												}
									} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='leftpanelbanners'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/left manu banner.png" />&nbsp;</i> <span>Left Menu Banner</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_banners'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/leftpanelbanners/all_banners"><i class="fa fa-circle-o"></i> All Banner</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_banners'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/leftpanelbanners/add_banners"><i class="fa fa-circle-o"></i> Add Banner</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
			
			<!-- Pages -->
		<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
				<li class=" shadow_right treeview <?php if($this->params['controller']=='pages'){?> active <?php }?>">
				  <a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/pages management.png" />&nbsp;</i> <span>Pages Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class=" treeview-menu">
					<li class="shadow_right <?php if($this->params['action']=='all_pages'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pages/all_pages"><i class="fa fa-circle-o"></i> All Pages</a></li>
				  </ul>
				</li>
		<?php }else{ ?>
		<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='pages' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='pages'){?> active <?php }?>">
								  <a href="#">
									<i class=""><img src="<?php echo $this->webroot; ?>admin/images/pages management.png" />&nbsp;</i> <span>Pages Management</span> <i class="fa fa-angle-left pull-right"></i>
								  </a>
								  <ul class=" treeview-menu">
									<li class="shadow_right <?php if($this->params['action']=='all_pages'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pages/all_pages"><i class="fa fa-circle-o"></i> All Pages</a></li>
								  </ul>
								</li>
				<?php	} 
					}
				}
		}?>
			
			
			</ul>
			</li>
			
			
	<li class="shadow_right treeview">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Product Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
			<!-- Product -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='products' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='products'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Product Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_products'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/products/all_products"><i class="fa fa-circle-o"></i> All Products</a></li>
									<?php if(!empty($permissions)){ ?>
										<?php foreach($permissions as $k=>$permission) {?>
											<?php if($permission['AdminMenu']['controller']=='products' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
												<li class="shadow_right <?php if($this->params['action']=='add_product'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/products/add_product"><i class="fa fa-circle-o"></i> Add Products</a></li>
											<?php }
												}
									} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
					<li class=" shadow_right treeview <?php if($this->params['controller']=='products'){?> active <?php }?>">
					  <a href="#">
						<i class=""><img src="<?php echo $this->webroot; ?>admin/images/product management.png" />&nbsp;</i> <span>Product Management</span> <i class="fa fa-angle-left pull-right"></i>
					  </a>
					  <ul class=" treeview-menu">
						<li class="shadow_right <?php if($this->params['action']=='all_products'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/products/all_products"><i class="fa fa-circle-o"></i> All Products</a></li>
						<li class="shadow_right <?php if($this->params['action']=='add_product'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/products/add_product"><i class="fa fa-circle-o"></i> Add Products</a></li>
					  </ul>
					</li>
				<?php }
			} ?>
			
			<!-- Category -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='categories' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='categories'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Category Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_category'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/categories/all_category"><i class="fa fa-circle-o"></i> All Category</a></li>
								<?php if(!empty($permissions)){ ?>
									<?php foreach($permissions as $k=>$permission) {?>
										<?php if($permission['AdminMenu']['controller']=='categories' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
											<li class="shadow_right <?php if($this->params['action']=='add_category'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/categories/add_category"><i class="fa fa-circle-o"></i> Add Category</a></li>
										<?php }
									}
								} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='categories'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/category management.png" />&nbsp;</i> <span>Category Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_category'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/categories/all_category"><i class="fa fa-circle-o"></i> All Category</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_category'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/categories/add_category"><i class="fa fa-circle-o"></i> Add Category</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
			
			<!-- Manufacturer -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='manufacturers' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='manufacturers'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Brand Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_manufacturer'){?>active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/manufacturers/all_manufacturer"><i class="fa fa-circle-o"></i> All Brands</a></li>
									<?php if(!empty($permissions)){ ?>
										<?php foreach($permissions as $k=>$permission) {?>
											<?php if($permission['AdminMenu']['controller']=='manufacturers' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
												<li class="shadow_right <?php if($this->params['action']=='add_manufacturer'){?>active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/manufacturers/add_manufacturer"><i class="fa fa-circle-o"></i> Add Brands</a></li>
											<?php }
												}
									} ?>
							  </ul>
							</li>
					<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='manufacturers'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/brand management.png" />&nbsp;</i> <span>Brand Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_manufacturer'){?>active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/manufacturers/all_manufacturer"><i class="fa fa-circle-o"></i> All Brands</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_manufacturer'){?>active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/manufacturers/add_manufacturer"><i class="fa fa-circle-o"></i> Add Brands</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
			
			<!-- Attribute -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='attributes' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='attributes'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Attribute Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_attribute'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/attributes/all_attribute"><i class="fa fa-circle-o"></i> All Attributes</a></li>
									<?php if(!empty($permissions)){ ?>
										<?php foreach($permissions as $k=>$permission) {?>
											<?php if($permission['AdminMenu']['controller']=='attributes' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
												<li class="shadow_right <?php if($this->params['action']=='add_attribute'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/attributes/add_attribute"><i class="fa fa-circle-o"></i> Add Attributes</a></li>
											<?php }
										}
									} ?>
							  </ul>
							</li>
					<?php }
				}
			}else{ ?>
				<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='attributes'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/attribute management.png" />&nbsp;</i> <span>Attribute Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_attribute'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/attributes/all_attribute"><i class="fa fa-circle-o"></i> All Attributes</a></li>
								<li class="shadow_right <?php if($this->params['action']=='add_attribute'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/attributes/add_attribute"><i class="fa fa-circle-o"></i> Add Attributes</a></li>
							  </ul>
							</li>
					<?php }
				} ?>
				
				<!-- UnitType -->
				<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='unit_types' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
								<li class=" shadow_right treeview <?php if($this->params['controller']=='unit_types'){?> active <?php }?>">
								  <a href="#">
									<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Unit Types Management</span> <i class="fa fa-angle-left pull-right"></i>
								  </a>
								  <ul class=" treeview-menu">
									<li class="shadow_right <?php if($this->params['action']=='all_unit_type'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/unit_types/all_unit_type"><i class="fa fa-circle-o"></i> All Types Of Units</a></li>
										<?php if(!empty($permissions)){ ?>
											<?php foreach($permissions as $k=>$permission) {?>
												<?php if($permission['AdminMenu']['controller']=='units' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
													<li class="shadow_right <?php if($this->params['action']=='add_unit_type'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/unit_types/add_unit_type"><i class="fa fa-circle-o"></i> Add Types Of Units</a></li>
												<?php }
											} 
										} ?>
								  </ul>
								</li>
						<?php }
					}
			}else{ ?>
				<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='unit_types'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/unit type management.png" />&nbsp;</i> <span>Unit Types Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_unit_type'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/unit_types/all_unit_type"><i class="fa fa-circle-o"></i> All Types Of Units</a></li>
								<li class="shadow_right <?php if($this->params['action']=='add_unit_type'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/unit_types/add_unit_type"><i class="fa fa-circle-o"></i> Add Types Of Units</a></li>
							  </ul>
							</li>
					<?php }
				} ?>
				
				<!-- Wishlist -->
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
				<li class=" shadow_right treeview <?php if($this->params['controller']=='wishlists'){?> active <?php }?>">
				  <a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/wish management.png" />&nbsp;</i> <span>Wishlist Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class=" treeview-menu">
					<li class="shadow_right <?php if($this->params['action']=='all_wishlist'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/wishlists/all_wishlist"><i class="fa fa-circle-o"></i> All Wishlist</a></li>
				  </ul>
				</li>
			<?php }else{ ?>
			<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='wishlists' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='wishlists'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/wish management.png" />&nbsp;</i> <span>Wishlist Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_wishlist'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/wishlists/all_wishlist"><i class="fa fa-circle-o"></i> All Wishlist</a></li>
							  </ul>
							</li>
					<?php } 
					}
				}
			}?>
			
			<!-- WaitList -->
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
				<li class=" shadow_right treeview <?php if($this->params['controller']=='waitings'){?> active <?php }?>">
				  <a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/wait list management.png" />&nbsp;</i> <span>WaitList Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class=" treeview-menu">
					<li class="shadow_right <?php if($this->params['action']=='all_waitings'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/waitings/all_waitings"><i class="fa fa-circle-o"></i> All WaitList</a></li>
				  </ul>
				</li>
			<?php }else{ ?>
			<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='waitings' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='waitings'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/wait list management.png" />&nbsp;</i> <span>WaitList Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_waitings'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/waitings/all_waitings"><i class="fa fa-circle-o"></i> All WaitList</a></li>
							  </ul>
							</li>
					<?php }
					}
				}
			} ?>
				
			<!-- Suggestion -->
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
				<li class=" shadow_right treeview <?php if($this->params['controller']=='suggestions'){?> active <?php }?>">
				  <a href="#">
					<i class=""><img src="<?php echo $this->webroot; ?>admin/images/suggest management.png" />&nbsp;</i> <span>Suggestion Management</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class=" treeview-menu">
					<li class="shadow_right <?php if($this->params['action']=='all_suggestion'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/suggestions/all_suggestion"><i class="fa fa-circle-o"></i> All Suggestions</a></li>
				  </ul>
				</li>
			<?php }else{ ?>
			<?php if(!empty($permissions)){ ?>
					<?php foreach($permissions as $k=>$permission) {?>
						<?php if($permission['AdminMenu']['controller']=='suggestions' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
							<li class=" shadow_right treeview <?php if($this->params['controller']=='suggestions'){?> active <?php }?>">
							  <a href="#">
								<i class=""><img src="<?php echo $this->webroot; ?>admin/images/suggest management.png" />&nbsp;</i> <span>Suggestion Management</span> <i class="fa fa-angle-left pull-right"></i>
							  </a>
							  <ul class=" treeview-menu">
								<li class="shadow_right <?php if($this->params['action']=='all_suggestion'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/suggestions/all_suggestion"><i class="fa fa-circle-o"></i> All Suggestions</a></li>
							  </ul>
							</li>
					<?php }
					}
				}
			} ?>
			</ul>
		</li>
			
			
			
	<li class="shadow_right treeview">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Warehouse Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
			<!-- WareHouse -->
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='warehouses' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='warehouses'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>WareHouse Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_warehouse'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/warehouses/all_warehouse"><i class="fa fa-circle-o"></i> All WareHouse</a></li>
								<?php if(!empty($permissions)){ ?>
									<?php foreach($permissions as $k=>$permission) {?>
										<?php if($permission['AdminMenu']['controller']=='warehouses' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
											<li class="shadow_right <?php if($this->params['action']=='add_warehouse'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/warehouses/add_warehouse"><i class="fa fa-circle-o"></i> Add WareHouse</a></li>
										<?php }
									}
								} ?>
						  </ul>
						</li>
				<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='warehouses'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/warehouse management.png" />&nbsp;</i> <span>WareHouse Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_warehouse'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/warehouses/all_warehouse"><i class="fa fa-circle-o"></i> All WareHouse</a></li>
							<li class="shadow_right <?php if($this->params['action']=='add_warehouse'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/warehouses/add_warehouse"><i class="fa fa-circle-o"></i> Add WareHouse</a></li>
						  </ul>
						</li>
				<?php }
			} ?>
			
	<!-- Pincode -->
		<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='pincodes' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='pincodes'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Pincode Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_pincode'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pincodes/all_pincode"><i class="fa fa-circle-o"></i> All Pincode</a></li>
								<?php if(!empty($permissions)){ ?>
									<?php foreach($permissions as $k=>$permission) {?>
										<?php if($permission['AdminMenu']['controller']=='pincodes' && $permission['AdminMenuPermission']['add_permission']=='Y'){ ?>
											<li class=" shadow_right<?php if($this->params['action']=='add_pincode'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pincodes/add_pincode"><i class="fa fa-circle-o"></i> Add Pincode</a></li>
										<?php }
									}
								} ?>
						  </ul>
						</li>
				<?php }
				}
		}else{ ?>
			<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
					<li class=" shadow_right treeview <?php if($this->params['controller']=='pincodes'){?> active <?php }?>">
					  <a href="#">
						<i class=""><img src="<?php echo $this->webroot; ?>admin/images/pincoad management.png" />&nbsp;</i> <span>Pincode Management</span> <i class="fa fa-angle-left pull-right"></i>
					  </a>
					  <ul class=" treeview-menu">
						<li class="shadow_right <?php if($this->params['action']=='all_pincode'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pincodes/all_pincode"><i class="fa fa-circle-o"></i> All Pincode</a></li>
						<li class="shadow_right <?php if($this->params['action']=='add_pincode'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/pincodes/add_pincode"><i class="fa fa-circle-o"></i> Add Pincode</a></li>
					  </ul>
					</li>
					<?php }
			} ?>
			</ul>
		</li>
			
	<li class="shadow_right treeview">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Order Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
		<!-- Order -->
		<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
			<li class=" shadow_right treeview <?php if($this->params['controller']=='orders'){?> active <?php }?>">
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/order management.png" />&nbsp;</i> <span>Order Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class=" treeview-menu">
                <li class="shadow_right <?php if($this->params['action']=='all_orders'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/orders/all_orders"><i class="fa fa-circle-o"></i> All Orders</a></li>
              </ul>
            </li>
		<?php }else{ ?>
		<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='orders' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='orders'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/order management.png" />&nbsp;</i> <span>Order Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_orders'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/orders/all_orders"><i class="fa fa-circle-o"></i> All Orders</a></li>
						  </ul>
						</li>
				<?php }
				}
			}
		} ?>
		
		<!-- Shipping -->
		<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
			<li class=" shadow_right treeview <?php if($this->params['controller']=='shippings'){?> active <?php }?>">
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/shipping management.png" />&nbsp;</i> <span>Shipping Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class=" treeview-menu">
                <li class="shadow_right <?php if($this->params['action']=='all_shipping_details'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/shippings/all_shipping_details"><i class="fa fa-circle-o"></i> All Shipping Details</a></li>
              </ul>
            </li>
		<?php }else{ ?>
		<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='shippings' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='shippings'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/shipping management.png" />&nbsp;</i> <span>Shipping Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_shipping_details'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/shippings/all_shipping_details"><i class="fa fa-circle-o"></i> All Shipping Details</a></li>
						  </ul>
						</li>
				<?php }
				}
			}
		} ?>
		</ul>
	</li>
			

	
		
	<li class="shadow_right treeview">
		<a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/admin management.png" />&nbsp;</i> <span>Contact Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
		<ul class="treeview-menu ">
		<!-- Newsletter -->
		<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
			<li class=" shadow_right treeview <?php if($this->params['controller']=='newsletters'){?> active <?php }?>">
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/newsletter manegement.png" />&nbsp;</i> <span>Newsletter Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class=" treeview-menu">
                <li class="shadow_right <?php if($this->params['action']=='all_newsletters'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/newsletters/all_newsletters"><i class="fa fa-circle-o"></i> All Newsletter</a></li>
              </ul>
            </li>
		<?php }else{ ?>
		<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='newsletters' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='newsletters'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/newsletter manegement.png" />&nbsp;</i> <span>Newsletter Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_newsletters'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/newsletters/all_newsletters"><i class="fa fa-circle-o"></i> All Newsletter</a></li>
						  </ul>
						</li>
				<?php }
				}
			}
		} ?>
		
		<!-- Contact -->
		<?php if($this->Session->read('Auth.User.type')=='A'){ ?>
			<li class=" shadow_right treeview <?php if($this->params['controller']=='contacts'){?> active <?php }?>">
              <a href="#">
                <i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Contact Management</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class=" treeview-menu">
                <li class="shadow_right <?php if($this->params['action']=='all_contacts'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/contacts/all_contacts"><i class="fa fa-circle-o"></i> All Contacts</a></li>
              </ul>
            </li>
		<?php }else{ ?>
		<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='contacts' && $permission['AdminMenuPermission']['view_permission']=='Y'){ ?>
						<li class=" shadow_right treeview <?php if($this->params['controller']=='contacts'){?> active <?php }?>">
						  <a href="#">
							<i class=""><img src="<?php echo $this->webroot; ?>admin/images/contact.png" />&nbsp;</i> <span>Contact Management</span> <i class="fa fa-angle-left pull-right"></i>
						  </a>
						  <ul class=" treeview-menu">
							<li class="shadow_right <?php if($this->params['action']=='all_contacts'){?> active <?php }?>"><a href="<?php echo $this->webroot; ?>admin/contacts/all_contacts"><i class="fa fa-circle-o"></i> All Contacts</a></li>
						  </ul>
						</li>
				<?php }
				}
			}
		} ?>
		</ul>
	</li>
			
           <!-- <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>