        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
            <!-- navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $this->webroot; ?>admin/users/dashboard">
<?php echo $this->Html->image('/admin/assets/img/logo.png', array('alt' => 'Confiq')); ?>

                   
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- navbar-top-links -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- main dropdown -->
         
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-3x"></i>
                    </a>
                   
                    <ul class="dropdown-menu dropdown-user">
                        <!--<li><a href="#"><i class="fa fa-user fa-fw"></i>User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i>Settings</a>
                        </li>-->
						
						<li>
						<?php 
						echo $this->Html->link(
								'<i class="fa fa-file-text fa-fw"></i> Change Password',
								array(
									'controller' => 'users',
									'action' => 'change_password',
									'full_base' => true,
									'plugin'=>'admin'
									
								),
								array(
									'escape' => FALSE
								)
							);
						?>
						
                        </li>
                        <li class="divider"></li>
                        <li>
						<?php 
						echo $this->Html->link(
								'<i class="fa fa-sign-out fa-fw"></i> Logout',
								array(
									'controller' => 'users',
									'action' => 'logout',
									'full_base' => true,
									'plugin'=>'admin'
								),
								array(
									'escape' => FALSE
								)
							);
						?>
						
                        </li>
                    </ul>
                    <!-- end dropdown-user -->
                </li>
                <!-- end main dropdown -->
            </ul>
            <!-- end navbar-top-links -->

        </nav>
       