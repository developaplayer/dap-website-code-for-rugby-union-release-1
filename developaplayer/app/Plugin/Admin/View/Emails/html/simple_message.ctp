<table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial, sans-serif; color:#333; font-size:13px; line-height:22px; padding:0; margin:0px auto; box-shadow:0 0 6px #9a9a9a;">
  <tbody>
    <tr>
      <td height="94" align="center" valign="middle" bgcolor="#15373F"><img src="<?php echo FULL_BASE_URL.$this->webroot;?>images/logo.png" alt="Confiq" width="137" height="44" style="" /></td>
    </tr>
    <tr>
      <td align="left" valign="bottom" bgcolor="#7fd0f1"><img src="<?php echo FULL_BASE_URL.$this->webroot;?>images/email-banner.png" width="600" height="175" alt="" /></td>
    </tr>
    <tr>
      <td align="left" valign="top" style="padding-top:25px; padding-bottom:25px; padding-left:60px; padding-right:60px;">
       <p style="margin:0; padding:0; display:block">Dear <?php echo $user['name']?>,</p>
       <p style="margin:0; margin-bottom:15px padding:0; display:block"><?php echo $message; ?></p>
       <p style="margin:0; padding:0; display:block">Team Confiq :-) </p>
      </td>
    </tr>
    <tr>
      <td height="60" align="center" valign="middle" bgcolor="#15373f" style="color:#e4dfdf; font-size:11px;">
      Copyright &copy; 2014 Confiq. All Rights Reserved
      </td>
    </tr>
  </tbody>
</table>
