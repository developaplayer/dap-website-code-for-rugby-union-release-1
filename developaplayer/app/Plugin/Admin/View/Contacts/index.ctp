      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Contact header) -->
        <section class="content-header">
          <h1>
            Front Pages Details
            <small>advanced tables</small>
          </h1>
      
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Pages</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				
				<form method="post" action="<?php echo $this->webroot; ?>admin/contacts/contact_multiple_delete" enctype="multipart" id="delete_UnitType">
                  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y' && $this->Session->read('permissions.Contacts.delete')=='Y'))
					{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" class="btn btn-primary" onclick="return confirm('Sure You Want To Delete?');" value="Delete" />
					<?php } ?>
					<table id="example1" class="table table-bordered table-striped">
				 
                    <thead>
                      <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y' && $this->Session->read('permissions.Contacts.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
                        <th>First Name</th>
                        <th>Last Name</th>
						<th>Email</th>						
						<th>Phone</th>	
						<th>Message</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y' && $this->Session->read('permissions.Contacts.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>					
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($view_content as $k=>$contact){ ?>
                   <tr>
					 <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y' && $this->Session->read('permissions.Contacts.delete')=='Y'))
						{ ?>
                      <td>
					   <input class="checkbox_del" name="id[]" value="<?php echo $contact['Contact']['id'] ?>" type="checkbox" />
					  </td>		 			  					  
						<?php } ?>
						<td><?php echo $contact['Contact']['first_name'] ?></a></td>
                        <td><?php echo $contact['Contact']['last_name']; ?></td>                        
                        <td><?php echo $contact['Contact']['email']; ?></td>                        
                        <td><?php echo $contact['Contact']['phone']; ?></td> 
						<td><?php echo $contact['Contact']['message']; ?></td>						
						<td style="width:118px !important">
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Contacts.view')=='Y' && $this->Session->read('permissions.Contacts.delete')=='Y'))
						{ ?>																
						<a href="<?php echo $this->webroot; ?>admin/Contacts/delete_page/<?php echo base64_encode($contact['Contact']['id']); ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						<?php } ?>
						
						</td>
						
						
                      </tr>

					<?php } ?>
                    </tbody>
                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
