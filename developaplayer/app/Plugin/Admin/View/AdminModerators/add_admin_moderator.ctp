
<?php 

//pr($menus);

//exit;
?>
<script type="text/javascript">
//window.onload=load_country();
</script>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Admin Moderator Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
				  <form method="post" action="" enctype="multipart/form-data" id="form" onsubmit="return validate_form()">
					
					<?php
					
						echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'First Name',
													'type'=>'text',
													'name'=>'first_name',
													'placeholder'=>'Your First Name'
													)
												);
					
						echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Last Name',
													'type'=>'text',
													'name'=>'last_name',
													'placeholder'=>'Your Last Name'
													)
												);
					
						

						echo $this->Form->input('mobile_number',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Mobile',
													'type'=>'text',
													'name'=>'mobile_number',
													'placeholder'=>'Enter Mobile'
													)
												);
														
						echo $this->Form->input('address',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Address',
													'type'=>'text',
													'name'=>'address',
													'placeholder'=>'Enter Address'
													)
												);
										?>
										
					
					<h3>Login Details</h3>
                    
				  <?php
				  
					echo $this->Form->input('email',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Email address',
													'type'=>'email',
													'id'=>'email',
													'name'=>'email_address',
													'placeholder'=>'Enter Email',
													'onKeyUp'=>'checkEmail()',
													'onblur'=>'checkEmail()'
													)
												);
					?>
					<span id="errs" style="color:red;"></span>
					<?php
				  
				  	echo $this->Form->input('password',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Password',
													'type'=>'password',
													'name'=>'password',
													'placeholder'=>'Password'
													)
												);
				  
				   echo $this->Form->input('conf_password',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Type Password Again',
													'type'=>'password',
													'name'=>'conf_password',
													'placeholder'=>'Confirm Password'
													)
												);
						// pr($menus);
					  //exit;						
				?>
				
				
				<a style="width:100%" class="btn btn-primary various" href="#menu">Select Roles</a>
					
					 <div id="menu" style="width:400px; display:none;">
						<h3 style="border-bottom:2px solid; color:#2e6da4">Select Roles</h3>
			    <table width="400px">
					<?php foreach($menus as $k=>$menu)
					{ 
			
					?>
					<tr>
					<td>
					<b><?php echo $menu['AdminMenu']['menu_name'] ?></b></td>								
								<td><input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][view_permission]" value="Y" />
									<a href="javascript:void(0)">View</a>
								</td>								
																
								<td>
								   <input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][edit_permission]" value="Y" />&nbsp;
									<a href="javascript:void(0)">Edit</a>
								</td>								
								<td><input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][add_permission]" value="Y" />&nbsp;
									<a href="javascript:void(0)">Add
								</a>							
								</td>
								<td><input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id']?>][delete_permission]" value="Y" />&nbsp;
									<a href="javascript:void(0)">Delete</a>
							   </td>
							<td>
							<input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id']?>][delete_permission]" value="Y" />&nbsp;
									<a href="javascript:void(0)">Status</a>
								
							</td>
							
						  </tr>
				<?php } ?>
				
		</table>
	</div>
					  
				
				
				<?php
				   echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control select2',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'published',
																	'I'=>'unpublished'
																	)
																)
												);
				  ?>
				  
				 
				 
				   <div class="box-footer">
                    <!--<input type="submit" value="submit" class="btn btn-primary">-->
                  </div>
				  
                  </div><!-- /.box-body -->

                 
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
            
	  
     <script type="javascript/text">
	 
	 $(function () {
    //Initialize Select2 Elements
        $(".select2").select2();
      });
    </script>
	
<script type="text/javascript">
		
function checkEmail(){
	$('.box-footer > input').remove();
	var email=$("#email").val();
	$.ajax({
		type: 'post',
		url:'<?php echo $this->webroot; ?>admin/admin_moderators/check_exist_email/',
		data:{
			email:email
			},
		success: function(response){
			if(response==1)
			{
				$('#errs').html('Email Already Exist.');
				$('#email').val('');
				$('.box-footer > input').remove();
			}
			else
			{
				$('#errs').html('');
				$('.box-footer').html('<input type="submit" value="submit" class="btn btn-primary">');
				//$('#email').val('');
			}
		},
		error: function(response){
		}
	});
}	
</script>
     