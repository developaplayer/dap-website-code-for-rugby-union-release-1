 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>
		
		  <!-- Main content -->
        <section class="content">
		
		<div class="row">
            <div class="col-xs-12">

              <div class="box">
			   <div class="box-header">
                  <h3 class="box-title">User Data</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
				<!---<form method="post" type="multipart" action="<?php echo $this->webroot; ?>admin/users/add_user">--->
                <div class="box-body">
				  
				 <form method="post" action="" enctype="multipart" id="form">
                  <div class="box-body">
				<?php
				  
				  echo $this->Form->input('id',array(
													'div'=>'form-group',
													'label' =>false,
													'type'=>'hidden',
													'name'=>'id',
													'value'=>$moderator['User']['id']
													)
												);
												
				  echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'First Name',
													'type'=>'text',
													'name'=>'first_name',
													'placeholder'=>'Enter First Name',
													'value'=>$moderator['User']['first_name']
													)
												);
												
				  echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Last Name',
													'type'=>'text',
													'name'=>'last_name',
													'placeholder'=>'Enter Last Name',
													'value'=>$moderator['User']['last_name']
													)
												);
												

				  
				  echo $this->Form->input('mobile_number',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Email',
													'disabled'=>'disabled',
													'type'=>'text',
													'name'=>'mobile_number',
													'value'=>$moderator['User']['email_address']
													)
												);
				  
				  echo $this->Form->input('mobile_number',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Mobile',
													'type'=>'text',
													'name'=>'mobile_number',
													'placeholder'=>'Enter Mobile',
													'value'=>$moderator['User']['mobile_number']
													)
												);
												
				  echo $this->Form->input('address',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' =>'Address',
													'type'=>'text',
													'name'=>'address',
													'placeholder'=>'Enter Address',
													'value'=>$moderator['User']['address']
													)
												);
												
				 
										?>
										
										<a style="width:100%" class="btn btn-primary various" href="#menu">Select Roles</a>
					
					 <div id="menu" style="width:400px;display: none;">
						<h3 style="border-bottom:2px solid; color:#2e6da4">Select Roles</h3>
						<table width="400px">
					<?php foreach($menus as $k=>$menu){ ?>
								<tr>
								<td><b><?php echo $menu['AdminMenu']['menu_name'] ?></b></td>
								<td><input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][view_permission]" <?php if($menu['AdminMenuPermission']['view_permission']!='' && $menu['AdminMenuPermission']['view_permission']=='Y'){ ?> checked="checked" <?php } ?> value="Y" />&nbsp;<a href="javascript:void(0)">View</a></td>
								<input type="hidden" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][id]" value="<?php echo $menu['AdminMenuPermission']['id'] ?>" />
								
								
											<td>
												<input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][edit_permission]" <?php if($menu['AdminMenuPermission']['edit_permission']!='' && $menu['AdminMenuPermission']['edit_permission']=='Y'){ ?> checked="checked" <?php } ?> value="Y" />&nbsp;
													<a href="javascript:void(0)">Edit</a>
											</td>
										
												
								
											<td>
												<input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id'] ?>][add_permission]" <?php if($menu['AdminMenuPermission']['add_permission']!='' && $menu['AdminMenuPermission']['add_permission']=='Y'){ ?> checked="checked" <?php } ?> value="Y" />&nbsp;
												<a href="javascript:void(0)">Add</a>
											</td>
							
								
							
											<td>
												<input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id']?>][delete_permission]" <?php if($menu['AdminMenuPermission']['delete_permission']!='' && $menu['AdminMenuPermission']['delete_permission']=='Y'){ ?> checked="checked" <?php } ?> value="Y" />&nbsp;
													<a href="javascript:void(0)">Delete</a>
											</td>
												
							
											<td>
												<input type="checkbox" name="permission[<?php echo $menu['AdminMenu']['id']?>][status_permission]" <?php if($menu['AdminMenuPermission']['status_permission']!='' && $menu['AdminMenuPermission']['status_permission']=='Y'){ ?> checked="checked" <?php } ?> value="Y" />&nbsp;
													<a href="javascript:void(0)">Status</a>
											</td>
								</tr>
					<?php } ?>
							</table>
					  </div>
				  
				 <div class="form-group has-feedback">
                    <label>Status</label>
                    <select name="status" class="required form-control select2" data-placeholder="Select a Status">
					
					<option <?php if($moderator['User']['status']=='A'){ ?> selected="selected" <?php }?> value="A">Published</option>
					<option <?php if($moderator['User']['status']=='I'){ ?> selected="selected" <?php }?> value="I">Unpublished</option>
					
					</select>
                    </div>
				  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
					 <b><a href="<?php echo $this->webroot; ?>admin/users/view_all">Cancel</a></b>
                  </div>
                </form>
			   </div>
			  </div>
			  </div>
			  </div>
		
		
		
		</section>
		
		 <script type="javascript/text">
	 
	 $(function () {
    //Initialize Select2 Elements
        $(".select2").select2();
	 });
	  
	 </script>