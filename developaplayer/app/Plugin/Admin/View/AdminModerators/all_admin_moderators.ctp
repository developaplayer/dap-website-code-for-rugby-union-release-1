      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Admin Moderators</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/admin_moderators/admin_moderator_multiple_delete" enctype="multipart" id="delete_admin_moderator">
				 <input class="selectall" type="checkbox" />
						  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px" />
						  <br />
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
						<th>Select</th>
                        <th>Admin Moderators</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
						<th>Options</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($admin_moderators as $k=>$admin_moderator){ 
					 //r($admin_moderator);
					 
					 ?>
                   <tr>
					 
					 <td><input class="checkbox_del" name="id[]" value="<?php echo $admin_moderator['User']['id'] ?>" type="checkbox" /></td>
					  
                     <!-- <td><a href="<?php //echo $this->webroot; ?>admin/admin_moderators/admin_moderator_detail/<?php echo base64_encode($admin_moderator['User']['id']); ?>"><?php echo $admin_moderator['User']['first_name']."&nbsp".$admin_moderator['User']['last_name'] ?></a></td>-->
					 
					 <td><?php echo $admin_moderator['User']['first_name']."&nbsp".$admin_moderator['User']['last_name'] ?></td>
                        
						<!--<td><a onclick="fancy_box('<?php echo $admin_moderator['User']['id'] ?>','<?php echo $admin_moderator['User']['email_address'] ?>')" class="<?php echo $admin_moderator['User']['id'] ?>" href="javascript:void(0)"><?php echo $admin_moderator['User']['email_address'] ?></a></td>-->
						<td><?php echo $admin_moderator['User']['email_address'] ?></td>						
                        <td><?php echo $admin_moderator['User']['mobile_number'] ?></td>
                        <td><?php echo $admin_moderator['User']['created'] ?></td>
						<td style="width:118px !important">
						
						<a href="<?php echo $this->webroot; ?>admin/admin_moderators/admin_moderator_detail/<?php echo base64_encode($admin_moderator['User']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>&nbsp;
						
						<a onclick="confirm_delete('<?php echo base64_encode($admin_moderator['User']['id']) ?>','<?php echo $admin_moderator['User']['first_name'].' '.$admin_moderator['User']['last_name'] ?>',' Admin Moderator','<?php echo $this->webroot.'admin/admin_moderators/admin_moderator_delete/'.base64_encode($admin_moderator['User']['id']) ?>')" href="javascript:void(0)" title="Delete" id="<?php echo base64_encode($admin_moderator['User']['id']) ?>">
						<img alt="Delete" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/delete.png" /></a>
						</td>
					  
                      </tr>

					<?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
						<th>Select</th>
                        <th>Admin Moderators</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Sign Up Date</th>
						<th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
	  <script>
	  
	  function confirm_delete(id,name,type,redirect_link)
		{
			 var x;
			if (confirm("are you sure you want to delete moderator ?") == true) {
				//x = "You pressed OK!";
				 window.location=(redirect_link)
			} else {
				//x = "You pressed Cancel!";
			}
			
			//alert();
				/*$('#dialog').dialogBox({
					hasClose: true,
					effect: 'fade',
					hasBtn: true,
					confirm:function(){						
							 window.location=(redirect_link)						
					},
					title: 'Confirm',
					content: 'Are you sure you want to delete'+type+' '+name+'?',
					callback: function(){
						console.log('loading over')
					}
				});*/
			}
		</script>	
			