
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Capability Matrix By Position</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					
					echo $this->Form->input('Position',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Position',
													'type'=>'select',
													'name'=>'player_position_id',
													'required'=>true,
													'data-placeholder'=>'Select a Position',
													'options'=>array(
																	''=>'Loading Position..'
																	)
																)
															);
					
					echo $this->Form->input('age_group',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Age Group',
													'type'=>'select',
													'name'=>'age_group_id',
													'required'=>true,
													'data-placeholder'=>'Select a Age Group',
													'options'=>array(
																	''=>'Loading Age Group..'
																	)
																)
															);
												
					echo $this->Form->input('lower_limit ',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Lower Limit',
													'type'=>'number',
													'id'=>'lower_limit',
													'name'=>'lower_limit',
													'value'=>$CapabilityArray['CapabilityMatrixAge']['lower_limit'],
													'placeholder'=>'Lower Limit'
													)
												);
					echo $this->Form->input('upper_limit ',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Upper Limit',
													'type'=>'number',
													'id'=>'upper_limit',
													'name'=>'upper_limit',
													'value'=>$CapabilityArray['CapabilityMatrixAge']['upper_limit'],
													'placeholder'=>'Upper Limit'
													)
												);
					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
	 <script type="text/javascript">
		$(function(){
			$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/capability_matrix_positions/get_position',
					data:{
						'id':'<?php echo $CapabilityArray['CapabilityMatrixAge']['player_position_id']?>'
					},
					success:function(response){
						$('#Position').html(response);
					}
			});
			
			$.ajax({
					type:"post",
					url:'<?php echo $this->webroot; ?>admin/capability_matrix_positions/get_agegroup',
					data:{
						'id':'<?php echo $CapabilityArray['CapabilityMatrixAge']['age_group_id']?>'
					},
					success:function(response){
						$('#age_group').html(response);
					}
			});
		});
</script>