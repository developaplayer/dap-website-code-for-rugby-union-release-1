<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Coach Level Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					if(!isset($this->request->data['name'])){
						$CoachLevel['name']="";
					}else{
						$CoachLevel['name']=$this->request->data['name'];
					}
					if(!isset($this->request->data['level_number'])){
						$CoachLevel['level_number']="";
					}else{
						$CoachLevel['level_number']=$this->request->data['level_number'];
					}
					/*echo $this->Form->input('category',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Category',
													'type'=>'select',
													'name'=>'category_id',
													'required'=>true,
													'data-placeholder'=>'Select a Category',
													'options'=>array(
																	''=>'Loading Category..'
																	)
																)
															);*/
					echo $this->Form->input('level_number',array(
														'div'=>'form-group has-feedback',
														'class'=>'required form-control',
														'label' => 'Coach Level Value',
														'type'=>'number',
														'id'=>'level_number',
														'name'=>'level_number',
														'value'=>$CoachLevel['level_number'],
														'placeholder'=>'Coach Level Value'
														)
					);
					
					echo $this->Form->input('name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Coach Level Text',
													'type'=>'text',
													'id'=>'name',
													'name'=>'name',
													'value'=>$CoachLevel['name'],
													'placeholder'=>'Coach Level Text'
													)
												);
												
					
												
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'Active',
																	'I'=>'InActive'
																	)
																)
															);
					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
<script type="text/javascript">
$(function(){
		$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/coach_levels/get_category',
				data:{
					'id':'no'
				},
				success:function(response){
					$('#category').html(response);
				}
		});
});
</script>