 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $pages[0]['Page']['title'] ?>
            <small>pages Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Pages Data</a></li>
            <li class="active"><?php echo $pages[0]['Page']['title'] ?></li>
          </ol>
        </section>
		
		  <!-- Main content -->
        <section class="content">
		
		<div class="row">
            <div class="col-xs-12">

              <div class="box">
			   <div class="box-header">
                  <h3 class="box-title">Page Data</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
				<!---<form method="post" type="multipart" action="<?php echo $this->webroot; ?>admin/users/add_user">--->
                <div class="box-body">
				  
				 <form method="post" action="" enctype="multipart" id="form">
                  <div class="box-body">
				  
				   <div class="form-group">
                      <input type="hidden" name="id" value="<?php echo $pages[0]['Page']['id'] ?>" id="id">
                    </div>
					
				    <div class="form-group  has-feedback">
                      <label for="exampleInputText">Page name</label>
                      <input type="text" name="title" value="<?php echo $pages[0]['Page']['title'] ?>" class="required form-control" id="title" placeholder="Page name">
                    </div>
					
					 <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Description</h3>
                 
                 
                </div><!-- /.box-header -->
                <div class="box-body pad">
                 
                    <textarea id="editor1" name="description" placeholder="Description" rows="10" cols="80">
					<?php echo $pages[0]['Page']['description'] ?>
                    </textarea>
                  
                </div>
              </div><!-- /.box -->
					
					
					 <div class="form-group has-feedback">
                      <label for="exampleInputText">Meta Title</label>
                      <input value="<?php echo $pages[0]['Page']['meta_title'] ?>" type="text" class="required form-control" name="meta_title" id="meta_title" placeholder="Meta Title">
                    </div>

						<div class="form-group has-feedback">
                      <label for="exampleInputText">Meta Keyword</label>
                      <input type="text"  value="<?php echo $pages[0]['Page']['meta_keyword'] ?>" class="required form-control" name="meta_keyword" id="meta_keyword" placeholder="pages Code">
                    </div>
				  
					
					  <div class="form-group has-feedback">
                      <label for="exampleInputText">Meta Description</label>
                      <input type="text"  value="<?php echo $pages[0]['Page']['meta_description'] ?>" class="required form-control" name="meta_description" id="meta_description" placeholder="pages Code">
                    </div>
                  
					
					<div class="form-group has-feedback">
                    <label>Status</label>
                    <select  name="status" class="required form-control select2" data-placeholder="Select a Status">
					<?php if($pages[0]['Page']['status']==1){ ?>
						<option value="1">Published</option>
					<option value="0">Unpublished</option>
					<?php }else{ ?>
						<option value="0">Unpublished</option>
					<option value="1">Published</option>
					<?php } ?>
					
					</select>
                    </div>
				  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
					 <b><a href="<?php echo $this->webroot; ?>admin/pagess/all_pagess">Cancel</a></b>
                  </div>
                </form>
			   </div>
			  </div>
			  </div>
			  </div>
		
		
		
		</section>
		<script type="text/javascript">
		(function () {
    //Initialize Select2 Elements
        $(".select2").select2();
		  });
		  
		  $(function () {
       // CKEDITOR.replace('editor1'); 
		CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        //$(".textarea").wysihtml5();
      });
		</script>