      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Pages</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Meta Title</th>
                        <th>Meta Keyword</th>
						<th>Meta Description</th>
						<th>Status</th>
						<th>Options</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					 foreach($pages as $k=>$page){ ?>
                   <tr>
                        <td><a href="<?php echo $this->webroot; ?>admin/Pages/Pages_detail/<?php echo base64_encode($page['Page']['id']); ?>">
						<?php echo $page['Page']['title'] ?></a></td>
						<td><?php echo $this->Text->excerpt(strip_tags($page['Page']['description']),15) ?></td>
                        <td><?php echo $page['Page']['meta_title'] ?></td>
						<td><?php echo $page['Page']['meta_keyword'] ?></td>
						<td><?php echo $page['Page']['meta_description'] ?></td>
						<td><?php if($page['Page']['status']==1){
											echo "Published";
										}else{
											echo "Unpublished";
										} ?>
						</td>						<td style="width:118px !important">
						<b>
					<?php if($this->Session->read('Auth.User.id')==1){	?>
						<a href="<?php echo $this->webroot; ?>admin/pages/pages_detail/<?php echo base64_encode($page['Page']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>
					<?php } ?>
					
			<?php if(!empty($permissions)){ ?>
				<?php foreach($permissions as $k=>$permission) {?>
					<?php if($permission['AdminMenu']['controller']=='pages' && $permission['AdminMenuPermission']['edit_permission']=='Y'){ ?>
						<a href="<?php echo $this->webroot; ?>admin/pages/pages_detail/<?php echo base64_encode($page['Page']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>
					<?php }
				}
			} ?>
						</td>
                      </tr>

					<?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Meta Title</th>
                        <th>Meta Keyword</th>
						<th>Meta Description</th>
						<th>Status</th>
						<th>Options</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
