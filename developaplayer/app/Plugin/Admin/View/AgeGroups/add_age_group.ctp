
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Age Group Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					
					echo $this->Form->input('title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Title Name',
													'type'=>'text',
													'id'=>'name',
													'name'=>'title',
													'placeholder'=>'Title Name'
													)
												);
					
					echo $this->Form->input('initial_age',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Lower Age',
													'type'=>'number',
													'id'=>'number',
													'name'=>'initial_age',
													'placeholder'=>'Lower Age'
													)
												);
												
					echo $this->Form->input('final_age',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Upper Age',
													'type'=>'number',
													'id'=>'final_number',
													'name'=>'final_age',
													'placeholder'=>'Upper Age'
													)
												);
												
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'Y'=>'Active',
																	'N'=>'InActive'
																	)
																)
															);
					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>