
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Banner  Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
					<?php
					
					echo $this->Form->input('name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Banner Name',
													'type'=>'text',
													'id'=>'name',
													'name'=>'name',
													'placeholder'=>'Banner Name'
													)
												);
					
					echo $this->Form->input('image',array(
													'div'=>'form-group has-feedback',
													'class'=>'required btn btn-primary',
													'label' => 'Banner Image For Best View  (1920px X 765px)',
													'type'=>'file',
													'style'=>array('width'=>'300px'),
													'name'=>'image',
													)
												);
												
					echo "<output id='list'></output>";
												
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'Active',
																	'I'=>'InActive'
																	)
																)
															);
					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
    <script type="text/javascript">
		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/>'
					].join('');

					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);
	  </script>