<?php echo $this->Html->script('/admin/js/ckeditor/ckeditor.js'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">About Us</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
                  <div class="box-body">
				  <?php
												
					echo $this->Form->input('id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'type'=>'hidden',
													'id'=>'id',
													'label'=>false,
													'name'=>'id',
													'value'=>$page['Page']['id'],
													'placeholder'=>'About Us'
													)
												);
												
					echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'type'=>'text',
													'id'=>'description',
													'name'=>'description',
													'value'=>$page['Page']['description'],
													'placeholder'=>'About Us'
													)
												);
												
					/*echo $this->Form->input('meta_title',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'type'=>'text',
													'id'=>'meta_title',
													'name'=>'meta_title',
													'value'=>$page['Page']['meta_title'],
													'placeholder'=>'Meta Title'
													)
												);
												
					echo $this->Form->textarea('meta_description',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'type'=>'text',
													'id'=>'meta_description',
													'name'=>'meta_description',
													'value'=>$page['Page']['meta_description'],
													'placeholder'=>'Meta Description',
													'style'=>'height: 150px'
													)
												);
					echo $this->Form->input('meta_keyword',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'type'=>'text',
													'id'=>'meta_keyword',
													'name'=>'meta_keyword',
													'value'=>$page['Page']['meta_keyword'],
													'placeholder'=>'Meta Keyword'
													)
												);*/
												
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<script>
$('document').ready(function(){
	CKEDITOR.replace('description');
	$('#description').before('<label for="description">Content</label>');
	$('#meta_description').before('<label for="meta_description">Meta Description</label>');
});
</script>