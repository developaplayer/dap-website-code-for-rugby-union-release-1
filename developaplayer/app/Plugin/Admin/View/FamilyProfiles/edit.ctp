<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
			 
              <div class="box box-primary">
			   <!-- form start -->
              <form method="post" action="<?php echo $this->webroot; ?>admin/family_profiles/edit/<?php echo $this->params['pass'][0]; ?>" enctype="multipart/form-data" id="form">
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
                  <h3 class="box-title">Input Family Details</h3>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>
				  <p style="padding-top:5px;"></p>
				 <?php
				 echo $this->Form->input('email_address',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Username (email)',
													'type'=>'email',
													'name'=>'email_address',
													'value'=>$user['User']['email_address'],
													'placeholder'=>'Email',
													'readonly'=>true
													)
												);
					echo $this->Form->input('password',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Password',
													'type'=>'password',
													'value'=>'Test Password',
													'placeholder'=>'Password',
													'readonly'=>true
													)
												);
												
					if($user['User']['status']=="A"){
					$options = array(array('name' => 'Active','value' => 'A','selected' => TRUE),'I'=>'InActive');
					}else{
						$options = array(array('name' => 'InActive','value' => 'I','selected' => TRUE),'A'=>'Active');
					}
				
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>$options
																)
															);
				 
				 ?>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Personal Information </div>
                </div><!-- /.box-header -->
                
				
				<!--- body --->
                  <div class="box-body">
				  
					<?php
					
					echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'First Name',
													'type'=>'text',
													'id'=>'first_name',
													'name'=>'first_name',
													'value'=>$user['User']['first_name'],
													'placeholder'=>'First Name'
													)
												);
												
					echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Last Name',
													'type'=>'text',
													'id'=>'last_name',
													'name'=>'last_name',
													'value'=>$user['User']['last_name'],
													'placeholder'=>'Last Name'
													)
												); ?>
					<div class="form-group has-feedback">
					<label for="dob">Date Of Birth</label>
						<div class='input-group date' id='datetimepicker1'>
							<input value="<?php echo $user['FamilyDetail']['dob']; ?>" type="text" name="dob" class="form-control pickDate" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<?php
										
					echo $this->Form->input('nationality',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Nationality',
													'type'=>'select',
													'name'=>'nationality',
													'data-placeholder'=>'Select a Nationality',
													'options'=>array(
																	''=>'Loading Nationalities',
																	)
																)
															);
					
					echo '<img src="'.$this->webroot.'uploads/profiles/family/thumb/'.$user['FamilyDetail']['profile_img'].'" />';
					
					echo $this->Form->input('profile_img',array(
													'div'=>'form-group has-feedback',
													'class'=>'btn btn-primary',
													'label' => 'Profile Picture',
													'type'=>'file',
													'id'=>'profile_img',
													'style'=>array('width'=>'300px'),
													'name'=>'profile_img'
													)
												);
												
					echo "<output id='list'></output>";
					
				
					/*echo $this->Form->input('player_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Player Name',
													'type'=>'select',
													'name'=>'player_id',
													'selected'=>isset($user['FamilyDetail']['player_id'])  ? $user['FamilyDetail']['player_id'] : "",													
													'data-placeholder'=>'Choose Player Name',
													'style'=>'width:100%',
													'options'=>$player_list
																)
															);
															
					echo $this->Form->input('relation_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Relationship Name',
													'type'=>'select',
													'selected'=>isset($user['FamilyDetail']['relation_id'])  ? $user['FamilyDetail']['relation_id'] : "",
													'name'=>'relation_id',
													'data-placeholder'=>'Select Specialities',
													'style'=>'width:100%',
													'options'=>$relation_list
																)
															);*/
					
										
					?>
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2>Relationship with player</h2>
					</div>	
					
					<div class="row" id="row_relation_main">
                         <div id="row_relation">
						
					<?php
					// pr($all_relation_player_id); exit;
                     $i=0;					
                      foreach($all_relation_player_id as $all_relation_player_ids)
					  {
					?>
					<div id="remove_<?php echo $i; ?>">
							<?php
								echo $this->Form->input('player_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'Choose Player Name',
													'type'=>'select',
													'onChange'=>'player_id_check(this)',
													'name'=>'player_id[]',
													'selected'=>$all_relation_player_ids['FamilyPlayerRelationship']['player_id'],
													'data-placeholder'=>'Select Your Player',
													'empty' => 'Select Your Player',
													'options'=>$player_list
																)
															);
													
								echo $this->Form->input('relation_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'My Relation with the player',
													'type'=>'select',
													'onChange'=>'relation_check(this)',
													'name'=>'relation_id[]',
													'selected'=>$all_relation_player_ids['FamilyPlayerRelationship']['relation_id'],
													'data-placeholder'=>'Select Your Relationship',
													'empty' => 'Select Your Relationship',
													'options'=>$relation_list
													)
												);
							if($i>0)	
							{?>								
						 <div onclick="remove_relation_div(<?php echo $i;?>)" class="removeIcon"><a style="cursor:pointer;float:right;"><span class="glyphicon glyphicon-remove-circle"></span></a></div>
							<?php } ?>
					</div> 	
					  <?php 
					   $i++;
					  }
					 ?>					  
			
			
					</div>
					<span class="hide" id="last_span"></span>
				</div>                    
              	<span class="addMoreBtn" id="p_scents">
                   <a href="javascript:void(0);"><span class="glyphicon glyphicon-plus-sign"></span> Add More</a>
                </span>
					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<script type="text/javascript">
function handleFileSelect(evt) {
	var files = evt.target.files;
	$('.hide_shown_image').hide();

	// Loop through the FileList and render image files as thumbnails.
	for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}
		
		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
		return function(e) {
			// Render thumbnail.
			var span = document.createElement('span');
			span.innerHTML = 
			[
			'<img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
			e.target.result,
			'" title="', escape(theFile.name), 
			'"/>'
			].join('');

			document.getElementById('list').insertBefore(span, null);
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
	}
}

document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);

$(function(){


	$('label[for="dob"]').after('<br />');
	$(".select2").select2();
	$('.ajax_error').remove();
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/family_profiles/get_nationality',
		data:{			
			'id':"<?php if(!empty($user['FamilyDetail']['nationality'])) { echo $user['FamilyDetail']['nationality'];} else { echo 'no'; } ?>"
			
		},
		success:function(response){
			$('#nationality').html(response);
		},
		error:function(response){
			$('#nationality').html('');
			$('#nationality').after('<span class="error ajax_error">There was an error while loading Nationality. Refresh the page.</span>');
		}
	});
	
	
});

$('#datetimepicker1').datetimepicker({
		
		format: 'YYYY-MM-DD'
	});
	
var clicks = 0;	
$(function() 
{
	var relation_html =$('#row_relation_hide').html();
	
    $('#p_scents').click(function() {
	clicks += 1;
	if(clicks<=2)
	{
	 $('#last_span').before('<div id="remove_'+clicks+'">'+relation_html+'<div class="removeIcon" onclick="remove_relation_div('+clicks+')"><a style="cursor:pointer;float:right;"><span class="glyphicon glyphicon-remove-circle"></span></a></div></div>'); 
	}
	
	if(clicks==2)
	{				
		 $('#p_scents').hide();				
	}	
	    			
    });
});
	
	
	var removeclicks=0;
function remove_relation_div(Id)
{
	clicks=clicks-1;
	//alert(clicks);
	removeclicks += 1;
	$('#remove_'+Id).remove('');
	$('#p_scents').show();
	
}
</script>
<div class="row" style="display:none;" id="row_relation_hide">
						 
					<?php
                    
							
								echo $this->Form->input('player_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'require form-control',
													'label' => 'Choose Player Name',
													'type'=>'select',
													'onChange'=>'player_id_check(this)',
													'name'=>'player_id[]',												
													'data-placeholder'=>'Select Your Player',
													'empty' => 'Select Your Player',
													'options'=>$player_list
																)
															);
													
								echo $this->Form->input('relation_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'require form-control',
													'label' => 'My Relation with the player',
													'type'=>'select',
													'name'=>'relation_id[]',													
													'data-placeholder'=>'Select Your Relationship',
													'empty' => 'Select Your Relationship',
													'options'=>$relation_list
													)
												);
					
?>					  
</div>
<script type="text/javascript">

function player_id_check(Obj)
{
	var current_id =Obj.value;
	Obj.setAttribute('name','');
	//alert(current_id);
	 //var i=0;
	$('select[name="player_id[]"]').each(function(){
	var each_id=this.value;
	//if(i>0)
	//{
		if(each_id==current_id)
		{
			
			alert('You already selected this player. please change');
		
			$(Obj).val('');
			
			
		}
	//}
	
    //i++;
  });
  Obj.setAttribute('name','player_id[]');
	
	
}

//=================With Relatioship=============================//

function relation_check(Obj)
{
	var current_id =Obj.value;
	//Obj.setAttribute('name','');	
	//==============get Palyer id==================//	
	 var each_player_id="";
	$('select[name="player_id[]"]').each(function(){
	  return each_player_id=this.value;
     
    });
	
	$('select[name="player_id[]"]').each(function(){
	  return player_name=this.text;
     
    });
	
	//=====================End======================//
	
	//======================Get Relation=============//
	
	$('select[name="relation_id[]"]').each(function(){
		relation_id=this.value;			
       return relation_id=this.value;
	   
    });
	
	$('select[name="relation_id[]"]').each(function(){
		relation_id=this.value;			
       /// return relation_text=$(this).text();
	   
    });
	
	if(each_player_id=="")
	{
		
		//alert('Please Select Player');
		//$('select[name="relation_id[]"]').val('');
		//$(Obj).val('');		
	}
	
	if(each_player_id!="" && relation_id!="")
	{
	  $.ajax({
			type: "POST",
			url: "<?php echo $this->webroot;?>families/relation_ship_exit_or_not",		
			data: 'player_id='+each_player_id+'&relation_id='+relation_id,
			success: function(data)
			{			
             if(data>0)
			 {
				//alert('Selected Relation With Palyer Aleready Exits,Please Select other.');
				//$('select[name="relation_id[]"]').val('');
				 
			 }				 
			}
		});
		
	}	
	
	 //alert(each_player_id+''+relation_id);
	
	// Obj.setAttribute('name','relation_id[]');
  
  //============================End====================//
}


</script> 
							