<?php  //print_r('2');
					// exit; ?>

<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
			 
              <div class="box box-primary">
			   <!-- form start -->
              <form method="post" action="<?php echo $this->webroot; ?>admin/coach_profile/edit/<?php echo $this->params['pass'][0]; ?>" enctype="multipart/form-data" id="form">
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
                  <h3 class="box-title">Input Coach Details</h3>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>
				  <p style="padding-top:5px;"></p>
				 <?php
				 echo $this->Form->input('email_address',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Username (email)',
													'type'=>'email',
													'name'=>'email_address',
													'value'=>$user['User']['email_address'],
													'placeholder'=>'Email',
													'readonly'=>true
													)
												);
					
												
					if($user['User']['status']=="A"){
						$options = array(array('name' => 'Active','value' => 'A','selected' => TRUE),'I'=>'InActive');
					}else{
						$options = array(array('name' => 'InActive','value' => 'I','selected' => TRUE),'A'=>'Active');
					}
				
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>$options
																)
															);
				 
				 ?>
				  <p style="padding-top:5px;"></p>
				  <div style="background-color:#ccc; padding:5px;font-size: 150%">Personal Information </div>
                </div><!-- /.box-header -->
                
				
				<!--- body --->
                  <div class="box-body">
				  
					<?php
					
					echo $this->Form->input('first_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Coach First Name',
													'type'=>'text',
													'id'=>'first_name',
													'name'=>'first_name',
													'value'=>$user['User']['first_name'],
													'placeholder'=>'Coach First Name'
													)
												);
												
					echo $this->Form->input('last_name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Coach Last Name',
													'type'=>'text',
													'id'=>'last_name',
													'name'=>'last_name',
													'value'=>$user['User']['last_name'],
													'placeholder'=>'Coach Last Name'
													)
												); ?>
					<div class="form-group has-feedback">
					<label for="dob">Date Of Birth</label>
						<div class='input-group date' id='datetimepicker1'>
							<input value="<?php echo $user['CoachDetail']['dob']; ?>" type="text" name="dob" class="form-control pickDate" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<?php
					
					echo $this->Form->input('height',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Height (cm)',
													'type'=>'number',
													'id'=>'height',
													'name'=>'height',
													'value'=>$user['CoachDetail']['height'],
													'placeholder'=>'Height'
													)
												);
												
					echo $this->Form->input('weight',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Weight (kg)',
													'type'=>'number',
													'id'=>'weight',
													'name'=>'weight',
													'value'=>$user['CoachDetail']['weight'],
													'placeholder'=>'Weight'
													)
												);
															
					echo $this->Form->input('coach_speciality_primary',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Primary Speciality',
													'type'=>'select',
													'name'=>'coach_speciality_primary',
													'data-placeholder'=>'Select Specialities',
													'style'=>'width:100%',
													'options'=>array(
																	''=>'Loading Specialities',
																	)
																)
															);
															
					echo $this->Form->input('coach_speciality_secondary',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Secondary Speciality',
													'type'=>'select',
													'name'=>'coach_speciality_secondary',
													'data-placeholder'=>'Select Specialities',
													'style'=>'width:100%',
													'options'=>array(
																	''=>'Loading Specialities',
																	)
																)
															);
															
					echo $this->Form->input('coach_certificate',array(
													'div'=>'form-group has-feedback update_certificate_field',
													'class'=>'required form-control',
													'label' => 'Certificate',
													'type'=>'select',
													'name'=>'certificate_id[]',
													'data-placeholder'=>'Select Certificate',
													'style'=>'width:100%',
													'options'=>array(
																	''=>'Loading Certificates',
																	)
																)
															);
															
															
					
					echo $this->Form->input('nationality',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Nationality',
													'type'=>'select',
													'name'=>'nationality',
													'empty'=>'-Select a Nationality-',
													'options'=>array(
																	''=>'Loading Nationalities',
																	)
																)
															);
															
					echo $this->Form->textarea('Notes',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'style'=>'word-wrap: break-word; overflow-wrap: break-word; width: 100%; white-space: pre-line;',
													'label' => 'Notes',
													'type'=>'text',
													'id'=>'Notes',
													'rows'=>'8',
													'cols'=>'50',
													'name'=>'note',
													'value'=>$user['CoachDetail']['note'],
													'placeholder'=>'Notes'
													)
												); 

					/*echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Description',
													'type'=>'textarea',
													'id'=>'description',
													'name'=>'description',
													'placeholder'=>'Product Description'
													)
												);*/	
												
					
					echo '<img src="'.$this->webroot.'uploads/profiles/coach/thumb/'.$user['CoachDetail']['profile_img'].'" />';
					
					echo $this->Form->input('profile_img',array(
													'div'=>'form-group has-feedback',
													'class'=>'btn btn-primary',
													'label' => 'Profile Picture',
													'type'=>'file',
													'id'=>'profile_img',
													'style'=>array('width'=>'300px'),
													'name'=>'profile_img'
													)
												);
												
					echo "<output id='list'></output>";
					
					$z = 1;
					$class = '';
					$div_id = 'video_default_div';
					foreach($user['Video'] as $video_link){
						if($z!=1){
							$class = 'video_link_'.$z.$video_link['id'];
							$div_id = 'video_'.$z.$video_link['id'];
						}
						//pr($video_link);
						if($video_link['video_type']==0)
						{
						?>
						<div id="<?php echo $div_id; ?>" class="form-group has-feedback <?php echo $class; ?>">
						<?php if($z==1){ ?>
							<label for="video_link_<?php echo $z.$video_link['id']; ?>">Video Link ( Youtube Link. Example: https://www.youtube.com/watch?v=1KqWm87BkAM)</label>
						<?php } ?>
							<input onblur='show_video("<?php echo $z.$video_link['id']; ?>")' type='text' id='video_link_<?php echo $z.$video_link['id']; ?>' placeholder='Video Link' value='<?php echo $video_link['video_link']; ?>' class='required form-control' name='video_link[]'>
							<?php if($z!=1){ ?>
									<a class="pull-right text-danger" onclick="delete_video('<?php echo $z.$video_link['id']; ?>')" href="javascript:void(0)">- Remove</a>
							<?php } ?>
						</div>
						<div id="youtube_video_<?php echo $z.$video_link['id']; ?>"><?php echo $video_link['video_iframe']; ?></div>
					<?php	$z++;
						}
					} 
					
					echo '<span onclick="add_video(2)" id="add_videos" class="btn btn-primary">+ Add More Videos</span>';
					
					?>

					<?php 
						$id = "";
						$i = 1;
						foreach($user['CoachCertificate'] as $certificate){
							if($i!=1){
								$id = $id.','.$certificate['certificate_id'];
							}else{
								$id = $certificate['certificate_id'];
							}
							$i++;
						}
					?>
					
					<br/>
					<div class="form-group has-feedback">
					<label for="dob">Select Lavel by Category</label>
						
					</div>
				
					<?php
					 $i=0;
					// echo '<pre>';
					// print_r($coachcategories);
					// exit;
					  foreach($coachcategories as $key=>$coachcategories_data)
					  {
					  ?>

                      <div><strong><?php echo $coachcategories_data['CoachCategory']['category_name']; ?><strong></div>	
					  <?php
					  //echo '<pre>';
					
					
							$all_data=array();
							foreach($coachcategories_data['LavelAssignment'] as $all_edit_lavels)
							{
								$all_data[]=$all_edit_lavels['lavel_id'];
							}
					                           if($coachcategories_data['CoachCategory']['id']==1)
											   {
												   $lave_array_select=$lave_array_select;
											   }
											   
											    if($coachcategories_data['CoachCategory']['id']==2)
											   {
												   $lave_array_select=$lave_array_select2;
											   }
											    if($coachcategories_data['CoachCategory']['id']==3)
											   {
												   $lave_array_select=$lave_array_select3;
											   }
											    if($coachcategories_data['CoachCategory']['id']==4)
											   {
												   $lave_array_select=$lave_array_select4;
											   }
											   
											  // print_r($lave_array_select);
						                        echo $this->Form->input('coach_level',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'label' => '',
													'selected'=>$lave_array_select,
													'multiple' => 'multiple',
													'type'=>'select',
													'name'=>'Coach_Level['.$coachcategories_data['CoachCategory']['id'].']',
													'empty'=>'--Select a Level--',
													'options'=>$CoachLevel
																)
															);						  							
							$i++;							
							//exit();
						}
					    ?>	
					</div>
					
					
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>
<script type="text/javascript">
function show_video(id){
	var link = $('#video_link_'+id).val();
	$('#youtube_video_'+id).html("Loading Video..");
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/load_video',
		data:{
			'link':$('#video_link_'+id).val()
		},
		success:function(response){
			$('#youtube_video_'+id).html(response);
		},
		error:function(response){
			$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
}

function add_video(num){
	var next_num = num+1;
	var total = $('div[class*="video_link_"]').length;
	var html_data = '<div class="form-group has-feedback video_link_'+num+'" id="video_'+num+'"><input onblur="show_video('+num+')" type="text" placeholder="Video Link" id="video_link_'+num+'" class="required form-control" name="video_link[]"><a href="javascript:void(0)" onclick="delete_video('+num+')" class="pull-right text-danger">- Remove</a></div"><div id="youtube_video_'+num+'"></div>';
	$('#add_videos').before(html_data);
	if((++total)==5){
		$('#add_videos').hide();
	}else{
		$('#add_videos').attr('onclick','add_video('+next_num+')');
	}
}

function delete_video(num){
	$('#add_videos').show();
	$('#video_'+num).remove();
	$('#youtube_video_'+num).remove();
}

function handleFileSelect(evt) {
	var files = evt.target.files;
	$('.hide_shown_image').hide();

	// Loop through the FileList and render image files as thumbnails.
	for (var i = 0, f; f = files[i]; i++) {

		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}
		
		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
		return function(e) {
			// Render thumbnail.
			var span = document.createElement('span');
			span.innerHTML = 
			[
			'<img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
			e.target.result,
			'" title="', escape(theFile.name), 
			'"/>'
			].join('');

			document.getElementById('list').insertBefore(span, null);
			};
		})(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
	}
}

document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);

$(function(){
	/*$('#password').after('<a class="text-danger password_toogle pull-right" onclick="show_password()" href="javascript:void(0)">show password</a>');*/
	$('label[for="dob"]').after('<br />');
	$(".select2").select2();
	$('.ajax_error').remove();
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/get_nationality',
		data:{
			'id':"<?php if(!empty($user['CoachDetail']['nationality'])) { echo $user['CoachDetail']['nationality'];} else { echo 'no'; } ?>"
		},
		success:function(response){
			$('#nationality').html(response);
		},
		error:function(response){
			$('#nationality').html('');
			$('#nationality').after('<span class="error ajax_error">There was an error while loading Nationality. Refresh the page.</span>');
		}
	});
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/get_speciality',
		data:{
			'id':'no'
		},
		success:function(response){
			$('#coach_speciality_primary').html(response);
			$('#coach_speciality_secondary').html(response);
			$('#coach_speciality_primary #'+<?php echo $user['CoachDetail']['primary_speciality_id']; ?>).attr('selected','selected');
			$('#coach_speciality_secondary #'+<?php echo $user['CoachDetail']['secondary_speciality_id']; ?>).attr('selected','selected');
		},
		error:function(response){
			$('#coach_speciality_primary').html('');
			$('#coach_speciality_secondary').html('');
			$('#coach_speciality_primary').after('<span class="error ajax_error">There was an error while loading Primary Speciality. Refresh the page.</span>');
			$('#coach_speciality_secondary').after('<span class="error ajax_error">There was an error while loading Secondary Speciality. Refresh the page.</span>');
		}
	});
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/coach_profile/get_certificate',
		data:{
			'id': '<?php if(!empty($id)){echo $id;} else { echo "no";} ?>'
		},
		success:function(response){
			$('.update_certificate_field').html(response);
			$(".select2").select2();
		},
		error:function(response){
			$('#coach_certificate').html('');
			$('.update_certificate_field').after('<span class="error ajax_error">There was an error while loading Ceritificates. Refresh the page.</span>');
		}
	});
});

$('#datetimepicker1').datetimepicker({
		//format: 'DD/MM/YYYY' // Disable Time Picker
		format: 'YYYY-MM-DD' // Disable Time Picker
	});
	

</script>