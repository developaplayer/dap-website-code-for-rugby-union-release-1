<?php echo $this->Html->css('/admin/admin_css_js/plugins/select2/select2.min'); ?>
<?php echo $this->Html->css('/admin/css/bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min'); ?>
<?php echo $this->Html->script('/js/moment-with-locales'); ?>
<?php echo $this->Html->script('/admin/js/bootstrap-datetimepicker'); ?>

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
			   <form method="post" action="" enctype="multipart/form-data" id="form">
              <div class="box box-primary">
			  <!-- form start -->
               
				
                <div class="box-header with-border">
				 <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                  <h3 class="box-title">Input Coach Rating</h3>
				  <p style="padding-top:5px;"></p>
				  <!--<div style="background-color:#ccc; padding:5px;font-size: 150%">Account Information </div>-->
				  <p style="padding-top:5px;"></p>			
                </div>
				
				
	
	<?php
	//pr($all_rating_list);
	//exit;
$k=1;
for($z=1;$z<=count($AgeGroup_list);$z++)
{
	if(!empty($all_rating_list[$k-1]['AddCoachRating']['age_group_id']))
	{
		$val=$all_rating_list[$k-1]['AddCoachRating']['age_group_id'];
	}
	else
	{
		$val=0;
	}
	 
	//echo $k= isset($all_rating_list[$k-1]['AddCoachRating']['coach_cat_id'])  ? $all_rating_list[$k-1]['AddCoachRating']['coach_cat_id'] : "";
?>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div style="background-color:#ccc; padding:5px;margin-bottom :5px;font-size: 150%">Rating Information </div>	
<?php			
			echo $this->Form->input('age_group',array(
					'div'=>'form-group has-feedback',
					'class'=>'required form-control',
					'label' => 'Age Group',
					'type'=>'select',
					'selected'=>$val,									
					'name'=>'age_group_'.$z,
					'data-placeholder'=>'Select Age Group',
					'style'=>'width:100%',
					'options'=>$AgeGroup_list
				)
			);	


			echo $this->Form->input('upper_lower_limit',array(
					'div'=>'form-group has-feedback update_certificate_field',
					'class'=>'required form-control',
					'label' => 'lower Upper Limit',
					'type'=>'select',
					'name'=>'upper_lower_limit_'.$z,
					'selected'=>isset($all_rating_list[$k-1]['AddCoachRating']['rating_limit_id'])  ? $all_rating_list[$k-1]['AddCoachRating']['rating_limit_id'] : "",
					'data-placeholder'=>'Select Upper lower Limit',
					'style'=>'width:100%',
					'options'=>$UpperLowerLimit_list,
				)
			);		
		?>
		
	
		
		<?php
				for($i=1;$i<=count($all_coach_category_list);$i++)
				{
		?>
		<div class=" col-lg-2 col-md-3 col-sm-3 col-xs-12">
		<?php					
					echo $this->Form->input('coach_category',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Label Category',
													'type'=>'select',
													'selected'=>$i,
													'disabled'=>'disabled',
													'name'=>'coach_category_'.$k,
													'data-placeholder'=>'Select Category',
													'style'=>'width:100%',
													'options'=>$all_coach_category_list
													)
												);
					echo $this->Form->input('coach_category_hidden',array('label' => false,'hidden'=>'hidden','value'=>$i,'name'=>'coach_category_hidden_'.$k,));				
					
					echo $this->Form->input('rating_val',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Rating',
													'type'=>'text',
													'value'=>isset($all_rating_list[$k-1]['AddCoachRating']['rating'])  ? $all_rating_list[$k-1]['AddCoachRating']['rating'] : "",
													'name'=>'rating_val_'.$k
																)
														);
														?>
					</div>
				<?php
				$k++;
				}
				
				?>
				
			</div>
			
			<?php	
		}
		?>
	</div>		
					<br/>
				
					
					</div>				  
				  <!-- /.box-body -->
                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>				  
				  <div class="box-footer">
                  </div>				  
                </form>
              </div>
		

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
<style>
.select2-selection__choice{
	color: black !important;
}
</style>
