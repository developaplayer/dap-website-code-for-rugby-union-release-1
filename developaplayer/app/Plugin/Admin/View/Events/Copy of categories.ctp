<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">All Event Categories </h1>
					<?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <div class="search_section">
                    <div class="col-lg-5 col-md-5 col-sm-5 text_box">
                    <div class="col-lg-3 col-md-4">
                    <label>Title :</label>
                    </div>
                    <div class="col-lg-9 col-md-8">
                    <input id="myName" type="text" name="myName" placeholder=""></div>
                    </div>
                    
                    <div class="col-lg-5 col-md-5 col-sm-5 category_box">
                    <div class="col-lg-4 col-md-5"><label>Category :</label></div>
                    <div class="col-lg-8 col-md-7">
                    <select id="room_number" name="room_number">
			<option>Categories</option>
			<option>Lorem ipsum dolor</option>
			<option>Lorem ipsum dolor</option>
			<option>Lorem ipsum dolor</option>
			<option>Lorem ipsum dolor</option>
			<option>Lorem ipsum dolor</option>
			</select>
            </div>
            </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 search_btn">
                    <button class="btn btn-primary search_btn" type="button">Search</button>
                    <!--<button class="btn btn-danger" type="button">Add</button>-->
                    </div>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <!--End Page Header -->
            </div>
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <button class="btn btn-danger add_btn" type="button">Add New</button>
                </div>
                <!--End Page Header -->
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Category Management</strong>
                        </div> 
						<?php $paginator=$this->Paginator; ?>
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
							<th><?php echo $paginator->sort('category_name', 'Category Name'); ?></th>
							<th><?php echo  $paginator->sort('delflag', 'Status'); ?></th> 
							<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php 
							if(is_array($categories_arr) && count($categories_arr)>0) 
							{ 
								foreach($categories_arr as $allcat)
								{
							?> 
							
								<tr>
								<td><?php echo $allcat['Category']['category_name']; ?></td>
								<td> 
								<?php  if($allcat['Category']['delflag']=='N') { $image='admin/icon_success.png'; } else { $image='admin/icon_error.gif'; } 
								echo $this->Html->image($image, array("alt" => "Logo","id" => "status_image_".$allcat['Category']['id'],"title" => "Click to change status","onclick"=>'javascript:change_status("'.$allcat['Category']['id'].'","admin/filelevels/change_status")',"style" =>"cursor:pointer;"));?> 
								</td>
								<td> 		
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_edit.gif',array('alt' => 'my account','title' => 'Edit')), 
								array('controller' => 'filelevels', 'action' => 'edit',2),array('escape'=>false)); ?> 
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_delete.png',array('alt' => 'Delete','title' => 'Delete')), 
								array('controller' => 'filelevels', 'action' => 'delete',6),array('escape'=>false),'Are you sure?'); ?>								
								<?php	 
								echo $this->Html->link($this->Html->image('admin/file_view.png',array('alt' => 'View','title' => 'View')), 
								array('controller' => 'filelevels', 'action' => 'details',2),array('escape'=>false)); ?> 
								</td>
								</tr>
							<?php
								}
							}
							else
							{
							?>		
								<tr>
								<td colspan="3" align="center" style="color:#FF0000;"  >* No record found.</td>
								</tr>
							<?php
							}
							?>
							</tbody> 
							</table>	

							<div class="pagination pagination-large">
								<ul class="pagination">
										<?php
											echo $this->Paginator->prev(__('<< prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											echo $this->Paginator->next(__('next >>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										?>
									</ul>
							</div>										
                        </div> 
                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
       