<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Edit CMS Page</h1>
					 <?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class="fa fa-plus-square-o fa-fw"></i>Add Page
                       <?php $page_detail['Page']['title']?>
					   </div>

                        <div class="panel-body">
                            <?php 
						echo $this->Form->create("Page", array('url'=>array('controller' =>'pages', 'action' => 'edit','plugin'=>'admin'),'id'=>'Page','enctype'=>'multipart/form-data'));			
							
							?>   
							<?php  
											   echo $this->Form->input('id',array(
																	'id'=>'id',
																	'value'=>$page_detail['Page']['id'],
																	'type'=>'hidden',
																	'required'=>true,
																	'label' => false
																	  )
																  ); 
											   ?>
								<div class="form-group">
                                            <label>Title <span class="required">*</span></label>
                                            <?php  
											   echo $this->Form->input('title',array(
																	'class'=>'form-control',
																	'id'=>'title',
																	'value'=>$page_detail['Page']['title'],
																	'type'=>'text',
																	'autofocus'=>true,
																	'required'=>true,
																	'onkeyup' => 'convertToSlug(this.value);',
																	'label' => false
																	  )
																  ); 
											   ?>
                                        </div>
                                      

							
										<div class="form-group">
                                            <label>Slug <span class="required">*</span></label>
                                            <?php  
											   echo $this->Form->input('slug',array(
																	'class'=>'form-control',
																	'type'=>'text',
																	'value'=>$page_detail['Page']['slug'],
																	'id'=>'slug',
																	'required'=>true,
																	'readonly' => true,
																	'label' => false
																	  )
																  ); 
											   ?>
                                        </div>
                                      
                                       
                                        <div class="form-group">
                                            <label>Content <span class="required">*</span></label>
                                           
											 <?php  
											   echo $this->Form->textarea('content', array(
																'class' => 'ckeditor',
																'value'=>$page_detail['Page']['content'],
																'required'=>true
													));
											   ?>
                                        </div>
                                        
                                        <input class="btn btn-success" type="submit" name="send_mass_message" value="Save">
										<?php 
											echo $this->Html->link(
													'Cancel',
													array(
														'controller' => 'pages',
														'action' => 'index',
														'full_base' => true,
														'plugin'=>'admin'
														
													),
													array('class'=>'btn btn-primary')
												);
											?>
								   
								   <?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
<script type="text/javascript">
function convertToSlug(Text)
{
   // var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
			   
	//alert(slug);
	$("#slug").val(Text)
        
}
</script>		
