<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Event: <?php echo $eventarr[0]['Event']['event_name']?></h1>
					<?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>
			 <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                   
					<a href="<?php echo $this->webroot ?>admin/events/event_details/<?php echo $eventarr[0]['Event']['id']?>" title="Add New" class="btn btn-danger add_btn">Back</a>
					
                </div>
            </div>
			<div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Reviewed User's List</strong>
                        </div> 
						<?php 
						$paginator=$this->Paginator; 
						$getPageNumber=$paginator->options['url'];
						if(is_array($getPageNumber) && count($getPageNumber)>0) 
						{
							$pageno=$getPageNumber['page'];
							$pagestat=count($eventarr);
						}
						else
						{
							$pageno=1;
							$pagestat=count($eventarr);
						}
						?>
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr style="background-color:#FFF">
							<th>Image</th>
							<th>Name</th>
							<th>Twitter Screen Name</th>
							<th>Email</th>
							<th>Review-Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							<th>User Went As</th>
							<th>Overall Rating</th> 
							
							<th>Choice of speakers</th> 
							<th>Topics & theme</th>
							<th>Workshops/Exhibits</th>
							<th>Programming & Planning</th>
							<th>Venue/Location</th>
							<th>Inspiration</th>
							<th>Information</th>
							<th>Hands-on workshops</th>
							<th>Networking</th>
							<th>Demos & Exhibits</th>
							<th>Best Three Words</th>
							<th style="width: 10%;">Review Tips</th> 
							<th>Is-Flaged</th>
							<th>Flaged By</th>
							<th>Flaged Reasons</th>
							<th>Flaged Email</th>
							<th>Up Vote</th>
							<th>Status</th>
							</tr>
							</thead>
							<tbody>
							<?php  
							
							if(is_array($eventarr) && count($eventarr)>0) 
							{ 
								foreach($eventarr as $allevent)
								{
								//print"<pre>";
								//print_r($allevent);
							?> 
							
								<tr>
								<td><img src="<?php echo $allevent['User']['twitter_profile_image_url_https']; ?>" alt="<?php echo $allevent['User']['name']; ?>"></td>
								<td><?php echo $allevent['User']['name']; ?></td>
								
								<td><?php echo $allevent['User']['twitter_screen_name']; ?></td>
								<td><?php echo $allevent['User']['email_address']; ?></td>
								<td> 
								
								<?php echo date("F j, Y",strtotime($allevent['Review']['created']));   ?>
								</td>
								<td><?php echo $allevent['Review']['i_went_as']; ?></td>
								<td>
									
									<?php if($allevent['Review']['overall_rating'] == '1'){ ?>
										Bad
									<?php } else if($allevent['Review']['overall_rating'] == '2'){?>
										Poor
									<?php }else if($allevent['Review']['overall_rating'] == '3'){?>
										Average
									<?php }else if($allevent['Review']['overall_rating'] == '4'){?>
										Good
									<?php }else{?>
										Excellent
									<?php }?>
								</td>
								<td><?php echo $allevent['Review']['choice_of_speakers']; ?></td>
								<td><?php echo $allevent['Review']['topics_theme']; ?></td>
								<td><?php echo $allevent['Review']['workshops_exhibits']; ?></td>
								<td><?php echo $allevent['Review']['programming_planning']; ?></td>
								<td><?php echo $allevent['Review']['venue_location']; ?></td>
								<td><?php echo $allevent['Review']['inspiration']; ?></td>
								<td><?php echo $allevent['Review']['information']; ?></td>
								<td><?php echo $allevent['Review']['hands_on_workshops']; ?></td>
								<td><?php echo $allevent['Review']['networking']; ?></td>
								<td><?php echo $allevent['Review']['demos_exhibits']; ?></td>
								<td><?php echo $allevent['Review']['describe_word_1']; ?>, <?php echo $allevent['Review']['describe_word_2']; ?>, <?php echo $allevent['Review']['describe_word_3']; ?></td>
								<td>
									<div style="max-height: 200px; overflow: auto; min-width: 305px;">
									<?php echo $allevent['Review']['review_tips']; ?>
									</div>
								</td>
								<td><span id="flaged_data_<?php echo $allevent['Review']['id']; ?>"><?php echo $allevent['Review']['is_flaged']; ?></span>
									<?php if($allevent['Review']['is_flaged'] == 'Y'){?>
									
										<br>
										<a id="flaged_<?php echo $allevent['Review']['id']; ?>" href="javascript: flag_off('<?php echo $allevent['Review']['id']; ?>')">Delete Flag</a>
									<?php }?>
								
								</td>
								<td><?php echo $allevent['Review']['flaged_by']; ?></td>
								<td><?php echo $allevent['Review']['flag_reason']; ?></td>
								<td><?php echo $allevent['Review']['flag_email']; ?></td>
								
								<td><?php echo $allevent['Review']['upvote']; ?></td>
								
								<td><?php if($allevent['Review']['delflag'] == 'N'){ ?>
								<a id="status_<?php echo $allevent['Review']['id']; ?>" href="javascript: change_review_status('<?php echo $allevent['Review']['id']; ?>')" title="Make Inactive">Active</a>
								<?php }else{ ?>
								<a id="status_<?php echo $allevent['Review']['id']; ?>" href="javascript: change_review_status('<?php echo $allevent['Review']['id']; ?>')" title="Make Active">Inactive</a>
								<?php }?>
								</td>

								<!--<td> 		
								<?php	 
								//echo $this->Html->link($this->Html->image('admin/icon_edit.gif',array('alt' => 'Edit','title' => 'Edit')), 
								//array('controller' => 'events', 'action' => 'edit_event',$allevent['Event']['id'],$pageno),array('escape'=>false)); ?> 
								
								<?php	 
								//echo $this->Html->link($this->Html->image('admin/icon_delete.png',array('alt' => 'Delete','title' => 'Delete')), 
								//array('controller' => 'events', 'action' => 'event_delete',$allevent['Event']['id'],$pageno,$pagestat),array('escape'=>false),'Are you sure?'); ?> 
								
								<?php	 
								//echo $this->Html->link($this->Html->image('admin/file_view.png',array('alt' => 'View','title' => 'View')), 
								//array('controller' => 'events', 'action' => 'event_details',$allevent['Event']['id'],$pageno),array('escape'=>false)); ?> 
								</td>-->
								</tr>
							<?php
								}
							}
							else
							{
							?>		
								<tr>
								<td colspan="6" align="center" style="color:#FF0000;"  >* No record found.</td>
								</tr>
							<?php
							}
							?>
							
							</tbody> 
							</table>	

							<div class="pagination pagination-large">
								<ul class="pagination">
										<?php
											echo $this->Paginator->prev(__('<< prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											echo $this->Paginator->next(__('next >>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										?>
									</ul>
							</div>										
                        </div> 
                    </div>
                    <!--End Notifications-->
                </div> 
		   </div> 
</div>

<script language="javascript"> 
$(document).ready(function(){
	$('.msg_success').delay(5000).fadeOut('slow', function() {}); 
}); 
function change_status(id,geturl)
{
	$.ajax({		
			type:'POST',
			url:base_url+geturl+'/'+id,
			success:function(jData){
			 var moddt=jData.trim(); 
				$("#status_image_"+id).attr('src',base_url+'img/admin/'+moddt);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}


function flag_off(review_id){
if(confirm('Do you really want to do this?')){
		$.ajax({		
			type:'POST',
			url:'<?php echo $this->webroot; ?>admin/events/flag_off/',
			dataType: "json",
			data: { review_id:review_id},
			success:function(jData){
				if(jData.flaged == 'Done'){
						$("#flaged_"+review_id).remove();
						$("#flaged_data_"+review_id).html('N');
							
					}
				else if(jData.flaged == 'error'){
							alert('Already flaged');
					}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
	}	
}

function change_review_status(review_id){
if(confirm('Do you really want to do this?')){
		$.ajax({		
			type:'POST',
			url:'<?php echo $this->webroot; ?>admin/events/change_review_status/',
			dataType: "json",
			data: { review_id:review_id},
			success:function(jData){
				if(jData.flaged == 'Done'){
						//$("#status_"+review_id).remove();
						$("#status_"+review_id).attr('title',jData.title);
						$("#status_"+review_id).html(jData.str);
							
					}
				else if(jData.flaged == 'error'){
							alert('Already flaged');
					}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
	}	
}
</script>
<style>
#wrapper {
 display:table;
}

</style>       