<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Event Management</h1>
					<?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                   
					<a href="<?php echo $this->webroot ?>admin/events/ad_event" title="Add New" class="btn btn-danger add_btn">Add New</a>
					
                </div>
                <!--End Page Header -->
				<span style="float:left;color:#96B442;" class="msg_success" ><?php $success_msg=$this->Session->flash('success'); if(!empty($success_msg)) { ?><strong><?php echo $success_msg; ?></strong><?php } ?></span>
            </div>
            
			<?php 
echo $this->Form->create("Category", array('url'=>array('controller' =>'events', 'action' => 'listing','plugin'=>'admin'),'id'=>'event','enctype'=>'multipart/form-data','class' => 'form-inline','role' => 'form')); 	
echo $this->Form->input('mode',array('type' => 'hidden', 'name' => 'mode','value'=>'1' ));		
?>
				<div class="row">
					<!-- Page Header -->
					<div class="col-lg-12">
						<div class="search_section">
                        <div class="col-lg-12 gap_n gap_tn12">
							<div class="col-lg-4 col-md-4 col-sm-4 text_box gap_n">
							<div class="col-lg-3 col-md-4">
							<label>Event:</label>
							</div>
							<div class="col-lg-9 col-md-8">
							<input id="myName" type="text" name="data[Event][event_name]" placeholder="Type Event Name" value="<?php echo $searchpa;?>" ></div>
							</div> 
                            
                          <!--<div class="col-lg-4 col-md-4 col-sm-4 text_box gap_n">
							<div class="col-lg-4 col-md-4">
							<label>Category:</label>
							</div>
							<div class="col-lg-8 col-md-8 gap_n">
								<select autofocus="1" id="category_id" class="form-control" name="data[Subcategory][category_id]">
								<option value="">Select Category</option>
								<option value="1">Event category 1 gfhdfhg</option>
								<option value="2">Event category 2 </option> 
							</select>
							</div>
							</div>-->
                            
                           
                            <div class="clear"></div>
                           </div>
                           
                                                   
                            
                             <div class="col-lg-12">
							<div class="col-lg-12 col-md-12 col-sm-12 search_btn gap_n"> 
							<input class="btn btn-primary search_btn1" type="submit" value="Search" name="Search">&nbsp;<input class="btn btn-primary search_btn1" type="button" onclick="window.location.href='<?php echo $this->webroot ?>admin/events/viewall'" value="View All" name="Search">
							</div>
                            </div>
							<div class="clearfix"></div>
						</div>
					</div> 
				</div>
<?php echo $this->Form->end(); ?>

			<div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Event Management</strong>
                        </div> 
						<?php 
						$paginator=$this->Paginator; 
						$getPageNumber=$paginator->options['url'];
						if(is_array($getPageNumber) && count($getPageNumber)>0) 
						{
							$pageno=$getPageNumber['page'];
							$pagestat=count($eventarr);
						}
						else
						{
							$pageno=1;
							$pagestat=count($eventarr);
						}
						?>
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
							<th>Logo</th>
							<th>Name</th>
							<th>Start Date</th>
							<th>Review Count</th>
							<th>Trending Now</th>
							<th>Status</th> 
							<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php  
							if(is_array($eventarr) && count($eventarr)>0) 
							{ 
								foreach($eventarr as $allevent)
								{
							?> 
							
								<tr>
								<td>
								<img src="<?php echo $this->webroot ?>uploaded/eventfiles/<?php echo "106_".$allevent['Event']['picture']; ?>" height="50" width="50" border="0" >
								</td>
								<td><?php echo $allevent['Event']['event_name']; ?></td>
								<td> 
								<?php echo $allevent['Event']['start_date']; ?>
								</td>
								<td>
								<?php 
								 $review_count = $this->Confiq->get_review_count_by_event_id($allevent['Event']['id']); 
								echo $review_count;
								?></td>
								<td>
								<?php if($review_count > 0){?>
								<?php  if($allevent['Event']['trending_now_status']=='Y') { $image='admin/icon_success.png'; } else { $image='admin/icon_error.gif'; } 
								echo $this->Html->image($image, array("alt" => "Logo","id" => "trending_now_status".$allevent['Event']['id'],"title" => "Click to change 'Trending Now' status","onclick"=>'javascript:change_trending_now_status("'.$allevent['Event']['id'].'","admin/events/change_trending_now_status")',"style" =>"cursor:pointer;"));?> 
								<?php }else{?>
									No reviews!
								<?php }?>
								</td>
								
								<td>
								<?php  if($allevent['Event']['active_status']=='Y') { $image='admin/icon_success.png'; } else { $image='admin/icon_error.gif'; } 
								echo $this->Html->image($image, array("alt" => "Logo","id" => "status_image_".$allevent['Event']['id'],"title" => "Click to change status","onclick"=>'javascript:change_status("'.$allevent['Event']['id'].'","admin/events/changeevent_status")',"style" =>"cursor:pointer;"));?> 
								</td>
								<td> 		
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_edit.gif',array('alt' => 'Edit','title' => 'Edit')), 
								array('controller' => 'events', 'action' => 'edit_event',$allevent['Event']['id'],$pageno),array('escape'=>false)); ?> 
								
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_delete.png',array('alt' => 'Delete','title' => 'Delete')), 
								array('controller' => 'events', 'action' => 'event_delete',$allevent['Event']['id'],$pageno,$pagestat),array('escape'=>false),'Are you sure?'); ?> 
								
								<?php	 
								echo $this->Html->link($this->Html->image('admin/file_view.png',array('alt' => 'View','title' => 'View')), 
								array('controller' => 'events', 'action' => 'event_details',$allevent['Event']['id'],$pageno),array('escape'=>false)); 
								?> 
								<?php	 
								echo $this->Html->link($this->Html->image('admin/user_review.png',array('alt' => 'Reviews','title' => 'Reviews')), 
								array('controller' => 'events', 'action' => 'reviews',$allevent['Event']['id']),array('escape'=>false)); 
								?> 
								</td>
								</tr>
							<?php
								}
							}
							else
							{
							?>		
								<tr>
								<td colspan="6" align="center" style="color:#FF0000;"  >* No record found.</td>
								</tr>
							<?php
							}
							?>
							
							</tbody> 
							</table>	

							<div class="pagination pagination-large">
								<ul class="pagination">
										<?php
											echo $this->Paginator->prev(__('<< prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											echo $this->Paginator->next(__('next >>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										?>
									</ul>
							</div>										
                        </div> 
                    </div>
                    <!--End Notifications-->
                </div> 
		   </div> 
</div>

<script language="javascript"> 
$(document).ready(function(){
	$('.msg_success').delay(5000).fadeOut('slow', function() {}); 
}); 
function change_status(id,geturl)
{
	$.ajax({		
			type:'POST',
			url:base_url+geturl+'/'+id,
			success:function(jData){
			 var moddt=jData.trim(); 
				$("#status_image_"+id).attr('src',base_url+'img/admin/'+moddt);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}
function change_trending_now_status(id,geturl)
{
	if(confirm('Are you sure about this?')){
	$.ajax({		
			type:'POST',
			url:base_url+geturl+'/'+id,
			success:function(jData){
			 var moddt=jData.trim(); 
				$("#trending_now_status"+id).attr('src',base_url+'img/admin/'+moddt);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
	}	
}

</script>       