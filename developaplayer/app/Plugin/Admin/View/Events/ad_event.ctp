<?php
echo $this->Html->css('/admin/assets/datepicker/datepicker');
echo $this->Html->script('/admin/assets/datepicker/datepicker'); 
echo $this->Html->css('/admin/assets/datepicker/jquery-ui');
echo $this->Html->css('/admin/assets/datepicker/jquery-ui-timepicker-addon');
echo $this->Html->script('/admin/assets/datepicker/jquery-ui'); 
echo $this->Html->script('/admin/assets/datepicker/jquery-ui-timepicker-addon');
?>
 
<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Add Event</h1>
					 <?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Add Event</strong>
					   </div>

                        <div class="panel-body">
<?php 
echo $this->Form->create("Event", array('url'=>array('controller' =>'events', 'action' => 'ad_event','plugin'=>'admin'),'id'=>'Event','enctype'=>'multipart/form-data'));	 
?>  
 <div class="col-lg-12 gap_n">
  <div class="col-md-6 gap_n">
<div class="form-group">
<label>Select Category <span class="required">*</span></label>
<?php echo $this->Form->input('main_category',array('class'=> 'form-control', 'id'=>'main_category','options' => $option,'empty' =>'Select Category','autofocus'=>true,'required'=>true, 'onChange'=>'javascript:changecategory(this.value);', 'label'=>false)); ?> 
</div>
</div>
<?php $blankArr=array(); ?>
 <div class="col-md-6 gap_n">
<div class="form-group">
<label>Select SubCategory <span class="required">*</span></label>
<div id="subcategorydivid" >
<?php echo $this->Form->input('sub_category',array('class'=> 'form-control', 'id'=>'sub_category','options' => $blankArr,'empty' =>'Select Subcategory','autofocus'=>true, 'label'=>false)); ?>
</div> 
</div>
</div>
</div>

 <div class="col-lg-12 gap_n">
 <div class="col-md-6 gap_n">
 <div class="form-group">
<label>Select Country <span class="required">*</span></label>
<?php echo $this->Form->input('country_id',array('class'=> 'form-control', 'id'=>'country_id','options' => $optioncountry,'empty' =>'Select Country','autofocus'=>true,'required'=>true, 'onChange'=>'javascript:changestate(this.value);', 'label'=>false)); ?> 
</div>
</div>
 <div class="col-md-6 gap_n">
<div class="form-group">
<label>Select State <span class="required">*</span></label>
<div  id="statedivid">
<?php echo $this->Form->input('state_id',array('class'=> 'form-control', 'id'=>'state_id','options'=>$blankArr,'empty' =>'Select State','autofocus'=>true,'required'=>true,'onChange'=>'javascript:changecity(this.value);', 'label'=>false)); ?> 
</div>
</div>
</div>
</div>

 <div class="col-lg-12 gap_n">
 <div class="col-md-6 gap_n">
 <div class="form-group">
<label>Select City <span class="required">*</span></label>
<div  id="citydivid">
<?php echo $this->Form->input('city_id',array('class'=> 'form-control', 'id'=>'city_id','options'=>$blankArr,'empty' =>'Select City','autofocus'=>true,'required'=>true, 'label'=>false)); ?> 
</div>
</div>
</div>
 
  <div class="col-md-6 gap_n">
<div class="form-group">
<label>Event Name <span class="required">*</span></label>
<?php  
echo $this->Form->input('event_name',array('class'=>'form-control','id'=>'event_name','type'=>'text','autofocus'=>true,'required'=>true,'label' => false,'onblur'=>'convertToSlug(this.value)')); 
?>

<?php  
echo $this->Form->input('event_slug',array('class'=>'form-control','id'=>'event_slug','type'=>'text','readonly'=>true,'required'=>true,'label' => false,'placeholder'=>'Url Slug')); 
?>

</div>
</div>
</div>

 <div class="col-lg-12 gap_n">
 <div class="col-md-6 gap_n"><div class="form-group">
<label>Event Start Date <span class="required">*</span></label>
<?php  echo $this->Form->input('start_date',array('class'=>'form-control','id'=>'start_date_select','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div></div>

 <div class="col-md-6 gap_n"><div class="form-group">
<label>Event End Date <span class="required">*</span></label>
<?php  echo $this->Form->input('end_date',array('class'=>'form-control','id'=>'end_date_select','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div></div>
</div>
 <div class="col-lg-12 gap_n">
 <div class="col-md-6 gap_n"><div class="form-group">
<label>Twitter HashTag </label>
<?php  echo $this->Form->input('twitter_hash_tag',array('class'=>'form-control','id'=>'twitter_hash_tag','type'=>'text','autofocus'=>true,'required'=>false,'label' => false)); 
?>
</div></div>

 <div class="col-md-6 gap_n"><div class="form-group">
<label>Instagram ID </label>
<?php  echo $this->Form->input('instagram_id',array('class'=>'form-control','id'=>'instagram_id','type'=>'text','autofocus'=>true,'required'=>false,'label' => false)); 
?>
</div></div>

 <div class="col-md-12 gap_n"><div class="form-group">
<label>Facebook Url</label>
<?php  echo $this->Form->input('facebook_url',array('class'=>'form-control','id'=>'facebook_url','type'=>'text','autofocus'=>true,'required'=>false,'label' => false)); 
?>
</div></div>


</div>





 <div class="col-lg-12 gap_n">
 <div class="col-md-6 gap_n"><div class="form-group">
<label>Event Description <span class="required">*</span></label>
<?php  echo $this->Form->input('description',array('class'=>'form-control','id'=>'description','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div></div>

 <div class="col-md-6 gap_n"><div class="form-group">
<label>Event Picture</label>
<input  type="file"  name="file_nm" id="file_nm"   />
</div>

<div class="form-group">
<label>Event Link(Ex - http://www.example.com)</label>
<?php  echo $this->Form->input('link_on_picture',array('class'=>'form-control','id'=>'link_on_picture','type'=>'text','autofocus'=>true,'required'=>false,'label' => false)); 
?>
</div>
</div>
</div>

<input class="btn btn-success" type="submit" name="mode" value="Save"> 
<a class="btn btn-primary" href="<?php echo $this->webroot ?>admin/events/listing">Cancel</a>
<?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
<script>
  $(function() {
  
  	$("#start_date_select").datetimepicker({dateFormat: 'dd-mm-yy',timeFormat: 'HH:mm:ss', changeMonth: true, changeYear: true, yearRange:'1900:c'});
	
	$("#end_date_select").datetimepicker({dateFormat: 'dd-mm-yy',timeFormat: 'HH:mm:ss', changeMonth: true, changeYear: true, yearRange:'1900:c'}); 
  });
  </script>	
<script type="text/javascript"> 
function convertToSlug(Text)
{
	if(Text!=''){
	   Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
		$.ajax({		
			type:'POST',
			dataType: "json",
			url:base_url+'admin/events/check_event_slug/'+Text,
			success:function(jData){
			//alert(jData.slug);
			  if(jData.slug == 'match_found'){
				alert('This url slug is already in use, please try another');
				$("#event_name").val('');
				$("#event_slug").val('');
				$("#event_name").focus();
			  }else{
				$("#event_slug").val(Text);
			  }
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("Some server error occoured.");
			}
		});
	}	
}

function changecategory(categoryid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changesubcategory/'+categoryid,
			success:function(jData){
			  $("#subcategorydivid").html(jData);	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}

function changestate(countryid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changestate/'+countryid,
			success:function(jData){
			  $("#statedivid").html(jData);	 
			  $("#citydivid").html('<select required="required" autofocus="1" id="city_id" class="form-control" name="data[Event][city_id]"><option value="">Select City</option></select>');	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}

function changecity(stateid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changecity/'+stateid,
			success:function(jData){
			  $("#citydivid").html(jData);	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}


</script>		
