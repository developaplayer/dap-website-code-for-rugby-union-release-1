<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Add Event</h1>
					 <?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Add Event</strong>
					   </div>

                        <div class="panel-body">
<?php 
echo $this->Form->create("Event", array('url'=>array('controller' =>'events', 'action' => 'add','plugin'=>'admin'),'id'=>'Event','enctype'=>'multipart/form-data'));	 
?>  

<div class="form-group">
<label>Select Category <span class="required">*</span></label>
<?php echo $this->Form->input('main_category',array('class'=> 'form-control', 'id'=>'main_category','options' => $option,'empty' =>'Select Category','autofocus'=>true,'required'=>true, 'onChange'=>'javascript:changecategory(this.value);', 'label'=>false)); ?> 
</div>

<?php $blankArr=array(); ?>

<div class="form-group">
<label>Select SubCategory <span class="required">*</span></label>
<div id="subcategorydivid" >
<?php echo $this->Form->input('sub_category',array('class'=> 'form-control', 'id'=>'sub_category','options' => $blankArr,'empty' =>'Select Subcategory','autofocus'=>true,'required'=>true, 'label'=>false)); ?>
</div> 
</div>


 
<div class="form-group">
<label>Select Country <span class="required">*</span></label>
<?php echo $this->Form->input('country_id',array('class'=> 'form-control', 'id'=>'country_id','options' => $optioncountry,'empty' =>'Select Country','autofocus'=>true,'required'=>true, 'onChange'=>'javascript:changestate(this.value);', 'label'=>false)); ?> 
</div>


<div class="form-group">
<label>Select State <span class="required">*</span></label>
<div  id="statedivid">
<?php echo $this->Form->input('state_id',array('class'=> 'form-control', 'id'=>'state_id','options'=>$blankArr,'empty' =>'Select State','autofocus'=>true,'required'=>true,'onChange'=>'javascript:changecity(this.value);', 'label'=>false)); ?> 
</div>
</div>

<div class="form-group">
<label>Select City <span class="required">*</span></label>
<div  id="citydivid">
<?php echo $this->Form->input('city_id',array('class'=> 'form-control', 'id'=>'city_id','options'=>$blankArr,'empty' =>'Select City','autofocus'=>true,'required'=>true, 'label'=>false)); ?> 
</div>
</div>


 
<div class="form-group">
<label>Event Name <span class="required">*</span></label>
<?php  echo $this->Form->input('event_name',array('class'=>'form-control','id'=>'event_name','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div>

<div class="form-group">
<label>Event Start Date <span class="required">*</span></label>
<?php  echo $this->Form->input('start_date',array('class'=>'form-control','id'=>'start_date','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div>


<div class="form-group">
<label>Event End Date <span class="required">*</span></label>
<?php  echo $this->Form->input('end_date',array('class'=>'form-control','id'=>'end_date','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div>

<div class="form-group">
<label>Event Description <span class="required">*</span></label>
<?php  echo $this->Form->input('description',array('class'=>'form-control','id'=>'description','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div>

<div class="form-group">
<label>Event Picture <span class="required">*</span></label>
<input type="file" name="picture" >
</div>


<div class="form-group">
<label>Event Link(URL - if any) <span class="required">*</span></label>
<?php  echo $this->Form->input('link_on_picture',array('class'=>'form-control','id'=>'link_on_picture','type'=>'text','autofocus'=>true,'required'=>true,'label' => false)); 
?>
</div>


<input class="btn btn-success" type="submit" name="send_mass_message" value="Save"> 
<a class="btn btn-primary" href="<?php echo $this->webroot ?>admin/cities/listing">Cancel</a>
                                   <?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
<script type="text/javascript">
function convertToSlug(Text)
{
   // var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
			   
	//alert(slug);
	$("#slug").val(Text)
        
}

function changecategory(categoryid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changesubcategory/'+categoryid,
			success:function(jData){
			  $("#subcategorydivid").html(jData);	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}

function changestate(countryid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changestate/'+countryid,
			success:function(jData){
			  $("#statedivid").html(jData);	 
			  $("#citydivid").html('<select required="required" autofocus="1" id="city_id" class="form-control" name="data[Event][city_id]"><option value="">Select City</option></select>');	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}

function changecity(stateid)
{
	$.ajax({		
			type:'POST',
			url:base_url+'admin/events/changecity/'+stateid,
			success:function(jData){
			  $("#citydivid").html(jData);	 
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".err").html("Some server error occoured.</div>");
			}
		});
}


</script>		
