<div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Add Category</h1>
					 <?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

         
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Add Category</strong>
					   </div>

                        <div class="panel-body">
                            <?php 
						echo $this->Form->create("Category", array('url'=>array('controller' =>'events', 'action' => 'ad_category','plugin'=>'admin'),'id'=>'Category','enctype'=>'multipart/form-data'));			
				
							?> 
                            <div class="col-md-12 gap_n"> 
                            <div class="col-md-6 gap_n"> 
								<div class="form-group">
                                            <label>Category Name <span class="required">*</span></label>
                                            <?php  
											   echo $this->Form->input('category_name',array(
																	'class'=>'form-control',
																	'id'=>'category_name',
																	'type'=>'text',
																	'autofocus'=>true,
																	'required'=>true,
																	'label' => false
																	  )
																  ); 
											   ?>
                                        </div>
                                  </div> 
                                  
                                  <div class="col-md-6 gap_n"> </div>
                                       
                                  </div>      
                                        <input class="btn btn-success" type="submit" name="send_mass_message" value="Save"> 
										<a class="btn btn-primary" href="<?php echo $this->webroot ?>admin/events/categories">Cancel</a>
                                   <?php echo $this->Form->end(); ?>
                        </div>

                    </div>
                    <!--End Notifications-->
                </div>
				
          
		   </div>


         


        </div>
<script type="text/javascript">
function convertToSlug(Text)
{
   // var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
			   
	//alert(slug);
	$("#slug").val(Text)
        
}
</script>		
