<div id="page-wrapper">

<div class="row">
<!-- Page Header -->
<div class="col-lg-12">
<h1 class="page-header">Add Event</h1>
<?php echo $this->Session->flash(); ?>
</div>
<!--End Page Header -->
</div>


<div class="row">
<div class="col-lg-12">
<!-- Notifications-->
<div class="panel panel-primary">
<div class="panel-heading">
<i class="fa fa-plus-square-o fa-fw"></i>Add Event

</div>

<div class="panel-body">
<div class="col-lg-12 gap_n"> 
<?php 
echo $this->Form->create("Page", array('url'=>array('controller' =>'pages', 'action' => 'add','plugin'=>'admin'),'id'=>'FormAddCmsPage','enctype'=>'multipart/form-data'));			

?> 
<div class="col-md-4 gap_n">   
<div class="form-group">
<label>Title <span class="required">*</span></label>
<?php  
echo $this->Form->input('title',array(
                    'class'=>'form-control',
                    'id'=>'title',
                    'type'=>'text',
                    'autofocus'=>true,
                    'required'=>true,
                    'onkeyup' => 'convertToSlug(this.value);',
                    'label' => false
                      )
                  ); 
?>
</div>
</div>

<div class="col-md-4 gap_n"> 
<div class="form-group">
<label>Slug <span class="required">*</span></label>
<?php  
echo $this->Form->input('slug',array(
                    'class'=>'form-control',
                    'type'=>'text',
                    'id'=>'slug',
                    'required'=>true,
                    'readonly' => true,
                    'label' => false
                      )
                  ); 
?>
</div>
</div>

<div class="col-md-4 gap_n"> 
<div class="form-group">
<label>Content <span class="required">*</span></label>

<?php  
echo $this->Form->textarea('content', array(
                'class' => 'ckeditor',
                'required'=>true
    ));
?>
</div>
</div>
</div>
<input class="btn btn-success" type="submit" name="send_mass_message" value="Send">
<input class="btn btn-primary" type="reset" name="cacel_mass_message" value="Cancel">
<?php echo $this->Form->end(); ?>
</div>

</div>
<!--End Notifications-->
</div>


</div>





</div>
<script type="text/javascript">
function convertToSlug(Text)
{
// var Text = $(this).val();
Text = Text.toLowerCase();
Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');

//alert(slug);
$("#slug").val(Text)

}
</script>		
