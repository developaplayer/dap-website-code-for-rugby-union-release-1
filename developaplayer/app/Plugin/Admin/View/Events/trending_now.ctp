<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header"><?php echo $title_for_layout?></h1>
					<?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

            <div class="row">
                
                <!--End Page Header -->
				<span style="float:left;color:#96B442;" class="msg_success" ><?php $success_msg=$this->Session->flash('success'); if(!empty($success_msg)) { ?><strong><?php echo $success_msg; ?></strong><?php } ?></span>
            </div>
            


			<div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong><?php echo $title_for_layout?> Management</strong>
                        </div> 
						<?php 
echo $this->Form->create("Event", array('url'=>array('controller' =>'events', 'action' => 'trending_now','plugin'=>'admin'),'id'=>'FormEditEvent','enctype'=>'multipart/form-data'));			

?>
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
							<th>Logo</th>
							<th>Name</th>
							<th>Start Date</th>
							<th>End Date</th>
							
							<th>Order</th>
							</tr>
							</thead>
							<tbody>
							<?php  
							//print"<pre>";
							//print_r($eventarr);
							if(is_array($eventarr) && count($eventarr)>0) 
							{ 
								foreach($eventarr as $allevent)
								{
							?> 
							
								<tr>
								<td>
								<img src="<?php echo $this->webroot ?>uploaded/eventfiles/<?php echo "106_".$allevent['picture']; ?>" height="50" width="50" border="0" >
								</td>
								<td><?php echo $allevent['event_name']; ?></td>
								<td> 
								<?php echo $allevent['start_date']; ?>
								</td>
								<td><?php echo $allevent['end_date']; ?></td>
								
								<td> 		
								<input type="text" name="trending_now_oredr[]" id="" value="<?php echo $allevent['trending_now_event_oredrs']?>" size="2"> 
								<input type="hidden" name="event_id[]" value="<?php echo $allevent['event_id']; ?>">
								</td>
								</tr>
							<?php
								}
							}
							else
							{
							?>		
								<tr>
								<td colspan="6" align="center" style="color:#FF0000;"  >* No record found.</td>
								</tr>
							<?php
							}
							?>
							
							</tbody> 
							</table>	

							<div class="pagination pagination-large">
								<input type="submit" name="trending_now_oredr_submit" value="Submit" class="btn btn-danger add_btn">
							</div>										
                        </div> 
					<?php echo $this->Form->end(); ?>	
                    </div>
                    <!--End Notifications-->
                </div> 
		   </div> 
</div>

<script language="javascript"> 
$(document).ready(function(){
	$('.msg_success').delay(5000).fadeOut('slow', function() {}); 
}); 
function change_status(id,geturl)
{
	$.ajax({		
			type:'POST',
			url:base_url+geturl+'/'+id,
			success:function(jData){
			 var moddt=jData.trim(); 
				$("#status_image_"+id).attr('src',base_url+'img/admin/'+moddt);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}
</script>       