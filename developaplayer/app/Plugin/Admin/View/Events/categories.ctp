<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Category Management</h1>
					<?php echo $this->Session->flash(); ?>
                </div>
                <!--End Page Header -->
            </div>

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                   
					<a href="<?php echo $this->webroot ?>admin/events/ad_category" title="Add New" class="btn btn-danger add_btn">Add New</a>
					
                </div>
                <!--End Page Header -->
				<span style="float:left;color:#96B442;" class="msg_success" ><?php $success_msg=$this->Session->flash('success'); if(!empty($success_msg)) { ?><strong><?php echo $success_msg; ?></strong><?php } ?></span>
            </div>
            
			<?php 
echo $this->Form->create("Category", array('url'=>array('controller' =>'events', 'action' => 'category_search','plugin'=>'admin'),'id'=>'Category','enctype'=>'multipart/form-data','class' => 'form-inline','role' => 'form')); 	
echo $this->Form->input('mode',array('type' => 'hidden', 'name' => 'mode','value'=>'1' ));		
?>
				<div class="row">
					<!-- Page Header -->
					<div class="col-lg-12">
						<div class="search_section">
							<div class="col-lg-5 col-md-5 col-sm-5 text_box">
							<div class="col-lg-3 col-md-4">
							<label>Category:</label>
							</div>
							<div class="col-lg-9 col-md-8">
							<input id="myName" type="text" name="data[Category][category_name]" placeholder="Type Category Name" value="<?php echo $searchpa;?>" ></div>
							</div>  
							<div class="col-lg-7 col-md-7 col-sm-7 search_btn"> 
							<input class="btn btn-primary search_btn1" type="submit" value="Search" name="Search">&nbsp;<input class="btn btn-primary search_btn1" type="button" onclick="window.location.href='<?php echo $this->webroot ?>admin/events/viewall'" value="View All" name="Search">
							</div>
							<div class="clearfix"></div>
						</div>
					</div> 
				</div>
<?php echo $this->Form->end(); ?>

			<div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Category Management</strong>
                        </div> 
						<?php 
						$paginator=$this->Paginator; 
						$getPageNumber=$paginator->options['url'];
						if(is_array($getPageNumber) && count($getPageNumber)>0) 
						{
							$pageno=$getPageNumber['page'];
							$pagestat=count($categories_arr);
						}
						else
						{
							$pageno=1;
							$pagestat=count($categories_arr);
						}
						?>
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
							<th>Category Name</th>
							<th>Status</th> 
							<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php  
							if(is_array($categories_arr) && count($categories_arr)>0) 
							{ 
								foreach($categories_arr as $allcat)
								{
							?> 
							
								<tr>
								<td><?php echo $allcat['Category']['category_name']; ?></td>
								<td> 
								<?php  if($allcat['Category']['active_status']=='Y') { $image='admin/icon_success.png'; } else { $image='admin/icon_error.gif'; } 
								echo $this->Html->image($image, array("alt" => "Logo","id" => "status_image_".$allcat['Category']['id'],"title" => "Click to change status","onclick"=>'javascript:change_status("'.$allcat['Category']['id'].'","admin/events/change_status")',"style" =>"cursor:pointer;"));?> 
								</td>
								<td> 		
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_edit.gif',array('alt' => 'Edit','title' => 'Edit')), 
								array('controller' => 'events', 'action' => 'edit_category',$allcat['Category']['id'],$pageno),array('escape'=>false)); ?> 
								<?php	 
								echo $this->Html->link($this->Html->image('admin/icon_delete.png',array('alt' => 'Delete','title' => 'Delete')), 
								array('controller' => 'events', 'action' => 'category_delete',$allcat['Category']['id'],$pageno,$pagestat),array('escape'=>false),'Are you sure?'); ?> 
								<?php	 
								echo $this->Html->link($this->Html->image('admin/file_view.png',array('alt' => 'View','title' => 'View')), 
								array('controller' => 'events', 'action' => 'category_details',$allcat['Category']['id'],$pageno),array('escape'=>false)); ?> 
								</td>
								</tr>
							<?php
								}
							}
							else
							{
							?>		
								<tr>
								<td colspan="3" align="center" style="color:#FF0000;"  >* No record found.</td>
								</tr>
							<?php
							}
							?>
							</tbody> 
							</table>	

							<div class="pagination pagination-large">
								<ul class="pagination">
										<?php
											echo $this->Paginator->prev(__('<< prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
											echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
											echo $this->Paginator->next(__('next >>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
										?>
									</ul>
							</div>										
                        </div> 
                    </div>
                    <!--End Notifications-->
                </div> 
		   </div> 
</div>

<script language="javascript"> 
$(document).ready(function(){
	$('.msg_success').delay(5000).fadeOut('slow', function() {}); 
}); 
function change_status(id,geturl)
{
	$.ajax({		
			type:'POST',
			url:base_url+geturl+'/'+id,
			success:function(jData){
			 var moddt=jData.trim(); 
				$("#status_image_"+id).attr('src',base_url+'img/admin/'+moddt);
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
			$(".contactus-block").html("Some server error occoured. Please report to system administrator.</div>");
			}
		});
}
</script>       