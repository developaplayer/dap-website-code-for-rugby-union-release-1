<div id="page-wrapper"> 
            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Event Details</h1>  
                </div>
                <!--End Page Header -->
            </div>  
            <div class="row"> 
			 
			
                <div class="col-lg-12"> 
				
					<a href="<?php echo $this->webroot ?>admin/events/listing/<?php echo $pageno; ?>" title="Add New" class="btn btn-danger add_btn" >Back</a> 

					<a href="<?php echo $this->webroot ?>admin/events/reviews/<?php echo $eventdetails['Event']['id']; ?>" title="Add New" class="btn btn-danger add_btn" style="margin-right:10px;">Reviews</a> 
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- Notifications-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong>Event Details - <?php echo $eventdetails['Event']['event_name']; ?></strong>
                        </div> 
					 
                        <div class="panel-body table-responsive"> 
							<table class="table table-striped table-bordered table-hover"> 
							<tbody> 
								<tr>
									<td><strong>Category Name:</strong></td>
									<td><?php echo $eventdetails['Category']['category_name']; ?></td> 
								</tr>  
								<tr>
									<td><strong>Sub-Category Name:</strong></td>
									<td><?php echo $eventdetails['Subcategory']['subcategory_name']; ?></td> 
								</tr>  		
								<tr>
									<td><strong>Country:</strong></td>
									<td><?php echo $eventdetails['Country']['countryName']; ?></td> 
								</tr> 	
								<tr>
									<td><strong>State:</strong></td>
									<td><?php echo $eventdetails['State']['stateName']; ?></td> 
								</tr>
								<tr>
									<td><strong>City:</strong></td>
									<td><?php echo $eventdetails['City']['cityName']; ?></td> 
								</tr>		
								<tr>
									<td><strong>Event Start Date:</strong></td>
									<td><?php echo $eventdetails['Event']['start_date']; ?></td> 
								</tr>	
								<tr>
									<td><strong>Event End Date:</strong></td>
									<td><?php echo $eventdetails['Event']['end_date']; ?></td> 
								</tr>	
								<tr>
									<td><strong>Event Description:</strong></td>
									<td><?php echo $eventdetails['Event']['description']; ?></td> 
								</tr>	
								<tr>
									<td><strong>Event Picture:</strong></td>
									<td><img src="<?php echo $this->webroot ?>uploaded/eventfiles/<?php echo "106_".$eventdetails['Event']['picture']; ?>" height="50" width="50" border="0" ></td> 
								</tr>
								<tr>
									<td><strong>Event Link:</strong></td>
									<td><?php echo $eventdetails['Event']['link_on_picture']; ?></td> 
								</tr>	

								<tr>
									<td><strong>Event Url:</strong></td>
									<td><?php echo Router::url('/users/login/', true).base64_encode($eventdetails['Event']['id']); ?></td> 
								</tr>
							</tbody> 
							</table>	 		
                        </div> 
                    </div>
                    <!--End Notifications-->
                </div> 
		   </div> 
		   <div class="row" style="float:left;"> 
                <div class="col-lg-12"> 
					<a href="<?php echo $this->webroot ?>admin/events/listing/page:<?php echo $pageno; ?>" title="Add New" class="btn btn-danger add_btn">Back</a> 
                </div> 
            </div>
</div> 