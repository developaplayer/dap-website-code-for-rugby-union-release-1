<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input ProductCategory Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
                  <div class="box-body">

				    <div class="form-group  has-feedback">
                      <label for="exampleInputText">Product Category Name</label><span></span>
                      <input type="text" name="name" class="required form-control" id="name" placeholder="ProductCategory Name">
                    </div>

					<div class="form-group has-feedback">
                    <label>Select Parent Category </label>(Please select a Product category for Product subcategory creation else leave it blank)
                    <select  name="parent_id" class="required form-control " >
					<option value="0">--Select a Parent Category--</option>
					<?php foreach($ProductCategory as $k=>$parent) {?>
                      <option value="<?php echo $parent['ProductCategory']['id'] ?>" ><?php echo $parent['ProductCategory']['name'] ?></option>
					  <?php } ?>
                    </select>
                  </div><!-- /.form-group -->
				  
				  
					
				<div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Description</h3>
                
                 
                </div><!-- /.box-header -->
                <div class="box-body pad">
                 
                    <textarea class="form-control" id="editor1" name="description"  rows="10" cols="80">
                    </textarea>
                  
                </div>
              </div>
					
					 <div class="form-group has-feedback">
                    <label>Status</label>
                    <select  name="status" class="required form-control select2" data-placeholder="Select a Status">
					<option value="1">Published</option>
					<option value="0">Unpublished</option>
					</select>
                    </div>

                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				                    <div class="box-footer">
                   
                  </div>
                </form>
              </div><!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
            
	  
<?php 
echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask.date.extensions');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask.extensions');
echo $this->Html->script('/admin/admin_css_js/gins/daterangepicker/daterangepicker');
echo $this->Html->script('/admin/admin_css_js/plugins/colorpicker/bootstrap-colorpicker.min');
echo $this->Html->script('/admin/admin_css_js/plugins/timepicker/bootstrap-timepicker.min');
echo $this->Html->script('/admin/admin_css_js/plugins/iCheck/icheck.min');	 
 ?>
    <script type="text/javascript">
	
	  
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
	  });
	  
	  function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/>'
					].join('');

					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);
		
	  $(function () {
       // CKEDITOR.replace('editor1'); 
		CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        //$(".textarea").wysihtml5();
      });
	  

    </script>
     