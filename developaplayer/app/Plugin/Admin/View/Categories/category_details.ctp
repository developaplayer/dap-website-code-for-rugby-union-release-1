 <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $ProductCategory[0]['ProductCategory']['name'] ?>
            <small>Category Data</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Category Data</a></li>
            <li class="active"><?php echo $ProductCategory[0]['ProductCategory']['name'] ?></li>
          </ol>
        </section>
		
		  <!-- Main content -->
        <section class="content">
		
		<div class="row">
            <div class="col-xs-12">

              <div class="box">
			   <div class="box-header">
                  <h3 class="box-title">ProductCategory Data</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
				<!---<form method="post" type="multipart" action="<?php echo $this->webroot; ?>admin/users/add_user">--->
                <div class="box-body">
				  
				 <form method="post" action="" enctype="multipart/form-data" id="form">
                  <div class="box-body">
				  
				   <div class="form-group">
                      <input type="hidden" name="id" value="<?php echo $ProductCategory[0]['ProductCategory']['id'] ?>" id="id">
                    </div>
					
					<div class="form-group has-feedback">
                    <label>Select Parent</label>
                    <select name="parent_id" class="required form-control select2" data-placeholder="Select a Parent">
					<option>--select  Parent</option>
					
					<?php foreach($allcategories as $k=>$allcategorie) {?>
                      <option  <?php if($allcategorie['ProductCategory']['id']==$ProductCategory[0]['ProductCategory']['parent_id']){ echo 'selected=selected'; } ?>  value="<?php echo $allcategorie['ProductCategory']['id'] ?>" ><?php echo $allcategorie['ProductCategory']['name'] ?></option>
					 <?php } ?>
					  
					  
					
                    </select>
                  </div><!-- /.form-group -->
					
				    <div class="form-group  has-feedback">
                      <label for="exampleInputText">ProductCategory Name</label>
                      <input type="text" name="first_name" value="<?php echo $ProductCategory[0]['ProductCategory']['name'] ?>" class="required form-control" id="first_name" placeholder="ProductCategory Name">
                    </div>
					
					
				<div class="box box-info">
					<div class="box-header">
					  <h3 class="box-title">Description</h3>
					
					 
					</div><!-- /.box-header -->
					<div class="box-body pad">
					 
						<textarea class="" id="description" name="description" placeholder="Description" rows="10" cols="80">
						 <?php echo $ProductCategory[0]['ProductCategory']['description']; ?>
						</textarea>
					  
					</div>
				</div><!-- /.box -->
			  
			 <!-- <div class="form-group has-feedback">
				<table colspan="8">
				  <label for="exampleInputText">ProductCategory Image</label>
				  <tr>
					  <td style="width:300px">
					  <input type="file" class="btn btn-primary" name="image[]" id="image" multiple />
					  <output id="list"></output>
					 
					  </td>
				 <td></td>
					  <td>
					  <input type="submit" style="display:none;" value="Save Image" class="show_submit btn btn-primary" />
					  </td>
				  </tr>
				  </table>
				</div>-->
				
				<!--<img src="<?php echo $this->webroot; ?>uploads/ProductCategory/thumb/<?php echo $ProductCategory[0]['ProductCategory']['image'] ?>" width="70px" height="70px" />-->
				
				 
					
					<!--<div class="form-group has-feedback">
                      <label for="exampleInputText">Sorting Order</label>
                      <input value="<?php echo $ProductCategory[0]['ProductCategory']['sort_order'] ?>" type="text" class="required form-control" name="sort_order" id="sort_order" placeholder="Sorting Order">
                    </div>-->

					
					 <div class="form-group has-feedback">
                    <label>Status</label>
                    <select  name="status" class="required form-control select2" data-placeholder="Select a Status">
					<?php if($ProductCategory[0]['ProductCategory']['status']==1){ ?>
						<option value="1">Published</option>
					<option value="0">Unpublished</option>
					<?php }else{ ?>
						<option value="0">Unpublished</option>
					<option value="1">Published</option>
					<?php } ?>
					
					</select>
                    </div>
					
					
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
					 <b><a href="<?php echo $this->webroot; ?>admin/users/view_all">Cancel</a></b>
                  </div>
                </form>
			   </div>
			  </div>
			  </div>
			  </div>
		
		
		
		</section>
		
		<?php 
echo $this->Html->script('/admin/admin_css_js/plugins/select2/select2.full.min');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask.date.extensions');
echo $this->Html->script('/admin/admin_css_js/plugins/input-mask/jquery.inputmask.extensions');
echo $this->Html->script('/admin/admin_css_js/gins/daterangepicker/daterangepicker');
echo $this->Html->script('/admin/admin_css_js/plugins/colorpicker/bootstrap-colorpicker.min');
echo $this->Html->script('/admin/admin_css_js/plugins/timepicker/bootstrap-timepicker.min');
echo $this->Html->script('/admin/admin_css_js/plugins/iCheck/icheck.min');	 
 ?>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js" type="text/javascript"></script>
   
   
    <!-- Page script -->
     
    <script type="text/javascript">
	
	  
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

	  });
	  
	  function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				

				$(function(){
					$('[id^="deflt_value_"]').prop('value','0');
				});


				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<input type="hidden" /><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/>'
					].join('');

					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);
	  
	  $(function () {
       // CKEDITOR.replace('editor1'); 
		//CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        //$(".textarea").wysihtml5();
      });

    </script>
     