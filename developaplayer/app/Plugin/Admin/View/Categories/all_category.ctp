      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All ProductCategory</h3>
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				
				<div id="dialog"></div>
				
				 <form method="post" action="<?php echo $this->webroot; ?>admin/categories/category_multiple_delete" enctype="multipart" id="delete_UnitType">
                  
			  <?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.delete')=='Y')))
				{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
						  <br />
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y'  && $this->Session->read('permissions.Product_Category.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
                        <th>Category Name</th>
                        <th>Description</th>                      						
						<th>Status</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='N' && $this->Session->read('permissions.Product_Category.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php
					
					 foreach($categories as $k=>$ProductCategory){ ?>
					 
					<?php  if($ProductCategory['ProductCategory']['parent_id']==$ProductCategory['ProductCategory']['id'])
					{
						$class='padding:10 px';
						
					}else
					{
						$class='';
					}
					$all_SubCategory_list=array();
					
					foreach($ProductCategory['SubCategory'] as $all_SubCategory)
					{
						$all_SubCategory_list[]=$all_SubCategory;
					}
					
					?>
					 
              <tr style="<?php echo $class;?>"> 
				<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y'  && $this->Session->read('permissions.Product_Category.delete')=='Y'))
				{ ?>
				  <td>
				  <input class="checkbox_del" name="id[]" value="<?php echo $ProductCategory['ProductCategory']['id'] ?>" type="checkbox" />
				  </td>
				<?php } ?>
					<td><a href="<?php echo $this->webroot; ?>admin/categories/category_details/<?php echo base64_encode($ProductCategory['ProductCategory']['id']); ?>">							
					<?php echo $ProductCategory['ProductCategory']['name'] ?></a></td>
						<td><?php echo $ProductCategory['ProductCategory']['description'] ?></td>												
						<td><?php if($ProductCategory['ProductCategory']['status']==1){
							echo "Published";
						}else{
							echo "Unpublished";
						} ?></td>
						
						<td style="width:120px !important">
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y'  && $this->Session->read('permissions.Product_Category.delete')=='Y'))
						{ ?>
						<a href="<?php echo $this->webroot; ?>admin/categories/category_details/<?php echo base64_encode($ProductCategory['ProductCategory']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>
						&nbsp;												
						<a id="delete_5" title="Delete" href="/developaplayer/admin/categories/category_delete/<?php echo base64_encode($ProductCategory['ProductCategory']['id']); ?>" onclick="return confirm('Are You Sure You Want To Delete <?php echo $ProductCategory['ProductCategory']['name'] ?>?')">
						<img width="30px" height="23px" src="/developaplayer/admin/admin_css_js/dist/img/delete.png" alt="Delete">
						</a>
						<?php } ?>
						
						<?php if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y')
						{?>
						<a href="<?php echo $this->webroot; ?>admin/categories/category_details/<?php echo base64_encode($ProductCategory['ProductCategory']['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>
						<?php } ?>
						
						<?php if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.delete')=='Y')
						{ ?>
						<a id="delete_5" title="Delete" href="/developaplayer/admin/categories/category_delete/<?php echo base64_encode($ProductCategory['ProductCategory']['id']); ?>" onclick="return confirm('Are You Sure You Want To Delete <?php echo $ProductCategory['ProductCategory']['name'] ?>?')">
						<img width="30px" height="23px" src="/developaplayer/admin/admin_css_js/dist/img/delete.png" alt="Delete">
						</a>
						<?php } ?>
						</td>			
         </tr>					  
					
             <?php 
			  foreach ( (array) $all_SubCategory_list as $all_SubCategory_lists)
			   {
				?>
					
					<tr style="<?php echo $class;?>">
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y'  && $this->Session->read('permissions.Product_Category.delete')=='Y'))
						{ ?>
						<td>
						<input class="checkbox_del" name="id[]" value="<?php echo $all_SubCategory_lists['id'] ?>" type="checkbox" />
						</td>
						<?php } ?>
						<td style="padding:8px 0px 0px 20px"><a href="<?php echo $this->webroot; ?>admin/categories/category_details/<?php echo base64_encode($all_SubCategory_lists['id']); ?>">							
						<?php echo $all_SubCategory_lists['name'] ?></a></td>
						<td><?php echo $all_SubCategory_lists['description'] ?></td>												
						<td><?php if( $all_SubCategory_lists['status']==1){
							echo "Published";
						}else{
							echo "Unpublished";
						} ?></td>
						
						<td style="width:120px !important"><b>
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.edit')=='Y'))
						{?>
						<a href="<?php echo $this->webroot; ?>admin/categories/category_details/<?php echo base64_encode( $all_SubCategory_lists['id']); ?>">
						<img alt="Edit" title="Edit" width="30px" height="23px" src="<?php echo $this->webroot; ?>admin/admin_css_js/dist/img/edit.png" /></a>
						<?php } ?>
						&nbsp;
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Product_Category.view')=='Y' && $this->Session->read('permissions.Product_Category.delete')=='Y'))
						{ ?>
						<a id="delete_5" title="Delete" href="/developaplayer/admin/categories/category_delete/<?php echo base64_encode( $all_SubCategory_lists['id']); ?>" onclick="return confirm('Are You Sure You Want To Delete <?php echo  $all_SubCategory_lists['name'] ?>?')">
						<img width="30px" height="23px" src="/developaplayer/admin/admin_css_js/dist/img/delete.png" alt="Delete">
						</a>
						<?php } ?>
					
						
			
			</td>
		</tr>
			
    <?php
	}
	
}
	?>			
	
					  
                    </tbody>                    
                  </table>
				  </form>
                </div><!-- /.box-body -->
            </div><!-- /.col -->
              </div><!-- /.box 
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->