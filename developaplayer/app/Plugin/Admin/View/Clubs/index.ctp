<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Clubs</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/clubs/delete_club_multiple" enctype="multipart" id="delete_club">
				<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.delete')=='Y'))
				{ ?>
					<input class="selectall" type="checkbox" />
					<input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
						  <br />
                  <table id="club_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
						<?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.delete')=='Y'))
						{ ?>
						<th>Select</th>
						<?php } ?>
						
                        <th>Logo</th>
                        <th>Club Name</th>
						<th>Status</th>
                        <th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.edit')=='N' && $this->Session->read('permissions.Club.status')=='N' && $this->Session->read('permissions.Club.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					</tbody>
                    <tfoot>
                      <tr>
						 <?php if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.delete')=='Y')){ ?>
								<th>Select</th>
						<?php } ?>
                        <th>Logo</th>
                        <th>Club Name</th>
						<th>Status</th>
						<th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Club.view')=='Y' && $this->Session->read('permissions.Club.edit')=='N' && $this->Session->read('permissions.Club.status')=='N' && $this->Session->read('permissions.Club.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	function table(){
			var dataTable = $('#club_table').DataTable({
				"dom": '<"row"<"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/clubs/club_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".club_table-error").html("");
						$("#club_table").append('<tbody class="club_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#club_table_processing").css("display","none");
					}
				}
			});
	}
	
	function change_status(id,status){
		$('#club_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/clubs/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#club_table_processing').hide();
					}
			}
		});
	}
</script>
