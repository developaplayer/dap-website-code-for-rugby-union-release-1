<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">All Certificates</h3>
				  
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" position-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
				  
                </div><!-- /.box-header -->
                <div class="box-body">
				 <div id="dialog"></div>
				<form method="post" action="<?php echo $this->webroot; ?>admin/certificates/delete_certificate_multiple" enctype="multipart" id="delete_certificate">
				<?php if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.delete')=='Y')
				{ ?>
				  <input class="selectall" type="checkbox" />
				  <input type="submit" onclick="return confirm('Sure You Want To Delete?');" value="Delete" class="btn btn-primary" style="width:100px; margin-bottom: 10px;" />
				<?php } ?>
						  <br />
                  <table id="certificate_table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
					  <?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.delete')=='Y'))
						{ ?>
								<th>Select</th>
						<?php } ?>
						
                        <th>Certificate</th>
						<th>Status</th>
                        <th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.edit')=='N' && $this->Session->read('permissions.Certificate.status')=='N' && $this->Session->read('permissions.Certificate.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </thead>
                    <tbody>
					</tbody>
                    <tfoot>
                      <tr>
						<?php if($this->Session->read('Auth.User.type')=='A' ||($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.delete')=='Y'))
						{ ?>
								<th>Select</th>
						<?php } ?>
                        <th>Certificate</th>
						<th>Status</th>
						<th>Created</th>
						<?php 
						if($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && $this->Session->read('permissions.Certificate.edit')=='N' && $this->Session->read('permissions.Certificate.status')=='N' && $this->Session->read('permissions.Certificate.delete')=='N')
						{ ?>
						<?php } else {?>
						<th>Options</th>
						<?php } ?>
                      </tr>
                    </tfoot>
                  </table>
				  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript" language="javascript" >
	$(document).ready(function(){
		
		table();
	});
	
	function table(){
			var dataTable = $('#certificate_table').DataTable({
				"dom": '<"row"<"col-md-5 pull-left add_input"><"pull-left error_message hide"><"col-md-2 pull-right"l><"col-md-3 pull-right"f>><"row"<"col-sm-12"tr>><"row"<"col-sm-5"i><"col-sm-5 pull-right"p>>',
				//"dom": '<"toolbar">frtip',
				"processing": true,
				"serverSide": true,
				 "stateSave": true,
				"ajax":{
					url :"<?php echo $this->webroot; ?>admin/certificates/certificate_data", // json datasource
					type: "post",  // method  , by default get
					error: function(){  // error handling
						$(".certificate_table-error").html("");
						$("#certificate_table").append('<tbody class="certificate_table-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
						$("#certificate_table_processing").css("display","none");
					}
				}
			});
			<?php 
				if($this->Session->read('Auth.User.type')=='A' || ($this->Session->read('Auth.User.type')=='M' && $this->Session->read('permissions.Certificate.view')=='Y' && ($this->Session->read('permissions.Certificate.add')=='Y' || $this->Session->read('permissions.Certificate.edit')=='Y')))
			{ ?>
			$(".add_input").html('<input style="width:63%" class="form-control" id="certificate_input" placeholder="Add a Certificate.." /><div onclick="add_positions()"  id="save" class="btn btn-default">Save</div>');
			<?php } ?>
	}
	
	function add_positions(){
		//alert();
		if($('#position_id').length > 0){
			var id = $('#position_id').val();
			update_positions(id);
		}else{
			$('.error_message').removeClass('hide');
			$('.error_message').html('<span style="color:green">Loading..</span>');
			var name = $('#certificate_input').val();
			$('#certificate_table_processing').show();
			//alert(name);
			$('#certificate_input').val('');
			$.ajax({
				type:"post",
				url:'<?php echo $this->webroot; ?>admin/certificates/add_certificate',
				data:{
					'name':name
				},
				success:function(response){
					if(response=="save_error"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:red">There was an unexpected error</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
					}else
						if(response=="empty_error"){
							$('.error_message').removeClass('hide');
							$('.error_message').html('<span style="color:red">Certificate name must not be empty</span>');
							setTimeout(function(){
								$('.error_message').addClass('hide');
							},4000);
						}else
							if(response=="exist"){
								$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Certificate Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
							}else{
									$('.dataTables_empty').remove();
									$('#certificate_table').append('<tr>'+response+'</tr>');
									$('.error_message').removeClass('hide');
									$('.error_message').html('<span style="color:green">Certificate Added</span>');
									setTimeout(function(){
											$('.error_message').addClass('hide');
										},3000);
								}
					$('#certificate_table_processing').hide();
				}
			});
		}
	}
	
	function change_status(id,status){
		$('#certificate_table_processing').show();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/certificates/status',
			data:{
				'id':id, 'status':status
			},
			success:function(response){
				if(response=="update_error"){
					
				}else
					if(response=="success"){
						if(status=='A'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/unlock.png');
							$('#status_of_'+id).html('Active');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"I")');
						}else
							if(status=='I'){
							$('#status_img_'+id).prop('src','<?php echo $this->webroot; ?>admin/img/icons/lock.png');
							$('#status_of_'+id).html('InActive');
							$('#status_'+id).prop('href','javascript:change_status('+id+',"A")');
						}
						$('#certificate_table_processing').hide();
					}
				/*setTimeout(function(){
					$('#message').hide();
				},2000);*/
			}
		});
	}
	
	function edit_data(id,position){
		//alert(position);
		//$('.name_'+id).val();
		if($('#cancel').length == 0){
			$('#certificate_input').val($('#name_'+id).text());
			$('#save').after('<div class="btn btn-default" onclick="cancel()" id="cancel">Cancel</div>');
			$('#save').after('<input type="hidden" id="position_id" value="'+id+'" />');
		}
	}
	
	function cancel(){
		$('#certificate_input').val('');
		$('#cancel').remove();
		$('#position_id').remove();
		//$('#save').prop('onclick','add_positions()');
	}
	
	function update_positions(id){
		//alert(id);
		$('.error_message').removeClass('hide');
		$('.error_message').html('<span style="color:green">Loading..</span>');
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>admin/certificates/update_certificate',
			data:{
				'id':id, 'position':$('#certificate_input').val()
			},
			success:function(response){
				if(response=="update_error"){
					$('.error_message').removeClass('hide');
					$('.error_message').html('<span style="color:red">Unexpected Error Occurred..</span>');
					setTimeout(function(){
							$('.error_message').addClass('hide');
						},3000);
				}else
					if(response=="success"){
						$('.error_message').removeClass('hide');
						$('.error_message').html('<span style="color:green">Certificate Updated..</span>');
						setTimeout(function(){
								$('.error_message').addClass('hide');
							},3000);
						$('#name_'+id).html($('#certificate_input').val());
						cancel();
					}
					else
						if(response=="exist"){
							$('.error_message').removeClass('hide');
								$('.error_message').html('<span style="color:red">Certificate Already Exist</span>');
								setTimeout(function(){
									$('.error_message').addClass('hide');
								},4000);
						}
			}
		});
	}
</script>
