﻿ 
 <div class="home-left">
        <div class="home-head">
            HAND-CURATED<br>
            COMMUNITY-LED<br>
            ORGANIC
        </div>
        <div class="home-cont">
          <p>We are a start-up that loves conferences.</p>
          <p>The buzz, the learning, the inspiration. New trends,new places and  people - both known &amp; new.</p>
          <p>We want our users to discover the top events in their
            domains. Discover the events to invest in - as attendees,
            as speakers or as sponsors. Through community driven
            curation - your expert feedback, views &amp; opinions.</p>
          <p>So, join us in this journey... </p>
        </div>
      </div>
	  
	  	  <div class="home-right">
   	    <h2>been there?<br>
   	      join our   	    community of      specialists</h2>
   	    <p>If you’re a conference regular and a subject specialist - we would be delighted to have you on our panel of experts. Simply sign up with your email address and we will send you an invite to register pre-launch.</p>
   	    <?php echo $this->Form->create('User', array('action' => 'signup'));?>
		<p>
   	      <input name="name" type="text" id="name" class="required"  placeholder="your name">
   	    <label id="name_error_msg" class="error" for="name"></label>
		</p>
   	    <p>
   	      <input name="email_address" type="text" id="email_address" class="required email"  placeholder="your email">
   	     <label id="email_error_msg" class="error" for="email_address"></label>
		</p>
   	    <p>
   	      <input type="submit" name="button" id="button" value="get me on the list">
	   <span id="signup_success_msg" ></span>
	   </p>
	    
		<?php echo $this->Form->end(); ?>
        <div class="clear"></div>
   	    <div class="spam-msg"> <a href="javascript:void(0);"><img src="<?php echo $this->webroot; ?>images/spam-cross.png" alt=""/></a> We don’t spam. We won’t share
or sell your personal data! Never!</div>
   	  </div>
<script type="text/javascript">

$(document).ready(function () {

		$("#UserSignupForm").validate({
			submitHandler: function(form) {
				var myData = $('#UserSignupForm').serialize();
				var img='<img src="<?php echo $this->webroot; ?>images/loader.gif" style="vertical-align: middle;" class="loader" alt="Loading"  />';	
				$("#signup_success_msg").html(img);
					
				$.ajax({
				type:'POST',
				url:'<?php echo $this->webroot; ?>users/signup',
				data: myData,
				dataType:'json',
				success:function(jData){
				$("#signup_success_msg").html('');
					if(jData.signup_success){
					//$("#signup_success_msg").html(jData.signup_success);
					//$("#signup_success_msg").show()
					//setTimeout(function(){$( "#signup_success_msg" ).empty();}, 3000);
							// scroll to top
							$('html, body').animate({
								scrollTop: "0px"
								}, 1000);
							// reset the form inputs
							$('#UserSignupForm')[0].reset();
							// for open up popup message
							var id = '<?php echo $this->webroot; ?>users/signup_success';
							$("#popupContainer").show();
							$(".blackcover").show();
							$("#popupContainer").load(id); 
					}
					if(jData.name_error){ 
					$("#name_error_msg").html(jData.name_error);
					$("#name_error_msg").show()
					}
					if(jData.email_address_error){ 
					$("#email_error_msg").html(jData.email_address_error); 
					$("#email_error_msg").show();
					}
				  }
				});
			}
			
		});
});
</script>	  
	  
      
   	  