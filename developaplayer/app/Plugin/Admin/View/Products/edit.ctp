<?php //pr($product_data); exit;?>
 
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Product Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				
				<!--- body --->
           <div class="box-body">
			<label for="name">Product Category</label>
	    <select id="cat_id" name="cat_id" class="required form-control">	
			<option value="">--Select Product Category----</option>
			 <?php
			 foreach((array) $all_category as $all_categorys) 
			 {					 
//===================for sub category=========================================			
				    $all_SubCategory_list=array();
                    if(!empty($all_categorys))
					{						
						foreach((array)$all_categorys['SubCategory'] as $all_SubCategory)
						{
							$all_SubCategory_list[]=$all_SubCategory;
						}
					}
					
//============================================================================	
				?>				
				
				  <optgroup label="<?php echo $all_categorys['ProductCategory']['name'];?>">
					<?php 
					  foreach ( (array) $all_SubCategory_list as $all_SubCategory_lists)
					   {
					  ?>
					  <option  <?php if(isset($product_data['Product']['cat_id']) && $product_data['Product']['cat_id']==$all_SubCategory_lists['id']) echo 'selected';?>  value="<?php echo $all_SubCategory_lists['id'];?>"><?php echo $all_SubCategory_lists['name'];?></option>	
					
					  <?php 
						  }
						  ?>
				
					
				<?php	 
					} 
				  ?>
                <optgroup/>	
				 </select> 
					<?php

						echo $this->Form->input('Product Title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Title',
													'type'=>'text',
													'id'=>'name',
													'name'=>'name',
													'value'=>$product_data['Product']['name'],
													'placeholder'=>'Product Title'
													)
												);?>
												
							<label for="name">Product Description</label>					
						<?php
						echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'form-control',
													'label' => 'Product Description',
													'type'=>'textarea',
													'id'=>'description',
													'name'=>'description',
													'value'=>$product_data['Product']['description'],
													'placeholder'=>'Product Description'
													)
												);	
						?>
							<!--<div>
							<img src="<?php echo $this->webroot; ?>uploads/product/thumbnail/<?php echo $product_data['Product']['logo']; ?>" />
							</div>-->
												
					<?php					
					echo $this->Form->input('image',array(
													'div'=>'form-group has-feedback',
													'class'=>'btn btn-primary',
													'label' => 'Product Image',
													'type'=>'file',
													'style'=>array('width'=>'300px'),
													'name'=>'ProductImage[]',
													'multiple'=>'multiple',
													)
												);												
																			
							echo '<div>Press  Ctrl Select Multiple Image. </div>';	
								?>
								
			<output id='list'>
				<?php
				//pr($product_data);
				foreach((array)$product_data['allProductImage'] as $ProductImages)
				{
					
					
               ?>					
				<span id="image_span_id<?php echo $ProductImages['id'];?>">					
					<div style="display:inline-block;">
					  <img title="<?php echo $ProductImages['image']?>" src="<?php echo $this->webroot?>uploads/product/original/<?php echo $ProductImages['image']?>" style="width:100px;height:75px; border: 1px solid #000; margin: 5px" class="hide_shown_image"/>
					 <p><input type="radio" value="<?php echo $ProductImages['id']?>" id="default_image" name="default_image" <?php echo ($ProductImages['deflt_value']== 1) ?  "checked" : "" ;  ?>  style="margin-left:40px"><span style="padding-left:4px;cursor:pointer;"><a onclick="remove_image('<?php echo $ProductImages['id'];?>')"; >X</a></span></p>
												
					</div>
			</span>
			<?php 
			  } 
			?>							
		</output>
		
           <label for="name">Product Size/Quantity</label>
			<div id="size_quitity_div" class="size_quitity_div col-lg-12" style="padding-left:0">
						<?php
						$i=0;
						 $total_sale_qty_count=0;
						foreach( (array) $product_size_qties as $product_size_qty)
						{
						?>						
                          <div id="size_id<?php echo $i?>">							
					          <div style="float:left; margin:0 5px 5px 0">							 
								<select name="product_size[]" id="select_size<?php echo $i?>" class="required form-control"  onchange="select_size_value('<?php echo $i?>')">
								<option value="">---Select Product Size---</option>
								  <?php foreach($all_Size as $key=>$all_Sizes){?>
									<option <?php if(isset($product_size_qty['ProductSizeQty']['size_id']) && $product_size_qty['ProductSizeQty']['size_id']==$key) echo 'selected';?> value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option>									
									<?php } ?>									
								 </select>								 
					         	</div>
								<?php 
								   //$total_sale_qty =$product_size_qty['ProductSizeQty']['sale_qty'];
								$total_sale_qty_count= ($total_sale_qty_count+$product_size_qty['ProductSizeQty']['sale_qty']);								   
								$reaming_qty =$product_size_qty['ProductSizeQty']['qty'] -$product_size_qty['ProductSizeQty']['sale_qty'];								
								if($reaming_qty>0 || $reaming_qty==0)
								{
									$ReamingQty= $reaming_qty;
								}else{
									
									$ReamingQty= 0;
								}
								
								?>
								
                     		<div style="float:left; margin:0 5px 5px 0">Total Quantity: <input type="number" value="<?php echo $product_size_qty['ProductSizeQty']['qty'];?>"  name="qty[]" id="qty<?php echo $i;?>" placeholder="Quantity" onblur=add_qty('<?php echo $i?>') class="required"/> </div>
							<!--<div style="float:left; margin:0 5px 5px 0; cursor:pointer;" > <strong >Sale Quantity</strong>: <span style="color:red"><?php echo $total_sale_qty_count; ?></span> </div>-->
							<div style="clear:both"></div>
								<div style="float:left; margin:5px 15px 15px 198px"><strong>Reaming Quantity: <?php echo $reaming_qty; ?></strong></div>
							<!--<div style="float:left; margin:0 5px 5px 0; cursor:pointer;" > <strong >Sale Quantity</strong>: <span style="color:red"><?php echo $total_sale_qty_count; ?></span> </div>-->
							<div style="clear:both"></div>
					      </div>
						
						
					   
						
						<?php
						$i++;
						} 						
						?>
						</div>
						
						<div><input type="button" class="btn btn-primary" id="add_size" value="Add Size"></div>
                                <?php //echo $total_sale_qty_count;?>
                            <?php    
                                echo $this->Form->input('price',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product price',
													'type'=>'number',
													'id'=>'price',													
													'name'=>'price',
													'value'=>$product_data['Product']['price'],
													'placeholder'=>'Product price'
													)
												);	
												
						echo $this->Form->input('current_stock',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Current Stock',
													'type'=>'text', 
													'id'=>'current_qty',
													'readonly'=>'readonly',
													'name'=>'current_qty',
													'value'=>$product_data['Product']['total_qty'],
													'placeholder'=>'Current Product Stock'
													)
												);
							echo $this->Form->input('total_stock',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Total Stock',
													'type'=>'text',
													'id'=>'total_qty',
													'readonly'=>'readonly',
													'name'=>'total_qty',
													'value'=>$product_data['Product']['total_qty'],
													'placeholder'=>'Total Product Stock'
													)
												);						
												
						/*echo $this->Form->input('slug_url',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Slug Url',
													'type'=>'text',
													'id'=>'slug_url',
													'name'=>'slug_url',
													'value'=>$product_data['Product']['slug_url'],
													'placeholder'=>'Product Slug Url'
													)
												);							
					
				
												
					echo $this->Form->input('meta_title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Meta Title',
													'type'=>'text',
													'id'=>'meta_title',
													'name'=>'meta_title',
													'value'=>$product_data['Product']['meta_title'],
													'placeholder'=>'Product Meta Title'
													)
												);
												
												echo $this->Form->textarea('meta_description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Meta Title',
													'type'=>'textarea',
													'id'=>'meta_title',
													'name'=>'meta_title',
													'value'=>$product_data['Product']['meta_description'],
													'placeholder'=>'Product Meta Description'
													)
												);
												
												*/							
										?>
										
								<label for="name">Product Return Policy</label>										
									<?php	//$product_data['Product']['return_policy'];		
									echo $this->Form->textarea('return_policy',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Return Policy',
													'type'=>'textarea',
													'id'=>'return_policy',
													'name'=>'return_policy',
													'value'=>$product_data['Product']['return_policy'],
													'placeholder'=>'Product Return Policy'
													)
												);			

								?>			
					   
					
							<div class="form-group has-feedback">
								<label>Status</label>
								<select  name="status" class="required form-control" data-placeholder="Select a Status">
									<?php if($product_data['Product']['status']=="A"){ ?>
											<option value="A">Published</option>
											<option value="I">Unpublished</option>
									<?php }else{ ?>
											<option value="A">Unpublished</option>
											<option value="I">Published</option>
									<?php } ?>
								</select>
							</div>
					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
    <script type="text/javascript">
		
		$(document).ready(function(){
		var total_sale_sum='<?php echo $sale_sum;?>';
		var sum = 0;		
		  $("#size_quitity_div input[type=number]").each(function()
		  {			  
              //var input = $(this).val(); 
               sum += Number($(this).val());  
			 // alert(sum); 
          });
		  var now_surrent_stock= (sum-total_sale_sum);
		   $('#current_qty').val(now_surrent_stock);
		
		});
		function handleFileSelect(evt) {
			var files = evt.target.files;
			//$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			var g=0;
			for (var i = 0, f; f = files[i]; i++) {
				
                     //alert(i);
				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					count =g++;
					//alert(count);
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div id="new_product_image'+count+'" style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/><p><input style="margin-left:40px" type="radio" name="default_image"  id="default_image" value="'+escape(theFile.name)+'"/><span style="padding-left:4px"><a onclick="new_remove_image('+count+')"; style="cursor:pointer;" >X</a></span></p></div>'].join('');
					//document.getElementById('list').insertBefore(span, null);
					$('#list').append(span);	
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
				g++;
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);

		//================================Edit Size Qty====================================
		
		//===================edit qty size================================	
var html="";
var num = '<?php echo count($product_size_qties); ?>';
$(document).ready(function(){
	
	$("#add_size" ).click(function() {	
	var next_num = num++;
	//alert(next_num);
    var html=' <div id="size_id'+next_num+'"><div style="float:left; margin:0 5px 5px 0"><select onchange=select_size_value('+next_num+') name="product_size[]" id="select_size'+next_num+'" class="form-control"><option value="">---Select Product Size---</option> <?php foreach($all_Size as $key=>$all_Sizes){?><option value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option><?php } ?> </select></div><div style="float:left; margin:0 5px 5px 27px">New Quantity: <input type="number" onblur=add_qty('+next_num+')  name="qty[]" id="qty'+next_num+'" placeholder="Quantity" class="required"/></div><div style="float:left; margin:0 5px 5px 0" onclick=delete_size_qnty('+next_num+')><a href="#">X</a></div><div style="clear:both"></div></div>'; 									 	
	$('#size_quitity_div').append(html);							
								 
	 // $("#select_size'"+next_num+"' option[value='2']").attr('disabled','disabled');
	});
});

	function select_size_value(number)
	{
	   var get_val=$('#select_size'+number).val();	
        if( $('#size_quitity_div').find('select option[value='+get_val+']:selected').length>1)
		{
            alert('option is already selected');			
            $('#select_size'+number).val('');   
        }
 
	}
		
	function add_qty(number)
	{		var sum = 0;
             var total_sale_sum='<?php echo $sale_sum;?>';	
		  $("#size_quitity_div input[type=number]").each(function()
		  {			  
              //var input = $(this).val(); 
               sum += Number($(this).val());  
			 // alert(sum); 
          });
		  
		   $('#current_qty').val(parseInt(sum)-parseInt(total_sale_sum));
		   
		      var total_stock='<?php echo $product_data['Product']['total_qty']; ?>';
			  if(total_stock>sum)
			  {
				 var increasestock =(parseInt(total_stock)-parseInt(sum)); 
			  }else
			  {
                var increasestock =(parseInt(sum)-parseInt(total_stock));				  
			  }
		     
			 var total_sale_qty_count= '<?php echo $total_sale_qty_count; ?>';
			 //parseInt(total_sale_qty_count);
			 var totalincreasestock =(parseInt(sum));
			 $('#total_qty').val(totalincreasestock);
			 
			 //==============================update product qty=========================//
			 
			$.ajax({
		    type:"post",
			url:'<?php echo $this->webroot; ?>admin/products/remove_image',
			data:{
				'id': imageId
			},
		    success:function(response){			
			 $('#image_span_id'+imageId).animate({
			opacity: 0.25,
			//left: "+=50",
			height: "toggle"
		  }, 1000, function() {
			// Animation complete.
			$('#image_span_id'+imageId).remove();
		});
	
		},
		error:function(response)
		{
		}
	});
	
		
	}
	
	function delete_size_qnty(delete_size_qnty)
	{
		$('#size_id'+delete_size_qnty).remove();
		
	}
	
	
	//============Previous Image=======================//
	function remove_image(imageId)
	{
		$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>admin/products/remove_image',
		data:{
			'id': imageId
		},
		success:function(response){
			
			 $('#image_span_id'+imageId).animate({
			opacity: 0.25,
			//left: "+=50",
			height: "toggle"
		  }, 1000, function() {
			// Animation complete.
			$('#image_span_id'+imageId).remove();
		});
				
			
			
		//	$('.update_certificate_field').html(response);
			//$(".select2").select2();
		},
		error:function(response){
			//$('#coach_certificate').html('');
			//$('.update_certificate_field').after('<span class="error ajax_error">There was an error while loading Ceritificates. Refresh the page.</span>');
		}
	});
		
		
		
		
	}
	
	//================new add image==================================//
	
	function new_remove_image(divId)
	{
		
		 $('#new_product_image'+divId).animate({
			opacity: 0.25,
			//left: "+=50",
			//height: "toggle"
		  }, 5000, function() {
			// Animation complete.
			$('#new_product_image'+divId).remove();
		});
		//alert(divId);
		
		
	}

	  </script>