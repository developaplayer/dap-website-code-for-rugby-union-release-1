
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <?php echo $this->element('breadcrumb'); ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Input Product Details</h3>
				  
				  <?php if($this->Session->check('Message.flash')){?>
					<div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  
					<?php echo $this->Session->flash(); ?>   
                  </div>
				<?php }?>
				  
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="post" action="" enctype="multipart/form-data" id="form">
				<?php //pr($all_category); exit; ?>
				<!--- body --->
            <div class="box-body">
			 <label for="name">Product Category</label>
			<select id="cat_id" name="cat_id" class="required form-control">	
			<option value="">--Select Product Category----</option>
			 <?php
			 foreach((array) $all_category as $all_categorys) 
			 {					 
//===================for sub category=========================================			
				    $all_SubCategory_list=array();
                    if(!empty($all_categorys))
					{						
						foreach((array)$all_categorys['SubCategory'] as $all_SubCategory)
						{
							$all_SubCategory_list[]=$all_SubCategory;
						}
					}
					
//============================================================================	
				?>				
				
				  <optgroup label="<?php echo $all_categorys['ProductCategory']['name'];?>">
					<?php 
					  foreach ( (array) $all_SubCategory_list as $all_SubCategory_lists)
					   {
					  ?>
					  <option value="<?php echo $all_SubCategory_lists['id'];?>"><?php echo $all_SubCategory_lists['name'];?></option>	
					
					  <?php 
						  }
						  ?>
				
					
				<?php	 
					} 
				  ?>
                <optgroup/>				  
				 </select>
				  
					<?php					
						/*echo $this->Form->input('cat_id',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Select Category',
													'type'=>'select',
													'name'=>'cat_id',
													'id'=>'cat_id',
													'required'=>true,
													'data-placeholder'=>'Select a Category',
													'default' => '--select Product category--',
													'options'=>$all_category
																)
															);*/
					
						echo $this->Form->input('name',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Title',
													'type'=>'text',
													'id'=>'name',
													'name'=>'name',
													'placeholder'=>'Product Title'
													)
												);
												?>
					
				  <label for="name">Product Description</label>
						<?php echo $this->Form->textarea('description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Description',
													'type'=>'textarea',
													'id'=>'description',
													'name'=>'description',
													'placeholder'=>'Product Description'
													)
												);	
												
								echo $this->Form->input('image',array(
													'div'=>'form-group has-feedback',
													'class'=>'required btn btn-primary',
													'label' => 'Product Image',
													'type'=>'file',
													'style'=>array('width'=>'300px'),
													'name'=>'ProductImage[]',
													'multiple'=>'multiple',
													)
												);												
							echo "<output id='list'></output>";															
							echo '<div>Press  Ctrl Select Multiple Image. </div>';	
?>							
                          <label for="name">Add Size/Quantity</label>			
						  <div id="size_quitity_div" class="size_quitity_div col-lg-12" style="padding-left:0">
                          <div id="size_id0">							
					          <div style="float:left; margin:0 5px 5px 0">							 
								<select name="product_size[]" id="select_size0" class="required form-control"  onchange="select_size_value(0)">
								<option value="">---Select Product Size---</option>
								  <?php foreach($all_Size as $key=>$all_Sizes){?>
									<option value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option>									
									<?php } ?>									
								 </select>								 
					         	</div>							 
                     		<div style="float:left; margin:0 5px 5px 0">Quantity: <input type="number"  name="qty[]" id="qty0" placeholder="Quantity" onblur=add_qty(0) class="required"/> </div>
							<div style="float:left; margin:0 5px 5px 0;cursor:pointer;" ><a href="#"></a></div>
							<div style="clear:both"></div>
					    </div>
						</div>
							  
						<div><input type="button" class="btn btn-primary" id="add_size" value="Add Size"></div>
						  
						<?php						
						echo $this->Form->input('price',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product price',
													'type'=>'number',
													'id'=>'price',
													'name'=>'price',
													'placeholder'=>'Product price'
													)
												);	
												
						echo $this->Form->input('total_qty',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Quenty',
													'type'=>'number',
													'id'=>'total_qty',
													'name'=>'total_qty',
													'placeholder'=>'Product Quenty'
													)
												);							
												
						/*echo $this->Form->input('slug_url',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Slug Url',
													'type'=>'text',
													'id'=>'slug_url',
													'name'=>'slug_url',
													'placeholder'=>'Product Slug Url'
													)
												);							
					
				
												
					echo $this->Form->input('meta_title',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Meta Title',
													'type'=>'text',
													'id'=>'meta_title',
													'name'=>'meta_title',
													'placeholder'=>'Product Meta Title'
													)
												);	
												
										echo $this->Form->input('meta_keyword',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Meta Title',
													'type'=>'text',
													'id'=>'meta_keyword',
													'name'=>'meta_keyword',
													'placeholder'=>'Product Meta Keyword'
													)
												);								
												
												
									echo $this->Form->textarea('meta_description',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Meta Title',
													'type'=>'textarea',
													'id'=>'meta_description',
													'name'=>'meta_description',
													'placeholder'=>'Product Meta Description'
													)
												);*/
											?>	
                                            <label for="name">Product Return Policy</label>
                                           <?php echo $this->Form->textarea('return_policy',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Product Return Policy',
													'type'=>'textarea',
													'id'=>'return_policy',
													'name'=>'return_policy',
													'value'=>'',
													'placeholder'=>'Product Return Policy'
													)
												);	
												
				
												
					echo $this->Form->input('status',array(
													'div'=>'form-group has-feedback',
													'class'=>'required form-control',
													'label' => 'Status',
													'type'=>'select',
													'name'=>'status',
													'data-placeholder'=>'Select a Status',
													'options'=>array(
																	'A'=>'Active',
																	'I'=>'InActive'
																	)
																)
															);
					
					?>

					
					</div>
				  
				  <!-- /.box-body -->

                  <div class="box-footer">
                    <input type="submit" value="submit" class="btn btn-primary">
                  </div>
				  
				  <div class="box-footer">
                  </div>
				  
                </form>
              </div>
			  
			  <!-- /.box -->

              <!-- Form Element sizes -->
              

            </div><!--/.col (left) -->
          
          </div>   <!-- /.row -->
		  
		 </section>
	 </div>
            
    <script type="text/javascript">
	
	var html="";
	var num = 1;
$(document).ready(function(){
	
	$("#add_size" ).click(function() {	
	var next_num = num++;
	//alert(next_num);
    var html=' <div id="size_id'+next_num+'"><div style="float:left; margin:0 5px 5px 0"><select onchange=select_size_value('+next_num+') name="product_size[]" id="select_size'+next_num+'" class="form-control"><option value="">---Select Product Size---</option> <?php foreach($all_Size as $key=>$all_Sizes){?><option value="<?php echo $key; ?>"><?php echo $all_Sizes; ?></option><?php } ?> </select></div><div style="float:left; margin:0 5px 5px 0">Quantity: <input type="number" onblur=add_qty('+next_num+')  name="qty[]" id="qty'+next_num+'" placeholder="Quantity" class="required"/></div><div style="float:left; margin:0 5px 5px 0;cursor:pointer" onclick=delete_size_qnty('+next_num+')><a href="#">X</a></div><div style="clear:both"></div></div>'; 									 	
	$('#size_quitity_div').append(html);							
								 
	 // $("#select_size'"+next_num+"' option[value='2']").attr('disabled','disabled');
	});
});

	function select_size_value(number)
	{
			//alert(number);
			
			var get_val=$('#select_size'+number).val();
			
			
        if( $('#size_quitity_div').find('select option[value='+get_val+']:selected').length>1)
		{
            alert('option is already selected');
			
            $('#select_size'+number).val('');   
        }
 
	}
		
	function add_qty(number)
	{		var sum = 0;		
		  $("#size_quitity_div input[type=number]").each(function()
		  {
              //var input = $(this).val(); 
               sum += Number($(this).val());   			  
          });
		   $('#total_qty').val(sum);
		//alert(sum);
	}
	
		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/><p><input style="margin-left:40px" type="radio" name="default_image"  id="default_image" value="'+escape(theFile.name)+'"/></p></div>'].join('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('image').addEventListener('change', handleFileSelect, false);

$('document').ready(function(){
	CKEDITOR.replace('description');
	$('#description').before('<label for="description">Content</label>');
	$('#meta_description').before('<label for="meta_description">Meta Description</label>');
});

function delete_size_qnty(delete_size_qnty)
{
	$('#size_id'+delete_size_qnty).remove();
	
}





	  </script>