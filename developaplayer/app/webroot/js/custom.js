$(document).ready(function(){
	topbar_height = $('nav').height();
	$('.main_container').css('margin-top', topbar_height+'px');
	
	footer_height = $('footer').outerHeight()-8;
	$('body').css('margin-bottom', footer_height+'px');
	
	$('ul.nav li.dropdown').hover(function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});		
	
	<!-- Start Flexslider -->
	var $window = $(window), flexslider;
	
	<!-- Start Flexslider for Brand Logos --> 
	function getGridSize1() {
		return (window.innerWidth < 480) ? 1 :
		       (window.innerWidth < 568) ? 2 :
			   (window.innerWidth < 768) ? 4 :
			   (window.innerWidth < 1024) ? 5 : 6;
	} 
  	$('.flexslider.brand_logos').flexslider({
		  animation: "slide",
		  animationLoop: false,
		  itemWidth: 90,
		  itemMargin: 0,
		  controlNav: false,
		  directionNav: false,
		  minItems: getGridSize1(),
		  maxItems: getGridSize1()
    });
	<!-- End Flexslider for Brand Logos -->
	
	<!-- Start Flexslider for Training Gears --> 
	function getGridSize2() {
		return (window.innerWidth < 568) ? 1 :
			   (window.innerWidth < 768) ? 2 :
			   (window.innerWidth < 1024) ? 3 : 4;
	} 
  	$('.flexslider.training_gears').flexslider({
		  animation: "slide",
		  animationLoop: false,
		  itemWidth: 250,
		  itemMargin: 0,
		  controlNav: false,
		  directionNav: true,
		  minItems: getGridSize2(),
		  maxItems: getGridSize2()
    });
	<!-- End Flexslider for Training Gears -->
		
	<!-- Start form-group focus blur -->	
	$('.form-group input').on('focus', function(){
   		$(this).parent().addClass('focused');		
	}).on('blur', function(){  		
		if(!$(this).val()){
     		$(this).parent().removeClass('focused');
			$(this).parent().addClass('error');
    	}
		else{
			$(this).parent().removeClass('error');
		}
	});
	
	<!-- End form-group focus blur -->
	
	<!-- Start date-time picker -->
	$('#datetimepicker1').datetimepicker({
		//format: 'DD/MM/YYYY' // Disable Time Picker
		format: 'YYYY-MM-DD' // Disable Time Picker
	});
	$('#dob').datetimepicker({		
		format: 'YYYY-MM-DD' // Disable Time Picker
	});
	<!-- End date-time picker -->
	
	<!-- start form validation -->	
	var delay = 0;
	var offset = topbar_height + 30;

	document.addEventListener('invalid', function(e){
   		$(e.target).addClass("invalid");
   		$('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
	}, true);
	
	document.addEventListener('change', function(e){
   		$(e.target).removeClass("invalid")
	}, true);
	<!-- end form validation -->
	
	<!-- Start select dropdown -->
	$( ".filter_holder select" ).selectmenu();
	$( ".reg_bg select#country" ).selectmenu();
	$( ".reg_bg select#primary" ).selectmenu();
	$( ".reg_bg select#secondary" ).selectmenu();
	$( ".sortby_holder select" ).selectmenu();
	$(".graph_filter_holder select").selectmenu();
	<!-- End select dropdown -->
	
	<!-- Start scroller -->
	$(".achievement_innr,.achievement_rght_mid_outer,.transaction_tbl_outer, #add_note_comment").mCustomScrollbar({
		scrollButtons:{
			enable:false
		},
		advanced: {
			updateOnContentResize: Boolean
		}
	});
	<!-- End scroller -->	
	
	<!-- Start masterslider -->
	var playerslider = new MasterSlider();
	playerslider.setup('masterslider' , {
		loop:true,
		width:355,
		height:400,
		speed:20,
		view:'flow',
		preload:0,
		space:0,
		wheel:true
	});
	playerslider.control('arrows');
	playerslider.control('slideinfo',{insertTo:'#staff-info'});
	<!-- End masterslider -->
	
	<!-- Start vertical expandable menu -->
	$('#cssmenu > ul > li > a').click(function() {
		$('#cssmenu li').removeClass('active');
		$(this).closest('li').addClass('active');	
		var checkElement = $(this).next();
		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
		  $(this).closest('li').removeClass('active');
		  checkElement.slideUp('normal');
		}
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
		  $('#cssmenu ul ul:visible').slideUp('normal');
		  checkElement.slideDown('normal');
		}
		if($(this).closest('li').find('ul').children().length == 0) {
		  return true;
		} else {
		  return false;	
		}		
	});
	<!-- End vertical expandable menu -->
	
	<!-- Start Carousel -->
	$('#carousel').carousel({
	  	interval: 6000,
	  	cycle: true
	}); 
	<!-- End Carousel -->
	
	<!-- Start equal-height -->
	jQuery('#equal_list_height div.prod_holder').equalHeights();
	<!-- End equal-height -->
});