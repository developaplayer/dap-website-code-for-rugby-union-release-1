<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	Router::connect('/', array('controller' => 'homes', 'action' => 'index'));
	Router::connect('/', array('controller' => 'homes', 'action' => 'index'));
	Router::connect('/Home', array('controller' => 'homes', 'action' => 'index'));
	//Router::connect('/about', array('controller' => 'cmspages', 'action' => 'about'));
	/*
	Router::connect('/wishlists', array('controller' => 'wishlists', 'action' => 'index'));
	Router::connect('/about', array('controller' => 'cmspages', 'action' => 'about'));
	Router::connect('/faq', array('controller' => 'cmspages', 'action' => 'faq'));
	Router::connect('/how-it-works', array('controller' => 'cmspages', 'action' => 'how_it_works'));
	Router::connect('/amigros-points', array('controller' => 'cmspages', 'action' => 'amigros_points'));
	Router::connect('/delivery-details', array('controller' => 'cmspages', 'action' => 'delivery_details'));
	Router::connect('/career', array('controller' => 'cmspages', 'action' => 'career'));
	Router::connect('/return-and-cancellation', array('controller' => 'cmspages', 'action' => 'return_and_cancellation'));
	Router::connect('/terms-and-conditions', array('controller' => 'cmspages', 'action' => 'terms_and_conditions'));
	Router::connect('/privacy-policy', array('controller' => 'cmspages', 'action' => 'privacy_policy'));
	Router::connect('/social-media-disclaimer', array('controller' => 'cmspages', 'action' => 'social_media_disclaimer'));
	Router::connect('/contact-us', array('controller' => 'cmspages', 'action' => 'contactus'));
	*/
	Router::connect('/admin', array('plugin' => 'Admin', 'controller' => 'users', 'action' => 'login'));
	
	/*
	Router::connect('/webservice', array('plugin' => 'Webservice', 'controller' => 'users', 'action' => 'login'));
	Router::connect('/search', array('controller' => 'search', 'action' => 'index'));
	Router::connect('/checkout', array('controller' => 'carts', 'action' => 'index'));
	Router::connect('/cart', array('controller' => 'checkout', 'action' => 'index'));
	Router::connect('/carts', array('controller' => 'checkout', 'action' => 'index'));
	Router::connect('/trackorder', array('controller' => 'carts', 'action' => 'trackorder'));
	Router::connect('/trackorderlist', array('controller' => 'carts', 'action' => 'trackorderlist'));
	Router::connect('/bundle', array('controller' => 'products', 'action' => 'bundle'));
	Router::connect('/promotional', array('controller' => 'products', 'action' => 'promotional'));
	Router::connect('/discount', array('controller' => 'products', 'action' => 'discount'));
	Router::connect('/trackorderdetailsprint', array('controller' => 'carts', 'action' => 'trackorderdetailsprint'));
	Router::connect('/admin/profile', array('plugin' => 'Admin','controller' => 'profiles', 'action' => 'index'));
	Router::connect('/blog/', array('action' => '/app/webroot/blog/'));
	*/


	//Router::connect('/:htaccessId/*',array('controller' => 'products','action' => 'product_list','listByCategory'),array('htaccessId'=>'[a-z0-9-_]*list$'));
	//Router::connect('/:htaccessId/*',array('controller' => 'products','action' => 'product_list','listByBrand'),array('htaccessId'=>'[a-z0-9-_]*brand$'));
	//Router::connect('/:htaccessId',array('controller' => 'products','action' => 'productdetails'),array('htaccessId'=>'[a-z0-9-_~]*'));
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
