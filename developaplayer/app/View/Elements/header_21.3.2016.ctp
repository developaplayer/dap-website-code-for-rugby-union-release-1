			
<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navigation">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>                    
	</button>
	<a class="navbar-brand" href="<?php echo $this->webroot; ?>"><img src="<?php echo $this->webroot; ?>images/logo.png" alt="Develop a Player"></a>
</div>
<div class="collapse navbar-collapse main-navigation pull-right">             
	<ul class="nav navbar-nav">                        
		<li><a href="<?php echo $this->webroot; ?>">Home</a></li>
		<li><a href="<?php echo $this->webroot; ?>cms_pages/about">About</a></li>
		<li><a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions">Terms & Conditions</a></li>
		<li><a href="#">Contact</a></li>
	</ul>
</div> 
<div class="topbar_bottom"> 

      
	<div class="collapse navbar-collapse main-navigation pull-left">             
		<ul class="nav navbar-nav"> 

		
			<li><a href="<?php echo $this->webroot; ?>players/player_list">Players</a></li>
			<li><a href="<?php echo $this->webroot; ?>coach_courses">Coaching Courses</a></li>
			<?php if($this->Session->read('Auth.Member.type')=='C' || $this->Session->read('Auth.Member.id')=="")
			{
				?>
			<li><a href="<?php echo $this->webroot; ?>coaches/coach_list">Coaches</a></li>
			<?php } ?>
			<li><a href="<?php echo $this->webroot; ?>products/product_list">Purchase Rugby Gears</a></li>
		</ul>
	</div> 
	
	 
	<div class="pull-right">             
		<ul class="nav navbar-nav">
	
			<li class="dropdown">
			<?php if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P')
			           {
				      ?>
				<a href="<?php echo $this->webroot; ?>players/player_list" class="dropdown-toggle" data-toggle="dropdown">My Profile<b class="caret"></b></a>
		<?php }else{?>
		<a href="<?php echo $this->webroot; ?>players/player_list" class="dropdown-toggle" data-toggle="dropdown">Players ZONE <b class="caret"></b></a>
		
		<?php } ?>
				<ul class="dropdown-menu">
					
					
					<?php if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P')
			           {
				      ?>
					    <li class="reg"><a href="<?php echo $this->webroot; ?>players/player_profile">Player Profile </a></li>
					  	<li class="log"><a href="<?php echo $this->webroot; ?>players/logout">Logout </a></li>					
					<?php }else{ ?>
					   <li class="reg"><a href="<?php echo $this->webroot; ?>players/register">Player Profile Registration</a></li>
						<li class="log"><a href="<?php echo $this->webroot; ?>players/login">Player Login</a></li>
					   
					 <?php } ?>
					<?php if($this->Session->read('Auth.Member.type')=='F' ||$this->Session->read('Auth.Member.id')=='')
					 {
					 ?>
				 <?php if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='F')
					 {
					 ?>
					  <li class="reg family_member"><a href="<?php echo $this->webroot; ?>families/family_profile">Family Member Profile</a></li>
					
					 <?php }else{ ?>
						 
						<li class="reg family_member"><a href="<?php echo $this->webroot; ?>families/register">Family Member Registration</a></li>
					<?php  } ?>
					
					<?php if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='F')
					 {
					 ?>
					  <li class="log family_member"><a href="<?php echo $this->webroot; ?>families/logout">Logout</a></li>
					
					 <?php }else{?>
					<li class="log family_member"><a href="<?php echo $this->webroot; ?>families/login">Family Member Login</a></li>
					 <?php } }?>
					
				</ul>
			</li>
			
			
			<?php if($this->Session->read('Auth.Member.type')=='C' ||$this->Session->read('Auth.Member.id')=='')
					 {
					 ?>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Coaches Zone <b class="caret"></b></a>
				<ul class="dropdown-menu">
				<?php      
					 if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C')
					 {
					 ?>
					  <li class="reg"><a href="<?php echo $this->webroot; ?>coaches/coach_profile"> Profile</a></li>
					
					 <?php }else{?>
					 
					  <li class="reg"><a href="<?php echo $this->webroot; ?>coaches/register">Coach Profile Registration</a></li>
					 
					 <?php } ?>
					 
					<?php 					
					 if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C')
					 {
					 ?>
						<li class="log"><a href="<?php echo $this->webroot; ?>coaches/logout">Logout</a></li>		
					<?php }else{?>
						<li class="log"><a href="<?php echo $this->webroot; ?>coaches/login">Coach Login</a></li>								
					 <?php } } ?>
				</ul>
			</li>
			
		</ul>
	</div> 
</div>      
