<section class="newsletter_holder vspace30 center">
	<div class="container">
		<h2>Subscribe for Develop A Player Newsletter</h2>
		<p>Join our email list &amp; stay updated with all upcoming courses, events.</p>
		<label id="msg" style="font-size:20px"></label>
		<form action="<?php echo $this->webroot;?>homes/newslatter" method="post">
			<span class="newsletter_fld_holder">
				<input type="email" placeholder="Enter your Email Address" name="newslatter_email" id="newslatter_email" autocomplete="off" >
				<span data-toggle="modal" data-target="#myModal_subscribe">
				<input type="button" class="book_training" onclick="newslatter()" value="Subscribe" >
				</span>
			</span>
		</form>
	</div>
</section>

<div class="modal fade" id="myModal_subscribe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content subscription_modal">
				<div class="modal-header">
				   
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
				</div>	
				
                <div class="modal-body">
                	<h2 id="msg_id"></h2>
                </div>
                <div class="modal-footer"></div>									
			</div>
		</div>
	</div>
</div>

<section class="vspace30">    	
	<div class="container">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
			<h3>Develop A Player</h3>
			<ul>
				<li><a href="<?php echo $this->webroot?>players/player_list">Players</a></li>
				<li><a href="<?php echo $this->webroot?>coach_courses">Coaching Courses</a></li>
				<li><a href="<?php echo $this->webroot?>coaches/coach_list">Coaches</a></li>
				<li><a href="<?php echo $this->webroot?>products/product_list">Purchase Rugby Gear</a></li>             
			</ul>     
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
			<h3>Help</h3>        	
			<ul>
				<li><a href="<?php echo $this->webroot?>cms_pages/about">About</a></li>
				<li><a href="<?php echo $this->webroot?>cms_pages/contactus">Contact Us</a></li>
				<li><a href="<?php echo $this->webroot?>cms_pages/terms_and_conditions">Terms &amp; Conditions</a></li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
			<h3>Social Media</h3>
			<ul>
				<li><a class="fb" target="_blank" href="https://www.facebook.com/developaplayer/?fref=ts">Facebook</a></li>
				<li><a class="tw" target="_blank" href=" https://twitter.com/developaplayer">Twitter</a></li>
				<li><a class="youtube" target="_blank" href="https://www.youtube.com/channel/UCHbYsxEQQOm7YxUX3JM8s1Q">Youtube</a></li>
				<li><a class="ln" target="_blank" href="https://www.linkedin.com/in/chris-miles-141730119?trk=nav_responsive_tab_profile_pic">Linkedin</a></li>
			</ul>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 center">
			<a href="<?php echo $this->webroot; ?>"><img src="<?php echo $this->webroot; ?>images/logo.png" alt="Develop a Player"></a><br>
			<span class="copyright left">Copyright 2016. All Rights Reserved.<br>Powered by: <a href="http://www.accenza.com/" target="_blank">ACCENZA</a></span>
		</div>
	</div>
</section>

<script>
function newslatter(){
		$("#msg_id").html(' ');
		var newslatter_email= $('#newslatter_email').val();
		var atpos = newslatter_email.indexOf("@");
		var dotpos = newslatter_email.lastIndexOf(".");
		if(newslatter_email=="")
		{
			
			$("#msg_id").html('Enter Email Id.');
			return false;
		}
		else if(atpos<1 || dotpos<atpos+2 || dotpos+2>=newslatter_email.length)
		{
			
			$("#msg_id").html('Please Enter Your Valid Email Id.');
			return false;
		}
			$.ajax({
		type:"post",
		dataType:'json',
		url:'<?php echo $this->webroot; ?>homes/newslatter',
		data:{			
			'email':newslatter_email
		},
		success:function(response){				
			if(response.state == "1") {
				$("#msg_id").html('Thank you for your Subscription.');
				$('#newslatter_email').val('');
			} else if(response.state == "0") {
				$("#msg_id").html('You have already subscribed!');
				$('#newslatter_email').val('');
			}				
			},			
		});
	}

</script>