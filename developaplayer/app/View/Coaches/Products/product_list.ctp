
    <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Apparel</li>
                </ul>
                  <a href="<?php echo $this->webroot;?>carts/add_to_cart"><div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot ?>images/cart_icon.png" alt="">
                    	<span class="cart_val"><?php echo CakeSession::read('total_size'); ?></span>
                  	</span>
                    <span class="cart_prc">AUD <?php echo CakeSession::read('fprice'); ?><sup><?php echo CakeSession::read('sprice'); ?></sup></span>
                </div></a>
            </div>
		</section>
        <section class="product_listing">
            <div class="container"> 
            	<div class="prod_list_holder">           	
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 prod_list_left">
                        <h2 class="product_list_hd">Apparel</h2>                	    
              <div id='cssmenu'>
			   
              <ul> 
		    
		   <?php
 		   foreach($categories as $k=>$ProductCategory)
			{    
			         $all_SubCategory_list=array();					
						foreach($ProductCategory['SubCategory'] as $all_SubCategory)
						{
							$all_SubCategory_list[]=$all_SubCategory;
						}					
					?>
					<li class='has-sub'><a href='#'><span><?php echo $ProductCategory['ProductCategory']['name'] ?></span></a>

                                <ul>
							     
								   <?php foreach ( (array) $all_SubCategory_list as $all_SubCategory_lists){?>

                                        <li class="custom_class_sub" id="<?php echo $all_SubCategory_lists['id'] ?>"><a href="#" class="sub_cat_class_link" data-idval="<?php echo $all_SubCategory_lists['id'] ?>" id="k<?php echo $all_SubCategory_lists['id'] ?>"><?php echo $all_SubCategory_lists['name'] ?></a></li>   
                                    <?php } ?>										
                                 </ul>
									
								   
                             </li>
								
			              <?php } ?>  
                               
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 prod_list_right">
                        <h2 class="product_list_hd rightpanel">
                        	<ul>
							    <li><a href="javascript:void(0);" data-myAttri="All" onclick="filter_by_attribue('All',this)" class="custom_filter_class active">All</a></li>
                            	<li><a href="javascript:void(0);" data-myAttri="heigh" onclick="filter_by_attribue('heigh',this)" class="custom_filter_class">High Price</a></li>
                                <li><a href="javascript:void(0);" data-myAttri="low" class="custom_filter_class" onclick="filter_by_attribue('low',this)">Low Price</a></li>
                                <li><a href="javascript:void(0);" data-myAttri="new" class="custom_filter_class" onclick="filter_by_attribue('new',this)" >New</a></li>
								<!-- <li><a href="<?php echo $this->webroot ?>products/product_list"><input type="button" name="reset_filter" id="reset_filter" value="Reset"/></a></li>
                             --></ul>
                        </h2>
					<div id="loader1" style="text-align:center;"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>	
				<div id="div_product_filter_id">	
                    <div class="col-lg-12" id="equal_list_height">
					<?php	if(!empty($all_product) && count($all_product)>0)
						{
							foreach((array) $all_product as $all_products)
							{
						?>
                        	<div class="col-lg-4 col-lg-6 col-sm-6 col-xs-6 center prod_holder">
                            	<div class="prod_img"><a href="<?php echo $this->webroot; ?>products/product_details/<?php echo base64_encode($all_products['Product']['id']); ?>"><img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $all_products['ProductImage']['image'] ?>" alt=""></a></div>
                                <div class="prod_nm"><a href="<?php echo $this->webroot; ?>products/product_details/<?php echo base64_encode($all_products['Product']['id']); ?>"><?php echo $all_products['Product']['name'];?></a></div>
								
							<?php 							
							$price_explode = explode('.',$all_products['Product']['price']);

							if(isset($price_explode[0]))
							{
								$fistpartprice= $price_explode[0];
								
							}else
								
							{
								$fistpartprice=00;	
									
							}							
							if(isset($price_explode[1]))
							{
								$secpartprice= $price_explode[1];
								
							}else
								
							{
								$secpartprice='';										
							}
							
							?>
							
								
                                <div class="prod_prc">AUD <?php echo $fistpartprice; ?><sup><?php echo $secpartprice; ?></sup></div>
                            </div>
                          <?php 
							}
						   }
						?>
                      </div>
					  
					  <div id="loader" style="display:none;text-align:center;"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>
            </div>
				</div>
			
                    </div>
	
            	</div>
					
			
		</section>                                                   
	</div>  

<script type="text/javascript">
	var is_loading = false;
	var limit = 4;
	var offset = 1;
	$('#loader').hide();
	$('#loader1').hide();	
function filter_by_attribue(FilterAtt,obj)
{  
	 is_loading = false;
	 limit = 4;
	 offset = 1;
	  $('#loader1').show();
	
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>products/product_search_filter',
		data:{
			'filterAttr':FilterAtt
		},
		success:function(response)
		{
			 $('#div_product_filter_id').html(response);				
			 $('.custom_filter_class').removeClass('active');
			 $(obj).addClass('active');	
              $('#loader1').hide();			 
		},
		error:function(response){
			
		}
	});
 }

//=================================================================================//
$(document).ready(function(){
	$( ".custom_class_sub" ).click(function(){
      
	  is_loading = false;
	  limit = 4;
	  offset = 1;
	  $('#loader1').show();
	  var  cat_id = $(this).attr('id');	  
	  var last_activeAttr = $('.custom_filter_class.active').attr('data-myAttri');	  
	  //=================for filter by category=============================//	  
	  $.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>products/product_search_filter',
		data:{
			'cat_id':cat_id,'filterAttr':last_activeAttr,
		},
		success:function(response)
		{
			   $('#div_product_filter_id').html(response);			  
			   $('.sub_cat_class_link').removeClass('active');			   
			   $('#k'+cat_id).addClass('active');
			   $('#loader1').hide();
				  
              //$( ".custom_class_sub" ).addClass();			 
			// $('.custom_filter_class').removeClass('active');
			// $(obj).addClass('active');		
		},
		error:function(response){
			
		}
	});
	
	});	
});

//=======================For Pagination========================================// 
       	  		
	  
       	
    // initialize is_loading by false to accept new loading
    //svar limit = 4; // limit items per page
	
    $(function() {
		
        $(window).scroll(function() {
		 			
		  var last_activeAttr = $('.custom_filter_class.active').attr('data-myAttri');
		  var cat_id = $('.sub_cat_class_link.active').attr('data-idval');			 
			//alert(last_activeAttr);
            if($(window).scrollTop() + $(window).height() == $(document).height()) {				
					//alert(is_loading);	           
                if (is_loading == false) {										
					//alert(total_hit_count);
                  /* if(total_hit_count==0)
					 {						 
						if(last_activeAttr=='heigh')
						{
							
							var limit1 = 4;
							var offset1 = 1;
							
							//var limit = limit1;
							//var offset = offset1;
							var total_hit_count =1;						
						}
	              
					 }*/	  
					   //alert(last_activeAttr);
				      offset = limit + offset;	
					 // alert( total_hit_count+','+limit+','+offset);
					// stop loading many times for the same page
                    // set is_loading to true to refuse new loading
						// alert(offset);
					//alert(offset);
                      is_loading = true;
                    // display the waiting loader
                    $('#loader').show();
                    // execute an ajax query to load more statments
                    $.ajax({
                        url: '<?php echo $this->webroot;?>products/load_more_product_by_filter',
                        type: 'POST',
                        data: {last_id:offset, limit:limit,cat_id:cat_id,filterAttr:last_activeAttr},
                        success:function(data){		
                         						
                            // now we have the response, so hide the loader
                            $('#loader').hide();
                            // append: add the new statments to the existing data
                            $('#equal_list_height').append(data);
							is_loading = false;
                            // set is_loading to false to accept new loading
                            
                        }
                    });
                }
           }
        });
    });


</script>	
