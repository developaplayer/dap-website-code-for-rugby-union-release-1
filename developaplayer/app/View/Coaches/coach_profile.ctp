<script type="text/JavaScript">
function video_count(count)
{
	$('#video_count').val(count);
	$('#coustom_video').val(count);
}
function add_video_url()
{
	var URL =$('#video_url').val();
	var USERID='<?php echo $this->Session->read('Auth.Member.id'); ?>';
	var video_count=$('#video_count').val();
	var last_id=$('#last_id'+video_count).val();
 $.ajax({
			data: {'URL':URL,'USERID':USERID,'position':video_count,'last_id':last_id},
			url:'<?php echo $this->webroot ?>coaches/add_coach_video',
			type:'POST',
			success:function(result)
			{
                    //==================data================================
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();
					if(dd<10){
						dd='0'+dd
					} 
					if(mm<10){
						mm='0'+mm
					} 
                    var today = dd+'/'+mm+'/'+yyyy;
                    //=======================================================					
			   obj = JSON.parse(result);
			   $("#video"+video_count).attr("src",obj.video_image);
			   $("#videotitle"+video_count).html(obj.video_title);
			   $("#craete_date"+video_count).html(today);			   
			   $("#image_thumb"+video_count).val(obj.video_image);
			   $("#youtube_url"+video_count).val(obj.video_link);
			   $("#last_id"+video_count).val(obj.id);
			   $("#iframe_id"+video_count).html(obj.video_iframe);
			   $("#iframe_title_id"+video_count).html(obj.video_title);
			   $('#video_url').val('');
			   $('#myModal').modal('hide');	
                window.location.reload();			   
			}
		})
}
</script>
 <div class="main_container">
    <section class="innrpg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Coach Profile</a></li>
                    <li><span>&raquo;</span>&nbsp;<?php echo $all_coach_data['User']['first_name'].' '.$all_coach_data['User']['last_name'];?></li>
                </ul>
            </div>
		</section>
        <section>
            <div class="container vspace30">
            	<div class="profile_top_holder coach_profile">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile_top_holder_lft">
                      <div class="coach_profile_img"><img src="<?php echo $this->webroot.'uploads/profiles/coach/profile_pic/'.$all_coach_data['CoachDetail']['profile_img']?>"/></div>
                         <!--  <div class="coach_profile_img"><img src="<?php echo $this->webroot;?>images/coach_img.jpg"></div>-->  						
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 profile_top_holder_rght">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2 class="coach_heading"><?php echo $all_coach_data['User']['first_name'].' '.$all_coach_data['User']['last_name'];?></h2>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="short_desc_holder">
                                    D.O.B .: <?php echo date("jS M Y", strtotime($all_coach_data['CoachDetail']['dob'])); ?><br>
                                    Nationality:  <?php echo $all_coach_data['Nationality']['country_name'];?>
                                </div>
                            </div>
							<?php 					
							if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C' && $this->Session->read('Auth.Member.id')==$user_id) 
							{
							?>
						   <span  class="edit_profile_btn"><a href="<?php echo $this->webroot;?>coaches/edit_profile">Edit</a></span>
							<?php } ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coach_details_holder">
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Coaching Speciality</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php echo $all_coach_data['CoachSpecialityP']['name'];?></div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Secondary Speciality</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php echo $all_coach_data['CoachSpecialityS']['name'];?></div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Height / Weight</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php echo $all_coach_data['CoachDetail']['height'];?> cm / <?php echo $all_coach_data['CoachDetail']['weight'];?> Kg</div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Coaching Certificates</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght">
                                    <ul>
									<?php
									if(!empty($all_coachcertificate_data ))
									{
									foreach($all_coachcertificate_data as $certificate_data){?>
                                        <li><?php echo $certificate_data['Certificate']['name']; ?></li>
                                    <?php }
									}else{
									echo 'No Certificate Found';
									}
									?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
		</section>
	</section>
    <section class="graph_area">
        <div class="container">
            <ul class="nav nav-tabs">
                <!-- <li class="">
                    <a href="#1" data-toggle="tab">Current Coaching Profile</a>                
               	</li>-->
               <li class="active">
                    <a href="#3" data-toggle="tab">Record of Achievement</a>                
                </li>
            </ul>
          	<div class="tab-content">
               <!-- <div class="tab-pane" id="1">
                	<div class="graph_filter_holder center">
                    	<select id="select_age_group">
						<option selected>Select Age Group</option>
						<?php foreach($AgeGroup_list as  $key=>$AgeGroup_lists){?>}
                            <option value="<?php echo $key;?>"><?php echo $AgeGroup_lists;?></option>
						<?php  }?>	
                        </select>
                      <select>
                        	<option selected>Select Age Group</option>
                            <option>Under 19</option>
                            <option>Above 19</option>
                        </select>
                        <input type="button" value="Analyse" onclick="get_graph_val()">
                    </div>
					 <div id="load_icon_div" class="search_loader" style="display:none; text-align: center;"><span class="loader_icon"><img src="<?php echo $this->webroot;?>images/ring.gif"/></span></div>
					<div id="total_div_content">
                    <div class="graph_holder">
                    	<div class="bar_outer one">
                        	<div class="bar_innr">
                            	<ul></ul>                                
                            </div>                            
                        </div>
                        <div class="bar_details one">
                        	<img src="<?php echo $this->webroot;?>images/ico_a.png" alt=""> <span class="bar_details_val">1.4</span> <img src="<?php echo $this->webroot;?>images/ico_attack_small.png" alt="">                        </div>												                            
                        <div class="bar_outer two">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details two">
                        	<img src="<?php echo $this->webroot;?>images/ico_d.png" alt=""> <span class="bar_details_val">1.8</span> <img src="<?php echo $this->webroot;?>images/ico_defence_small.png" alt="">                        </div>						                            
                        <div class="bar_outer three">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details three">
                        	<img src="<?php echo $this->webroot;?>images/ico_c.png" alt=""> <span class="bar_details_val">2.2</span> <img src="<?php echo $this->webroot;?>images/ico_physical_cond_small.png" alt="">                  	</div>						                            
                        <div class="bar_outer four">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details four">
                       <img src="<?php echo $this->webroot;?>images/ico_m.png" alt=""> <span class="bar_details_val">1.7</span> <img src="<?php echo $this->webroot;?>images/ico_mental_cond_small.png" alt=""> </div>
						<div id="script_text_val">	
						<script type="text/JavaScript">
							var low_lim = 1;
							var up_lim = 1.5;
							var low_limit_val = (low_lim * 80) - 1;
							low_limit_val=parseInt(low_limit_val);
							//alert(low_limit_val);
							$('.bar_outer').css('bottom',low_limit_val+'px');
							var up_limit_val = (up_lim - low_lim) * 80;
							up_limit_val=parseInt(up_limit_val);
							$('.bar_innr').css('height',up_limit_val+'px');
							var cur_pos1 = 1.1								
							var val1 = (cur_pos1 - low_lim)*10;														
							//alert(val1);
							val1=parseInt(val1);
							//alert(val1);
							for(i=0; i<val1;i++){
								$(".bar_outer.one .bar_innr ul").append("<li>&nbsp;</li>");
							}
							var cur_pos2 = 1.8								
							var val2 = (cur_pos2 - low_lim)*10;														
							for(i=0; i<val2;i++){
								$(".bar_outer.two ul").append("<li>&nbsp;</li>");
							}
							var cur_pos3 = 2.2								
							var val3 = (cur_pos3 - low_lim)*10;														
							for(i=0; i<val3;i++){
								$(".bar_outer.three ul").append("<li>&nbsp;</li>");
							}
							var cur_pos4 = 1.7								
							var val4 = (cur_pos4 - low_lim)*10;														
							for(i=0; i<val4;i++){
								$(".bar_outer.four ul").append("<li>&nbsp;</li>");
							}
							var one = $('.bar_outer.one ul').height()+ low_limit_val - 13;
							$('.bar_details.one').css('bottom', one+'px');
							var two = $('.bar_outer.two ul').height()+ low_limit_val - 13;
							$('.bar_details.two').css('bottom', two+'px');
							var three = $('.bar_outer.three ul').height()+ low_limit_val - 13;
							$('.bar_details.three').css('bottom', three+'px');
							var four = $('.bar_outer.four ul').height()+ low_limit_val - 13;
							$('.bar_details.four').css('bottom', four+'px');
						</script>
						</div>
                    	<span class="graph_row_val eight">3.5</span>
                    	<span class="graph_row_val seven">3.0</span>
                        <span class="graph_row_val six">2.5</span>
                        <span class="graph_row_val five">2.0</span>
                        <span class="graph_row_val four">1.5</span>
                        <span class="graph_row_val three">1.0</span>
                        <span class="graph_row_val two">0.5</span>
                        <span class="graph_row_val one">0</span>
                    </div>
                    <div class="graph_table_holder">
                    	<div class="graph_table_row">
                        	<div class="graph_table_col">&nbsp;</div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot;?>images/ico_attack_small.png"> Attack <img src="<?php echo $this->webroot;?>images/ico_a.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot;?>images/ico_defence_small.png"> Defence <img src="<?php echo $this->webroot;?>images/ico_d.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot;?>images/ico_physical_cond_small.png"> Physical Conditioning <img src="<?php echo $this->webroot;?>images/ico_c.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot;?>images/ico_mental_cond_small.png"> Mental Conditioning <img src="<?php echo $this->webroot;?>images/ico_m.png"></div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Upper Limit <img src="<?php echo $this->webroot;?>images/ico_upperlimit.png" alt=""></div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Current Position <img src="<?php echo $this->webroot;?>images/ico_current_pos.png" alt=""></div>
                            <div class="graph_table_col">1.1</div>
                            <div class="graph_table_col">1.2</div>
                            <div class="graph_table_col">1.3</div>
                            <div class="graph_table_col">1.1</div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Lower Limit <img src="<?php echo $this->webroot;?>images/ico_lowerlimit.png" alt=""></div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                        </div>
                    </div>
                </div>
				</div>-->                
                <div class="tab-pane active" id="3">
                	<div class="achievement_holder coach_profile">
                    	<div class="achievement">
                        	<h2 class="achievement_hd">Attack</h2>
                        	<div class="achievement_innr red">
							<?php
                             if(!empty($all_lavel_by_coach_Tight_5))
							 {
							foreach ($all_lavel_by_coach_Tight_5 as $all_lavel_by_coach_Tight_5s) {?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Level <?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['level_number']; ?></span>
                                    <span class="achievement_dt">on <?php echo date('j F, Y',strtotime($all_lavel_by_coach_Tight_5s['CoachLevel']['created'])); ?></span>
                                    <span class="achievement_det"><?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['name']; ?></span>
                                </div>
							<?php } }?> 
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Defence</h2>
                        	<div class="achievement_innr blue">
                            	<?php
                             if(!empty($all_lavel_by_coach_Loose_Forwards))
							 {
							foreach ($all_lavel_by_coach_Loose_Forwards as $all_lavel_by_coach_Tight_5s) {?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Level <?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['level_number']; ?></span>
                                     <span class="achievement_dt">on <?php echo date('j F, Y',strtotime($all_lavel_by_coach_Tight_5s['CoachLevel']['created'])); ?></span>
                                    <span class="achievement_det"><?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['name']; ?></span>
                                </div>
							<?php } }?> 
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Physical Conditioning</h2>
                        	<div class="achievement_innr orange">
                            	<?php
                             if(!empty($all_lavel_by_coach_Half_Backs))
							 {
							foreach ($all_lavel_by_coach_Half_Backs as $all_lavel_by_coach_Tight_5s) {?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Level <?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['level_number']; ?></span>
                                     <span class="achievement_dt">on <?php echo date('j F, Y',strtotime($all_lavel_by_coach_Tight_5s['CoachLevel']['created'])); ?></span>
                                    <span class="achievement_det"><?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['name']; ?></span>
                                </div>
							<?php } }?> 
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Mental Conditioning</h2>
                        	<div class="achievement_innr green">
                            	<?php
                             if(!empty($all_lavel_by_coach_Inside_Backs))
							 {
							foreach ($all_lavel_by_coach_Inside_Backs as $all_lavel_by_coach_Tight_5s) {?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Level <?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['level_number']; ?></span>
                                    <span class="achievement_dt">on <?php echo date('j F, Y',strtotime($all_lavel_by_coach_Tight_5s['CoachLevel']['created'])); ?></span>
                                    <span class="achievement_det"><?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['name']; ?></span>
                                </div>
							<?php } }?> 
                            </div>
                        </div>
                        <!--<div class="achievement">
                        	<h2 class="achievement_hd">Outside Backs</h2>
                        	<div class="achievement_innr purple">
                            	<?php
                             if(!empty($all_lavel_by_coach_Outside_Backs))
							 {
							foreach ($all_lavel_by_coach_Outside_Backs as $all_lavel_by_coach_Tight_5s) {?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Level <?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['level_number']; ?></span>
                                    <span class="achievement_dt">on <?php echo date('j F, Y',strtotime($all_lavel_by_coach_Tight_5s['CoachLevel']['created'])); ?></span>
                                    <span class="achievement_det"><?php echo $all_lavel_by_coach_Tight_5s['CoachLevel']['name']; ?></span>
                                </div>
							<?php } }?> 
                        </div>
                    </div>-->
                </div>
			</div>
        </div>
	</section>
    <section>
        <div class="container">
        	<div class="profile_bottom_holder">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vspace30 profile_bottom_lft">
                    <h2 class="innrpg_subhd">Highlights</h2>
                    <ul>
					<?php for($i=1;$i<=6;$i++){?>
					  	<?php if(!empty($all_video_data)&& isset($all_video_data[$i]['video_image'])){?>
                        <li class="vid_ico">
						<?php  if($all_video_data[$i]['video_type']==0){ ?>	
							<span class="vid_ico_img">                           								    								
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>"> <img id="video<?php echo $i; ?>" src="<?php echo $all_video_data[$i]['video_image'];?>"></a>
								   <?php 					
									if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C' && $this->Session->read('Auth.Member.id')==$user_id)
									{
									?>
								   <span class="upload_vid_icon">
								      <a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								    </span>
								<?php } ?>
							</span>
							   <span class="vid_ico_nm">
                                       	<a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php echo $all_video_data[$i]['video_title'];?></a>
                              	</span>	
                               <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"><?php echo date('d/m/Y',strtotime($all_video_data[$i]['created']));?></span>
							   	</span>
							  <input type="hidden" name="image_thumb<?php echo $i ?>" id="image_thumb<?php echo $i ?>" value=""/>			
						      <input type="hidden" name="youtube_url<?php echo $i ?>" id="youtube_url<?php echo $i ?>" value=""/>
						      <input type="hidden" name="last_id<?php echo $i ?>" id="last_id<?php echo $i ?>" value="<?php echo $all_video_data[$i]['id'] ?>"/>
			<!-- Start Modal for Video -->
	                             <?php } ?>
			<?php  if($all_video_data[$i]['video_type']==0){ ?>			
							<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id<?php echo $i; ?>"> <?php echo $all_video_data[$i]['video_title'] ?></span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body" id="iframe_id<?php echo $i; ?>">
										 <?php echo $all_video_data[$i]['video_iframe'] ?>
										</div>
									</div>
								</div>
							</div>
						</div>
			<?php } ?>
			<?php if($all_video_data[$i]['video_type']==1){?> 
                    	<span class="vid_ico_img">                           								    								
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>"> 
								  <video width="150" height="94">
								          <source src="<?php echo $this->webroot ?>uploads/video/coach/<?php echo $all_video_data[$i]['video_link'] ?>" >							
							      </video>
							</a>
								   <?php 					
									if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C' && $this->Session->read('Auth.Member.id')==$user_id)
									{
									?>
								   <span class="upload_vid_icon">
								      <a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								    </span>
								<?php } ?>
								</span>	
								  <span class="vid_ico_nm">
                                       	<a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php echo $all_video_data[$i]['video_title'];?></a>
                              	</span>	
                               <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"><?php echo date('d/m/Y',strtotime($all_video_data[$i]['created']));?></span>
							   	</span>
						<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id<?php echo $i; ?>"> <?php echo $all_video_data[$i]['video_title'] ?></span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body" id="iframe_id<?php echo $i; ?>">
										<video width="574" height="263" controls>
								          <source src="<?php echo $this->webroot ?>uploads/video/coach/<?php echo $all_video_data[$i]['video_link'] ?>" >							
							         </video>
										</div>
									</div>
								</div>
							</div>
						</div>	
					<?php
					 }
					?>
                  </li>							
					<?php
					    }else{								
					?>
						  <li class="vid_ico">
                        <span class="vid_ico_img">
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>" > <img id="video<?php echo $i; ?>" src="<?php echo $this->webroot; ?>images/no_video.jpg"></a>
								    <span class="upload_vid_icon">
									<?php 					
										if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C' && $this->Session->read('Auth.Member.id')==$user_id) 
										{
										?>
								<a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								<?php } ?>
								</span></span>
							       <span class="vid_ico_nm">
                                       	<a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php //echo $all_video_data[$i]['video_title'];?>No Video Title</a>
                              	</span>	
                               <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"></span>
							   	</span>
							  <input type="hidden" name="image_thumb<?php echo $i ?>" id="image_thumb<?php echo $i ?>" value=""/>
						     <input type="hidden" name="youtube_url<?php echo $i ?>" id="youtube_url<?php echo $i ?>" value=""/>
						     <input type="hidden" name="last_id<?php echo $i ?>" id="last_id<?php echo $i ?>" value="0"/>
							 <!-- Start Modal for Video -->				  
							<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id<?php echo $i ?>">Video Title</span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body" id="iframe_id<?php echo $i ?>">
										 No video Found !
										</div>
									</div>
								</div>
							</div>
						</div>
		<!-- End Modal -->	
                         </li>			
                          <?php } ?>
					<?php } ?>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vspace30 profile_bottom_right">
                    <h2 class="innrpg_subhd">Notes</h2>
                    <p class="innrpg_txt" style="word-wrap: break-word; overflow-wrap: break-word; width: 100%; white-space: pre-line;">
                        <?php echo $all_coach_data['CoachDetail']['note'];?>
                    </p>
                </div>
        	</div>
        </div>
    </section>
    </div>   
 <!-- Start Modal --> 
<?php 					
 if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='C' && $this->Session->read('Auth.Member.id')==$user_id) 
 {
 ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Upload your video</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
				<form action="" method="post">
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="text" name="video_url" id="video_url" class="form-control" placeholder="video url" required></div>
                        </div>
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="button" name="vide_url_submit" id="vide_url_submit" value="Upload" onclick="add_video_url();"></div>
                        </div>
						<input type="hidden" name="video_count" id="video_count"/>
                </form>
						<?php  echo $this->Form->create('coaches',array('id'=>'edit_id','controller'=>'coaches','action'=>'upload_video', 'enctype'=>'multipart/form-data')); ?>							
						 <div class="othersVidHolder">	
                         	<div class="row vspace10">
                            	<div class="col-lg-12"><input type="text" name="video_title" id="video_title" class="form-control" placeholder="Video Title" required></div>				 
							</div>                                
						    <input type="hidden" name="member_id_video" id="member_id_video" value="<?php echo $this->Session->read('Auth.Member.id') ?>"/>	
						    <input type="hidden" name="coustom_video" id="coustom_video"/>					 
                            <div class="row vspace10">
                            	<div class="col-lg-12"><input type="file" name="own_video" id="own_video"></div>								
                           	</div>
							<div><strong>Please upload Supported video format  mp4/WebM/Ogg</strong></div>
                            <div class="row vspace10">
								<div class="col-lg-12"><input type="submit" name="vide_url_submit" id="video_url_submit" value="Upload Video"></div>                     		</div>
                        </div>
						<?php echo $this->Form->end(); ?>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
<?php } ?>
 <script type="text/javascript">
function get_graph_val(){	
	var select_value = $('#select_age_group').val();
	 $('#load_icon_div').show();
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>coaches/get_graph_value_by_age_group',
		data:{
			'select_value':select_value,'coach_id':'<?php echo $user_id; ?>'
		},
		success:function(response){
		        $('#load_icon_div').hide();
				$('#total_div_content').html(response);				
		},
		error:function(response){
			//$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
}
$(document).ready(function(){
	 //get_graph_val();
	 $('input[type=file]').change(function(){
		var file = this.files[0];
		name = file.name;
		size = file.size;
		//type = file.type;
	var type = name.split(".");
	if(type[1]=='mp4' || type[1]=='WebM' || type[1]=='Ogg')
	{	
		$('#video_url_submit').attr('type','submit');
	}else
	{   
        $('#video_url_submit').attr('type','button');
		$('#invalid_video_message').modal('show');
		 return false;
	}
	});
});
</script>
 	<!-- Start Modal: Add New Achievement -->     
    <div class="modal fade" id="invalid_video_message" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Message</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
				     	<p class="select_dt"><span style="color:red">invalid file format error !</span> &nbsp; <strong>Please upload video format mp4/WebM/Ogg</strong></p>
						<p></p>
						  <div class="row vspace10"></div>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>