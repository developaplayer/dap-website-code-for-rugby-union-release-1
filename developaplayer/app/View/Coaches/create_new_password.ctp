 <div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span> Coach's Registration</li>
			</ul>
		</div>
	</section> 
  <section class="vspace70 center">
  <div style="color:green"> <?php 
								if ($this->Session->check('Message.flash')) {
									echo $this->Session->flash();
								}
								if ($this->Session->check('Message.auth')) {
									//echo $this->Session->flash('auth');
								}
		                        ?>
								</div>
            <div class="container">
            	<div class="login_bg">
                	<div class="login_bg_innr">
                    	<div class="profile_ico_holder"><img src="<?php echo $this->webroot; ?>images/ico_profile.png" alt=""></div>                    
                    	
						  <?php  echo $this->Form->create('Coach',array('id'=>'sign_up_id','onSubmit'=>'return last_validation()','controller'=>'coaches','action'=>'create_new_password')); ?>
                            <div class="form-group email-input">
                                <label>New Password</label>
                                <!--<input type="email" name="email" class="form-control" required>-->
								<?php echo $this->Form->input('User.password', array('type' => 'password','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group password-input">
                                <label>Confrim Password</label>
<!--<input type="password" name="password" class="form-control" required>-->
								<?php echo $this->Form->input('User.cpassword', array('type' => 'password','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group">
                                <div>
								
							
								<?php echo $this->Form->input('User.forget_user_id', array('type' => 'hidden','div'=>false,'label'=>false,'class'=>"form-control","value"=>$param_id));?>
								</div>
								
                                <span class="pull-right"><input type="submit" value="Submit"></span>
                            </div>
                       <?php echo $this->Form->end(); ?>
                    </div>
                </div>   
                <!--<span class="login_bg_bottom_txt">Don't have an account? <a href="javascript:void(0);">Sign up</a> as a coach.</span>-->
            </div>
		</section>   

</div>		