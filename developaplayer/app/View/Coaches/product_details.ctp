 <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Apparel</li>
                </ul>
                <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot;?>images/cart_icon.png" alt="">
                    	<span class="cart_val">5</span>
                  	</span>
                    <span class="cart_prc">AUD 135<sup>99</sup></span>
                </div>
            </div>
		</section>
        <section class="product_listing">
            <div class="container"> 
            	<div class="prod_det_holder">           	
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 prod_det_left">
                    	<div class="prod_det_gallery">
                            <div class="slide-smalls">
                                <ul  id="scroller" class="selectors">
                                    <li><a class="scroll_li_id" style="display:none" data-zoom-id="zoom1" href="<?php echo $this->webroot;?>images/jersey1.jpg" data-image="<?php echo $this->webroot;?>images/jersey1.jpg?scale.width=450"><img srcset="<?php echo $this->webroot;?>images/jersey1.jpg?scale.width=112 2x" src="<?php echo $this->webroot;?>images/jersey1.jpg?scale.width=56" alt=""/></a></li>
                                                
                                    <li><a class="scroll_li_id" style="display:none" data-zoom-id="zoom1" href="<?php echo $this->webroot;?>images/jersey2.jpg" data-image="<?php echo $this->webroot;?>images/jersey2.jpg?scale.width=450"><img srcset="<?php echo $this->webroot;?>images/jersey2.jpg?scale.width=112 2x" src="<?php echo $this->webroot;?>images/jersey2.jpg?scale.width=56" alt=""/></a></li>	
                                            
                                    <li><a class="scroll_li_id" style="display:none" data-zoom-id="zoom1" href="<?php echo $this->webroot;?>images/jersey3.jpg" data-image="<?php echo $this->webroot;?>images/jersey3.jpg?scale.width=450"><img srcset="<?php echo $this->webroot;?>images/jersey3.jpg?scale.width=112 2x" src="<?php echo $this->webroot;?>images/jersey3.jpg?scale.width=56" alt=""/></a></li>
                                                
                                    <li><a class="scroll_li_id" style="display:none" data-zoom-id="zoom1" href="<?php echo $this->webroot;?>images/jersey4.jpg" data-image="<?php echo $this->webroot;?>images/jersey4.jpg?scale.width=450"><img srcset="<?php echo $this->webroot;?>images/jersey4.jpg?scale.width=112 2x" src="<?php echo $this->webroot;?>images/jersey4.jpg?scale.width=56" alt=""/></a></li>
                                    
                                    <li><a class="scroll_li_id" style="display:none" data-zoom-id="zoom1" href="<?php echo $this->webroot;?>images/jersey5.jpg" data-image="<?php echo $this->webroot;?>images/jersey5.jpg?scale.width=450"><img srcset="<?php echo $this->webroot;?>images/jersey5.jpg?scale.width=112 2x" src="<?php echo $this->webroot;?>images/jersey5.jpg?scale.width=56" alt=""/></a></li>        
                                </ul>
                            </div>                             
                            <div class="main-display">
                                <a id="zoom1" data-options="textHoverZoomHint: ''; textClickZoomHint: ''; textExpandHint: '';rightClick: true;" class="MagicZoom" href="<?php echo $this->webroot;?>images/jersey1.jpg"><img src="<?php echo $this->webroot;?>images/jersey1.jpg?scale.width=450" alt=""/></a>           
                            </div>
						</div>
                        <script>
                        	$(document).ready(function(){
								$("#scroller").simplyScroll({
									customClass:'vert',
									orientation:'vertical',
									auto: false	,
									speed: 6
								});
								$(".scroll_li_id").show();
							});
                        </script>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-6 prod_det_mid">                        
                        <h2 class="product_title">Kids Dresden Blue Canterbury Tech Plus Impact Shoulder Pads</h2>
                        <span class="product_price">AUD 52<sup>99</sup></span>
                        <span class="size_selection_row">
                        	SELECT SIZE:<br>
                            <ul>
                            	<li>S</li>
                                <li>M</li>
                                <li>L</li>
                                <li>XL</li>
                                <li>XXL</li>
                            </ul>
                            <script>
                            	$(document).ready(function(){
									$('.size_selection_row ul li').click(function(){
										$(this).toggleClass('active');
									});
								});
                            </script>
                            <a href="javascript:void(0);" class="size_guide_link">Size Guide</a>
                        </span>
                        <span class="size_selection_row plus_minus">
                        	SELECT Quantity:<br>
                            <a href="javascript:void(0);" class="minus">-</a>
            				<input type="text" value="0" />
            				<a href="javascript:void(0);" class="plus">+</a>
                            <script>
                            	$(document).ready(function(){	
									$(".plus").click(function() {
										var text = $(this).prev(":text");
										text.val(parseInt(text.val(), 10) + 1);
									});
								
									$(".minus").click(function() {
										var text = $(this).next(":text");
										text.val(parseInt(text.val(), 10) - 1);
									});
								});	
                            </script>
                        </span>
                        <span class="size_selection_row">
                        	<a href="javascript:void(0);" class="add_to_cart">Add To Bag</a>
                        </span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 prod_det_right">                        
                        <div id="prod_det_tabs">
                            <ul>
                              <li><a href="#tabs-1">Details</a></li>
                              <li><a href="#tabs-2">Shipping &amp; Returns</a></li>
                            </ul>
                            <div id="tabs-1">
                                  <p>Canterbury Tech Plus Impact Shoulder Pads Dresden Blue Kids<br><br>
                                  This protective vest is ideal for junior and social club players. Specially designed to provide protection to the main impact zones of the shoulders.<br><br>
                                  Canterbury Tech Plus Impact Shoulder Pads Dresden Blue Kids<br><br>
                                  This protective vest is ideal for junior and social club players. Specially designed to provide protection to the main impact zones of the shoulders.<br><br>
                                  Canterbury Tech Plus Impact Shoulder Pads Dresden Blue Kids<br><br>
                                  This protective vest is ideal for junior and social club players. Specially designed to provide protection to the main impact zones of the shoulders.<br><br>
                                  Canterbury Tech Plus Impact Shoulder Pads Dresden Blue Kids<br><br>
                                  This protective vest is ideal for junior and social club players. Specially designed to provide protection to the main impact zones of the shoulders.<br><br>
                                  Canterbury Tech Plus Impact Shoulder Pads Dresden Blue Kids<br><br>
                                  This protective vest is ideal for junior and social club players. Specially designed to provide protection to the main impact zones of the shoulders.<br><br></p>
                            </div>
                            <div id="tabs-2">
                                  <p>Shipping &amp; Returns content goes here..</p>
                            </div>
                            <script>
								$(document).ready(function(){
									$( "#prod_det_tabs" ).tabs();
									$("#prod_det_tabs .ui-tabs-panel").mCustomScrollbar({
										scrollButtons:{
											enable:false
										},
										advanced: {
											updateOnContentResize: Boolean
										}
									});
								});
							</script>
                        </div>
                    </div>
            	</div>
            </div>
		</section>
        <section class="featured_training_gear vspace30 center">
            <div class="container">
            	<h2>You might be interested</h2>
            	<div class="flexslider training_gears">
                  	<ul class="slides">
                    	<li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod1.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Black and Orange Kooga IPS Pro V Protection Vest</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                        <li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod2.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Kids Dresden Blue Canterbury Tech Plus Impact Shoulder Pads</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                        <li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod3.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Electric Blue Asics Gel Lethal Tight 5 Rugby Boots</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                        <li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod4.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Red adidas Torpedo X-Ebition Training Rugby Ball</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                        <li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod2.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Electric Blue Asics Gel Lethal Tight 5 Rugby Boots</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                        <li>
                        	<span class="training_gear_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot;?>images/prod3.jpg" alt=""></a></span>
                            <span class="training_gear_nm"><a href="javascript:void(0);">Kids Dresden Blue Canterbury Tech Plus Impact Shoulder Pads</a></span>
                            <span class="training_gear_prc">AUD 69<sup>99</sup></span>
                       	</li>
                    </ul>
                </div>
            </div>
		</section>                                                   
	</div>    