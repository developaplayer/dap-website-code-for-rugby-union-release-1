<!--<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>-->
<div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span> Coach's Registration</li>
			</ul>
		</div>
	</section>
	<section class="vspace70">
		<div class="container">
		<!--<form action="javascript:void(0)" method="post" enctype="multipart/form-data" id="register_form">-->
		<?php  echo $this->Form->create('coaches',array('enctype'=>'multipart/form-data','url'=>array('controller'=>'coaches','action'=>'register'))); ?>
			<div class="reg_bg">	
				<div class="row vspace20">                   	
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/lock.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Account Information</h2>
								<!--<h5 style="color:red">All fields are mandatory.Please fill in the information.</h5>-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<?php if(isset($message)){
										echo $message;
									} ?>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								
								<input type="email" name="email_address" id="email_address" class="form-control required" placeholder="ex: steve@gmail.com">
							</div>
							
						</div>
						
						<div class="hide" style="color:red" id="email_id" >Your Email Address Already Register. </div>
                        <div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Password</label><span class="pull-right hide" id="password" style="color:red">Password Must Be Of Atleast 6 Characters</span>
								<input type="password" name="password" class="form-control required" placeholder="Minimum 6 characters">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Confirm Password</label><span class="pull-right hide" id="confirm_password" style="color:red">Password Does Not Match</span>
								<input type="password" name="confirm_password" class="form-control required" placeholder="Minimum 6 characters">
							</div>
						</div>                        
					</div>                        
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/person.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Personal Information</h2>
							</div>
						</div> 
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>First Name</label><span class="pull-right hide" id="first_name" style="color:red">Enter A Valid First Name</span>
								<input type="text" name="first_name" class="form-control required" placeholder="Enter First Name">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Last Name</label><span class="pull-right hide" id="last_name" style="color:red">Enter A Valid Last Name</span>
								<input type="text" name="last_name" class="form-control required" placeholder="Enter Last Name">
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Date of Birth</label><span class="pull-right hide" id="date" style="color:red">Invalid Date Format</span>
							
								<!--<div class='input-group date' id='datetimepicker1'>
									<input type='date' name="dob" id="dob" class="form-control required" data-date-format="YYYY-MM-DD"  placeholder="YYYY-MM-DD" autocomplete="off" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>-->
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' name="dob" id="dob" class="form-control required" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" autocomplete="off" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								
							</div>
							<?php 
								echo $this->Form->input('country',array('div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12','class'=>'form-control required','label' => 'Nationality','type'=>'select','name'=>'nationality','required'=>true,'data-placeholder'=>'Select Your Country','options'=>$country,'empty' => 'Select Nationality'));
							?>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Height</label><span class="pull-right hide" id="height" style="color:red">Height must be in digits</span>
								<input type="text" name="height" class="form-control required" placeholder="ex: 180 cm">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Weight</label><span class="pull-right hide" id="weight" style="color:red">Weight must be in digits</span>
								<input type="text" name="weight" class="form-control required" placeholder="ex: 80 kg">
							</div>
						</div>
						<div class="row">
							<?php 
								echo $this->Form->input('certificate_id',array('div'=>'col-lg-12 col-md-16 col-sm-12 col-xs-16','class'=>'form-control required','label' => 'Ceritificates','type'=>'select','name'=>'certificate_id','required'=>true,'multiple'=>true,'data-placeholder'=>'Select Your Certificates','options'=>$certificate,'style'=>array('height'=>'Auto')));
							?>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Upload Your Profile Photo</label><span class="pull-right hide" id="image" style="color:red">Upload A Valid Image</span>
								<input type="file" id="profile_img" name="profile_img" class="form-control required" accept="image/*">
							</div>
						</div>	
				
                   <?php echo "<output id='list'></output>"; ?>
				   
					</div>
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/coach.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Coaching Information</h2>
							</div>
						</div>
						<div class="row">
							
							<?php 
								echo $this->Form->input('primary',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control required',
													'label' => 'Primary Speciality',
													'type'=>'select',
													'name'=>'primary_speciality_id',
													'data-placeholder'=>'Select Your Speciality',
													'empty' => 'Select Primary Speciality',
													'options'=>$speciality
																)
															);
															
								echo $this->Form->input('secondary',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control required',
													'label' => 'Secondary Speciality',
													'type'=>'select',
													'name'=>'secondary_speciality_id',
													'data-placeholder'=>'Select Your Speciality',
													'empty' => 'Select Secondary Speciality',
													'options'=>$speciality
																)
															);
							?>
						</div>   

                        <div class="row vspace20">
						  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<input name="trams_cond" id="trams_cond" type="checkbox" class="required"> 
								I agree with the <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Terms &amp; Conditions</a> &amp; <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Privacy Policy</a> of develop a player.
							</div>
						</div>  
						
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<input type="submit" id="register_sub" value="Register" >
							</div>
						</div>                                                                         
					</div>
				</div>
				
			</div>
			</form>
		</div>
	</section>                                                   
</div>    
<script type="text/javascript">
		
	$(function() {
        $("#coachesRegisterForm").validate({
			
		rules: {
           password: {
           required: true,
            minlength: 6
        },		
        confirm_password: {
            required: true,
            minlength: 6
            // set this on the field you're trying to match
        },
		 email: {
           required: true
          
        },
       },
     
        messages: {
    
        },
        
        submitHandler: function(form) {
			//alert(succes);
			if(succes==1)
			{
              form.submit();
			}
        }
			
		});
		
	 });






//==========email exit or not==================//

$('#email_address').blur(function()
{	
	var email_address = $('#email_address').val()	
	if(email_address!="")
	{
		$.ajax({
				type: "POST",
				url: "<?php echo $this->webroot;?>coaches/email_exit_or_not",
				data:'email_address='+$('#email_address').val(),		
				success: function(data){
					//alert(data);
					if(parseInt(data)==0)
					{   $('#email_address').val('');
						$('#email_id').removeClass('hide');
						$('#email_address').focus();
						return false;
						
					}else{
						
						$('#email_id').addClass('hide');
						 return true;
						//$('#email_address').focus();
						
					}
				}
			});	
	}
	
});


//=============================================//
var succes="";

$('input[name="password"]').keyup(function(){
	if($('input[name="password"]').val().length < 6){
		$('#password').removeClass('hide');
		return false;
	}else{
		$('#password').addClass('hide');
		return true;
	}
});
$('input[name="confirm_password"]').keyup(function(){
	if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
		$('#confirm_password').removeClass('hide');
		 succes =0;
		
	}else{
		$('#confirm_password').addClass('hide');
		 succes =1;
	}
});

$('input[name="height"]').keyup(function(){
	if($.isNumeric($('input[name="height"]').val())){
		$('#height').addClass('hide');
	}else{
		$('#height').removeClass('hide');
	}
});
$('input[name="weight"]').keyup(function(){
	if($.isNumeric($('input[name="weight"]').val())){
		$('#weight').addClass('hide');
	}else{
		$('#weight').removeClass('hide');
	}
});

$('#coachesRegisterForm').submit(function(){

	var email_address = $('#email_address').val();	
	if(email_address!="")
	{
		if($('input[name="password"]').val().length < 6){
						$('#password').removeClass('hide');
						$('input[name="password"]').focus();
						return false;
					}else{
						$('#password').addClass('hide');
						}
					if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
						$('#confirm_password').removeClass('hide');
						$('input[name="confirm_password"]').focus();
						return false;
					}else{
						$('#confirm_password').addClass('hide');
						}
					if($.isNumeric($('input[name="height"]').val())){
						$('#height').addClass('hide');
					}else{
						$('#height').removeClass('hide');
						$('input[name="height"]').focus();
						return false;
						}
					if($.isNumeric($('input[name="weight"]').val())){
						$('#weight').addClass('hide');
					}else{
						$('#weight').removeClass('hide');
						$('input[name="weight"]').focus();
						return false;
						}		
		
		$.ajax({
			type: "POST",
			url: "<?php echo $this->webroot;?>coaches/email_exit_or_not",
			data:'email_address='+$('#email_address').val(),		
			success: function(data){
				//alert(data);
				if(parseInt(data)==0)
				{ 
					$('#email_address').val('');
					$('#email_id').removeClass('hide');
					$('#email_address').focus();					
					return false;					
				}else{					
					$('#email_id').addClass('hide');
					$('#register_form').attr('action','<?php echo $this->webroot?>coaches/register');
					$('#register_sub').trigger('click');
					// return true;
				}
			}
		});	
	}
});


//=========================Image Showing=========================================//		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/></div>'].join('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}
		document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);
		

/*$.validator.addMethod( 'passwordMatch', function(value, element) {    
    // The two password inputs
    var password = $("#password").val();
    var confirmPassword = $("#confirm_password").val();
    // Check for equality with the password inputs
    if (password != confirmPassword ) {
        return false;
    } else {
        return true;
    }
}, "Your Passwords Must Match");*/

</script>



<!--<form id="form">
    <input type="text" value="" name="input" id="input" />
	<input type="submit" name="submit" value="submit"/>
</form>-->
