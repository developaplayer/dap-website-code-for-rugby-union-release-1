<?php
				$last_id = 0;
                if(!empty($all_coach_details))
				{					
					foreach($all_coach_details as $all_coach_details_data)
					{
						list($width, $height, $type, $attr) = getimagesize(PHYCALPATH.'profiles/coach/profile_pic/'.$all_coach_details_data['CoachDetail']['profile_img']);
							
							$imgclass="";
							if($width>$height)
							{
								$imgclass='resizeLandscape2';						
						    }else if($width<$height)
							{
								$imgclass='resizePortrait2';
							}else
							{
							   $imgclass='resizeSqr';
							}
					?>
						<li>
							<div class="coach_img_holder_outer">
							
							<div class="coach_img_holder_innr"><div class="imageCenter"><a href="<?php echo $this->webroot;?>coaches/coach_profile/<?php echo base64_encode($all_coach_details_data['User']['id']);?>"><img class="<?php echo  $imgclass; ?>" src="<?php echo $this->webroot.'uploads/profiles/coach/profile_pic/'.$all_coach_details_data['CoachDetail']['profile_img']?>" alt=""></a></div></div>
							
							</div>
							<span class="coach_nm"><a href="javascript:void(0);"><?php echo $all_coach_details_data['User']['first_name'].' '.$all_coach_details_data['User']['last_name'];?></a></span>
							<span class="coach_subhd"><?php echo $all_coach_details_data['CoachSpecialityP']['name'];?></span>
						</li>
					  
					<?php 
					  $last_id++;
					    } 
				      }
					
					
				
				if ($last_id != 0) {
	                 echo '<script type="text/javascript">var last_id = '.$last_id.';</script>';
     }
 
// sleep for 1 second to see loader, it must be deleted in prodection
sleep(1);
?>