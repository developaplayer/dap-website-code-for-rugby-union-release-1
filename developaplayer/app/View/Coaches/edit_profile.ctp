<?php 
 $multiple_certi=explode(",",$new_arry_certi);


?>
<div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span> Coach's Registration</li>
			</ul>
		</div>
	</section>
	<section class="vspace70">
	 
		<div class="container">
			<div class="reg_bg">
				<!--<form action="" method="post" enctype="multipart/form-data" id="register_form">-->
				<?php  echo $this->Form->create('Coach',array('id'=>'edit_id','controller'=>'coaches','action'=>'edit_profile', 'enctype'=>'multipart/form-data')); ?>
				<div class="row vspace20">                   	
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/lock.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Account Information</h2>
								<h5 style="color:red">All fields are mandatory.Please fill in the information.</h5>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div style="color:green">
							<?php 
								if ($this->Session->check('Message.flash')) {
									echo $this->Session->flash();
								}
								if ($this->Session->check('Message.auth')) {
									//echo $this->Session->flash('auth');
								}
		                        ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Email Id</label><span class="pull-right hide" id="email_id" style="color:red">Email Id Already Exist</span>
								<input readonly="readonly" type="email" name="email_address" class="form-control" value="<?php echo (isset($get_edit_data['User']['email_address']))  ? $get_edit_data['User']['email_address'] : "";  ?>" placeholder="ex: steve@gmail.com" required>
							</div>
						</div>
						                   
					</div>                        
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/person.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Personal Information</h2>
							</div>
						</div> 
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>First Name</label><span class="pull-right hide" id="first_name" style="color:red">Enter A Valid First Name</span>
								<input type="text" name="first_name" value="<?php echo (isset($get_edit_data['User']['first_name']))  ? $get_edit_data['User']['first_name'] : "";  ?>" class="form-control" required>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Last Name</label><span class="pull-right hide" id="last_name"  style="color:red">Enter A Valid Last Name</span>
								<input type="text" name="last_name" class="form-control" value="<?php echo (isset($get_edit_data['User']['last_name']))  ? $get_edit_data['User']['last_name'] : "";  ?>" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Date of Birth</label><span class="pull-right hide" id="date" style="color:red">Invalid Date Format</span>
								<!--<input type="text" name="dob" class="form-control" placeholder="dd/mm/yyyy" required>-->
								
								<div class='input-group date' id='datetimepicker1'>
									<input type='date' name="dob" class="form-control" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo (isset($get_edit_data['CoachDetail']['dob']))  ? $get_edit_data['CoachDetail']['dob'] : "";  ?>" required />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								
							</div>
							<?php 
								echo $this->Form->input('country',array('div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12','class'=>'form-control','label' => 'Nationality','type'=>'select','name'=>'nationality','required'=>true,'data-placeholder'=>'Select Your Country','options'=>$country,'empty' => 'Select Nationality','selected'=>isset($get_edit_data['CoachDetail']['nationality'])  ? $get_edit_data['CoachDetail']['nationality'] : ""));
							?>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Height</label><span class="pull-right hide" id="height" style="color:red">Height must be in digits</span>
								<input type="text" name="height" class="form-control" value="<?php echo (isset($get_edit_data['CoachDetail']['height']))  ? $get_edit_data['CoachDetail']['height'] : "";  ?>" required>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Weight</label><span class="pull-right hide" id="weight" style="color:red">Weight must be in digits</span>
								<input type="text" name="weight" class="form-control" value="<?php echo (isset($get_edit_data['CoachDetail']['weight']))  ? $get_edit_data['CoachDetail']['weight'] : "";  ?>" required>
							</div>
						</div>
						<div class="row">
							<?php 
								echo $this->Form->input('certificate_id',array('div'=>'col-lg-12 col-md-16 col-sm-12 col-xs-16','class'=>'form-control','label' => 'Ceritificates','type'=>'select','name'=>'certificate_id','required'=>true,'multiple'=>true,'data-placeholder'=>'Select Your Certificates','options'=>$certificate,'selected' =>$multiple_certi,'style'=>array('height'=>'Auto')));
							?>
						</div>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Upload Your Profile Photo</label><span class="pull-right hide" id="image" style="color:red">Upload A Valid Image</span>
								<input type="file" id="profile_img" name="profile_img" class="form-control"  accept="image/*">
							</div>
						</div>	
				
                   <output  id='list'><span><img src="<?php echo $this->webroot;?>uploads/profiles/coach/thumb/<?php echo isset($get_edit_data['CoachDetail']['profile_img'])  ? $get_edit_data['CoachDetail']['profile_img'] : "" ?>"/></span></output>
				  
					</div>
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/coach.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Coaching Information</h2>
							</div>
						</div>
						<div class="row">
							<!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Primary Speciality</label>
								<select class="form-control">
									<option selected>--Select--</option>
								</select>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Secondary Speciality</label>
								<select class="form-control">
									<option selected>--Select--</option>
								</select>
							</div>-->
							<?php 
								echo $this->Form->input('primary',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'Primary Speciality',
													'type'=>'select',
													'name'=>'primary_speciality_id',
													'required'=>true,
													'data-placeholder'=>'Select Your Speciality',
													'empty' => 'Select Primary Speciality',
													'options'=>$speciality,
													'selected'=>isset($get_edit_data['CoachDetail']['primary_speciality_id'])  ? $get_edit_data['CoachDetail']['primary_speciality_id'] : ""
																)
															);
															
								echo $this->Form->input('secondary',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'Secondary Speciality',
													'type'=>'select',
													'name'=>'secondary_speciality_id',
													'required'=>true,
													'data-placeholder'=>'Select Your Speciality',
													'empty' => 'Select Secondary Speciality',
													'options'=>$speciality,
													'selected'=>isset($get_edit_data['CoachDetail']['secondary_speciality_id'])  ? $get_edit_data['CoachDetail']['secondary_speciality_id'] : ""
																)
															);
							?>
						</div>   
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Coach's Note</label>
								<textarea name="note" rows="10" cols="106" placeholder="Note"><?php echo (isset($get_edit_data['CoachDetail']['note']))  ? $get_edit_data['CoachDetail']['note'] : "";  ?></textarea>
							</div>
						</div>
						
						<div class="row vspace10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<input type="submit" value="Save">
							</div>
						</div>                                                                         
					</div>
				</div>
				 <?php echo $this->Form->end(); ?>
			</div>
		</div>
	</section>                                                   
</div>    
<script>
$(document).ready(function() 
		{
			$('#edit_id').validate();			
           
		});
		
$('input[name="password"]').keyup(function(){
	if($('input[name="password"]').val().length < 6){
		$('#password').removeClass('hide');
	}else{
		$('#password').addClass('hide');
	}
});
$('input[name="confirm_password"]').keyup(function(){
	if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
		$('#confirm_password').removeClass('hide');
	}else{
		$('#confirm_password').addClass('hide');
	}
});
$('input[name="height"]').keyup(function(){
	if($.isNumeric($('input[name="height"]').val())){
		$('#height').addClass('hide');
	}else{
		$('#height').removeClass('hide');
	}
});
$('input[name="weight"]').keyup(function(){
	if($.isNumeric($('input[name="weight"]').val())){
		$('#weight').addClass('hide');
	}else{
		$('#weight').removeClass('hide');
	}
});
$('#register_form').submit(function(){
	if($('input[name="password"]').val().length < 6){
		$('#password').removeClass('hide');
		$('input[name="password"]').focus();
		return false;
	}else{
		$('#password').addClass('hide');
	}
	if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
		$('#confirm_password').removeClass('hide');
		$('input[name="confirm_password"]').focus();
		return false;
	}else{
		$('#confirm_password').addClass('hide');
	}
	if($.isNumeric($('input[name="height"]').val())){
		$('#height').addClass('hide');
	}else{
		$('#height').removeClass('hide');
		$('input[name="height"]').focus();
		return false;
	}
	if($.isNumeric($('input[name="weight"]').val())){
		$('#weight').addClass('hide');
	}else{
		$('#weight').removeClass('hide');
		$('input[name="weight"]').focus();
		return false;
	}
});


//=========================Image Showing=========================================//		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<img class="hide_shown_image" style="height: 160px; width:130px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/>'].join('');
					$("#list").html('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);

document.getElementById("uploadBtn").onchange = function () {
    document.getElementById("uploadFile").value = this.value;
};
</script>