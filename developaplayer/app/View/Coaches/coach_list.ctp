<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#country-list{float:left;list-style:none;margin:0;padding:0;width:100px;}
#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#ccc 1px solid}
#country-list li:hover{background:#F0F0F0;}
</style>
<script type="text/javascript">
$(document).ready(function(){        

        var is_loading = false;
 	var limit = 25;
	var offset = 0;
        var total = <?php echo $coach_total ?>;  
       
        var search_box_val =$('#search-box').val();
        var select_box_val =$("#select_value_speciality").val();
        $('#btn_id').hide();
        
        if(total>=25){
	$('#btn_id').show();
	}
        
	$("#search-box").keyup(function(){		
		if($(this).val()!="")
		{
		  $.ajax({
		type: "POST",
		url: "<?php echo $this->webroot;?>coaches/auto_complate_name",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
		}
	});
	
	
	$('body').click(function(e) {
		
    if (!$(e.target).closest('#suggesstion-box').length){
        $("#suggesstion-box").fadeOut('slow');
    }
});


      	 /*=============== button click ajax call ================*/
	
		$("#btn_id").click(function(){		
		
		 offset = limit + offset;  
			
			$.ajax({
				url:'<?php echo $this->webroot;?>coaches/coach_list_ajax',
				type: 'post',
				data: {last_id:offset, limit:limit,search_box:search_box_val,select_box_val:select_box_val},
				success:function(result){
					$("#items").append(result);
					var success = $($.parseHTML(result)).filter("li").length;
					
					if(success<limit){
					$('#btn_id').hide();
				        }
				}
			});		
		});



});

	function selectCountry(val)
	{	
		$("#search-box").val(val);
		$("#suggesstion-box").hide();		
		click_auto_complt(val);		
	}

  
  
 
 

$(function() {

     $( "#speciality_id").val(<?php echo $select_value_speciality ?>);
     $( "#speciality_id" ).selectmenu({
		 
         change: function( event, ui ) {        
             var selected_value = ui.item.value;
			$("#select_value_speciality").val(selected_value);
            $('#search_from_speciality').submit();
         }
     });
});
	
      
	
	function click_auto_complt(val)
	{
			var is_loading = false;
			var limit = 25
			var offset = 0;
			$('#loader').hide();
		
		
			var search_box_val =val;
			var select_box_val =$("#select_value_speciality").val();
            
                if (is_loading == false) { // stop loading many times for the same page
                    // set is_loading to true to refuse new loading
					offset = limit + offset;
					
					//alert(offset);
                    is_loading = true;
                    // display the waiting loader
                    $('#loader').show();
                    // execute an ajax query to load more statments
                    $.ajax({
                        url: '<?php echo $this->webroot;?>coaches/coach_list_auto_complate',
                        type: 'POST',
                        data: {last_id:offset, limit:limit,search_box:search_box_val,select_box_val:select_box_val},
                        success:function(data){
							
                            $('#loader').hide();
                            // append: add the new statements to the existing data
                            $('#items').html(data);
                            // set is_loading to false to accept new loading
                            is_loading = false;
                        }
                    });
                }
           
        
	}
	
</script>
<div class="main_container">
    <section class="innrpg_white">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Coaches</li>
                </ul>
            </div>
		</section>
        <section class="search_filter_holder center">
            <div class="container vspace30">
            	<div class="search_holder">
                <form name="search_from" id="search_from" action="<?php echo $this->webroot ?>coaches/coach_list" method="post">
             		<input type="text" name="search_box" id="search-box" placeholder="Search By Name" autocomplete="off" value="<?php if(isset($search_text)) echo $search_text;?>">
					<div id="suggesstion-box"></div>
                	<input type="submit" value="submit">
                 </form>
                </div>
				
				
                <div class="filter_holder">
				<form name="search_from_speciality" id="search_from_speciality" action="<?php echo $this->webroot ?>coaches/coach_list" method="post">
                	<select id="speciality_id">
                    	<option selected>Filter By Speciality</option>
						<?php foreach($all_coachspeciality as $all_coachspecialitys){?>
							<option value="<?php echo $all_coachspecialitys['CoachSpeciality']['id']?>"><?php echo $all_coachspecialitys['CoachSpeciality']['name']?></option>
							
					<?php } ?>
						
                    </select>
					<input type="hidden" name="select_value_speciality" id="select_value_speciality" value="<?php echo $select_value_speciality ?>"/>
				</form>
                </div>
			
            </div>
		</section> 
        <section class="coach_listing center">
            <div class="container vspace30">
            	<ul id="items">				
				<?php
				$last_id = 0;
                if(!empty($all_coach_details))
				{					
					foreach($all_coach_details as $all_coach_details_data)
					{
						
					?>
						<li>
							<div class="coach_img_holder_outer">
							
							<?php

							list($width, $height, $type, $attr) = getimagesize(PHYCALPATH.'profiles/coach/profile_pic/'.$all_coach_details_data['CoachDetail']['profile_img']);
							
							$imgclass="";
							if($width>$height)
							{
								$imgclass='resizeLandscape2';						
						    }else if($width<$height)
							{
								$imgclass='resizePortrait2';
							}else
							{
							   $imgclass='resizeSqr';
							}
							

?>
							
							<div class="coach_img_holder_innr">
                            	<div class="imageCenter">
                            		<a href="<?php echo $this->webroot;?>coaches/coach_profile/<?php echo base64_encode($all_coach_details_data['User']['id']);?>"><img class="<?php echo $imgclass; ?>" style="" src="<?php echo $this->webroot.'uploads/profiles/coach/profile_pic/'.$all_coach_details_data['CoachDetail']['profile_img']?>" alt=""></a>
                          		</div>
                            </div>
                            
							
							</div>
							<span class="coach_nm"><a href="javascript:void(0);"><?php echo $all_coach_details_data['User']['first_name'].' '.$all_coach_details_data['User']['last_name'];?></a></span>
							<span class="coach_subhd"><?php echo $all_coach_details_data['CoachSpecialityP']['name'];?></span>
						</li>
					  
					<?php 
					
					 $last_id++;
					    } 
				      }else{?>						  
						<li>						
							<div style="padding-left:392px;width:634px"><strong>No More Record Found!</strong></div>		
						</li>  
					 <?php }
					
					?>
 </ul>
	<button id="btn_id" class="pull-right">View more <i>
				    	<img title="icon_view1" alt="icon_view1" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAANCAYAAACpUE5eAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEQ1NDQ0RDAxQkZFMTFFNkFGODFCQ0ZGMDcxNUZEQzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEQ1NDQ0RDExQkZFMTFFNkFGODFCQ0ZGMDcxNUZEQzEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4RDU0NDRDRTFCRkUxMUU2QUY4MUJDRkYwNzE1RkRDMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4RDU0NDRDRjFCRkUxMUU2QUY4MUJDRkYwNzE1RkRDMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnNl17sAAAEWSURBVHjaYvz//z8DDuAHxNFAbADEEkDMCMQvgfgSEK8E4lVYdYEMRMMuQPzkP2HwGoj90fWjGzYRi8bdQJwOxKlAvAWL/HxcBq7EotgNiw8sgfgHmrqt6AZOwWJYGlQuCOq9D0AcDxWLxKJ+LszAeCySH6AaNbDIWUHlHmGRy2ACxksJlrh6DqUdscjZQOmnWORyQQZuwiIhDqWPYpE7AaUlscgthYXhaSzOT4DKxQDxNyD+CY1tkFgwFvWrkSNFChrwyOAvENtiiWVDIP6CpvYItmQDMvQeFps3AXE0EEdAXYEO1uNL2IxA3P+fOAByZRy6Dxhx5GUxIE4BYlcgVgJiQaj4GyC+CcSrgXgxEP9G1wgQYAAkN5esa57ppgAAAABJRU5ErkJggg==">
				    </i></button>
				 
            </div>
			<div id="loader" style="display:none"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>
		</section>                                                            
	</section>    
    </div>    
