	<div class="main_container login-reg-pg">
		<section class="breadcrumb vspace20">
			<div class="container">
				<ul>
					<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
					<li><span>&raquo;</span> Family Registration</li>
				</ul>
			</div>
		</section>
		<section class="vspace70">
			<div class="container">
			<?php  echo $this->Form->create('families',array('id'=>'register_form','controller'=>'families','action'=>'register', 'enctype'=>'multipart/form-data')); ?>
				<div class="reg_bg family">
					<!--<form action="" method="post" enctype="multipart/form-data" id="register_form">-->
					
					<div class="row vspace20">                   	
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/lock.png" alt=""></span>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h2>Account Information</h2>
									<h5 style="color:red">All fields are mandatory.Please fill in the information.</h5>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php 
									if ($this->Session->check('Message.flash')) {
										echo $this->Session->flash();
									}
									if ($this->Session->check('Message.auth')) {
										//echo $this->Session->flash('auth');
									}
									?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>Email Id</label><span class="pull-right hide" id="email_id" style="color:red">Email Id Already Exist</span>
									<input type="email" name="email_address" id="email_address" class="form-control required" placeholder="ex: steve@gmail.com" required>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>Password</label><span class="pull-right hide" id="password" style="color:red">Password Must Be Of Atleast 6 Characters</span>
									<input type="password" name="password" class="form-control" placeholder="Minimum 6 characters" required>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>Confirm Password</label><span class="pull-right hide" id="confirm_password" style="color:red">Password Does Not Match</span>
									<input type="password" name="confirm_password" class="form-control" placeholder="Minimum 6 characters" required>
								</div>
							</div>                        
						</div>                        
					</div>
					<div class="row vspace20">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/person.png" alt=""></span>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h2>Personal Information</h2>
								</div>
							</div> 
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>First Name</label><span class="pull-right hide" id="first_name" style="color:red">Enter A Valid First Name</span>
									<input type="text" name="first_name" class="form-control" required>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>Last Name</label><span class="pull-right hide" id="last_name" style="color:red">Enter A Valid Last Name</span>
									<input type="text" name="last_name" class="form-control" required>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>Date of Birth</label><span class="pull-right hide" id="date" style="color:red">Invalid Date Format</span>
									<!--<input type="text" name="dob" class="form-control" placeholder="dd/mm/yyyy" required>-->
									
									<div class='input-group date' id='datetimepicker1'>
										<input type='date' name="dob" class="form-control" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" required />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
									
								</div>
								<?php 
									echo $this->Form->input('country',array('div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12','class'=>'form-control','label' => 'Nationality','type'=>'select','name'=>'nationality','required'=>true,'data-placeholder'=>'Select Your Country','options'=>$country,'empty' => 'Select Nationality'));
								?>
							</div>
							
							
							
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<label>Upload Your Profile Photo</label>
									<input type="file" id="profile_img" required name="profile_img" class="form-control" accept="image/*">
									<span class="pull-left hide" id="image" style="color:red">Upload a Image</span>
								</div>
							</div>	
					   <?php echo "<output id='list'></output>"; ?>				   
						</div>
					</div>
					<div class="row vspace20">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/coach.png" alt=""></span>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<h2>Relationship with player</h2>
								</div>
							</div>
							<div class="row" id="row_relation_main">
								<div id="row_relation">							
								<?php 
									echo $this->Form->input('player_id',array(
														'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
														'class'=>'form-control',
														'label' => 'Choose Player Name',
														'type'=>'select',
														'name'=>'player_id[]',
														'required'=>true,
														'onChange'=>'player_id_check(this)',
														'data-placeholder'=>'Select Your Player',
														'empty' => 'Select Your Player',
														'options'=>$player_list
																	)
																);
													?>	
																						
										<?php						
									echo $this->Form->input('relation_id',array(
														'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
														'class'=>'form-control',
														'label' => 'My Relation with the player',
														'type'=>'select',
														'name'=>'relation_id[]',
														'onChange'=>'relation_check(this)',
														'required'=>true,
														'data-placeholder'=>'Select Your Relationship',
														'empty' => 'Select Your Relationship',
														'options'=>$relation_list
																	)
																);
								?>
								
								</div> 
	<span class="hide" id="last_span"></span>							
							</div>						
							<span class="addMoreBtn" id="p_scents">
							   <a href="javascript:void(0);"><span class="glyphicon glyphicon-plus-sign"></span> Add More</a>
							</span> 						
							<div class="row vspace20">

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<input type="checkbox" id="chk" name="chk" onclick="chk_test();"> 
									I agree with the <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Terms &amp; Conditions</a> &amp; <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Privacy Policy</a> of develop a player.
							    <span class="pull-right hide" id="term" style="color:red">Please accept the terms and conditions</span>

								</div>

							</div>  
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<input type="submit" value="Register" onclick="return click_submit()">
								</div>
							</div>                                                                         
						</div>
					</div>
					 <?php echo $this->Form->end(); ?>
				</div>
			</div>
		</section>                                                   
	</div>    
	<script>
	
	$('input[name="password"]').keyup(function(){
		if($('input[name="password"]').val().length < 6){
			$('#password').removeClass('hide');
		}else{
			$('#password').addClass('hide');
		}
	});
	$('input[name="confirm_password"]').keyup(function(){
		if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
			$('#confirm_password').removeClass('hide');
		}else{
			$('#confirm_password').addClass('hide');
		}
	});

	/*
	$('#register_form').submit(function(){
		
		var email=$('#email_address').val();

		if(email!="")
		{				
			$.ajax({
				type: "POST",
				url: "<?php echo $this->webroot;?>families/email_exist",
				data:'email_address='+ email,
				async: false,			
				success: function(data){						
				if(data==1)
				{
					$('#email_id').removeClass('hide');
					return false;
				}else
				{
					
					$('#email_id').addClass('hide');
					return true;
				}
				
			   }
			});
		}

		if($('input[name="password"]').val().length < 6){
			$('#password').removeClass('hide');
			$('input[name="password"]').focus();
			return false;
		}else{
			$('#password').addClass('hide');
		}
		if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
			$('#confirm_password').removeClass('hide');
			$('input[name="confirm_password"]').focus();
			return false;
		}else{
			$('#confirm_password').addClass('hide');
		}
		var refreshId = setInterval(function() { }, 25000);
	});*/

		//============================================================================================================
		$("#register_form").validate({
			rules: {                   				
			add_quinty:{
				number: true
				}
			},                             
			messages: {
				password :" Enter Password",
				conf_password :" Enter Confirm Password Same as Password",
				mobile :" Enter A Vaild Number",
				unit :" Enter A Vaild Unit",
				price :" Enter A Vaild Price",
				add_quinty :" Enter A Vaild Quantity",
				pincode :" Enter A Vaild Pincode"
			}
		});
		function click_submit()
		{
			
			var email=$('#email_address').val();
			var success_id='';
			if(email!="")
			{				
				$.ajax({
					type: "POST",
					url: "<?php echo $this->webroot;?>families/email_exist",
					data:'email_address='+ email,
					async: false,			
					success: function(data){						
					if(data==1)
					{
						$('#email_id').removeClass('hide');
						//return false;
						success_id=2;
					}else
					{
						
						$('#email_id').addClass('hide');
						//return true;
						success_id=1;
					}
					
				   }
				});
			}

			if($('#chk').is(':checked')){
			
				$('#term').removeClass('hide');		
				$('input[name="checkbox"]').focus();
				return false;
			}
			else
		    {
			   alert("Please accept the terms and conditions");
			   $('#term').addClass('hide');
			}
			
			if($('input[name="password"]').val().length < 6){
				$('#password').removeClass('hide');
				$('input[name="password"]').focus();
				//success_id=2;
				return false;
			}else{
				$('#password').addClass('hide');
				//success_id=1;
			}
			
			//image upload restrict jquery
			if($('#profile_img').val()==""){
			$('#image').removeClass('hide');		
			$('#profile_img').focus();
			return false;
			}
			else
			{
				$('#image').addClass('hide');
			}
			
			

			//terms and condition restrict jquery
			//if($('#chk').attr('checked', true)){
			//$('#term').removeClass('hide');		
			//$('#chk').focus();
			//return false;
			//}
			//else
			//{
			//	$('#term').addClass('hide');
			//}
			
			if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
				$('#confirm_password').removeClass('hide');
				$('input[name="confirm_password"]').focus();
				//success_id=2;
				return false;
			}else{
				$('#confirm_password').addClass('hide');
				//success_id=1;
			}
			
			setTimeout(function(){ 
				if(success_id==1)
				{	
					//alert(success_id);
					$('#register_form').submit();
				}
				else
				{
					//alert(success_id);
					return false;
				}
			}, 2000);
		}


	//=========================Image Showing=========================================//		
			function handleFileSelect(evt) {
				var files = evt.target.files;
				$('.hide_shown_image').hide();

				// Loop through the FileList and render image files as thumbnails.
				for (var i = 0, f; f = files[i]; i++) {

					// Only process image files.
					if (!f.type.match('image.*')) {
						continue;
					}
					
					var reader = new FileReader();

					// Closure to capture the file information.
					reader.onload = (function(theFile) {
					return function(e) {
						// Render thumbnail.
						var span = document.createElement('span');
						span.innerHTML = 
						[
						'<div style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
						e.target.result,
						'" title="', escape(theFile.name), 
						'"/></div>'].join('');
						document.getElementById('list').insertBefore(span, null);
						};
					})(f);

					// Read in the image file as a data URL.
					reader.readAsDataURL(f);
				}
			}

			document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);

	 
	$(document).ready(function(){
		$("#email_address").blur(function(){
		
			if($('#email_address').val()!="")
			{
			  $.ajax({
			type: "POST",
			url: "<?php echo $this->webroot;?>families/email_exist",
			data:'email_address='+$('#email_address').val(),		
			success: function(data){
				if(data==1)
				{
					$('#email_id').removeClass('hide');
					return false;
				}else
				{
					
					$('#email_id').addClass('hide');
					return true;
				}
				
			}
			});
			}
		});
	});
	 var clicks = 0;
	$(function() 
	{	
		  var relation_html =$('#row_relation').html();
			$('#p_scents').click(function() {
				clicks += 1;
				if(clicks<=2)
				{
				 $('#last_span').before('<div id="remove_'+clicks+'">'+relation_html+'<div class="removeIcon" onclick="remove_relation_div('+clicks+')"><a style="cursor:pointer;float:right;"><span class="glyphicon glyphicon-remove-circle"></span></a></div></div>'); 
				}
				
				if(clicks==2)
				{				
					 $('#p_scents').hide();				
				}

			});
		
			
	});



	var removeclicks=0;
	function remove_relation_div(Id)
	{
		clicks=clicks-1;
		//alert(clicks);
		removeclicks += 1;
		$('#remove_'+Id).remove('');
		 $('#p_scents').show();
		
	}

	function player_id_check(Obj)
	{
		var current_id =Obj.value;
		Obj.setAttribute('name','');
		
		$('select[name="player_id[]"]').each(function(){
		var each_id=this.value;
			if(each_id==current_id)
			{			
				alert('You already selected this player. please change');		
				$(Obj).val('');						
			}
		
	  });
	  Obj.setAttribute('name','player_id[]');
		
		
	}

	//=================With Relatioship=============================//

	function relation_check(Obj)
	{
		var current_id =Obj.value;
		//Obj.setAttribute('name','');	
		//==============get Palyer id==================//	
		 var each_player_id="";
		$('select[name="player_id[]"]').each(function(){
		  return each_player_id=this.value;
		 
		});
		
		$('select[name="player_id[]"]').each(function(){
		  return player_name=this.text;
		 
		});
		
		//=====================End======================//
		
		//======================Get Relation=============//
		
		$('select[name="relation_id[]"]').each(function(){
			relation_id=this.value;			
		   return relation_id=this.value;
		   
		});
		
		$('select[name="relation_id[]"]').each(function(){
			relation_id=this.value;			
		   /// return relation_text=$(this).text();
		   
		});
		
		if(each_player_id=="")
		{
			
			//alert('Please Select Player');
			//$('select[name="relation_id[]"]').val('');
			//$(Obj).val('');		
		}
		
		if(each_player_id!="" && relation_id!="")
		{
		  $.ajax({
				type: "POST",
				url: "<?php echo $this->webroot;?>families/relation_ship_exit_or_not",		
				data: 'player_id='+each_player_id+'&relation_id='+relation_id,
				success: function(data)
				{			
				 if(data>0)
				 {
					//alert('Selected Relation With Palyer Aleready Exits,Please Select other.');
					//$('select[name="relation_id[]"]').val('');
					 
				 }				 
				}
			});
			
		}	
		
		 //alert(each_player_id+''+relation_id);
		
		// Obj.setAttribute('name','relation_id[]');
	  
	  //============================End====================//
	}
	
	

	</script>