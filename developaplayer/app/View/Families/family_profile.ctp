<?php //pr( $family_player_relation); exit; ?>
 <div class="main_container">
    <section class="innrpg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Family Member Profile</a></li>
                    <li><span>&raquo;</span> &nbsp;<?php echo $all_family_data['User']['first_name'].' '.$all_family_data['User']['last_name'];?></li>
                </ul>
            </div>
		</section>
        <section>
            <div class="container vspace30">
            	<div class="profile_top_holder player_profile">	
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile_top_holder_lft">
                        <div class="coach_profile_img"><img src="<?php echo $this->webroot.'uploads/profiles/family/profile_pic/'.$all_relation_family_data['FamilyDetail']['profile_img']?>"></div>                
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 profile_top_holder_rght">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2 class="coach_heading"><?php echo $all_family_data['User']['first_name'].' '.$all_family_data['User']['last_name'];?></h2>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="short_desc_holder">
                                    Nationality:  <?php echo $all_relation_family_data['Nationality']['country_name'];?>
                              	</div>								 
                            </div>
                            <span  class="edit_profile_btn"><a href="<?php echo $this->webroot;?>families/edit_profile">Edit</a></span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coach_details_holder">
						
						 <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Related Player</div>								
                                <div class="col-lg-7 col-md-7 col-sm-5 col-xs-5 coach_details_row_lft">Relation with the player</div>							 
                          </div>
						
						  <div class="row coach_details_row">
						 <?php
							 if(!empty($family_player_relation))
							  {
							   foreach((array)$family_player_relation as $family_player_relations)
								{
							    ?>  
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft"><a href="<?php echo $this->webroot?>players/player_profile/<?php echo base64_encode($family_player_relations['FamilyPlayerRelationship']['player_id']);?>"><?php echo $family_player_relations['User']['first_name'].' '.$family_player_relations['User']['last_name']; ?></a></div>								
                                <div class="col-lg-7 col-md-7 col-sm-5 col-xs-5 coach_details_row_lft"><?php echo $family_player_relations['PlayerRelationship']['relationship'];?></div>
								
							  <?php } } ?>
								
                          </div>
						      
						
                        </div>
                    </div>
            	</div>
            </div>
		</section>                                                 
	</section>
                
    <section>
        <div class="container">
        	<div class="profile_bottom_holder">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vspace30 profile_bottom_lft">
                    <h2 class="innrpg_subhd">Transaction History</h2>
                    <div class="transaction_tbl">
                    	<div class="transaction_tbl_row">
                        	<div class="transaction_tbl_col">Date</div>
                            <div class="transaction_tbl_col">Description</div>
                            <div class="transaction_tbl_col">Amount</div>
                        </div>
					</div>
					
                    <div class="transaction_tbl_outer">
					<?php if(!empty($player_top_up))
							  { ?>
					<?php foreach($player_top_up as $player){?>
                        <div class="transaction_tbl">                                                
                            <div class="transaction_tbl_row">
                                <div class="transaction_tbl_col"><?php 
								$date = $player['PlayerTopup']['payment_date'];
								echo date("jS F, Y", strtotime($date));								
								//echo date($player['PlayerTopup']['payment_date']);?></div>
                                <div class="transaction_tbl_col"><?php echo $player['PlayerTopup']['description_topup']?></div>
                                <div class="transaction_tbl_col"><?php echo $player['PlayerTopup']['amount']?></div>
                            </div>
                        </div>
							  <?php } } else {?>
								  	<p class="notFound">No Transaction Found !</p>
							 <?php }?>
                      
                    </div>
                </div>
                <!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vspace30 profile_bottom_right">
                    <h2 class="innrpg_subhd">Notes</h2>
                    <div class="notes_row vspace10">
                    	<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                        	<div class="coach_img"><img src="<?php echo $this->webroot; ?>images/family_member.jpg" alt=""></div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9">
                        	<div class="relation_to_player">Mom <span class="dt">31.07.2015</span></div>
                            <div class="comment">Great! Kid is developing well. Now need to work on developing sidestep off left foot.</div>
                        </div>
                    </div>
                    <div class="notes_row vspace10">
                    	<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                        	<div class="coach_img"><img src="<?php echo $this->webroot; ?>images/family_member.jpg" alt=""></div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9">
                        	<div class="relation_to_player">Mom <span class="dt">31.07.2015</span></div>
                            <div class="comment">Great! Kid is developing well. Now need to work on developing sidestep off left foot.</div>
                        </div>
                    </div>
                    <h2 class="innrpg_subhd2">Add a note</h2>
                    <form>
                    	<div class="notes_row vspace10">
                            <textarea></textarea>
                        </div>
                        <div class="notes_row vspace10">
                            <input type="submit" value="Submit">
                        </div>
                    </form>                  
                </div>-->
        	</div>
        </div>
    </section>
    </div>