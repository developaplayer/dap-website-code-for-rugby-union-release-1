 <div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span> Family Member ForgotPassword</li>
			</ul>
		</div>
	</section> 
  <section class="vspace70 center">
  <div style="color:green"> <?php 
								if ($this->Session->check('Message.flash')) {
									echo $this->Session->flash();
								}
								if ($this->Session->check('Message.auth')) {
									//echo $this->Session->flash('auth');
								}
		                        ?>
								</div>
							
            <div class="container">
            	<div class="login_bg">
                	<div class="login_bg_innr">
                    	<div class="profile_ico_holder"><img src="<?php echo $this->webroot; ?>images/ico_profile.png" alt=""></div>                    
                    
						  <?php  echo $this->Form->create('families',array('id'=>'sign_up_id','controller'=>'families','action'=>'forgot_password')); ?>
                            <div class="form-group email-input">
                                <label>Please Enter Your Register  Email Id</label>
                              
								<?php echo $this->Form->input('User.email_address', array('type' => 'email','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                          <div class="form-group">                                
                                <span class="pull-right"><input type="submit" value="Submit"></span>
                            </div>
                       <?php echo $this->Form->end(); ?>
                    </div>
                </div>   
                
            </div>
		</section>   

</div>		