<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#country-list{float:left;list-style:none;margin:0;padding:0;width:100px;}
#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}
#country-list li:hover{background:#F0F0F0;}
#search-box{padding: 15px;border: #F0F0F0 1px solid;}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#search-box").keyup(function(){
	<?php echo $this->webroot;?>	
		if($(this).val()!="")
		{
		  $.ajax({
		type: "POST",
		url: "<?php echo $this->webroot;?>coaches/auto_complate_name",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
		}
		});
		}
	});
});

	function selectCountry(val)
	{	
		$("#search-box").val(val);
		$("#suggesstion-box").hide();
	}

  
  
  
 

$(function() {

     $( "#speciality_id").val(<?php echo $select_value_speciality ?>);
     $( "#speciality_id" ).selectmenu({
		 
         change: function( event, ui ) {        
             var selected_value = ui.item.value;
			$("#select_value_speciality").val(selected_value);
            $('#search_from_speciality').submit();
         }
     });
});
	
    var is_loading = false;
	var limit = 4
	var offset = 1;
	 $('#loader').hide();
    // initialize is_loading by false to accept new loading
    //svar limit = 4; // limit items per page
    $(function() {
        $(window).scroll(function() {
			var search_box_val =$('#search-box').val();
			var select_box_val =$("#select_value_speciality").val();
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if (is_loading == false) { // stop loading many times for the same page
                    // set is_loading to true to refuse new loading
					offset = limit + offset;
					
					//alert(offset);
                    is_loading = true;
                    // display the waiting loader
                    $('#loader').show();
                    // execute an ajax query to load more statments
                    $.ajax({
                        url: '<?php echo $this->webroot;?>coaches/coach_list_ajax',
                        type: 'POST',
                        data: {last_id:offset, limit:limit,search_box:search_box_val,select_box_val:select_box_val},
                        success:function(data){
							//alert(data);
                            // now we have the response, so hide the loader
                            $('#loader').hide();
                            // append: add the new statments to the existing data
                            $('#items').append(data);
                            // set is_loading to false to accept new loading
                            is_loading = false;
                        }
                    });
                }
           }
        });
    });
</script>
<div class="main_container">
    <section class="innrpg_white">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Coaches</li>
                </ul>
            </div>
		</section>
        <section class="search_filter_holder center">
            <div class="container vspace30">
            	<div class="search_holder">
                <form name="search_from" id="search_from" action="<?php echo $this->webroot ?>coaches/coach_list" method="post">
             		<input type="text" name="search_box" id="search-box" placeholder="Search By Name" value="<?php if(isset($search_text)) echo $search_text;?>">
					<div id="suggesstion-box"></div>
                	<input type="submit" value="submit">
                 </form>
                </div>
				
				
                <div class="filter_holder">
				<form name="search_from_speciality" id="search_from_speciality" action="<?php echo $this->webroot ?>coaches/coach_list" method="post">
                	<select id="speciality_id" onchange="search_coach_by_specility()">
                    	<option selected>Filter By Speciality</option>
						<?php foreach($all_coachspeciality as $all_coachspecialitys){?>
							<option value="<?php echo $all_coachspecialitys['CoachSpeciality']['id']?>"><?php echo $all_coachspecialitys['CoachSpeciality']['name']?></option>
							
					<?php } ?>
						
                    </select>
					<input type="hidden" name="select_value_speciality" id="select_value_speciality" value="<?php echo $select_value_speciality ?>"/>
				</form>
                </div>
			
            </div>
		</section> 
        <section class="coach_listing center">
            <div class="container vspace30">
            	<ul id="items">				
				<?php
				$last_id = 0;
                if(!empty($all_coach_details))
				{					
					foreach($all_coach_details as $all_coach_details_data)
					{
						//$last_id=$all_coach_details_data['CoachDetail']['id'];
					?>
						<li>
							<div class="coach_img_holder_outer">
							
							<div class="coach_img_holder_innr"><a href="<?php echo $this->webroot;?>coaches/coach_profile/<?php echo base64_encode($all_coach_details_data['User']['id']);?>"><img src="<?php echo $this->webroot.'uploads/profiles/coach/thumb/'.$all_coach_details_data['CoachDetail']['profile_img']?>" alt=""></a></div>
							
							</div>
							<span class="coach_nm"><a href="javascript:void(0);"><?php echo $all_coach_details_data['User']['first_name'].' '.$all_coach_details_data['User']['last_name'];?></a></span>
							<span class="coach_subhd"><?php echo $all_coach_details_data['CoachSpecialityP']['name'];?></span>
						</li>
					  
					<?php 
					
					   $last_id++;
					    } 
				      }else{?>
						  
						  <li>
						
							<div style="padding-left:392px;width:634px"><strong>No Record Found!</strong></div>
						
						</li>
						  
					 <?php }
					
					?>
				
				<script type="text/javascript">var last_id = <?php echo $last_id; ?>;</script>
                </ul>
				 <div id="loader" style="display:none"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>
            </div>
		</section>                                                            
	</section>    
    </div>    