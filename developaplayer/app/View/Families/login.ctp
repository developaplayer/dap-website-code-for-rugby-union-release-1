 <div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<!--<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span>Family Member Login</li>
			</ul>-->
		</div>
	</section> 
  <section class="vspace70 center">
  <div style="color:green"> <?php
                                $focus_class=""; 
							    $error_class=""; 
								if ($this->Session->check('Message.flash'))
								{
									echo $this->Session->flash();
									$focus_class="focused";
									$error_class="error" ;
								}
								
		                        ?>
								
								</div>
            <div class="container">
            	<div class="login_bg">
                	<div class="login_bg_innr">
                    	<div class="profile_ico_holder"><img src="<?php echo $this->webroot; ?>images/ico_profile2.png" alt=""></div>                    
                    	
						  <?php  echo $this->Form->create('families',array("controller"=>"families","action"=>"login","enctype"=>"multipart/form-data",'id'=>'sign_up_id','onSubmit'=>'return last_validation()')); ?>
                            <div class="form-group email-input <?php echo $focus_class.' '.$error_class ?>">
                                <label>Email Id</label>
                                <!--<input type="email" name="email" class="form-control" required>-->
								<?php echo $this->Form->input('User.email_address', array('type' => 'text','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group password-input <?php echo $focus_class.' '.$error_class ?>">
                                <label>Password</label>
<!--<input type="password" name="password" class="form-control" required>-->
								<?php echo $this->Form->input('User.password', array('type' => 'password','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group family">
                                <span class="pull-left"><a href="<?php echo $this->webroot; ?>families/forgot_password" class="forgot_password">Forgot your password?</a></span>
                                <span class="pull-right"><input type="submit" value="Login"></span>
                            </div>
                       <?php echo $this->Form->end(); ?>
                    </div>
                </div>   
                <span class="login_bg_bottom_txt">Don't have an account? <a href="<?php echo $this->webroot;?>families/register">Sign up</a> as a family.</span>
            </div>
		</section>   

</div>		