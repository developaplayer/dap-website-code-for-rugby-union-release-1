<?php 
 $multiple_certi=explode(",",$new_arry_certi);
?>
<div class="main_container login-reg-pg">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="<?php echo $this->webroot; ?>"><span>&raquo;</span> Develop a player</a></li>
				<li><span>&raquo;</span> Edit Family Member</li>
			</ul>
		</div>
	</section>
	<section class="vspace70">
	 
		<div class="container">
			<div class="reg_bg">
				<!--<form action="" method="post" enctype="multipart/form-data" id="register_form">-->
				<?php  echo $this->Form->create('families',array('id'=>'edit_id','controller'=>'families','action'=>'edit_profile', 'enctype'=>'multipart/form-data')); ?>
				<div class="row vspace20">                   	
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/lock.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Account Information</h2>
								<h5 style="color:red">All fields are mandatory.Please fill in the information.</h5>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div style="color:green"> <?php 
								if ($this->Session->check('Message.flash')) {
									echo $this->Session->flash();
								}
								if ($this->Session->check('Message.auth')) {
									//echo $this->Session->flash('auth');
								}
		                        ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Email Id</label><span class="pull-right hide" id="email_id" style="color:red">Email Id Already Exist</span>
								<input readonly="readonly" type="email" name="email_address" class="form-control" value="<?php echo (isset($get_edit_data['User']['email_address']))  ? $get_edit_data['User']['email_address'] : "";  ?>" placeholder="ex: steve@gmail.com" required>
							</div>
						</div>						                   
					</div>                        
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/person.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Personal Information</h2>
							</div>
						</div> 
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>First Name</label><span class="pull-right hide" id="first_name" style="color:red">Enter A Valid First Name</span>
								<input type="text" name="first_name" value="<?php echo (isset($get_edit_data['User']['first_name']))  ? $get_edit_data['User']['first_name'] : "";  ?>" class="form-control" required>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Last Name</label><span class="pull-right hide" id="last_name"  style="color:red">Enter A Valid Last Name</span>
								<input type="text" name="last_name" class="form-control" value="<?php echo (isset($get_edit_data['User']['last_name']))  ? $get_edit_data['User']['last_name'] : "";  ?>" required>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Date of Birth</label><span class="pull-right hide" id="date" style="color:red">Invalid Date Format</span>
								<!--<input type="text" name="dob" class="form-control" placeholder="dd/mm/yyyy" required>-->
								
								<div class='input-group date' id='datetimepicker1'>
									<input type='date' name="dob" class="form-control" data-date-format="YYYY-MM-DD" placeholder="YYYY-MM-DD" value="<?php echo (isset($FamilyDetail_data['FamilyDetail']['dob']))  ? $FamilyDetail_data['FamilyDetail']['dob'] : "";  ?>" required />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								
							</div>
							<?php 
								echo $this->Form->input('country',array('div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12','class'=>'form-control','label' => 'Nationality','type'=>'select','name'=>'nationality','required'=>true,'data-placeholder'=>'Select Your Country','options'=>$country,'empty' => 'Select Nationality','selected'=>isset($FamilyDetail_data['FamilyDetail']['nationality'])  ? $FamilyDetail_data['FamilyDetail']['nationality'] : ""));
							?>
						</div>
					
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<label>Upload Your Profile Photo</label><span class="pull-right hide" id="image" style="color:red">Upload A Valid Image</span>
								<input type="file" id="profile_img" name="profile_img" class="form-control"  accept="image/*">
							</div>
						</div>	
				
                   <output  id='list'><span><img src="<?php echo $this->webroot;?>uploads/profiles/family/thumb/<?php echo isset($FamilyDetail_data['FamilyDetail']['profile_img'])  ? $FamilyDetail_data['FamilyDetail']['profile_img'] : "" ?>"/></span></output>
				  
					</div>
				</div>
				<div class="row vspace20">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
						<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/coach.png" alt=""></span>
					</div>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h2>Relationship with player</h2>
							</div>
						</div>
						
							<!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Primary Speciality</label>
								<select class="form-control">
									<option selected>--Select--</option>
								</select>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<label>Secondary Speciality</label>
								<select class="form-control">
									<option selected>--Select--</option>
								</select>
							</div>-->
							<?php 
							
								/*echo $this->Form->input('player_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'Choose Player Name',
													'type'=>'select',
													'name'=>'player_id',
													'required'=>true,
													'data-placeholder'=>'Select Your Player',
													'empty' => 'Select Your Player',
													'selected'=>isset($FamilyDetail_data['FamilyDetail']['player_id'])  ? $FamilyDetail_data['FamilyDetail']['player_id'] : "",
													'options'=>$player_list
																)
															);*/
															
								?>
												
					<div class="row" id="row_relation_main">
                         <div id="row_relation">
						 
					<?php
                     $i=0;					
                      foreach($all_relation_player_id as $all_relation_player_ids)
					  {
					?>
					<div id="remove_<?php echo $i; ?>">
							<?php
								echo $this->Form->input('player_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'Choose Player Name',
													'type'=>'select',
													'onChange'=>'player_id_check(this)',
													'name'=>'player_id[]',
													'selected'=>$all_relation_player_ids['FamilyPlayerRelationship']['player_id'],
													'data-placeholder'=>'Select Your Player',
													'empty' => 'Select Your Player',
													'options'=>$player_list
																)
															);
													
								echo $this->Form->input('relation_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'form-control',
													'label' => 'My Relation with the player',
													'type'=>'select',
													'onChange'=>'relation_check(this)',
													'name'=>'relation_id[]',
													'selected'=>$all_relation_player_ids['FamilyPlayerRelationship']['relation_id'],
													'data-placeholder'=>'Select Your Relationship',
													'empty' => 'Select Your Relationship',
													'options'=>$relation_list
													)
												);
							if($i>0)	
							{?>								
						 <div onclick="remove_relation_div(<?php echo $i;?>)" class="removeIcon"><a style="cursor:pointer;float:right;"><span class="glyphicon glyphicon-remove-circle"></span></a></div>
							<?php } ?>
					</div> 	
					  <?php 
					   $i++;
					  }
					 ?>					  
			
			
					</div>
					<span class="hide" id="last_span"></span>
				</div>                    
              	<span class="addMoreBtn" id="p_scents">
                   <a href="javascript:void(0);"><span class="glyphicon glyphicon-plus-sign"></span> Add More</a>
                </span> 					
                <div class="row vspace20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="submit" value="Save">
                    </div>
                </div>				
				<?php echo $this->Form->end(); ?>
			</div>
			</div>
		</div>            
	</section>                                                   
</div>    
<script>
$(document).ready(function() 
		{
			$('#edit_id').validate();			
           
		});
var clicks = 0;
$(document).ready(function(){
	
	var all_relation_player_id = '<?php echo count($all_relation_player_id);?>';
	clicks =all_relation_player_id-1;
	if(all_relation_player_id==3)
	{
	   $('#p_scents').hide();
	}
		
});

$('input[name="password"]').keyup(function(){
	if($('input[name="password"]').val().length < 6){
		$('#password').removeClass('hide');
	}else{
		$('#password').addClass('hide');
	}
});
$('input[name="confirm_password"]').keyup(function(){
	if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
		$('#confirm_password').removeClass('hide');
	}else{
		$('#confirm_password').addClass('hide');
	}
});
$('input[name="height"]').keyup(function(){
	if($.isNumeric($('input[name="height"]').val())){
		$('#height').addClass('hide');
	}else{
		$('#height').removeClass('hide');
	}
});
$('input[name="weight"]').keyup(function(){
	if($.isNumeric($('input[name="weight"]').val())){
		$('#weight').addClass('hide');
	}else{
		$('#weight').removeClass('hide');
	}
});
$('#register_form').submit(function(){
	if($('input[name="password"]').val().length < 6){
		$('#password').removeClass('hide');
		$('input[name="password"]').focus();
		return false;
	}else{
		$('#password').addClass('hide');
	}
	if($('input[name="confirm_password"]').val() != $('input[name="password"]').val()){
		$('#confirm_password').removeClass('hide');
		$('input[name="confirm_password"]').focus();
		return false;
	}else{
		$('#confirm_password').addClass('hide');
	}
	if($.isNumeric($('input[name="height"]').val())){
		$('#height').addClass('hide');
	}else{
		$('#height').removeClass('hide');
		$('input[name="height"]').focus();
		return false;
	}
	if($.isNumeric($('input[name="weight"]').val())){
		$('#weight').addClass('hide');
	}else{
		$('#weight').removeClass('hide');
		$('input[name="weight"]').focus();
		return false;
	}
});


//=========================Image Showing=========================================//		
		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<img class="hide_shown_image" style="height: 160px; width:130px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/>'].join('');
					$("#list").html('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);



//=======================================================================================================//

$(function() 
{
	var relation_html =$('#row_relation_hide').html();
	
    $('#p_scents').click(function() {
	clicks += 1;
	if(clicks<=2)
	{
	 $('#last_span').before('<div id="remove_'+clicks+'">'+relation_html+'<div class="removeIcon" onclick="remove_relation_div('+clicks+')"><a style="cursor:pointer;float:right;"><span class="glyphicon glyphicon-remove-circle"></span></a></div></div>'); 
	}
	
	if(clicks==2)
	{				
		 $('#p_scents').hide();				
	}	
	    			
    });
});

var removeclicks=0;
function remove_relation_div(Id)
{
	clicks=clicks-1;
	//alert(clicks);
	removeclicks += 1;
	$('#remove_'+Id).remove('');
	$('#p_scents').show();
	
}

</script>
 <div class="row" style="display:none;" id="row_relation_hide">
						 
					<?php
                    
							
								echo $this->Form->input('player_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'require form-control',
													'label' => 'Choose Player Name',
													'type'=>'select',
													'onChange'=>'player_id_check(this)',
													'name'=>'player_id[]',												
													'data-placeholder'=>'Select Your Player',
													'empty' => 'Select Your Player',
													'options'=>$player_list
																)
															);
													
								echo $this->Form->input('relation_id',array(
													'div'=>'col-lg-6 col-md-6 col-sm-12 col-xs-12',
													'class'=>'require form-control',
													'label' => 'My Relation with the player',
													'type'=>'select',
													'name'=>'relation_id[]',													
													'data-placeholder'=>'Select Your Relationship',
													'empty' => 'Select Your Relationship',
													'options'=>$relation_list
													)
												);
					
?>					  
						    </div> 		
<script type="text/javascript">

function player_id_check(Obj)
{
	var current_id =Obj.value;
	Obj.setAttribute('name','');
	//alert(current_id);
	 //var i=0;
	$('select[name="player_id[]"]').each(function(){
	var each_id=this.value;
	//if(i>0)
	//{
		if(each_id==current_id)
		{
			
			alert('You already selected this player. please change');
		
			$(Obj).val('');
			
			
		}
	//}
	
    //i++;
  });
  Obj.setAttribute('name','player_id[]');
	
	
}

//=================With Relatioship=============================//

function relation_check(Obj)
{
	var current_id =Obj.value;
	//Obj.setAttribute('name','');	
	//==============get Palyer id==================//	
	 var each_player_id="";
	$('select[name="player_id[]"]').each(function(){
	  return each_player_id=this.value;
     
    });
	
	$('select[name="player_id[]"]').each(function(){
	  return player_name=this.text;
     
    });
	
	//=====================End======================//
	
	//======================Get Relation=============//
	
	$('select[name="relation_id[]"]').each(function(){
		relation_id=this.value;			
       return relation_id=this.value;
	   
    });
	
	$('select[name="relation_id[]"]').each(function(){
		relation_id=this.value;			
       /// return relation_text=$(this).text();
	   
    });
	
	if(each_player_id=="")
	{
		
		//alert('Please Select Player');
		//$('select[name="relation_id[]"]').val('');
		//$(Obj).val('');		
	}
	
	if(each_player_id!="" && relation_id!="")
	{
	  $.ajax({
			type: "POST",
			url: "<?php echo $this->webroot;?>families/relation_ship_exit_or_not",		
			data: 'player_id='+each_player_id+'&relation_id='+relation_id,
			success: function(data)
			{			
             if(data>0)
			 {
				//alert('Selected Relation With Palyer Aleready Exits,Please Select other.');
				//$('select[name="relation_id[]"]').val('');
				 
			 }				 
			}
		});
		
	}	
	
	 //alert(each_player_id+''+relation_id);
	
	// Obj.setAttribute('name','relation_id[]');
  
  //============================End====================//
}


</script>

