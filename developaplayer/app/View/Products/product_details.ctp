<?php 

//pr($product_sizes);
//exit;
$price_explode = explode('.',$product_details['Product']['price']);
	if(isset($price_explode[0]))
	{
		$fistpartprice= $price_explode[0];	
	}else								
	{
		$fistpartprice=00;	
			
	}							
	if(isset($price_explode[1]))
	{
		$secpartprice= $price_explode[1];								
	}else								
	{
		$secpartprice='';										
	}							
							
	?>

 <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Apparel</li>
                </ul>
               <a href="<?php echo $this->webroot;?>carts/add_to_cart"> <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot ?>images/cart_icon.png" alt="">
                    	<span class="cart_val"><?php echo CakeSession::read('total_size'); ?></span>
                  	</span>
                    <span class="cart_prc">AUD <?php echo CakeSession::read('fprice'); ?><sup><?php echo CakeSession::read('sprice'); ?></sup></span>
                </div></a>
            </div>
		</section>
        <section class="product_listing">
		<?php echo $this->Form->create('carts',array('type' => 'post', 'class' => 'form-horizontal', 'controller'=>'carts', 'action'=>'add_to_cart')); ?>
             <!-- 
			 <form action="<?php $this->webroot ?>carts/add_to_cart" method="post" enctype="multipart/form-data" id="register_form">
            -->
			<div class="container"> 
            	<div class="prod_det_holder">           	
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6 prod_det_left">
                    	<div class="prod_det_gallery">
                            <div class="slide-smalls">
                                <ul  id="scroller" class="selectors">
								<?php foreach((array)$product_details['AllProductImage'] as $AllProductImage){?>
                                    <li><a class="scroll_li_id" style="display:block" data-zoom-id="zoom1" href="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $AllProductImage['image'] ?>" data-image="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $AllProductImage['image'] ?>?scale.width=56" alt=""/><img srcset="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $AllProductImage['image'] ?>?scale.width=112 2x" src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $AllProductImage['image'] ?>?scale.width=56" alt=""/></a></li>
								<?php } ?>	
                                </ul>
                            </div>                             
                            <div class="main-display">
                                <a id="zoom1" data-options="textHoverZoomHint: ''; textClickZoomHint: ''; textExpandHint: '';rightClick: true;" class="MagicZoom" href="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $product_details['ProductImage']['image'] ?>"><img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $product_details['ProductImage']['image'] ?>?scale.width=450" alt=""/></a>           
                            </div>
						</div>
                        <script>
                        	$(document).ready(function(){
								$("#scroller").simplyScroll({
									customClass:'vert',
									orientation:'vertical',
									auto: false	,
									speed: 6
								});
								$(".scroll_li_id").show();
							});
                        </script>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-6 col-xs-6 prod_det_mid">                        
                        <h2 class="product_title"><?php echo $product_details['Product']['name']; ?></h2>
                        <span class="product_price">AUD <?php echo $fistpartprice; ?><sup><?php echo $secpartprice; ?></sup></span>
                        <span class="size_selection_row">
                        	SELECT SIZE:<br>
                            <ul>
							 <?php foreach((array) $product_sizes as $all_product_sizes)
							 {
							?>
                                <li id="<?php echo $all_product_sizes['Size']['id']; ?>"><?php echo $all_product_sizes['Size']['name']; ?></li>
							
						   <?php } ?>
                               
                            </ul>
							<div id="error_size" style="display:none; color:red"> Please Select Your Product Size</div>
                            <script>
                            	$(document).ready(function(){
									$('.size_selection_row ul li').click(function(){
										
										$('.size_selection_row ul li').removeClass('active');
										
										$(this).toggleClass('active');
										 var html_size=$(this).html();
										 $('#product_size').val(html_size);
										 var size_id=$(this).attr('id');
										 $('#product_size_code').val(size_id);
										 $('#error_size').hide();
										 
										 var productId=$('#product_id').val();
										
										 //==================check product Quantityinty================//
										 
										  $.ajax({
													url: '<?php echo $this->webroot;?>products/ajx_check_stock',
													type: 'POST',
													data: {'productId':productId,'Product_size_code':size_id},
													success:function(data)
													{
														
														if(parseInt(data)<1)
														{	
													   $('.size_selection_row.plus_minus').hide();
													    $('#out_of_stock_id').show();    
														 $('#add_to_cart').attr("disabled", 'disable');
														 //$("#add_to_cart").css("opacity", "0.5"); 
														  $('#add_to_cart').addClass("cart_disabled");
														
														 
														}else
														{   
													         $('.size_selection_row.plus_minus').show();
															 $('#add_to_cart').removeAttr("disabled");
															 //$("#add_to_cart").css("opacity", ""); 
															 $('#add_to_cart').removeClass("cart_disabled");
															 $('#out_of_stock_id').hide();  
															
														}

													}
												});
		
										 
									});
								});
                            </script>
                            <!--<a href="javascript:void(0);" class="size_guide_link">Size Guide</a>-->
                        </span>
                        <span class="size_selection_row plus_minus">
                        	SELECT Quantity:<br>
                            <a href="javascript:void(0);" class="minus">-</a>
            				<input type="text" id="user_product_qty" value="1" />
            				<a href="javascript:void(0);" class="plus">+</a>
                            <script>
                        $(document).ready(function(){	
									$(".plus").click(function() 
									{
										var text = $(this).prev(":text");
										var qty_val=text.val(parseInt(text.val(), 10) + 1);
									
										$('#product_qty').val(parseInt(text.val()));
									});
								
									$(".minus").click(function()
									{
										
										var text = $(this).next(":text");										
										var select_qty=text.val();
										if(select_qty>1)
										{
											text.val(parseInt(text.val(), 10) - 1);
											$('#product_qty').val(parseInt(text.val()));
											
										}
										
									
										
										//alert(parseInt(text.val()));
										
									});
									
									//===========================================
									
									$("#add_to_cart").click(function()
									{
										//=============Stock===================//
										
										//var Product_size_code=$('#product_size').val();
										var Product_size_code=$('#product_size_code').val();
										var productId=$('#product_id').val();
									//	alert(Product_size_code);
									
									                
										if(Product_size_code=="")
										{
											$('#error_size').show();
											return false;
											
										}else
										{
                                              $.ajax({
													url: '<?php echo $this->webroot;?>carts/ajx_check_stock',
													type: 'POST',
													data: {'productId':productId,'Product_size_code':Product_size_code},
													success:function(data){
														var userProductQty= $('#user_product_qty').val();
														
														if(parseInt(data)>=parseInt(userProductQty))
														{
															//alert('123');
															$('#cartsAddToCartForm').submit();															
														}else
														{
															//alert('321');
															$('#error_qty').show();
															$('#max_qty').html(data);
															$('#user_product_qty').val(data);
															$('#product_qty').val(data);
															
														}
													  
													}
												});
		
									       
										}	
										
	                                  
                                    });
									
									
										$( "#user_product_qty" ).keyup(function() 
										{
											var qty=$( "#user_product_qty" ).val();
											
											if(qty==0)
											{
											  $( "#user_product_qty" ).val(1)
											   var qty=1;	
											}
											$('#product_qty').val(qty);
											
                                        });
									
							});	
                            </script>
                        </span>
						<div id="error_qty" style="display:none; color:red">We're sorry! We are able to accommodate only <span id="max_qty"></span> units</div>
                        <span class="size_selection_row">
                        	<!--<a href="<?php echo $this->webroot;?>carts/cart" class="add_to_cart">Add To Bag</a>-->
							<div id="out_of_stock_id" style="display:none;color:red"><strong>out of stock</strong></div>
							<input  type="button" name="add_to_cart" id="add_to_cart"  value="Add To Bag"  />
                        </span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 prod_det_right">                        
                        <div id="prod_det_tabs">
                            <ul>
                              <li><a href="#tabs-1">Details</a></li>
                              <li><a href="#tabs-2">Shipping &amp; Returns</a></li>
                            </ul>
                            <div id="tabs-1">
                                  <p> <?php  if(isset($product_details['Product']['description'])) echo $product_details['Product']['description']; ?></p>
                            </div>
                            <div id="tabs-2">
                                  <p><?php  if(isset($product_details['Product']['return_policy'])) echo $product_details['Product']['return_policy']; ?></p>
                            </div>
                            <script>
								$(document).ready(function(){
									$( "#prod_det_tabs" ).tabs();
									$("#prod_det_tabs .ui-tabs-panel").mCustomScrollbar({
										scrollButtons:{
											enable:false
										},
										advanced: {
											updateOnContentResize: Boolean
										}
									});
								});
							</script>
                        </div>
                    </div>
            	</div>
            </div>
			
			<input type="hidden" name="product_id" id="product_id" value="<?php echo $product_details['Product']['id'];?>"/>
			<input type="hidden" name="product_size" id="product_size"/>
			<input type="hidden" name="product_unit_price" id="product_unit_price" value="<?php echo $product_details['Product']['price'];?>"/>
			<input type="hidden" name="product_qty" id="product_qty" value="1"/>
			<input type="hidden" name="product_size_code" id="product_size_code"/>
				
		<?php echo $this->Form->end(); ?> 				
		</section>
        <section class="featured_training_gear vspace30 center">
            <div class="container">
            	<h2>You Recently Viewed Products</h2>
            	<div class="flexslider training_gears">
                  	<ul class="slides">					
					  <?php 					
					  if(!empty($GuestDetails))
					  {
					  foreach((array) $GuestDetails as $AllGuestDetails)
					  {
						  
						 $price_explode = explode('.',$AllGuestDetails['Product']['price']);
						if(isset($price_explode[0]))
						{
							$fistpartprice_recent= $price_explode[0];	
						}else								
						{
							$fistpartprice_recent=00;	
								
						}							
						if(isset($price_explode[1]))
						{
							$secpartprice_recent= $price_explode[1];								
						}else								
						{
							$secpartprice_recent='';										
						} 
						  
						?>  
					 
                    	<li>
                        	<span class="training_gear_img"><a href="<?php echo $this->webroot;?>products/product_details/<?php echo base64_encode($AllGuestDetails['Product']['id']) ?>"><img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $AllGuestDetails['Product']['ProductImage']['image']; ?>" alt=""></a></span>
                            <span class="training_gear_nm"><a href="<?php echo $this->webroot;?>products/product_details/<?php echo base64_encode($AllGuestDetails['Product']['id']) ?>"><?php echo $AllGuestDetails['Product']['name'] ?></a></span>
                            <span class="training_gear_prc">AUD <?php echo $fistpartprice_recent; ?><sup><?php echo $secpartprice_recent; ?></sup></span>
                       	</li>
					  <?php } } ?> 
                    </ul>
                </div>
            </div>
		</section>                                                   
	</div>

