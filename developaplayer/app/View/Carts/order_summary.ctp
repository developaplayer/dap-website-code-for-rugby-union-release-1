 <?php //pr($all_purches_orders_details);  exit;?>
 
 <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Order Details</li>
                </ul>
               <!-- <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="images/cart_icon.png" alt="">
                    	<span class="cart_val">5</span>
                  	</span>
                    <span class="cart_prc">AUD 135<sup>99</sup></span>
                </div> -->
            </div>
		</section>
		
		
        <section class="product_listing">
		
            <div class="container orderSummaryPg"> 
	<?php
	     if(!empty($all_purches_orders_details))
		 {	 
		foreach ((array)$all_purches_orders_details as $all_purches_orders)
		{
		?>
            	<div class="cart_holder orderSummaryRow">
                    <div class="cart_row orderSummaryHd">
                        <div class="cart_col"><a target="_blank" href="<?php echo $this->webroot ?>carts/order_information/<?php echo base64_encode($all_purches_orders['Order']['id']); ?>" class="orderId">Order Id: <?php echo $all_purches_orders['Order']['invoice_id']; ?></a></div>
                        <div class="cart_col">&nbsp;</div>
                        <div class="cart_col">&nbsp;</div>
                        <div class="cart_col">&nbsp;</div>
                        <div class="cart_col">&nbsp;</div>
                        <div class="cart_col">&nbsp;</div>
                        <div class="cart_col">&nbsp;</div>
                    </div>
                    <div class="cart_row">
                        <div class="cart_col">Product Description</div>
                        <div class="cart_col">Qty</div>
                        <div class="cart_col">Size</div>
                        <div class="cart_col">Unit Price</div>
                        <div class="cart_col">Total</div>
                        <div class="cart_col">Order Date</div>
                        <div class="cart_col">Status</div>
                    </div>
					
					<?php //pr($all_purches_orders['OrderProduct']['MYPRODUCT']);
					//exit; ?>
					<?php
                     $i=0;
					foreach((array) $all_purches_orders['OrderProduct']['MYPRODUCT'] as $MyProducts ) { ?>
                    <div class="cart_row">
                        <div class="cart_col">
                            <span class="prod_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $MyProducts['ProductImage']['image']; ?>" alt=""></a></span>
                            <span class="prod_desc"><a href="javascript:void(0);"><?php echo $MyProducts['name']; ?></a></span>
                        </div>
						<?php 							
							$price_explode = explode('.',$MyProducts['price']);

							if(isset($price_explode[0]))
							{
								$fistpartprice= $price_explode[0];
								
							}else
								
							{
								$fistpartprice=00;	
									
							}							
							if(isset($price_explode[1]))
							{
								$secpartprice= $price_explode[1];
								
							}else
								
							{
								$secpartprice='';										
							}
							
							?>
							
						
							
						
							
							<?php 							
							$price_explode = explode('.',$MyProducts['price']*$all_purches_orders['OrderProduct'][$i]['quantity']);

							if(isset($price_explode[0]))
							{
								$fistpartprice1= $price_explode[0];
								
							}else
								
							{
								$fistpartprice1=00;	
									
							}							
							if(isset($price_explode[1]))
							{
								$secpartprice1= $price_explode[1];
								
							}else
								
							{
								$secpartprice1='00';										
							}
							
							?>
							
							
						
                        <div class="cart_col price"><?php echo $all_purches_orders['OrderProduct'][$i]['quantity']; ?></div>
                        <div class="cart_col price"><?php echo $MyProducts['size']; ?></div>
                        <div class="cart_col price">AUD <?php echo $fistpartprice; ?><sup><?php echo $secpartprice;?></sup></div>
                        <div class="cart_col price">AUD <?php echo $fistpartprice1; ?><sup><?php echo $secpartprice1;?></sup></div>
                        <div class="cart_col price"><?php echo date('j,F Y',strtotime($all_purches_orders['Order']['created'])); ?></div>
						
						<?php 
						
						if($all_purches_orders['Order']['order_status']==1)
						{
							$status='Pending';

						}else if($all_purches_orders['Order']['order_status']==2)
						{
							$status='Processing';
						}else
						{
							$status='Delivered';
							
						}							
						?>
						
                        <div class="cart_col price"><?php echo $status; ?></div>
                    </div>
                  
					<?php 
					$i++;
					} 
					
					?>
					
					<?php 							
							$price_explode = explode('.',$all_purches_orders['Order']['total']);

							if(isset($price_explode[0]))
							{
								$fistpartprice2= $price_explode[0];
								
							}else
								
							{
								$fistpartprice2=00;	
									
							}							
							if(isset($price_explode[1]))
							{
								$secpartprice2= $price_explode[1];
								
							}else
								
							{
								$secpartprice2='';										
							}
							
				?>
                                                    
                    <div class="cart_row">
                        <div class="cart_col final">You pay</div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col final price">AUD <?php echo $fistpartprice2 ?><sup><?php echo $secpartprice2 ?></sup></div>                        
                    </div>
                </div>
				

				
              <?php } 
			  
			  }else{?>
			  
			   	<p class="notFound">No Order Found !</div>
				  
			
			  <?php } ?>			
                                       
           
			<div class="pagination pagination-large" style="float:right">
    <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
        </ul>
    </div>
 
  </div>
		
		</section> 

	</div>    