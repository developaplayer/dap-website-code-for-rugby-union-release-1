<div class="main_container">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
				<li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
				<li><span>&raquo;</span> Cart</li>
			</ul>
			<!--<div class="topbar_cart">
				Shopping Cart:
				<span class="cart_ico_holder"><img src="images/cart_icon.png" alt="">
					<span class="cart_val">5</span>
				</span>
				<span class="cart_prc">AUD 135<sup>99</sup></span>
			</div>-->
		</div>
	</section>
	<section class="product_listing">
		<div class="container">             	
			<div class="orderDetailsWrapper">
				<div class="row">
					<div class="col-lg-12"><h2 class="orderDetailsHeading">Order Details</h2></div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div class="col-lg-12 vspace10">Order Id: <span><?php echo $orderDetails['Order']['invoice_id']; ?></span></div>
						<div class="col-lg-12 vspace10">Order Date: <span><!--07 April, 2016, 02:17 PM--><?php echo date('j F, Y, H:i a',strtotime($orderDetails['Order']['created'])); ?></span></div>
						<div class="col-lg-12 vspace10">Amount Paid: <span> AUD.<?php echo $orderDetails['Order']['total']; ?></span></div>
					</div>
					<div class="col-lg-6">
						<div class="col-lg-12 vspace10"><span style="font-size:24px"><?php echo $orderDetails['Order']['payment_fullname']; ?></span> <?php //echo $orderDetails['Order']['contact_no']; ?></div>
						<div class="col-lg-12 vspace10"><?php echo $orderDetails['Order']['payment_address']; ?>, <?php echo $orderDetails['Order']['payment_city']; ?>-<?php echo $orderDetails['Order']['payment_zip']; ?>, <?php echo $orderDetails['Order']['payment_state']; ?>, Australia</div>
					</div>
				</div>
			</div>
		</div>
	</section>                                                           
</div>  