<?php
//pr($order_address24);
//==================Shipping===========================//
if(isset($order_address24['ship_full_name']))
{
	$ship_full_name=$order_address24['ship_full_name'];	
}else
{
	$ship_full_name="";		
}
	
if(isset($order_address24['ship_address']))
{
	$ship_address=$order_address24['ship_address'];	
}else
{
	$ship_address="";		
}

if(isset($order_address24['ship_city']))
{
	$ship_city=$order_address24['ship_city'];	
}else
{
	$ship_city="";		
}

if(isset($order_address24['ship_state']))
{
	$ship_state=$order_address24['ship_state'];	
}else
{
	$ship_state="";		
}

if(isset($order_address24['ship_locality']))
{
	$ship_locality=$order_address24['ship_locality'];	
}else
{
	$ship_locality="";		
}

if(isset($order_address24['contact_no']))
{
	$contact_no=$order_address24['contact_no'];	
}else
{
	$contact_no="";		
}

if(isset($order_address24['ship_zip']))
{
	$ship_zip=$order_address24['ship_zip'];	
}else
{
	$ship_zip="";		
}
//==================End===========================	
//==================Bill============================

if(isset($order_address24['bill_name']))
{
	$bill_name=$order_address24['bill_name'];	
}else
{
	$bill_name="";		
}
	
if(isset($order_address24['bill_address']))
{
	$bill_address=$order_address24['bill_address'];	
}else
{
	$bill_address="";		
}

if(isset($order_address24['bill_city']))
{
	$bill_city=$order_address24['bill_city'];	
}else
{
	$bill_city="";		
}

if(isset($order_address24['bill_state']))
{
	$bill_state=$order_address24['bill_state'];	
}else
{
	$bill_state="";		
}

if(isset($order_address24['bill_locality']))
{
	$bill_locality=$order_address24['bill_locality'];	
}else
{
	$bill_locality="";		
}

if(isset($order_address24['bill_zip']))
{
	$bill_zip=$order_address24['bill_zip'];	
}else
{
	$bill_zip="";		
}
	
?>
<div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Shipping Address</li>
                </ul>
                 <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot ?>images/cart_icon.png" alt="">
                    	<span class="cart_val"><?php echo CakeSession::read('total_size'); ?></span>
                  	</span>
                    <span class="cart_prc">AUD <?php echo CakeSession::read('fprice'); ?><sup><?php echo CakeSession::read('sprice'); ?></sup></span>
                </div>
            </div>
		</section>
        <section class="product_listing">
            <div class="container">
            	<!--<div class="tab_holder">
                	<ul>
                    	<li class="active">1.Details &amp; Shipping</li>
                        <li>2. Review your order</li>
                        <li>3. Pay &amp; Finish</li>
                    </ul>
                </div>-->
                <form action="<?php echo $this->webroot?>carts/shipping_address" method="post">
                <div class="shipping_billing">                	
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<h2>Shipping Address:</h2>                        
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Full Name</label>
                                <input type="text" name="ship_full_name" id="ship_full_name" value="<?php echo $ship_full_name; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Shipping Address</label>
                                <input type="text" name="ship_address" id="ship_address" value="<?php echo $ship_address; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>City</label>
                                <input type="text" name="ship_city" id="ship_city" value="<?php echo $ship_city; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>State</label>
								   <input type="text" name="ship_state" id="ship_state" value="<?php echo $ship_state; ?>" class="form-control" placeholder="" required="">
                                <!--<select class="form-control">
                                    <option selected="">Select your State</option>
                                </select>-->
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Locality</label>
                                <input type="text" name="ship_locality" id="ship_locality"  value="<?php echo $ship_locality; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Zip</label>
                                <input type="text" name="ship_zip" id="ship_zip" value="<?php echo $ship_zip; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Contact No</label>
                                <input type="text" name="contact_no" id="contact_no" value="<?php echo $contact_no; ?>" class="form-control" placeholder="" required>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<h2>Billing Address: <span><input type="checkbox" id="ship_check" name="ship_check" > Same as Shiiping Address</span></h2>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Full Name</label>
                                <input type="text" name="bill_name" id="bill_name" value="<?php echo $bill_name; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Billing Address</label>
                                <input type="text" name="bill_address" id="bill_address" value="<?php echo $bill_address; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>City</label>
                                <input type="text" name="bill_city" id="bill_city" value="<?php echo $bill_city; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>State</label>
								 <input type="text" name="bill_state" id="bill_state" value="<?php echo $bill_state; ?>"  class="form-control" placeholder="" required="">
                                <!--<select class="form-control">
                                    <option selected="">Select your State</option>
                                </select>-->
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Locality</label>
                                <input type="text" name="bill_locality" id="bill_locality" value="<?php echo $bill_locality; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Zip</label>
                                <input type="text" name="bill_zip" id="bill_zip" value="<?php echo $bill_zip; ?>" class="form-control" placeholder="" required="">
                            </div>
                        </div>
                    </div>                    
                </div>
            	<div class="cart_bottom checkout">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vspace10 right"><input type="submit"  value="Checkout"></div>
                </div>
                </form>
            </div>
		</section>                                                           
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	  $( "#ship_check" ).click(function()
	  {
		  if($("#ship_check" ).is(":checked"))
		  {
				$('#bill_name').val($('#ship_full_name').val());
				$('#bill_address').val($('#ship_address').val());
				$('#bill_city').val($('#ship_city').val());
				$('#bill_state').val($('#ship_state').val());
				$('#bill_locality').val($('#ship_locality').val());
				$('#bill_zip').val($('#ship_zip').val());
		  }else         
		  {
			  
			     $('#bill_name').val("");
				$('#bill_address').val("");
				$('#bill_city').val("");
				$('#bill_state').val("");
				$('#bill_locality').val("");
				$('#bill_zip').val("");
			  
		  }	  
		
		  
		});
		
		
		
		
	})
	
	function button_click()
		{
			//alert('sdfdsf');
			window.location.href="<?php echo $this->webroot;?>/products/product_list";
			
		}
	
	
	</script>