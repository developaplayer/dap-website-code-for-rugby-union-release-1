<?php 
$Subtotal = CakeSession::read('Subtotal');
 	//echo $this->Session->read('Auth.Member.id');
				//exit;
?>
<div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
			
			<?php if($this->request->params['action']=='review_order')
			{
				?>
			  <div class="tab_holder">
                	<ul>
                    	<li>1. Details &amp; Shipping</li>
                        <li class="active">2. Review your order</li>
                        <li>3. Pay &amp; Finish</li>
                    </ul>
                </div>
			<?php } ?>
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Cart</li>
                </ul>
				
                <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot ?>images/cart_icon.png" alt="">
                    	<span class="cart_val"><?php echo CakeSession::read('total_size'); ?></span>
                  	</span>
                    <span class="cart_prc">AUD <?php echo CakeSession::read('fprice'); ?><sup><?php echo CakeSession::read('sprice'); ?></sup></span>
                </div>
            </div>
		</section>
        <section class="product_listing">
		<!--<div id="loding_div_icon"  style="text-align:center;dispaly:none;"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>-->
		<span style="color:green;display:none;align:center;" id="loding_div_icon" ><strong>Loading....</strong></span>
            <div class="container">		
			<div id="error_qty" style="display:none; color:red;padding:8px 2px 3px 140px">We're sorry! We are able to accommodate only <span id="max_qty"></span> units of <span id="product_name_title"></span></div>
			
            	<div class="cart_holder">	
                	<div class="cart_row">
					
					<!--<span style="color:green;display:none;align:center;p" id="loding_div_icon" ><strong>Loding....</strong></span>-->
					<div class="cart_col">Product Description</div>
                        <div class="cart_col">Details</div>
                        <div class="cart_col">Unit Price</div>
                        <div class="cart_col">Total</div>
                        <div class="cart_col">&nbsp;</div>
                    </div>
				 
			<?php    if(!empty($products) && count($products)>0)
			            {
			         foreach((array) $products as $key=>$all_cart_products){ ?>
                    <div class="cart_row" id="cart_id_row<?php echo $key; ?>">
                    	<div class="cart_col">
                        	<span class="prod_img"><a href="javascript:void(0);"><img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $all_cart_products['image'] ?>" alt=""></a></span>
                            <span class="prod_desc"><a href="javascript:void(0);" id="product_name_cart<?php echo $all_cart_products['product_size_code']; ?>"><?php echo $all_cart_products['product_name']; ?></a></span>
                       	</div>
                        <div class="cart_col">
                        	<span class="bold_hd">Size</span> <span class="bold"><?php echo $all_cart_products['size']; ?></span><br><br>
                            <span class="bold_hd">Qty</span> <input id="product_qty_edit<?php echo $key;?>" type="text" value="<?php echo $all_cart_products['qty']; ?>"> 
                            <a href="javascript:void(0);"  onclick="edit_ajx_qty('<?php echo $key;  ?>','<?php echo $all_cart_products['product_id'].'-'.$all_cart_products['product_size_code']?>')" class="edit_btn">Edit</a>
							
                       	</div>
						<?php
						//===========================for price==================================//
						
						$price_explode = explode('.',$all_cart_products['product_unit_price']);
						if(isset($price_explode[0]))
						{
							$fistpartprice= $price_explode[0];
							
						}else
							
						{
							$fistpartprice=00;	
								
						}

						if(isset($price_explode[1]))
						{
							$secpartprice= $price_explode[1];
							
						}else
							
						{
							$secpartprice='';	
								
						}
	
//================Total Price=======================================	
							
				   $price_explode_total = explode('.',$all_cart_products['total']);
					if(isset($price_explode_total[0]))
					{
						$fistpartprice_total= $price_explode_total[0];	
					}else								
					{
						$fistpartprice_total=00;	
							
					}							
					if(isset($price_explode_total[1]))
					{
						$secpartprice_total= $price_explode_total[1];								
					}else								
					{
						$secpartprice_total='00';										
					}	
							
//===================================================================//
?>
						
                        <div class="cart_col price">AUD <?php echo $fistpartprice; ?><sup><?php echo $secpartprice; ?></sup></div>
                        <div class="cart_col price">AUD <?php echo $fistpartprice_total; ?><sup><?php echo $secpartprice_total; ?></sup></div>
                        <div class="cart_col close_btn"><a href="javascript:void(0);" onclick="ajx_remove_item('<?php echo $key;  ?>','<?php echo $all_cart_products['product_id'].'-'.$all_cart_products['product_size_code']?>');">X</a></div>
                    </div>
					<?php
					}
				}else{
					?> 
			<div style="text-align:center;padding:4px;color:red"><strong>No Cart items Added !</strong></div>		
					
		   <?php } ?>
		<?php //================Total Price=======================================	
	            
	        
			
			   $price_explode_sub_total = explode('.',$Subtotal['subtotal']);
				if(isset($price_explode_sub_total[0]))
				{
					$fistpartprice_sub_total= $price_explode_sub_total[0];	
				}else								
				{
					$fistpartprice_sub_total=00;	
						
				}							
				if(isset($price_explode_sub_total[1]))
				{
					$secpartprice_sub_total= $price_explode_sub_total[1];								
				}else								
				{
					$secpartprice_sub_total='00';										
				}	
							
//===================================================================//
?>

                    <div class="cart_row">
                    	<div class="cart_col">Subtotal</div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col price">AUD <?php echo $fistpartprice_sub_total ?><sup><?php echo $secpartprice_sub_total ?></sup></div>
                        <div class="cart_col"></div>
                    </div>
                    <div class="cart_row">
                    	<div class="cart_col final">You pay</div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col final price">AUD <?php echo $fistpartprice_sub_total ?><sup><?php echo $secpartprice_sub_total ?></sup></div>
                        <div class="cart_col"></div>
                    </div>
                </div>
				
				
                <div class="cart_bottom">
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vspace10">Need help ?<br><span class="call">Call us on (+61 1234 5678)</span></div>
										
				<?php
				//echo $this->Session->read('Auth.Member.id');
				//exit;
				if($this->Session->read('Auth.Member.id')>0)
				{
				?>									
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vspace10 right">
					   <a href="<?php echo $this->webroot;?>products/product_list"><input type="button" value="< Continue Shopping"></a>
					  <a href="<?php echo $this->webroot;?>carts/shipping_address"><input type="button" value="Checkout"></a>
					   
					</div>
				<?php } else { ?>
				 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vspace10 right">
			        <?php  echo $this->Form->create('Players',array('id'=>'check_out_login','controller'=>'players','action'=>'login')); ?>
		  <a href="<?php echo $this->webroot;?>products/product_list"><input type="button" value="< Continue Shopping"></a>
						<input type="submit" value="Checkout">					
					   <input type="hidden" name="no_login" value="0"/>
					
					<?php echo $this->Form->end(); ?>		   
					<?php } ?>
			
				</div>
                </div>
				
				 
				
				
			
			 
            </div>
		</section>                                                           
	</div>  

	<script type="text/javascript" >
	function ajx_remove_item(productId,Product_size_code)
	{
		 $.ajax({
                        url: '<?php echo $this->webroot;?>carts/ajx_remove_item',
                        type: 'POST',
                        data: {'productId':productId,'Product_size_code':Product_size_code},
                        success:function(data){						
							$('#cart_id_row'+productId).hide().animate({height: "20px"}, 500);
						    $('.main_container').html(data);
							
                        }
                    });
		
		
	}
	
//==============================================================//	
	
	function edit_ajx_qty(productId,Product_size_code)
	{ 
          $('#loding_div_icon').show();
		  var ProductSizeCode = Product_size_code;
          var ProductSizeCodeArray = ProductSizeCode.split('-'); 
		
		
		  
		  $.ajax({
			  url: '<?php echo $this->webroot;?>carts/ajx_check_stock',
													type: 'POST',
													data: {'productId':ProductSizeCodeArray[0],'Product_size_code':ProductSizeCodeArray[1]},
													success:function(data){
														  var edit_qty=$('#product_qty_edit'+productId).val();
													
														if(parseInt(data)>=parseInt(edit_qty))
														{
															
															 $.ajax({
																			url: '<?php echo $this->webroot;?>carts/ajx_edit_item',
																			type: 'POST',
																			data: {productId:productId,edit_qty:edit_qty,Product_size_code:Product_size_code},
																			success:function(data1){
																				$('#loding_div_icon').hide();
																				$('.main_container').html(data1);
																
																			}
																		});
		
															
														}else
														{
															  $('#product_qty_edit'+productId).val(data);
															
																
																$('#max_qty').html(data); 
																 $.ajax({
																			url: '<?php echo $this->webroot;?>carts/ajx_edit_item',
																			type: 'POST',
																			data: {productId:productId,edit_qty:data,Product_size_code:Product_size_code},
																			success:function(data1){
																				$('#loding_div_icon').hide();
																				
																				$('.main_container').html(data1);
																				
																				$('#error_qty').show();
																				$('#max_qty').html(data);
																				var product_name = $('#product_name_cart'+ProductSizeCodeArray[1]).html();
																				$('#product_name_title').html(product_name);
																				
																				
																
																			}
																		});
															
														}
													  
													}
												});
		  
		  
		 
	    
		
	}
	
	
	</script>	