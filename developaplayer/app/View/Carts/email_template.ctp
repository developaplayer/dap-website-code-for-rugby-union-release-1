
<?php //pr($all_oder_details);?>
<div style="width:50%; margin:0 auto; border:1px solid #d1d1d1; color:#333; font-size:12px;  font-family:Arial, Helvetica, sans-serif; ">
	<div style="background:#579EC8; background-image:url('<?php //echo $details['url'].'images/green-fibers.png' ?>'); height:64px; display:table-cell; vertical-align:middle; width:550px; border-bottom:3px solid #bbbbbb;" align="center">
		<img src="<?php //echo $details['url'].'/images/logo_mail.png';?>" alt="" />
	</div>
	<div style="padding:10px;">
		<p style="padding:0; margin:0;font-weight:bold; text-align:center; font-size:14px;"><?php echo __("Order Details");?> - <font style="color:#7fba00;"> <?php echo __("Fabrique Group");?></font></p>
	   <div class="padd0">
	  <!-- <div class="col-lg-12 padd0">-->
		 <div class="table-responsive">
			<table id="sample-table-1" class="table table-striped table-bordered table-hover">
			<tr>
				<td colspan="3" width="100%" align="left" valign="top" style="background:#108EEE; padding-left:10px;color:#ffffff;font-weight:bold;"><?php echo __("Thank you for your order from Fabrique Group");?>
				
				</td>
			  </tr>
			  <tr>
				   <td>
						<table width="100%">
							<tr>
								<td colspan="3"><?php echo __('BILLING DETAILS:');?></td>
							</tr>
							<tr>
								<td><?php echo __('FULL NAME');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_fullname'];?></td>
							</tr>
							
							<tr>
								<td><?php echo __('BILLING ADDRESS');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_address'];?></td>
							</tr>
							<tr>
								<td><?php echo __('COUNTRY');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_country'];?></td>
							</tr>
							<tr>
								<td><?php echo __('STATE');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_state'];?></td>
							</tr>
							<tr>
								<td><?php echo __('CITY');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_city'];?></td>
							</tr>
							<tr>
								<td><?php echo __('ZIP');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['payment_zip'];?></td>
							</tr>
							<tr>
								<td><?php echo __('CONTACT NO');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['contact_no'];?></td>
							</tr>
						</table>
				   </td>
				   <td>&nbsp;</td>
				   <td>
						<table width="100%">
							<tr>
								<td colspan="3"><?php echo __('SHIPPING DETAILS:');?></td>
							</tr>
							<tr>
								<td><?php echo __('FULL NAME');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_fullname'];?></td>
							</tr>
						
							<tr>
								<td><?php echo __('SHIPPING ADDRESS');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_address'];?></td>
							</tr>
							<tr>
								<td><?php echo __('COUNTRY');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_country'];?></td>
							</tr>
							<tr>
								<td><?php echo __('STATE');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_state'];?></td>
							</tr>
							<tr>
								<td><?php echo __('CITY');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_city'];?></td>
							</tr>
							<tr>
								<td><?php echo __('ZIP');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['shipping_zip'];?></td>
							</tr>
							<tr>
								<td><?php echo __('CONTACT NO');?></td>
								<td>&nbsp;</td>
								<td><?php echo $all_oder_details[0]['Order']['contact_no'];?></td>
							</tr>
						</table>
				   </td>
			  </tr>
			  <tr>
			  <td colspan="3">
			  <table width="100%">
				<thead>
				  <tr>
					  <th class="center"> <?php echo __('PRODUCT DESCRIPTION');?> </th>
					  <th><?php echo __('DETAILS');?> </th>
					  <th style="white-space:nowrap;" ><?php echo __('UNIT PRICE');?></th>
					  <th><?php echo __('TOTAL');?></th>
					  <th></th>
					 </tr>
				</thead>
				<tbody>
				
					<?php
					$base_url=$this->webroot;
					$total_image_path=$base_url.'uploads/Product/thumbnail/';
                     
					foreach($all_oder_details as $key=>$val ) { ?>
					
							<?php 
								if($val['ProductImage']['image'] != '' ) {
									$defaultImage =$total_image_path.''.$val['ProductImage']['image'] ;
								} else {
									$defaultImage = 'img/no_image.jpg';
								}
								
							?>
						  <tr>
								<td>
									<img src="<?php echo $defaultImage; ?>" alt="<?php //echo __($val['product_name']);?>" class="table-img" style="width:100px;height: 130px;" >
									<strong><?php echo __($val['Product']['name']);?></strong>
								</td>
							  <td>
								 
								  <?php if( isset($val['Size']['name']) && ($val['Size']['name'] != '' )) { ?>
									Size:&nbsp;&nbsp;&nbsp; <?php echo $val['Size']['name'];  ?><br>								
									<?php
									}	
								   ?>
								     Qty.&nbsp;&nbsp;&nbsp; <?php echo  $val['OrderProduct']['quantity']; ?>
								
							  </td>
								<td><span class="aud" style="white-space:nowrap;font-size: 17px;" >AUD <?php echo $val['Product']['price'];?></span></td>
								<td><span class="aud1" style="white-space:nowrap;font-size: 17px;" >AUD<span><?php echo number_format(($val['Product']['price']* $val['OrderProduct']['quantity']), 2, '.', ''); ?></span></span></td>
							  <td>&nbsp;</td> 
						  </tr>
					<?php } ?>
				  <tr>
					  <td> <?php echo __('Subtotal');?></td>
					   <td>&nbsp; </td>
					   <td>&nbsp; </td>
					   <td><span class="aud1">AUD<?php //echo  $val[0]['Order']['total'];?></span></td>
					   <td>&nbsp;</td>
				  </tr>

				  <tr>
					  <td>
						  <strong><?php echo __('YOU PAY');?></strong>
					  </td>

					  <td>&nbsp;
						 
					  </td>
					  <td>&nbsp;</td>
					  <td><span class="aud-t">AUD<?php //echo $val[0]['Order']['total'];?></span></td>
					  <td>&nbsp;</td>
				  </tr>
				  
				
				</tbody>
			</table>
				</td>
			</tr>
			</table>
		 </div>
	  </div>

	</div>
	<div style="background-image:url(); background-repeat:no-repeat; background-size:cover; height:110px;">
		<p style="color:#fff; padding-top:80px; text-align:center; font-weight:bold;"></p>
	</div>
</div>
