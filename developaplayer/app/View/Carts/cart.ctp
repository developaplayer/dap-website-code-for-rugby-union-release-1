 <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Cart</li>
                </ul>
                <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="images/cart_icon.png" alt="">
                    	<span class="cart_val">5</span>
                  	</span>
                    <span class="cart_prc">AUD 135<sup>99</sup></span>
                </div>
            </div>
		</section>
        <section class="product_listing">
            <div class="container"> 
            	<div class="cart_holder">
                	<div class="cart_row">
                    	<div class="cart_col">Product Description</div>
                        <div class="cart_col">Details</div>
                        <div class="cart_col">Unit Price</div>
                        <div class="cart_col">Total</div>
                        <div class="cart_col">&nbsp;</div>
                    </div>
                    <div class="cart_row">
                    	<div class="cart_col">
                        	<span class="prod_img"><a href="javascript:void(0);"><img src="images/prod1.png" alt=""></a></span>
                            <span class="prod_desc"><a href="javascript:void(0);">Black and Orange Kooga IPS Pro V Protection Vest</a></span>
                       	</div>
                        <div class="cart_col">
                        	<span class="bold_hd">Size</span> <span class="bold">M</span><br><br>
                            <span class="bold_hd">Qty</span> <input type="text" value="1"> 
                            <a href="javascript:void(0);" class="edit_btn">Edit</a>
                       	</div>
                        <div class="cart_col price">AUD 55<sup>99</sup></div>
                        <div class="cart_col price">AUD 55<sup>99</sup></div>
                        <div class="cart_col close_btn"><a href="javascript:void(0);">X</a></div>
                    </div>
                    <div class="cart_row">
                    	<div class="cart_col">
                        	<span class="prod_img"><a href="javascript:void(0);"><img src="images/prod2.png" alt=""></a></span>
                            <span class="prod_desc"><a href="javascript:void(0);">Black and Orange Kooga IPS Pro V Protection Vest</a></span>
                       	</div>
                        <div class="cart_col">
                        	<span class="bold_hd">Size</span> <span class="bold">M</span><br><br>
                            <span class="bold_hd">Qty</span> <input type="text" value="1"> 
                            <a href="javascript:void(0);" class="edit_btn">Edit</a>
                       	</div>
                        <div class="cart_col price">AUD 55<sup>99</sup></div>
                        <div class="cart_col price">AUD 55<sup>99</sup></div>
                        <div class="cart_col close_btn"><a href="javascript:void(0);">X</a></div>
                    </div>                    
                    <div class="cart_row">
                    	<div class="cart_col">Subtotal</div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col price">AUD 135<sup>97</sup></div>
                        <div class="cart_col"></div>
                    </div>
                    <div class="cart_row">
                    	<div class="cart_col final">You pay</div>
                        <div class="cart_col"></div>
                        <div class="cart_col"></div>
                        <div class="cart_col final price">AUD 135<sup>97</sup></div>
                        <div class="cart_col"></div>
                    </div>
                </div>
                <div class="cart_bottom">
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vspace10">Need help ?<br><span class="call">Call us on (+61 1234 5678)</span></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 vspace10 right"><input type="button" value="Checkout"></div>
                </div>
            </div>
		</section>                                                           
	</div>  