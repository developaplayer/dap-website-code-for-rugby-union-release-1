<?php
//pr($order_address24);
//==================Shipping===========================//
if(isset($order_address24['ship_full_name']))
{
	$ship_full_name=$order_address24['ship_full_name'];	
}else
{
	$ship_full_name="";		
}
	
if(isset($order_address24['ship_address']))
{
	$ship_address=$order_address24['ship_address'];	
}else
{
	$ship_address="";		
}

if(isset($order_address24['ship_city']))
{
	$ship_city=$order_address24['ship_city'];	
}else
{
	$ship_city="";		
}

if(isset($order_address24['ship_state']))
{
	$ship_state=$order_address24['ship_state'];	
}else
{
	$ship_state="";		
}

if(isset($order_address24['ship_locality']))
{
	$ship_locality=$order_address24['ship_locality'];	
}else
{
	$ship_locality="";		
}

if(isset($order_address24['contact_no']))
{
	$contact_no=$order_address24['contact_no'];	
}else
{
	$contact_no="";		
}

if(isset($order_address24['ship_zip']))
{
	$ship_zip=$order_address24['ship_zip'];	
}else
{
	$ship_zip="";		
}
//==================End===========================	
//==================Bill============================

if(isset($order_address24['bill_name']))
{
	$bill_name=$order_address24['bill_name'];	
}else
{
	$bill_name="";		
}
	
if(isset($order_address24['bill_address']))
{
	$bill_address=$order_address24['bill_address'];	
}else
{
	$bill_address="";		
}

if(isset($order_address24['bill_city']))
{
	$bill_city=$order_address24['bill_city'];	
}else
{
	$bill_city="";		
}

if(isset($order_address24['bill_state']))
{
	$bill_state=$order_address24['bill_state'];	
}else
{
	$bill_state="";		
}

if(isset($order_address24['bill_locality']))
{
	$bill_locality=$order_address24['bill_locality'];	
}else
{
	$bill_locality="";		
}

if(isset($order_address24['bill_zip']))
{
	$bill_zip=$order_address24['bill_zip'];	
}else
{
	$bill_zip="";		
}
	
?>
<div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Purchase Rugby Gears</a></li>
                    <li><span>&raquo;</span> Shipping Address</li>
                </ul>
                 <div class="topbar_cart">
                	Shopping Cart:
                	<span class="cart_ico_holder"><img src="<?php echo $this->webroot ?>images/cart_icon.png" alt="">
                    	<span class="cart_val"><?php echo CakeSession::read('total_size'); ?></span>
                  	</span>
                    <span class="cart_prc">AUD <?php echo CakeSession::read('fprice'); ?><sup><?php echo CakeSession::read('sprice'); ?></sup></span>
                </div>
            </div>
		</section>
        <section class="product_listing">
            <div class="container">
            	<!--<div class="tab_holder">
                	<ul>
                    	<li class="active">1.Details &amp; Shipping</li>
                        <li>2. Review your order</li>
                        <li>3. Pay &amp; Finish</li>
                    </ul>
                </div>-->
                <form id="shipping_form_id" name="shipping_form_id" action="<?php echo $this->webroot?>carts/shipping_address" onsubmit="return show_popup()" method="post">
                <div class="shipping_billing">                	
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<h2>Shipping Address:</h2>                        
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Full Name</label>
                                <input type="text" name="ship_full_name" id="ship_full_name" value="<?php echo $ship_full_name; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Shipping Address</label>
                                <input type="text" name="ship_address" id="ship_address" value="<?php echo $ship_address; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>City</label>
                                <input type="text" name="ship_city" id="ship_city" value="<?php echo $ship_city; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>State</label>
								   <input type="text" name="ship_state" id="ship_state" value="<?php echo $ship_state; ?>" class="form-control" placeholder="" required="">
                                <!--<select class="form-control">
                                    <option selected="">Select your State</option>
                                </select>-->
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Locality</label>
                                <input type="text" name="ship_locality" id="ship_locality"  value="<?php echo $ship_locality; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Zip</label>
                                <input type="text" name="ship_zip" id="ship_zip" value="<?php echo $ship_zip; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Contact No</label>
                                <input type="text" name="contact_no" id="contact_no" value="<?php echo $contact_no; ?>" class="form-control" placeholder="" required>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<h2>Billing Address: <span><input type="checkbox" id="ship_check" name="ship_check" > Same as Shiiping Address</span></h2>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Full Name</label>
                                <input type="text" name="bill_name" id="bill_name" value="<?php echo $bill_name; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Billing Address</label>
                                <input type="text" name="bill_address" id="bill_address" value="<?php echo $bill_address; ?>" class="form-control" placeholder="" required>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>City</label>
                                <input type="text" name="bill_city" id="bill_city" value="<?php echo $bill_city; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>State</label>
								 <input type="text" name="bill_state" id="bill_state" value="<?php echo $bill_state; ?>"  class="form-control" placeholder="" required="">
                                <!--<select class="form-control">
                                    <option selected="">Select your State</option>
                                </select>-->
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Locality</label>
                                <input type="text" name="bill_locality" id="bill_locality" value="<?php echo $bill_locality; ?>" class="form-control" placeholder="" required="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Zip</label>
                                <input type="text" name="bill_zip" id="bill_zip" value="<?php echo $bill_zip; ?>" class="form-control" placeholder="" required="">
                            </div>
                        </div>
                    </div>                    
                </div>
            	<div class="cart_bottom checkout">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 vspace10 right"><input type="submit" id="checkout" name="checkout"  value="Checkout"></div>
                </div>
				
				
				
				
				<div class="modal fade" id="ecommerce_payment_type" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Select Your Payment Type</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                    
                	<div class="modal-body">
					
					 <?php  
					 $total_price= CakeSession::read('fprice').'.'.CakeSession::read('sprice');
					 
					 if($topupPoint['PlayerDetail']['topup_point']>$total_price)
					 {
						 $attr_desabled='';
					 }else
					 {
						$attr_desabled='disabled';
						 
					 } 

					 ?>
				
                        <div class="row vspace10">
                            <div class="col-lg-12" style="font-size: 16px;">Personal Development Fund: <span style="font-weight:700">AUD <?php echo $topupPoint['PlayerDetail']['topup_point'];?></span></div>
							 <div class="col-lg-12" style="font-size: 16px;">Total Cart Price:  <span style="font-weight:700">AUD <?php echo CakeSession::read('fprice').'.'.CakeSession::read('sprice'); ?></span></div>
                        </div>
                        
                        <div class="row vspace10">
                        	<div class="col-lg-12">		  
                                <fieldset id="group1">
                 
									<input type="radio" <?php echo $attr_desabled; ?> value="personal" id="r1" name="payment_option">
									
									<label for="r1" style="font-weight:400;position:relative;left:5px;top:-2px;"> Personal Development Fund</label>
									
									<br/>
                                    <input type="radio" value="comweb" id="r2" checked="checked" name="payment_option">
									<label for="r2" style="font-weight:400;position:relative;left:5px;top:-2px;"> Comweb Payment</label>
									<!--<input type="hidden" name="player_id" id="player_id" value="<?php echo $player_id?>">
									<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id?>">
									<input type="hidden" name="course_date_id" id="course_date_id" value="<?php echo $course_date_id?>">
									<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_id?>">
									<input type="hidden" name="player_tot_points" id="player_tot_points" value="<?php echo $player_tot_points?>">
									<input type="hidden" name="player_detail_id" id="player_detail_id" value="<?php echo $player_detail_id?>">
									<input type="hidden" name="course_cost" id="course_cost" value="<?php echo $courseArray['Course']['cost']?>">-->
                                </fieldset>
                        	</div>
                        </div>
                        <div class="row vspace10">	
                        	<div class="col-lg-12"><input type="submit" id="popupClick" value="PayMent" ></div>
                        </div>
                        
                    </div>  
            	</div>
        	</div>
    	</div>
	</div>
				
				
				
                </form>
            </div>
		</section>                                                           
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
	  $( "#ship_check" ).click(function()
	  {
		  if($("#ship_check" ).is(":checked"))
		  {
				$('#bill_name').val($('#ship_full_name').val());
				$('#bill_address').val($('#ship_address').val());
				$('#bill_city').val($('#ship_city').val());
				$('#bill_state').val($('#ship_state').val());
				$('#bill_locality').val($('#ship_locality').val());
				$('#bill_zip').val($('#ship_zip').val());
		  }else         
		  {
			  
			     $('#bill_name').val("");
				$('#bill_address').val("");
				$('#bill_city').val("");
				$('#bill_state').val("");
				$('#bill_locality').val("");
				$('#bill_zip').val("");
			  
		  }	  
		
		  
		});
		
		
		
		
	})
	
	function button_click()
		{
			//alert('sdfdsf');
			window.location.href="<?php echo $this->webroot;?>/products/product_list";
			
		}
	
	function show_popup(id)
		{
			if(id==1)
			{     //
		$("#shipping_form_id").trigger("submit");
				 return true;
			}
			else
			{
				$('#ecommerce_payment_type').modal('show');
			    return false;
			}				
			
		
		}
		
		
	
	 $( "#popupClick" ).click(function()
	  {
		  //$('#shipping_form_id').submit();
		  document.getElementById('shipping_form_id').submit();
		 // alert('fdgfd');
		 // $('#ecommerce_payment_type').modal('close');
		 //alert($('#shipping_form_id').submit()) ;
		  //var show_popup = show_popup(1);
		  
		 // alert(show_popup);
		//$("form[name='shipping_form_id']").submit(function(){
			//return true;
	   
		//});
         });
		
 /*function submit_function()
 {
	$("#shipping_form_id").trigger("submit");
 } */
	
	</script>
	
	