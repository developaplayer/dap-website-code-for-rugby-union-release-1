<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#country-list{float:left;list-style:none;margin:0;padding:0;width:100px;}
#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#ccc 1px solid}
#country-list li:hover{background:#F0F0F0;}
</style>
<script>
$(document).ready(function(){

	var is_loading = false;
	var limit = 15;
	var offset = 0;
        var total = <?php echo $all_player_total ?>;
        var position_id =$("#primary_position_id").val();
	var club_id =$('#club_id').val();	
	var nationality =$('#nationality').val();
	var search_box_val =$('#search-box').val();	
		
        $('#btn_id').hide();

        if(total>=15){
	$('#btn_id').show();
	}

	$("#search-box").keyup(function(){		
		if($(this).val()!="")
		{
		  $.ajax({
		type: "POST",
		url: "<?php echo $this->webroot;?>players/auto_complate_name",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			//alert(data);
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
			if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
		}
		});
		}
	});
	
	
	$('body').click(function(e) {
		
    if (!$(e.target).closest('#suggesstion-box').length){
        $("#suggesstion-box").fadeOut('slow');
    }
	});
	
	
	 /*=============== button click ajax call ================*/
	
		$("#btn_id").click(function(){		
		
		 offset = limit + offset;  
			
			$.ajax({
				url:'<?php echo $this->webroot;?>players/player_list_ajax',
				type: 'post',
				data: {last_id:offset, limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
				success:function(result){
					$("#items").append(result);
					var success = $($.parseHTML(result)).filter("li").length;
					
					if(success<limit){
					$('#btn_id').hide();
				        }
				}
			});		
		});	

});

//=================== Position Search =============================================
	$(function() {
	$("#primary_position_id").selectmenu({
		change: function( event, ui ) {        
			var position_id = ui.item.value;
			//alert(position_id);
			var club_id =$('#club_id').val();	
			var nationality =$('#nationality').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	
	//=================== Playing Club Search =============================================
	$(function() {
	$("#club_id").selectmenu({
		change: function( event, ui ) { 
			var club_id = ui.item.value;
			//alert(club_id);
			var position_id = $('#primary_position_id').val();			
			//alert(position_id);			
			var nationality =$('#nationality').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						// set is_loading to false to accept new loading
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	//=================== Nationality Search =============================================
	$(function() {
	$("#nationality").selectmenu({
		change: function( event, ui ) { 
			var nationality = ui.item.value;
			//alert(nationality);
			var position_id = $('#primary_position_id').val();			
			//alert(position_id);			
			var club_id =$('#club_id').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						// set is_loading to false to accept new loading
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	
	
	
	function selectPlayer(val)
	{	
		//alert(val);
		$("#search-box").val(val);
		$("#suggesstion-box").hide();		
		click_auto_complt(val);
	}
//======================= Auto complete Search ===================================================
	function click_auto_complt(val)
	{
		var is_loading = false;
		var limit = 15;
		var offset = 0;
		$('#loader').hide();
		var search_box_val =val;		
		search_box_val =$('#search-box').val();				
		//alert(search_box_val);
		//var select_box_val =$("#select_value_speciality").val();	
		if (is_loading == false) { // stop loading many times for the same page
			// set is_loading to true to refuse new loading
			offset = limit + offset;			
			//alert(offset);
			is_loading = true;
			// display the waiting loader
			$('#loader').show();
			// execute an ajax query to load more statments
			$.ajax({
				url: '<?php echo $this->webroot;?>players/player_list_auto_complate',
				type: 'POST',
				//data: {last_id:offset, limit:limit,search_box:search_box_val,select_box_val:select_box_val},
				data: {last_id:offset, limit:limit,search_box:search_box_val},
				success:function(data){					
					$('#loader').hide();
					// append: add the new statements to the existing data
					$('#items').html(data);
					// set is_loading to false to accept new loading
					if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
					
					is_loading = false;
				}
			});
		}
	}


</script>
 <div class="main_container">
    <section class="innrpg_white">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Player Profiles</li>
                </ul>
            </div>
		</section>
        <section class="search_filter_holder player_listing center">
            <div class="container vspace30">
            	<div class="search_holder">
             		<input type="text" type="text" name="search_box" id="search-box" autocomplete="off" placeholder="Search By Name">
                	<div id="suggesstion-box"></div>
					<input type="button" value="submit" onclick="return click_auto_complt()">
                </div>
                <span class="filter_holder_txt">FILTER BY:</span>
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Position</option>
                        <option>Attack</option>
                        <option>Defence</option>
                        <option>Conditioning</option>
                        <option>Mindset</option>
                    </select>-->
					
					<?php
					echo $this->Form->input('PlayerDetail.primary_position_id', array('class'=>'form-control','id'=>'primary_position_id','empty'=>'Playing Position','options' => $player_position,'onclick'=>'return position(this.value)','div'=>false,'label'=>false));
					?>
                </div>
                <!--<div class="filter_holder">
                	<select>
                    	<option selected>Age Group</option>
                        <option>Group1</option>
                        <option> Group2</option>
                    </select>
                </div>-->
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Playing Club</option>
                        <option>Club1</option>
                        <option>Club2</option>
                    </select>-->
					<?php
					echo $this->Form->input('PlayerDetail.club_id', array('class'=>'form-control','id'=>'club_id','empty'=>'Playing Club','options' => $club,'div'=>false,'label'=>false));
					?>
                </div>
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Nationality</option>
                    </select>-->
					<?php
					echo $this->Form->input('PlayerDetail.nationality', array('class'=>'form-control','id'=>'nationality','empty'=>'Nationality','options' => $country,'div'=>false,'label'=>false));
					?>
                </div>
                <!--<div class="filter_holder">
                	<select>
                    	<option selected>Course Attended</option>
                    </select>
                </div>-->
            </div>
		</section> 
        <section class="player_listing">
            <div class="container vspace30">
			<ul id="items">	
			<?php //pr($all_player_details);?>
			<?php
			$last_id = 0;
			if(!empty($all_player_details))
			{
				foreach($all_player_details as $key=>$player_value)
				{
					// pr($player_value);
				?>
					<li>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 player_holder_outer">
							<div class="player_holder_inner">
								<a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>">
								<div class="player_img">
								<?php if(!empty($player_value['PlayerDetail']['profile_img']))
								{?> 
								<img src="<?php echo $this->webroot.'uploads/profiles/player/thumb/'.$player_value['PlayerDetail']['profile_img']?>" alt="<?php echo $player_value['User']['first_name']?>">
								<?php } else { ?>
								<img src="<?php echo $this->webroot; ?>images/no_images.jpg" alt="No Image">
								<?php } ?>	
									<!--<span class="level green">
										<span class="level_ico"><img src="<?php echo $this->webroot; ?>images/level_ico.png" alt=""></span>
										<span class="level_txt">Level 3</span>
									</span>-->
								</div>
								</a>
								<div class="player_holder_inner_rght">								
									<span class="player_nm"><a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>" style="text-decoration:none;"><?php echo $player_value['User']['first_name']." ".$player_value['User']['last_name']?></a></span>
									<span class="player_pos"><?php echo $player_value['PrimaryPosition']['name']?></span>
									<span class="player_country">
									<?php echo $player_value['Country']['country_name']?>
									<!--<img src="<?php echo $this->webroot; ?>images/flag.jpg" alt="">-->
									</span>
									<!--<span class="age_group">Under 19</span>-->
									<span class="age_group"><?php echo $player_value['Club']['name']?></span>
									
									<span class="team_logo"><img src="<?php echo $this->webroot; ?>uploads/club/logo/thumbnail/<?php echo $player_value['Club']['logo']?>" width="30px" height="30px" alt=""></span>                            
								</div>
							</div>
						</div>
					</li>
				<?php
				$last_id++;
				}
			}
			else
			{
			?> 
			<!--<li>						
				<div style="padding-left:392px;width:634px"><strong>No More Record Found!</strong></div>		
			</li>-->  
			<?php } ?>
			
			<script type="text/javascript">var last_id = <?php echo $last_id; ?>;</script>
			</ul>
				<div class="viewAllButtonWrapper center" style="padding-bottom:5px;">
				<button id="btn_id" class="viewAllButton">View more <i>
                    	<img title="icon_view1" alt="icon_view1" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAANCAYAAACpUE5eAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEQ1NDQ0RDAxQkZFMTFFNkFGODFCQ0ZGMDcxNUZEQzEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEQ1NDQ0RDExQkZFMTFFNkFGODFCQ0ZGMDcxNUZEQzEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4RDU0NDRDRTFCRkUxMUU2QUY4MUJDRkYwNzE1RkRDMSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4RDU0NDRDRjFCRkUxMUU2QUY4MUJDRkYwNzE1RkRDMSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnNl17sAAAEWSURBVHjaYvz//z8DDuAHxNFAbADEEkDMCMQvgfgSEK8E4lVYdYEMRMMuQPzkP2HwGoj90fWjGzYRi8bdQJwOxKlAvAWL/HxcBq7EotgNiw8sgfgHmrqt6AZOwWJYGlQuCOq9D0AcDxWLxKJ+LszAeCySH6AaNbDIWUHlHmGRy2ACxksJlrh6DqUdscjZQOmnWORyQQZuwiIhDqWPYpE7AaUlscgthYXhaSzOT4DKxQDxNyD+CY1tkFgwFvWrkSNFChrwyOAvENtiiWVDIP6CpvYItmQDMvQeFps3AXE0EEdAXYEO1uNL2IxA3P+fOAByZRy6Dxhx5GUxIE4BYlcgVgJiQaj4GyC+CcSrgXgxEP9G1wgQYAAkN5esa57ppgAAAABJRU5ErkJggg==">
                    </i>
            	</button>
			</div>			
            </div>
			
		</section>                                                            
	</section> 
       <div id="loader" style="display:none;padding-left:492px;width:658px;"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>	
    </div>    
