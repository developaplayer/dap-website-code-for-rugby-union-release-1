<?php 
//echo '<pre>';
//print_r($player_details);
//exit; ?>

<style>
.mCSB_scrollTools .mCSB_draggerRail {
        border-radius: 2px;
    height: 100%;
    margin: 0 auto;
    width: 4px;
}

</style>
<script type="text/JavaScript">

$(document).ready(function(){	
	
//$('#add_note_comment').mCustomScrollbar({ 
    // theme:"dark-3"        
 //});

	  
	});		
function video_count(count)
{	
	$('#video_count').val(count);
	$('#coustom_video').val(count);	
}

function add_video_url()
{
	var URL =$('#video_url').val();
	var USERID='<?php echo $this->Session->read('Auth.Member.id'); ?>';
	var video_count=$('#video_count').val();
	var last_id=$('#last_id'+video_count).val();
	
 $.ajax({
			
			data: {'URL':URL,'USERID':USERID,'position':video_count,'last_id':last_id},
			url:'<?php echo $this->webroot ?>players/add_player_video',
			type:'POST',
			
			success:function(result)
			{

                    //==================data================================
					var today = new Date();
					var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!

					var yyyy = today.getFullYear();
					if(dd<10){
						dd='0'+dd
					} 
					if(mm<10){
						mm='0'+mm
					} 
                    var today = dd+'/'+mm+'/'+yyyy;
                    //=======================================================					
			
			   obj = JSON.parse(result);
			   
			  
	
			   $("#video"+video_count).attr("src",obj.video_image);
			   $("#videotitle"+video_count).html(obj.video_title);
			   $("#craete_date"+video_count).html(today);			   
			  
			   $("#image_thumb"+video_count).val(obj.video_image);
			   $("#youtube_url"+video_count).val(obj.video_link);
			   $("#last_id"+video_count).val(obj.id);
			   $("#iframe_id"+video_count).html(obj.video_iframe);
			   $("#iframe_title_id"+video_count).html(obj.video_title);
			   
			   $('#video_url').val('');
			
			   $('#myModal').modal('hide');	
               window.location.reload();		   
			}
		})
}
</script>
 <div class="main_container">
    <section class="innrpg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><a href="javascript:void(0);"><span>&raquo;</span> Player Profile</a></li>
                    <li><span>&raquo;</span> <?php if(isset($player_details['User']['first_name']) && isset($player_details['User']['last_name']))   echo $player_details['User']['first_name'].' '.$player_details['User']['last_name'];?></li>
                </ul>
            </div>
		</section>
        <section>
            <div class="container vspace30">
            	<div class="profile_top_holder player_profile">	
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 profile_top_holder_lft">
					<?php
				//echo  PHYCALPATH1;
				//echo $player_details['PlayerDetail']['profile_img'];
				//exit;
					if (isset($player_details['PlayerDetail']['profile_img']) && file_exists(PHYCALPATH.'profiles/player/profile_pic/'.$player_details['PlayerDetail']['profile_img'])) 
					{
					?>
                           <div class="coach_profile_img"><img style="height:439px;" src="<?php  echo $this->webroot; ?>uploads/profiles/player/profile_pic/<?php if(count($player_details)>0 && isset($player_details['PlayerDetail']['profile_img']))  echo $player_details['PlayerDetail']['profile_img'];?>"></div>                               
                  <?php  } else { ?>
				  
                         <div class="coach_profile_img"><img style="height:439px;" src="<?php  echo $this->webroot; ?>images/no_1213images.png"></div>                               
                      <?php   } ?>
					
                           </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 profile_top_holder_rght">
                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                <h2 class="coach_heading"><?php if(isset($player_details['User']['first_name']) && isset($player_details['User']['last_name']))  echo $player_details['User']['first_name'].' '.$player_details['User']['last_name'];?></h2>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="short_desc_holder">
                                    D.O.B .: <?php if(isset($player_details['PlayerDetail']['dob']))  echo date("jS M Y", strtotime($player_details['PlayerDetail']['dob'])); ?> <br>
                                    Nationality:  <?php if(isset($player_details['Country']['country_name']))  echo $player_details['Country']['country_name'];?><br>
                                    Playing Club:  <?php if(isset($player_details['Club']['name'])) echo $player_details['Club']['name'];?>                               </div>
                            </div>
                        </div>
						
                            
							<?php 	
							//pr($player_details);							
if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P' && $this->Session->read('Auth.Member.id')==$auth_user_id) 
							{
							?>
						   <span  class="edit_profile_btn"><a href="<?php echo $this->webroot;?>players/edit_profile">Edit</a></span>
							<?php } ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 coach_details_holder">
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Primary Playing Position</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php if(isset($player_details['PrimaryPosition']['name'])) echo $player_details['PrimaryPosition']['name'];?> </div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Secondary Playing Position</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php if(isset($player_details['SecondaryPosition']['name'])) echo $player_details['SecondaryPosition']['name'];?></div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Height / Weight</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php if(isset($player_details['PlayerDetail']['height'])) echo $player_details['PlayerDetail']['height'];?> cm / <?php if(isset($player_details['PlayerDetail']['weight']))  echo $player_details['PlayerDetail']['weight'];?> Kg</div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">Bench Press / Dead Lift</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php  if(isset($player_details['PlayerDetail']['bench_press'])) echo $player_details['PlayerDetail']['bench_press'];?> Kg / <?php if(isset($player_details['PlayerDetail']['dead_lift'])) echo $player_details['PlayerDetail']['dead_lift'];?> Kg</div>
                            </div>
                            <div class="row coach_details_row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 coach_details_row_lft">20m Sprint / 60m Sprint</div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 coach_details_row_rght"><?php if(isset($player_details['PlayerDetail']['20m_sprint'])) echo $player_details['PlayerDetail']['20m_sprint'];?> s / <?php if(isset($player_details['PlayerDetail']['60m_sprint'])) echo $player_details['PlayerDetail']['60m_sprint'];?> s</div>
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
		</section>                                                 
	</section>
    <section class="graph_area">
        <div class="container">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#1" data-toggle="tab">Current Playing Profile</a>                
               	</li>
                <!--<li class="">
                    <a href="#2" data-toggle="tab">Historical tracking &amp; progression</a>                
                </li>-->
                <li class="">
                    <a href="#3" data-toggle="tab">Record of Achievement</a>                
                </li>
            </ul>
                      
          	<div class="tab-content">
                <div class="tab-pane active" id="1">
				<div id="total_div_content_extra">
                	<div class="graph_filter_holder center">
                    	<select name="select_position" id="select_position">
                        	<option value="">Select Playing Position</option>
						<?php foreach($all_player_positions as $key=>$all_player_position)
						{
						?>
                            <option value="<?php echo  $key; ?>" <?php if( isset($player_details['PlayerDetail']['primary_position_id']) && $player_details['PlayerDetail']['primary_position_id']==$key){ echo 'selected=selected';} ?>><?php echo $all_player_position; ?></option>
							
					   <?php } ?>							
                        </select>
						
                        <select name="select_group" id="select_group">
                        	<option selected>Select Age Group</option>
                           <?php foreach($AgeGroup as $key=>$AgeGroups){?>
                            <option value="<?php echo  $key; ?>" <?php if( isset($player_details['PlayerDetail']['age_group_id']) && $player_details['PlayerDetail']['age_group_id']==$key){ echo 'selected=selected';} ?>><?php echo $AgeGroups; ?></option>
                           
							<?php } ?>
                        </select>
						
                        <input type="button" value="Analyse"  onclick="get_graph_val()">						
						<input type="button" value="Current Profile"  onclick="get_current_graph_val()">
						
                    </div>
				<div id="total_div_content">
                   <div class="graph_holder">
                    	<div class="bar_outer one">
                        	<div class="bar_innr">
                            	<ul></ul>                                
                            </div>                            
                        </div>
                        <div class="bar_details one">
                        	<img src="<?php echo $this->webroot;?>images/ico_a.png" alt=""> <span class="bar_details_val">1.4</span> <img src="<?php echo $this->webroot;?>images/ico_attack_small.png" alt="">                        </div>												                            
                            
                        <div class="bar_outer two">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details two">
                        	<img src="<?php echo $this->webroot;?>images/ico_d.png" alt=""> <span class="bar_details_val">1.8</span> <img src="<?php echo $this->webroot;?>images/ico_defence_small.png" alt="">                        </div>						                            
                            
                        <div class="bar_outer three">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details three">
                        	<img src="<?php echo $this->webroot;?>images/ico_c.png" alt=""> <span class="bar_details_val">2.2</span> <img src="<?php echo $this->webroot;?>images/ico_physical_cond_small.png" alt="">                  	</div>						                            
                            
                        <div class="bar_outer four">
                        	<div class="bar_innr">
                            	<ul></ul>
                            </div>
                        </div>
                        <div class="bar_details four">
                       <img src="<?php echo $this->webroot;?>images/ico_m.png" alt=""> <span class="bar_details_val">1.7</span> <img src="<?php echo $this->webroot;?>images/ico_mental_cond_small.png" alt=""> </div>
						<div id="script_text_val">	
						<script type="text/JavaScript">
						
					
							var low_lim = 2.5;
							var up_lim = 2.9;
							
							var low_limit_val = (low_lim * 80) - 1;
							low_limit_val=parseInt(low_limit_val);
							//alert(low_limit_val);
							$('.bar_outer').css('bottom',low_limit_val+'px');
														
							var up_limit_val = (up_lim - low_lim) * 80;
							up_limit_val=parseInt(up_limit_val);
							$('.bar_innr').css('height',up_limit_val+'px');

							var cur_pos1 = 1								
							var val1 = (cur_pos1 - low_lim)*10;														
							//alert(val1);
							val1=parseInt(val1);
							//alert(val1);
							for(i=0; i<val1;i++){
								$(".bar_outer.one .bar_innr ul").append("<li>&nbsp;</li>");
							}

							var cur_pos2 = 2.6								
							var val2 = (cur_pos2 - low_lim)*10;														
							
							for(i=0; i<val2;i++){
								$(".bar_outer.two ul").append("<li>&nbsp;</li>");
							}

							var cur_pos3 = 2.6								
							var val3 = (cur_pos3 - low_lim)*10;														
							
							for(i=0; i<val3;i++){
								$(".bar_outer.three ul").append("<li>&nbsp;</li>");
							}

							var cur_pos4 = 2.6							
							var val4 = (cur_pos4 - low_lim)*10;														
							
							for(i=0; i<val4;i++){
								$(".bar_outer.four ul").append("<li>&nbsp;</li>");
							}
							
							var one = $('.bar_outer.one ul').height()+ low_limit_val - 13;
							$('.bar_details.one').css('bottom', one+'px');
							
							var two = $('.bar_outer.two ul').height()+ low_limit_val - 13;
							$('.bar_details.two').css('bottom', two+'px');
							
							var three = $('.bar_outer.three ul').height()+ low_limit_val - 13;
							$('.bar_details.three').css('bottom', three+'px');
							
							var four = $('.bar_outer.four ul').height()+ low_limit_val - 13;
							$('.bar_details.four').css('bottom', four+'px');
							
						</script>
						
					</div>                        
                    	<span class="graph_row_val eight">3.5</span>
                    	<span class="graph_row_val seven">3.0</span>
                        <span class="graph_row_val six">2.5</span>
                        <span class="graph_row_val five">2.0</span>
                        <span class="graph_row_val four">1.5</span>
                        <span class="graph_row_val three">1.0</span>
                        <span class="graph_row_val two">0.5</span>
                        <span class="graph_row_val one">0</span>
                    </div>
					
                    <div class="graph_table_holder">
                    	<div class="graph_table_row">
                        	<div class="graph_table_col">&nbsp;</div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot; ?>images/ico_attack_small.png"> Attack <img src="<?php echo $this->webroot; ?>images/ico_a.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot; ?>images/ico_defence_small.png"> Defence <img src="<?php echo $this->webroot; ?>images/ico_d.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot; ?>images/ico_physical_cond_small.png"> Conditioning <img src="<?php echo $this->webroot; ?>images/ico_c.png"></div>
                            <div class="graph_table_col"><img src="<?php echo $this->webroot; ?>images/ico_mental_cond_small.png"> Mindset <img src="<?php echo $this->webroot; ?>images/ico_m.png"></div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Upper Limit <img src="<?php echo $this->webroot; ?>images/ico_upperlimit.png" alt=""></div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                            <div class="graph_table_col">2.0</div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Current Position <img src="<?php echo $this->webroot; ?>images/ico_current_pos.png" alt=""></div>
                            <div class="graph_table_col">1.1</div>
                            <div class="graph_table_col">1.2</div>
                            <div class="graph_table_col">1.3</div>
                            <div class="graph_table_col">1.1</div>
                        </div>
                        <div class="graph_table_row">
                        	<div class="graph_table_col">Lower Limit <img src="<?php echo $this->webroot; ?>images/ico_lowerlimit.png" alt=""></div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                            <div class="graph_table_col">1.0</div>
                        </div>
                    </div>
                </div>
			 </div>	
			 </div>
                <div class="tab-pane" id="2">

                    <ul class="nav nav-tabs chart_tabs">
                        <li class="active">
                            <a href="#subtab1" data-toggle="tab">Attack</a>                
                        </li>
                        <li class="">
                            <a href="#subtab2" data-toggle="tab">Defence</a>                
                        </li>
                        <li class="">
                            <a href="#subtab3" data-toggle="tab">Conditioning</a>                
                        </li>
                        <li class="">
                            <a href="#subtab4" data-toggle="tab">Mental</a>                
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                    	
                        <script>
							<!-- Start canvas line-graph -->
							window.onload = function () {
								var chart1 = new CanvasJS.Chart("chartContainer1",
								{
								backgroundColor: "transparent",
								width: 1140,
								height: 500,
								axisX: {
									labelAngle: 90,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 14,
									gridDashType: "solid",
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1,
									labelMaxWidth: 120,
									labelWrap: true
								},
								axisY:{
									includeZero: true,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 12,
									interval:0.5,
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									gridDashType: "dash",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1
								},
								toolTip:{
									backgroundColor: "#ff6363",
									borderThickness: 0,
									cornerRadius: 5,
									fontColor: "#fff",
									fontFamily: "montserratregular",
									fontSize: 12,
									fontStyle: "normal"
								},
								data: [
								{        
									type: "line",
									color: "#ff6363",
									highlightEnabled: false,
									indexLabel: "{y}",
									indexLabelFontColor: "#fff",
									indexLabelFontSize: 13,
									indexLabelFontFamily: "montserratbold",
									markerColor: "#fff",
									markerSize: 13,
									markerBorderColor: "#ff6363",
									markerBorderThickness: 3,
									toolTipContent: "<span style='padding:15px;display:inline-block;text-align:center;'><span style='font-size:21px;color:#262626;text-decoration:underline;padding-bottom:5px;display:inline-block;letter-spacing:1px;font-family:bebas'>Score {y}</span></br>Enjoys game has fun and contributes throughout game.</span>",
									dataPoints: [
									{ label: "Assessment 01 Jan 2010", y: 1.1 },
									{ label: "Assessment 02 Nov 2010", y: 1.1 },
									{ label: "Assessment 03 Apr 2011", y: 1.2 },
									{ label: "Assessment 04 Nov 2011", y: 1.3 },
									{ label: "Assessment 05 Feb 2012", y: 1.4 },
									{ label: "Assessment 06 Oct 2012", y: 1.7 },
									{ label: "Assessment 07 Mar 2014", y: 2.1 },
									{ label: "Assessment 08 Dec 2014", y: 2.4 },
									{ label: "Assessment 09 Jan 2015", y: 2.8 },
									{ label: "Assessment 10 Nov 2015", y: 3.1 }
									]
								}]
							});		
							chart1.render();
							chart1 = {};
							
							var chart2 = new CanvasJS.Chart("chartContainer2",
								{
								backgroundColor: "transparent",
								width: 1140,
								height: 500,
								axisX: {
									labelAngle: 90,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 14,
									gridDashType: "solid",
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1,
									labelMaxWidth: 120,
									labelWrap: true
								},
								axisY:{
									includeZero: true,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 12,
									interval:0.5,
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									gridDashType: "dash",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1
								},
								toolTip:{
									backgroundColor: "#1e99ea",
									borderThickness: 0,
									cornerRadius: 5,
									fontColor: "#fff",
									fontFamily: "montserratregular",
									fontSize: 12,
									fontStyle: "normal"
								},
								data: [
								{        
									type: "line",
									color: "#1e99ea",
									highlightEnabled: false,
									indexLabel: "{y}",
									indexLabelFontColor: "#fff",
									indexLabelFontSize: 13,
									indexLabelFontFamily: "montserratbold",
									markerColor: "#fff",
									markerSize: 13,
									markerBorderColor: "#1e99ea",
									markerBorderThickness: 3,
									toolTipContent: "<span style='padding:15px;display:inline-block;text-align:center;'><span style='font-size:21px;color:#262626;text-decoration:underline;padding-bottom:5px;display:inline-block;letter-spacing:1px;font-family:bebas'>Score {y}</span></br>Enjoys game has fun and contributes throughout game.</span>",
									dataPoints: [
									{ label: "Assessment 01 Jan 2010", y: 1.1 },
									{ label: "Assessment 02 Nov 2010", y: 1.1 },
									{ label: "Assessment 03 Apr 2011", y: 1.2 },
									{ label: "Assessment 04 Nov 2011", y: 1.3 },
									{ label: "Assessment 05 Feb 2012", y: 1.4 },
									{ label: "Assessment 06 Oct 2012", y: 1.7 },
									{ label: "Assessment 07 Mar 2014", y: 2.1 },
									{ label: "Assessment 08 Dec 2014", y: 2.4 },
									{ label: "Assessment 09 Jan 2015", y: 2.8 },
									{ label: "Assessment 10 Nov 2015", y: 3.1 }
									]
								}]
							});		
							chart2.render();
							chart2 = {};
							
							var chart3 = new CanvasJS.Chart("chartContainer3",
								{
								backgroundColor: "transparent",
								width: 1140,
								height: 500,
								axisX: {
									labelAngle: 90,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 14,
									gridDashType: "solid",
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1,
									labelMaxWidth: 120,
									labelWrap: true
								},
								axisY:{
									includeZero: true,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 12,
									interval:0.5,
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									gridDashType: "dash",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1
								},
								toolTip:{
									backgroundColor: "#df7300",
									borderThickness: 0,
									cornerRadius: 5,
									fontColor: "#fff",
									fontFamily: "montserratregular",
									fontSize: 12,
									fontStyle: "normal"
								},
								data: [
								{        
									type: "line",
									color: "#df7300",
									highlightEnabled: false,
									indexLabel: "{y}",
									indexLabelFontColor: "#fff",
									indexLabelFontSize: 13,
									indexLabelFontFamily: "montserratbold",
									markerColor: "#fff",
									markerSize: 13,
									markerBorderColor: "#df7300",
									markerBorderThickness: 3,
									toolTipContent: "<span style='padding:15px;display:inline-block;text-align:center;'><span style='font-size:21px;color:#262626;text-decoration:underline;padding-bottom:5px;display:inline-block;letter-spacing:1px;font-family:bebas'>Score {y}</span></br>Enjoys game has fun and contributes throughout game.</span>",
									dataPoints: [
									{ label: "Assessment 01 Jan 2010", y: 1.1 },
									{ label: "Assessment 02 Nov 2010", y: 1.1 },
									{ label: "Assessment 03 Apr 2011", y: 1.2 },
									{ label: "Assessment 04 Nov 2011", y: 1.3 },
									{ label: "Assessment 05 Feb 2012", y: 1.4 },
									{ label: "Assessment 06 Oct 2012", y: 1.7 },
									{ label: "Assessment 07 Mar 2014", y: 2.1 },
									{ label: "Assessment 08 Dec 2014", y: 2.4 },
									{ label: "Assessment 09 Jan 2015", y: 2.8 },
									{ label: "Assessment 10 Nov 2015", y: 3.1 }
									]
								}]
							});		
							chart3.render();
							chart3 = {};
							
							var chart4 = new CanvasJS.Chart("chartContainer4",
								{
								backgroundColor: "transparent",
								width: 1140,
								height: 500,
								axisX: {
									labelAngle: 90,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 14,
									gridDashType: "solid",
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1,
									labelMaxWidth: 120,
									labelWrap: true
								},
								axisY:{
									includeZero: true,
									labelFontColor: "white",
									labelFontFamily: "montserratbold",
									labelFontSize: 12,
									interval:0.5,
									gridColor: "#2998e0",
									lineColor: "#2998e0",
									gridDashType: "dash",
									tickLength: 0,
									gridThickness: 1,
									lineThickness: 1
								},
								toolTip:{
									backgroundColor: "#1fbb82",
									borderThickness: 0,
									cornerRadius: 5,
									fontColor: "#fff",
									fontFamily: "montserratregular",
									fontSize: 12,
									fontStyle: "normal"
								},
								data: [
								{        
									type: "line",
									color: "#1fbb82",
									highlightEnabled: false,
									indexLabel: "{y}",
									indexLabelFontColor: "#fff",
									indexLabelFontSize: 13,
									indexLabelFontFamily: "montserratbold",
									markerColor: "#fff",
									markerSize: 13,
									markerBorderColor: "#1fbb82",
									markerBorderThickness: 3,
									toolTipContent: "<span style='padding:15px;display:inline-block;text-align:center;'><span style='font-size:21px;color:#262626;text-decoration:underline;padding-bottom:5px;display:inline-block;letter-spacing:1px;font-family:bebas'>Score {y}</span></br>Enjoys game has fun and contributes throughout game.</span>",
									dataPoints: [
									{ label: "Assessment 01 Jan 2010", y: 1.1 },
									{ label: "Assessment 02 Nov 2010", y: 1.1 },
									{ label: "Assessment 03 Apr 2011", y: 1.2 },
									{ label: "Assessment 04 Nov 2011", y: 1.3 },
									{ label: "Assessment 05 Feb 2012", y: 1.4 },
									{ label: "Assessment 06 Oct 2012", y: 1.7 },
									{ label: "Assessment 07 Mar 2014", y: 2.1 },
									{ label: "Assessment 08 Dec 2014", y: 2.4 },
									{ label: "Assessment 09 Jan 2015", y: 2.8 },
									{ label: "Assessment 10 Nov 2015", y: 3.1 }
									]
								}]
							});		
							chart4.render();
							chart4 = {};
							
							}
							<!-- End canvas line-graph -->
							</script>
                    
                    	<div class="tab-pane active" id="subtab1">                        	
                			<div id="chartContainer1" style="height: 500px; width: 100%;"></div>
                        </div>
                        <div class="tab-pane" id="subtab2">                        	
                			<div id="chartContainer2" style="height: 500px; width: 100%;"></div>
                        </div>
                        <div class="tab-pane" id="subtab3">
                        	<div id="chartContainer3" style="height: 500px; width: 100%;"></div>
                        </div>
                        <div class="tab-pane" id="subtab4">
                        	<div id="chartContainer4" style="height: 500px; width: 100%;"></div>
                        </div>
                    </div>
                    
                </div>
                <div class="tab-pane" id="3">
                	<div class="achievement_holder">
                    	<div class="achievement">
                        	<h2 class="achievement_hd">Attack</h2>
                        	<div class="achievement_innr red">
							
							<?php 
							//pr($all_achive_certificate_attack);
							//exit;
							if(!empty($all_achive_certificate_attack))
							{
							foreach($all_achive_certificate_attack as $all_achive_certificate_attacks)
							{
								//echo $formatted = date('S',strtotime($all_achive_certificate_attacks['AchiveCertificate']['achivement_date'])) . '</sup> ' .date('F j',strtotime($all_achive_certificate_attacks['AchiveCertificate']['achivement_date'])) . '<sup>'.date('Y',strtotime($all_achive_certificate_attacks['AchiveCertificate']['achivement_date']));
								?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Competency: Level <?php echo $all_achive_certificate_attacks['Level']['level_number'];?></span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
									<?php if($all_achive_certificate_attacks['AchiveCertificate']['achivement_date']!='0000-00-00 00:00:00'){ ?>
									 <span class="achievement_dt"><span class="glyphicon glyphicon-ok"></span> Achieved on <?php echo date('jS F Y',strtotime($all_achive_certificate_attacks['AchiveCertificate']['achivement_date']));?></span>
							<?php } else{?>
									<span class="achievement_dt">
                              <span class="glyphicon glyphicon-remove"></span>
                                    Achieved on N/a
                                      </span>
									  <div class="achievementDisable"></div>
							<?php } ?>
                                    <span class="achievement_det"><?php echo $all_achive_certificate_attacks['PlayerPosition']['name'];?></span>
                                </div>
								
							<?php
							}
							}else{ ?>
								<div class="achievement_row">
                                	<span class="achievement_level">No Achievements</span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
                                    <span class="achievement_det"></span>
                                </div>
							<?php }
							?>
                               
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Defence</h2>
                        	<div class="achievement_innr blue">
                            <?php 
							if(!empty($all_achive_certificate_defence))
							{
							foreach($all_achive_certificate_defence as $all_achive_certificate_defences)
							{
								?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Competency: Level <?php echo $all_achive_certificate_defences['Level']['level_number'];?></span>
                                   <?php if($all_achive_certificate_defences['AchiveCertificate']['status']==1){ ?>
									 <span class="achievement_dt"><span class="glyphicon glyphicon-ok"></span> Achieved on <?php echo date('jS F Y',strtotime($all_achive_certificate_defences['AchiveCertificate']['achivement_date']));?></span>
							<?php } else{?>
									<span class="achievement_dt">
                              <span class="glyphicon glyphicon-remove"></span>
                                    Achieved on N/a
                                      </span>
									  <div class="achievementDisable"></div>
							<?php } ?>
                                    <span class="achievement_det"><?php echo $all_achive_certificate_defences['PlayerPosition']['name'];?></span>
                                </div>
								
							<?php
							}
							}else{ ?>
								<div class="achievement_row">
                                	<span class="achievement_level">No Achievements</span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
                                    <span class="achievement_det"></span>
                                </div>
							<?php }
							?>
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Physical Conditioning</h2>
                        	<div class="achievement_innr orange">
                            	<?php 
							if(!empty($all_achive_certificate_phycalconditions))
							{
							foreach($all_achive_certificate_phycalconditions as $all_achive_certificate_phycalconditionses)
							{
								?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Competency: Level <?php echo $all_achive_certificate_phycalconditionses['Level']['level_number'];?></span>
                                    <?php if($all_achive_certificate_phycalconditionses['AchiveCertificate']['status']==1){ ?>
									 <span class="achievement_dt"><span class="glyphicon glyphicon-ok"></span> Achieved on <?php echo date('jS F Y',strtotime($all_achive_certificate_phycalconditionses['AchiveCertificate']['achivement_date']));?></span>
							<?php } else{?>
									<span class="achievement_dt">
                              <span class="glyphicon glyphicon-remove"></span>
                                    Achieved on N/a
                                      </span>
									  <div class="achievementDisable"></div>
							<?php } ?>
                                    <span class="achievement_det"><?php echo $all_achive_certificate_phycalconditionses['PlayerPosition']['name'];?></span>
                                </div>
								
							<?php
							}
							}else{ ?>
								<div class="achievement_row">
                                	<span class="achievement_level">No Achievements</span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
                                    <span class="achievement_det"></span>
                                </div>
							<?php }
							?>
                            </div>
                        </div>
                        <div class="achievement">
                        	<h2 class="achievement_hd">Mental Conditioning </h2>
                        	<div class="achievement_innr green">
                            	<?php 
							if(!empty($all_achive_certificate_mental))
							{
							foreach($all_achive_certificate_mental as $all_achive_certificate_mentals)
							{
								?>
                            	<div class="achievement_row">
                                	<span class="achievement_level">Competency: Level <?php echo $all_achive_certificate_mentals['Level']['level_number'];?></span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
									 <?php if($all_achive_certificate_mentals['AchiveCertificate']['status']==1){ ?>
									 <span class="achievement_dt"><span class="glyphicon glyphicon-ok"></span> Achieved on <?php echo date('jS F Y',strtotime($all_achive_certificate_mentals['AchiveCertificate']['achivement_date']));?></span>
							<?php } else{?>
									<span class="achievement_dt">
                              <span class="glyphicon glyphicon-remove"></span>
                                    Achieved on N/a
                                      </span>
									  <div class="achievementDisable"></div>
							<?php } ?>
                                    <span class="achievement_det"><?php echo $all_achive_certificate_mentals['PlayerPosition']['name'];?></span>
                                </div>
								
							<?php
							}
							}else{ ?>
								<div class="achievement_row">
                                	<span class="achievement_level">No Achievements</span>
                                    <!--<span class="achievement_dt">on 03rd Jan 2014</span>-->
                                    <span class="achievement_det"></span>
                                </div>
							<?php }
							?>
                            </div>
                        </div>
                        <div class="achievement_rght">
                        	<div class="achievement_rght_top_outer">
                            	<div class="achievement_rght_top">
                                    <div class="achievement-row">
                                        <div class="achievement-col">
										<?php if($player_details['PlayerDetail']['user_id'] == $this->Session->read('Auth.Member.id')){?>
											<input id="selectAllAchievements" onchange="checkAll()" type="checkbox">
										<?php } ?>	
										</div>
                                        <div class="achievement-col">Date</div>
                                        <div class="achievement-col">Record of Achievements</div>
                                    </div>
                            	</div>
                            </div>
                            <div class="achievement_rght_mid_outer">
                            	<div class="achievement_rght_mid">
								<?php foreach($PlayerRecoardAchivements as $PlayerRecoardAchivement){ ?>
                                    <div class="achievement-row" id="achievement<?php echo $PlayerRecoardAchivement['PlayerRecoardAchivement']['id']; ?>">
                                        <div class="achievement-col">
										<?php if($player_details['PlayerDetail']['user_id'] == $this->Session->read('Auth.Member.id')){?>
											<input class="achievements" name="achievement_id[]" value="<?php echo $PlayerRecoardAchivement['PlayerRecoardAchivement']['id']; ?>" type="checkbox">
										<?php } ?>
										</div>
										
										<?php $date = split(' ',$PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_date']); $date = split('-',$date[0]); ?>
										
                                        <div class="achievement-col">
										<?php echo date('j', strtotime($PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_date'])); ?>
										<sup>
											<?php echo date('S', strtotime($PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_date'])); ?>
										</sup>
										<?php echo date("M", strtotime($PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_date'])); ?>,
										<?php echo date("y", strtotime($PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_date'])); ?>
										</div>
                                        <div class="achievement-col"><span><?php echo $PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_title']; ?></span><br><?php echo $PlayerRecoardAchivement['PlayerRecoardAchivement']['achivement_desc']; ?></div>
                                    </div>
								<?php } ?>
                                <?php if(empty($PlayerRecoardAchivements)){ ?>
									   <div class="achievement-row" id="noAchievements">
											<div class="achievement-col" style="width:100%;padding:15px;">No Achievements</div>											
										</div>
								   <?php } ?>
                            	</div>
                            </div>
							<?php
							if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P')
			                {
								if($player_details['PlayerDetail']['user_id'] == $this->Session->read('Auth.Member.id')){?>
						
                            <div class="achievement_rght_bottom center">
                            	<a href="#" data-toggle="modal" data-target="#addAchievement" class="achievement_btn blue">Add New Achievement</a><a href="javascript:deleteAchievements()" class="achievement_btn red">Delete</a>
                            </div>
						<?php }
							}	?>
                       	</div>
                    </div>
                </div>
			</div>
        </div>
	</section>            
    <section>
        <div class="container">
        	<div class="profile_bottom_holder">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vspace30 profile_bottom_lft">
                    <h2 class="innrpg_subhd">Highlights</h2>
                    <ul>
					
					<?php for($i=1;$i<=6;$i++){?>
					
					
					  	<?php if(!empty($all_video_data)&& isset($all_video_data[$i]['video_image'])){?>
                        <li class="vid_ico">
						
								<?php  if($all_video_data[$i]['video_type']==0){ ?>	
								
								<span class="vid_ico_img">                           								    								
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>"> <img id="video<?php echo $i; ?>" src="<?php echo $all_video_data[$i]['video_image'];?>"></a>
								   <?php 					
									if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P' && $this->Session->read('Auth.Member.id')==$auth_user_id)
									{
									?>
								   <span class="upload_vid_icon">
								      <a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								    </span>
								<?php } ?>
								</span>
								
							<span class="vid_ico_nm">
                              	<a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php echo $all_video_data[$i]['video_title'];?></a>
                              	</span>	
								
   <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"><?php echo date('d/m/Y',strtotime($all_video_data[$i]['created']));?></span>
					
							  <input type="hidden" name="image_thumb<?php echo $i ?>" id="image_thumb<?php echo $i ?>" value=""/>			
						      <input type="hidden" name="youtube_url<?php echo $i ?>" id="youtube_url<?php echo $i ?>" value=""/>
						      <input type="hidden" name="last_id<?php echo $i ?>" id="last_id<?php echo $i ?>" value="<?php echo $all_video_data[$i]['id'] ?>"/>
			<!-- Start Modal for Video -->
	                             <?php } ?>
								 
			<?php  if($all_video_data[$i]['video_type']==0){ ?>			
							<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id<?php echo $i; ?>"> <?php echo $all_video_data[$i]['video_title'] ?></span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body center" id="iframe_id<?php echo $i; ?>">
										 <?php echo $all_video_data[$i]['video_iframe'] ?>
										</div>
									</div>
								</div>
							</div>
						</div>
			<?php } ?>
						<?php if($all_video_data[$i]['video_type']==1){?> 

                    	<span class="vid_ico_img">                           								    								
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>"> 
								  <video width="150" height="94">
								          <source src="<?php echo $this->webroot ?>uploads/video/player/<?php echo $all_video_data[$i]['video_link'] ?>" >							
							      </video>
							</a>
								   <?php 					
									if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P' && $this->Session->read('Auth.Member.id')==$auth_user_id)
									{
									?>
								   <span class="upload_vid_icon">
								      <a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								    </span>
								<?php } ?>
						</span>	
                            
											
						<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" style="text-align:center;" id="iframe_title_id<?php echo $i; ?>"> <?php echo $all_video_data[$i]['video_title'] ?></span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
					
										<div class="modal-body" id="iframe_id<?php echo $i; ?>">
										<video width="574" height="263" controls>
								          <source src="<?php echo $this->webroot ?>uploads/video/player/<?php echo $all_video_data[$i]['video_link'] ?>" >							
							         </video>
										</div>
									</div>
								</div>
							</div>
						</div>	
						
 <span class="vid_ico_nm"><a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php echo $all_video_data[$i]['video_title'];?></a> </span>	
 <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"><?php echo date('d/m/Y',strtotime($all_video_data[$i]['created']));?></span>
                           
					<?php
					 }
					?>
		<!-- End Modal -->					  
                         </li>							
					<?php
					    }else{								
					?>
						  <li class="vid_ico">
						
                        <span class="vid_ico_img">
                           								    								
								  <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video<?php echo $i; ?>" > <img id="video<?php echo $i; ?>" src="<?php echo $this->webroot; ?>images/no_video.jpg"></a>
								    <span class="upload_vid_icon">
									<?php 					
										if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P' && $this->Session->read('Auth.Member.id')==$auth_user_id ) 
										{
										?>
								<a href="#" data-toggle="modal" data-target="#myModal" ><img src="<?php echo $this->webroot; ?>images/video_upload_icon.png" onclick="video_count('<?php echo $i ?>')"></a>
								<?php } ?>
								</span></span>
								
							       <span class="vid_ico_nm">
                                       	<a href="javascript:void(0);" id="videotitle<?php echo $i; ?>"><?php //echo $all_video_data[$i]['video_title'];?>No Video Title</a>
                              	</span>	
                               <span class="vid_ico_dt" id="craete_date<?php echo $i; ?>"></span>
							   	</span>
                           
							  <input type="hidden" name="image_thumb<?php echo $i ?>" id="image_thumb<?php echo $i ?>" value=""/>
						     <input type="hidden" name="youtube_url<?php echo $i ?>" id="youtube_url<?php echo $i ?>" value=""/>
						     <input type="hidden" name="last_id<?php echo $i ?>" id="last_id<?php echo $i ?>" value="0"/>
							 
							 <!-- Start Modal for Video -->				  
						<div class="modal fade" id="myModal_video<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id<?php echo $i ?>">Video Title</span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body center" id="iframe_id<?php echo $i ?>">
										 No video Found !
										</div>
									</div>
								</div>
							</div>
						</div>
		<!-- End Modal -->	
                         </li>			
                           
                          <?php } ?>
                      
					
					<?php } ?>
                    </ul>
					
                    <h2 class="innrpg_subhd">Notes</h2>
					<!--====================show comment====================== -->
				<div id="add_note_comment">
			
				<?php 
					
				if($all_comment_data)
				{	
					foreach($all_comment_data as $all_comment_datas)
					{
						
						$imgclass="";
						list($width, $height, $type, $attr) = getimagesize(PHYCALPATH1.'/'.$all_comment_datas['image']);							
						//echo PHYCALPATH1;
					//echo PHYCALPATH1.''.$all_comment_datas['image'];
					//exit;
							if($width>$height)
							{
								$imgclass='resizeLandscape1';						
						    }else if($width<$height)
							{
								$imgclass='resizePortrait1';
							}else
							{
							   $imgclass='resizeSqr';
							}
					?>
			<div id="comment_any_user">
				<div class="notes_row vspace10">
                    	<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                        	<div class="coach_img"><div class="imageCenterSmall"><img class="<?php echo $imgclass; ?>" src="<?php echo $this->webroot?><?php echo $all_comment_datas['image']?>" alt=""></div></div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9">
                        	<div class="relation_to_player"><?php echo $all_comment_datas['name']?><span class="dt"><?php echo date('Y.m.d',strtotime($all_comment_datas['post_date']));?></span></div>
                            <div class="comment"><?php echo $all_comment_datas['UserComment']?></div>
                        </div>
					</div>
		</div>	
				<?php  
				   } 
				 } 
				?>
                   
				</div>
				
                    <h2 class="innrpg_subhd2">Add a note</h2>
                    <form>
                    	<div class="notes_row vspace10">
                            <textarea id="comment_id"></textarea>
                        </div>
                        <div class="notes_row vspace10">
                            <input id="insert_sub_comment"  onclick="do_comment()" type="button" value="Submit" >
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 profile_bottom_right two">
				
				<?php if(($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P' && $this->Session->read('Auth.Member.id')==$auth_user_id) || $this->Session->read('Auth.Member.type')=='C' ||  $this->Session->read('Auth.Member.type')=='F') 
				{
				?>				
                    <div class="personal_development_fund_holder center">
                    	<p>Personal Development Fund</p>
                        <h2>
						<?php //echo date('Y-m-d H:i:s ,a');?>
						AUD <?php
						if(!empty($player_details['PlayerDetail']['topup_point']))
						{
							echo $player_details['PlayerDetail']['topup_point'];
						}
						else
						{
							echo "0";
						}
						?></h2>
                        <ul class="label">
                        	<li>Managed By<br><span><?php if(isset($player_details['PlayerDetail']['manage_by'])) echo $player_details['PlayerDetail']['manage_by']; else echo '--' ?></span></li>
                            <li>Sponsored By<br><span><?php if(isset($player_details['PlayerDetail']['sponsor_by'])) echo $player_details['PlayerDetail']['sponsor_by']; else echo '--' ?></span></li>
                        </ul>
                        <ul class="bottom_links">
                        	<li><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_add_top">Add Top Up</a></li>
                            <li><a href="<?php echo $this->webroot;?>coach_courses/index">Book A Development Course</a></li>
                            <li><a href="<?php echo $this->webroot;?>products/product_list">Purchase Training Gear</a></li>
                        </ul>
                    </div>
					
				<?php } ?>
					
                  <div class="upcoming_dev_course">
                    	<h2 class="innrpg_subhd">Upcoming Development Courses</h2>
                        <ul>
                        	<li>
                            	<a href="<?php echo $this->webroot ?>coach_courses/index/1">
                                	<span class="upcoming_dev_course_img"><img src="<?php echo $this->webroot; ?>images/ico_attack.png" alt=""></span><br>
                                	<span class="upcoming_dev_course_txt">Attack</span>                                </a>                           	</li>
                            <li>
                            	<a href="<?php echo $this->webroot ?>coach_courses/index/2">
                                	<span class="upcoming_dev_course_img"><img src="<?php echo $this->webroot; ?>images/ico_defence.png" alt=""></span><br>
                                	<span class="upcoming_dev_course_txt">Defence</span>                                </a>                           	</li>
                            <li>
                            	<a href="<?php echo $this->webroot ?>coach_courses/index/4">
                                	<span class="upcoming_dev_course_img"><img src="<?php echo $this->webroot; ?>images/ico_physical_cond.png" alt=""></span><br>
                                	<span class="upcoming_dev_course_txt">Physical Conditioning</span>                                </a>                           	</li>
                            <li>
                            	<a href="<?php echo $this->webroot ?>coach_courses/index/3">
                                	<span class="upcoming_dev_course_img"><img src="<?php echo $this->webroot; ?>images/ico_mental_cond.png" alt=""></span><br>
                                	<span class="upcoming_dev_course_txt">Mental Conditioning</span>                                </a>                           	</li>
                        </ul>
                    </div>
                </div>
        	</div>
        </div>
    </section>
    </div> 
	
	<!-- Start Modal -->     
<?php 
//$this->Session->read('Auth.Member.id')==$user_id;					
 if($this->Session->read('Auth.Member.id')>0 && $this->Session->read('Auth.Member.type')=='P') 
 {
 ?>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Upload your video</span> 
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
						<form action="" method="post">
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="text" name="video_url" id="video_url" class="form-control" placeholder="Video URL" required></div>
                        </div>
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="button" name="vide_url_submit" id="vide_url_submit" value="Upload" onclick="add_video_url();"></div>
                        </div>                     
						<input type="hidden" name="video_count" id="video_count"/>
                    	</form>
						
						<?php  echo $this->Form->create('Player',array('id'=>'edit_id','controller'=>'players','action'=>'upload_video', 'enctype'=>'multipart/form-data')); ?>							
						 <div class="othersVidHolder">	
                         
                         	<div class="row vspace10">
                            	<div class="col-lg-12"><input type="text" name="video_title" id="video_title" class="form-control" placeholder="Video Title" required></div>				 
							</div>                                
						    <input type="hidden" name="member_id_video" id="member_id_video" value="<?php echo $this->Session->read('Auth.Member.id') ?>"/>	
						    <input type="hidden" name="coustom_video" id="coustom_video"/>					 
                            <div class="row vspace10">
                            	<div class="col-lg-12"><input type="file" name="own_video" accept="video/mp4,video/WebM,image/Ogg" id="own_video" required ></div>
									
                           	</div>
							
							   <div><strong>Please upload Supported video format  mp4/WebM/Ogg</strong></div>
							
                            <div class="row vspace10">
								<div class="col-lg-12"><input type="submit" name="vide_url_submit" id="video_url_submit" value="Upload Video"></div>                     		</div>
                        </div>
						<?php echo $this->Form->end(); ?>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
	
<?php } ?>

<script type="text/javascript">

function do_comment(){

    var userLoginId = '<?php echo $login_user_id; ?>';
   var comment_text = $('#comment_id').val();

    
	
	if(userLoginId>0)
	{
			if(comment_text=="")
		   {
			   return false;
			   
		   }	  

	var player_id = '<?php echo $player_details['PlayerDetail']['user_id']; ?>';	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>players/insert_comment',
		data:{
			'commentText':comment_text,'player_id':player_id
		},
		success:function(response){
			//alert(response)
			//$('#add_note_comment').prepend(response);
			$('#comment_any_user').prepend(response);
			
			$('#comment_id').val('');
			
		   
		},
		error:function(response){
			//$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
	
 }else{
	 
    alert('Please Login! and Write Your Valueable Note'); 
	 
 }
}

function get_graph_val()
{
	  var group_id = $('#select_group').val();
	  var position_id = $('#select_position').val();
	  var player_id = '<?php echo $auth_user_id; ?>';
	 
	  $('#load_icon_div').show();
	  
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>Players/get_graph_value_by_age_group',
		data:{
			
			'Group_id':group_id,'position_id':position_id,'player_id':player_id
		},
		success:function(response){
		        $('#load_icon_div').hide();
				$('#total_div_content').html(response);				
						
		},
		error:function(response){
			//$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
}

$(document).ready(function(){	
	 get_current_graph_val();
	 $( "#addAchievement" ).on('shown.bs.modal', function(){
		$('#addAchievementForm').trigger('reset');
	});
});

function get_current_graph_val()
{	
     var player_id = '<?php echo $auth_user_id; ?>';
	 var pimery_position='<?php echo $player_details['PlayerDetail']['primary_position_id']; ?>';
	 var age_group='<?php echo $player_details['PlayerDetail']['age_group_id']; ?>';
	
//alert(pimery_position);
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>Players/get_graph_value_by_current',
		data:{			
			'player_id':player_id,'position':pimery_position,'age_group':age_group
		},
		success:function(response){
		        $('#load_icon_div').hide();
				$('#total_div_content_extra').html(response);				
						
		},
		error:function(response){
			//$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});
	
	
}
function add_player_achivement()
{
	$('#add_achive_ment').val('Loading..');
	var acivement_title =$('#acivement_title').val();
	var acivement_title_desc =$('#acivement_title_desc').val();
	var datetimepicker1 =$('#achievement_date').val();
	
	$.ajax({
		type:"post",
		url:'<?php echo $this->webroot; ?>Players/insert_player_achivement',
		data:{			
			'acivement_title':acivement_title,'acivement_title_desc':acivement_title_desc,'datetimepicker1':datetimepicker1
		},
		success:function(response){
			var fortnightAway = new Date(datetimepicker1),
					date = fortnightAway.getDate(), 
					month = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sept,Oct,Nov,Dec".split(",")[fortnightAway.getMonth()];
					
		      var Total_data='<div class="achievement-row" id="achievement'+response+'"><div class="achievement-col"><input class="achievements" value="'+response+'" type="checkbox"></div><div class="achievement-col">'+date+'<sup>'+nth(date)+'</sup> '+month+', '+fortnightAway.getFullYear()+'</div> <div class="achievement-col"><span>'+acivement_title+'</span><br>'+acivement_title_desc+'</div></div>';                                
              $('.achievement_rght_mid').prepend(Total_data);
			  $('#addAchievement').modal('hide');
			$('#noAchievements').remove();
			$('#add_achive_ment').val('Submit');
		},
		error:function(response){
			$('#add_achive_ment').val('Submit');
			//$('#youtube_video_'+id).html("Error While Loading The Video..");
		}
	});                  
 
}

// Date ==================================================================================

function nth(d) {
  if(d>3 && d<21) return 'th'; // thanks kennebec
  switch (d % 10) {
		case 1:  return "st";
		case 2:  return "nd";
		case 3:  return "rd";
		default: return "th";
	}
}
// ==============================================================================================

$('#selectAllAchievements').click(function(event) {  //on click
	if(this.checked) { // check select status
		$('.achievements').each(function() { //loop through each checkbox
			this.checked = true;  //select all checkboxes with class "checkbox1"              
		});
	}else{
		$('.achievements').each(function() { //loop through each checkbox
			this.checked = false; //deselect all checkboxes with class "checkbox1"                      
		});        
	}
});
/*function checkAll(){
	if($('#selectAllAchievements:checked').length > 0){
		///alert('checked');
		$('.achievements').attr('checked',true);
		//$('.achievements').checked = true;
	}else{
		//alert('unchecked');
		$('.achievements').attr('checked',false);
		//$('.achievements').checked = false;
	}
}*/

function deleteAchievements(){
	$('.achievements:checked').each(function(){
	$('.achievement_btn.red').text('Deleting...');
		if($(this).val() != ''){
			var id = $(this).val();
		$.ajax({
			type:"post",
			url:'<?php echo $this->webroot; ?>Players/delete_player_achivement',
			data:{			
				'id':id
			},
			success:function(response){
				  $('#achievement'+id).remove();
				  $('.achievement_btn.red').text('Delete');
				  if($('.achievements').length == 0){
					  $('.achievement_rght_mid').append('<div class="achievement-row" id="noAchievements"><div class="achievement-col" style="width:100%;padding:15px;">No Achievements</div></div>');
				  }
			},
			error:function(response){
				$('.achievement_btn.red').text('Delete');
				//$('#youtube_video_'+id).html("Error While Loading The Video..");
			}
		});
		}else{
			$('.achievement_btn.red').text('Delete');
		}
	});
}

</script>

 <!-- Start Modal: Add New Achievement -->     
    <div class="modal fade" id="addAchievement" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Add New Achievement</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
						<form action="" method="post" id="addAchievementForm">
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="text" name="acivement_title" id="acivement_title" class="form-control" placeholder="Achievement Title" style="margin-bottom:0"></div>
                        </div>
                        <div class="row vspace10">
                            <div class="col-lg-12"><textarea name="acivement_title_desc" id="acivement_title_desc" class="form-control" placeholder="Add Description"></textarea></div>
                        </div>
                        <div class="row vspace10">
                        	<div class="col-lg-12">	
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="achievement_date" id="achievement_date" class="form-control" placeholder="yyyy-mm-dd" style="margin-bottom:0">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
							</div>                                
                        </div>
                        <div class="row vspace10">
                            <div class="col-lg-12"><input type="button" name="add_achive_ment" id="add_achive_ment" value="Submit" onclick="add_player_achivement();"></div>
                        </div>
                    	</form>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
    <!-- End Modal: Add New Achievement -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
    $('.close').on('click', function() {
		//alert('ggf');
   //$('iframe').attr('');
  // $("iframe").attr('src','');   
	
	var frameid  =$(this).attr('id');
	var privious_src=$('#iframe_id'+frameid).find('iframe').attr('src');
	$('#iframe_id'+frameid).find('iframe').attr('src', '');
	$('#8pm').val(privious_src);
	$('#9pm').val(frameid);
	getsrc=$('#8pm').val();
	getid=$('#9pm').val();
	
	var privious_src=$('#iframe_id'+frameid).find('iframe').attr('src',getsrc);
//	location.reload(); 
	
	//alert($('.modal-body').find('iframe').parent().parent().children().attr('id'));
	 //$('div.MenuItemSelected').next()
    // location.reload(); 
//alert($( "#iframe_id1 > span" ).html());	
//$('#kelo').contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
});

$(document).ready(function(){
	
	$("#add_note_comment").mCustomScrollbar({
								scrollButtons:{
									enable:false
								},
								advanced: {
									updateOnContentResize: Boolean
								}
							});
							
						});
						
	$('input[type=file]').change(function(){
		var file = this.files[0];
		name = file.name;
		size = file.size;
		//type = file.type;
	var type = name.split(".");
	
	if(type[1]=='mp4' || type[1]=='WebM' || type[1]=='Ogg')
	{	
		$('#video_url_submit').attr('type','submit');
	}else
	{   
        $('#video_url_submit').attr('type','button');
		$('#invalid_video_message').modal('show');
		
		 return false;
		
	}

});	

});		
	 setTimeout(function(){
        $("#add_note_comment").mCustomScrollbar("scrollTo","bottom");
    },1000);




</script>  

<input id="8pm" type="hidden"  value=""/>
<input id="9pm" type="hidden"  value=""/>

<!-- Start Modal: Add New Achievement -->     
    <div class="modal fade" id="invalid_video_message" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Message</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
				     	<p class="select_dt"><span style="color:red">invalid file format error !</span> &nbsp; <strong>Please upload video format mp4/WebM/Ogg</strong></p>
						<p></p>
						  <div class="row vspace10"></div>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
	
	<!-- Start Modal for Add Top Up -->				  
	<div class="modal fade" id="myModal_add_top" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						<span class="upload_video_hd" id="top_up_id">Put Your Amount</span>
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
					</div>
					<?php  echo $this->Form->create('Player',array('id'=>'topup_id','controller'=>'players','action'=>'add_topup', 'enctype'=>'multipart/form-data')); ?>							
					<div class="othersVidHolder addTopUp">
						<div class="row vspace10">
							<div class="col-lg-12">
							<input type="text" name="top_up_amount" id="top_up_amount" class="form-control" placeholder="Amount" required>
							</div>
						</div>  
                        <div class="row"> 
                        	<div class="col-lg-12">                         
                            <input type="hidden" name="player_id_hidd" id="player_id_hidd" value="<?php echo $auth_user_id;?>">
							</div>				 
						</div> 
						 <div class="row"> 
                        	<div class="col-lg-12">                         
                            <input type="text" name="description_topup" id="description_topup" class="form-control" placeholder="Description" required>
							</div>				 
						</div> 
						<div class="row vspace10">
							<div class="col-lg-12">
								<input type="submit" name="top_up_submit" id="top_up_submit" value="Add Top Up">
							</div>         
						</div>
					</div>
					<?php echo $this->Form->end(); ?>					
				</div>
			</div>
		</div>
	</div>
	<!-- End Modal -->
