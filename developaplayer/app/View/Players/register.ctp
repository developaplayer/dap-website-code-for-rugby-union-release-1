<!--<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>-->

 <div class="main_container login-reg-pg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Player's Registration</li>
                </ul>
            </div>
		</section>
        <section class="vspace70">
            <div class="container">
			<?php  
     echo $this->Form->create('PlayerDetail',array('id'=>'PlayerDetailRegisterForm','enctype'=>'multipart/form-data','novalidate' => true));?>
            	<div class="reg_bg">                	
                    <!--<form action="/developaplayer/players/register" method="post" enctype="multipart/form-data" id="PlayerDetailRegisterForm">-->
					<?php			   
					//echo $this->Form->create('PlayerDetail', array('enctype' => 'multipart/form-data','novalidate'=>false)); ?>


<div class="row vspace20">                    	
                		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        	<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/lock.png" alt=""></span>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        	<div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Account Information</h2>
                              	</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<?php if(isset($message)){
										echo $message;
									} ?>
								</div>
                            </div>
                            <div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<label>Email Id</label>
                                	<!--<input type="email" name="email" class="form-control" placeholder="ex: steve@gmail.com" required>-->
                            	<?php echo $this->Form->input('User.email_address',array('class'=>'form-control required','id'=>'email','type'=>'email','placeholder'=>'ex: steve@gmail.com','label' => false,'div'=>false));?>
								
								</div>
                            </div>
							<!--'pattern'=>'.{5,}',-->
                            <div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Password</label>
                                    <!--<input type="password" name="password" class="form-control" pattern=".{5,}"   required title="5 characters minimum" placeholder="Minimum 5 characters">-->									
									<?php echo $this->Form->input('User.password',array('class'=>'form-control','id'=>'password','type'=>'password','title'=>'Minimum 5 characters' ,'placeholder'=>'Minimum 5 characters','label' => false,'div'=>false));?>
									
                            	</div>
                            </div>
                            <div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<label>Confirm Password</label>
                                	<!--<input type="password" name="password" class="form-control" placeholder="Minimum 8 characters" required>-->
									<?php echo $this->Form->input('User.Confirm_password',array('class'=>'form-control','id'=>'Confirm_password','type'=>'password','pattern'=>'.{5,}','title'=>'Minimum 5 characters' ,'placeholder'=>'Confirm password','label' => false,'div'=>false));?>
								
								</div>
                            </div>                        
                        </div>                        
                	</div>
                    <div class="row vspace20">
                    	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        	<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/person.png" alt=""></span>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        	<div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Personal Information</h2>
                               	</div>
                          	</div> 
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>First Name</label>
                                    <!--<input type="text" name="fname" class="form-control" required>-->
									<?php echo $this->Form->input('User.first_name',array('class'=>'form-control required','id'=>'first_name','type'=>'text','placeholder'=>'ex: Chris','label' => false,'div'=>false));?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Last Name</label>
                                    <!--<input type="text" name="lname" class="form-control" required>-->
									<?php echo $this->Form->input('User.last_name',array('class'=>'form-control required','id'=>'last_name','type'=>'text','placeholder'=>'ex: Miles','label' => false,'div'=>false));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Date of Birth</label>
                                    <!--<input type="text" name="dob" class="form-control" placeholder="dd/mm/yyyy" required>-->
                                    
                                    <div class='input-group date' id='datetimepicker1'>
                                       <!-- <input type='text' name="dob" id="dob" class="form-control" data-date-format="YYYY-MM-DD"  autocomplete="off" placeholder="YYYY-MM-DD" required />-->										
										<?php echo $this->Form->input('PlayerDetail.dob',array('class'=>'form-control required','id'=>'dob', 'type'=>'text','placeholder'=>'YYYY-MM-DD','data-date-format'=>'YYYY-MM-DD', 'autocomplete'=>'off', 'label' => false,'div'=>false));?>										
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Nationality</label>
                                    <!--<select class="form-control">
                                        <option selected>Select Your Country</option>
                                        <option>India</option>
                                    </select>-->									
									<?php
									echo $this->Form->input('PlayerDetail.nationality', array('class'=>'form-control required','id'=>'nationality','empty'=>'Select Your Country','options' => $country,'div'=>false,'label'=>false));
									?>
									
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Upload Your Profile Photo</label>
                                    <input type="file" name="profile_img" id="profile_img" class="form-control required">
                                </div>
                            </div>
                             <?php echo "<output id='list'></output>"; ?>							
                        </div>
                    </div>
                    <div class="row vspace20">
                    	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        	<span class="reg_lft_img"><img src="<?php echo $this->webroot; ?>images/player.png" alt=""></span>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        	<div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Player Information</h2>
                             	</div>
                         	</div>
							<div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Current Playing Club</label>
                                    <!--<select class="form-control">
                                        <option selected>Select Your Playing Club</option>
                                    </select>-->
									<?php
									echo $this->Form->input('PlayerDetail.club_id', array('class'=>'form-control required','id'=>'club_id','empty'=>'Select Your Playing Club','options' => $club,'div'=>false,'label'=>false));
									?>
									
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<label>Age Group</label>
									<?php
									echo $this->Form->input('PlayerDetail.age_group_id', array('class'=>'form-control required','id'=>'id','empty'=>'Select Your Age Group','options' => $age_group,'div'=>false,'label'=>false));
									?>
								</div>
                            </div>    
                             <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Primary Playing Position</label>
                                    <!--<select class="form-control">
                                        <option selected>--Select--</option>
                                    </select>-->
									<?php
									echo $this->Form->input('PlayerDetail.primary_position_id', array('class'=>'form-control required','id'=>'primary_position_id','empty'=>'Select Your Primary Playing Position','options' => $player_position,'div'=>false,'label'=>false));
									?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Secondary Playing Position</label>
                                    <!--<select class="form-control">
                                        <option selected>--Select--</option>
                                    </select>-->					
									<?php
									echo $this->Form->input('PlayerDetail.secondary_position_id', array('class'=>'form-control required','id'=>'secondary_position_id','empty'=>'Select Your Secondary Playing Position','options' => $player_position,'div'=>false,'label'=>false));
									?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Height(cm)</label>
                                    <!--<input type="text" name="fname" class="form-control" placeholder="ex: 180 cm" required>-->
									<?php echo $this->Form->input('PlayerDetail.height',array('class'=>'form-control required','id'=>'height','type'=>'text','placeholder'=>'ex: 180 cm','label' => false,'div'=>false));?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Weight(KG)</label>
                                    <!--<input type="text" name="lname" class="form-control" placeholder="ex: 80 kg" required>-->
									<?php echo $this->Form->input('PlayerDetail.weight',array('class'=>'form-control required','id'=>'weight','type'=>'text','placeholder'=>'ex: 80 kg','label' => false,'div'=>false));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Bench Press</label>
                                    <!--<input type="text" name="fname" class="form-control" placeholder="ex: 130 kg" required>-->
									<?php echo $this->Form->input('PlayerDetail.bench_press',array('class'=>'form-control required','id'=>'bench_press','type'=>'text','placeholder'=>'ex: 80 kg','label' => false,'div'=>false));?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Dead Lift</label>
                                    <!--<input type="text" name="lname" class="form-control" placeholder="ex: 160 kg" required>-->
									<?php echo $this->Form->input('PlayerDetail.dead_lift',array('class'=>'form-control required','id'=>'dead_lift','type'=>'text','placeholder'=>'ex: 100 kg','label' => false,'div'=>false));?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>20m Sprint</label>
                                    <!--<input type="text" name="fname" class="form-control" placeholder="ex: 3 s" required>-->
									<?php echo $this->Form->input('PlayerDetail.20m_sprint',array('class'=>'form-control required','id'=>'20m_sprint','type'=>'text','placeholder'=>'ex: 3 set','label' => false,'div'=>false));?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>60m Sprint</label>
                                    <!--<input type="text" name="lname" class="form-control" placeholder="ex: 10 s" required>-->
									<?php echo $this->Form->input('PlayerDetail.60m_sprint',array('class'=>'form-control required','id'=>'60m_sprint','type'=>'text','placeholder'=>'ex: 4 set','label' => false,'div'=>false));?>
                                </div>
                            </div>
                            <div class="row vspace20">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<input name="trams_cond" type="checkbox" class="required" required> 
                                    I agree with the <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Terms &amp; Conditions</a> &amp; <a href="<?php echo $this->webroot; ?>cms_pages/terms_and_conditions" target="_blank">Privacy Policy</a> of develop a player.
                                </div>
                         	</div>
                            <div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<input type="submit" id="submit" value="Register">
                             	</div>
                         	</div>                                                                         
                        </div>
                    </div>                  
                </div>
				 <?php echo $this->Form->end(); ?>
            </div>
		</section>                                                   
	</div>
	<script type="text/javascript">
	
//=========================Image Showing=========================================//		
$(document).ready(function() 
		{
			$('#PlayerDetailRegisterForm222').validate();			
           
		});

		function handleFileSelect(evt) {
			var files = evt.target.files;
			$('.hide_shown_image').hide();

			// Loop through the FileList and render image files as thumbnails.
			for (var i = 0, f; f = files[i]; i++) {

				// Only process image files.
				if (!f.type.match('image.*')) {
					continue;
				}
				
				var reader = new FileReader();

				// Closure to capture the file information.
				reader.onload = (function(theFile) {
				return function(e) {
					// Render thumbnail.
					var span = document.createElement('span');
					span.innerHTML = 
					[
					'<div style="display:inline-block;"><img class="hide_shown_image" style="height: 75px; border: 1px solid #000; margin: 5px" src="', 
					e.target.result,
					'" title="', escape(theFile.name), 
					'"/></div>'].join('');
					document.getElementById('list').insertBefore(span, null);
					};
				})(f);

				// Read in the image file as a data URL.
				reader.readAsDataURL(f);
			}
		}

		document.getElementById('profile_img').addEventListener('change', handleFileSelect, false);
		
		
	


	</script>
	
