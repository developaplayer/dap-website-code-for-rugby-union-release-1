<style>
.frmSearch {border: 1px solid #F0F0F0;background-color:#C8EEFD;margin: 2px 0px;padding:40px;}
#country-list{float:left;list-style:none;margin:0;padding:0;width:100px;}
#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#ccc 1px solid}
#country-list li:hover{background:#F0F0F0;}
</style>
<script>
$(document).ready(function(){
	$("#search-box").keyup(function(){		
		if($(this).val()!="")
		{
		  $.ajax({
		type: "POST",
		url: "<?php echo $this->webroot;?>players/auto_complate_name",
		data:'keyword='+$(this).val(),
		beforeSend: function(){
			//$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data){
			//alert(data);
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search-box").css("background","#FFF");
			if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
		}
		});
		}
	});
	
	
	$('body').click(function(e) {
		
    if (!$(e.target).closest('#suggesstion-box').length){
        $("#suggesstion-box").fadeOut('slow');
    }
	});

});

//=================== Position Search =============================================
	$(function() {
	$("#primary_position_id").selectmenu({
		change: function( event, ui ) {        
			var position_id = ui.item.value;
			//alert(position_id);
			var club_id =$('#club_id').val();	
			var nationality =$('#nationality').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	
	//=================== Playing Club Search =============================================
	$(function() {
	$("#club_id").selectmenu({
		change: function( event, ui ) { 
			var club_id = ui.item.value;
			//alert(club_id);
			var position_id = $('#primary_position_id').val();			
			//alert(position_id);			
			var nationality =$('#nationality').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						// set is_loading to false to accept new loading
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	//=================== Nationality Search =============================================
	$(function() {
	$("#nationality").selectmenu({
		change: function( event, ui ) { 
			var nationality = ui.item.value;
			//alert(nationality);
			var position_id = $('#primary_position_id').val();			
			//alert(position_id);			
			var club_id =$('#club_id').val();
			var search_box_val =$('#search-box').val();
			var is_loading = false;
			var limit = 15
			var offset = 0;
			$('#loader').hide();
			//var select_box_val =$("#select_value_speciality").val();           
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				//offset = limit + offset;					
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_by_select',
					type: 'POST',
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					success:function(data){							
						$('#loader').hide();
						// append: add the new statements to the existing data
						$('#items').html(data);
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						// set is_loading to false to accept new loading
						is_loading = false;
					}
				});
			}		
		}
		});
	});
	
	
	
	
	function selectPlayer(val)
	{	
		//alert(val);
		$("#search-box").val(val);
		$("#suggesstion-box").hide();		
		click_auto_complt(val);
	}
//======================= Auto complete Search ===================================================
	function click_auto_complt(val)
	{
		var is_loading = false;
		var limit = 15;
		var offset = 0;
		$('#loader').hide();
		var search_box_val =val;		
		search_box_val =$('#search-box').val();				
		//alert(search_box_val);
		//var select_box_val =$("#select_value_speciality").val();	
		if (is_loading == false) { // stop loading many times for the same page
			// set is_loading to true to refuse new loading
			offset = limit + offset;			
			//alert(offset);
			is_loading = true;
			// display the waiting loader
			$('#loader').show();
			// execute an ajax query to load more statments
			$.ajax({
				url: '<?php echo $this->webroot;?>players/player_list_auto_complate',
				type: 'POST',
				//data: {last_id:offset, limit:limit,search_box:search_box_val,select_box_val:select_box_val},
				data: {last_id:offset, limit:limit,search_box:search_box_val},
				success:function(data){					
					$('#loader').hide();
					// append: add the new statements to the existing data
					$('#items').html(data);
					// set is_loading to false to accept new loading
					if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
					
					is_loading = false;
				}
			});
		}
	}
	
	
/*=====================================================================================================*/
var is_loading = false;
var limit = 15;
var offset = 0;
 $('#loader').hide();
// initialize is_loading by false to accept new loading
//svar limit = 4; // limit items per page
$(function() {
	$(window).scroll(function() {
		//var search_box_val =$('#search-box').val();
		var position_id =$("#primary_position_id").val();
		var club_id =$('#club_id').val();	
		var nationality =$('#nationality').val();
		var search_box_val =$('#search-box').val();	
		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {
			if (is_loading == false) { // stop loading many times for the same page
				// set is_loading to true to refuse new loading
				offset = limit + offset;				
				//alert(offset);
				is_loading = true;
				// display the waiting loader
				$('#loader').show();
				// execute an ajax query to load more statments
				$.ajax({
					url: '<?php echo $this->webroot;?>players/player_list_ajax',
					type: 'POST',				
					data: {last_id:offset,limit:limit,search_box:search_box_val,position:position_id,club_id:club_id,nationality:nationality},
					//data: {last_id:offset, limit:limit},
					success:function(data){	
                         // alert(data);					
						// now we have the response, so hide the loader	
						$('#loader').hide();
						// append: add the new statments to the existing data
						$('#items').append(data);
						if($.trim(data)=="")
						{
							$('#loader').show();
							$('#loader').html('<strong>No More Record Found!</strong>');
						}
						// set is_loading to false to accept new loading
						is_loading = false;
					}
				});
			}
	   }
	});
});

</script>
 <div class="main_container">
    <section class="innrpg_white">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Player Profiles</li>
                </ul>
            </div>
		</section>
        <section class="search_filter_holder player_listing center">
            <div class="container vspace30">
            	<div class="search_holder">
             		<input type="text" type="text" name="search_box" id="search-box" autocomplete="off" placeholder="Search By Name">
                	<div id="suggesstion-box"></div>
					<input type="button" value="submit" onclick="return click_auto_complt()">
                </div>
                <span class="filter_holder_txt">FILTER BY:</span>
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Position</option>
                        <option>Attack</option>
                        <option>Defence</option>
                        <option>Conditioning</option>
                        <option>Mindset</option>
                    </select>-->
					
					<?php
					echo $this->Form->input('PlayerDetail.primary_position_id', array('class'=>'form-control','id'=>'primary_position_id','empty'=>'Playing Position','options' => $player_position,'onclick'=>'return position(this.value)','div'=>false,'label'=>false));
					?>
                </div>
                <!--<div class="filter_holder">
                	<select>
                    	<option selected>Age Group</option>
                        <option>Group1</option>
                        <option> Group2</option>
                    </select>
                </div>-->
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Playing Club</option>
                        <option>Club1</option>
                        <option>Club2</option>
                    </select>-->
					<?php
					echo $this->Form->input('PlayerDetail.club_id', array('class'=>'form-control','id'=>'club_id','empty'=>'Playing Club','options' => $club,'div'=>false,'label'=>false));
					?>
                </div>
                <div class="filter_holder">
                	<!--<select>
                    	<option selected>Nationality</option>
                    </select>-->
					<?php
					echo $this->Form->input('PlayerDetail.nationality', array('class'=>'form-control','id'=>'nationality','empty'=>'Nationality','options' => $country,'div'=>false,'label'=>false));
					?>
                </div>
                <!--<div class="filter_holder">
                	<select>
                    	<option selected>Course Attended</option>
                    </select>
                </div>-->
            </div>
		</section> 
        <section class="player_listing">
            <div class="container vspace30">
			<ul id="items">	
			<?php //pr($all_player_details);?>
			<?php
			$last_id = 0;
			if(!empty($all_player_details))
			{
				foreach($all_player_details as $key=>$player_value)
				{
					// pr($player_value);
				?>
					<li>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 player_holder_outer">
							<div class="player_holder_inner">
								<a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>">
								<div class="player_img">
								<?php if(!empty($player_value['PlayerDetail']['profile_img']))
								{?> 
								<img src="<?php echo $this->webroot.'uploads/profiles/player/thumb/'.$player_value['PlayerDetail']['profile_img']?>" alt="<?php echo $player_value['User']['first_name']?>">
								<?php } else { ?>
								<img src="<?php echo $this->webroot; ?>images/no_images.jpg" alt="No Image">
								<?php } ?>	
									<!--<span class="level green">
										<span class="level_ico"><img src="<?php echo $this->webroot; ?>images/level_ico.png" alt=""></span>
										<span class="level_txt">Level 3</span>
									</span>-->
								</div>
								</a>
								<div class="player_holder_inner_rght">								
									<span class="player_nm"><a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>" style="text-decoration:none;"><?php echo $player_value['User']['first_name']." ".$player_value['User']['last_name']?></a></span>
									<span class="player_pos"><?php echo $player_value['PrimaryPosition']['name']?></span>
									<span class="player_country">
									<?php echo $player_value['Country']['country_name']?>
									<!--<img src="<?php echo $this->webroot; ?>images/flag.jpg" alt="">-->
									</span>
									<!--<span class="age_group">Under 19</span>-->
									<span class="age_group"><?php echo $player_value['Club']['name']?></span>
									
									<span class="team_logo"><img src="<?php echo $this->webroot; ?>uploads/club/logo/thumbnail/<?php echo $player_value['Club']['logo']?>" width="30px" height="30px" alt=""></span>                            
								</div>
							</div>
						</div>
					</li>
				<?php
				$last_id++;
				}
			}
			else
			{
			?> 
			<!--<li>						
				<div style="padding-left:392px;width:634px"><strong>No More Record Found!</strong></div>		
			</li>-->  
			<?php } ?>
			<script type="text/javascript">var last_id = <?php echo $last_id; ?>;</script>
			</ul>				
            </div>
			
		</section>                                                            
	</section> 
       <div id="loader" style="display:none;padding-left:492px;width:658px;"><img src="<?php echo $this->webroot;?>images/ring.gif"/></div>	
    </div>    
