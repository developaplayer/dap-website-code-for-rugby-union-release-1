<?php
	$last_id = 0;
	if(!empty($all_player_details))
	{
		foreach($all_player_details as $key=>$player_value)
		{
		?>
			<li>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 player_holder_outer">
					<div class="player_holder_inner">
						<a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>">
						<div class="player_img">
						<?php if(!empty($player_value['PlayerDetail']['profile_img']))
						{?> 
						<img src="<?php echo $this->webroot.'uploads/profiles/player/thumb/'.$player_value['PlayerDetail']['profile_img']?>" alt="<?php echo $player_value['User']['first_name']?>">
						<?php } else { ?>
						<img src="<?php echo $this->webroot; ?>images/no_images.jpg" alt="No Image">
						<?php } ?>	
							<!--<span class="level green">
								<span class="level_ico"><img src="<?php echo $this->webroot; ?>images/level_ico.png" alt=""></span>
								<span class="level_txt">Level 3</span>
							</span>-->
						</div>
						</a>
						<div class="player_holder_inner_rght">
							<span class="player_nm"><a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>" style="text-decoration:none;"><?php echo $player_value['User']['first_name']." ".$player_value['User']['last_name']?></a></span>
							<span class="player_pos"><?php echo $player_value['PrimaryPosition']['name']?></span>
							<span class="player_country">
							<?php echo $player_value['Country']['country_name']?>
							<!--<img src="<?php echo $this->webroot; ?>images/flag.jpg" alt="">-->
							</span>
							<!--<span class="age_group">Under 19</span>-->
							<span class="age_group"><?php echo $player_value['Club']['name']?></span>
							
							<span class="team_logo"><img src="<?php echo $this->webroot; ?>uploads/club/logo/thumbnail/<?php echo $player_value['Club']['logo']?>" width="30px" height="30px" alt=""></span>                            
						</div>
					</div>
				</div>
			</li>
		<?php
		$last_id++;
		}
	}

	if ($last_id != 0) {
		echo '<script type="text/javascript">var last_id = '.$last_id.';</script>';
	}
 
	// sleep for 1 second to see loader, it must be deleted in prodection
	sleep(1);
?>