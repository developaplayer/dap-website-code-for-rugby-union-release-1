<div class="main_container login-reg-pg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Player's Login</li>
                </ul>
            </div>
		</section>
        <section class="vspace70 center">
        <?php
       		$focus_class=""; 
			$error_class=""; 
			if ($this->Session->check('Message.flash'))
			{
				echo $this->Session->flash();
				$focus_class="focused";
				$error_class="error" ;
			}								
		  ?>
            <div class="container">
            	<div class="login_bg">
                	<div class="login_bg_innr">
                    	<div class="profile_ico_holder"><img src="<?php echo $this->webroot; ?>images/ico_profile.png" alt=""></div>                    
                    	<form autocomplete="off" action="" method="post">
						  <?php  echo $this->Form->create('User',array("enctype"=>"multipart/form-data",'id'=>'sign_up_id','novalidate','onSubmit'=>'return last_validation()')); ?>
                            <div class="form-group email-input <?php echo $focus_class.' '.$error_class ?>">
                                <label>Email Id</label>
                                <!--<input type="email" name="email" class="form-control" required>-->
								<?php echo $this->Form->input('User.email_address', array('type' => 'text','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group password-input <?php echo $focus_class.' '.$error_class ?>">
                                <label>Password</label>
<!--<input type="password" name="password" class="form-control" required>-->
								<?php echo $this->Form->input('User.password', array('type' => 'password','div'=>false,'label'=>false,'class'=>"form-control",'required'=>'required'));?>
                            </div>
                            <div class="form-group">
                                <span class="pull-left"><a href="<?php echo $this->webroot; ?>players/forgot_password" class="forgot_password">Forgot your password?</a></span>
                                <span class="pull-right"><input type="submit" value="Login"></span>
                            </div>
                       <?php echo $this->Form->end(); ?>
                    </div>
                </div>   
                <span class="login_bg_bottom_txt">Don't have an account? <a href="<?php echo $this->webroot;?>players/register">Sign up</a> as a player.</span>
            </div>
		</section>                                                   
	</div>    