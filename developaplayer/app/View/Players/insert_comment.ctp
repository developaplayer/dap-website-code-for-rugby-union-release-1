<?php $imgclass="";
						list($width, $height, $type, $attr) = getimagesize(PHYCALPATH1.'/'.$commentDetails['image']);							
						
							if($width>$height)
							{
								$imgclass='resizeLandscape1';						
						    }else if($width<$height)
							{
								$imgclass='resizePortrait1';
							}else
							{
							   $imgclass='resizeSqr';
							}
							?>
<div class="notes_row vspace10">
                    	<div class="col-lg-2 col-md-3 col-sm-3 col-xs-3">
                        	<div class="coach_img"><div class="imageCenterSmall"><img class="<?php echo $imgclass; ?>" src="<?php echo $this->webroot?><?php echo $commentDetails['image']?>" alt=""></div></div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9">
                        	<div class="relation_to_player"><?php echo $commentDetails['name']?><span class="dt"><?php echo date('Y.m.d',strtotime($commentDetails['post_date']));?></span></div>
                            <div class="comment"><?php echo $commentDetails['UserComment']?></div>
                        </div>
					</div>