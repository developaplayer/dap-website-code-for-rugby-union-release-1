 <div class="main_container login-reg-pg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Terms &amp; Conditions</li>
                </ul>
            </div>
		</section>
        <section class="vspace70">
		<?php 
		//pr($page_data);
		echo $page_data['Page']['description'];
		?>
            <!--<div class="container">
            	<div class="reg_bg tnc">                	                    
                    <h2 class="aboutUsTitle center">Terms &amp; Conditions</h2>
                    <p class="tnc_hd">DEVELOP A PLAYER PTY LTD</p>
                    <p class="tnc_hd">Terms and Conditions</p>
                    <ol>
                    	<li>BACKGROUND
                        	<ol>
                            	<li>You are reading a legal document which is the agreement between you, the Customer (whom we refer to as "you", "your" or the "Customer" in this document) and us. We are DEVELOP A PLAYER Pty Ltd (ACN 608 535 520) and we are the owner of this website. We are a company with our registered office in Broad beach, QLD 4218 (and we refer to ourselves as "DEVELOP A PLAYER", "we" or "us" or "our" in this document).</li>
                                <li>Please read this agreement carefully. By browsing, accessing or using this website or by using any facilities or services made available through it or by transacting through or on it, you are agreeing to the terms and conditions that appear below (all of which are called the "Agreement"). This Agreement is made between you and us.</li>
                                <li>We reserve the right to amend these terms and conditions at any time and it is your responsibility to review these terms and conditions. All amendments to these terms and conditions will be posted on-line and disclosed to you by email to the email address you Register with us. You may terminate this Agreement by written notice to us (either filling the form at DEVELOP A PLAYER.com or by post or fax) if you do not wish to be bound by such new terms and conditions. However, continued use of the Service or the Website will be deemed to constitute acceptance of the new terms and conditions.</li>
                                <li>Your statutory rights: As a consumer, nothing in this Agreement affects your non-excludable statutory rights.</li>
                            </ol>
                        </li>
                        
                        <li>DEFINITIONS
                        	<ol>
                            	<li>In this Agreement, we use various defined terms. You will know they are defined because they begin with a capital letter. This is what they mean:
                                	<ol>
                                    	<li>"Breach of Duty" is defined in clause 11.11.1.</li>
                                        <li>"Liability" is defined in clause 11.11.2.</li>
                                        <li>"Merchant" means a third party seller of goods and services.</li>
                                        <li>"Course" means training course.</li>
                                        <li>"Purchase" means the purchase of a training course.</li>
                                        <li>"Register" means "create an account on the Website" (and "Registration" means the action of creating an account).</li>
                                        <li>"Player Profile" means the online profile of the Player</li>
                                        <li>"Service" means all or any of the services provided by DEVELOP A PLAYER via the Website (or via other electronic or other communication from DEVELOP A PLAYER) including the information services, content and transaction capabilities on the Website (including the ability to make a Purchase)..</li>
                                        <li>"Website" means the DEVELOP A PLAYER.com website and any Microsite.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        
                        <li>GENERAL ISSUES ABOUT THIS WEBSITE AND THE SERVICE
                            <ol>
                                <li>Applicability of these terms and conditions: Use by you of the Service and the Website and any Purchase are each subject to the terms and conditions set out in this Agreement.</li>
                                <li>Age: To use the Service and make any Purchase of a Voucher, you must be 18 years of age or over.</li>
                                <li>Place: The Website and the Service and any Purchase are directed solely at those who access the Website from Australia. We make no representation that the Service (or any goods or services) are available or otherwise suitable for use outside of Australia. If you choose to access the Website (or use the Service or make a Purchase) from locations outside Australia, you do so on your own initiative and are responsible for the consequences and for compliance with all applicable laws.</li>
                                <li>Scope: The Website, Service and any Purchase are for your non-commercial, personal use only and must not be used for business purposes. For the avoidance of doubt, scraping of the Website (and hacking of the Website) is not allowed.</li>
                                <li>Prevention on use: We reserve the right to prevent you using the Website and the Service (or any part of them) and to prevent you from making any Purchase.</li>
                                <li>Equipment: The Service and use of the Website and the making of any Purchase does not include the provision of a computer or other necessary equipment to access the Website or the Service or to make any Purchase. To use the Website or Service or to make a Purchase, you will require Internet connectivity and appropriate telecommunication links. We shall not be liable for any telephone costs, telecommunications costs or other costs that you may incur.</li>
                            </ol>
                   		</li>
                        
                        <li>REGISTRATION AND ACCOUNTS
                            <ol>
                                <li>Why to register: You do not need to Register to use much of the functionality of the Website or to access much of the Service. However, you must Register in order to make a Purchase from the Website and to access your Player Profile. This is so that we can provide you with easy access to print your orders, view your past purchases and modify your preferences. We reserve the right to decline a new Registration or to cancel an existing Registration at any time.</li>
                                <li>How to register: To Register you need to supply us with your name, postcode, email address, payment details and possibly some other personal information. See our Privacy Policy for more details about this.</li>
                                <li>Passwords: Once you finish Registration, we will allocate one primary password and two additional passwords credentials associated with your account. You must keep all the passwords confidential and immediately notify us if any authorized third party becomes aware of that password or if there is any unauthorized use of your email address or any breach of security known to you. You agree that any person to whom your user name or password is disclosed is authorized to act as your agent for the purposes of using (and/or transacting via) the Service and Website. Please note that you are entirely responsible if you do not maintain the confidentiality of your password.</li>
                                <li>Valid email addresses: All accounts must be registered with a valid personal email address that you access regularly, so that, among other things, moderation emails can be sent to you. Any accounts which have been registered with someone else’s email address or with temporary email addresses may be closed without notice. We may require users to re-validate their accounts if we believe they have been using an invalid email address.</li>
                                <li>Closing accounts: We reserve the right to close accounts if any user is seen to be using proxy IPs (Internet Protocol addresses) in order to attempt to hide the use of multiple registration accounts, or if a non-AU user pretends to be a user, or disrupts the Website or the Service in any way.</li>
                            </ol>
                   		</li>
                        
                        <li>REFUNDS
                            <ol>
                                <li>In certain circumstances you will be entitled to a refund:
                                	<ol>
                                    	<li>The Merchant goes into liquidation – If a merchant goes into liquidation, then a refund will be provided by DEVELOP A PLAYER.</li>
                                        <li>The Merchant fails to provide the goods or services - If a merchant fails to provide the good/service, we will investigate why the merchant was unable to provide the good/service. In most instances we will try to ensure that the good/service is provided. However If a sufficient alternative cannot be found then a refund will be provided.</li>
                                        <li>The goods or services provided by the Merchant are not as advertised. – If we find that there has been a significant departure from the good/service advertised in the offer, then we will provide a refund.</li>
                                    </ol>
                                </li>
                            </ol>
                   		</li>
                        
                        <li>YOUR OBLIGATIONS
                            <ol>
                                <li>Merchant terms: Merchants will have their own applicable terms and conditions, in relation to their own supply of their goods and services, and you agree to (and shall) abide by those terms and conditions. The responsibility to do so is yours alone.</li>
                                <li>Accurate information: You warrant that all information provided on Registration and contained as part of your account during the course of this Agreement is true, complete and accurate and that you will promptly inform us of any changes to such information by updating the details in your account.</li>
                                <li>Content on the Website and Service: It is your responsibility to ensure that any products, services or information available through the Website or the Service meet your specific requirements.</li>
                                <li>Things you cannot do: Without limitation, you undertake not to use or permit anyone else to use the Service or Website:
                                	<ol>
                                    	<li>to send or receive any material which is not civil or tasteful;</li>
                                        <li>to send or receive any material which is threatening, grossly offensive, of an indecent, obscene or menacing character, blasphemous or defamatory of any person, in contempt of court or in breach of confidence, copyright, rights of personality, publicity or privacy or any other third party rights;</li>
                                        <li>to send or receive any material for which you have not obtained all necessary licenses and/or approvals (from us or third parties), or which constitutes or encourages conduct that would be considered a criminal offence, give rise to civil liability, or otherwise be contrary to the law of or infringe the rights of any third party in any country in the world;</li>
                                        <li>to send or receive any material which is technically harmful (including computer viruses, logic bombs, Trojan horses, worms, harmful components, corrupted data or other malicious software or harmful data);</li>
                                        <li>to cause annoyance, inconvenience or needless anxiety;</li>
                                        <li>to intercept or attempt to intercept any communications transmitted by way of a telecommunications system;</li>
                                        <li>for a purpose other than which we have designed them or intended them to be used;</li>
                                        <li>for any fraudulent purpose;</li>
                                        <li>other than in conformance with accepted Internet practices and practices of any connected networks; or</li>
                                        <li>in any way which is calculated to incite hatred against any ethnic, religious or any other minority or is otherwise calculated to adversely affect any individual, group or entity.</li>
                                    </ol>                                
                                </li>
                                <li>Forbidden uses: The following uses of the Service (and Website) are expressly prohibited and you undertake not to do (or to permit anyone else to do) any of the following:
                                	<ol>
                                    	<li>resale of the Service (or Website);</li>
                                        <li>furnishing false data including false names, addresses and contact details and fraudulent use of credit/debit card numbers;</li>
                                        <li>attempting to circumvent our security or network including accessing data not intended for you, logging into a server or account you are not expressly authorized to access, or probing the security of other networks (such as running a port scan);</li>
                                        <li>accessing the Service (or Website) in such a way as to, or commit any act that would or does, impose an unreasonable or disproportionately large load on our infrastructure;</li>
                                        <li>executing any form of network monitoring which will intercept data not intended for you;</li>
                                        <li>sending unsolicited mail messages, including the sending of "junk mail" or other advertising material to individuals who did not specifically request such material. You are explicitly prohibited from sending unsolicited bulk mail messages. This includes bulk mailing of commercial advertising, promotional, or informational announcements, and political or religious tracts. Such material may only be sent to those who have explicitly requested it. If a recipient asks to stop receiving email of this nature, you may not send that person any further email;</li>
                                        <li>creating or forwarding "chain letters" or other "pyramid schemes" of any type, whether or not the recipient wishes to receive such mailings;</li>
                                        <li>ending malicious email, including flooding a user or site with very large or numerous emails;</li>
                                        <li>entering into fraudulent interactions or transactions with us or a Merchant (which shall include entering into interactions or transactions purportedly on behalf of a third party where you have no authority to bind that third party or you are pretending to be a third party);</li>
                                        <li>using the Service or Website (or any relevant functionality of either of them) in breach of this Agreement;</li>
                                        <li>unauthorised use, or forging, of mail header information;</li>
                                        <li>engage in any unlawful activity in connection with the use of the Website and/or the Service or any Voucher; or</li>
                                        <li>engage in any conduct which restricts or inhibits any other customer from properly using or enjoying the Website and Service.</li>
                                    </ol>
                                </li>
                            </ol>
                   		</li>
                        
                        <li>RULES ABOUT USE OF THE SERVICE AND THE WEBSITE
                        	<ol>
                            	<li>We will use reasonable endeavours to correct any errors or omissions as soon as practicable after being notified of same. However, we do not guarantee that the Service or the Website will be free of faults (or Vouchers will be free of error) and we do not accept liability for any errors or omissions. In the event of an error or fault, you should report it by email to: technical@developaplayer.com</li>
                                <li>We do not warrant that your use of the Service or the Website will be uninterrupted and we do not warrant that any information (or messages) transmitted via the Service or the Website will be transmitted accurately, reliably, in a timely manner or at all.</li>
                                <li>We do not give any warranty that the Service or the Website is free from viruses or anything else which may have a harmful effect on any technology.</li>
                                <li>Also, although we will try to allow uninterrupted access to the Service and the Website, access to the Service and the Website may be suspended, restricted or terminated at any time.</li>
                                <li>We reserve the right to change, modify, substitute, suspend or remove without notice any information or Voucher or service on the Website or forming part of the Service from time to time. Your access to the Website and/or the Service may also be occasionally restricted to allow for repairs, maintenance or the introduction of new facilities or services. We will attempt to restore such access as soon as we reasonably can. We assume no responsibility for functionality which is dependent on your browser or other third party software to operate (including, without limitation, RSS feeds). For the avoidance of doubt, we may also withdraw any information or Voucher from the Website or Service at any time.</li>
                                <li>We reserve the right to block access to and/or to edit or remove any material which in our reasonable opinion may give rise to a breach of any of this Agreement.</li>
                            </ol>
                        </li>
                        
                        <li>SUSPENSION AND TERMINATION
                        	<ol>
                            	<li>If you use (or anyone other than you with your permission uses) the Website or Service or a Voucher in contravention of this Agreement, we may suspend your use of the Service and/or Website (in whole or in part) and/or a Voucher.</li>
                                <li>If we suspend the Service or Website or a Voucher, we may refuse to restore the Service or Website or Voucher until we receive an assurance from you, in a form we deem acceptable that there will be no further breach of the provisions of this Agreement.</li>
                                <li>DEVELOP A PLAYER Australia shall fully co-operate with any law enforcement authorities or court order requesting or directing DEVELOP A PLAYER Australia to disclose the identity or locate anyone in breach of this Agreement.</li>
                                <li>Without limitation to anything else in this Clause 9, we shall be entitled immediately or at any time (in whole or in part) to: i) suspend the Service and/or Website; ii) suspend your use of the Service and/or Website; iii) suspend the use of the Service and/or Website for persons we believe to be connected (in whatever manner) to you; and/or iv) terminate this Agreement immediately if:
                                	<ol>
                                    	<li>you commit any breach of this Agreement;</li>
                                        <li>we suspect, on reasonable grounds, that you have, might or will commit a breach of these terms; or</li>
                                        <li>we suspect, on reasonable grounds, that you may have committed or be committing any fraud against us or any other person.</li>
                                    </ol>
                                </li>
                                <li>Our rights to terminate this Agreement shall not prejudice any other right or remedy we may have in respect of any breach or any rights, obligations or liabilities accrued prior to termination.</li>
                            </ol>
                        </li>
                        
                        <li>INDEMNITY
                        	<ol>
                            	<li>You shall indemnify us against each loss, liability or cost incurred by us arising out of:
                                	<ol>
                                    	<li>any claims or legal proceedings which are brought or threatened against us by any person arising from:
                                        	<ol>
                                            	<li>your use of the Service or Website;</li>
                                                <li>the use of a Voucher;</li>
                                                <li>the use of the Service or Website through your password; or</li>
                                            </ol>
                                        </li>
                                        <li>any breach of this Agreement by you.</li>
                                    </ol>
                                </li>                                
                            </ol>
                        </li> 
                        
                        <li>STANDARDS AND LIMITATION OF LIABILITY
                        	<ol>
                            	<li>We warrant that:
                                	<ol>
                                    	<li>we will exercise reasonable care and skill in performing any obligation under this Agreement, and</li>
                                        <li>we have the right to market and advertise Partners’ products and facilitate the sale of vouchers between Partners and Customers.</li>
                                    </ol>
                                </li>
                                <li>This Clause 11 (and Clause 1.4) prevails over all other Clauses of this Agreement and sets forth our entire Liability, and your sole and exclusive remedies in respect of:
                                	<ol>
                                    	<li>the performance, non-performance, purported performance or delay in performance by us of this Agreement (including in relation to the Website or Service supplied by us); or</li>
                                        <li>otherwise in relation to this Agreement or the entering into or performance of this Agreement.</li>
                                    </ol>
                                </li>
                                <li>Nothing in this Agreement shall exclude or limit our Liability for (i) fraud; (ii) death or personal injury caused by our Breach of Duty; (iii) any breach of the statutory consumer guarantees relating to the supply of goods and/or services in Schedule 2 to the Competition and Consumer Act 2010 (Cth); or (iv) any other Liability which cannot be excluded or limited by applicable law (including, without limitation liability pursuant to Clause 1.4).</li>
                                <li>Save as provided in Clause 11.3, we do not warrant and we exclude all Liability in respect of:
                                	<ol>
                                    	<li>the accuracy, completeness, fitness for purpose or legality of any information accessed using the Service or Website or otherwise; and</li>
                                        <li>the transmission or the reception of or the failure to transmit or to receive any material of whatever nature; and</li>
                                        <li>your use of any information or materials on the Website (which is entirely at your own risk and it is your responsibility);</li>
                                        <li>Voucher Products for which Vouchers may be redeemed and in respect of the quality, safety, usability or any other aspect of the goods or services in respect of which is Voucher may be redeemed by a Merchant).</li>
                                    </ol>
                                </li>
                                <li>Save as provided in Clause 11.3 but subject to Clause 11.6, we do not accept and hereby exclude any Liability for loss of or damage to your (or any person's) tangible property other than that caused by our Breach of Duty.</li>
                                <li>Save as provided in Clause 11.3 but subject to Clauses 11.4.3 and 11.8, our Liability for loss of or damage to your (or another person's) tangible property caused by us, our employees, subcontractors or agents acting within the course of their employment during the performance of this Agreement, shall not exceed AU$10. Neither corruption of data nor loss of data shall constitute physical damage to property for the purposes of this Clause 11.6.</li>
                                <li>Save as provided in Clauses 11.3 and 11.4.3, we do not accept and hereby exclude any Liability for Breach of Duty other than any such Liability arising pursuant to the terms of this Agreement.</li>
                                <li>Save as provided in Clause 11.3, we shall have no Liability for:
                                	<ol>
                                    	<li>loss of revenue;</li>
                                        <li>loss of actual or anticipated profits;</li>
                                        <li>loss of contracts;</li>
                                        <li>loss of the use of money;</li>
                                        <li>loss of anticipated savings;</li>
                                        <li>loss of business;</li>
                                        <li>loss of opportunity;</li>
                                        <li>loss of goodwill;</li>
                                        <li>loss of reputation;</li>
                                        <li>loss of, damage to or corruption of data; or</li>
                                        <li>any indirect or consequential loss; and such Liability is excluded whether it is foreseeable, known, foreseen or otherwise. For the avoidance of doubt, Clauses 11.8.1 to 11.8.10 apply whether such losses are direct, indirect, consequential or otherwise.</li>
                                    </ol>
                                </li>
                                <li>Save as provided in Clause 11.3, our total Liability to you or any third party shall in no circumstances exceed, in aggregate, a sum equal to the greater of: a) AU$50; or b) 110% of any aggregate amount paid by you to us in the 12 months preceding any cause of action arising.</li>
                                <li>The limitation of Liability under Clause 11.9 has effect in relation both to any Liability expressly provided for under this Agreement and to any Liability arising by reason of the invalidity or unenforceability of any clause of this Agreement.</li>
                                <li>In this Clause 11:
                                	<ol>
                                    	<li>"Liability" means liability in or for breach of contract, Breach of Duty,  restitution or any other cause of action whatsoever relating to or arising under or in connection with this Agreement, including, without limitation, liability expressly provided for under this Agreement or arising by reason of the invalidity or unenforceability of any clause of this Agreement; and</li>
                                        <li>"Breach of Duty" means the breach of any (i) obligation arising from the express or implied terms of a contract to take reasonable care or exercise reasonable skill in the performance of the contract or (ii) common law duty to take reasonable care or exercise reasonable skill (but not any stricter duty).</li>
                                    </ol>
                                </li>                                                                
                            </ol>
                        </li> 
                        
                        <li>Privacy 
                            <ol>
                                <li>Please see our Privacy Policy which is incorporated into and forms part of this Agreement</li>
                            </ol>
                        </li>
                                
                        <li>ADVERTISEMENTS 
                            <ol>
                                <li>We may place advertisements in different locations on the Website and at different points during your use of the Service. These locations and points may change from time to time. We will always clearly mark distinguish third party advertisements for goods and services from the goods and/or services which are the subject of a Voucher and will be supplied by a Merchant when a Voucher purchased by you is redeemed..</li>
                                <li>You are free to select or click on advertised goods and services or not as you see fit.</li>
                                <li>Any advertisements may be delivered on our behalf by a third party advertising company.</li>
                                <li>No personal data (for example your name, address, email address or telephone number) will be used if you click on any advertising links on the Website on promotional emails. However, on our behalf, a third-party advertiser or affiliate may place or recognise a unique "cookie" on your browser.</li>
                            </ol>
                        </li>
                              
                        <li>LINKS TO AND FROM OTHER WEBSITES 
                            <ol>
                                <li>Where the Website contains links to third party sites and to resources provided by third parties (together "Other Sites"), those Other Sites are merely linked to provide information only and are solely for your convenience. We have no control over and do not accept and we assume no responsibility for Other Sites or for the content or products or services of Other Sites (including, without limitation, relating to social networking sites such as Facebook) and we accept no responsibility for any loss or damage that may arise from your use of them. If you decide to access any of the third party websites linked to the website, you do so entirely at your own risk.</li>
                                <li>This DEVELOP A PLAYER.com website may make available access to Microsites and if it does, it may do so within or otherwise through external hyperlinks.</li>
                            </ol>
                        </li>
                        
                        <li>INTELLECTUAL PROPERTY RIGHTS 
                            <ol>
                                <li>All intellectual property rights (including the various rights conferred by statute, common law and equity in and in relation to copyright, patents, trade marks, service marks, trade names and/or designs (including the "look and feel" and other visual or non-literal elements)) (whether registered or unregistered) in (a) the Website and Service, (b) (subject to Clause 15.4) information content on the Website or accessed as part of the Service, (c) any database operated by us and (d) all the website design, text and graphics, software, photos, videos, music, sounds, and their selection and arrangement, and all software compilations, underlying source code and software (including applets and scripts) are owned by us or licensed to us. You shall not, and shall not attempt to, obtain any title to any such intellectual property rights. All rights are reserved.</li>
                                <li>None of the material listed in Clause 15.1 may be reproduced or redistributed or copied, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, sold, rented or sub-licensed, used to create derivative works, or in any way exploited without our prior express written permission. You may, however, retrieve and display the content of the Website on a computer screen (including any tablet or smartphone device), store such content in electronic form on disk (but not on any server or other storage device connected to a network) or print one copy of such content for your own personal, non-commercial use, provided you keep intact all and any copyright and proprietary notices. You may not otherwise reproduce, modify, copy or distribute or use for commercial purposes any of the materials or content on the Website or accessed as part of the Service without our permission.</li>
                                <li>All rights (including goodwill and, where relevant, trade marks) in the DEVELOP A PLAYER Australia name are owned by us or licensed to us. Other product and company names mentioned on the Website are the trade marks or registered trade marks of third parties.</li>
                                <li>Title, ownership rights and intellectual property rights in and to the content accessed using the Service is the property of the applicable content owner or Merchant and may be protected by applicable copyright or other law. Subject to the limited rights described in Clause 15.2, this Agreement gives you no rights to such content.</li>
                                <li>The authors of the literary and artistic works in the pages in the Website have asserted their moral rights to be identified as the author of those works.</li>
                                <li>Any material you transmit, post or submit to us either through the Website or otherwise ("Material") shall be considered (and we may treat it as) non-confidential (subject to our obligations under data protection legislation). You grant us a royalty-free, perpetual, irrevocable, non-exclusive licence to use, copy, modify, adapt, translate, publish and distribute world-wide any Material (other than Ideas).</li>
                                <li>All comments, suggestions, ideas, notes, drawings or concepts in which intellectual property rights subsist: (i) disclosed or offered to us by you; or (ii) in response to solicitations by us regarding the Service or the Website; (in each foregoing case, these are called "Ideas") shall be deemed to be and shall remain our property and you hereby assign all existing present and future intellectual property rights in Ideas, to us. You must do all things reasonably requested by us to assure further the assignment of such rights. You understand and acknowledge that we have both internal resources and other external resources which may have developed or may in the future develop ideas identical to or similar to Ideas and that we are only willing to consider Ideas on these terms. In any event, any Ideas are not submitted in confidence and we assume no obligation, express or implied by considering it. Without limitation, we shall exclusively own all now known or hereafter existing rights to the Ideas of every kind and nature throughout the world and shall be entitled to unrestricted use of the Ideas for any purpose whatsoever, commercial or otherwise without compensation to the provider of the Ideas.</li>
                            </ol>
                        </li>
                        
                        <li>GENERAL
                            <ol>
                                <li>Interpretation: In this Agreement:
                                	<ol>
                                    	<li>words denoting persons includes natural persons, partnerships, limited liability partnerships, bodies corporate and unincorporated associations of persons;</li>
                                        <li>clause headings such as ("16. GENERAL" at the start of this Clause) and clause titles (such as "Interpretation:" at the start of this Clause 16.1) are purely for ease of reference and do not form part of or affect the interpretation of this Agreement; and</li>
                                        <li>references to "include" and "including" shall be deemed to mean respectively "include(s) without limitation" and "including without limitation".</li>
                                    </ol>
                                </li>
                                <li>No partnership/agency: Nothing in this Agreement shall be construed to create a joint venture, partnership or agency relationship between you and us and neither party shall have the right or authority to incur any liability debt or cost or enter into any contracts or other arrangements in the name of or on behalf of the other.</li>
                                <li>No other terms: Except as expressly stated in this Agreement, all warranties, conditions and other terms, whether express or implied, by statute, common law or otherwise are hereby excluded to the fullest extent permitted by law.</li>
                                <li>Assignment: You may not assign or delegate or otherwise deal with all or any of your rights or obligations under this Agreement. We shall have the right to assign or otherwise delegate all or any of our rights or obligations under this Agreement to any person.</li>
                                <li>Force majeure: We shall not be liable for any breach of our obligations under this Agreement where we are hindered or prevented from carrying out our obligations by any cause outside our reasonable control, including by lightning, fire, flood, extremely severe weather, strike, lock-out, labour dispute, act of God, war, riot, civil commotion, malicious damage, failure of any telecommunications or computer system, compliance with any law, accident (or by any damage caused by any of such events).</li>
                                <li>Entire agreement: This Agreement (and our Privacy Policy) contains all the terms agreed between the parties regarding its subject matter and supersedes and excludes any prior agreement, understanding or arrangement between the parties, whether oral or in writing. No representation, undertaking or promise shall be taken to have been given or be implied from anything said or written in negotiations between the parties prior to this Agreement except as expressly stated in this Agreement. However, the Service is provided to you under our operating rules, policies, and procedures as published from time to time on the Website.</li>
                                <li>No waiver: No waiver by us of any default of yours under this Agreement shall operate or be construed as a waiver by us of any future defaults, whether or a like or different character. No granting of time or other forbearance or indulgence by us to you shall in any way release, discharge or otherwise affect your liability under this Agreement.</li>
                                <li>Notices: Unless otherwise stated within this Agreement, notices to be given to either party shall be in writing and shall be delivered by hand, electronic mail (other than, if you are sending a notice to us for the purpose of legal process) sent by fax or by pre-paid post, to you at the address you supplied to us or to us at our registered office.</li>
                                <li>Third party rights: All provisions of this Agreement apply equally to and are for the benefit of DEVELOP A PLAYER Australia, its subsidiaries, any holding companies of DEVELOP A PLAYER Australia, its (or their) affiliates and its (or their) third party content providers and licensors and each shall have the right to assert and enforce such provisions directly or on its own behalf (save that this Agreement may be varied or rescinded without the consent of those parties).</li>
                                <li>Survival: In any event, the provisions of Clauses 1, 2, 5.7, 5.8, 5.9, 5.10, 5.11, 5.12, 7.1, 10, 11, 15 and 16 of this Agreement, together with those provisions that either are expressed to survive its expiry or termination or from their nature or context it is contemplated that they are to survive such termination, shall survive termination of the Agreement. In the event you use the Website or Service again, then the provisions of the terms and conditions that then apply will govern your re-use of the Website or Service. In the event you use Vouchers bought under this Agreement, then those provisions applicable to Vouchers will survive termination of this Agreement.</li>
                                <li>Severability: If any provision of this Agreement is held to be unlawful, invalid or unenforceable, that provision shall be deemed severed and where capable the validity and enforceability of the remaining provisions of this agreement shall not be affected.</li>
                                <li>Governing law: This Agreement (and all non-contractual relationships between you and us) shall be governed by and construed in accordance with the law of New South Wales and both parties hereby submit to the exclusive jurisdiction of the courts of New South Wales.</li>
                            </ol>
                        </li> 
                        
                        <li>MISCELLANEOUS
                        	<ol>
                            	<li>The Website and the Service is owned and operated by DEVELOP A PLAYER Australia Pty Ltd a company registered in Australia whose registered office is in Gold Coast, QLD4218.</li>
                            </ol>
                        </li>                                                
                    </ol>
                    
                    <h2 class="aboutUsTitle center">Privacy Policy</h2>
                    <ol>
                    	<li>STATEMENT
                        	<ol>
                            	<li>Privacy Statement (Privacy Statement) explains how Develop A Player Pty Ltd (ACN 608 535 520) a company registered in Australia with our registered office in Gold Coast, QLD (“Develop A Player”, us, our, and we), collects, processes and uses information about you. We are the operator of this website, and a provider of a range of services thereto. We provide a platform where we may list offers for local services, goods and travel which are made available by us or other sellers (collectively: Sellers). This Privacy Statement applies to information we collect through our website, mobile application, electronic communications or services or other websites we offer and that link to or specifically reference the applicability of this Privacy Statement (collectively, the Site). Throughout this document, we will refer to us and our affiliates as the Develop A Player Group.</li>
                                <li>We will routinely update this Privacy Statement to clarify our practices and to reflect new or different privacy practices, such as when we add new services, functionality or features to the Site. If we make any material changes we will notify you, either by email (sent to the email address specified in your account), by means of notice on the Site or otherwise. You can determine when this version of the Privacy Statement was adopted by referring to the Effective Date above.</li>
                            </ol>
                        </li>
                        <li>Situations in which We Collect Information
                        	<ol>
                            	<li>We may collect and retain the following information from and about you if you interact with us through the Site:
                                	<ol>
                                    	<li>your email address and Site password;</li>
                                        <li>your payment details, billing and delivery addresses;</li>
                                        <li>your phone numbers ;</li>
                                        <li>your location, browser type and operating system, website browsing patterns (e.g., through cookies) and purchase history;</li>
                                        <li>other information you actively submit to us or we can determine about you based on your interactions with our Site and services.</li>
                                    </ol>
                                </li>
                                <li>You may interact with us, for example, if you:
                                	<ol>
                                    	<li>register, subscribe, authorize the transfer of, or create an account with us;</li>
                                        <li>open or respond to emails;</li>
                                        <li>provide information to enroll or participate in programs provided on behalf of, or together with other Sellers, merchants, co-marketers, distributors, resellers and other business partners, with your consent or as necessary to provide services you have requested;</li>
                                        <li>visit any page online that displays our ads or content;</li>
                                        <li>purchase products or services on or through the Site;</li>
                                        <li>interact or connect with or link to the Site via integrated social networking tools; and</li>
                                        <li>post comments to the the Site.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>Your Choices
                        	<ol>
                            	<li>You can manage the types of personal data you provide to us and can limit how we communicate with you.</li>
                                <li>You may manage how your browser handles cookies by adjusting its privacy and security settings. Browsers are different, so refer to instructions related to your browser to learn about cookie-related and other privacy and security settings that may be available. You may also manage how your mobile device and mobile browser share information on and about your devices with us, as well as how your mobile browser handles cookies by adjusting the privacy and security settings on your mobile device. Please refer to instructions provided by your mobile service provider and the manufacturer of your device to learn how to adjust your settings.</li>
                                <li>You may also manage the sharing of certain personal data with us when you connect with us through social networking platforms or applications. Please refer to Section 9 below and also the privacy policy and settings of the social networking website or application to determine how you may adjust our permissions and manage the interactivity between us and your social networking account or your mobile device.</li>
                                <li>If you register for customized email newsletters containing offers for local services, goods and travel, we will work to make information more relevant for you and customize newsletters based on information you share with us, your location, website browsing preferences (for example, through cookies), purchase history or based on other attributes of your relationship with us. You can reject and delete cookies and unsubscribe from newsletters at any time by clicking links in each newsletter you wish to unsubscribe from.</li>
                            </ol>
                        </li>
                        <li>How We Use Information
                        	<ol>
                            	<li>We process personal data for the following purposes:
                                	<ol>
                                    	<li>Operate, maintain and improve the Site;</li>
                                        <li>Provide you with personalized direct marketing initiatives via email SMS and direct marketing offers;</li>
                                        <li>Facilitate and process orders - for example, for training and other goods and services;</li>
                                        <li>Determine your eligibility for certain types of offers, products or services that may be of interest to you, and analyze advertising effectiveness;</li>
                                        <li>Answer your questions and respond to your requests;</li>
                                        <li>To establish and analyze individual and group profiles and customer behavior</li>
                                        <li>Communicate and provide additional information that may be of interest to you about us, the Sellers and our business partners;</li>
                                        <li>Send you reminders, technical notices, updates, security alerts, support and administrative messages, service bulletins, marketing messages, and requested information, including on behalf of business partners;</li>
                                        <li>Administer rewards, surveys, sweepstakes, contests, or other promotional activities or events;</li>
                                        <li>Manage our everyday business needs, such as administration of the Site, forum management, fulfillment, analytics, fraud prevention, and enforcement of our corporate reporting obligations and Terms of Use or to comply with the law;</li>
                                        <li>Comply with our legal obligations, resolve disputes, and enforce our agreements:</li>
                                        <li>Allow you to apply for a job, post a video or sign up for special offers from merchants and other business partners through the Site, and to</li>
                                        <li>Enhance other information we have about you to help us better understand you, determine your interests and provide you with more relevant and compelling services</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        <li>When and Why We Disclose Information
                        	<ol>
                            	<li>We are not in the business of selling or renting personal data. We will not share your personal data, except as follows:
                                	<ol>
                                    	<li>with affiliated and unaffiliated service providers within and outside of Australia, including, without limitation, Develop A Player Global, to provide services for us, subject to strict contractual obligations to protect your personal data and process it on our behalf and under our control only;</li>
                                        <li>with the Sellers, so they can sell and deliver to you;</li>
                                        <li>to report or collect on debts owed to Sellers, merchants or other business partners;</li>
                                        <li>as necessary to perform contractual obligations towards you with business partners to the extent you have purchased or redeemed a Develop A Player voucher, goods or services offered by a business partner or participated in an offer, rewards, contest or other activity or program sponsored or offered through us or the Sellers on behalf of a business partner;</li>
                                        <li>in case of a merger, acquisition or reorganization with a purchaser of our company or all or substantially all of our assets;</li>
                                        <li>to comply with legal orders and government requests, or as needed to support auditing, compliance, and corporate governance functions;</li>
                                        <li>to combat fraud or criminal activity, and to protect our rights or those of our affiliates, business partners and users, or as part of legal proceedings affecting the develop A Player Group;</li>
                                        <li>in response to a subpoena, or similar legal process, including to law enforcement agencies, regulators, and courts, or</li>
                                        <li>with your consent or as otherwise required or permitted by law.</li>
                                    </ol>
                                </li>
                                <li>We encourage all parties that we share your personal data with to adopt and post privacy policies. However, while we share personal data only for the above-mentioned purposes, the subsequent processing and use of personal data obtained through the Develop A Player Group is governed by own privacy policies and practices and not subject to our control. You acknowledge that you are aware that the overseas recipient of your personal data may not be bound by laws which provide the same level of protection for personal data which is provided by the Australian Privacy Principles and agree we will not be responsible for any breach of privacy by the recipient of your personal data.</li>
                            </ol>
                        </li>
                        <li>How We Collect Information
                        	<ol>
                            	<li>You provide us with personal data when you register, subscribe, create an account, make a purchase or redeem vouchers or otherwise contact us or communicate with us actively. For example, you provide personal data to us when you contact us online for customer service and other support using self-help tools, such as email, text, or by posting to online communities.</li>
                                <li>We also receive personal data and other online and offline information from our business partners who may report to us redemption, collection, or refund-return events. We will use such information in accordance with applicable laws and this Privacy Statement.</li>
                            </ol>
                        </li>
                        <li>Security of Personal Data
                        	<ol>
                            	<li>We have implemented an information security program that contains administrative, technical and physical controls that are designed to safeguard your personal data, including industry-standard encryption technology.</li>
                            </ol>
                        </li>
                        <li>Your Rights in relation to Personal Data; Contact Us
                        	<ol>
                            	<li>You can access, update, rectify, and delete your information you provided to us in your profile by logging into your account or sending us an email at contact@developaplayer.comKeeping your personal data current helps ensure that we can respect your preferences and offer you the goods and services that are most relevant to you.</li>
                                <li>In accordance with applicable law, you may (i) request access to any other personal data we hold about you and request that it be updated, rectified, deleted or blocked, (ii) request that we refrain from further use of any personal data we hold about you, and (iii) object to our creation of user profiles about you. If you have any questions or comments about our privacy practices or this Privacy Statement, if you want to make use of any of the above rights, or other rights that you may have in relation to your personal data if you wish to close your Develop A Player account or if you have other questions or requests, please email us at contact@developaplayer.com.</li>
                                <li>While we are ready to assist you in managing your subscriptions, closing your account, and removing your active profile, we cannot always delete records of past interactions and transactions. For example, we are required to retain records relating to previous purchases on the Site for financial reporting and compliance reasons.</li>
                            </ol>
                        </li>
                        <li>Retention of Personal Data
                        	<ol>
                            	<li>We will retain your personal data for as long as your account is active or as needed to provide you services and to maintain a record of your transactions for financial reporting purposes. We will also retain your personal data as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements.</li>
                            </ol>
                        </li>
                        <li>Social Site Features
                        	<ol>
                            	<li>The Site may contain connections to areas where you may be able to publicly post information, communicate with others such as discussion boards or blogs, review products and merchants, and otherwise submit your own original content. Prior to posting in these areas, please read our Terms of Conditions carefully. All the information you post may be accessible to anyone with Internet access, and any information you include in your posting may be read, collected, and used by others. For example, if you post your email address along with a public restaurant review, you may receive unsolicited messages from other parties.</li>
                            </ol>
                        </li>
                        <li>Connecting through Social Networks
                        	<ol>
                            	<li>We offer social networking users the opportunity to interact with friends and to share on social networks. If you are logged into both the Site and a social network, when you use the Site's social networking connection functions, you may connect your social network account with your Develop A Player account (this happens automatically, if the email addresses match). If the email addresses do not match, we ask you if you want to link them and you must validate that you control the accounts. If you are already logged into the Site but not logged into your social network site, when you use the Site's social network connection functions, you will be prompted to enter your social network website credentials or to sign up for the social network.</li>
                                <li>If you are not currently registered as a Develop A Player user and you use the Site's social network connection functions, you will first be asked to enter your social network credentials and then be given the option to register and join Develop A Player. Once you register with us and connect with the social network, you will be able to automatically post recent Develop A Player activity back to your social network. Please refer to the privacy settings in your social network account to manage the data that is shared through your account.</li>
                                <li>By using the Site's social network connection function, you will grant us permission to access all of the elements of your social network profile information that you have made available to be shared (as per the settings chosen by you in your social network profile) and to use it in accordance with the social network's terms of use and this Privacy Statement.</li>
                            </ol>
                        </li>
                        <li>Privacy Practices of Third Parties
                        	<ol>
                            	<li>This Privacy Statement only addresses the collection, processing and use (including disclosure) of information by us through your interaction with the Site. Other websites that may be accessible through links from the Site may have their own privacy statements and personal information collection, processing, use, and disclosure practices. Our business partners may also have their own privacy statements. We encourage you to familiarize yourself with the privacystatements provided by these other parties prior to providing them with information or taking advantage of a sponsored offer or promotion.</li>
                            </ol>
                        </li>
                    </ol>
                    
                    <h2 class="aboutUsTitle center">Returns Policy</h2>
                    <ol>
                    	<li>Time Limit: Develop A Player carries no responsibility for training or merchandis being outdated by new laws or government changes. There is a 1 month period (from the time of purchase) that merchandise under the fair trade agreemnt</li>
                        <li>Change of Mind: We are unable to offer a refund for a change of mind.</li>
                        <li>Faulty or items received in error: If you have received a faulty item or received an item in error, we are happy to replace, exchange or refund the cost of the item for you. We will pay the costs of postage and handling if you return a faulty or unsuitable item or an item received in error. If you choose to exchange or replace the item we will pay the costs of postage and handling of the replacement item. Please contact us before returning any items.</li>
                        <li>Damaged Items: All products are carefully packed to endure reasonable handling through the delivery process. We are not responsible for damage resulting from abnormal or extreme delivery conditions.</li>
                        <li>Refunds: Refunds are usually processed within 14 business days. All refunds must be issued to the original purchaser. Refunds may incur a 20% restocking/handling fee. All refunds must be issued to the original purchaser.</li>
                    </ol>
                    <p>Dispatch and Deliveries Policy:<br><br>
All Develop A Player products and services are delivered via Australia Post Regular Post service. Postage will be calculated at checkout; an additional charge will be incurred for buyers requesting express delivery. We will endeavour to have your order in the post within 30 working days of placing it. Customised covers require a longer period to prepare so please allow up to 30 working days for goods to be delivered. Please ensure that you provide a valid delivery address. Where a valid address is not provided the purchaser will bear the expense to have the goods re-delivered.
</p>
                    <p>Ordering Policy:<br><br>
All orders taken by telephone must be paid for by credit card (Mastercard or Visa) when placing the order. All orders made using a purchase order system must be in writing (email, or post) and have a valid purchase order number. Payment for goods may be made by credit card, funds transfer. All prices are subject to change without notice.
</p>
            	</div>
            </div>
		--></section>                                                   
</div>   