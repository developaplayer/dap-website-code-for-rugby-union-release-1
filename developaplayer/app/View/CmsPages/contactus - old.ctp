<div class="inner-page-wrap">
    <div class="container clearfix">
        <div class="row">
            <h1 class="page-title">Drop Us a Line</h1>

        </div>
        <div class="row dis-tbl">
            <div class="dis-tbl-cel contact-left-sec">
                <!-- contact form starts -->
				
			
			
			
                <div class="contact_form">
                    <?php	
      $Contact="";
					
			echo $this->Form->create("Contact", array('url'=>array('controller' =>'cmspages', 'action' => 'contactus'),'enctype'=>'multipart/form-data' ,'id'=>'cntctfrm'));
		 ?>
                        <div class="contact_row clearfix">
						
						<?php
				if ($this->Session->check('Message.flash')) {
					echo $this->Session->flash();
				}
				
			?>	
                            <div class="form-group">
                              
								 <?php echo $this->Form->input('first_name', array('type' => 'text', 'div'=>false,'label'=>false, 'class'=>'form-control','id'=>'cnname','placeholder'=>'First Name','required'=>'true'));?>
                                <i class="icon person_icon"><img src="images/sprites/small_person_icon.png" alt=""/></i>
                            </div>
							<div class="form-group">
                              
								 <?php echo $this->Form->input('last_name', array('type' => 'text', 'div'=>false,'label'=>false, 'class'=>'form-control','id'=>'cnname','placeholder'=>'Last Name','required'=>'true'));?>
                                <i class="icon person_icon"><img src="images/sprites/small_person_icon.png" alt=""/></i>
                            </div>
                        </div>
                        <div class="contact_row clearfix">
                            <div class="form-group">
                               
								<?php echo $this->Form->input('email', array('type' => 'email','div'=>false,'label'=>false, 'class'=>'form-control','id'=>'email','placeholder'=>'Email','required'=>'true'));?>
                                <i class="icon small_mail_icon"><img src="images/sprites/small_mail_icon.png" alt=""/></i>
                            </div>
                        </div>
                        <div class="contact_row clearfix">
                            <div class="form-group">
                                	<?php echo $this->Form->input('phone', array('type' => 'number','div'=>false,'label'=>false, 'class'=>'form-control','id'=>'phone','placeholder'=>'Phone','required'=>'true'));?>
                                <i class="icon small_phone_icon"><img src="images/sprites/small_phone_icon.png" alt=""/></i>
                            </div>
                        </div>
                        <div class="contact_row clearfix">
  							<?php echo $this->Form->input('message', array('type' => 'textarea','div'=>false,'label'=>false,'id'=>'coments', 'class'=>'form-control', 'placeholder'=>'Comments','required'=>'true'));?>
							
                        </div>
                        <div class="contact_row clearfix">
                            <input type="submit" value="Send" class="contact-sub">
                        </div>

                  <?php echo $this->Form->end(); ?>
		
                </div>
                <!-- contact form ends -->
            </div>
            <div class="dis-tbl-cel contact-right-sec">
                <div class="contact-info">
                    <div class="email">
                        <p><i class="icon"><img alt="" src="images/email_icon.png"></i><a href="#"><?php echo $ContactDetail['ContactDetail']['email']; ?></a></p>
                    </div>
                    <div class="phone">
                        <p><i class="icon"><img alt="" src="images/phone_icon.png"></i><?php echo $ContactDetail['ContactDetail']['mobile']; ?></p>
                    </div>
                    
                    <div class="address">
                        <p><i class="icon"><img alt="" src="images/address_icon.png"></i><?php echo $ContactDetail['ContactDetail']['address']; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

