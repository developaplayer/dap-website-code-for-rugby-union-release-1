 <div class="main_container login-reg-pg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Terms &amp; Conditions</li>
                </ul>
            </div>
		</section>
        <section class="vspace70">
            <div class="container">
            	<div class="reg_bg tnc">                	                    
                    <h2 class="aboutUsTitle center">Terms &amp; Conditions</h2>
                    <p class="tnc_hd">DEVELOP A PLAYER PTY LTD</p>
                    <p class="tnc_hd">Terms and Conditions</p>
                    <ol>
                    	<li>BACKGROUND
                        	<ol>
                            	<li>You are reading a legal document which is the agreement between you, the Customer (whom we refer to as "you", "your" or the "Customer" in this document) and us. We are DEVELOP A PLAYER Pty Ltd (ACN 608 535 520) and we are the owner of this website. We are a company with our registered office in Broad beach, QLD 4218 (and we refer to ourselves as "DEVELOP A PLAYER", "we" or "us" or "our" in this document).</li>
                                <li>Please read this agreement carefully. By browsing, accessing or using this website or by using any facilities or services made available through it or by transacting through or on it, you are agreeing to the terms and conditions that appear below (all of which are called the "Agreement"). This Agreement is made between you and us.</li>
                                <li>We reserve the right to amend these terms and conditions at any time and it is your responsibility to review these terms and conditions. All amendments to these terms and conditions will be posted on-line and disclosed to you by email to the email address you Register with us. You may terminate this Agreement by written notice to us (either filling the form at DEVELOP A PLAYER.com or by post or fax) if you do not wish to be bound by such new terms and conditions. However, continued use of the Service or the Website will be deemed to constitute acceptance of the new terms and conditions.</li>
                                <li>Your statutory rights: As a consumer, nothing in this Agreement affects your non-excludable statutory rights.</li>
                            </ol>
                        </li>
                        
                        <li>DEFINITIONS
                        	<ol>
                            	<li>In this Agreement, we use various defined terms. You will know they are defined because they begin with a capital letter. This is what they mean:
                                	<ol>
                                    	<li>"Breach of Duty" is defined in clause 11.11.1.</li>
                                        <li>"Liability" is defined in clause 11.11.2.</li>
                                        <li>"Merchant" means a third party seller of goods and services.</li>
                                        <li>"Course" means training course.</li>
                                        <li>"Purchase" means the purchase of a training course.</li>
                                        <li>"Register" means "create an account on the Website" (and "Registration" means the action of creating an account).</li>
                                        <li>"Player Profile" means the online profile of the Player</li>
                                        <li>"Service" means all or any of the services provided by DEVELOP A PLAYER via the Website (or via other electronic or other communication from DEVELOP A PLAYER) including the information services, content and transaction capabilities on the Website (including the ability to make a Purchase)..</li>
                                        <li>"Website" means the DEVELOP A PLAYER.com website and any Microsite.</li>
                                    </ol>
                                </li>
                            </ol>
                        </li>
                        
                        <li>GENERAL ISSUES ABOUT THIS WEBSITE AND THE SERVICE
                            <ol>
                                <li>Applicability of these terms and conditions: Use by you of the Service and the Website and any Purchase are each subject to the terms and conditions set out in this Agreement.</li>
                                <li>Age: To use the Service and make any Purchase of a Voucher, you must be 18 years of age or over.</li>
                                <li>Place: The Website and the Service and any Purchase are directed solely at those who access the Website from Australia. We make no representation that the Service (or any goods or services) are available or otherwise suitable for use outside of Australia. If you choose to access the Website (or use the Service or make a Purchase) from locations outside Australia, you do so on your own initiative and are responsible for the consequences and for compliance with all applicable laws.</li>
                                <li>Scope: The Website, Service and any Purchase are for your non-commercial, personal use only and must not be used for business purposes. For the avoidance of doubt, scraping of the Website (and hacking of the Website) is not allowed.</li>
                                <li>Prevention on use: We reserve the right to prevent you using the Website and the Service (or any part of them) and to prevent you from making any Purchase.</li>
                                <li>Equipment: The Service and use of the Website and the making of any Purchase does not include the provision of a computer or other necessary equipment to access the Website or the Service or to make any Purchase. To use the Website or Service or to make a Purchase, you will require Internet connectivity and appropriate telecommunication links. We shall not be liable for any telephone costs, telecommunications costs or other costs that you may incur.</li>
                            </ol>
                   		</li>
                        
                        <li>REGISTRATION AND ACCOUNTS
                            <ol>
                                <li>Why to register: You do not need to Register to use much of the functionality of the Website or to access much of the Service. However, you must Register in order to make a Purchase from the Website and to access your Player Profile. This is so that we can provide you with easy access to print your orders, view your past purchases and modify your preferences. We reserve the right to decline a new Registration or to cancel an existing Registration at any time.</li>
                                <li>How to register: To Register you need to supply us with your name, postcode, email address, payment details and possibly some other personal information. See our Privacy Policy for more details about this.</li>
                                <li>Passwords: Once you finish Registration, we will allocate one primary password and two additional passwords credentials associated with your account. You must keep all the passwords confidential and immediately notify us if any authorized third party becomes aware of that password or if there is any unauthorized use of your email address or any breach of security known to you. You agree that any person to whom your user name or password is disclosed is authorized to act as your agent for the purposes of using (and/or transacting via) the Service and Website. Please note that you are entirely responsible if you do not maintain the confidentiality of your password.</li>
                                <li>Valid email addresses: All accounts must be registered with a valid personal email address that you access regularly, so that, among other things, moderation emails can be sent to you. Any accounts which have been registered with someone else’s email address or with temporary email addresses may be closed without notice. We may require users to re-validate their accounts if we believe they have been using an invalid email address.</li>
                                <li>Closing accounts: We reserve the right to close accounts if any user is seen to be using proxy IPs (Internet Protocol addresses) in order to attempt to hide the use of multiple registration accounts, or if a non-AU user pretends to be a user, or disrupts the Website or the Service in any way.</li>
                            </ol>
                   		</li>
                        
                        <li>REFUNDS
                            <ol>
                                <li>In certain circumstances you will be entitled to a refund:
                                	<ol>
                                    	<li>The Merchant goes into liquidation – If a merchant goes into liquidation, then a refund will be provided by DEVELOP A PLAYER.</li>
                                        <li>The Merchant fails to provide the goods or services - If a merchant fails to provide the good/service, we will investigate why the merchant was unable to provide the good/service. In most instances we will try to ensure that the good/service is provided. However If a sufficient alternative cannot be found then a refund will be provided.</li>
                                        <li>The goods or services provided by the Merchant are not as advertised. – If we find that there has been a significant departure from the good/service advertised in the offer, then we will provide a refund.</li>
                                    </ol>
                                </li>
                            </ol>
                   		</li>
                        
                        <li>YOUR OBLIGATIONS
                            <ol>
                                <li>Merchant terms: Merchants will have their own applicable terms and conditions, in relation to their own supply of their goods and services, and you agree to (and shall) abide by those terms and conditions. The responsibility to do so is yours alone.</li>
                                <li>Accurate information: You warrant that all information provided on Registration and contained as part of your account during the course of this Agreement is true, complete and accurate and that you will promptly inform us of any changes to such information by updating the details in your account.</li>
                                <li>Content on the Website and Service: It is your responsibility to ensure that any products, services or information available through the Website or the Service meet your specific requirements.</li>
                                <li>Things you cannot do: Without limitation, you undertake not to use or permit anyone else to use the Service or Website:
                                	<ol>
                                    	<li>to send or receive any material which is not civil or tasteful;</li>
                                        <li>to send or receive any material which is threatening, grossly offensive, of an indecent, obscene or menacing character, blasphemous or defamatory of any person, in contempt of court or in breach of confidence, copyright, rights of personality, publicity or privacy or any other third party rights;</li>
                                        <li>to send or receive any material for which you have not obtained all necessary licenses and/or approvals (from us or third parties), or which constitutes or encourages conduct that would be considered a criminal offence, give rise to civil liability, or otherwise be contrary to the law of or infringe the rights of any third party in any country in the world;</li>
                                        <li>to send or receive any material which is technically harmful (including computer viruses, logic bombs, Trojan horses, worms, harmful components, corrupted data or other malicious software or harmful data);</li>
                                        <li>to cause annoyance, inconvenience or needless anxiety;</li>
                                        <li>to intercept or attempt to intercept any communications transmitted by way of a telecommunications system;</li>
                                        <li>for a purpose other than which we have designed them or intended them to be used;</li>
                                        <li>for any fraudulent purpose;</li>
                                        <li>other than in conformance with accepted Internet practices and practices of any connected networks; or</li>
                                        <li>in any way which is calculated to incite hatred against any ethnic, religious or any other minority or is otherwise calculated to adversely affect any individual, group or entity.</li>
                                    </ol>                                
                                </li>
                                <li>Forbidden uses: The following uses of the Service (and Website) are expressly prohibited and you undertake not to do (or to permit anyone else to do) any of the following:
                                	<ol>
                                    	<li>resale of the Service (or Website);</li>
                                        <li>furnishing false data including false names, addresses and contact details and fraudulent use of credit/debit card numbers;</li>
                                        <li>attempting to circumvent our security or network including accessing data not intended for you, logging into a server or account you are not expressly authorized to access, or probing the security of other networks (such as running a port scan);</li>
                                        <li>accessing the Service (or Website) in such a way as to, or commit any act that would or does, impose an unreasonable or disproportionately large load on our infrastructure;</li>
                                        <li>executing any form of network monitoring which will intercept data not intended for you;</li>
                                        <li>sending unsolicited mail messages, including the sending of "junk mail" or other advertising material to individuals who did not specifically request such material. You are explicitly prohibited from sending unsolicited bulk mail messages. This includes bulk mailing of commercial advertising, promotional, or informational announcements, and political or religious tracts. Such material may only be sent to those who have explicitly requested it. If a recipient asks to stop receiving email of this nature, you may not send that person any further email;</li>
                                        <li>creating or forwarding "chain letters" or other "pyramid schemes" of any type, whether or not the recipient wishes to receive such mailings;</li>
                                        <li>ending malicious email, including flooding a user or site with very large or numerous emails;</li>
                                        <li>entering into fraudulent interactions or transactions with us or a Merchant (which shall include entering into interactions or transactions purportedly on behalf of a third party where you have no authority to bind that third party or you are pretending to be a third party);</li>
                                        <li>using the Service or Website (or any relevant functionality of either of them) in breach of this Agreement;</li>
                                        <li>unauthorised use, or forging, of mail header information;</li>
                                        <li>engage in any unlawful activity in connection with the use of the Website and/or the Service or any Voucher; or</li>
                                        <li>engage in any conduct which restricts or inhibits any other customer from properly using or enjoying the Website and Service.</li>
                                    </ol>
                                </li>
                            </ol>
                   		</li>
                        
                        <li>RULES ABOUT USE OF THE SERVICE AND THE WEBSITE
                        	<ol>
                            	<li>We will use reasonable endeavours to correct any errors or omissions as soon as practicable after being notified of same. However, we do not guarantee that the Service or the Website will be free of faults (or Vouchers will be free of error) and we do not accept liability for any errors or omissions. In the event of an error or fault, you should report it by email to: technical@developaplayer.com</li>
                                <li>We do not warrant that your use of the Service or the Website will be uninterrupted and we do not warrant that any information (or messages) transmitted via the Service or the Website will be transmitted accurately, reliably, in a timely manner or at all.</li>
                                <li>We do not give any warranty that the Service or the Website is free from viruses or anything else which may have a harmful effect on any technology.</li>
                                <li>Also, although we will try to allow uninterrupted access to the Service and the Website, access to the Service and the Website may be suspended, restricted or terminated at any time.</li>
                                <li>We reserve the right to change, modify, substitute, suspend or remove without notice any information or Voucher or service on the Website or forming part of the Service from time to time. Your access to the Website and/or the Service may also be occasionally restricted to allow for repairs, maintenance or the introduction of new facilities or services. We will attempt to restore such access as soon as we reasonably can. We assume no responsibility for functionality which is dependent on your browser or other third party software to operate (including, without limitation, RSS feeds). For the avoidance of doubt, we may also withdraw any information or Voucher from the Website or Service at any time.</li>
                                <li>We reserve the right to block access to and/or to edit or remove any material which in our reasonable opinion may give rise to a breach of any of this Agreement.</li>
                            </ol>
                        </li>
                        
                        <li>SUSPENSION AND TERMINATION
                        	<ol>
                            	<li>If you use (or anyone other than you with your permission uses) the Website or Service or a Voucher in contravention of this Agreement, we may suspend your use of the Service and/or Website (in whole or in part) and/or a Voucher.</li>
                                <li>If we suspend the Service or Website or a Voucher, we may refuse to restore the Service or Website or Voucher until we receive an assurance from you, in a form we deem acceptable that there will be no further breach of the provisions of this Agreement.</li>
                                <li>DEVELOP A PLAYER Australia shall fully co-operate with any law enforcement authorities or court order requesting or directing DEVELOP A PLAYER Australia to disclose the identity or locate anyone in breach of this Agreement.</li>
                                <li>Without limitation to anything else in this Clause 9, we shall be entitled immediately or at any time (in whole or in part) to: i) suspend the Service and/or Website; ii) suspend your use of the Service and/or Website; iii) suspend the use of the Service and/or Website for persons we believe to be connected (in whatever manner) to you; and/or iv) terminate this Agreement immediately if:
                                	<ol>
                                    	<li>you commit any breach of this Agreement;</li>
                                        <li>we suspect, on reasonable grounds, that you have, might or will commit a breach of these terms; or</li>
                                        <li>we suspect, on reasonable grounds, that you may have committed or be committing any fraud against us or any other person.</li>
                                    </ol>
                                </li>
                                <li>Our rights to terminate this Agreement shall not prejudice any other right or remedy we may have in respect of any breach or any rights, obligations or liabilities accrued prior to termination.</li>
                            </ol>
                        </li>                                                   
                    </ol>
                    
                    <h2 class="aboutUsTitle center">Returns Policy</h2>
                    <ol>
                    	<li>Time Limit: Develop A Player carries no responsibility for training or merchandis being outdated by new laws or government changes. There is a 1 month period (from the time of purchase) that merchandise under the fair trade agreemnt</li>
                        <li>Change of Mind: We are unable to offer a refund for a change of mind.</li>
                        <li>Faulty or items received in error: If you have received a faulty item or received an item in error, we are happy to replace, exchange or refund the cost of the item for you. We will pay the costs of postage and handling if you return a faulty or unsuitable item or an item received in error. If you choose to exchange or replace the item we will pay the costs of postage and handling of the replacement item. Please contact us before returning any items.</li>
                        <li>Damaged Items: All products are carefully packed to endure reasonable handling through the delivery process. We are not responsible for damage resulting from abnormal or extreme delivery conditions.</li>
                        <li>Refunds: Refunds are usually processed within 14 business days. All refunds must be issued to the original purchaser. Refunds may incur a 20% restocking/handling fee. All refunds must be issued to the original purchaser.</li>
                    </ol>
                    <p>Dispatch and Deliveries Policy:<br><br>
All Develop A Player products and services are delivered via Australia Post Regular Post service. Postage will be calculated at checkout; an additional charge will be incurred for buyers requesting express delivery. We will endeavour to have your order in the post within 30 working days of placing it. Customised covers require a longer period to prepare so please allow up to 30 working days for goods to be delivered. Please ensure that you provide a valid delivery address. Where a valid address is not provided the purchaser will bear the expense to have the goods re-delivered.
</p>
                    <p>Ordering Policy:<br><br>
All orders taken by telephone must be paid for by credit card (Mastercard or Visa) when placing the order. All orders made using a purchase order system must be in writing (email, or post) and have a valid purchase order number. Payment for goods may be made by credit card, funds transfer. All prices are subject to change without notice.
</p>
            	</div>
            </div>
		</section>                                                   
</div>   