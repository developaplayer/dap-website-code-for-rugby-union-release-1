 <div class="main_container login-reg-pg">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Contact Us</li>
                </ul>
            </div>
		</section>
        <section class="vspace70">
            <div class="container">
            	<div class="reg_bg aboutUs">                	                    
                    <div class="aboutUsContainer">
                    	<h2 class="aboutUsTitle center">Contact Us</h2>
						<?php if ($this->Session->check('Message.flash'))
			           {
						   ?>
						<div class="alert alert-success"><center ><strong><?php echo $this->Session->flash(); ?></strong></center></div>
					   <?php }?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contactIconDetails_holder">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="contactIconDetails">
                                    <div class="contactIcon"><span class="contactIconImg"><img src="<?php echo $this->webroot; ?>images/ico_location.png" alt=""></span></div>
                                    <div class="contactDetails">
                                        <h2>Develop A Player Pty. Ltd</h2>
                                        <p>Apartment 21502, Oracle Tower 2 </br> 21 Elizabeth Avenue Broad beach, QLD.</br>
										4218 Australia</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="contactIconDetails">
                                    <div class="contactIcon"><span class="contactIconImg"><img src="<?php echo $this->webroot; ?>images/ico_ph.png" alt=""></span></div>
                                    <div class="contactDetails">
                                        <h2>Call</h2>
                                        <p>+61 (0) 415 605 494<br></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="contactIconDetails">
                                    <div class="contactIcon"><span class="contactIconImg"><img src="<?php echo $this->webroot; ?>images/ico_msg.png" alt=""></span></div>
                                    <div class="contactDetails">
                                        <h2>Email Id</h2>
                                        <p><a href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></p>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        	<div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<h2>Personal Information</h2>
                               	</div>
                          	</div> 
                            <form action="" method="post">
							<form method="post" action="" enctype="multipart/form-data" id="">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" class="form-control invalid" required>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control invalid" required>
                                </div>
                      		</div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Email Id</label>
                                    <input type="email" name="email" class="form-control invalid" required>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <label>Phone</label>
                                    <input type="tel" name="phone" class="form-control invalid" required>
                                </div>
                      		</div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<label>Message</label>
                                    <textarea class="form-control invalid" name="message"></textarea>
                                </div>
                      		</div>
                            <div class="row">
                            	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                	<input type="submit" value="Submit">
                             	</div>
                         	</div>
                            </form>
                    	</div> 
                    </div>
                    <div class="aboutBanner">	
                   <!-- <div id="map-canvas"></div>-->   
					<!--<img src="<?php echo $this->webroot;?>images/contact_map.jpg" alt="">-->
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3521.7985943755807!2d153.43056231506938!3d-28.03063098265907!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9104fc0361e8c9%3A0x74b797e9e1c7c903!2sThe+Oracle+Broadbeach!5e0!3m2!1sen!2sin!4v1459770929779" width="1366" height="616" frameborder="0" style="border:0" allowfullscreen></iframe>					
					</div>
                </div>
            </div>
		</section>                                                   
	</div> 
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>	
<style>
     #map-canvas {
        width: 1366px;
        height: 616px;
      }
img[src*="gstatic.com/"], img[src*="googleapis.com/"] {
max-width: none !important;
}
gmap_info_window { width: 350px;}
.gmap_info_window .thumbnails { width: 100px; height: 100px; overflow: hidden; border-radius: 100%; border: 5px solid #ddd; float: left; margin-right: 15px;}
.gmap_info_window .thumbnails img { max-width: 100%; height: auto;}
.gmap_info_window .title { font:bold 18px Arial; color: #535353; margin: 0 0 10px 0; padding: 0;}
.gmap_info_window p { margin: 0 0 10px 0; font:14px Arial; color: #88898b;}
.gmap_info_window p a, .gmap_info_window a { color: #515497; text-decoration: none;} 
.gmap_info_window a:hover, .gmap_info_window p a:hover { text-decoration: underline;}
.gmap_info_window .brief { float: right; width: 218px;}  	  
 </style>
<script>	
   function initialize() {		  
		
   var infowindow = new google.maps.InfoWindow();
   var center = new google.maps.LatLng('-28.036532','153.425744');

    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });	
	
	 var latLng = new google.maps.LatLng(
                          -28.036532,
                         153.425744);
	
        var marker = new google.maps.Marker({
            position: latLng,
            map: map,
		
        });
		
		
		   markers.push(marker);	
			   //map.setZoom(16);
              var infowindow = new google.maps.InfoWindow();			   
			  google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
				 
				  
				return function (){ 
			        
			 var contentString = 'FGHFTHFGHFGHF';	  
				  
				 
			  infoWindow.setContent(contentString);
			 // infoWindow.open(map,this);  				  
			  infowindow.open(map,marker);
			  
				}
				
  })(marker, i)); 
      
  
}
       // google.maps.event.addDomListener(window, 'load', initialize);

 </script>       
