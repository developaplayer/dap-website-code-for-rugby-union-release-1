<div class="main_container">
	<div id="carousel" class="carousel slide">
		<?php 
		$banner_count=count($banner_details);
		?>
		<ol class="carousel-indicators">
		<?php for($i=0;$i<$banner_count;$i++) {
			if($i==0)
			{
				$class="active";
			}
			else
			{
				$class="";
			}	
			?>
			<li data-target="#carousel" data-slide-to="<?php echo $i;?>" class="<?php echo $class?>"></li>
		<?php } ?>
			<!--<li data-target="#carousel" data-slide-to="1"></li>-->
		</ol>
		
		<div class="carousel-inner">
		<?php 
		$i=1;
		foreach($banner_details as $banner) {
			if($i==1)
			{
				$class="active";
			}
			else
			{
				$class="";
			}			
			?>
			<div class="item <?php echo $class?>">
			<?php 
			if(!empty($banner['Banner']['image']))
			{
			?>
				<img src="<?php echo $this->webroot; ?>uploads/banners/modified_banner/<?php echo $banner['Banner']['image'];?>" alt="" />	
			<?php
			}
			?>				
			</div>
		<?php $i++; } ?>
		</div> 
		
			<a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
              	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              	<span class="sr-only">Next</span>
            </a>
		
	</div>
	<section class="hp_login_banner_outer">
		<a name="has_tag" href="#"></a><div class="container hp_login_banner">
			<div class="hp_login_banner_innr">
			
					
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center">
                <?php 
			if($this->Session->read('Auth.Member.id')=="") 
			{
			?>	
					<div class="hp_login_banner_container login">
						<a href="<?php echo $this->webroot;?>players/login">
							<span class="hp_login_banner_ico"></span>
							<span class="hp_login_banner_icotxt">Player Login</span>
						</a>
					</div>
                    <?php } ?>
				</div>
				
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center">
					<div class="hp_login_banner_container course">
						<a href="<?php echo $this->webroot; ?>coach_courses">
							<span class="hp_login_banner_ico"></span>
							<span class="hp_login_banner_icotxt">Book a Training Course</span>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center">
					<div class="hp_login_banner_container gear">
						<a href="<?php echo $this->webroot; ?>products/product_list">
							<span class="hp_login_banner_ico"></span>
							<span class="hp_login_banner_icotxt">Purchase Training Gear</span>
						</a>
					</div>
				</div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 center">
                    <?php 
			if($this->Session->read('Auth.Member.id')=="") 
			{?>
					<div class="hp_login_banner_container login coach">
						<a href="<?php echo $this->webroot; ?>coaches/login">
							<span class="hp_login_banner_ico"></span>
							<span class="hp_login_banner_icotxt">Coach Login</span>
						</a>
					</div>
				</div>
				
			<?php } ?>	
			</div>
		</div>    
	</section> 
	<section class="hp_listing_area">
		<div class="container">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<div class="row popular_training_topbar vspace10">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<h2>Popular Training Courses</h2>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 right">
					<?php if(!empty($course_details))
					{
					?>
                    <a href="<?php echo $this->webroot;?>coach_courses/index" class="seeall_btn">See All</a>
					<?php } ?>
                            <!--<span class="sortby_holder">
                            	<span class="sortby_txt">Sort By:</span>
                                <select>
                                	<option>Attack</option>
                                    <option>Defence</option>
                                    <option>Conditioning</option>
                                    <option>Mindset</option>
                                </select>                             
                            </span>-->
                        </div>
				</div>
				<div class="row">				
					<?php 
					//pr($course_details);
					if(!empty($course_details))
					{
						foreach($course_details as $key=>$home_course)
						{
						?>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 training_course_details">
							<span class="training_course_nm"><?php echo stripslashes($home_course['Course']['course_title'])?></span>
							
							<span class="trainer_nm">Trainer:<span>
							<?php foreach($home_course['CourseCoache'] as $trainer)
							{
								//pr($trainer);
								$i=1;
								if(!empty($trainer['coach_id']))
								{
									//echo $trainer['coach_id'];
									$trainer_name = $this->requestAction(array('controller' => 'homes','action' => 'trainer_name','coach_id' =>$trainer['coach_id']));
									echo $trainer_name.",";	
								}
							}
							?>
							</span></span>
							<span class="trainer_nm">Date:<span> <?php
							$date = $home_course['CourseDate']['course_date'];
							echo date("jS F, Y", strtotime($date));
							?></span></span>
							<p class="training_course_details_txt"><?php 
							echo $description = substr($home_course['Course']['description'], 0, 100);						
							 ?></p>
							<span class="view_details_training"><a href="<?php echo $this->webroot;?>coach_courses/event_details/<?php echo $home_course['Course']['id'];?>/<?php echo $home_course['CourseDate']['id'];?>/<?php echo $home_course['Course']['category_id'];?>">View Details</a></span>
							<span class="book_training"><a href="<?php echo $this->webroot;?>coach_courses/event_details/<?php echo $home_course['Course']['id'];?>/<?php echo $home_course['CourseDate']['id'];?>/<?php echo $home_course['Course']['category_id'];?>">Book</a></span>
						</div>
						<?php
						}  
					}else
					{					
					?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 training_course_details">No Popular Training Courses available</div>
					<?php } ?>					
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 top_highlight_panel">
				<h2>Top Highlights</h2>
				<?php
				$z=0;
				foreach($highlight_video as $k=>$highlights)
				{
					if($highlights['Highlight']['video_image'] != "")
					{
				?>				
				<div class="row vspace10">
						<?php if($highlights['Highlight']['type'] == "Youtube")
					{?>
                    	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 vid_ico">
							<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video_highlight<?php echo $z?>"> 
							<img id="video" src="<?php echo $highlights['Highlight']['video_image'] ?>"></a>
						</div>
						
					<?php }else{?>
							<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 vid_ico">
								<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_video_highlight<?php echo $z?>"> 
								<?php echo $highlights['Highlight']['video_iframe'] ?></a>
							</div>
					<?php }?>
					
					
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        	<p class="highlight_txt"><?php echo $highlights['Highlight']['video_title'] ?></p>
                            <span class="highlight_dt">
								<?php 
								$date_h = $highlights['Highlight']['created'];
								echo date("jS F, Y", strtotime($date_h));					
								?>
							</span>
                        </div>
				</div>										
				<?php 
					} 				
					else
					{
				?>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                        	<p class="highlight_txt"><?php echo "Video Not Found"; ?></p>                            
                    </div>
				<?php
					}
				?>
				
				<?php $z++;} ?>	
			</div>
			
			
			<?php
			$x = 0;
			foreach($highlight_video as $k=>$highlights)
			{						     				
			?>
			
			<div class="modal fade" id="myModal_video_highlight<?php echo $x?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header">
											<span class="upload_video_hd" id="iframe_title_id">
											<?php echo $highlights['Highlight']['video_title'] ?>
											</span>
											<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div class="modal-body" id="iframe_id">
										<?php if($highlights['Highlight']['type']=='Custom'){?>
		<video width="500" height="380" controls="">
		<source src="<?php  echo $this->webroot ?>uploads/video/custom_video/<?php echo $highlights['Highlight']['video_image']; ?>"></source></video>
										<?php }else{ ?>
										
										<?php echo $highlights['Highlight']['video_iframe'] ; }?>
										
										</div>
									</div>
								</div>
							</div>
			</div>
			
			<?php $x++;} ?>
			
		</div>
	</section>
	<section class="top_16_popular vspace30">
		<div class="container center">
			<h2 class="heading_type1">Players in Focus</h2>
			<div class="ms-staff-carousel">
                    <div class="master-slider" id="masterslider">
                        <?php 
						foreach($all_player_details as $key=>$player_value)
						{
						?>
						<div class="ms-slide">
							<?php 
							if(!empty($player_value['PlayerDetail']['profile_img']))
							{
							?>
							<img src="blank.gif" data-src="<?php echo $this->webroot.'uploads/profiles/player/profile_pic/'.$player_value['PlayerDetail']['profile_img']?>" alt="<?php echo $player_value['User']['first_name']?>" onclick="return player_redirect('<?php echo base64_encode($player_value['User']['id']);?>')"/>
							<?php } else
							{?>
							<img src="blank.gif" data-src="<?php echo $this->webroot; ?>images/no_images.jpg" onclick="return player_redirect('<?php echo base64_encode($player_value['User']['id']);?>')" alt="No Image"/>
							<?php }?>
                            <div class="ms-info">
                                <h3><a href="<?php echo $this->webroot;?>players/player_profile/<?php echo base64_encode($player_value['User']['id']);?>" style="text-decoration:none;">
								<?php echo $player_value['User']['first_name']." ".$player_value['User']['last_name']?>
								</a></h3>
                                <h4><?php echo $player_value['PrimaryPosition']['name']?></h4>
								<!--<h4><?php echo $player_value['PrimaryPosition']['name']?>, Under 17</h4>-->
                                <!--<span class="image_tag">Level 3 Certified</span>-->
                            </div>     
                        </div>
						<?php } ?>
                     
                    </div>
                    <div class="ms-staff-info" id="staff-info"> </div>
                </div>
		</div>
	</section> 
	<section class="top_coaches vspace30 center">
		<div class="container">
			<h2>Top 8 <span>Coaches</span></h2>
			<div class="row vspace30 left">
				<?php foreach($all_coach_data as $home_coach)
				{
					
					list($width, $height, $type, $attr) = getimagesize(PHYCALPATH.'profiles/coach/profile_pic/'.$home_coach['CoachDetail']['profile_img']);							
							$imgclass="";
							if($width>$height)
							{
								$imgclass='resizeLandscape3';						
						    }else if($width<$height)
							{
								$imgclass='resizePortrait3';
							}else
							{
							   $imgclass='resizeSqr';
							}
							
							//echo "Image width " .$width;
							//echo "<BR>";
							//echo "Image height " .$height;
							//echo "<BR>";
							//echo "Image type " .$type;
							//echo "<BR>";
							//echo "Attribute " .$attr;
					
				?>
				
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 vspace20">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
						<span class="coach_img">
						<span class="imageCenterMedium"><img class="<?php echo $imgclass; ?>" src="<?php echo $this->webroot; ?>uploads/profiles/coach/profile_pic/<?php echo $home_coach['CoachDetail']['profile_img'];?>" alt=""></span></span>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
						<span class="coach_det_nm"><a href="<?php echo $this->webroot;?>coaches/coach_profile/<?php echo base64_encode($home_coach['User']['id']);?>"><?php echo $home_coach['User']['first_name']." ".$home_coach['User']['last_name'];?></a></span>
						<span class="coach_det_cat"><?php echo $home_coach['CoachSpecialityP']['name'];?></span>
					</div>
				</div>
				
				<?php } ?>
				
			</div>
			<div class="row">
				<a href="<?php echo $this->webroot;?>coaches/coach_list" class="viewall_btn">View All</a>
			</div>
		</div>
	</section>
	<section class="featured_training_gear vspace30 center">
		<div class="container">
			<h2>Featured Training Gear</h2>
			<div class="flexslider training_gears">
				<ul class="slides">
				<?php foreach($all_product as $home_product)
				{ 
				?>
					<li>
						<span class="training_gear_img"><a href="<?php echo $this->webroot; ?>products/product_details/<?php echo base64_encode($home_product['Product']['id']);?>">
						<?php if(!empty($home_product['ProductImage']['image']))
						{?>
						<img src="<?php echo $this->webroot; ?>uploads/product/original/<?php echo $home_product['ProductImage']['image']?>" alt="">
						<?php } else {?>
						<img src="<?php echo $this->webroot; ?>images/no_images.jpg" alt="">
						<?php } ?>						
						</a></span>
						<span class="training_gear_nm"><a href="<?php echo $this->webroot; ?>products/product_details/<?php echo base64_encode($home_product['Product']['id']);?>"><?php echo $home_product['Product']['name']?></a></span>						
						<?php 							
							$price_explode = explode('.',$home_product['Product']['price']);
							if(isset($price_explode[0]))
							{
								$fistpartprice= $price_explode[0];								
							}else								
							{
								$fistpartprice=00;									
							}							
							if(isset($price_explode[1]))
							{
								$secpartprice= $price_explode[1];								
							}else								
							{
								$secpartprice='';									
							}							
							?>
						<span class="training_gear_prc">AUD <?php echo $fistpartprice; ?><sup><?php echo $secpartprice; ?></sup></span>
					</li>
				<?php } ?>					
				</ul>
			</div>
			<div class="row">
				<a href="<?php echo $this->webroot; ?>products/product_list/" class="viewall_btn">View All</a>
			</div>
		</div>
	</section>
	
	<!--<section class="brand_logo_holder vspace30">
		<div class="container">
			<div class="flexslider brand_logos">
				<ul class="slides">
					<li><img src="<?php echo $this->webroot; ?>images/brand1.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand2.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand3.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand4.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand5.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand6.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand3.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand4.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand1.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand2.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand6.jpg" alt=""></li>
					<li><img src="<?php echo $this->webroot; ?>images/brand5.jpg" alt=""></li>
				</ul>
			</div>
		</div>
	</section>  --> 
	
</div>

<?php if(isset($this->request->params['pass'][0]) && $this->request->params['pass'][0]=1)
{
?>		
<div class="modal fade" id="myModal_login_poup_check" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<div class="vertical-alignment-helper">
								<div class="modal-dialog vertical-align-center">
									<div class="modal-content">
										<div class="modal-header" style="background-color: rgb(30, 153, 234); color: white;text-align:center;font-size:21px">	
                                            Login  Details<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
										</div>
										<div id="iframe_id" class="modal-body login-dtal-popup" >									
										   <a href="<?php echo $this->webroot; ?>families/login">Family Login</a>										
										</div>
										
										<div id="iframe_id" class="modal-body login-dtal-popup" >									
										   <a href="<?php echo $this->webroot; ?>players/login">Player Login</a>										
										</div>
										
										<div id="iframe_id" class="modal-body login-dtal-popup" >								
										    <a href="<?php echo $this->webroot; ?>coaches/login">Coach Login</a>										
										</div>
										
									</div>
								</div>
							</div>
                        </div>


<?php } ?>



<script>

  /*$(window).load(function(){		
        $('#myModal_login_poup_check').modal('show');
    });*/
	
	$( document ).ready(function() {
        $('#myModal_login_poup_check').modal('show');
    });
	
function player_redirect(player_id)
{
	//alert(player_id);
	window.location.href = "<?php echo $this->webroot;?>players/player_profile/"+player_id;
	
}
</script>
