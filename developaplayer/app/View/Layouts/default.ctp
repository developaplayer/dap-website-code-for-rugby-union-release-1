<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>images/FAVICON.png"/>
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<title><?php echo $title_for_layout; ?></title>
<?php echo $this->Html->css('bootstrap.min'); ?>
<?php echo $this->Html->css('flexslider'); ?>
<?php echo $this->Html->css('bootstrap-datetimepicker'); ?>
<?php echo $this->Html->css('jquery-ui'); ?>
<?php echo $this->Html->css('jquery.mCustomScrollbar'); ?>
<?php echo $this->Html->css('masterslider'); ?>
<?php echo $this->Html->css('ms-staff-style'); ?>
<?php echo $this->Html->css('eventCalendar'); ?>
<?php echo $this->Html->css('eventCalendar_theme_responsive'); ?>
<?php echo $this->Html->css('magiczoomplus'); ?>
<?php echo $this->Html->css('jquery.simplyscroll'); ?>
<?php echo $this->Html->css('fonts'); ?>
<?php echo $this->Html->css('custom'); ?>
<?php echo $this->Html->script('jquery-1.11.3.min'); ?>
<?php echo $this->Html->script('jquery.validate'); ?>

</head>
<body>
<header>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<?php echo $this->element('header'); ?>
		</div>
	</nav>
</header>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
<footer>
	<?php echo $this->element('footer'); ?>
</footer>

<?php echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->Html->script('jquery.equalheights'); ?>
<?php echo $this->Html->script('jquery.flexslider-min'); ?>
<?php echo $this->Html->script('moment-with-locales'); ?>
<?php echo $this->Html->script('bootstrap-datetimepicker'); ?>
<?php echo $this->Html->script('jquery.mCustomScrollbar.concat.min'); ?>
<?php echo $this->Html->script('canvasjs'); ?>
<?php echo $this->Html->script('jquery-ui.min'); ?>
<?php echo $this->Html->script('masterslider'); ?>
<?php echo $this->Html->script('jquery.eventCalendar'); ?>
<?php echo $this->Html->script('magiczoomplus'); ?>
<?php echo $this->Html->script('jquery.simplyscroll'); ?>
<?php echo $this->Html->script('custom'); ?>
<?php echo $this->Html->script('jquery.validate.min'); ?>
</body>
</html>