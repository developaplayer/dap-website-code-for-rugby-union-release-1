<div class="main_container login-reg-pg" style="margin-top: 103px;">
	<section class="breadcrumb vspace20">                    
		<section class="vspace70">
			<div class="container">        	
				<div class="reg_bg center">			
					<span class="glyphicon glyphicon-ok-circle" style="font-size: 70px;color: #1e99ea;"></span><br><br>
					<?php if(!empty($current_booking)) { ?>
					<span class="success_view_txt">Payment successfully done.</span></br>
					<span class="success_view_txt">Your Transaction No is <?php echo $current_booking['BookCourseByPlayer']['vpc_MerchTxnRef']?></span>
					<div class="courseDetailsTable">
						<table border="1" bordercolor="#e7e7e7" cellpadding="0" cellspacing="0" width="100%" align="center">
							<tr>
								<td>Player:</td>
								<td><?php echo $current_booking['Player']['first_name']." ".$current_booking['Player']['last_name']?></td>
							</tr>
							<tr>
								<td>Payment Date:</td>
								<td><?php
								$date = $current_booking['BookCourseByPlayer']['date'];
								echo date("jS F, Y", strtotime($date));
								
								?></td>
							</tr>
							
						</table>
					</div>
					<div class="courseDetailsAmount">Total paid amount: <span>AUD <?php echo  $current_booking['BookCourseByPlayer']['amount']?></span></div>
					<?php } else { ?>
					<span class="glyphicon glyphicon-remove-circle" style="font-size: 70px;color: #d70000;"></span><br><br>
					<span class="success_view_txt">Your payment was not successful. The transaction was declined by the issuer.</span></br>
					<?php } ?>
				</div>
			</div>
		</section>
	</section>
</div>