 <?php 
 $default=$this->request->params['pass'][0];

if(isset($this->request->params['pass'][0]))
{
 if($default==1)
 {
	 $active1='active';
 }else 
 {
	 $active1=''; 
 }
 
 if($default==2)
 {
	 $active2='active';
 }else
 {
	  $active2='';
 }

if($default==3)
 {
	 $active3='active';
 }else
 {
	  $active3='';
 }	 

if($default==4)
 {
	 $active4='active';
 }else
 {
	  $active4='';
 }

}else
{
	 $active1='active';
	 $active2='';
	 $active3='';
	 $active4='';
} 

 ?>
 
 <div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Attack Course</li>
              </div>
		</section>
        <section class="event_calendar_bg">
            <div class="container">
			
			 <ul class="nav nav-tabs">
                    <li class="<?php echo $active1 ?>">
                        <a href="#subtab1" data-toggle="tab">Attack</a>                
                    </li>
                    <li class="<?php echo $active2 ?>">
                        <a href="#subtab2"  data-toggle="tab">Defence</a>                
                    </li>
                    <li class="<?php echo $active4 ?>">
                        <a href="#subtab3"  data-toggle="tab">Physical Conditioning</a>                
                    </li>
                    <li class="<?php echo $active3 ?>">
                        <a href="#subtab4"  data-toggle="tab">Mental Conditioning</a>                
                    </li>
                </ul>
	
                <div class="tab-content">
                    <div class="tab-pane event_calendar_bg red <?php echo $active1 ?>" id="subtab1"> 
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                       	
                        <div id="eventCalendarLocaleFile1"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg blue <?php echo $active2 ?>" id="subtab2">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                        	
                       	<div id="eventCalendarLocaleFile2"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg orange <?php echo $active4 ?>" id="subtab3">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                         	
                       	<div id="eventCalendarLocaleFile3"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg green <?php echo $active3 ?>" id="subtab4">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                         	
                       	<div id="eventCalendarLocaleFile4"></div>                 		
                    </div>
                </div>
            	
                <!--<div class="course_listing_topbar center">
                	<div class="course_ico"><img src="images/ico_attack_b.png" alt=""></div>
                    <div class="course_nm">Attack Course</div>
                    <div class="course_nm_desc">Select a Date on the calendar to book your course.</div>
                </div>-->
            
				<div id="eventCalendarLocaleFile"></div> 
                <script>
                	$(document).ready(function(){
						//attack();
						});
						
						
					/*$(document).ready(function() {
						$("#eventCalendarLocaleFile").eventCalendar({
							eventsjson:  '<?php echo $this->webroot; ?>coach_courses/even_json_file/7',
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
							eventsScrollable: true
						});
					
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
					});*/
					
					$(document).ready(function() {
					
					$("#eventCalendarLocaleFile1").eventCalendar({
							eventsjson:  '<?php echo $this->webroot; ?>coach_courses/even_json_file/1',
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
							eventsScrollable: true
						});
					
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
					
					
					
					$("#eventCalendarLocaleFile2").eventCalendar({
							eventsjson:  '<?php echo $this->webroot; ?>coach_courses/even_json_file/2',
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
							eventsScrollable: true
						});
					
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
				
					
					
					
					$("#eventCalendarLocaleFile3").eventCalendar({
							eventsjson:  '<?php echo $this->webroot; ?>coach_courses/even_json_file/4',
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
							eventsScrollable: true
						});
					
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
					
					
					
					
					$("#eventCalendarLocaleFile4").eventCalendar({
							eventsjson:  '<?php echo $this->webroot; ?>coach_courses/even_json_file/3',
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
							eventsScrollable: true
						});
					
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
					});
					
                </script>                 
            </div>
		</section>                                                   
	</div> 