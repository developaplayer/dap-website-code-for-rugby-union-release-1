<div class="main_container login-reg-pg" style="margin-top: 103px;">
	<section class="breadcrumb vspace20">                    
		<section class="vspace70">
			<div class="container">        	
				<div class="reg_bg center">			
					<span class="glyphicon glyphicon-ok-circle"></span><br><br>
					<span class="success_view_txt">Course Details </span>
					<div class="courseDetailsTable">
						<table border="1" bordercolor="#e7e7e7" cellpadding="0" cellspacing="0" width="100%" align="center">
							<tr>
								<td>Course Name:</td>
								<td><?php echo $booking_details['Course']['course_title']?></td>
							</tr>
							<tr>
								<td>Course Date:</td>
								<td><?php 
								$date = $booking_details['Course']['course_date'];
								echo date("jS F, Y", strtotime($date));
								?></td>
							</tr>
							<tr>
								<td>Course Location:</td>
								<td><?php echo $booking_details['Course']['location']?></td>
							</tr>
							<tr>
								<td>Course Duration:</td>
								<td><?php echo $booking_details['Course']['duration']?></td>
							</tr>
							<tr>
								<td>Course Coaches:</td>
								<td>
								<?php echo $booked_coach = $this->requestAction(array('controller' => 'coach_courses', 'action' => 'all_booked_coach','course_id' =>$booking_details['Course']['id']));?>
							</td>
							</tr>
							<tr>
								<td>Booking Date:</td>
								<td>
								<?php 								
								$payment_date = $booking_details['BookCourseByPlayer']['date'];
								echo date("jS F, Y", strtotime($payment_date));								
								?></td>
							</tr>
						</table>
					</div>
					<div class="courseDetailsAmount">Total paid amount: <span>AUD<?php echo $booking_details['BookCourseByPlayer']['amount']?></span></div>
				</div>
			</div>
		</section>
	</section>
</div>
