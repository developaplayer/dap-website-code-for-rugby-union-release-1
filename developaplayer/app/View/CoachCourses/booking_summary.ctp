<div class="main_container">
	<section class="breadcrumb vspace20">
		<div class="container">
			<ul>
				<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
				<li><a href="javascript:void(0);"><span>&raquo;</span> Course Booking Summary</a></li>
				
			</ul>			
		</div>
	</section>
	<section class="product_listing">
		<div class="container orderSummaryPg"> 
			<div class="cart_holder orderSummaryRow courseBookingDetails">                    
				<div class="cart_row">
					<div class="cart_col">Course Name</div>
					<div class="cart_col">Course Date</div>
					<div class="cart_col">Course Location</div>
					<div class="cart_col">Course Coaches</div>
					<div class="cart_col">&nbsp;</div>
				</div>
				<?php if(!empty($booking_details)) { 
				foreach($booking_details as $key=>$book_details)
				{
				?>
				<div class="cart_row">
					<div class="cart_col price"><?php echo $book_details['Course']['course_title']?></div>
					<div class="cart_col price">
					<?php 
						$date = $book_details['Course']['course_date'];
						echo date("jS F, Y", strtotime($date));
					?>
					</div>
					<div class="cart_col price"><?php echo $book_details['Course']['location']?></div>
					<div class="cart_col price">
					<?php echo $booked_coach = $this->requestAction(array('controller' => 'coach_courses', 'action' => 'all_booked_coach','course_id' =>$book_details['Course']['id']));?>
					
					</div>
					<div class="cart_col price">
					<?php $booking_id=base64_encode($book_details['BookCourseByPlayer']['id']);?>
					<a href="<?php echo $this->webroot?>coach_courses/booking_details/<?php echo $booking_id?>">View Details</a></div>
				</div>
				<?php } } else
				{
					echo "No Records Found";
					
				}?>
				
			</div>                                                          
		</div>
	</section>                                                           
</div>    
