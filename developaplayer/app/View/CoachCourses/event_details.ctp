<?php //pr($this->request->params['pass']); exit;?>
<style>
	 .eventCalendar-list>li
	 {
		display:none;
	}
	.eventCalendar-list>li:first-child
	{
		display:block;
	}
</style>

<?php 
 $default=$this->request->params['pass'][2];

if(isset($this->request->params['pass'][2]))
{
 if($default==1)
 {
	 $active1='active';
 }else 
 {
	 $active1=''; 
 }
 
 if($default==2)
 {
	 $active2='active';
 }else
 {
	  $active2='';
 }

if($default==3)
 {
	 $active3='active';
 }else
 {
	  $active3='';
 }	 

if($default==4)
 {
	 $active4='active';
 }else
 {
	  $active4='';
 }

}else
{
	 $active1='active';
	 $active2='';
	 $active3='';
	 $active4='';
} 

 ?>

<div class="main_container">
    	<section class="breadcrumb vspace20">
            <div class="container">
            	<ul>
                	<li><a href="javascript:void(0);"><span>&raquo;</span> Develop a player</a></li>
                    <li><span>&raquo;</span> Attack Course</li>
                </ul>
            </div>
		</section>
        <section class="event_calendar_bg">
            <div class="container">
            	
          <ul class="nav nav-tabs">
                <li class="<?php echo $active1 ?>">
                        <a href="#subtab1" onclick="subTab_click(1)" data-toggle="tab">Attack</a>                
                    </li>
                    <li class="<?php echo $active2 ?>">
                        <a href="#subtab2" onclick="subTab_click(2)" data-toggle="tab">Defence</a>                
                    </li>
                    <li class="<?php echo $active4 ?>">
                        <a href="#subtab3" onclick="subTab_click(4)" data-toggle="tab">Conditioning</a>                
                    </li>
                    <li class="<?php echo $active3 ?>">
                        <a href="#subtab4" onclick="subTab_click(3)" data-toggle="tab">Mental</a>                
                    </li>
                </ul>
                
                <div class="tab-content">
                    <div class="tab-pane event_calendar_bg red <?php echo $active1 ?>" id="subtab1"> 
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                       	
                        <div id="eventCalendarLocaleFile1"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg blue <?php echo $active2 ?>" id="subtab2">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                        	
                       	<div id="eventCalendarLocaleFile2"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg orange <?php echo $active4 ?>" id="subtab3">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                         	
                       	<div id="eventCalendarLocaleFile3"></div>                 		
                    </div>
                    <div class="tab-pane event_calendar_bg green <?php echo $active3 ?>" id="subtab4">
                    	<p class="select_dt">Select a Date on the calendar to view &amp; book your course.</p>                         	
                       	<div id="eventCalendarLocaleFile4"></div>                 		
                    </div>
                </div>                
                <script>
					$(document).ready(function(){
						
						$("#eventCalendarLocaleFile1").eventCalendar({
							eventsjson: '<?php echo $this->webroot; ?>coach_courses/even_json_details/<?php echo  $this->request->params['pass'][0] ?>/<?php echo  $this->request->params['pass'][1] ?>',
							eventsScrollable: true,
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
						});
						
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
							$("#eventCalendarLocaleFile2").eventCalendar({
							eventsjson: '<?php echo $this->webroot; ?>coach_courses/even_json_details/<?php echo  $this->request->params['pass'][0] ?>/<?php echo  $this->request->params['pass'][1] ?>',
							eventsScrollable: true,
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
						});
						
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
						$("#eventCalendarLocaleFile3").eventCalendar({
							eventsjson: '<?php echo $this->webroot; ?>coach_courses/even_json_details/<?php echo  $this->request->params['pass'][0] ?>/<?php echo  $this->request->params['pass'][1] ?>',
							eventsScrollable: true,
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
						});
						
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
						
						$("#eventCalendarLocaleFile4").eventCalendar({
							eventsjson: '<?php echo $this->webroot; ?>coach_courses/even_json_details/<?php echo  $this->request->params['pass'][0] ?>/<?php echo  $this->request->params['pass'][1] ?>',
							eventsScrollable: true,
							jsonDateFormat: 'human',  // 'YYYY-MM-DD HH:MM:SS'
						});
						
						$(".eventCalendar-list-content.scrollable").mCustomScrollbar({
							scrollButtons:{
								enable:false
							},
							advanced: {
								updateOnContentResize: Boolean
							}
						});
						
						
					});
					
					function subTab_click(tabId)
					{
						
						
						 window.location.href='<?php echo $this->webroot ?>coach_courses/index/'+tabId;
						  return true;
						
					}
				function message_book()
				{				
					//alert('players members only book the course.please login as a palyer!');
					$('#addAchievement_message').modal('show');
        		
				}
				
				function select_payment_type()
				{
					
					$('#select_payment_type').modal('show');
					
					
				}
				
				</script>                 
            </div>
			
			
		</section>                                                   
	</div>
	
	
	<!-- Start Modal: Add New Achievement -->     
    <div class="modal fade" id="addAchievement_message" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Message</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                	<div class="modal-body">
					<!--<a href="<?php echo $this->webroot; ?>players/login"></a>--> 
				     	<p class="select_dt"><strong>Only player can book the course! Please login as a player.</strong></p>
						<p></p>
						  <div class="row vspace10"></div>
                    </div>
            	</div>
        	</div>
    	</div>
	</div>
	
	
	<div class="modal fade" id="select_payment_type" tabindex="-1" role="dialog" aria-labelledby="addAchievementLabel" aria-hidden="true">
    	<div class="vertical-alignment-helper">
        	<div class="modal-dialog vertical-align-center">
            	<div class="modal-content">
                	<div class="modal-header">
                    	<span class="upload_video_hd">Message</span>
                    	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                                       
                	</div>
                    
                	<div class="modal-body">
					<!--<a href="<?php echo $this->webroot; ?>players/login"></a>--> 
				     	
                      <!--<form id="BookCourseByPlayer" method="post" action="/coach_courses/book_course">-->
					  <?php	
					  //echo $this->Form->create('BookCourseByPlayer', array('enctype' => 'multipart/form-data'));
					  
					echo $this->Form->create('BookCourseByPlayer', array('url' => array('controller'=>'coach_courses','action'=> 'book_course')));					  
					  ?>	
                        <div class="row vspace10">
                            <div class="col-lg-12" style="font-size: 16px;">Personal Development Fund: <span style="font-weight:700">AUD <?php echo $player_tot_points;?></span></div>
							 <div class="col-lg-12" style="font-size: 16px;">Course Booking cost:  <span style="font-weight:700">AUD <?php echo $courseArray['Course']['cost']?></span></div>
                        </div>
                        
                        <div class="row vspace10">
                        	<div class="col-lg-12">		  
                                <fieldset id="group1">
                                    <?php if($player_tot_points >= $courseArray['Course']['cost']) {?>
									
									<input type="radio" value="personal" id="r1" name="payment_option">
									<?php  } else { ?>
									<input type="radio" value="personal" disabled="disabled" id="r1" name="payment_option">
									<?php } ?>
									<label for="r1" style="font-weight:400;position:relative;left:5px;top:-2px;"> Personal Development Fund</label>
									
									<br/>
                                    <input type="radio" value="comweb" id="r2" checked="checked" name="payment_option">
									<label for="r2" style="font-weight:400;position:relative;left:5px;top:-2px;"> Comweb Payment</label>
									<input type="hidden" name="player_id" id="player_id" value="<?php echo $player_id?>">
									<input type="hidden" name="course_id" id="course_id" value="<?php echo $course_id?>">
									<input type="hidden" name="course_date_id" id="course_date_id" value="<?php echo $course_date_id?>">
									<input type="hidden" name="cat_id" id="cat_id" value="<?php echo $cat_id?>">
									<input type="hidden" name="player_tot_points" id="player_tot_points" value="<?php echo $player_tot_points?>">
									<input type="hidden" name="player_detail_id" id="player_detail_id" value="<?php echo $player_detail_id?>">
									<input type="hidden" name="course_cost" id="course_cost" value="<?php echo $courseArray['Course']['cost']?>">
                                </fieldset>
                        	</div>
                        </div>
                        <div class="row vspace10">	
                        	<div class="col-lg-12"><input type="submit" value="PayMent"></div>
                        </div>
                        </form>
                    </div>  
            	</div>
        	</div>
    	</div>
	</div>