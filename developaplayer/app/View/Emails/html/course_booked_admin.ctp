<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Develop a Player</title>
</head>

<body style="margin:0; padding:0px; background-color:#fff;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:sans-serif;color:#5c5c5c;font-size:13px;">
	<tr>
        <td align="center" valign="top">
            <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#f7f7f7">
                <tr bgcolor="#0b76bc">
                    <td colspan="3" style="padding-left:15px;"><a href="#"><img src="<?php echo Router::url('/',true); ?>images/logo.png" alt="Develop a Player" width="91" height="103" /></a></td>
                    <td style="color:#ffffff;text-align:right; padding-right:15px"><img src="<?php echo Router::url('/',true); ?>images/ico_ph.png" alt="" width="16" /> +61 (0) 415 605 494<br /> <img src="<?php echo Router::url('/',true); ?>images/ico_msg.png" alt="" width="16" /> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>                    
                </tr>
                <tr bgcolor="#1e99ea">
                	<td colspan="4" style="color:#ffffff;font-size:16px; font-weight:bold; text-align:center; padding:10px 15px;">Email Template  Name</td>
                </tr>
                <tr>
                	<td colspan="4" style="font-size:13px; line-height:22px; padding:30px"> Hi, Admin <br/>
					<p><?php $all_details['first_name'].' '.$all_details['last_name']?>,  have booked the course please check the details below.</p> 
               </tr>			   
                  <tr>
                	<td colspan="4" style="padding:0px 15px 15px 15px;">
                    	<table width="100%" border="1" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" align="center">
                        	
                        	<tr bgcolor="#efefef">
                            	<td style="color:#000000; padding:10px;">Course Name</td>
                                <td style="color:#000000; padding:10px;">Location</td>
                                <td style="color:#000000; padding:10px;">Course Date</td>                               
								<td style="color:#000000; padding:10px;"> Duration</td>
								<td style="color:#000000; padding:10px;">Course Book Date</td>
								<td style="color:#000000; padding:10px;">cost</td>
								<td style="color:#000000; padding:10px;">Transaction id</td>
								
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;"><?php echo $BookCourse['Course']['course_title']; ?></td>
                                <td style="padding:7px 10px;"><a target="_blank" href="<?php echo $BookCourse['Course']['google_location']; ?>"><?php echo $BookCourse['Course']['location']; ?></a></td>
                                <td style="padding:7px 10px;"><?php echo $BookCourse['Course']['course_date']; ?></td>
                                <td style="padding:7px 10px;"><?php echo $BookCourse['Course']['duration']; ?> min</td>
								<td style="padding:7px 10px;"><?php echo date('Y-m-d') ?></td>
								<td style="padding:7px 10px;">AUD <?php echo $BookCourse['Course']['cost']; ?></td>
							    <td style="padding:7px 10px;"><?php echo $all_respone_by_comweb['vpc_MerchTxnRef']; ?></td>
								
                            </tr>
                          
                        </table>
                    </td>
                </tr>
                <tr>
                	<td colspan="4" align="center" style="padding-bottom:15px">PayAmount:AUD <?php echo $BookCourse['Course']['cost']; ?></td>
                </tr>
				
			<tr bgcolor="#0b76bc">
                    <td colspan="4" style="padding:15px; text-align:center"><a style="color:#ffffff; text-decoration:none" href="http://developaplayer.com/" target="_blank">www.developaplayer.com</a> <span style="color:#fff">|</span> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>
             	</tr>
            </table>
        </td>
        </tr>
</table>
</body>
</html>