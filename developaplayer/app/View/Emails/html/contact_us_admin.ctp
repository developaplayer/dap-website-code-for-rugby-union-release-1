<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Develop a Player</title>
</head>

<body style="margin:0; padding:0px; background-color:#fff;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:sans-serif;color:#5c5c5c;font-size:13px;">
	<tr>
        <td align="center" valign="top">
            <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#efefef">
                <tr bgcolor="#0b76bc">
                    <td colspan="3" style="padding-left:15px;"><a href="#"><img src="<?php echo Router::url('/',true); ?>images/logo.png" alt="Develop a Player" width="91" height="103" /></a></td>
                    <td style="color:#ffffff;text-align:right; padding-right:15px"><img src="ico_ph.png" alt="" width="16" /> +61 (0) 415 605 494<br /> <img src="ico_msg.png" alt="" width="16" /> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>                    
        </tr>
               <tr bgcolor="#1e99ea">
                	<td colspan="4" style="color:#ffffff;font-size:16px; font-weight:bold; text-align:center; padding:10px 15px;">Contact Us</td>
                </tr>
               
				
				 
				<tr>
				  
			
                	<td colspan="4" style="padding:30px">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">  

					 <tr> 
                	 <td colspan="4" style="padding:30px">Hello Admin,<br/>
				     <?php echo $contactus['Contact']['first_name'].''.$contactus['Contact']['last_name'];?>, has send a message.Please check and get back.				   
				    </td>			
                   </tr>                     	
                        	<tr>
                            	<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;  border-left:1px solid #fff;  border-top:1px solid #fff;" bgcolor="#efefef">Name</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;  border-top:1px solid #fff;" bgcolor="#efefef">:</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff; border-right:1px solid #fff; border-top:1px solid #fff;" bgcolor="#efefef"><?php echo $contactus['Contact']['first_name'].''.$contactus['Contact']['last_name'];?></td>
								 
                            </tr>
							<tr>
                            	<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;  border-left:1px solid #fff;" bgcolor="#efefef">Email</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;" bgcolor="#efefef">:</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff; border-right:1px solid #fff;" bgcolor="#efefef"><?php echo $contactus['Contact']['email'];?></td>
								 
                            </tr>
							<tr>
                            	<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;  border-left:1px solid #fff;" bgcolor="#efefef">Phone No</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;" bgcolor="#efefef">:</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff; border-right:1px solid #fff;" bgcolor="#efefef"><?php echo $contactus['Contact']['phone']; ?></td>
								 
                            </tr>
							<tr>
                            	<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;  border-left:1px solid #fff;" bgcolor="#efefef">Message</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff;" bgcolor="#efefef">:</td> 
								<td style="color:#000000; padding:10px; border-bottom:1px solid #fff; border-right:1px solid #fff;" bgcolor="#efefef"><?php echo $contactus['Contact']['message']; ?></td>
								 
                            </tr>
                                                  
                        </table>
                    </td>
                </tr>
 
			<tr bgcolor="#0b76bc">
                    <td colspan="4" style="padding:15px; text-align:center"><a style="color:#ffffff; text-decoration:none" href="http://developaplayer.com/" target="_blank">www.developaplayer.com</a> <span style="color:#fff">|</span> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>
             	</tr>
            </table>
        </td>
        </tr>
</table>
</body>
</html>
