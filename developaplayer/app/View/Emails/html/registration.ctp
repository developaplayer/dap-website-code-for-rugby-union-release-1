<table width='100%' cellspacing='0' cellpadding='0' border='0' style='font:12px verdana;color:black'>
<tr><td style='padding:0 14px 28px 15px; color:#535151; font-size:12px;'>
<br />
Dear ,<br />
<br />
<td>
</tr>
<tr><td style='padding:0 14px 28px 15px; color:#535151; font-size:12px;'>
Thank you for registering at Trip Auction. Before we can activate your account and get you running, one last step must be taken to complete your registration.

<a href="<?php echo WWW_ROOT."users/check_email/".$data['account_activation_string'] ?>" >Click here</a> to activate your account.

This verification process is designed to confirm that a valid email address was given by you, rather than someone else, to open a new Trip Auction member account.

Trip Auction respects member's privacy and will never sell, rent or share your email address.

If you continue to have problems signing up or activating your account, please contact us at <?php echo COMPANY_ADMIN_EMAIL; ?> 


</td></tr>
<tr><td style='padding:0 14px 28px 15px; color:#535151; font-size:12px;'>All the best <br>Trip Auction</td></tr>				
</table>