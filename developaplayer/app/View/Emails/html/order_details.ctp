<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Develop a Player</title>
</head>

<body style="margin:0; padding:0px; background-color:#fff;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" style="font-family:sans-serif;color:#5c5c5c;font-size:13px;">
	<tr>
        <td align="center" valign="top">
            <table width="700" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#f7f7f7">
                <tr bgcolor="#0b76bc">
                    <td colspan="3" style="padding-left:15px;"><a href="#"><img src="<?php echo Router::url('/',true); ?>images/logo.png" alt="Develop a Player" width="91" height="103" /></a></td>
                    <td style="color:#ffffff;text-align:right; padding-right:15px"><img src="<?php echo Router::url('/',true); ?>images/ico_ph.png" alt="" width="16" /> +61 (0) 415 605 494<br /> <img src="ico_msg.png" alt="" width="16" /> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>                    
                </tr>
                <tr bgcolor="#1e99ea">
                	<td colspan="4" style="color:#ffffff;font-size:16px; font-weight:bold; text-align:center; padding:10px 15px;">ORDER DETAILS</td>
                </tr>
                <tr>
                	<td style="color:#000000; font-size:16px; padding:15px">Thank you for your order from Developaplayer!!</td>
                </tr>
                <tr>
                	<td colspan="4" style="padding:0px 15px 15px 15px;">
                    	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                        	<tr bgcolor="#e9e9e9">
                            	<td colspan="2" style="color:#000000; padding:10px;border-bottom:#ffffff 1px solid;border-top:#ffffff 1px solid;">BILLING DETAILS:</td>
                                <td colspan="2" style="color:#000000; padding:10px;border-bottom:#ffffff 1px solid;border-top:#ffffff 1px solid;">SHIPPING DETAILS:</td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Name</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:<?php echo $all_oder_details[0]['Order']['payment_fullname'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Name</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['shipping_fullname'];?></td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Billing Address</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:<?php echo $all_oder_details[0]['Order']['payment_address'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Shipping Address</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['shipping_address'];?></td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Country</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:Australia</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Country</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: Australia</td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">State</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['payment_state'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">State</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['shipping_state'];?></td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">City</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:  <?php echo $all_oder_details[0]['Order']['payment_city'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">City</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['shipping_city'];?></td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Zip</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:<?php echo $all_oder_details[0]['Order']['payment_zip'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Zip</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['shipping_zip'];?></td>
                            </tr>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Contact No</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid;border-right:#ffffff 1px solid">:<?php echo $all_oder_details[0]['Order']['contact_no'];?></td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">Contact No</td>
                                <td style="padding:7px 10px;border-bottom:#ffffff 1px solid">: <?php echo $all_oder_details[0]['Order']['contact_no'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                	<td colspan="4" style="padding:0px 15px 15px 15px;">
                    	<table width="100%" border="1" bordercolor="#FFFFFF" cellspacing="0" cellpadding="0" align="center">
                        	<tr bgcolor="#e9e9e9">
                            	<td colspan="4" style="color:#000000; padding:10px; text-transform:uppercase;">Product Details:</td>
                            </tr>
                        	<tr bgcolor="#efefef">
                            	<td style="color:#000000; padding:10px;">Image</td>
                                <td style="color:#000000; padding:10px;">Description</td>
                                <td style="color:#000000; padding:10px;">Unit Price</td>
                                <td style="color:#000000; padding:10px;">Total Price</td>
                            </tr>
							
							<?php
								$base_url=$this->webroot;
								$total_image_path=$base_url.'uploads/Product/thumbnail/';
								 
								foreach($all_oder_details as $key=>$val ) { ?>
								
										<?php 
											if($val['ProductImage']['image'] != '' ) {
												$defaultImage =$total_image_path.''.$val['ProductImage']['image'] ;
											} else {
												$defaultImage = 'img/no_image.jpg';
											}
								
							?>
                            <tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;"><img src="<?php echo Router::url('/',true); ?>uploads/product/thumbnail/<?php echo $val['ProductImage']['image']?>" width="75" /></td>
                                <td style="padding:7px 10px;"><?php echo $val['Product']['name']; ?><br /><br /><?php if( isset($val['Size']['name']) && ($val['Size']['name'] != '' )) { ?>
									Size:<?php echo $val['Size']['name'];  ?><br>								
									<?php
									}	
								   ?><br /><?php echo  $val['OrderProduct']['quantity']; ?></td>
                                <td style="padding:7px 10px;">AUD <?php echo $val['Product']['price'];?></td>
                                <td style="padding:7px 10px;">AUD <span><?php echo number_format(($val['Product']['price']* $val['OrderProduct']['quantity']), 2, '.', ''); ?></td>
                            </tr>
							
							<?php } ?>
							
							
                            <!--<tr bgcolor="#efefef">
                            	<td style="padding:7px 10px;"><img src="jersey2.jpg" width="75" /></td>
                                <td style="padding:7px 10px;">Melbourne Rebels Kids T-Shirt<br /><br />Size: L<br />Qty: 1</td>
                                <td style="padding:7px 10px;">AUD 177.00</td>
                                <td style="padding:7px 10px;">AUD177.00</td>
                            </tr>-->
                            <tr bgcolor="#e9e9e9">
                            	<td colspan="3" style="color:#000000; padding:10px">Subtotal:</td>
                                <td style="color:#000000; padding:10px">AUD <?php echo  $all_oder_details[0]['Order']['total'];?></td>
                            </tr>
                            <tr bgcolor="#e9e9e9">
                            	<td colspan="3" style="color:#000000; padding:10px; text-transform:uppercase; font-weight:bold">YOU PAY:</td>
                                <td style="color:#000000; padding:10px; font-weight:bold">AUD <?php echo $all_oder_details[0]['Order']['total'];?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr bgcolor="#0b76bc">
                    <td colspan="4" style="padding:15px; text-align:center"><a style="color:#ffffff; text-decoration:none" href="http://developaplayer.com/" target="_blank">www.developaplayer.com</a> <span style="color:#fff">|</span> <a style="color:#ffffff; text-decoration:none" href="mailto:contact@developaplayer.com">contact@developaplayer.com</a></td>
             	</tr>
            </table>
        </td>
        </tr>
</table>
</body>
</html>