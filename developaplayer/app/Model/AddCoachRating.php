<?php
class AddCoachRating extends AppModel
{
	public $name = 'AddCoachRating';
	
	public $hasOne = array(
		'LevelCategory'=>array(
			'className'=>'LevelCategory',	
           'conditions'   => 'AddCoachRating.coach_cat_id=LevelCategory.id',			
			 'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'	=>'',
			 'fields'       =>''
		),
		'UpperLowerLimit'=>array(
			'className'=>'UpperLowerLimit',		
			 'order'        => '',
			 'conditions'   => 'AddCoachRating.rating_limit_id =UpperLowerLimit.id',
			 'dependent'    =>  true,
			 'foreignKey'	=>'',
			 'fields'       =>''
		)
	);
		
	/*public $belongsTo = array(
	         'Certificate'=>array(
			 'className'=>'Certificate',
			 'conditions'   => 'CoachCertificate.certificate_id=Certificate.id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields' =>'certificate_id'
	         )
	);*/
	
}
