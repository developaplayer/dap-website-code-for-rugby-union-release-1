<?php
class LavelAssignment extends AppModel
{
	public $name = 'LavelAssignment';
		
	public $belongsTo = array(
	         'CoachLevel'=>array(
			 'className'=>'CoachLevel',
			 'conditions'   => 'LavelAssignment.lavel_id=CoachLevel.id',
			 'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'=>'',			
	         )
	);
	
}
