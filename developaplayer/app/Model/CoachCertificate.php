<?php
class CoachCertificate extends AppModel
{
	public $name = 'CoachCertificate';
	
	public $belongsTo = array(
	         'Certificate'=>array(
			 'className'=>'Certificate',
			 'conditions'   => 'CoachCertificate.certificate_id=Certificate.id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields' =>'Certificate.name'
	         )
	);
		
	
 
}
