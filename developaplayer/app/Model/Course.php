<?php
class Course extends AppModel
{
	public $name = 'Course';	
	public $hasOne = array(
				 'CourseDate'=>array(
				 'className'=>'CourseDate', 				
				 'order'        => '',
				 'dependent'    =>  true,
				 'foreignKey'=>'course_id',			
				 ),
				 'PlayerPosition'=>array(
				 'className'=>'PlayerPosition', 
                 'conditions'=>'PlayerPosition.id=Course.position_id',				 
				 'order'        => '',
				 'dependent'    =>  true,
				 'foreignKey'=>'',			
				 ),				 
				 'Level'=>array(
				 'className'=>'Level',
                 'conditions'=>'Level.id=Course.player_certification_level_id',				 
				 'order'        => '',				 
				 'dependent'    =>  true,
				 'foreignKey'=>'',			
				 ),
				 'CoachCategory'=>array(
				 'className'=>'CoachCategory',
                 'conditions'=>'CoachCategory.id=Course.category_id',				 
				 'order'        => '',				 
				 'dependent'    =>  true,
				 'foreignKey'=>'',			
				 )
		);
		public $hasMany= array(
				 'CourseCoache'=>array(
				 'className'=>'CourseCoache',			
				 'order'        => '',
				 'dependent'    =>  true,
				 'foreignKey'=>'course_id',		
				 )
			);
		
		
		
			   
		
}
