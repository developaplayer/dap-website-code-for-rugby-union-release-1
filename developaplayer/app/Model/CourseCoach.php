<?php
class CourseCoach extends AppModel
{
	public $name = 'CourseCoach';	
	public $hasOne = array(
			 'User'=>array(
			 'className'=>'User', 
			 'conditions'=>'CourseCoach.coach_id=User.id',
			 'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'=>'',
			 )
	);
}