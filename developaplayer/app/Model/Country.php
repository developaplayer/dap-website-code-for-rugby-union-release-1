<?php
/*
*country Model 
* Rezaul Karim - Accenza Pvt Ltd 
* 27.2.2016 
*/
class Country extends AppModel
{
	public $name = 'Country';
	
	function get_country()
	{
		$country = $this->find('list',array('fields'=>array('Country.id','Country.country_name')));
		return $country;
	}
}
?>