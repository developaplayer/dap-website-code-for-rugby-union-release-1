<?php
/*
*Club Model 
* Rezaul Karim - Accenza Pvt Ltd 
* 27.2.2016 
*/
class Club extends AppModel
{
	public $name = 'Club';
	
	function get_club()
	{
		$club = $this->find('list',array('fields'=>array('Club.id','Club.name'),'conditions'=>array('Club.status'=>'A')));
		return $club;
	}
}
?>