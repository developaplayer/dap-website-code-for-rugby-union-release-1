<?php
class FamilyDetail extends AppModel
{
	public $name = 'FamilyDetail';
	
	public $hasOne = array(
	         'User'=>array(
			 'className'=>'User',
			 'conditions'   => 'FamilyDetail.player_id=User.id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields' =>''
	         ),
			  'PlayerRelationship'=>array(
			  'className'=>'PlayerRelationship',
			  'conditions'   => 'FamilyDetail.relation_id=PlayerRelationship.id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields' =>''
	         ),
		'Nationality'=>array(
			 'className'=>'Country',
			 'conditions'   => 'Nationality.id=FamilyDetail.nationality',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields'       =>'Nationality.country_name'
		)
		
	);
	

 
}
