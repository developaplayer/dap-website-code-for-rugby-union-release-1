<?php
/**
* GuestSearch  Model Class file
*
* @copyright 2011 Friday Media Pvt. Ltd.
* @author Koushik Samanta <koushik@techpourri.com>
*
*/
class GuestSearch extends AppModel
{
	var $name = 'GuestSearch';
	/*var $validate = array(			
			
			'product_id'=> array(
				'notEmpty'  => array(
					'rule'      => 'notEmpty',
					'message'   => 'product can not be blank.',
					'last' => true),
				'unique' => array(
					'rule'    => 'isUniqueBrowse',
					'message'    => 'This product is already in use.',
					'allowEmpty' => true)
			)
				
		);*/
		var $belongsTo = array(
		
					'Product'=>array(
						'className' => 'Product',
						'conditions'   => '',
						'fields'       => '',						 
						'order'        => '',
						'dependent'    =>  false,
						'foreignKey'   => 'product_id'
					)

	);
	
	
	function browsing_products($limit=10)
	{
		      
		//$this->Product->hasMany['ProductImage']['conditions'] = "ProductImage.deflt_value = '1'";			
	    $conditions="GuestSearch.session_id='".session_id()."'";
		$this->recursive = 2;
		$GuestDetails = $this->find('all', array('conditions'=>$conditions,'order' => array('GuestSearch.id' => 'DESC'),'limit'=>$limit));
		//pr($GuestDetails);
		$this->remove_old_browsing_products();
		return $GuestDetails;
		
		
	}
	
	/*removing old records*/
	function remove_old_browsing_products(){
		$this->query("SET `time_zone` = '".date('P')."'");
		$this->query("DELETE FROM `dp_guest_searches` WHERE UNIX_TIMESTAMP(`datetime`) <=UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 DAY))");	
	}
			
}
?>