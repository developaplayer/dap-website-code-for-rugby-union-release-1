<?php
/*
*Player Details Model
*Accenza web Pvt Ltd
*Develop By Rezaul Karim (karim@techpourri.com)
*/
class PlayerDetail extends AppModel
{
	public $name = 'PlayerDetail';
		public $validate = array(
			'dob' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Date of birth can not be blank.',
					'last' => true),
			),
			'nationality' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Nationality can not be blank.',
					'last' => true),
			),			
			'club_id' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Club can not be blank.',
					'last' => true),
			),			'age_group_id' => array(				'notEmpty' => array(					'rule' => 'notEmpty',					'message' => 'Age group can not be blank.',					'last' => true),			),			'primary_position_id' => array(				'notEmpty' => array(					'rule' => 'notEmpty',					'message' => 'Primary position can not be blank.',					'last' => true),			),			'secondary_position_id' => array(				'notEmpty' => array(					'rule' => 'notEmpty',					'message' => 'Secondary position can not be blank.',					'last' => true),			),
			'height' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Height can not be blank.',
					'last' => true),	
			),
			'weight' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Weight can not be blank.',
					'last' => true),
			)
		);
		public $belongsTo = array(
			'User'=>array(
				'className'=>'User',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'user_id',
				'fields' =>''
			),
			'PrimaryPosition'=>array(
				'className'=>'PlayerPosition',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'primary_position_id',
				'fields' =>''
			),
			'SecondaryPosition'=>array(
				'className'=>'PlayerPosition',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'secondary_position_id',
				'fields' =>''
			),
			'Country'=>array(
				'className'=>'Country',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'nationality',
				'fields' =>''
			),
			'Club'=>array(
				'className'=>'Club',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'club_id',
				'fields' =>''
			)
		);	
}
?>