<?php
class Product extends AppModel
{
	public $name = 'Product';
	
	public $hasOne = array(
		'ProductImage'=>array(
			'className'=>'ProductImage',
			 'conditions'   => array('ProductImage.deflt_value'=>1) ,
			 'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'	=>'product_id',
			 'fields'       =>''
		)
		
		);
		
		public $hasMany = array(			
				 'AllProductImage'=>array(
				'className'=>'ProductImage',
				 //'conditions'   => array('ProductImage.deflt_value'=>1) ,
				 'order'        => '',
				 'dependent'    =>  true,
				 'foreignKey'	=>'product_id',
			    'fields'       =>''
		)
			);
 
}
