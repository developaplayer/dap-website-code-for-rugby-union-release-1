<?php
/*
*Player Details Model
*Accenza web Pvt Ltd
*Develop By Rezaul Karim (karim@techpourri.com)
*/
class UserComment extends AppModel
{
	public $name = 'UserComment';	
		
		public $belongsTo = array(
			'User'=>array(
				'className'=>'User',
				'conditions'   =>false,
				'order'        => '',
				'dependent'    =>  false,
				'foreignKey'=>'user_id',
				'fields' =>''
			)
			
		);

		
}
?>