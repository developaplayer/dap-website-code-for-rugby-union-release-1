<?php
class User extends AppModel
{
	public $name = 'User';	
	public $validate = array(
			'email_address'=> array(
				'notEmpty'  => array(
					'rule'      => 'notEmpty',
					'last' => true,
					'message'   => 'Email address can not be blank.',
					),
				'email' => array(
					'rule' => 'email',
					'message' => 'Please enter a valid email address.'),
				'unique' => array(
					'rule'    => 'isUnique',
					'message'    => 'This email is already in use.',
					'allowEmpty' => true),
			),
			'password' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Password can not be blank.',
					'last' => true
					),
				'length' => array( 
					'rule' => array('minLength', 5), 
					'message' => 'Password must be at least 5 characters long.'
				)
			),
			'Confirm_password' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Confirm password can not be blank.',
					'last' => true),
				'confirmation' => array(
					'rule' => 'UserPasswordConfirmation',
					'message' => 'Passwords are mismatched.')
			),
			'first_name' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'First Name can not be blank.',
					'last' => true),
			),
			'last_name' => array(
				'notEmpty' => array(
					'rule' => 'notEmpty',
					'message' => 'Last Name can not be blank.',
					'last' => true),
			),
		);	
	public $hasOne = array(
		'CoachDetail'=>array(
			'className'=>'CoachDetail',				'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'	=>'user_id',
			 'fields'       =>''
		),
		'PlayerDetail'=>array(
			'className'=>'PlayerDetail',
			 'order'        => '',
			 'dependent'    =>  true,
			 'foreignKey'	=>'user_id',
			 'fields'       =>''
		),
		'CoachSpecialityP'=>array(
			 'className'=>'CoachSpeciality',
			 'conditions'   => 'CoachSpecialityP.id=CoachDetail.primary_speciality_id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields'       =>''
		),
		'CoachSpecialityS'=>array(
			'className'=>'CoachSpeciality',
			'conditions'   => 'CoachSpecialityS.id=CoachDetail.secondary_speciality_id',
			'order'        => '',
			'dependent'    =>  false,
			'foreignKey'=>'',
			'fields'       =>''
		),
		'Nationality'=>array(
			 'className'=>'Country',
			 'conditions'   => 'Nationality.id=CoachDetail.nationality',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields'       =>'Nationality.country_name'		)
	);
	
	/*Check if password and password confirmation are the same.*/
	/*function UserPasswordConfirmation() {
		//echo $this->alias;
		//exit;
		return $this->data[$this->alias]['password'] === $this->data[$this->alias]['Confirm_password'];
		
	}*/		
	function UserPasswordConfirmation() {
		
		if(trim($this->data['User']['password']) == trim($this->data['User']['Confirm_password']))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	/*public $belongsTo = array(
	         'CourseCoache'=>array(
			 'className'=>'CourseCoache',
			 'conditions'   => 'CourseCoache.coach_id=User.id',
			 'order'        => '',
			 'dependent'    =>  false,
			 'foreignKey'=>'',
			 'fields' =>''
	         )
	);*/
}