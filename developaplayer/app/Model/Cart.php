<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Cart extends AppModel {
 
    public $useTable = false;
     var $uses = array('ProductSizeQty');
    /*
     * add a product to cart
     */
    public function addProduct($productId,$size_code,$productDetails) {
		//CakeSession::write('cart',array());
		//exit;
		$productId_size_id=$productId.'-'.$size_code;
		$productDetails['Cart']=$productDetails;
		
        $allProducts = CakeSession::read('cart');
		
      if (null!=$allProducts) {
            if (array_key_exists($productId_size_id,$allProducts)) {
			$ProductSizeQty = ClassRegistry::init('ProductSizeQty');
			$checkStock = $ProductSizeQty->find('first',array('conditions' => array("product_id"=>$productId,"size_id"=>$size_code),'recursive' => -1 ));
			$total_remaing_quantiy=$checkStock ['ProductSizeQty']['qty']-$checkStock ['ProductSizeQty']['sale_qty'];
			$totala_qty=$allProducts[$productId_size_id]['product_qty']+$productDetails['Cart']['product_qty'];		
		if($totala_qty>$total_remaing_quantiy)
		{
			 $allProducts[$productId.'-'.$size_code]['product_qty'] =$total_remaing_quantiy ; 
		}else
		{
			  $allProducts[$productId.'-'.$size_code]['product_qty']=$allProducts[$productId_size_id]['product_qty']+$productDetails['Cart']['product_qty'];
		}	
            } else {
              $allProducts[$productId.'-'.$size_code] = $productDetails['Cart'];
            }
        } else {
            $allProducts[$productId.'-'.$size_code] = $productDetails['Cart'];
        }
		
		//pr($allProducts);
		//exit;
	
     /*if (null!=$allProducts) {
        	$allProducts[] = $productDetails['Cart'];
			echo 'lkml---ablek';
        } else {
        	$allProducts[] = $productDetails['Cart'];
			
        }*/
		
		
        $this->saveProduct($allProducts);
    }
     
    /*
     * get total count of products
     */
    public function getCount() {
        $allProducts = $this->readProduct();    
        if (count($allProducts)<1) {
            return 0;
        }         
        $count = 0;
       // foreach ($allProducts as $product) {
           // $count=$count+$product;
       // }
        $count=$count+count($allProducts);
        return $count;
    }
 
    /*
     * save data to session
     */
    public function saveProduct($data) 
	{
        return CakeSession::write('cart',$data);
    }
 
    /*
     * read cart data from session
     */
    public function readProduct() 
	{
        return CakeSession::read('cart');
    }
 	
    public function readProductCheckoutSummery() 
	{
    	return CakeSession::read('cart-view');
    }
	
	public function readProductCheckoutOrderAddress()
	{
    	return CakeSession::read('order-address');
    }
	
	public function destroyCart()
	{
		CakeSession::write('cart',array());
		CakeSession::write('cart-view',array());
    	CakeSession::write('order-address',array());
    }
}