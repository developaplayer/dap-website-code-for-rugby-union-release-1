<?php
class ProductSizeQty extends AppModel
{
	public $name = 'ProductSizeQty';

		public $hasOne = array(
			'Size'=>array(
				'className'=>'Size',				
				 'conditions' => 'ProductSizeQty.size_id=Size.id',
				 'dependent'    => true,
				 'foreignKey'	=>'',
				 'fields'       =>''
				 )
			);
 
}
